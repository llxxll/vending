package com.fc.v2.util;

import java.text.SimpleDateFormat;
import java.util.*;


public class DataTest {

    private static final int FIRST_DAY = Calendar.MONDAY;


    public static List<Map> dates(Integer number) {
        SimpleDateFormat dateFormatNow = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat dateFormatNow2 = new SimpleDateFormat("MM-dd EE");
        List<Map> aa=new ArrayList<>();
//        Map mapNow = new HashMap<>();
//        mapNow.put("year",dateFormatNow.format(new Date()));
//        mapNow.put("day",dateFormatNow2.format(new Date()));
//        aa.add(mapNow);
        Calendar calendar = Calendar.getInstance();
//        while (calendar.get(Calendar.DAY_OF_WEEK) != FIRST_DAY) {
//            calendar.add(Calendar.DATE, +1);
//        }
        for (int i = 0; i < number; i++) {
            SimpleDateFormat dateFormatNew = new SimpleDateFormat("yyyy/M/d");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd");
            SimpleDateFormat dateFormatzhou = new SimpleDateFormat("EE");
            Map map = new HashMap<>();
            map.put("year",dateFormatNew.format(calendar.getTime()));
            map.put("day",dateFormat.format(calendar.getTime()));
            map.put("zhou",dateFormatzhou.format(calendar.getTime()));

            aa.add(map);
            calendar.add(Calendar.DATE, 1);
        }
        return aa;
    }
}
