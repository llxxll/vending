package com.fc.v2.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Msg {

    private Integer code;
    private String msg;
    private Object data;
    private long count;
}
