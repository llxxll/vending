package com.fc.v2.util;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 读取Excel
 *
 * @
 */
public class ReadExcelUtils {
    private Logger logger = LoggerFactory.getLogger(ReadExcelUtils.class);
    private Workbook wb;
    private Sheet sheet;
    private Row row;

    public ReadExcelUtils(String filepath) {
        if(filepath==null){
            return;
        }
        String ext = filepath.substring(filepath.lastIndexOf("."));
        try {
            InputStream is = new FileInputStream(filepath);
            if(".xls".equals(ext)){
                wb = new HSSFWorkbook(is);
            }else if(".xlsx".equals(ext)){
                wb = new XSSFWorkbook(is);
            }else{
                wb=null;
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        } catch (IOException e) {
            logger.error("IOException", e);
        }
    }

    /**
     * 读取Excel表格表头的内容
     *
     * @param InputStream
     * @return String 表头内容的数组
     */
    public String[] readExcelTitle() throws Exception{
        if(wb==null){
            throw new Exception("Workbook对象为空！");
        }
        sheet = wb.getSheetAt(0);
        row = sheet.getRow(0);
        // 标题总列数
        int colNum = row.getPhysicalNumberOfCells();
        System.out.println("colNum:" + colNum);
        String[] title = new String[colNum];
        for (int i = 0; i < colNum; i++) {
            // title[i] = getStringCellValue(row.getCell((short) i));
            title[i] = row.getCell(i).getCellFormula();
        }
        return title;
    }

    /**
     * 读取Excel数据内容
     *
     * @param InputStream
     * @return Map 包含单元格数据内容的Map对象
     */
    public Map<Integer, Map<Integer,Object>> readExcelContent() throws Exception{
        if(wb==null){
            throw new Exception("Workbook对象为空！");
        }
        Map<Integer, Map<Integer,Object>> content = new HashMap<Integer, Map<Integer,Object>>();

        sheet = wb.getSheetAt(0);
        // 得到总行数
        int rowNum = sheet.getLastRowNum();
        row = sheet.getRow(0);
        int colNum = row.getPhysicalNumberOfCells();
        // 正文内容应该从第二行开始,第一行为表头的标题
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            int j = 0;
            Map<Integer,Object> cellValue = new HashMap<Integer, Object>();
            while (j < colNum) {
                Object obj = getCellFormatValue(row.getCell(j));
                cellValue.put(j, obj);
                j++;
            }
            content.put(i, cellValue);
        }
        return content;
    }

    /**
     *
     * 根据Cell类型设置数据
     *
     * @param cell
     * @return
     */
    private Object getCellFormatValue(Cell cell) {
        Object cellvalue = "";
        if (cell != null) {
            // 判断当前Cell的Type
            switch (cell.getCellType()) {
                case NUMERIC:// 如果当前Cell的Type为NUMERIC
                case FORMULA: {
                    // 判断当前的cell是否为Date
                    if (DateUtil.isCellDateFormatted(cell)) {
                        // 如果是Date类型则，转化为Data格式
                        // data格式是带时分秒的：2013-7-10 0:00:00
                        // cellvalue = cell.getDateCellValue().toLocaleString();
                        // data格式是不带带时分秒的：2013-7-10
                        Date date = cell.getDateCellValue();
                        cellvalue = date;
                    } else {// 如果是纯数字

                        // 取得当前Cell的数值
                        cellvalue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case STRING:// 如果当前Cell的Type为STRING
                    // 取得当前的Cell字符串
                    cellvalue = cell.getRichStringCellValue().getString();
                    break;
                default:// 默认的Cell值
                    cellvalue = "";
            }
        } else {
            cellvalue = "";
        }
        return cellvalue;
    }
    public static void main(String[] args) throws Exception {
        try {
            String filepath = "/Users/lxl/Desktop/123/bbb.xlsx";
            ReadExcelUtils excelReader = new ReadExcelUtils(filepath);
            Map<Integer, Map<Integer,Object>> map = excelReader.readExcelContent();
            System.out.println("获得Excel表格的内容:");
//            for (int i = 1; i <= map.size(); i++) {
//                Map<Integer,Object> mapp =  map.get(i);
//                List<String> list = Collections.singletonList(String.valueOf(mapp.get(40)));
//                System.out.println(list);
//            }
            Map<Integer,List<String>> mmm = new HashMap<>();
            for(int l = 1; l < map.size(); l++){
                Map<Integer,Object> mapp =  map.get(l);
                String s = String.valueOf(mapp.get(40));
                Map<Integer, Map<Integer, Object>> list2 = new HashMap<>(map);
                list2.remove(l);
                for (int i = 0; i < s.length() - 8; i++) {
                    String substring = s.substring(i, i + 8);
                    //String substring = substringw.replace(",", "").replace("，","");
                    int finalL = l;
                    if(substring.contains("2019") ||substring.contains("2020") ||substring.contains("中国移动") ||substring.contains("1008") ||substring.contains("2018") ||substring.contains("10086") ||substring.contains("2021") ||substring.contains("2022") || substring.contains("2023")){
                        break;
                    }
                    list2.forEach((k, v) -> {
                                String s2 = String.valueOf(v.get(40));
                                if (s2.contains(substring)) {
                                    //System.out.println("重复数据：" + substring + "，重复listid："+ finalL);
                                    int num = finalL + 1;
                                    int kk = k + 1;
                                    List<String> integerObjectMap = mmm.get(num);
                                    if(integerObjectMap!=null){
                                        integerObjectMap.add(substring);
                                        mmm.put(num, integerObjectMap);
                                    }else{
                                        List<String> integerObjectMap2 = new ArrayList<>();

                                        integerObjectMap2.add("相同行：" + kk);
                                        integerObjectMap2.add(substring);
                                        mmm.put(num, integerObjectMap2);
                                    }

                                }
                            }
                    );

                }
            }
            //System.out.println(JSONObject.toJSONString(mmm));
            Map<Integer,List<String>> mmm2 = new HashMap<>();
            mmm.forEach((k, v) -> {
                List<String> ls = v;
                int ii = k;
                List<String> ls2 = new ArrayList<>();

                for (int l = 0; l < ls.size(); l++) {
                    if(l==0){
                        ls2.add(ls.get(l));
                    }else{
                        String s = ls.get(l);
                        String replace = s.replace(",", "").replace("，", "");
                        if(replace.length()>7){
                            ls2.add(ls.get(l));
                        }
                    }


                }
                if(ls2.size()>1){
                    mmm2.put(k, ls2);
                }
            });
            //System.out.println(JSONObject.toJSONString(mmm));
            System.out.println(JSONObject.toJSONString(mmm2));
        } catch (FileNotFoundException e) {
            System.out.println("未找到指定路径的文件!");
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main2(String[] args) throws Exception {
        String sss = "/Users/lxl/Desktop/123/电子档（350X500）";
        try {
            String filepath = "/Users/lxl/Desktop/123/aaa.xls";
            ReadExcelUtils excelReader = new ReadExcelUtils(filepath);
            // 对读取Excel表格标题测试
//            String[] title = excelReader.readExcelTitle();
//            System.out.println("获得Excel表格的标题:");
//            for (String s : title) {
//                System.out.print(s + " ");
//            }

            // 对读取Excel表格内容测试
            Map<Integer, Map<Integer,Object>> map = excelReader.readExcelContent();
            System.out.println("获得Excel表格的内容:");
            for (int i = 1; i <= map.size(); i++) {
                Map<Integer,Object> mapp =  map.get(i);
                System.out.println(String.valueOf(mapp.get(0)));
                System.out.println(mapp.get(1));
                changeFileName(sss,mapp);
            }
        } catch (FileNotFoundException e) {
            System.out.println("未找到指定路径的文件!");
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void changeFileName(String path, Map<Integer, Object> mapp) throws Exception {
        File file = new File(path);
        if(file.exists()){
            File[] files = file.listFiles();
            if (null == files || files.length == 0) {
                System.out.println("文件夹是空的!");
                return;
            } else {
                for (File file2 : files) {

                        System.out.println("文件:" + file2.getAbsolutePath());
                        String filePath = file2.getAbsolutePath();//
                        //String fileName = filePath.substring(0,filePath.lastIndexOf("/"))+"/aaa"+filePath.substring(filePath.lastIndexOf("."));
                    String fileName = "";
                        System.out.println("filePath:"+filePath);
                        System.out.println("fileName:"+fileName);
                        String value = (String) mapp.get(1);
                        String gh = (String) mapp.get(0);
                        if(filePath.contains(value)){
                            System.out.println("名字对应成功，名字："+value+",工号："+gh);
                            BigDecimal decimal = new BigDecimal(gh);
                            int result = decimal.intValue();
                            fileName = filePath.substring(0,filePath.lastIndexOf("/"))+"/"+result+filePath.substring(filePath.lastIndexOf("."));
                            System.out.println("fileName2:"+fileName);
                            File oriFile = new File(filePath);
                            boolean b = oriFile.renameTo(new File(fileName));
                            System.out.println(b);
                        }else{
                            System.out.println("名字对应失败");
                        }


                }
            }
        }else{
            System.out.println("该路径不存在");
        }

    }
}
