package com.fc.v2.util;

import com.fc.v2.model.FacadeResponse;
import com.google.common.collect.Lists;
import org.apache.commons.collections.MapUtils;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

public class HttpFiled {

        private static final ContentType STRING_CONTENT_TYPE = ContentType.create("text/plain", StandardCharsets.UTF_8);

        public static FacadeResponse multipartPost(String url, Map<String, String> headers, Map<String, Object> paramMap) {
            // 创建 HttpPost 对象
            HttpPost httpPost = new HttpPost(url);
            // 设置请求头
            if (MapUtils.isNotEmpty(headers)) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    httpPost.setHeader(key, value);
                }
            }
            // 设置请求参数
            if (MapUtils.isNotEmpty(paramMap)) {
                // 使用 MultipartEntityBuilder 构造请求体
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                //设置浏览器兼容模式
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                //设置请求的编码格式
                builder.setCharset(Consts.UTF_8);
                // 设置 Content-Type
                builder.setContentType(ContentType.MULTIPART_FORM_DATA);
                for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = paramMap.get(key);
                    // 添加请求参数
                    addMultipartBody(builder, key, value);
                }
                HttpEntity entity = builder.build();
                // 将构造好的 entity 设置到 HttpPost 对象中
                httpPost.setEntity(entity);
            }
            return execute(httpPost, null);
        }

        private static void addMultipartBody(MultipartEntityBuilder builder, String key, Object value) {
            if (value == null) {
                return;
            }
            // MultipartFile 是 spring mvc 接收到的文件。
            if (value instanceof MultipartFile) {
                MultipartFile file = (MultipartFile) value;
                try {
                    builder.addBinaryBody(key, file.getInputStream(), ContentType.MULTIPART_FORM_DATA, file.getOriginalFilename());
                } catch (IOException e) {
                    System.out.println("read file err."+e);
                }
            } else if (value instanceof File) {
                File file = (File) value;
                builder.addBinaryBody(key, file, ContentType.MULTIPART_FORM_DATA, file.getName());
            } else if (value instanceof List) {
                // 列表形式的参数，要一个一个 add
                List<?> list = (List<?>) value;
                for (Object o : list) {
                    addMultipartBody(builder, key, o);
                }
            } else if (value instanceof Date) {
                // 日期格式的参数，使用约定的格式
                builder.addTextBody(key, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value));
            } else {
                // 使用 UTF_8 编码的 ContentType，否则可能会有中文乱码问题
                builder.addTextBody(key, value.toString(), STRING_CONTENT_TYPE);
            }
        }

        private static FacadeResponse execute(HttpRequestBase httpRequestBase, HttpContext context) {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            // 使用 try-with-resources 发起请求，保证请求完成后资源关闭
            try (CloseableHttpResponse httpResponse = httpClient.execute(httpRequestBase, context)) {
                // 处理响应头
                Map<String, List<String>> headers = headerToMap(httpResponse.getAllHeaders());
                // 处理响应体
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    String entityContent = EntityUtils.toString(httpEntity, Consts.UTF_8);
                    return new FacadeResponse(httpRequestBase.getRequestLine().getUri(), httpResponse.getStatusLine().getStatusCode(), headers, entityContent);
                }
            } catch (Exception ex) {
                System.out.println("http execute failed."+ex);
            }
            return new FacadeResponse(httpRequestBase.getRequestLine().getUri(), HttpStatus.INTERNAL_SERVER_ERROR.value(), null, "http execute failed.");
        }

        /**
         * 将headers转map
         *
         * @param headers 头信息
         * @return map
         */
        private static Map<String, List<String>> headerToMap(Header[] headers) {
            if (null == headers || headers.length == 0) {
                return Collections.emptyMap();
            }
            Map<String, List<String>> map = new HashMap<>();
            for (Header header : headers) {
                map.putIfAbsent(header.getName(), Lists.newArrayList(header.getValue()));
            }
            return map;
        }

}
