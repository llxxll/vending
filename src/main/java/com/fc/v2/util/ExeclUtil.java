package com.fc.v2.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.StringUtils;


public class ExeclUtil {
    static int sheetsize = 5000;

    /**
     * @param data
     *            导入到excel中的数据
     * @param out
     *            数据写入的文件
     * @param fields
     *            需要注意的是这个方法中的map中：每一列对应的实体类的英文名为键，excel表格中每一列名为值
     * @throws Exception
     */
    public static <T> void ListtoExecl(List<T> data, OutputStream out,
            Map<String, String> fields) throws Exception {
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 如果导入数据为空，则抛出异常。
        if (data == null || data.size() == 0) {
            workbook.close();
            throw new Exception("导入的数据为空");
        }
        // 根据data计算有多少页sheet
        int pages = data.size() / sheetsize;
        if (data.size() % sheetsize > 0) {
            pages += 1;
        }
        // 提取表格的字段名（英文字段名是为了对照中文字段名的）
        String[] egtitles = new String[fields.size()];
        String[] cntitles = new String[fields.size()];
        Iterator<String> it = fields.keySet().iterator();
        int count = 0;
        while (it.hasNext()) {
            String egtitle = (String) it.next();
            String cntitle = fields.get(egtitle);
            egtitles[count] = egtitle;
            cntitles[count] = cntitle;
            count++;
        }
        // 添加数据
        for (int i = 0; i < pages; i++) {
            int rownum = 0;
            // 计算每页的起始数据和结束数据
            int startIndex = i * sheetsize;
            int endIndex = (i + 1) * sheetsize - 1 > data.size() ? data.size()
                    : (i + 1) * sheetsize - 1;
            // 创建每页，并创建第一行
            HSSFSheet sheet = workbook.createSheet();
            HSSFRow row = sheet.createRow(rownum);

            // 在每页sheet的第一行中，添加字段名
            for (int f = 0; f < cntitles.length; f++) {
                HSSFCell cell = row.createCell(f);
                cell.setCellValue(cntitles[f]);
            }
            rownum++;
            // 将数据添加进表格
            for (int j = startIndex; j < endIndex; j++) {
                row = sheet.createRow(rownum);
                T item = data.get(j);
                for (int h = 0; h < cntitles.length; h++) {
                    Field fd = item.getClass().getDeclaredField(egtitles[h]);
                    fd.setAccessible(true);
                    Object o = fd.get(item);
                    String value = o == null ? "" : o.toString();
                    HSSFCell cell = row.createCell(h);
                    cell.setCellValue(value);
                }
                rownum++;
            }
        }
        // 将创建好的数据写入输出流
        workbook.write(out);
        // 关闭workbook
        workbook.close();
    }

    public static <T> List<T> ExecltoList(InputStream in, Class<T> entityClass,
                                          Map<String, String> fields,String filename) throws Exception {

        return null;
    }
    
    
//    public static <T> List<T> ExecltoList(InputStream in, Class<T> entityClass,
//            Map<String, String> fields,String filename) throws Exception {
//    	Workbook wb = null;
//        List<T> resultList = new ArrayList<T>();
//            if (filename.endsWith("xls")) {
//                wb = new HSSFWorkbook(in);
//            } else if (filename.endsWith("xlsx")) {
//                wb = new XSSFWorkbook(in);
//            }
//
//
//        // excel中字段的中英文名字数组
//        String[] egtitles = new String[fields.size()];
//        String[] cntitles = new String[fields.size()];
//        Iterator<String> it = fields.keySet().iterator();
//        int count = 0;
//        while (it.hasNext()) {
//            String cntitle = (String) it.next();
//            String egtitle = fields.get(cntitle);
//            egtitles[count] = egtitle;
//            cntitles[count] = cntitle;
//            count++;
//        }
//
//        // 得到excel中sheet总数
//        int sheetcount = wb.getNumberOfSheets();
//
//        if (sheetcount == 0) {
//            wb.close();
//            throw new Exception("Excel文件中没有任何数据");
//        }
//
//        // 数据的导出
//        for (int i = 0; i < sheetcount; i++) {
//            Sheet sheet = wb.getSheetAt(i);
//            if (sheet == null) {
//                continue;
//            }
//            // 每页中的第一行为标题行，对标题行的特殊处理
//            Row firstRow = sheet.getRow(0);
//            if (firstRow == null) {
//                continue;
//            }
//            int celllength = firstRow.getLastCellNum();
//
//            String[] excelFieldNames = new String[celllength];
//            LinkedHashMap<String, Integer> colMap = new LinkedHashMap<String, Integer>();
//
//            // 获取Excel中的列名
//            for (int f = 0; f < celllength; f++) {
//                Cell cell = firstRow.getCell(f);
//                excelFieldNames[f] = cell.getStringCellValue().trim();
//                // 将列名和列号放入Map中,这样通过列名就可以拿到列号
//                for (int g = 0; g < excelFieldNames.length; g++) {
//                    colMap.put(excelFieldNames[g], g);
//                }
//            }
//            // 由于数组是根据长度创建的，所以值是空值，这里对列名map做了去空键的处理
//            colMap.remove(null);
//            // 判断需要的字段在Excel中是否都存在
//            // 需要注意的是这个方法中的map中：中文名为键，英文名为值
//            boolean isExist = true;
//            List<String> excelFieldList = Arrays.asList(excelFieldNames);
//            for (String cnName : fields.keySet()) {
//                if (!excelFieldList.contains(cnName)) {
//                    isExist = false;
//                    System.out.println(cnName);
//                    break;
//                }
//            }
//            // 如果有列名不存在，则抛出异常，提示错误
//            if (!isExist) {
//                wb.close();
//                throw new Exception("Excel中缺少必要的字段，或字段名称有误");
//            }
//            // 将sheet转换为list
//            for (int j = 1; j <= sheet.getLastRowNum(); j++) {
//                Row row = sheet.getRow(j);
//                if(row==null) {
//              	  continue;
//                }
//                // 根据泛型创建实体类
//                T entity = entityClass.newInstance();
//                boolean xxreul=false;
//                // 给对象中的字段赋值
//                for (Entry<String, String> entry : fields.entrySet()) {
//                    // 获取中文字段名
//                    String cnNormalName = entry.getKey();
//                    // 获取英文字段名
//                    String enNormalName = entry.getValue();
//                    // 根据中文字段名获取列号
//                    int col = colMap.get(cnNormalName);
//                    // 获取当前单元格中的内容
//                  //  String content = row.getCell(col).toString().trim();
//                      Cell cell = row.getCell(col);
//                    //转换值
//                    String forwardCellValue = forwardCellValue(cell);
//                    if(!StringUtils.isEmpty(forwardCellValue)) {
//                    	xxreul=true;
//                    }
//                    // 给对象赋值
//                    setFieldValueByName(enNormalName, forwardCellValue, entity);
//                }
//                if(xxreul==true) {
//                	resultList.add(entity);
//                }
//            }
//        }
//        wb.close();
//        return resultList;
//    }
   
    
    
    /**
     * 转换单元格值得类型
    * @Title：forwardCellValue 
    * @param ：@param cell
    * @param ：@return 
    * @return ：String 
    * @author : 双鱼
    * @throws
     */
    
//    private static String forwardCellValue(Cell cell) {
//    	String cellValue = "";
//		if (null != cell) {
//			// 以下是判断数据的类型
//			switch (cell.getCellType()) {
//			case HSSFCell.CELL_TYPE_NUMERIC: // 数字
//				if (HSSFDateUtil.isCellDateFormatted(cell)) {// 处理日期格式、时间格式
//	                SimpleDateFormat sdf = null;
//	                // 验证short值
//	                if (cell.getCellStyle().getDataFormat() == 14) {
//	                    sdf = new SimpleDateFormat("yyyy/MM/dd");
//	                } else if (cell.getCellStyle().getDataFormat() == 21) {
//	                    sdf = new SimpleDateFormat("HH:mm:ss");
//	                } else if (cell.getCellStyle().getDataFormat() == 22) {
//	                    sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//	                } else {
//	                    throw new RuntimeException("日期格式错误!!!");
//	                }
//	                Date date = cell.getDateCellValue();
//	                cellValue = sdf.format(date);
//	            } else if (cell.getCellStyle().getDataFormat() == 0||cell.getCellStyle().getDataFormat() ==178) {//处理数值格式
//	                cell.setCellType(Cell.CELL_TYPE_STRING);
//	                cellValue = String.valueOf(cell.getRichStringCellValue().getString());
//	            }
//	            break;
//			case HSSFCell.CELL_TYPE_STRING: // 字符串
//				cellValue = cell.getStringCellValue();
//				break;
//			case HSSFCell.CELL_TYPE_BOOLEAN: // Boolean
//				cellValue = cell.getBooleanCellValue() + "";
//				break;
//			case HSSFCell.CELL_TYPE_FORMULA: // 公式
//				cellValue = cell.getCellFormula() + "";
//				break;
//			case HSSFCell.CELL_TYPE_BLANK: // 空值
//				cellValue = "";
//				break;
//			case HSSFCell.CELL_TYPE_ERROR: // 故障
//				cellValue = "非法字符";
//				break;
//			default:
//				cellValue = "未知类型";
//				break;
//			}
//		}
//
//		return cellValue;
//	}




	/**
     * @MethodName : setFieldValueByName
     * @Description : 根据字段名给对象的字段赋值
     * @param fieldName
     *            字段名
     * @param fieldValue
     *            字段值
     * @param o
     *            对象
     */
    private static void setFieldValueByName(String fieldName,
            Object fieldValue, Object o) throws Exception {
        Field field = getFieldByName(fieldName, o.getClass());
        if (field != null) {
            field.setAccessible(true);
            // 获取字段类型
            Class<?> fieldType = field.getType();
            if(fieldValue.equals("")||fieldValue==null||fieldValue.equals("null")) {
            	 field.set(o, null);
            }else {
            	 // 根据字段类型给字段赋值
                if (String.class == fieldType) {
                    field.set(o, String.valueOf(fieldValue));
                } else if ((Integer.TYPE == fieldType)
                        || (Integer.class == fieldType)) {
                	Integer x =(int) Double.parseDouble(fieldValue.toString());
                    field.set(o, x);
                } else if ((Long.TYPE == fieldType) || (Long.class == fieldType)) {
                    field.set(o, Long.valueOf(fieldValue.toString()));
                } else if ((Float.TYPE == fieldType) || (Float.class == fieldType)) {
                    field.set(o, Float.valueOf(fieldValue.toString()));
                } else if ((Short.TYPE == fieldType) || (Short.class == fieldType)) {
                    field.set(o, Short.valueOf(fieldValue.toString()));
                } else if ((Double.TYPE == fieldType)
                        || (Double.class == fieldType)) {
                    field.set(o, Double.valueOf(fieldValue.toString()));
                } else if (Character.TYPE == fieldType) {
                    if ((fieldValue != null)
                            && (fieldValue.toString().length() > 0)) {
                        field.set(o,
                                Character.valueOf(fieldValue.toString().charAt(0)));
                    }
                } else if (Date.class == fieldType) {
                    field.set(o, new SimpleDateFormat("yyyy/MM/dd")
                            .parse(fieldValue.toString()));
                } else {
                    field.set(o, fieldValue);
                }
            }
           
        } else {
            throw new Exception(o.getClass().getSimpleName() + "类不存在字段名 "
                    + fieldName);
        }
    }

    /**
     * @MethodName : getFieldByName
     * @Description : 根据字段名获取字段
     * @param fieldName
     *            字段名
     * @param clazz
     *            包含该字段的类
     * @return 字段
     */
    private static Field getFieldByName(String fieldName, Class<?> clazz) {
        // 拿到本类的所有字段
        Field[] selfFields = clazz.getDeclaredFields();

        // 如果本类中存在该字段，则返回
        for (Field field : selfFields) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }

        // 否则，查看父类中是否存在此字段，如果有则返回
        Class<?> superClazz = clazz.getSuperclass();
        if (superClazz != null && superClazz != Object.class) {
            return getFieldByName(fieldName, superClazz);
        }

        // 如果本类和父类都没有，则返回空
        return null;
    }
}
