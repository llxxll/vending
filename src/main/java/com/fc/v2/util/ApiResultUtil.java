package com.fc.v2.util;


import java.util.List;

public class ApiResultUtil {
    /**
     * 请求成功返回
     * @return
     */
    public static Msg success(List<?> list, long count){
        Msg msg=new Msg();
        msg.setCode(0);
        msg.setCount(count);
        msg.setMsg("");
        msg.setData(list);
        return msg;
    }


    public static Msg error(Integer code,String resultmsg){
        Msg msg=new Msg();
        msg.setCode(code);
        msg.setMsg(resultmsg);
        return msg;
    }


}
