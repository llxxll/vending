package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshSupplier;
import com.fc.v2.model.auto.JshSupplierExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 供应商/客户信息表 JshSupplierMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 23:17:26
 */
public interface JshSupplierMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshSupplierExample example);

    int deleteByExample(JshSupplierExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshSupplier record);

    int insertSelective(JshSupplier record);

    List<JshSupplier> selectByExample(JshSupplierExample example);
		
    JshSupplier selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshSupplier record, @Param("example") JshSupplierExample example);

    int updateByExample(@Param("record") JshSupplier record, @Param("example") JshSupplierExample example); 
		
    int updateByPrimaryKeySelective(JshSupplier record);

    int updateByPrimaryKey(JshSupplier record);

    List<JshSupplier> findAll();
}