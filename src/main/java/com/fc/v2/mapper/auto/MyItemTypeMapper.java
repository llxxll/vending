package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MyItemType;
import com.fc.v2.model.auto.MyItemTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  MyItemTypeMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-10-16 22:58:25
 */
public interface MyItemTypeMapper {
      	   	      	      	      	      	      	      
    long countByExample(MyItemTypeExample example);

    int deleteByExample(MyItemTypeExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MyItemType record);

    int insertSelective(MyItemType record);

    List<MyItemType> selectByExample(MyItemTypeExample example);
		
    MyItemType selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MyItemType record, @Param("example") MyItemTypeExample example);

    int updateByExample(@Param("record") MyItemType record, @Param("example") MyItemTypeExample example); 
		
    int updateByPrimaryKeySelective(MyItemType record);

    int updateByPrimaryKey(MyItemType record);

	List<MyItemType> getList(MyItemType myItemType);

	MyItemType selectByTypeName(@Param("typeName")String typeName);
  	  	
}