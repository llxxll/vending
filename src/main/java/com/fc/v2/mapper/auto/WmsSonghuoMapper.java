package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.WmsSonghuo;
import com.fc.v2.model.auto.WmsSonghuoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 出库单明细 WmsSonghuoMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:44
 */
public interface WmsSonghuoMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(WmsSonghuoExample example);

    int deleteByExample(WmsSonghuoExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(WmsSonghuo record);

    int insertSelective(WmsSonghuo record);

    List<WmsSonghuo> selectByExample(WmsSonghuoExample example);
		
    WmsSonghuo selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") WmsSonghuo record, @Param("example") WmsSonghuoExample example);

    int updateByExample(@Param("record") WmsSonghuo record, @Param("example") WmsSonghuoExample example); 
		
    int updateByPrimaryKeySelective(WmsSonghuo record);

    int updateByPrimaryKey(WmsSonghuo record);

	Integer getuncountBYpid(@Param("unitName")Long long1);
	
	Integer getcountBYpid(@Param("unitName")Long long1);

	Integer getcountbydevlied(@Param("deliverySid")String string);

	Integer getunCountBydevlied(@Param("deliverySid")String string);

	List<WmsSonghuo> getList(WmsSonghuo wmsSonghuo);

	List<WmsSonghuo> getListByWXid(@Param("unitName")String string);

	WmsSonghuo getOneBYpid(@Param("unitName")Long id);
  	  	
}