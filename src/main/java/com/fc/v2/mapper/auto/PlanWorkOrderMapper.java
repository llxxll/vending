package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.model.auto.PlanWorkOrderExample;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 工单 PlanWorkOrderMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:27:47
 */
public interface PlanWorkOrderMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanWorkOrderExample example);

    int deleteByExample(PlanWorkOrderExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanWorkOrder record);

    int insertSelective(PlanWorkOrder record);

    List<PlanWorkOrder> selectByExample(PlanWorkOrderExample example);
    List<PlanWorkOrder> getList(PlanWorkOrder example);
		
    PlanWorkOrder selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanWorkOrder record, @Param("example") PlanWorkOrderExample example);

    int updateByExample(@Param("record") PlanWorkOrder record, @Param("example") PlanWorkOrderExample example); 
		
    int updateByPrimaryKeySelective(PlanWorkOrder record);

    int updateByPrimaryKey(PlanWorkOrder record);

    Integer getCountByStatus(@Param(value="status") Integer status);

	PlanWorkOrder selectOneByworno(@Param(value="workerOrderNo") String workerOrderNo);

	int updateByworkorderNo(PlanWorkOrder planWorkOrderpz);

	int delBywrokNo(@Param(value="workerOrderNo")String long1);

	Integer getCountbydevlied(@Param(value="no")String no, @Param(value="deleteFlag")String deleteFlag);

	List<PlanWorkOrder> orderlist(PlanWorkOrder planWorkOrder);
  	  	
}