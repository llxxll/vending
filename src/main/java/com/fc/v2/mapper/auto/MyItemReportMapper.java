package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MyItemReport;
import com.fc.v2.model.auto.MyItemReportExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  MyItemReportMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-08-28 21:49:32
 */
public interface MyItemReportMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MyItemReportExample example);

    int deleteByExample(MyItemReportExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MyItemReport record);

    int insertSelective(MyItemReport record);

    List<MyItemReport> selectByExample(MyItemReportExample example);
		
    MyItemReport selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MyItemReport record, @Param("example") MyItemReportExample example);

    int updateByExample(@Param("record") MyItemReport record, @Param("example") MyItemReportExample example); 
		
    int updateByPrimaryKeySelective(MyItemReport record);

    int updateByPrimaryKey(MyItemReport record);

	int deleteByItemNo(@Param("itemNo")String itemNo);

	List<MyItemReport> getList(MyItemReport myItemReport);

	List<MyItemReport> getList1(MyItemReport myItemReport);
  	  	
}