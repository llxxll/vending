package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Point;
import com.fc.v2.model.auto.PointExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 点位管理表 PointMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:36:52
 */
public interface PointMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PointExample example);

    int deleteByExample(PointExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Point record);

    int insertSelective(Point record);

    List<Point> selectByExample(PointExample example);
		
    Point selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Point record, @Param("example") PointExample example);

    int updateByExample(@Param("record") Point record, @Param("example") PointExample example); 
		
    int updateByPrimaryKeySelective(Point record);

    int updateByPrimaryKey(Point record);
  	  	
}