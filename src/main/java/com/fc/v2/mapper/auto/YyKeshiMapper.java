package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyKeshi;
import com.fc.v2.model.auto.YyKeshiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院科室 YyKeshiMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:28
 */
public interface YyKeshiMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyKeshiExample example);

    int deleteByExample(YyKeshiExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyKeshi record);

    int insertSelective(YyKeshi record);

    List<YyKeshi> selectByExample(YyKeshiExample example);
		
    YyKeshi selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyKeshi record, @Param("example") YyKeshiExample example);

    int updateByExample(@Param("record") YyKeshi record, @Param("example") YyKeshiExample example); 
		
    int updateByPrimaryKeySelective(YyKeshi record);

    int updateByPrimaryKey(YyKeshi record);

    List<YyKeshi> listtoten();

    List<String> getListByField1();

    List<YyKeshi> selectByFiled1(@Param("field1")String field1);

	List<YyKeshi> selectsslist();
}