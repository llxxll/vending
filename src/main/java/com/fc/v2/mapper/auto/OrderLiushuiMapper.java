package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OrderLiushui;
import com.fc.v2.model.auto.OrderLiushuiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  OrderLiushuiMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-11 21:27:22
 */
public interface OrderLiushuiMapper {
      	   	      	      	      	      	      	      	      
    long countByExample(OrderLiushuiExample example);

    int deleteByExample(OrderLiushuiExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OrderLiushui record);

    int insertSelective(OrderLiushui record);

    List<OrderLiushui> selectByExample(OrderLiushuiExample example);
		
    OrderLiushui selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OrderLiushui record, @Param("example") OrderLiushuiExample example);

    int updateByExample(@Param("record") OrderLiushui record, @Param("example") OrderLiushuiExample example); 
		
    int updateByPrimaryKeySelective(OrderLiushui record);

    int updateByPrimaryKey(OrderLiushui record);

	List<OrderLiushui> getList(OrderLiushui orderLiushui);

    OrderLiushui getUpdateUserSid(@Param("orderNo")String orderNo);
}