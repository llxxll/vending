package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyDetail;
import com.fc.v2.model.auto.YyDetailExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  YyDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-21 18:33:41
 */
public interface YyDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyDetailExample example);

    int deleteByExample(YyDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyDetail record);

    int insertSelective(YyDetail record);

    List<YyDetail> selectByExample(YyDetailExample example);
		
    YyDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyDetail record, @Param("example") YyDetailExample example);

    int updateByExample(@Param("record") YyDetail record, @Param("example") YyDetailExample example);
		
    int updateByPrimaryKeySelective(YyDetail record);

    int updateByPrimaryKey(YyDetail record);

    List<YyDetail> selectByYyIdAndYyType(@Param("id")Long id, @Param("type")String type);

    void deleteByYyIdAndYyType(@Param("id")Long id, @Param("type")String type);
}