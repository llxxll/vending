package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanOut;
import com.fc.v2.model.auto.PlanOutExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 外协计划 PlanOutMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:02
 */
public interface PlanOutMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanOutExample example);

    int deleteByExample(PlanOutExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanOut record);

    int insertSelective(PlanOut record);

    List<PlanOut> selectByExample(PlanOutExample example);
		
    PlanOut selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanOut record, @Param("example") PlanOutExample example);

    int updateByExample(@Param("record") PlanOut record, @Param("example") PlanOutExample example); 
		
    int updateByPrimaryKeySelective(PlanOut record);

    int updateByPrimaryKey(PlanOut record);

	List<PlanOut> getListByMaterid(Long updater);

	List<String> getListNameByMaterid(@Param("updater")Long id);

	PlanOut selectByNameAndMatID(@Param("name")String departmentTwo, @Param("updater")String material);
  	  	
}