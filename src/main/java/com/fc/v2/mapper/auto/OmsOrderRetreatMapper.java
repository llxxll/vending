package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OmsOrderRetreat;
import com.fc.v2.model.auto.OmsOrderRetreatExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 销售退货表 OmsOrderRetreatMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2022-09-22 22:34:43
 */
public interface OmsOrderRetreatMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OmsOrderRetreatExample example);

    int deleteByExample(OmsOrderRetreatExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OmsOrderRetreat record);

    int insertSelective(OmsOrderRetreat record);

    List<OmsOrderRetreat> selectByExample(OmsOrderRetreatExample example);
		
    OmsOrderRetreat selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OmsOrderRetreat record, @Param("example") OmsOrderRetreatExample example);

    int updateByExample(@Param("record") OmsOrderRetreat record, @Param("example") OmsOrderRetreatExample example); 
		
    int updateByPrimaryKeySelective(OmsOrderRetreat record);

    int updateByPrimaryKey(OmsOrderRetreat record);

	Integer getcountbymingxiID(@Param("buySupplySid")Long sid, @Param("orderStatus")Integer orderStatus);

	List<OmsOrderRetreat> getList(OmsOrderRetreat omsOrderRetreat);

	List<OmsOrderRetreat> getListByorderNO(@Param("orderNo")String orderNo);
  	  	
}