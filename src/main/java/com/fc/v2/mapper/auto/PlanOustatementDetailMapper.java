package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.PlanOustatementDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 委外单结算 PlanOustatementDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:33
 */
public interface PlanOustatementDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanOustatementDetailExample example);

    int deleteByExample(PlanOustatementDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanOustatementDetail record);

    int insertSelective(PlanOustatementDetail record);

    List<PlanOustatementDetail> selectByExample(PlanOustatementDetailExample example);
		
    PlanOustatementDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanOustatementDetail record, @Param("example") PlanOustatementDetailExample example);

    int updateByExample(@Param("record") PlanOustatementDetail record, @Param("example") PlanOustatementDetailExample example); 
		
    int updateByPrimaryKeySelective(PlanOustatementDetail record);

    int updateByPrimaryKey(PlanOustatementDetail record);

	List<PlanOustatementDetail> getListBYwxID(@Param("pgid")Integer intValue);

	PlanOustatementDetail selectOneBywsID(@Param("shid")String string);

	List<PlanOustatementDetail> getList(PlanOustatementDetail planOustatementDetail);

	List<PlanOustatementDetail> getListByIds(@Param("ids")List<Long> stringB);

	Integer getcountBypGid(@Param("pgid")Integer id);

	List<Long> selcet();
  	  	
}