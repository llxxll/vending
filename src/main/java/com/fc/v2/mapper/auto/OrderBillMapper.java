package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OrderBill;
import com.fc.v2.model.auto.OrderBillExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 支付订单流水表 OrderBillMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-08-30 22:11:47
 */
public interface OrderBillMapper {
      	   	      	      	      	      	      	      	      	      
    long countByExample(OrderBillExample example);

    int deleteByExample(OrderBillExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OrderBill record);

    int insertSelective(OrderBill record);

    List<OrderBill> selectByExample(OrderBillExample example);
		
    OrderBill selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OrderBill record, @Param("example") OrderBillExample example);

    int updateByExample(@Param("record") OrderBill record, @Param("example") OrderBillExample example); 
		
    int updateByPrimaryKeySelective(OrderBill record);

    int updateByPrimaryKey(OrderBill record);

    OrderBill getByOutTradeNo(@Param("outTradeNo")String outTradeNo);

    OrderBill selectByOutTradeNo(@Param("outTradeNo") String outTradeNo);
}