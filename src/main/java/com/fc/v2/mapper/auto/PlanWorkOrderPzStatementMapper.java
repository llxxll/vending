package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanWorkOrderPzStatement;
import com.fc.v2.model.auto.PlanWorkOrderPzStatementExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 工单明细结算表(结算明细) PlanWorkOrderPzStatementMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-02-26 16:24:34
 */
public interface PlanWorkOrderPzStatementMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanWorkOrderPzStatementExample example);

    int deleteByExample(PlanWorkOrderPzStatementExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanWorkOrderPzStatement record);

    int insertSelective(PlanWorkOrderPzStatement record);

    List<PlanWorkOrderPzStatement> selectByExample(PlanWorkOrderPzStatementExample example);
		
    PlanWorkOrderPzStatement selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanWorkOrderPzStatement record, @Param("example") PlanWorkOrderPzStatementExample example);

    int updateByExample(@Param("record") PlanWorkOrderPzStatement record, @Param("example") PlanWorkOrderPzStatementExample example); 
		
    int updateByPrimaryKeySelective(PlanWorkOrderPzStatement record);

    int updateByPrimaryKey(PlanWorkOrderPzStatement record);

	PlanWorkOrderPzStatement selectByPgid(@Param("pgId")Integer pgId);

	List<PlanWorkOrderPzStatement> getList(PlanWorkOrderPzStatement planWorkOrderPzStatement);
  	  	
}