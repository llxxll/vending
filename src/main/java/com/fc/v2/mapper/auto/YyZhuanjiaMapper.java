package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyZhuanjia;
import com.fc.v2.model.auto.YyZhuanjiaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院专家 YyZhuanjiaMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:38
 */
public interface YyZhuanjiaMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyZhuanjiaExample example);

    int deleteByExample(YyZhuanjiaExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyZhuanjia record);

    int insertSelective(YyZhuanjia record);

    List<YyZhuanjia> selectByExample(YyZhuanjiaExample example);
		
    YyZhuanjia selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyZhuanjia record, @Param("example") YyZhuanjiaExample example);

    int updateByExample(@Param("record") YyZhuanjia record, @Param("example") YyZhuanjiaExample example); 
		
    int updateByPrimaryKeySelective(YyZhuanjia record);

    int updateByPrimaryKey(YyZhuanjia record);

	List<YyZhuanjia> getList(YyZhuanjia yyZhuanjia);
  	  	
}