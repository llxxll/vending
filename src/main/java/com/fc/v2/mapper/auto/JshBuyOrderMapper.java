package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshBuyOrder;
import com.fc.v2.model.auto.JshBuyOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 采购单 JshBuyOrderMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-26 23:13:09
 */
public interface JshBuyOrderMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshBuyOrderExample example);

    int deleteByExample(JshBuyOrderExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshBuyOrder record);

    int insertSelective(JshBuyOrder record);

    List<JshBuyOrder> selectByExample(JshBuyOrderExample example);
		
    JshBuyOrder selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshBuyOrder record, @Param("example") JshBuyOrderExample example);

    int updateByExample(@Param("record") JshBuyOrder record, @Param("example") JshBuyOrderExample example); 
		
    int updateByPrimaryKeySelective(JshBuyOrder record);

    int updateByPrimaryKey(JshBuyOrder record);
  	  	
}