package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Head;
import com.fc.v2.model.auto.HeadExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 负责人管理表 HeadMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:37:59
 */
public interface HeadMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(HeadExample example);

    int deleteByExample(HeadExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Head record);

    int insertSelective(Head record);

    List<Head> selectByExample(HeadExample example);
		
    Head selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Head record, @Param("example") HeadExample example);

    int updateByExample(@Param("record") Head record, @Param("example") HeadExample example); 
		
    int updateByPrimaryKeySelective(Head record);

    int updateByPrimaryKey(Head record);
  	  	
}