package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Feneun;
import com.fc.v2.model.auto.FeneunExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 分润 FeneunMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:56
 */
public interface FeneunMapper {
      	   	      	      	      	      	      	      	      	      
    long countByExample(FeneunExample example);

    int deleteByExample(FeneunExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Feneun record);

    int insertSelective(Feneun record);

    List<Feneun> selectByExample(FeneunExample example);
		
    Feneun selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Feneun record, @Param("example") FeneunExample example);

    int updateByExample(@Param("record") Feneun record, @Param("example") FeneunExample example); 
		
    int updateByPrimaryKeySelective(Feneun record);

    int updateByPrimaryKey(Feneun record);
  	  	
}