package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Order;
import com.fc.v2.model.auto.OrderBill;
import com.fc.v2.model.auto.OrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 扫码支付订单 OrderMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-08-30 22:03:02
 */
public interface OrderMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OrderExample example);

    int deleteByExample(OrderExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Order record);

    int insertSelective(Order record);

    List<Order> selectByExample(OrderExample example);
		
    Order selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example);

    int updateByExample(@Param("record") Order record, @Param("example") OrderExample example); 
		
    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    Order findByOutTradeNo(@Param("outTradeNo")String outTradeNo);

}