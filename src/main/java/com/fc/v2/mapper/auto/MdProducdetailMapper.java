package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MdProducdetail;
import com.fc.v2.model.auto.MdProducdetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 主数据-商品信息表 MdProducdetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-27 22:56:47
 */
public interface MdProducdetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MdProducdetailExample example);

    int deleteByExample(MdProducdetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MdProducdetail record);

    int insertSelective(MdProducdetail record);

    List<MdProducdetail> selectByExample(MdProducdetailExample example);
		
    MdProducdetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MdProducdetail record, @Param("example") MdProducdetailExample example);

    int updateByExample(@Param("record") MdProducdetail record, @Param("example") MdProducdetailExample example); 
		
    int updateByPrimaryKeySelective(MdProducdetail record);

    int updateByPrimaryKey(MdProducdetail record);

    void deleteByBrandSid(@Param("brandSid") Long brandSid);

    List<MdProducdetail> selectByBrandSid(@Param("brandSid") Long brandSid);

    List<MdProducdetail> getList(MdProducdetail mdProducdetail);
}