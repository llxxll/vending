package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Beihuo;
import com.fc.v2.model.auto.BeihuoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 备货管理 BeihuoMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:48
 */
public interface BeihuoMapper {
      	   	      	      	      	      	      	      	      	      	      
    long countByExample(BeihuoExample example);

    int deleteByExample(BeihuoExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Beihuo record);

    int insertSelective(Beihuo record);

    List<Beihuo> selectByExample(BeihuoExample example);
		
    Beihuo selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Beihuo record, @Param("example") BeihuoExample example);

    int updateByExample(@Param("record") Beihuo record, @Param("example") BeihuoExample example); 
		
    int updateByPrimaryKeySelective(Beihuo record);

    int updateByPrimaryKey(Beihuo record);
  	  	
}