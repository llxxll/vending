package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyJiankangzhishi;
import com.fc.v2.model.auto.YyJiankangzhishiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 健康知识 YyJiankangzhishiMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-02-13 12:16:40
 */
public interface YyJiankangzhishiMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyJiankangzhishiExample example);

    int deleteByExample(YyJiankangzhishiExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyJiankangzhishi record);

    int insertSelective(YyJiankangzhishi record);

    List<YyJiankangzhishi> selectByExample(YyJiankangzhishiExample example);
		
    YyJiankangzhishi selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyJiankangzhishi record, @Param("example") YyJiankangzhishiExample example);

    int updateByExample(@Param("record") YyJiankangzhishi record, @Param("example") YyJiankangzhishiExample example); 
		
    int updateByPrimaryKeySelective(YyJiankangzhishi record);

    int updateByPrimaryKey(YyJiankangzhishi record);

    int updateStatusByPrimaryKey(Long aLong);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);
}