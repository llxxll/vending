package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MyItemDetail;
import com.fc.v2.model.auto.MyItemDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  MyItemDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-08-28 21:49:20
 */
public interface MyItemDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MyItemDetailExample example);

    int deleteByExample(MyItemDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MyItemDetail record);

    int insertSelective(MyItemDetail record);

    List<MyItemDetail> selectByExample(MyItemDetailExample example);
		
    MyItemDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MyItemDetail record, @Param("example") MyItemDetailExample example);

    int updateByExample(@Param("record") MyItemDetail record, @Param("example") MyItemDetailExample example); 
		
    int updateByPrimaryKeySelective(MyItemDetail record);

    int updateByPrimaryKey(MyItemDetail record);

	List<MyItemDetail> getList(MyItemDetail myItemDetail);

	List<MyItemDetail> getList1(MyItemDetail myItemDetail);

    int deleteByItemNo(@Param("itemNo") String itemNo);

    MyItemDetail selectByPrimaryKey2(Long id1);
}