package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshBuyOrderPz;
import com.fc.v2.model.auto.JshBuyOrderPzExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 工单明细扩展表(派工,质检明细) JshBuyOrderPzMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-26 23:13:28
 */
public interface JshBuyOrderPzMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshBuyOrderPzExample example);

    int deleteByExample(JshBuyOrderPzExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshBuyOrderPz record);

    int insertSelective(JshBuyOrderPz record);

    List<JshBuyOrderPz> selectByExample(JshBuyOrderPzExample example);
		
    JshBuyOrderPz selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshBuyOrderPz record, @Param("example") JshBuyOrderPzExample example);

    int updateByExample(@Param("record") JshBuyOrderPz record, @Param("example") JshBuyOrderPzExample example); 
		
    int updateByPrimaryKeySelective(JshBuyOrderPz record);

    int updateByPrimaryKey(JshBuyOrderPz record);
  	  	
}