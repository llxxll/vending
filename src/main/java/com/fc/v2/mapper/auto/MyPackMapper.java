package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MyPack;
import com.fc.v2.model.auto.MyPackExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  MyPackMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-11-06 22:36:31
 */
public interface MyPackMapper {
      	   	      	      	      	      	      
    long countByExample(MyPackExample example);

    int deleteByExample(MyPackExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MyPack record);

    int insertSelective(MyPack record);

    List<MyPack> selectByExample(MyPackExample example);
		
    MyPack selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MyPack record, @Param("example") MyPackExample example);

    int updateByExample(@Param("record") MyPack record, @Param("example") MyPackExample example); 
		
    int updateByPrimaryKeySelective(MyPack record);

    int updateByPrimaryKey(MyPack record);

	MyPack selectByname(@Param("packName")String packName);

	List<MyPack> getList(MyPack myPack);
  	  	
}