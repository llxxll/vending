package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OrderReimbursement;
import com.fc.v2.model.auto.OrderReimbursementExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 报销表 OrderReimbursementMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-09-18 21:11:58
 */
public interface OrderReimbursementMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OrderReimbursementExample example);

    int deleteByExample(OrderReimbursementExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OrderReimbursement record);

    int insertSelective(OrderReimbursement record);

    List<OrderReimbursement> selectByExample(OrderReimbursementExample example);
		
    OrderReimbursement selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OrderReimbursement record, @Param("example") OrderReimbursementExample example);

    int updateByExample(@Param("record") OrderReimbursement record, @Param("example") OrderReimbursementExample example); 
		
    int updateByPrimaryKeySelective(OrderReimbursement record);

    int updateByPrimaryKey(OrderReimbursement record);

    List<OrderReimbursement> getList(OrderReimbursement orderReimbursement);
}