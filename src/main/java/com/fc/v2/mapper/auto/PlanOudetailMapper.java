package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOudetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 生产计划明细 PlanOudetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:11
 */
public interface PlanOudetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanOudetailExample example);

    int deleteByExample(PlanOudetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanOudetail record);

    int insertSelective(PlanOudetail record);

    List<PlanOudetail> selectByExample(PlanOudetailExample example);
		
    PlanOudetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanOudetail record, @Param("example") PlanOudetailExample example);

    int updateByExample(@Param("record") PlanOudetail record, @Param("example") PlanOudetailExample example); 
		
    int updateByPrimaryKeySelective(PlanOudetail record);

    int updateByPrimaryKey(PlanOudetail record);

	String getBYNOandMaterial(@Param("produceNo")String string, @Param("materiaNo")String string2);

	List<PlanOudetail> getList(PlanOudetail planOudetail);

	PlanOudetail getnewone(@Param("materiaNo")String string);

	PlanOudetail getOneBynO(@Param("produceNo")String deliverySid);
  	  	
}