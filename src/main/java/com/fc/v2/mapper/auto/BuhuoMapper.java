package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Buhuo;
import com.fc.v2.model.auto.BuhuoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 补货管理 BuhuoMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:39
 */
public interface BuhuoMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(BuhuoExample example);

    int deleteByExample(BuhuoExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Buhuo record);

    int insertSelective(Buhuo record);

    List<Buhuo> selectByExample(BuhuoExample example);
		
    Buhuo selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Buhuo record, @Param("example") BuhuoExample example);

    int updateByExample(@Param("record") Buhuo record, @Param("example") BuhuoExample example); 
		
    int updateByPrimaryKeySelective(Buhuo record);

    int updateByPrimaryKey(Buhuo record);
  	  	
}