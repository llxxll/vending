package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Machine;
import com.fc.v2.model.auto.MachineExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 机器管理表 MachineMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:39:06
 */
public interface MachineMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MachineExample example);

    int deleteByExample(MachineExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Machine record);

    int insertSelective(Machine record);

    List<Machine> selectByExample(MachineExample example);
		
    Machine selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Machine record, @Param("example") MachineExample example);

    int updateByExample(@Param("record") Machine record, @Param("example") MachineExample example); 
		
    int updateByPrimaryKeySelective(Machine record);

    int updateByPrimaryKey(Machine record);
  	  	
}