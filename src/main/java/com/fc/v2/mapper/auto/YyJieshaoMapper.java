package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyJieshao;
import com.fc.v2.model.auto.YyJieshaoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院介绍 YyJieshaoMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-05-22 19:46:03
 */
public interface YyJieshaoMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyJieshaoExample example);

    int deleteByExample(YyJieshaoExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyJieshao record);

    int insertSelective(YyJieshao record);

    List<YyJieshao> selectByExample(YyJieshaoExample example);
		
    YyJieshao selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyJieshao record, @Param("example") YyJieshaoExample example);

    int updateByExample(@Param("record") YyJieshao record, @Param("example") YyJieshaoExample example); 
		
    int updateByPrimaryKeySelective(YyJieshao record);

    int updateByPrimaryKey(YyJieshao record);
  	  	
}