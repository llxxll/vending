package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyDangjian;
import com.fc.v2.model.auto.YyDangjianExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  YyDangjianMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-03 21:19:10
 */
public interface YyDangjianMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyDangjianExample example);

    int deleteByExample(YyDangjianExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyDangjian record);

    int insertSelective(YyDangjian record);

    List<YyDangjian> selectByExample(YyDangjianExample example);
		
    YyDangjian selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyDangjian record, @Param("example") YyDangjianExample example);

    int updateByExample(@Param("record") YyDangjian record, @Param("example") YyDangjianExample example); 
		
    int updateByPrimaryKeySelective(YyDangjian record);

    int updateByPrimaryKey(YyDangjian record);

    int updateStatusByPrimaryKey(Long id);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);
}