package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyNews;
import com.fc.v2.model.auto.YyNewsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院新闻 YyNewsMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:44
 */
public interface YyNewsMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyNewsExample example);

    int deleteByExample(YyNewsExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyNews record);

    int insertSelective(YyNews record);

    List<YyNews> selectByExample(YyNewsExample example);
		
    YyNews selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyNews record, @Param("example") YyNewsExample example);

    int updateByExample(@Param("record") YyNews record, @Param("example") YyNewsExample example); 
		
    int updateByPrimaryKeySelective(YyNews record);

    int updateByPrimaryKey(YyNews record);

    int updateStatusByPrimaryKey(Long id);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);
}