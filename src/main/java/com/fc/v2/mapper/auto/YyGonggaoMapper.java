package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyGonggao;
import com.fc.v2.model.auto.YyGonggaoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院公告 YyGonggaoMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:27:28
 */
public interface YyGonggaoMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyGonggaoExample example);

    int deleteByExample(YyGonggaoExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyGonggao record);

    int insertSelective(YyGonggao record);

    List<YyGonggao> selectByExample(YyGonggaoExample example);
		
    YyGonggao selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyGonggao record, @Param("example") YyGonggaoExample example);

    int updateByExample(@Param("record") YyGonggao record, @Param("example") YyGonggaoExample example); 
		
    int updateByPrimaryKeySelective(YyGonggao record);

    int updateByPrimaryKey(YyGonggao record);

    int updateStatusByPrimaryKey(Long id);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);
  	  	
}