package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.JshMaterialCarfpriceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 产品工艺加个扩展表 JshMaterialCarfpriceMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:41:13
 */
public interface JshMaterialCarfpriceMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshMaterialCarfpriceExample example);

    int deleteByExample(JshMaterialCarfpriceExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshMaterialCarfprice record);

    int insertSelective(JshMaterialCarfprice record);

    List<JshMaterialCarfprice> selectByExample(JshMaterialCarfpriceExample example);
		
    JshMaterialCarfprice selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshMaterialCarfprice record, @Param("example") JshMaterialCarfpriceExample example);

    int updateByExample(@Param("record") JshMaterialCarfprice record, @Param("example") JshMaterialCarfpriceExample example); 
		
    int updateByPrimaryKeySelective(JshMaterialCarfprice record);

    int updateByPrimaryKey(JshMaterialCarfprice record);
    
	List<JshMaterialCarfprice> getListByMaterid(@Param("materialId")Long materiaNo);

    JshMaterialCarfprice selectByMaterialId(@Param("matterNo") String matterNo);

	List<JshMaterialCarfprice> getList(JshMaterialCarfprice jshMaterialCarfprice);

	List<String> getListNameByMaterid(@Param("materialId")Long id);

	JshMaterialCarfprice selectByNameAndMatID(@Param("ttxx")String departmentOne, @Param("materialId")String material);
}