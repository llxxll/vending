package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyLiuyan;
import com.fc.v2.model.auto.YyLiuyanExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院联系我们 YyLiuyanMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:30:03
 */
public interface YyLiuyanMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyLiuyanExample example);

    int deleteByExample(YyLiuyanExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyLiuyan record);

    int insertSelective(YyLiuyan record);

    List<YyLiuyan> selectByExample(YyLiuyanExample example);
		
    YyLiuyan selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyLiuyan record, @Param("example") YyLiuyanExample example);

    int updateByExample(@Param("record") YyLiuyan record, @Param("example") YyLiuyanExample example); 
		
    int updateByPrimaryKeySelective(YyLiuyan record);

    int updateByPrimaryKey(YyLiuyan record);
  	  	
}