package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyJiuyizhinan;
import com.fc.v2.model.auto.YyJiuyizhinanExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 就医指南 YyJiuyizhinanMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-02-13 12:17:41
 */
public interface YyJiuyizhinanMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyJiuyizhinanExample example);

    int deleteByExample(YyJiuyizhinanExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyJiuyizhinan record);

    int insertSelective(YyJiuyizhinan record);

    List<YyJiuyizhinan> selectByExample(YyJiuyizhinanExample example);
		
    YyJiuyizhinan selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyJiuyizhinan record, @Param("example") YyJiuyizhinanExample example);

    int updateByExample(@Param("record") YyJiuyizhinan record, @Param("example") YyJiuyizhinanExample example); 
		
    int updateByPrimaryKeySelective(YyJiuyizhinan record);

    int updateByPrimaryKey(YyJiuyizhinan record);

    int updateStatusByPrimaryKey(Long aLong);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);
}