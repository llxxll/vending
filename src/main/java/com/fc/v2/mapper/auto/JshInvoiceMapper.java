package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshInvoice;
import com.fc.v2.model.auto.JshInvoiceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 财务发票对账单 JshInvoiceMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-31 14:47:05
 */
public interface JshInvoiceMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshInvoiceExample example);

    int deleteByExample(JshInvoiceExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshInvoice record);

    int insertSelective(JshInvoice record);

    List<JshInvoice> selectByExample(JshInvoiceExample example);
		
    JshInvoice selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshInvoice record, @Param("example") JshInvoiceExample example);

    int updateByExample(@Param("record") JshInvoice record, @Param("example") JshInvoiceExample example); 
		
    int updateByPrimaryKeySelective(JshInvoice record);

    int updateByPrimaryKey(JshInvoice record);

	JshInvoice getOneNew(@Param("materialNo")String materialNo, @Param("orgName")String orgName);

	List<JshInvoice> getList(JshInvoice jshInvoice);
  	  	
}