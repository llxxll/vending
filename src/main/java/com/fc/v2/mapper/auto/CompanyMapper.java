package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Company;
import com.fc.v2.model.auto.CompanyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 公司管理表 CompanyMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:31:41
 */
public interface CompanyMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(CompanyExample example);

    int deleteByExample(CompanyExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Company record);

    int insertSelective(Company record);

    List<Company> selectByExample(CompanyExample example);
		
    Company selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Company record, @Param("example") CompanyExample example);

    int updateByExample(@Param("record") Company record, @Param("example") CompanyExample example); 
		
    int updateByPrimaryKeySelective(Company record);

    int updateByPrimaryKey(Company record);
  	  	
}