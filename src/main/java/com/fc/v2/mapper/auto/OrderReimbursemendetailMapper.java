package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OrderReimbursemendetail;
import com.fc.v2.model.auto.OrderReimbursemendetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 报销明细表 OrderReimbursemendetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-09-18 21:31:37
 */
public interface OrderReimbursemendetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OrderReimbursemendetailExample example);

    int deleteByExample(OrderReimbursemendetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OrderReimbursemendetail record);

    int insertSelective(OrderReimbursemendetail record);

    List<OrderReimbursemendetail> selectByExample(OrderReimbursemendetailExample example);
		
    OrderReimbursemendetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OrderReimbursemendetail record, @Param("example") OrderReimbursemendetailExample example);

    int updateByExample(@Param("record") OrderReimbursemendetail record, @Param("example") OrderReimbursemendetailExample example); 
		
    int updateByPrimaryKeySelective(OrderReimbursemendetail record);

    int updateByPrimaryKey(OrderReimbursemendetail record);

    void deleteByOrderNo(@Param("orderNo")String orderNo);
}