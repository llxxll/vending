package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.CompanyReview;
import com.fc.v2.model.auto.CompanyReviewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 公司审核 CompanyReviewMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2024-01-23 22:03:22
 */
public interface CompanyReviewMapper {
      	   	      	      	      	      	      	      	      
    long countByExample(CompanyReviewExample example);

    int deleteByExample(CompanyReviewExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(CompanyReview record);

    int insertSelective(CompanyReview record);

    List<CompanyReview> selectByExample(CompanyReviewExample example);
		
    CompanyReview selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") CompanyReview record, @Param("example") CompanyReviewExample example);

    int updateByExample(@Param("record") CompanyReview record, @Param("example") CompanyReviewExample example); 
		
    int updateByPrimaryKeySelective(CompanyReview record);

    int updateByPrimaryKey(CompanyReview record);
  	  	
}