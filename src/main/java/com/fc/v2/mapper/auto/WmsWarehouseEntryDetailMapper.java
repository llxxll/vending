package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 入库单明细 WmsWarehouseEntryDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:28:17
 */
public interface WmsWarehouseEntryDetailMapper {
      	      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(WmsWarehouseEntryDetailExample example);

    int deleteByExample(WmsWarehouseEntryDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(WmsWarehouseEntryDetail record);

    int insertSelective(WmsWarehouseEntryDetail record);

    List<WmsWarehouseEntryDetail> selectByExample(WmsWarehouseEntryDetailExample example);
		
    WmsWarehouseEntryDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") WmsWarehouseEntryDetail record, @Param("example") WmsWarehouseEntryDetailExample example);

    int updateByExample(@Param("record") WmsWarehouseEntryDetail record, @Param("example") WmsWarehouseEntryDetailExample example); 
		
    int updateByPrimaryKeySelective(WmsWarehouseEntryDetail record);

    int updateByPrimaryKey(WmsWarehouseEntryDetail record);

	Integer getCountByStatus(@Param("status")Integer status);

	List<WmsWarehouseEntryDetail> getList(WmsWarehouseEntryDetail wmsWarehouseEntryDetail);

	//精确
	List<WmsWarehouseEntryDetail> getListByparams(WmsWarehouseEntryDetail wmsWarehouseEntryDetail);

	//根据派工id 获取入库数据
	WmsWarehouseEntryDetail getBypGid(@Param("unitName")String pgid,@Param("qualityNo")String quno);

	Integer getCountbymattersc(@Param("matterDesc")String string, @Param("qualityNo")String string2,@Param("x")Integer x);
	Integer getBypGidxx(@Param("unitName")String string, @Param("qualityNo")String string2,@Param("x")Integer x);

	Integer getcountBydevlied(@Param("deliverySid")String string, @Param("x")Integer i,  @Param("qualityNo")String string2);

	List<WmsWarehouseEntryDetail> chejianlist(WmsWarehouseEntryDetail wmsWarehouseEntryDetail);

	List<WmsWarehouseEntryDetail> listwwaixie(WmsWarehouseEntryDetail wmsWarehouseEntryDetail);


  	  	
}