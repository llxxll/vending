package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyYiyuangaikuang;
import com.fc.v2.model.auto.YyYiyuangaikuangExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院概况 YyYiyuangaikuangMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-02-13 12:17:30
 */
public interface YyYiyuangaikuangMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyYiyuangaikuangExample example);

    int deleteByExample(YyYiyuangaikuangExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyYiyuangaikuang record);

    int insertSelective(YyYiyuangaikuang record);

    List<YyYiyuangaikuang> selectByExample(YyYiyuangaikuangExample example);
		
    YyYiyuangaikuang selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyYiyuangaikuang record, @Param("example") YyYiyuangaikuangExample example);

    int updateByExample(@Param("record") YyYiyuangaikuang record, @Param("example") YyYiyuangaikuangExample example); 
		
    int updateByPrimaryKeySelective(YyYiyuangaikuang record);

    int updateByPrimaryKey(YyYiyuangaikuang record);

    int updateStatusByPrimaryKey(Long aLong);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);

    YyYiyuangaikuang selectOne();
}