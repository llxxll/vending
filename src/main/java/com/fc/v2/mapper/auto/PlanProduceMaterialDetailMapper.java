package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.PlanProduceMaterialDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 生产计划明细 PlanProduceMaterialDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 20:11:36
 */
public interface PlanProduceMaterialDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanProduceMaterialDetailExample example);

    int deleteByExample(PlanProduceMaterialDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanProduceMaterialDetail record);

    int insertSelective(PlanProduceMaterialDetail record);

    List<PlanProduceMaterialDetail> selectByExample(PlanProduceMaterialDetailExample example);
		
    PlanProduceMaterialDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanProduceMaterialDetail record, @Param("example") PlanProduceMaterialDetailExample example);

    int updateByExample(@Param("record") PlanProduceMaterialDetail record, @Param("example") PlanProduceMaterialDetailExample example); 
		
    int updateByPrimaryKeySelective(PlanProduceMaterialDetail record);

    int updateByPrimaryKey(PlanProduceMaterialDetail record);

    Integer getCountByStatus(@Param(value="status") Integer status);

	List<PlanProduceMaterialDetail> getList(PlanProduceMaterialDetail planProduceMaterialDetail);

	Long getBYNOandMaterial(@Param(value="no")String no,@Param(value="materialNo") String materialNo);

	PlanProduceMaterialDetail getnewone(@Param(value="materialNo")String string);

}