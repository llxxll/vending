package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OmsOrder;
import com.fc.v2.model.auto.OmsOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 销售-订单表 OmsOrderMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-05-21 23:28:35
 */
public interface OmsOrderMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OmsOrderExample example);

    int deleteByExample(OmsOrderExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OmsOrder record);

    int insertSelective(OmsOrder record);

    List<OmsOrder> selectByExample(OmsOrderExample example);
		
    OmsOrder selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OmsOrder record, @Param("example") OmsOrderExample example);

    int updateByExample(@Param("record") OmsOrder record, @Param("example") OmsOrderExample example); 
		
    int updateByPrimaryKeySelective(OmsOrder record);

    int updateByPrimaryKey(OmsOrder record);

    void updateOmsOrderByStatus(@Param("sid") String sid, @Param("status") String status);

	List<OmsOrder> getList(OmsOrder omsOrder);

    List<OmsOrder> getListFH(OmsOrder omsOrder);
    List<OmsOrder> listshengcan(OmsOrder omsOrder);

	OmsOrder selectByorderNo(@Param("orderNo")String orderNo);

	int updateByPrimaryKeySelectivebyNull(OmsOrder selectByPrimaryKey);
}