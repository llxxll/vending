package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetailExample;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 出库单明细 WmsWarehouseDeliveryDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:28:43
 */
public interface WmsWarehouseDeliveryDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(WmsWarehouseDeliveryDetailExample example);

    int deleteByExample(WmsWarehouseDeliveryDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(WmsWarehouseDeliveryDetail record);

    int insertSelective(WmsWarehouseDeliveryDetail record);

    List<WmsWarehouseDeliveryDetail> selectByExample(WmsWarehouseDeliveryDetailExample example);
		
    WmsWarehouseDeliveryDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") WmsWarehouseDeliveryDetail record, @Param("example") WmsWarehouseDeliveryDetailExample example);

    int updateByExample(@Param("record") WmsWarehouseDeliveryDetail record, @Param("example") WmsWarehouseDeliveryDetailExample example); 
		
    int updateByPrimaryKeySelective(WmsWarehouseDeliveryDetail record);

    int updateByPrimaryKey(WmsWarehouseDeliveryDetail record);

	WmsWarehouseDeliveryDetail getOnebyParams(WmsWarehouseDeliveryDetail wms);

    Integer getCountByStatus(@Param("status") Integer status);

    List<WmsWarehouseDeliveryDetail> getList(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail);

    Integer  getCountbyUnitName(@Param("unitName")String unitName,@Param("qualityNo")String qualityNo);

	Integer getCountbydevlied(@Param("deliverySid")String unitName,@Param("qualityNo")String qualityNo);
	Integer getCountbydevliedByOrderType(@Param("deliverySid")String unitName,@Param("qualityNo")String qualityNo,@Param("type")String type);

	WmsWarehouseDeliveryDetail getOnebyUnitName(@Param("unitName")String string,@Param("qualityNo")String qualityNo);

	List<WmsWarehouseDeliveryDetail> getListlistchejian(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail);

	List<WmsWarehouseDeliveryDetail> getListlistweiwai(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail);

}