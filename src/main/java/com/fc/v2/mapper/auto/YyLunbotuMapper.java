package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyLunbotu;
import com.fc.v2.model.auto.YyLunbotuExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 医院轮播图 YyLunbotuMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:52
 */
public interface YyLunbotuMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyLunbotuExample example);

    int deleteByExample(YyLunbotuExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyLunbotu record);

    int insertSelective(YyLunbotu record);

    List<YyLunbotu> selectByExample(YyLunbotuExample example);
		
    YyLunbotu selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyLunbotu record, @Param("example") YyLunbotuExample example);

    int updateByExample(@Param("record") YyLunbotu record, @Param("example") YyLunbotuExample example); 
		
    int updateByPrimaryKeySelective(YyLunbotu record);

    int updateByPrimaryKey(YyLunbotu record);

    List<YyLunbotu> getList(@Param("type")String type);
}