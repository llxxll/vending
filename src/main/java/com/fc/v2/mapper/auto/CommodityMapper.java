package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Commodity;
import com.fc.v2.model.auto.CommodityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 商品管理表 CommodityMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:09:32
 */
public interface CommodityMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(CommodityExample example);

    int deleteByExample(CommodityExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Commodity record);

    int insertSelective(Commodity record);

    List<Commodity> selectByExample(CommodityExample example);
		
    Commodity selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Commodity record, @Param("example") CommodityExample example);

    int updateByExample(@Param("record") Commodity record, @Param("example") CommodityExample example); 
		
    int updateByPrimaryKeySelective(Commodity record);

    int updateByPrimaryKey(Commodity record);

    Commodity selectBygoodsId(@Param("goodsId")Long goodsId);
    List<Commodity> selectByExample2(@Param("record") Commodity record);

    Commodity selectByOutGoodId(@Param("outGoodId")String outGoodId);

    int getTotle();
}