package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.auto.MdProduccategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 主数据-商品信息表 类别 MdProduccategoryMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-23 22:57:59
 */
public interface MdProduccategoryMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MdProduccategoryExample example);

    int deleteByExample(MdProduccategoryExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MdProduccategory record);

    int insertSelective(MdProduccategory record);

    List<MdProduccategory> selectByExample(MdProduccategoryExample example);
		
    MdProduccategory selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MdProduccategory record, @Param("example") MdProduccategoryExample example);

    int updateByExample(@Param("record") MdProduccategory record, @Param("example") MdProduccategoryExample example); 
		
    int updateByPrimaryKeySelective(MdProduccategory record);

    int updateByPrimaryKey(MdProduccategory record);

	List<MdProduccategory> getList(MdProduccategory mdProduccategory);

	MdProduccategory selectByName(@Param("productTitle")String productTitle);

    List<MdProduccategory> getByBrandSid(@Param("brandSid")String brandSid);
}