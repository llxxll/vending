package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.model.auto.YyNewsTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 发布新闻类类型 YyNewsTypeMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-03-07 22:05:52
 */
public interface YyNewsTypeMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyNewsTypeExample example);

    int deleteByExample(YyNewsTypeExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyNewsType record);

    int insertSelective(YyNewsType record);

    List<YyNewsType> selectByExample(YyNewsTypeExample example);
		
    YyNewsType selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyNewsType record, @Param("example") YyNewsTypeExample example);

    int updateByExample(@Param("record") YyNewsType record, @Param("example") YyNewsTypeExample example); 
		
    int updateByPrimaryKeySelective(YyNewsType record);

    int updateByPrimaryKey(YyNewsType record);

    List<YyNewsType> selectByfw(@Param("fw") String fw);
}