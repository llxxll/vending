package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.model.auto.PlanWorkOrderDetailPz;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.TsysUserExample;
import com.fc.v2.model.auto.User;
import com.fc.v2.model.auto.WmsSonghuo;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TsysUserMapper {
    long countByExample(TsysUserExample example);

    int deleteByExample(TsysUserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TsysUser record);

    int insertSelective(TsysUser record);

    List<TsysUser> selectByExample(TsysUserExample example);

    TsysUser selectByPrimaryKey(Long id);
    TsysUser selectByname(@Param("nickname")String name);
    int updateByExampleSelective(@Param("record") TsysUser record, @Param("example") TsysUserExample example);

    int updateByExample(@Param("record") TsysUser record, @Param("example") TsysUserExample example);

    int updateByPrimaryKeySelective(TsysUser record);

    int updateByPrimaryKey(TsysUser record);

	List<TsysUser> selectListByDept(@Param("deptId")Integer deptId);

	///
	List<User> selectusList();

	String secletid(Long xid);

	List<PlanProduceMaterialDetail> selecpppsList();

	List<JshMaterial> selecppssspsList();

	List<PlanWorkOrder> selecpppxxsList();

	List<PlanWorkOrderDetailPz> selecpppxxxsList();

	List<WmsWarehouseDeliveryDetail> selecppxpxxxsList();

	List<WmsWarehouseEntryDetail> selecpsppxxxsList();

	List<PlanOudetail> selecpppsxxxxxxList();

	List<PlanOudetailGongying> selecpppsxggggList();

	List<WmsSonghuo> selecpppsxssggggList();

	List<PlanOustatementDetail> selecpppssssxssggggList();

	List<WmsWarehouseEntryDetail> selecpsppxxxstttList();

    TsysUser selectByPhone(@Param("phone")String phone);
	TsysUser selectByOpenId(@Param("openId")String openId);

    String selectByDepId(@Param("depId") Integer depId);
}