package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanOudetailGongyingExample;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 委外单 PlanOudetailGongyingMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:15
 */
public interface PlanOudetailGongyingMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanOudetailGongyingExample example);

    int deleteByExample(PlanOudetailGongyingExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanOudetailGongying record);

    int insertSelective(PlanOudetailGongying record);

    List<PlanOudetailGongying> selectByExample(PlanOudetailGongyingExample example);
		
    PlanOudetailGongying selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanOudetailGongying record, @Param("example") PlanOudetailGongyingExample example);

    int updateByExample(@Param("record") PlanOudetailGongying record, @Param("example") PlanOudetailGongyingExample example); 
		
    int updateByPrimaryKeySelective(PlanOudetailGongying record);

    int updateByPrimaryKey(PlanOudetailGongying record);

	Integer getCountbydevlied(@Param("produceNo")String string);

	List<PlanOudetailGongying> getList(PlanOudetailGongying planOudetailGongying);

	List<PlanOudetailGongying> getListxxx();

	Integer getzaitucount(@Param("materiaNo")String materiaNo,@Param("departmentOne") String departmentOne);

	Integer getzaitulingcountquliao(@Param("materiaNo")String materiaNo,@Param("departmentOne") String departmentOne);
	Integer getzaiturucountquliao(@Param("materiaNo")String materiaNo,@Param("departmentOne") String departmentOne);

	Integer getzaitucountzzz(@Param("materiaNo")String materiaNo,@Param("departmentOne") String departmentOne);
  	  	
}