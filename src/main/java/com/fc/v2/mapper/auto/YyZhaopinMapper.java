package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.YyZhaopin;
import com.fc.v2.model.auto.YyZhaopinExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 招聘信息 YyZhaopinMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-10 16:01:46
 */
public interface YyZhaopinMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(YyZhaopinExample example);

    int deleteByExample(YyZhaopinExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(YyZhaopin record);

    int insertSelective(YyZhaopin record);

    List<YyZhaopin> selectByExample(YyZhaopinExample example);
		
    YyZhaopin selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") YyZhaopin record, @Param("example") YyZhaopinExample example);

    int updateByExample(@Param("record") YyZhaopin record, @Param("example") YyZhaopinExample example); 
		
    int updateByPrimaryKeySelective(YyZhaopin record);

    int updateByPrimaryKey(YyZhaopin record);

    int updateStatusByPrimaryKey(Long id);

    int shangjiaByPrimaryKey(Long aLong);

    int xiajiaByPrimaryKey(Long aLong);
}