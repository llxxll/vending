package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshDepotExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 仓库表 JshDepotMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 23:06:31
 */
public interface JshDepotMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshDepotExample example);

    int deleteByExample(JshDepotExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshDepot record);

    int insertSelective(JshDepot record);

    List<JshDepot> selectByExample(JshDepotExample example);
		
    JshDepot selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshDepot record, @Param("example") JshDepotExample example);

    int updateByExample(@Param("record") JshDepot record, @Param("example") JshDepotExample example); 
		
    int updateByPrimaryKeySelective(JshDepot record);

    int updateByPrimaryKey(JshDepot record);

	List<JshDepot> findAll();

	JshDepot getByName(@Param("name")String houseName);
  	  	
}