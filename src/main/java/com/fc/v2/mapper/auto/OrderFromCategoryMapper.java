package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OrderFromCategory;
import com.fc.v2.model.auto.OrderFromCategoryExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 主数据-商品信息表 OrderFromCategoryMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-23 22:58:10
 */
public interface OrderFromCategoryMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OrderFromCategoryExample example);

    int deleteByExample(OrderFromCategoryExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OrderFromCategory record);

    int insertSelective(OrderFromCategory record);

    List<OrderFromCategory> selectByExample(OrderFromCategoryExample example);
		
    OrderFromCategory selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OrderFromCategory record, @Param("example") OrderFromCategoryExample example);

    int updateByExample(@Param("record") OrderFromCategory record, @Param("example") OrderFromCategoryExample example); 
		
    int updateByPrimaryKeySelective(OrderFromCategory record);

    int updateByPrimaryKey(OrderFromCategory record);

	List<OrderFromCategory> getList(OrderFromCategory orderFromCategory);

    List<Map<String, Object>> selectorderFromCategorylist();

    List<OrderFromCategory> findAll();

	OrderFromCategory selectByName(@Param("productTitle")String productTitle);
}