package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MdProducspecs;
import com.fc.v2.model.auto.MdProducspecsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 主数据-商品信息表 MdProducspecsMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-23 22:58:28
 */
public interface MdProducspecsMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MdProducspecsExample example);

    int deleteByExample(MdProducspecsExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MdProducspecs record);

    int insertSelective(MdProducspecs record);

    List<MdProducspecs> selectByExample(MdProducspecsExample example);
		
    MdProducspecs selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MdProducspecs record, @Param("example") MdProducspecsExample example);

    int updateByExample(@Param("record") MdProducspecs record, @Param("example") MdProducspecsExample example); 
		
    int updateByPrimaryKeySelective(MdProducspecs record);

    int updateByPrimaryKey(MdProducspecs record);

	List<MdProducspecs> getList(MdProducspecs mdProducspecs);

	MdProducspecs selectByName(@Param("productTitle")String productTitle,@Param("brandSid")Long brandSid);

    List<MdProducspecs> getByBrandSid(@Param("brandSid")String brandSid);
}