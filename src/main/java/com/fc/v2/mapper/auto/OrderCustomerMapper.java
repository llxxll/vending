package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OrderCustomer;
import com.fc.v2.model.auto.OrderCustomerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 客户表 OrderCustomerMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-09-15 20:45:08
 */
public interface OrderCustomerMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OrderCustomerExample example);

    int deleteByExample(OrderCustomerExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OrderCustomer record);

    int insertSelective(OrderCustomer record);

    List<OrderCustomer> selectByExample(OrderCustomerExample example);
		
    OrderCustomer selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OrderCustomer record, @Param("example") OrderCustomerExample example);

    int updateByExample(@Param("record") OrderCustomer record, @Param("example") OrderCustomerExample example); 
		
    int updateByPrimaryKeySelective(OrderCustomer record);

    int updateByPrimaryKey(OrderCustomer record);

	List<OrderCustomer> getList(OrderCustomer orderCustomer);

	OrderCustomer getbyPhone(@Param("customerPhone")String customerPhone);
  	  	
}