package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 产品表 JshMaterialMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:40:36
 */
public interface JshMaterialMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(JshMaterialExample example);

    int deleteByExample(JshMaterialExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(JshMaterial record);

    int insertSelective(JshMaterial record);

    List<JshMaterial> selectByExample(JshMaterialExample example);
    //材料 辅料 名称+规格 唯一
    JshMaterial selectByParams(JshMaterial example);
		
    JshMaterial selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") JshMaterial record, @Param("example") JshMaterialExample example);

    int updateByExample(@Param("record") JshMaterial record, @Param("example") JshMaterialExample example); 
		
    int updateByPrimaryKeySelective(JshMaterial record);

    int updateByPrimaryKey(JshMaterial record);
//物料 物料名称 唯一
	JshMaterial selectByName(@Param("name")String name);
	//材料
	JshMaterial selectByNameandmodel(@Param("name")String name,@Param("model")String model);

	List<JshMaterial> getList(JshMaterial jshMaterial);
  	  	
}