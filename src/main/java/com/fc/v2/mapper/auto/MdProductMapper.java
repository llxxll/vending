package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MdProduct;
import com.fc.v2.model.auto.MdProductExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 主数据-商品信息表 MdProductMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-05-21 23:28:15
 */
public interface MdProductMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MdProductExample example);

    int deleteByExample(MdProductExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MdProduct record);

    int insertSelective(MdProduct record);

    List<MdProduct> selectByExample(MdProductExample example);
		
    MdProduct selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MdProduct record, @Param("example") MdProductExample example);

    int updateByExample(@Param("record") MdProduct record, @Param("example") MdProductExample example); 
		
    int updateByPrimaryKeySelective(MdProduct record);

    int updateByPrimaryKey(MdProduct record);

	List<MdProduct> getList(MdProduct mdProduct);

    List<MdProduct> selectByExampleByTitleAnd(MdProductExample testExample);

	MdProduct selectByParams(@Param("productTitle")String productTitle);
}