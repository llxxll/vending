package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.OmsOrderDetail;
import com.fc.v2.model.auto.OmsOrderDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 销售-订单明细 OmsOrderDetailMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-05-21 23:28:42
 */
public interface OmsOrderDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(OmsOrderDetailExample example);

    int deleteByExample(OmsOrderDetailExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(OmsOrderDetail record);

    int insertSelective(OmsOrderDetail record);

    List<OmsOrderDetail> selectByExample(OmsOrderDetailExample example);
		
    OmsOrderDetail selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") OmsOrderDetail record, @Param("example") OmsOrderDetailExample example);

    int updateByExample(@Param("record") OmsOrderDetail record, @Param("example") OmsOrderDetailExample example); 
		
    int updateByPrimaryKeySelective(OmsOrderDetail record);

    int updateByPrimaryKey(OmsOrderDetail record);

    List<OmsOrderDetail> selectByorderNo(@Param("orderNo") String orderNo);

	List<OmsOrderDetail> getList(OmsOrderDetail omsOrderDetail);

    void deleteByOrderNo(@Param("orderNo")String orderNo);
}