package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MyItem;
import com.fc.v2.model.auto.MyItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *  MyItemMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-08-28 21:49:04
 */
public interface MyItemMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(MyItemExample example);

    int deleteByExample(MyItemExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MyItem record);

    int insertSelective(MyItem record);

    List<MyItem> selectByExample(MyItemExample example);
		
    MyItem selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MyItem record, @Param("example") MyItemExample example);

    int updateByExample(@Param("record") MyItem record, @Param("example") MyItemExample example); 
		
    int updateByPrimaryKeySelective(MyItem record);

    int updateByPrimaryKey(MyItem record);

	List<MyItem> getList(MyItem myItem);
  	  	
}