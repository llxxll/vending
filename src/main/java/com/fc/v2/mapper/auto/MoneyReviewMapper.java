package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.MoneyReview;
import com.fc.v2.model.auto.MoneyReviewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 提现审核 MoneyReviewMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:52
 */
public interface MoneyReviewMapper {
      	   	      	      	      	      	      	      	      	      
    long countByExample(MoneyReviewExample example);

    int deleteByExample(MoneyReviewExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(MoneyReview record);

    int insertSelective(MoneyReview record);

    List<MoneyReview> selectByExample(MoneyReviewExample example);
		
    MoneyReview selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") MoneyReview record, @Param("example") MoneyReviewExample example);

    int updateByExample(@Param("record") MoneyReview record, @Param("example") MoneyReviewExample example); 
		
    int updateByPrimaryKeySelective(MoneyReview record);

    int updateByPrimaryKey(MoneyReview record);
  	  	
}