package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.PlanWorkOrderDetailPz;
import com.fc.v2.model.auto.PlanWorkOrderDetailPzExample;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 工单明细 PlanWorkOrderDetailPzMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:39:43
 */
public interface PlanWorkOrderDetailPzMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(PlanWorkOrderDetailPzExample example);

    int deleteByExample(PlanWorkOrderDetailPzExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(PlanWorkOrderDetailPz record);

    int insertSelective(PlanWorkOrderDetailPz record);

    List<PlanWorkOrderDetailPz> selectByExample(PlanWorkOrderDetailPzExample example);
		
    PlanWorkOrderDetailPz selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") PlanWorkOrderDetailPz record, @Param("example") PlanWorkOrderDetailPzExample example);

    int updateByExample(@Param("record") PlanWorkOrderDetailPz record, @Param("example") PlanWorkOrderDetailPzExample example); 
		
    int updateByPrimaryKeySelective(PlanWorkOrderDetailPz record);

    int updateByPrimaryKey(PlanWorkOrderDetailPz record);

	List<PlanWorkOrderDetailPz> getList(PlanWorkOrderDetailPz planWorkOrderDetailPz);

	Integer getCountByStatus(@Param("status")Integer status);

    PlanWorkOrderDetailPz selectOneByworno(@Param("workerOrderNo") String workerOrderNo);

	List<PlanWorkOrderDetailPz> getListBYParams(PlanWorkOrderDetailPz planWorkOrderDetailPz);

	List<PlanWorkOrderDetailPz> getListBYworkno(String workorderNo);

	List<PlanWorkOrderDetailPz>  selectBystockcount(Integer stockCount);

	List<PlanWorkOrderDetailPz> getListByCarftidandWOno(@Param("carftId")String carftId, @Param("workerOrderNo")String workerOrderNo);

	List<Map<String, BigDecimal>> getCountGroupBycarfid(@Param("workerOrderNo")String workerOrderNo);
}