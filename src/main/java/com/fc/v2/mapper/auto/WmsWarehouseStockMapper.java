package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.WmsWarehouseStock;
import com.fc.v2.model.auto.WmsWarehouseStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 主数据-库存 WmsWarehouseStockMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:38:35
 */
public interface WmsWarehouseStockMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(WmsWarehouseStockExample example);

    int deleteByExample(WmsWarehouseStockExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(WmsWarehouseStock record);

    int insertSelective(WmsWarehouseStock record);

    List<WmsWarehouseStock> selectByExample(WmsWarehouseStockExample example);
		
    WmsWarehouseStock selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") WmsWarehouseStock record, @Param("example") WmsWarehouseStockExample example);

    int updateByExample(@Param("record") WmsWarehouseStock record, @Param("example") WmsWarehouseStockExample example); 
		
    int updateByPrimaryKeySelective(WmsWarehouseStock record);

    int updateByPrimaryKey(WmsWarehouseStock record);

	List<WmsWarehouseStock> getList(WmsWarehouseStock wmsWarehouseStock);

	WmsWarehouseStock getOneByParams(WmsWarehouseStock wmsWarehouseStock);
  	  	
}