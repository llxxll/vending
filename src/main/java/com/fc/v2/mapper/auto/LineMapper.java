package com.fc.v2.mapper.auto;

import com.fc.v2.model.auto.Line;
import com.fc.v2.model.auto.LineExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 线路管理表 LineMapper
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:35:21
 */
public interface LineMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(LineExample example);

    int deleteByExample(LineExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(Line record);

    int insertSelective(Line record);

    List<Line> selectByExample(LineExample example);
		
    Line selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") Line record, @Param("example") LineExample example);

    int updateByExample(@Param("record") Line record, @Param("example") LineExample example); 
		
    int updateByPrimaryKeySelective(Line record);

    int updateByPrimaryKey(Line record);
  	  	
}