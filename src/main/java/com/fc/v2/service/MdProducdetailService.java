package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MdProducdetailMapper;
import com.fc.v2.model.auto.MdProducdetail;
import com.fc.v2.model.auto.MdProducdetailExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 主数据-商品信息表 MdProducdetailService
 * @Title: MdProducdetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-27 22:56:47  
 **/
@Service
public class MdProducdetailService implements BaseService<MdProducdetail, MdProducdetailExample>{
	@Autowired
	private MdProducdetailMapper mdProducdetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MdProducdetail> list(Tablepar tablepar,MdProducdetail mdProducdetail){

	       // PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MdProducdetail> list= mdProducdetailMapper.getList(mdProducdetail);
	        PageInfo<MdProducdetail> pageInfo = new PageInfo<MdProducdetail>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MdProducdetailExample example=new MdProducdetailExample();
			example.createCriteria().andSidIn(stringB);
			return mdProducdetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MdProducdetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return mdProducdetailMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MdProducdetail record) {
		return mdProducdetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MdProducdetail record) {
				
		record.setSid(null);
		
				
		return mdProducdetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MdProducdetail record, MdProducdetailExample example) {
		
		return mdProducdetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MdProducdetail record, MdProducdetailExample example) {
		
		return mdProducdetailMapper.updateByExample(record, example);
	}

	@Override
	public List<MdProducdetail> selectByExample(MdProducdetailExample example) {
		
		return mdProducdetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MdProducdetailExample example) {
		
		return mdProducdetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MdProducdetailExample example) {
		
		return mdProducdetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param mdProducdetail
	 * @return
	 */
	public int updateVisible(MdProducdetail mdProducdetail) {
		return mdProducdetailMapper.updateByPrimaryKeySelective(mdProducdetail);
	}


	public List<MdProducdetail> getByBrandSid(String brandSid) {
		return  mdProducdetailMapper.selectByBrandSid(Long.valueOf(brandSid));
	}
}
