package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OmsOrderDetailMapper;
import com.fc.v2.mapper.auto.OmsOrderRetreatMapper;
import com.fc.v2.model.auto.OmsOrderDetail;
import com.fc.v2.model.auto.OmsOrderRetreat;
import com.fc.v2.model.auto.OmsOrderRetreatExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 销售退货表 OmsOrderRetreatService
 * @Title: OmsOrderRetreatService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2022-09-22 22:34:43  
 **/
@Service
public class OmsOrderRetreatService implements BaseService<OmsOrderRetreat, OmsOrderRetreatExample>{
	@Autowired
	private OmsOrderRetreatMapper omsOrderRetreatMapper;
	@Autowired
	private OmsOrderDetailMapper omsOrderDetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OmsOrderRetreat> list(Tablepar tablepar,OmsOrderRetreat omsOrderRetreat){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OmsOrderRetreat> list= omsOrderRetreatMapper.getList(omsOrderRetreat);
	        PageInfo<OmsOrderRetreat> pageInfo = new PageInfo<OmsOrderRetreat>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OmsOrderRetreatExample example=new OmsOrderRetreatExample();
			example.createCriteria().andSidIn(stringB);
			return omsOrderRetreatMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OmsOrderRetreat selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			OmsOrderRetreat selectByPrimaryKey = omsOrderRetreatMapper.selectByPrimaryKey(id1);
			OmsOrderDetail selectByPrimaryKey2 = omsOrderDetailMapper.selectByPrimaryKey(selectByPrimaryKey.getBuySupplySid());
			Integer count =omsOrderRetreatMapper.getcountbymingxiID(selectByPrimaryKey.getBuySupplySid(),null);
			if(count==null) {
				count =0;
			}
			selectByPrimaryKey.setZaitushu(count);
			selectByPrimaryKey.setZongshu(selectByPrimaryKey2.getItemNum().intValue());
			return selectByPrimaryKey;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OmsOrderRetreat record) {
		OmsOrderRetreat selectByPrimaryKey = omsOrderRetreatMapper.selectByPrimaryKey(record.getSid());
		Integer tuihuonumber = record.getZaitushu();
		Integer itemNum = record.getZongshu();
		Integer itemNumx =itemNum-tuihuonumber+selectByPrimaryKey.getOrderSourceSid().intValue();
		Integer yutuihuonumber = record.getOrderSourceSid().intValue();
		if(yutuihuonumber>itemNumx) {
			return 3;
		}
		return omsOrderRetreatMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OmsOrderRetreat record) {
				
		record.setSid(null);
		
				
		return omsOrderRetreatMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OmsOrderRetreat record, OmsOrderRetreatExample example) {
		
		return omsOrderRetreatMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OmsOrderRetreat record, OmsOrderRetreatExample example) {
		
		return omsOrderRetreatMapper.updateByExample(record, example);
	}

	@Override
	public List<OmsOrderRetreat> selectByExample(OmsOrderRetreatExample example) {
		
		return omsOrderRetreatMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OmsOrderRetreatExample example) {
		
		return omsOrderRetreatMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OmsOrderRetreatExample example) {
		
		return omsOrderRetreatMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param omsOrderRetreat
	 * @return
	 */
	public int updateVisible(OmsOrderRetreat omsOrderRetreat) {
		return omsOrderRetreatMapper.updateByPrimaryKeySelective(omsOrderRetreat);
	}


	public int tuihuo(String id) {
		OmsOrderRetreat omsOrderRetreat = omsOrderRetreatMapper.selectByPrimaryKey((Long.valueOf(id)));
		omsOrderRetreat.setOrderStatus(2);
		return omsOrderRetreatMapper.updateByPrimaryKeySelective(omsOrderRetreat);
	}

	public int tuihuoqueren(String id) {
		OmsOrderRetreat omsOrderRetreat = omsOrderRetreatMapper.selectByPrimaryKey((Long.valueOf(id)));
		omsOrderRetreat.setOrderStatus(3);
		return omsOrderRetreatMapper.updateByPrimaryKeySelective(omsOrderRetreat);
	}
}
