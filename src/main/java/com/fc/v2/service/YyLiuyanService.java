package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyLiuyanMapper;
import com.fc.v2.model.auto.YyLiuyan;
import com.fc.v2.model.auto.YyLiuyanExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院联系我们 YyLiuyanService
 * @Title: YyLiuyanService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:30:03  
 **/
@Service
public class YyLiuyanService implements BaseService<YyLiuyan, YyLiuyanExample>{
	@Autowired
	private YyLiuyanMapper yyLiuyanMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyLiuyan> list(Tablepar tablepar,YyLiuyan yyLiuyan){
	        YyLiuyanExample testExample=new YyLiuyanExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyLiuyan);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyLiuyan> list= yyLiuyanMapper.selectByExample(testExample);
	        PageInfo<YyLiuyan> pageInfo = new PageInfo<YyLiuyan>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyLiuyanExample example=new YyLiuyanExample();
			example.createCriteria().andIdIn(stringB);
			return yyLiuyanMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyLiuyan selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyLiuyanMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyLiuyan record) {
		return yyLiuyanMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyLiuyan record) {
				
		record.setId(null);
		
				
		return yyLiuyanMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyLiuyan record, YyLiuyanExample example) {
		
		return yyLiuyanMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyLiuyan record, YyLiuyanExample example) {
		
		return yyLiuyanMapper.updateByExample(record, example);
	}

	@Override
	public List<YyLiuyan> selectByExample(YyLiuyanExample example) {
		
		return yyLiuyanMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyLiuyanExample example) {
		
		return yyLiuyanMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyLiuyanExample example) {
		
		return yyLiuyanMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyLiuyan
	 * @return
	 */
	@Transactional
	public int updateVisible(YyLiuyan yyLiuyan) {
		return yyLiuyanMapper.updateByPrimaryKeySelective(yyLiuyan);
	}


}
