package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MachineMapper;
import com.fc.v2.model.auto.Machine;
import com.fc.v2.model.auto.MachineExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 机器管理表 MachineService
 * @Title: MachineService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:39:06  
 **/
@Service
public class MachineService implements BaseService<Machine, MachineExample>{
	@Autowired
	private MachineMapper machineMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Machine> list(Tablepar tablepar,Machine machine){
	        MachineExample testExample=new MachineExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(machine);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Machine> list= machineMapper.selectByExample(testExample);
	        PageInfo<Machine> pageInfo = new PageInfo<Machine>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MachineExample example=new MachineExample();
			example.createCriteria().andIdIn(stringB);
			return machineMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Machine selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return machineMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Machine record) {
		return machineMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Machine record) {
				
		record.setId(null);
		
				
		return machineMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Machine record, MachineExample example) {
		
		return machineMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Machine record, MachineExample example) {
		
		return machineMapper.updateByExample(record, example);
	}

	@Override
	public List<Machine> selectByExample(MachineExample example) {
		
		return machineMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MachineExample example) {
		
		return machineMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MachineExample example) {
		
		return machineMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param machine
	 * @return
	 */
	public int updateVisible(Machine machine) {
		return machineMapper.updateByPrimaryKeySelective(machine);
	}


}
