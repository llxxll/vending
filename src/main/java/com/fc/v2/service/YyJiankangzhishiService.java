package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyJiankangzhishiMapper;
import com.fc.v2.model.auto.YyJiankangzhishi;
import com.fc.v2.model.auto.YyJiankangzhishiExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 健康知识 YyJiankangzhishiService
 * @Title: YyJiankangzhishiService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-02-13 12:16:40  
 **/
@Service
public class YyJiankangzhishiService implements BaseService<YyJiankangzhishi, YyJiankangzhishiExample>{
	@Autowired
	private YyJiankangzhishiMapper yyJiankangzhishiMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyJiankangzhishi> list(Tablepar tablepar,YyJiankangzhishi yyJiankangzhishi){
	        YyJiankangzhishiExample testExample=new YyJiankangzhishiExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyJiankangzhishi);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyJiankangzhishi> list= yyJiankangzhishiMapper.selectByExample(testExample);
	        PageInfo<YyJiankangzhishi> pageInfo = new PageInfo<YyJiankangzhishi>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyJiankangzhishiExample example=new YyJiankangzhishiExample();
			example.createCriteria().andIdIn(stringB);
			return yyJiankangzhishiMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyJiankangzhishi selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyJiankangzhishiMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyJiankangzhishi record) {
		record.setUpdateTime(new Date());
		return yyJiankangzhishiMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyJiankangzhishi record) {
				
		record.setId(null);
		
				
		return yyJiankangzhishiMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyJiankangzhishi record, YyJiankangzhishiExample example) {
		
		return yyJiankangzhishiMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyJiankangzhishi record, YyJiankangzhishiExample example) {
		
		return yyJiankangzhishiMapper.updateByExample(record, example);
	}

	@Override
	public List<YyJiankangzhishi> selectByExample(YyJiankangzhishiExample example) {
		
		return yyJiankangzhishiMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyJiankangzhishiExample example) {
		
		return yyJiankangzhishiMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyJiankangzhishiExample example) {
		
		return yyJiankangzhishiMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyJiankangzhishi
	 * @return
	 */
	@Transactional
	public int updateVisible(YyJiankangzhishi yyJiankangzhishi) {
		return yyJiankangzhishiMapper.updateByPrimaryKeySelective(yyJiankangzhishi);
	}

	@Transactional
	public int updateStatusByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyJiankangzhishiMapper.updateStatusByPrimaryKey(aLong);
	}
	@Transactional
	public int shangjiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyJiankangzhishiMapper.shangjiaByPrimaryKey(aLong);
	}
	@Transactional
	public int xiajiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyJiankangzhishiMapper.xiajiaByPrimaryKey(aLong);
	}

    public int updateByPrimaryKey(YyJiankangzhishi yyJiankangzhishi) {
		return yyJiankangzhishiMapper.updateByPrimaryKey(yyJiankangzhishi);
    }
}
