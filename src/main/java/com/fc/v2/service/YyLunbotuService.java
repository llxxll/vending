package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyLunbotuMapper;
import com.fc.v2.model.auto.YyLunbotu;
import com.fc.v2.model.auto.YyLunbotuExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院轮播图 YyLunbotuService
 * @Title: YyLunbotuService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:52  
 **/
@Service
public class YyLunbotuService implements BaseService<YyLunbotu, YyLunbotuExample>{
	@Autowired
	private YyLunbotuMapper yyLunbotuMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyLunbotu> list(Tablepar tablepar,YyLunbotu yyLunbotu){
	        YyLunbotuExample testExample=new YyLunbotuExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyLunbotu);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyLunbotu> list= yyLunbotuMapper.selectByExample(testExample);
	        PageInfo<YyLunbotu> pageInfo = new PageInfo<YyLunbotu>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyLunbotuExample example=new YyLunbotuExample();
			example.createCriteria().andIdIn(stringB);
			return yyLunbotuMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyLunbotu selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyLunbotuMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyLunbotu record) {
		return yyLunbotuMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyLunbotu record) {
				
		record.setId(null);
		
				
		return yyLunbotuMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyLunbotu record, YyLunbotuExample example) {
		
		return yyLunbotuMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyLunbotu record, YyLunbotuExample example) {
		
		return yyLunbotuMapper.updateByExample(record, example);
	}

	@Override
	public List<YyLunbotu> selectByExample(YyLunbotuExample example) {
		
		return yyLunbotuMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyLunbotuExample example) {
		
		return yyLunbotuMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyLunbotuExample example) {
		
		return yyLunbotuMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyLunbotu
	 * @return
	 */
	@Transactional
	public int updateVisible(YyLunbotu yyLunbotu) {
		return yyLunbotuMapper.updateByPrimaryKeySelective(yyLunbotu);
	}


    public List<YyLunbotu> getList(String type) {
		return yyLunbotuMapper.getList(type);
    }
}
