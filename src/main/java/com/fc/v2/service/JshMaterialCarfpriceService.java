package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.JshMaterialCarfpriceExample;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 产品工艺加个扩展表 JshMaterialCarfpriceService
 * @Title: JshMaterialCarfpriceService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:41:13  
 **/
@Service
@Transactional
public class JshMaterialCarfpriceService implements BaseService<JshMaterialCarfprice, JshMaterialCarfpriceExample>{
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarfpriceMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshMaterialCarfprice> list(Tablepar tablepar,JshMaterialCarfprice jshMaterialCarfprice){
			
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshMaterialCarfprice> list= jshMaterialCarfpriceMapper.getList(jshMaterialCarfprice);
	       
	        PageInfo<JshMaterialCarfprice> pageInfo = new PageInfo<JshMaterialCarfprice>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshMaterialCarfpriceExample example=new JshMaterialCarfpriceExample();
			example.createCriteria().andIdIn(stringB);
			return jshMaterialCarfpriceMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshMaterialCarfprice selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshMaterialCarfpriceMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshMaterialCarfprice record) {
		return jshMaterialCarfpriceMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshMaterialCarfprice record) {
				
		record.setId(null);
		record.setCreateTime(new Date());
		record.setCreateUser(ShiroUtils.getUserId());
				
		return jshMaterialCarfpriceMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(JshMaterialCarfprice record, JshMaterialCarfpriceExample example) {
		
		return jshMaterialCarfpriceMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshMaterialCarfprice record, JshMaterialCarfpriceExample example) {
		
		return jshMaterialCarfpriceMapper.updateByExample(record, example);
	}

	@Override
	public List<JshMaterialCarfprice> selectByExample(JshMaterialCarfpriceExample example) {
		
		return jshMaterialCarfpriceMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshMaterialCarfpriceExample example) {
		
		return jshMaterialCarfpriceMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshMaterialCarfpriceExample example) {
		
		return jshMaterialCarfpriceMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshMaterialCarfprice
	 * @return
	 */
	public int updateVisible(JshMaterialCarfprice jshMaterialCarfprice) {
		return jshMaterialCarfpriceMapper.updateByPrimaryKeySelective(jshMaterialCarfprice);
	}

	public List<JshMaterialCarfprice> getListByMaterid(Long materiaNo) {
		// TODO Auto-generated method stub
		List<JshMaterialCarfprice> listByMaterid = jshMaterialCarfpriceMapper.getListByMaterid(materiaNo);
		for (JshMaterialCarfprice jshMaterialCarfprice : listByMaterid) {
			String ttxxspesc =jshMaterialCarfprice.getTtff()+jshMaterialCarfprice.getTtxx()+jshMaterialCarfprice.getSpecs();
			jshMaterialCarfprice.setTtffxxspesc(ttxxspesc);
		}
		return listByMaterid;
	}


}
