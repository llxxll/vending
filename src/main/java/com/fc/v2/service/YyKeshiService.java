package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyKeshiMapper;
import com.fc.v2.model.auto.YyKeshi;
import com.fc.v2.model.auto.YyKeshiExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院科室 YyKeshiService
 * @Title: YyKeshiService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:28  
 **/
@Service
public class YyKeshiService implements BaseService<YyKeshi, YyKeshiExample>{
	@Autowired
	private YyKeshiMapper yyKeshiMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyKeshi> list(Tablepar tablepar,YyKeshi yyKeshi){
	        YyKeshiExample testExample=new YyKeshiExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyKeshi);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyKeshi> list= yyKeshiMapper.selectByExample(testExample);
	        PageInfo<YyKeshi> pageInfo = new PageInfo<YyKeshi>(list);
	        return  pageInfo;
	 }
	 
	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyKeshiExample example=new YyKeshiExample();
			example.createCriteria().andIdIn(stringB);
			return yyKeshiMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyKeshi selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyKeshiMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyKeshi record) {
		return yyKeshiMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyKeshi record) {
				
		record.setId(null);
		
				
		return yyKeshiMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyKeshi record, YyKeshiExample example) {
		
		return yyKeshiMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyKeshi record, YyKeshiExample example) {
		
		return yyKeshiMapper.updateByExample(record, example);
	}

	@Override
	public List<YyKeshi> selectByExample(YyKeshiExample example) {
		
		return yyKeshiMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyKeshiExample example) {
		
		return yyKeshiMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyKeshiExample example) {
		
		return yyKeshiMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyKeshi
	 * @return
	 */
	@Transactional
	public int updateVisible(YyKeshi yyKeshi) {
		return yyKeshiMapper.updateByPrimaryKeySelective(yyKeshi);
	}


	public List<YyKeshi> listtoten() {
		return yyKeshiMapper.listtoten();
	}

	public List<String> getListByField1() {
		return yyKeshiMapper.getListByField1();
	}

	public List<YyKeshi> selectByFiled1(String field1) {
		return yyKeshiMapper.selectByFiled1(field1);
	}
	
	public  List<YyKeshi> selectsslist(){
	
        List<YyKeshi> list= yyKeshiMapper.selectsslist();
        return  list;
 }

    public int updateByPrimaryKey(YyKeshi keshi) {
		return yyKeshiMapper.updateByPrimaryKey(keshi);
    }
}
