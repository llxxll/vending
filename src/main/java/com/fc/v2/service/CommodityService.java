package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;

import com.fc.v2.controller.xinlingshou.ApiXlsController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.CommodityMapper;
import com.fc.v2.model.auto.Commodity;
import com.fc.v2.model.auto.CommodityExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 商品管理表 CommodityService
 * @Title: CommodityService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:09:32  
 **/
@Service
public class CommodityService implements BaseService<Commodity, CommodityExample>{
	@Autowired
	private CommodityMapper commodityMapper;
	@Autowired
	private ApiXlsController apiXlsController;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Commodity> list(Tablepar tablepar,Commodity commodity) throws Exception {
	        CommodityExample testExample=new CommodityExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(commodity);
	        }
		 	int totle = tablepar.getPage() * tablepar.getLimit();
		 	int totlenew  = commodityMapper.getTotle();
		 	if(totle > totlenew){
				apiXlsController.searchGoods(0, totle,tablepar.getLimit());
			}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Commodity> list= commodityMapper.selectByExample(testExample);

	        PageInfo<Commodity> pageInfo = new PageInfo<Commodity>(list);
		 	pageInfo.setTotal(100000);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			CommodityExample example=new CommodityExample();
			example.createCriteria().andIdIn(stringB);
			return commodityMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Commodity selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return commodityMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Commodity record) {
		return commodityMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Commodity record) {
				
		record.setId(null);


		return commodityMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Commodity record, CommodityExample example) {
		
		return commodityMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Commodity record, CommodityExample example) {
		
		return commodityMapper.updateByExample(record, example);
	}

	@Override
	public List<Commodity> selectByExample(CommodityExample example) {
		
		return commodityMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(CommodityExample example) {
		
		return commodityMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(CommodityExample example) {
		
		return commodityMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param commodity
	 * @return
	 */
	public int updateVisible(Commodity commodity) {
		return commodityMapper.updateByPrimaryKeySelective(commodity);
	}


	public Commodity selectBygoodsId(Long goodsId) {
		return commodityMapper.selectBygoodsId(goodsId);
	}
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<Commodity> list2(Tablepar tablepar,Commodity commodity){

		PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		List<Commodity> list= commodityMapper.selectByExample2(commodity);
		PageInfo<Commodity> pageInfo = new PageInfo<Commodity>(list);
		return  pageInfo;
	}

	public Commodity selectByOutGoodId(String outGoodId) {
		return commodityMapper.selectByOutGoodId(outGoodId);
	}
}
