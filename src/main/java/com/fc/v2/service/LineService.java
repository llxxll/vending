package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.LineMapper;
import com.fc.v2.model.auto.Line;
import com.fc.v2.model.auto.LineExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 线路管理表 LineService
 * @Title: LineService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:35:21  
 **/
@Service
public class LineService implements BaseService<Line, LineExample>{
	@Autowired
	private LineMapper lineMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Line> list(Tablepar tablepar,Line line){
	        LineExample testExample=new LineExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(line);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Line> list= lineMapper.selectByExample(testExample);
	        PageInfo<Line> pageInfo = new PageInfo<Line>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			LineExample example=new LineExample();
			example.createCriteria().andIdIn(stringB);
			return lineMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Line selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return lineMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Line record) {
		return lineMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Line record) {
				
		record.setId(null);
		
				
		return lineMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Line record, LineExample example) {
		
		return lineMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Line record, LineExample example) {
		
		return lineMapper.updateByExample(record, example);
	}

	@Override
	public List<Line> selectByExample(LineExample example) {
		
		return lineMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(LineExample example) {
		
		return lineMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(LineExample example) {
		
		return lineMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param line
	 * @return
	 */
	public int updateVisible(Line line) {
		return lineMapper.updateByPrimaryKeySelective(line);
	}


}
