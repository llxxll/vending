package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderCustomerMapper;
import com.fc.v2.model.auto.OrderCustomer;
import com.fc.v2.model.auto.OrderCustomerExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 客户表 OrderCustomerService
 * @Title: OrderCustomerService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-09-15 20:45:08  
 **/
@Service
public class OrderCustomerService implements BaseService<OrderCustomer, OrderCustomerExample>{
	@Autowired
	private OrderCustomerMapper orderCustomerMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OrderCustomer> list(Tablepar tablepar,OrderCustomer orderCustomer){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OrderCustomer> list= orderCustomerMapper.getList(orderCustomer);
	        PageInfo<OrderCustomer> pageInfo = new PageInfo<OrderCustomer>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderCustomerExample example=new OrderCustomerExample();
			example.createCriteria().andSidIn(stringB);
			return orderCustomerMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OrderCustomer selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderCustomerMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OrderCustomer record) {
		OrderCustomer recordss =orderCustomerMapper.getbyPhone(record.getCustomerPhone());
		if(recordss!=null) {
			//判断id  是否相同 
			if(record.getSid()!=recordss.getSid()) {
				return 3;
			}
		}
		return orderCustomerMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OrderCustomer record) {
				
		record.setSid(null);
		OrderCustomer recordss =orderCustomerMapper.getbyPhone(record.getCustomerPhone());
				if(recordss!=null) {
					return 3;
				}
		return orderCustomerMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OrderCustomer record, OrderCustomerExample example) {
		
		return orderCustomerMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OrderCustomer record, OrderCustomerExample example) {
		
		return orderCustomerMapper.updateByExample(record, example);
	}

	@Override
	public List<OrderCustomer> selectByExample(OrderCustomerExample example) {
		
		return orderCustomerMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderCustomerExample example) {
		
		return orderCustomerMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderCustomerExample example) {
		
		return orderCustomerMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param orderCustomer
	 * @return
	 */
	public int updateVisible(OrderCustomer orderCustomer) {
		return orderCustomerMapper.updateByPrimaryKeySelective(orderCustomer);
	}


}
