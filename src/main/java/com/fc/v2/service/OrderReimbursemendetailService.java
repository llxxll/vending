package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderReimbursemendetailMapper;
import com.fc.v2.model.auto.OrderReimbursemendetail;
import com.fc.v2.model.auto.OrderReimbursemendetailExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 报销明细表 OrderReimbursemendetailService
 * @Title: OrderReimbursemendetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-09-18 21:31:37  
 **/
@Service
public class OrderReimbursemendetailService implements BaseService<OrderReimbursemendetail, OrderReimbursemendetailExample>{
	@Autowired
	private OrderReimbursemendetailMapper orderReimbursemendetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OrderReimbursemendetail> list(Tablepar tablepar,OrderReimbursemendetail orderReimbursemendetail){
	        OrderReimbursemendetailExample testExample=new OrderReimbursemendetailExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(orderReimbursemendetail);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("sid ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OrderReimbursemendetail> list= orderReimbursemendetailMapper.selectByExample(testExample);
	        PageInfo<OrderReimbursemendetail> pageInfo = new PageInfo<OrderReimbursemendetail>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderReimbursemendetailExample example=new OrderReimbursemendetailExample();
			example.createCriteria().andSidIn(stringB);
			return orderReimbursemendetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OrderReimbursemendetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderReimbursemendetailMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OrderReimbursemendetail record) {
		return orderReimbursemendetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OrderReimbursemendetail record) {
				
		record.setSid(null);
		
				
		return orderReimbursemendetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OrderReimbursemendetail record, OrderReimbursemendetailExample example) {
		
		return orderReimbursemendetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OrderReimbursemendetail record, OrderReimbursemendetailExample example) {
		
		return orderReimbursemendetailMapper.updateByExample(record, example);
	}

	@Override
	public List<OrderReimbursemendetail> selectByExample(OrderReimbursemendetailExample example) {
		
		return orderReimbursemendetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderReimbursemendetailExample example) {
		
		return orderReimbursemendetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderReimbursemendetailExample example) {
		
		return orderReimbursemendetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param orderReimbursemendetail
	 * @return
	 */
	public int updateVisible(OrderReimbursemendetail orderReimbursemendetail) {
		return orderReimbursemendetailMapper.updateByPrimaryKeySelective(orderReimbursemendetail);
	}


    public void deleteByOrderNo(String orderNo) {
		orderReimbursemendetailMapper.deleteByOrderNo(orderNo);
    }
}
