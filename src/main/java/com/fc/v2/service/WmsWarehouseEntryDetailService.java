package com.fc.v2.service;

import java.util.List;
import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanOudetailGongyingMapper;
import com.fc.v2.mapper.auto.PlanOudetailMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderDetailPzMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderMapper;
import com.fc.v2.mapper.auto.SysDepartmentMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsSonghuoMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseStockMapper;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.model.auto.PlanWorkOrderDetailPz;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsSonghuo;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetailExample;
import com.fc.v2.model.auto.WmsWarehouseStock;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 入库单明细 WmsWarehouseEntryDetailService
 * @Title: WmsWarehouseEntryDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:28:17  
 **/
@Service
@Transactional
public class WmsWarehouseEntryDetailService implements BaseService<WmsWarehouseEntryDetail, WmsWarehouseEntryDetailExample>{
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private WmsWarehouseStockMapper wmsWarehouseStockMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private WmsSonghuoMapper wmsSonghuoMapper;
	@Autowired
	private PlanOudetailGongyingMapper planOudetailGongyingMapper;
	@Autowired
	private PlanOudetailMapper planOudetailMapper;
	@Autowired
	private PlanWorkOrderDetailPzMapper planWorkOrderDetailPzMapper;
	@Autowired
	private PlanWorkOrderMapper planWorkOrderMapper;
	@Autowired
	private SysDepartmentMapper sysDepartmentMapper ;
	
      	      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<WmsWarehouseEntryDetail> list(Tablepar tablepar,WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<WmsWarehouseEntryDetail> list= wmsWarehouseEntryDetailMapper.getList(wmsWarehouseEntryDetail);
	       
	        
	        
	        PageInfo<WmsWarehouseEntryDetail> pageInfo = new PageInfo<WmsWarehouseEntryDetail>(list);
	        return  pageInfo;
	 }
	 
	 public PageInfo<WmsWarehouseEntryDetail> chejianlist(Tablepar tablepar,
				WmsWarehouseEntryDetail wmsWarehouseEntryDetail) {
		 PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<WmsWarehouseEntryDetail> list= wmsWarehouseEntryDetailMapper.chejianlist(wmsWarehouseEntryDetail);
	        for (WmsWarehouseEntryDetail wmsWarehouseEntryDetail2 : list) {
	        	JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(wmsWarehouseEntryDetail2.getMatterNo()));
	        	if(selectByPrimaryKey3!=null) {
					 wmsWarehouseEntryDetail2.setChukuname(selectByPrimaryKey3.getName());
					 wmsWarehouseEntryDetail2.setChukuspesc(selectByPrimaryKey3.getModel());
					 
				 }
	        	//来源
	        	if(wmsWarehouseEntryDetail2.getModifiedUserSid()!=null) {
	        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getModifiedUserSid());
	        		
	        		wmsWarehouseEntryDetail2.setLaiyuanName(selectByPrimaryKey.getNickname());
	        	}
	        	//入库人
	        	if(wmsWarehouseEntryDetail2.getXUserSid()!=null) {
	        			TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getXUserSid());
	        		
	        		wmsWarehouseEntryDetail2.setRukuName(selectByPrimaryKey.getNickname());
	        	}
	        	
	        	//根据入库单号查询 工单明细
	        	PlanWorkOrderDetailPz selectOneByworno = planWorkOrderDetailPzMapper.selectOneByworno(wmsWarehouseEntryDetail2.getSid()+"");
	        	if(selectOneByworno!=null) {
	        		SysDepartment selectByPrimaryKey = sysDepartmentMapper.selectByPrimaryKey(selectOneByworno.getManager());
	        		wmsWarehouseEntryDetail2.setOrgName(selectByPrimaryKey.getDeptName());
	        	}
	        	
			}
	        
	        
	        PageInfo<WmsWarehouseEntryDetail> pageInfo = new PageInfo<WmsWarehouseEntryDetail>(list);
	        return  pageInfo;
		}
		public PageInfo<WmsWarehouseEntryDetail> listwwaixie(Tablepar tablepar,
				WmsWarehouseEntryDetail wmsWarehouseEntryDetail) {
			PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<WmsWarehouseEntryDetail> list= wmsWarehouseEntryDetailMapper.listwwaixie(wmsWarehouseEntryDetail);
	        for (WmsWarehouseEntryDetail wmsWarehouseEntryDetail2 : list) {
					 
	        	
	        	//来源
	        	if(wmsWarehouseEntryDetail2.getModifiedUserSid()!=null) {
	        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getModifiedUserSid());
	        		
	        		wmsWarehouseEntryDetail2.setLaiyuanName(selectByPrimaryKey.getNickname());
	        	}
	        	//入库人
	        	if(wmsWarehouseEntryDetail2.getXUserSid()!=null) {
	        			TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getXUserSid());
	        		
	        		wmsWarehouseEntryDetail2.setRukuName(selectByPrimaryKey.getNickname());
	        	}
	        	
	        	
			}
	        
	        
	        PageInfo<WmsWarehouseEntryDetail> pageInfo = new PageInfo<WmsWarehouseEntryDetail>(list);
	        return  pageInfo;
		}
	 
	 
	 
	 
	 
	 public PageInfo<WmsWarehouseEntryDetail> getlist(Tablepar tablepar,WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
		 
		 //PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		 List<WmsWarehouseEntryDetail> list= wmsWarehouseEntryDetailMapper.getListByparams(wmsWarehouseEntryDetail);
		 for (WmsWarehouseEntryDetail wmsWarehouseEntryDetail2 : list) {
			 if(wmsWarehouseEntryDetail2.getStockId()!=null) {
				 WmsWarehouseStock selectByPrimaryKey2 = wmsWarehouseStockMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getStockId());
				 JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey2.getMatterNo()));
				 wmsWarehouseEntryDetail2.setStockName(selectByPrimaryKey3.getName());
				 wmsWarehouseEntryDetail2.setStockNameModel(selectByPrimaryKey3.getModel());
				 wmsWarehouseEntryDetail2.setStockNamegongyi(selectByPrimaryKey2.getXs());
				 wmsWarehouseEntryDetail2.setStockNumber(selectByPrimaryKey2.getStockNumber()+"");
			 }
        	
        	//来源
        	if(wmsWarehouseEntryDetail2.getModifiedUserSid()!=null) {
        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getModifiedUserSid());
        		
        		wmsWarehouseEntryDetail2.setLaiyuanName(selectByPrimaryKey.getNickname());
        	}
        	//入库人
        	if(wmsWarehouseEntryDetail2.getXUserSid()!=null) {
        			TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(wmsWarehouseEntryDetail2.getXUserSid());
        		
        		wmsWarehouseEntryDetail2.setRukuName(selectByPrimaryKey.getNickname());
        	}
		}
		 PageInfo<WmsWarehouseEntryDetail> pageInfo = new PageInfo<WmsWarehouseEntryDetail>(list);
		 return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			WmsWarehouseEntryDetailExample example=new WmsWarehouseEntryDetailExample();
			example.createCriteria().andSidIn(stringB);
			return wmsWarehouseEntryDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public WmsWarehouseEntryDetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			WmsWarehouseEntryDetail selectByPrimaryKey = wmsWarehouseEntryDetailMapper.selectByPrimaryKey(id1);
			/**
			PlanOudetail selectByPrimaryKey2 = planOudetailMapper.getOneBynO(selectByPrimaryKey.getDeliverySid());
			PlanOudetailGongying selectByPrimaryKey1 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
			//获取入库数字
			Integer rukucount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"", "1",null);
			if(rukucount==null) {
				rukucount=0;
			}
			Integer lingcount =0;
			if(selectByPrimaryKey2.getField2().equals("0")) {
				//获取领料数据
				WmsWarehouseDeliveryDetail onebyUnitName = wmsWarehouseDeliveryDetailMapper.getOnebyUnitName(selectByPrimaryKey1.getId()+"", "1");
				lingcount=onebyUnitName.getMatterNumber().intValue();
			}else {
				lingcount=selectByPrimaryKey1.getCount();
			}
			*/
			
			return selectByPrimaryKey;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(WmsWarehouseEntryDetail record) {
		//获取入库单 数据
		int updateByPrimaryKeySelective=0;
		WmsWarehouseEntryDetail selectByPrimaryKey = wmsWarehouseEntryDetailMapper.selectByPrimaryKey(record.getSid());
		PlanOudetail selectByPrimaryKey2 = planOudetailMapper.getOneBynO(selectByPrimaryKey.getDeliverySid());
		PlanOudetailGongying selectByPrimaryKey1 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
		//获取入库数字
		Integer rukucount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"", "1",2);
		Integer tuiliaocount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"", "5",2);
		if(tuiliaocount==null) {
			tuiliaocount=0;
		}
		if(rukucount==null) {
			rukucount=0;
		}
		Integer lingcount =0;
		if(selectByPrimaryKey2.getField2().equals("0")) {
			//获取领料数据
			WmsWarehouseDeliveryDetail onebyUnitName = wmsWarehouseDeliveryDetailMapper.getOnebyUnitName(selectByPrimaryKey1.getId()+"", "1");
			lingcount=onebyUnitName.getMatterNumber().intValue();
			Integer countxxx =lingcount-tuiliaocount-rukucount-record.getMatterNumber().intValue();
			if(countxxx<0) {
				return 9;
			}
		}
		
		Integer x = selectByPrimaryKey.getX();
		if(x==1) {
			JshMaterial selectByNameandmodel =null;
			if(selectByPrimaryKey.getQualityNo().equals("5")) {
				selectByNameandmodel = jshMaterialMapper.selectByNameandmodel(record.getChukuname(), record.getChukuspesc());
			}else {
				
				selectByNameandmodel= jshMaterialMapper.selectByName(record.getChukuname());
			}
			//数量需小于库存
			//根据 名称和类型
			WmsWarehouseStock  wmsWarehouseStock = new WmsWarehouseStock();
			wmsWarehouseStock.setMatterNo(selectByNameandmodel.getId()+"");
			wmsWarehouseStock.setXs(record.getStockNamegongyi());
			wmsWarehouseStock.setHouseNo(selectByPrimaryKey.getHouseNo());
			
			WmsWarehouseStock oneByParams = wmsWarehouseStockMapper.getOneByParams(wmsWarehouseStock);
			if(oneByParams==null) {
				oneByParams= new WmsWarehouseStock();
				oneByParams.setMatterNo(selectByNameandmodel.getId()+"");
				oneByParams.setXs(record.getStockNamegongyi());
				oneByParams.setHouseNo(selectByPrimaryKey.getHouseNo());
				oneByParams.setStockNumber(0l);
				wmsWarehouseStockMapper.insertSelective(oneByParams);
				
			}
			
			//入库数量
			selectByPrimaryKey.setMatterNumber(record.getMatterNumber());
			//入库时间
			selectByPrimaryKey.setXTime(new Date());
			//状态已入库
			selectByPrimaryKey.setX(2);
			//入库人
			selectByPrimaryKey.setXUserSid(ShiroUtils.getUserId());
			//库存id
			selectByPrimaryKey.setStockId(oneByParams.getSid());
			updateByPrimaryKeySelective = wmsWarehouseEntryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
			
			if(updateByPrimaryKeySelective>0) {
				//修改库存数字
				//修改库存数字
				Long stockNumber = oneByParams.getStockNumber();
				Long matterNumber = selectByPrimaryKey.getMatterNumber();
				Long matterNumberx =stockNumber + matterNumber;
				
				oneByParams.setStockNumber(matterNumberx);
				updateByPrimaryKeySelective = wmsWarehouseStockMapper.updateByPrimaryKeySelective(oneByParams);
				
				if(selectByPrimaryKey.getQualityNo().equals("1")) {
					
					
					//获取委外单
					//获取退料数量
					Integer tuiliaoCount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","5",2);
					if(tuiliaoCount==null) {
						tuiliaoCount=0;
					}
					//获取入库数字
					Integer rukuCount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","1",2);
					Integer rukuCount1 = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","1",3);
					if(rukuCount==null) {
						rukuCount=0;
					}
					if(rukuCount1==null) {
						rukuCount1=0;
					}
					//获取不合格数字
					Integer unCount =wmsSonghuoMapper.getcountBYpid(selectByPrimaryKey1.getId());
					Integer uuuuuCount =0;
					if(selectByPrimaryKey2.getField2().equals("0")) {
						Integer count = selectByPrimaryKey1.getCount();
						uuuuuCount =count-tuiliaoCount-rukuCount-unCount-rukuCount1;
					}else {
						//获取领料数量
						Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByPrimaryKey1.getId()+"","1");
						if(countbyUnitName==null) {
							countbyUnitName=0;
						}
						
						uuuuuCount =countbyUnitName-tuiliaoCount-rukuCount-unCount-rukuCount1;
					}
					if(uuuuuCount<=0) {
						//数字无问题 状态改为已完成
						selectByPrimaryKey1.setField1("4");
						planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey1);
					}
					
				}else if(selectByPrimaryKey.getQualityNo().equals("2")) {
					//更改派工明细状态
					PlanWorkOrderDetailPz planWorkOrderDetailPz = planWorkOrderDetailPzMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
					//PlanWorkOrderDetailPz planWorkOrderDetailPz = new PlanWorkOrderDetailPz();
					planWorkOrderDetailPz.setStatus(5);
					//planWorkOrderDetailPz.setId(Long.valueOf(selectByPrimaryKey.getUnitName()));
					 planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
					
					//判断该工艺 是否全部入库  如果全部入库 则 更改工单状态为 已入库
					List<PlanWorkOrderDetailPz> listss =planWorkOrderDetailPzMapper.getListByCarftidandWOno(planWorkOrderDetailPz.getCarftId(),planWorkOrderDetailPz.getWorkerOrderNo());
					int pzx =0;
					for (PlanWorkOrderDetailPz planWorkOrderDetailPz2 : listss) {
						Integer status = planWorkOrderDetailPz2.getStatus();
						if(status!=5) {
							pzx=1;
						}
					}
					if(pzx==0) {
						//修改工单状态 为已入库
						PlanWorkOrder	planWorkOrderpz = new PlanWorkOrder();
						planWorkOrderpz.setStatus(3);
						planWorkOrderpz.setWorkerOrderNo(planWorkOrderDetailPz.getWorkerOrderNo());
						planWorkOrderMapper.updateByworkorderNo(planWorkOrderpz);
					}
					
					//判断是否需要委外
					Long zhuxuOrg = selectByPrimaryKey.getZhuxuOrg();
					if(zhuxuOrg==29) {
						//创建委外计划
						String workerOrderNo = planWorkOrderDetailPz.getWorkerOrderNo();
						PlanWorkOrder selectByworkerOrderNo = planWorkOrderMapper.selectOneByworno(workerOrderNo);
						PlanOudetail planProduceMaterialDetail = new PlanOudetail();
						planProduceMaterialDetail.setCount(selectByPrimaryKey.getMatterNumber().intValue());
						planProduceMaterialDetail.setCreateTime(new Date());
						planProduceMaterialDetail.setField2("0");
						planProduceMaterialDetail.setCreator(ShiroUtils.getUserId());
						planProduceMaterialDetail.setProduceNo(workerOrderNo);
						//判断如果为空 则从入库明细获取数据
						planProduceMaterialDetail.setMateriaNo(selectByworkerOrderNo.getMaterialNo());
						planProduceMaterialDetail.setStandard(selectByworkerOrderNo.getField2());
						planOudetailMapper.insertSelective(planProduceMaterialDetail);
						
					}
				}else if(selectByPrimaryKey.getQualityNo().equals("5")) {
					
						
							//获取所有 入库明细 数量相加=领料数量 则 修改委外结算状态
							//获取领料数字
							//获取退料数量
							Integer tuiliaoCount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","5",2);
							if(tuiliaoCount==null) {
								tuiliaoCount=0;
							}
							//获取入库数字
							Integer rukuCount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","1",2);
							Integer rukuCount1 = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","1",3);
							if(rukuCount==null) {
								rukuCount=0;
							}
							if(rukuCount1==null) {
								rukuCount1=0;
							}
							//获取不合格数字
							Integer unCount = wmsSonghuoMapper.getuncountBYpid(selectByPrimaryKey1.getId());
							if(unCount==null) {
								unCount=0;
							}
							//获取领料数量
							Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByPrimaryKey1.getId()+"","1");
							if(countbyUnitName==null) {
								countbyUnitName=0;
							}
							Integer countsxs =unCount+rukuCount+tuiliaoCount+rukuCount1;
								int compareTo = countsxs.compareTo(countbyUnitName);
			        			if(compareTo==0) {
			        				selectByPrimaryKey1.setField1("4");
			        				planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey1);
			        				//如果已完成 则变更委外单对应所有结算单状态为 待结算
			        			}
					
				}
			}
			
			
		}else {
			return 3;
		}
		
		
		
		return updateByPrimaryKeySelective;
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(WmsWarehouseEntryDetail record) {
				
		record.setSid(null);
		
				
		return wmsWarehouseEntryDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(WmsWarehouseEntryDetail record, WmsWarehouseEntryDetailExample example) {
		
		return wmsWarehouseEntryDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(WmsWarehouseEntryDetail record, WmsWarehouseEntryDetailExample example) {
		
		return wmsWarehouseEntryDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<WmsWarehouseEntryDetail> selectByExample(WmsWarehouseEntryDetailExample example) {
		
		return wmsWarehouseEntryDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(WmsWarehouseEntryDetailExample example) {
		
		return wmsWarehouseEntryDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(WmsWarehouseEntryDetailExample example) {
		
		return wmsWarehouseEntryDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param wmsWarehouseEntryDetail
	 * @return
	 */
	public int updateVisible(WmsWarehouseEntryDetail wmsWarehouseEntryDetail) {
		return wmsWarehouseEntryDetailMapper.updateByPrimaryKeySelective(wmsWarehouseEntryDetail);
	}

	public Integer getCountByStatus(Integer status) {
		// TODO Auto-generated method stub
		return wmsWarehouseEntryDetailMapper.getCountByStatus(status);
	}

	public int editSave(WmsWarehouseEntryDetail wmsWarehouseEntryDetail) {
		WmsWarehouseEntryDetail selectByPrimaryKey = wmsWarehouseEntryDetailMapper.selectByPrimaryKey(wmsWarehouseEntryDetail.getSid());
		
		
		String qualityNo = selectByPrimaryKey.getQualityNo();
		if(qualityNo.equals("1")) {
			PlanOudetailGongying selectByPrimaryKey2 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getMatterDesc()));
			if(selectByPrimaryKey2.getField1().equals("2")) {
				return 3;
			}
		}else if(qualityNo.equals("2")) {
			PlanWorkOrder selectOneByworno = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getMatterDesc());
			if(selectOneByworno.getStatus()==4) {
				return 3;
			}
		}else if(qualityNo.equals("5")) {
			
			String deliverySid = selectByPrimaryKey.getMatterDesc();
			String deliverySid2 = selectByPrimaryKey.getDeliverySid();
			if(deliverySid2!=null) {
			Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$"); 
			boolean matches = pattern.matcher(deliverySid).matches();
			if(matches==true) {
				PlanOudetailGongying selectByPrimaryKey2 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getMatterDesc()));
				if(selectByPrimaryKey2.getField1().equals("2")) {
					return 3;
				}
			}else {
				
				PlanWorkOrder selectOneByworno = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getMatterDesc());
				if(selectOneByworno.getStatus()==4) {
					return 3;
				}
			}
			
			
		}
			}
		
		
		
		//现在的
		Long matterNumber = wmsWarehouseEntryDetail.getMatterNumber();
		Long stockId = wmsWarehouseEntryDetail.getStockId();
		
		//之前的
		Long stockId2 = selectByPrimaryKey.getStockId();
		Long matterNumber2 = selectByPrimaryKey.getMatterNumber();
		
		selectByPrimaryKey.setMatterNumber(matterNumber);
		selectByPrimaryKey.setStockId(stockId);
		
		int updateByPrimaryKeySelective = wmsWarehouseEntryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		if(updateByPrimaryKeySelective>0) {
			if(stockId2!=null) {
				WmsWarehouseStock selectByPrimaryKey2 = wmsWarehouseStockMapper.selectByPrimaryKey(stockId2);
				Long stockNumber = selectByPrimaryKey2.getStockNumber();
				Long xxxx =stockNumber+matterNumber-matterNumber2;
				if(xxxx<0) {
					xxxx=0l;
				}
				selectByPrimaryKey2.setStockNumber(xxxx);
				wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey2);
			}
			
			if(selectByPrimaryKey.getQualityNo().equals("1")) {
				//获取委外单
				PlanOudetailGongying selectByPrimaryKey3 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getMatterDesc()));
				//获取退料数量
				Integer tuiliaoCount = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey3.getId()+"","5",2);
				if(tuiliaoCount==null) {
					tuiliaoCount=0;
				}
				//获取入库数字
				Integer rukuCount = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey3.getId()+"","1",2);
				Integer rukuCount1 = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey3.getId()+"","1",3);
				if(rukuCount==null) {
					rukuCount=0;
				}
				if(rukuCount1==null) {
					rukuCount1=0;
				}
				//获取不合格数字
				Integer unCount = wmsSonghuoMapper.getuncountBYpid(selectByPrimaryKey3.getId());
				if(unCount==null) {
					unCount=0;
				}
				Integer uuuuuCount =0;
				if(selectByPrimaryKey3.getField2().equals("0")) {
					Integer count = selectByPrimaryKey3.getCount();
					uuuuuCount =count-tuiliaoCount-rukuCount-unCount-rukuCount1;
				}else {
					//获取领料数量
					Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByPrimaryKey3.getId()+"","1");
					if(countbyUnitName==null) {
						countbyUnitName=0;
					}
					uuuuuCount =countbyUnitName-tuiliaoCount-rukuCount-unCount-rukuCount1;
				}
				if(uuuuuCount<=0) {
					//数字无问题 状态改为已完成
					selectByPrimaryKey3.setField1("4");
					planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey3);
				}else {
					selectByPrimaryKey3.setField1("3");
					planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey3);
				}
				
			}else if(selectByPrimaryKey.getQualityNo().equals("5")) {
				String deliverySiddddd = selectByPrimaryKey.getMatterDesc();
				String deliverySidx = selectByPrimaryKey.getDeliverySid();
				if(deliverySidx!=null) {
				Pattern pattern1 = Pattern.compile("^[-\\+]?[\\d]*$"); 
				boolean matches1 = pattern1.matcher(deliverySiddddd).matches();
				if(matches1==true) {
				
					PlanOudetailGongying selectByPrimaryKey2x = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getMatterDesc()));
					//获取所有 入库明细 数量相加=领料数量 则 修改委外结算状态
					//获取领料数字
					//获取退料数量
					Integer tuiliaoCount = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey2x.getId()+"","5",2);
					if(tuiliaoCount==null) {
						tuiliaoCount=0;
					}
					//获取入库数字
					Integer rukuCount = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey2x.getId()+"","1",2);
					Integer rukuCount1 = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey2x.getId()+"","1",3);
					if(rukuCount==null) {
						rukuCount=0;
					}
					if(rukuCount1==null) {
						rukuCount1=0;
					}
					//获取不合格数字
					Integer unCount = wmsSonghuoMapper.getuncountBYpid(selectByPrimaryKey2x.getId());
					if(unCount==null) {
						unCount=0;
					}
					//获取领料数量
					Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByPrimaryKey2x.getId()+"","1");
					if(countbyUnitName==null) {
						countbyUnitName=0;
					}
					Integer countsxs =unCount+rukuCount+tuiliaoCount+rukuCount1;
					Integer countsxsxxx =countbyUnitName-countsxs;
	        			if(countsxsxxx<=0) {
	        				selectByPrimaryKey2x.setField1("4");
	        				planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey2x);
	        			}else {
	        				selectByPrimaryKey2x.setField1("3");
	        				planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey2x);
	        				
	        			}
				}
			}
			}
			
			
			
		}
		return updateByPrimaryKeySelective;
	}
	public int chexiao(String ids) {
		int updateByPrimaryKeySelective =0;
		WmsWarehouseEntryDetail selectByPrimaryKey = wmsWarehouseEntryDetailMapper.selectByPrimaryKey(Long.valueOf(ids));
		//根据入库单号获取 工单
		List<PlanWorkOrderDetailPz> listBYworkno = planWorkOrderDetailPzMapper.getListBYworkno(selectByPrimaryKey.getSid()+"");
		int deleteByPrimaryKey =0;
		for (PlanWorkOrderDetailPz planWorkOrderDetailPz : listBYworkno) {
			deleteByPrimaryKey = planWorkOrderDetailPzMapper.deleteByPrimaryKey(planWorkOrderDetailPz.getId());
		}
		if(deleteByPrimaryKey>0) {
			planWorkOrderMapper.delBywrokNo(selectByPrimaryKey.getSid()+"");
			selectByPrimaryKey.setX(2);
			 updateByPrimaryKeySelective = wmsWarehouseEntryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		}
		return updateByPrimaryKeySelective;
		
	}
	public int savezhuanxu(WmsWarehouseEntryDetail wmsWarehouseEntryDetail) {
		WmsWarehouseEntryDetail selectBy = wmsWarehouseEntryDetailMapper.selectByPrimaryKey(wmsWarehouseEntryDetail.getSid());
		if(!selectBy.getQualityNo().equals("1")) {
			return 0;
		}
		//获取委外计划
		PlanOudetail selectByPrimaryKey = planOudetailMapper.selectByPrimaryKey(Long.valueOf(selectBy.getDeliverySid()));
		
		//根据单号获取 工单 
		PlanWorkOrder planWorkOrder = planWorkOrderMapper.selectOneByworno(wmsWarehouseEntryDetail.getSid()+"");
		if(planWorkOrder!=null) {
			return 3;
		}
		//添加 工单数据
		planWorkOrder = new PlanWorkOrder();
		
		String workNo = wmsWarehouseEntryDetail.getSid()+"";
		planWorkOrder.setWorkerOrderNo(workNo);
		//状态
		planWorkOrder.setStatus(2);
		//仓库
		planWorkOrder.setField1(selectBy.getHouseNo());
		//数量
		planWorkOrder.setCount(selectBy.getMatterNumber().intValue());
		
		planWorkOrder.setMaterialNo(selectBy.getMatterNo());
		planWorkOrder.setField2(selectByPrimaryKey.getStandard());
		planWorkOrder.setCreateTime(new Date());
		planWorkOrder.setCreator(ShiroUtils.getUserId());
		// 保存数据
		int result = planWorkOrderMapper.insertSelective(planWorkOrder);
		if(result>0) {
		
		
		//添加工单明细数据
		PlanWorkOrderDetailPz planWorkOrderDetailPz = new PlanWorkOrderDetailPz();
		//添加新的派工明细
		planWorkOrderDetailPz.setCount(selectBy.getMatterNumber().intValue());
				planWorkOrderDetailPz.setStatus(6);
				planWorkOrderDetailPz.setWorkerOrderNo(workNo);
				planWorkOrderDetailPz.setCreateTime(new Date());
				planWorkOrderDetailPz.setManager(wmsWarehouseEntryDetail.getXianManger());
				planWorkOrderDetailPz.setCreator(ShiroUtils.getUserId());
				result =  planWorkOrderDetailPzMapper.insertSelective(planWorkOrderDetailPz);
				
				if(result>0) {
					//添加出库明细数据(已出库)     入库单 转序 quno=1 数据问题
					 WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseDeliveryDetail();
						wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(planWorkOrderDetailPz.getCount()));
						wmsWarehouseDeliveryDetail.setUnitName(selectBy.getSid()+"");
						wmsWarehouseDeliveryDetail.setX(2);
						wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
						wmsWarehouseDeliveryDetail.setCreatedUserSid(ShiroUtils.getUserId());
						wmsWarehouseDeliveryDetail.setMatterNo(selectBy.getMatterNo());
						//添加出库仓库
						wmsWarehouseDeliveryDetail.setHouseNo(selectBy.getHouseNo());
						wmsWarehouseDeliveryDetail.setStockId(selectBy.getStockId());
						
						wmsWarehouseDeliveryDetail.setQualityNo("2");
						result = wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
						if(result>0) {
							//修改库存	 
							//根据stockid 获取库存
							WmsWarehouseStock wmsWarehouseStock = wmsWarehouseStockMapper.selectByPrimaryKey(wmsWarehouseDeliveryDetail.getStockId());
							//修改库存数字
							if(wmsWarehouseStock!=null) {
								
								Long stockNumber = wmsWarehouseStock.getStockNumber();
								Long matterNumber = wmsWarehouseDeliveryDetail.getMatterNumber();
								Long matterNumberx =stockNumber - matterNumber;
								wmsWarehouseStock.setStockNumber(matterNumberx);
								result = wmsWarehouseStockMapper.updateByPrimaryKeySelective(wmsWarehouseStock);
							}
							if(result>0) {
								//变更入库单状态
								 selectBy.setX(3);
								 result = wmsWarehouseEntryDetailMapper.updateByPrimaryKeySelective(selectBy);
								 
							}
							
						}
				}
		}
		return result;
	}

	public int piliangzhuanxusave(String ids, String xianManger) {
		Long[] integers = ConvertUtil.toLongArray(",", ids);
		List<Long> stringB = Arrays.asList(integers);
		int result =0;
		for (Long long1 : stringB) {
			WmsWarehouseEntryDetail selectBy = wmsWarehouseEntryDetailMapper.selectByPrimaryKey(long1);
			if(!selectBy.getQualityNo().equals("1")) {
				continue;
			}
			if(selectBy.getMatterNumber()==0) {
				continue;
			}
			
			
			
			//获取委外计划
			PlanOudetail selectByPrimaryKey = planOudetailMapper.selectByPrimaryKey(Long.valueOf(selectBy.getDeliverySid()));
			
			//根据单号获取 工单 
			PlanWorkOrder planWorkOrder = planWorkOrderMapper.selectOneByworno(long1+"");
			if(planWorkOrder!=null) {
				continue;
			}
			//添加 工单数据
			planWorkOrder = new PlanWorkOrder();
			
			planWorkOrder.setWorkerOrderNo(long1+"");
			//状态
			planWorkOrder.setStatus(2);
			//仓库
			planWorkOrder.setField1(selectBy.getHouseNo());
			//数量
			planWorkOrder.setCount(selectBy.getMatterNumber().intValue());
			
			planWorkOrder.setMaterialNo(selectBy.getMatterNo());
			planWorkOrder.setField2(selectByPrimaryKey.getStandard());
			planWorkOrder.setCreateTime(new Date());
			planWorkOrder.setCreator(ShiroUtils.getUserId());
			// 保存数据
			result = planWorkOrderMapper.insertSelective(planWorkOrder);
			
			if(result>0) {
				
			
			//添加工单明细数据
			PlanWorkOrderDetailPz planWorkOrderDetailPz = new PlanWorkOrderDetailPz();
			//添加新的派工明细
			planWorkOrderDetailPz.setCount(selectBy.getMatterNumber().intValue());
					planWorkOrderDetailPz.setStatus(6);
					planWorkOrderDetailPz.setWorkerOrderNo(long1+"");
					planWorkOrderDetailPz.setCreateTime(new Date());
					planWorkOrderDetailPz.setManager(Integer.valueOf(xianManger));
					planWorkOrderDetailPz.setCreator(ShiroUtils.getUserId());
					result =  planWorkOrderDetailPzMapper.insertSelective(planWorkOrderDetailPz);
					
					if(result>0) {
						//添加出库明细数据(已出库)     入库单 转序 quno=1 数据问题
						 WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseDeliveryDetail();
							wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(planWorkOrderDetailPz.getCount()));
							wmsWarehouseDeliveryDetail.setUnitName(selectBy.getSid()+"");
							wmsWarehouseDeliveryDetail.setX(2);
							wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
							wmsWarehouseDeliveryDetail.setCreatedUserSid(ShiroUtils.getUserId());
							wmsWarehouseDeliveryDetail.setMatterNo(selectBy.getMatterNo());
							//添加出库仓库
							wmsWarehouseDeliveryDetail.setHouseNo(selectBy.getHouseNo());
							wmsWarehouseDeliveryDetail.setStockId(selectBy.getStockId());
							
							wmsWarehouseDeliveryDetail.setQualityNo("2");
							result = wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
							if(result>0) {
								//修改库存	 
								//根据stockid 获取库存
								WmsWarehouseStock wmsWarehouseStock = wmsWarehouseStockMapper.selectByPrimaryKey(wmsWarehouseDeliveryDetail.getStockId());
								//修改库存数字
								if(wmsWarehouseStock!=null) {
									
									Long stockNumber = wmsWarehouseStock.getStockNumber();
									Long matterNumber = wmsWarehouseDeliveryDetail.getMatterNumber();
									Long matterNumberx =stockNumber - matterNumber;
									wmsWarehouseStock.setStockNumber(matterNumberx);
									result = wmsWarehouseStockMapper.updateByPrimaryKeySelective(wmsWarehouseStock);
								}
								if(result>0) {
									//变更入库单状态
									 selectBy.setX(3);
									 result = wmsWarehouseEntryDetailMapper.updateByPrimaryKeySelective(selectBy);
									 
								}
								
							}
					}
			}
		}
		return result;
	}
	


}
