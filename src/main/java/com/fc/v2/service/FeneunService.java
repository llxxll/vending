package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.FeneunMapper;
import com.fc.v2.model.auto.Feneun;
import com.fc.v2.model.auto.FeneunExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 分润 FeneunService
 * @Title: FeneunService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:56  
 **/
@Service
public class FeneunService implements BaseService<Feneun, FeneunExample>{
	@Autowired
	private FeneunMapper feneunMapper;
	
      	   	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Feneun> list(Tablepar tablepar,Feneun feneun){
	        FeneunExample testExample=new FeneunExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(feneun);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Feneun> list= feneunMapper.selectByExample(testExample);
	        PageInfo<Feneun> pageInfo = new PageInfo<Feneun>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			FeneunExample example=new FeneunExample();
			example.createCriteria().andIdIn(stringB);
			return feneunMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Feneun selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return feneunMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Feneun record) {
		return feneunMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Feneun record) {
				
		record.setId(null);
		
				
		return feneunMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Feneun record, FeneunExample example) {
		
		return feneunMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Feneun record, FeneunExample example) {
		
		return feneunMapper.updateByExample(record, example);
	}

	@Override
	public List<Feneun> selectByExample(FeneunExample example) {
		
		return feneunMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(FeneunExample example) {
		
		return feneunMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(FeneunExample example) {
		
		return feneunMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param feneun
	 * @return
	 */
	public int updateVisible(Feneun feneun) {
		return feneunMapper.updateByPrimaryKeySelective(feneun);
	}


}
