package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MyPackMapper;
import com.fc.v2.model.auto.MyPack;
import com.fc.v2.model.auto.MyPackExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 *  MyPackService
 * @Title: MyPackService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-11-06 22:36:31  
 **/
@Service
public class MyPackService implements BaseService<MyPack, MyPackExample>{
	@Autowired
	private MyPackMapper myPackMapper;
	
      	   	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MyPack> list(Tablepar tablepar,MyPack myPack){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MyPack> list= myPackMapper.getList(myPack);
	        PageInfo<MyPack> pageInfo = new PageInfo<MyPack>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MyPackExample example=new MyPackExample();
			example.createCriteria().andIdIn(stringB);
			return myPackMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MyPack selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return myPackMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MyPack record) {
		
		MyPack selectByPrimaryKey = myPackMapper.selectByPrimaryKey(record.getId());
		if(!selectByPrimaryKey.getPackName().equals(record.getPackName())) {
			MyPack selectByPrimaryKey1 =myPackMapper.selectByname(record.getPackName());
			if(selectByPrimaryKey1!=null) {
				return 3;
			}
		}
		
		return myPackMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MyPack record) {
				
		record.setId(null);
		MyPack selectByPrimaryKey1 =myPackMapper.selectByname(record.getPackName());
		if(selectByPrimaryKey1!=null) {
			return 3;
		}
				
		return myPackMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MyPack record, MyPackExample example) {
		
		return myPackMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MyPack record, MyPackExample example) {
		
		return myPackMapper.updateByExample(record, example);
	}

	@Override
	public List<MyPack> selectByExample(MyPackExample example) {
		
		return myPackMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MyPackExample example) {
		
		return myPackMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MyPackExample example) {
		
		return myPackMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param myPack
	 * @return
	 */
	public int updateVisible(MyPack myPack) {
		return myPackMapper.updateByPrimaryKeySelective(myPack);
	}


}
