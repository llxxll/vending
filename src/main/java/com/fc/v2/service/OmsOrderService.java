package com.fc.v2.service;

import java.util.List;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import com.fc.v2.mapper.auto.*;
import com.fc.v2.model.auto.*;
import com.fc.v2.util.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 销售-订单表 OmsOrderService
 * @Title: OmsOrderService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-05-21 23:28:35  
 **/
@Service
@Transactional
public class OmsOrderService implements BaseService<OmsOrder, OmsOrderExample>{
	@Autowired
	private OmsOrderMapper omsOrderMapper;
	@Autowired
	private OrderLiushuiMapper orderLiushuiMapper;
	@Autowired
	private OmsOrderDetailService omsOrderDetailService;
	@Autowired
	private OmsOrderDetailMapper omsOrderDetailMapper;
	@Autowired
	private OrderFromCategoryMapper orderFromCategoryMapper;
	@Autowired
	private JshInvoiceMapper jshInvoiceMapper;
	@Autowired
	private OmsOrderRetreatMapper omsOrderRetreatMapper;
	

	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OmsOrder> list(Tablepar tablepar,OmsOrder omsOrder){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OmsOrder> list= omsOrderMapper.getList(omsOrder);
	        for(OmsOrder order: list){
				OrderFromCategory orderFromCategory = orderFromCategoryMapper.selectByPrimaryKey(Long.valueOf(order.getDeliveryTypeSid()));
				if(orderFromCategory != null){
					order.setLy(orderFromCategory.getProductTitle());
				}
	        }
	        PageInfo<OmsOrder> pageInfo = new PageInfo<OmsOrder>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
			OmsOrder omsOrder = selectByPrimaryKey(ids);
			omsOrderDetailService.deleteByOrderNo(omsOrder.getOrderNo());
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OmsOrderExample example=new OmsOrderExample();
			example.createCriteria().andSidIn(stringB);
			return omsOrderMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OmsOrder selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return omsOrderMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OmsOrder record) {
			List<Long> delId = record.getDelId();
			if(delId!=null) {
				if(delId.size()>0) {
					for (Long long1 : delId) {
						if(long1!=null) {
							
							omsOrderDetailMapper.deleteByPrimaryKey(long1);
						}
					}
				}
			}
			
			List<OmsOrderDetail> mdProduct = record.getMdProduct();
			double doubleValuexxxtt=0.0;
			for (OmsOrderDetail omsOrderDetail : mdProduct) {
				OmsOrderDetail selectByPrimaryKey = omsOrderDetailMapper.selectByPrimaryKey(omsOrderDetail.getSid());
				if(selectByPrimaryKey==null) {
					omsOrderDetail.setOrderNo(record.getOrderNo());
					omsOrderDetailMapper.insertSelective(omsOrderDetail);
				}else {
					omsOrderDetailMapper.updateByPrimaryKeySelective(omsOrderDetail);
				}
				BigDecimal xiaoshouPrice = omsOrderDetail.getXiaoshouPrice();
				Long itemNum = omsOrderDetail.getItemNum();
				double doubleValue = xiaoshouPrice.doubleValue();
				double doubleValuexxx = doubleValue*itemNum;
				doubleValuexxxtt+=doubleValuexxx;
			}
			
			BigDecimal xiaoshouPricezong = new BigDecimal(doubleValuexxxtt);
			record.setProductMoney(xiaoshouPricezong);
			int updateByPrimaryKeySelective = omsOrderMapper.updateByPrimaryKeySelective(record);
			
		return updateByPrimaryKeySelective;
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OmsOrder record) {
				
		record.setSid(null);
		//插入一条明细 返回ID 
		JshInvoice jshin = new JshInvoice();
		jshin.setOrderNo("销售订单");
		jshInvoiceMapper.insertSelective(jshin);
		//获取当前时间
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		String format = simpleDateFormat.format(new Date());
		record.setOrderNo(ShiroUtils.getUserId()+"-"+format+"-"+jshin.getId());
		record.setOrderStatus(1);
		record.setCreateTime(new Date());
		record.setSellSupplySid(ShiroUtils.getUserId());
		record.setSellSupplyName(ShiroUtils.getUser().getNickname());
					List<OmsOrderDetail> mdProduct = record.getMdProduct();
					double doubleValuexxxtt=0.0;
					for (OmsOrderDetail mdProduct2 : mdProduct) {
						
						mdProduct2.setOrderNo(record.getOrderNo());
						BigDecimal xiaoshouPrice = mdProduct2.getXiaoshouPrice();
						Long itemNum = mdProduct2.getItemNum();
						double doubleValue = xiaoshouPrice.doubleValue();
						double doubleValuexxx = doubleValue*itemNum;
						doubleValuexxxtt+=doubleValuexxx;
						omsOrderDetailService.insertSelective(mdProduct2);
					}
					BigDecimal xiaoshouPricezong = new BigDecimal(doubleValuexxxtt);
					record.setProductMoney(xiaoshouPricezong);
				int insertSelective = omsOrderMapper.insertSelective(record);
		return insertSelective;
	}

	public int insertSelectiveapi(OmsOrder record) {

		record.setSid(null);
		record.setOrderStatus(1);
		record.setCreateTime(new Date());
		int insertSelective = omsOrderMapper.insertSelective(record);
		return insertSelective;
	}
	
	@Override
	public int updateByExampleSelective(OmsOrder record, OmsOrderExample example) {
		
		return omsOrderMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OmsOrder record, OmsOrderExample example) {
		
		return omsOrderMapper.updateByExample(record, example);
	}

	@Override
	public List<OmsOrder> selectByExample(OmsOrderExample example) {
		
		return omsOrderMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OmsOrderExample example) {
		
		return omsOrderMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OmsOrderExample example) {
		
		return omsOrderMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param omsOrder
	 * @return
	 */
	public int updateVisible(OmsOrder omsOrder) {
		return omsOrderMapper.updateByPrimaryKeySelective(omsOrder);
	}


	public int updateOmsOrderByStatus(String sid, String status, Long userid) {
		//1 代付款2 待审核3 待排产4 待结算5 待出库  6 已完成
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(Long.valueOf(sid));
		omsOrderMapper.updateOmsOrderByStatus(sid, status);
		//增加操作流水记录
		OrderLiushui oredll = new OrderLiushui();
		oredll.setTtxx(selectByPrimaryKey.getOrderNo());
		oredll.setCreateTime(new Date());
		oredll.setUserid(userid);
		oredll.setType("3");
		if(status.equals("1")) {
			oredll.setTitle("销售单创建");
		}else if(status.equals("2")) {
			oredll.setTitle("订金确认");	
			
		}else if(status.equals("3")) {
			oredll.setTitle("审核通过");
			
		}else if(status.equals("4")) {
			oredll.setTitle("生产完成");
			
		}else if(status.equals("5")) {
			oredll.setTitle("结算完成");
			
		}else if(status.equals("6")) {
			oredll.setTitle("出库完成");
			
		}
		return orderLiushuiMapper.insertSelective(oredll);
	}
	
	public int updateOmsOrderByStatusweb(String sid, String status, Long userid) {
		//1 代付款2 待审核3 待排产4 待结算5 待出库  6 已完成
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(Long.valueOf(sid));
		selectByPrimaryKey.setProductNum(1);
		omsOrderMapper.updateOmsOrderByStatus(sid, status);
		//增加操作流水记录
		OrderLiushui oredll = new OrderLiushui();
		oredll.setTtxx(selectByPrimaryKey.getOrderNo());
		oredll.setCreateTime(new Date());
		oredll.setUserid(userid);
		oredll.setType("1");
		if(status.equals("1")) {
			oredll.setTitle("销售单创建");
		}else if(status.equals("2")) {
			oredll.setTitle("审核通过");	
			
		}else if(status.equals("3")) {
			oredll.setTitle("可发货");
			
		}else if(status.equals("4")) {
			oredll.setTitle("发货确认");
			
		}else if(status.equals("5")) {
			oredll.setTitle("生产中");
			
		}
		return orderLiushuiMapper.insertSelective(oredll);
	}
	public int fahuoqueren(OmsOrder order) {
		//1 代付款2 待审核3 待排产4 待结算5 待出库  6 已完成
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(order.getSid());
		selectByPrimaryKey.setOrderStatus(order.getOrderStatus());
		selectByPrimaryKey.setOrderSourceSid(order.getOrderSourceSid());
		selectByPrimaryKey.setUserSid(order.getUserSid());
		omsOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		//增加操作流水记录
		OrderLiushui oredll = new OrderLiushui();
		oredll.setTtxx(selectByPrimaryKey.getOrderNo());
		oredll.setCreateTime(new Date());
		oredll.setUserid(ShiroUtils.getUserId());
		oredll.setType("1");
		Integer status = order.getOrderStatus();
		if(status==1) {
			oredll.setTitle("销售单创建");
		}else if(status==2) {
			oredll.setTitle("审核通过");	
			
		}else if(status==3) {
			oredll.setTitle("可发货");
			
		}else if(status==4) {
			oredll.setTitle("发货确认");
			
		}else if(status==5) {
			oredll.setTitle("生产中");
			
		}
		return orderLiushuiMapper.insertSelective(oredll);
	}
	
	public int caiwuqueren(Long ids) {
		//1 代付款2 待审核3 待排产4 待结算5 待出库  6 已完成
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(ids);
		selectByPrimaryKey.setOrderStatus(3);
		omsOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		//增加操作流水记录
		OrderLiushui oredll = new OrderLiushui();
		oredll.setTtxx(selectByPrimaryKey.getOrderNo());
		oredll.setCreateTime(new Date());
		oredll.setUserid(ShiroUtils.getUserId());
		oredll.setType("1");
		Integer status = selectByPrimaryKey.getOrderStatus();
		if(status==1) {
			oredll.setTitle("销售单创建");
		}else if(status==2) {
			oredll.setTitle("审核通过");	
			
		}else if(status==3) {
			oredll.setTitle("可发货");
			
		}else if(status==4) {
			oredll.setTitle("发货确认");
			
		}else if(status==5) {
			oredll.setTitle("生产中");
			
		}
		return orderLiushuiMapper.insertSelective(oredll);
	}
	
	
	
	public PageInfo<OmsOrder> listByown(Tablepar tablepar, OmsOrder omsOrder) {
		PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		List<OmsOrder> list= omsOrderMapper.getList(omsOrder);
		for(OmsOrder order: list){
			OrderFromCategory orderFromCategory = orderFromCategoryMapper.selectByPrimaryKey(Long.valueOf(order.getDeliveryTypeSid()));
			if(orderFromCategory != null){
				order.setLy(orderFromCategory.getProductTitle());
			}
		}
		PageInfo<OmsOrder> pageInfo = new PageInfo<OmsOrder>(list);
		return  pageInfo;
	}

	public PageInfo<OmsOrder> listshengcan(Tablepar tablepar, OmsOrder omsOrder) {
		PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		List<OmsOrder> list= omsOrderMapper.listshengcan(omsOrder);
		for(OmsOrder order: list){
			OrderFromCategory orderFromCategory = orderFromCategoryMapper.selectByPrimaryKey(Long.valueOf(order.getDeliveryTypeSid()));
			if(orderFromCategory != null){
				order.setLy(orderFromCategory.getProductTitle());
			}
		}
		PageInfo<OmsOrder> pageInfo = new PageInfo<OmsOrder>(list);
		return  pageInfo;
	}
	public PageInfo<OmsOrder> listFH(Tablepar tablepar, OmsOrder omsOrder) {
		PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		List<OmsOrder> list= omsOrderMapper.getListFH(omsOrder);
		for(OmsOrder order: list){
			OrderFromCategory orderFromCategory = orderFromCategoryMapper.selectByPrimaryKey(Long.valueOf(order.getDeliveryTypeSid()));
			if(orderFromCategory != null){
				order.setLy(orderFromCategory.getProductTitle());
			}
		}
		PageInfo<OmsOrder> pageInfo = new PageInfo<OmsOrder>(list);
		return  pageInfo;
	}

	public OrderLiushui getUpdateUserSid(String orderNo) {
		return orderLiushuiMapper.getUpdateUserSid(orderNo);
	}

	public int querenfukuan(String ids) {
		OmsOrder oms = new OmsOrder();
		oms.setSid(Long.valueOf(ids));
		oms.setPostage(2l);
		
		return omsOrderMapper.updateByPrimaryKeySelective(oms);
	}

	public int shengcanwancheng(Long ids) {
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(ids);
		selectByPrimaryKey.setProductNum(3);
		return omsOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		
	}
	public int shengcanpai(Long ids) {
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(ids);
		selectByPrimaryKey.setProductNum(2);
		return omsOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
	}

	public int chexiao(Long ids) {
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(ids);
		if(selectByPrimaryKey.getOrderStatus()!=5) {
			return 3;
		}
		selectByPrimaryKey.setOrderStatus(4);
		selectByPrimaryKey.setTs(null);
		return omsOrderMapper.updateByPrimaryKeySelectivebyNull(selectByPrimaryKey);
	}

	public int wancheng(Long ids) {
		
		OmsOrder selectByPrimaryKey = omsOrderMapper.selectByPrimaryKey(ids);
		//根据订单号 获取 退货信息
		List<OmsOrderRetreat> omsOrderRetreatList =omsOrderRetreatMapper.getListByorderNO(selectByPrimaryKey.getOrderNo());
		
		if(omsOrderRetreatList!=null) {
			if(omsOrderRetreatList.size()>0) {
				return 3;
			}
		}
		
		
		selectByPrimaryKey.setOrderStatus(5);
		selectByPrimaryKey.setTs(new Date());
		return omsOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
	}
	
    
}
