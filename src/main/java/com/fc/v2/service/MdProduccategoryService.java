package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MdProduccategoryMapper;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.auto.MdProduccategoryExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 主数据-商品信息表 类别 MdProduccategoryService
 * @Title: MdProduccategoryService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-23 22:57:59  
 **/
@Service
public class MdProduccategoryService implements BaseService<MdProduccategory, MdProduccategoryExample>{
	@Autowired
	private MdProduccategoryMapper mdProduccategoryMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MdProduccategory> list(Tablepar tablepar,MdProduccategory mdProduccategory){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MdProduccategory> list= mdProduccategoryMapper.getList(mdProduccategory);
	        PageInfo<MdProduccategory> pageInfo = new PageInfo<MdProduccategory>(list);
	        return  pageInfo;
	 }
	 public List<MdProduccategory> getList(MdProduccategory mdProduccategory){
		 
		 List<MdProduccategory> list= mdProduccategoryMapper.getList(mdProduccategory);
		 return  list;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MdProduccategoryExample example=new MdProduccategoryExample();
			example.createCriteria().andSidIn(stringB);
			return mdProduccategoryMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MdProduccategory selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return mdProduccategoryMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MdProduccategory record) {
		MdProduccategory selectByName = mdProduccategoryMapper.selectByName(record.getProductTitle());
		if(selectByName!=null) {
			return 3;
		}
		return mdProduccategoryMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MdProduccategory record) {
				
		record.setSid(null);
		record.setCreatedTime(new Date());
		record.setCreatedUserSid(ShiroUtils.getUserId());	
		MdProduccategory record1 =mdProduccategoryMapper.selectByName(record.getProductTitle());
		if(record1==null) {
			
			return mdProduccategoryMapper.insertSelective(record);
		}else {
			return 3;
		}
	}
	
	
	@Override
	public int updateByExampleSelective(MdProduccategory record, MdProduccategoryExample example) {
		
		return mdProduccategoryMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MdProduccategory record, MdProduccategoryExample example) {
		
		return mdProduccategoryMapper.updateByExample(record, example);
	}

	@Override
	public List<MdProduccategory> selectByExample(MdProduccategoryExample example) {
		
		return mdProduccategoryMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MdProduccategoryExample example) {
		
		return mdProduccategoryMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MdProduccategoryExample example) {
		
		return mdProduccategoryMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param mdProduccategory
	 * @return
	 */
	public int updateVisible(MdProduccategory mdProduccategory) {
		return mdProduccategoryMapper.updateByPrimaryKeySelective(mdProduccategory);
	}


	public List<MdProduccategory> getByBrandSid(String brandSid) {
		return mdProduccategoryMapper.getByBrandSid(brandSid);
	}
}
