package com.fc.v2.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.Optional;

import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderMapper;
import com.fc.v2.mapper.auto.SysDepartmentMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.model.auto.*;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.PlanProduceMaterialDetailMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderDetailPzMapper;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 生产计划明细 PlanProduceMaterialDetailService
 * @Title: PlanProduceMaterialDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 20:11:36  
 **/
@Service
@Transactional
public class PlanProduceMaterialDetailService implements BaseService<PlanProduceMaterialDetail, PlanProduceMaterialDetailExample>{
	@Autowired
	private PlanProduceMaterialDetailMapper planProduceMaterialDetailMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	public SysUserService sysUserService;
	@Autowired
	private PlanWorkOrderMapper planWorkOrderMapper;
	@Autowired
	private PlanWorkOrderDetailPzMapper planWorkOrderDetailPzMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarfpriceMapper;
	@Autowired
	private SysDepartmentMapper sysDepartmentMapper;

	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanProduceMaterialDetail> list(Tablepar tablepar,PlanProduceMaterialDetail planProduceMaterialDetail){

	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanProduceMaterialDetail> list= planProduceMaterialDetailMapper.getList(planProduceMaterialDetail);
	        for(PlanProduceMaterialDetail planProduceMaterialDetail1 : list){
				JshMaterial jshMaterial = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planProduceMaterialDetail1.getStandard()));
				planProduceMaterialDetail1.setClName(jshMaterial.getName());
				planProduceMaterialDetail1.setClProperty(jshMaterial.getModel());
			}
	        PageInfo<PlanProduceMaterialDetail> pageInfo = new PageInfo<PlanProduceMaterialDetail>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			PlanProduceMaterialDetailExample example=new PlanProduceMaterialDetailExample();
			example.createCriteria().andIdIn(stringB);
			return planProduceMaterialDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public PlanProduceMaterialDetail selectByPrimaryKey(String id) {
				
		PlanProduceMaterialDetail planProduceMaterialDetail = planProduceMaterialDetailMapper.selectByPrimaryKey(Long.valueOf(id));
		JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planProduceMaterialDetail.getStandard()));
		planProduceMaterialDetail.setClName(selectByPrimaryKey.getName());
		planProduceMaterialDetail.setClProperty(selectByPrimaryKey.getModel());
		return planProduceMaterialDetail;
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanProduceMaterialDetail record) {
		
		return planProduceMaterialDetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanProduceMaterialDetail record) {
				
		record.setId(null);
		record.setStatus(1);
		record.setCreateTime(new Date());
		record.setCreator(ShiroUtils.getUserId());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
			// 复制保留无需修改的数据
		record.setProduceNo("SC"+ simpleDateFormat.format(new Date()));

		return planProduceMaterialDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanProduceMaterialDetail record, PlanProduceMaterialDetailExample example) {
		
		return planProduceMaterialDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanProduceMaterialDetail record, PlanProduceMaterialDetailExample example) {
		
		return planProduceMaterialDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanProduceMaterialDetail> selectByExample(PlanProduceMaterialDetailExample example) {
		
		return planProduceMaterialDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanProduceMaterialDetailExample example) {
		
		return planProduceMaterialDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanProduceMaterialDetailExample example) {
		
		return planProduceMaterialDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planProduceMaterialDetail
	 * @return
	 */
	public int updateVisible(PlanProduceMaterialDetail planProduceMaterialDetail) {
		return planProduceMaterialDetailMapper.updateByPrimaryKeySelective(planProduceMaterialDetail);
	}


	public int billSave(PlanProduceMaterialDetail planProduceMaterialDetail) {

		Long userid = ShiroUtils.getUserId();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		//添加工单
		PlanWorkOrder planWorkOrder = new PlanWorkOrder();
		if(planProduceMaterialDetail.getField2().equals("1")) {
			planWorkOrder.setDeleteFlag("1");
			planWorkOrder.setStatus(2);

		}else {
			planWorkOrder.setStatus(1);
		}
		
		//工单号
		String workNo ="GD"+ShiroUtils.getUserId()+ simpleDateFormat.format(new Date());
		
		planWorkOrder.setCount(planProduceMaterialDetail.getNumber());
		planWorkOrder.setWorkerOrderNo(workNo);
		//状态
		
		//仓库
		planWorkOrder.setField1(planProduceMaterialDetail.getHouseNo());
		//数量
		
		planWorkOrder.setMaterialNo(planProduceMaterialDetail.getMateriaNo());
		planWorkOrder.setField2(planProduceMaterialDetail.getStandard());
		//计划
		planWorkOrder.setNo(String.valueOf(planProduceMaterialDetail.getId()));
		planWorkOrder.setCreateTime(new Date());
		planWorkOrder.setCreator(userid);
		// 保存数据
		int insertSelective = planWorkOrderMapper.insertSelective(planWorkOrder);
		if(insertSelective>0) {
			//添加工单明细数据
			PlanWorkOrderDetailPz	planWorkOrderDetail = new PlanWorkOrderDetailPz();
			planWorkOrderDetail.setCarftId(planProduceMaterialDetail.getJshcarftId());
			planWorkOrderDetail.setCount(planProduceMaterialDetail.getNumber());
			planWorkOrderDetail.setWorkerOrderNo(workNo);
			planWorkOrderDetail.setWorker(planProduceMaterialDetail.getWorkID());
			if(planProduceMaterialDetail.getField2().equals("1")) {
				planWorkOrderDetail.setStatus(2);
			}else {
				planWorkOrderDetail.setStatus(1);
				
			}
			TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planProduceMaterialDetail.getWorkID()));
			planWorkOrderDetail.setManager(selectByPrimaryKey.getDepId());
			planWorkOrderDetail.setCreator(userid);
			planWorkOrderDetail.setCreateTime(new Date());
			insertSelective = planWorkOrderDetailPzMapper.insertSelective(planWorkOrderDetail);
			if(insertSelective>0) {
				
			
			//添加出库单数据
			//出库数据
			WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseDeliveryDetail();
			wmsWarehouseDeliveryDetail.setHouseNo(planProduceMaterialDetail.getHouseNo());
			wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(planProduceMaterialDetail.getNumber()));
			
			wmsWarehouseDeliveryDetail.setDeliverySid(String.valueOf(planProduceMaterialDetail.getId()));
			wmsWarehouseDeliveryDetail.setMatterNo(planProduceMaterialDetail.getStandard());
			//工单出库
			
			wmsWarehouseDeliveryDetail.setQualityNo("2");
			wmsWarehouseDeliveryDetail.setUnitName(planWorkOrder.getWorkerOrderNo());
			wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
			wmsWarehouseDeliveryDetail.setCreatedUserSid(userid);
			if(planProduceMaterialDetail.getField2().equals("1")) {
				wmsWarehouseDeliveryDetail.setModifiedTime(new Date());
				wmsWarehouseDeliveryDetail.setModifiedUserSid(userid);
				wmsWarehouseDeliveryDetail.setX(2);
			}else {
				wmsWarehouseDeliveryDetail.setX(1);
			}
			
			insertSelective = wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
			}
			
		}
		//根据计划编号获取 计划
		PlanProduceMaterialDetail planProduceMaterialDetailNew = planProduceMaterialDetailMapper.selectByPrimaryKey(planProduceMaterialDetail.getId());

		//开单完计划状态 ： 生产
		planProduceMaterialDetailNew.setId(planProduceMaterialDetail.getId());
		planProduceMaterialDetailNew.setStatus(2);
		return planProduceMaterialDetailMapper.updateByPrimaryKeySelective(planProduceMaterialDetailNew);
	}

	public Integer getCountByStatus(Integer status) {

		return  planProduceMaterialDetailMapper.getCountByStatus(status);
	}

	public int upload(List<PlanProduceMaterialDetail> execltoList) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		int insertSelective =0;
		for (PlanProduceMaterialDetail plan : execltoList) {
			//根据物料号 获取物料数据
			String materialName = plan.getMaterialName();
			if(materialName!=null) {
				String trim = materialName.trim();
				if(trim.equals("")) {
					materialName=null;
				}
			}
			//材料
			String standardName = plan.getStandardName();
			if(standardName!=null) {
				String trim = standardName.trim();
				if(trim.equals("")) {
					standardName=null;
				}
			}else {
				standardName="/";
			}
			//材料规格
			String field1 = plan.getField1();
			if(field1!=null) {
				String trim = field1.trim();
				if(trim.equals("")) {
					field1="/";
				}
			}else {
				field1="/";
			}
			// 计划数量
			Integer planNumber = plan.getCount();
			
			// 备注
			String material = plan.getMaterial();
			if(material!=null) {
				String trim = material.trim();
				if(trim.equals("")) {
					material=null;
				}
			}
			
			String materialNo = plan.getMateriaNo();
			if(materialNo!=null) {
				String trim = materialNo.trim();
				if(trim.equals("")) {
					materialNo=null;
				}
			}
			if(materialName==null||standardName==null||field1==null||planNumber==null) {
				continue;
			}

			JshMaterial findByTitle = jshMaterialMapper.selectByName(materialName);
			if(findByTitle==null) {
				findByTitle =new JshMaterial();
				findByTitle.setName(materialName);
				findByTitle.setModel(materialNo);
				findByTitle.setCategoryId(1l);
				jshMaterialMapper.insertSelective(findByTitle);
			}
			plan.setMateriaNo(String.valueOf(findByTitle.getId()));
			//根据 材料 和规格获取 材料数据
			JshMaterial findByRawNameAndRawSpec = jshMaterialMapper.selectByNameandmodel(standardName,field1);
			if(findByRawNameAndRawSpec==null) {
				findByRawNameAndRawSpec = new JshMaterial();
				findByRawNameAndRawSpec.setName(standardName);
				findByRawNameAndRawSpec.setModel(field1);
				findByRawNameAndRawSpec.setCategoryId(2l);
				jshMaterialMapper.insertSelective(findByRawNameAndRawSpec);
			}
			plan.setStandard(String.valueOf(findByRawNameAndRawSpec.getId()));
			plan.setProduceNo("SC"+ simpleDateFormat.format(new Date()));
			plan.setMaterial(material);
			plan.setStatus(1);
			plan.setCreator(ShiroUtils.getUserId());
			plan.setCreateTime(new Date());
			
			plan.setStandardName(null);
			plan.setMaterialName(null);
			plan.setField1(null);	
			
			insertSelective = planProduceMaterialDetailMapper.insertSelective(plan);

		}
		return insertSelective;
	}

	public List<TsysUser> selectListByDept(Integer deptId) {
		// TODO Auto-generated method stub
		return tsysUserMapper.selectListByDept(deptId);
	}
	
	public List<SysDepartment> selectSysDepartmentListByParams() {
		// TODO Auto-generated method stub
		return sysDepartmentMapper.selectListByParams();
	}
	public List<SysDepartment> selectListByParamsbymyself(Integer depID) {
		
		
		return sysDepartmentMapper.selectListByParamsbymyself(depID);
	}
}
