package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyNewsTypeMapper;
import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.model.auto.YyNewsTypeExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 发布新闻类类型 YyNewsTypeService
 * @Title: YyNewsTypeService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-03-07 22:05:52  
 **/
@Service
public class YyNewsTypeService implements BaseService<YyNewsType, YyNewsTypeExample>{
	@Autowired
	private YyNewsTypeMapper yyNewsTypeMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyNewsType> list(Tablepar tablepar,YyNewsType yyNewsType){
	        YyNewsTypeExample testExample=new YyNewsTypeExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyNewsType);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyNewsType> list= yyNewsTypeMapper.selectByExample(testExample);
	        PageInfo<YyNewsType> pageInfo = new PageInfo<YyNewsType>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyNewsTypeExample example=new YyNewsTypeExample();
			example.createCriteria().andIdIn(stringB);
			return yyNewsTypeMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyNewsType selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyNewsTypeMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(YyNewsType record) {
		return yyNewsTypeMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(YyNewsType record) {
				
		record.setId(null);
		
				
		return yyNewsTypeMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(YyNewsType record, YyNewsTypeExample example) {
		
		return yyNewsTypeMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(YyNewsType record, YyNewsTypeExample example) {
		
		return yyNewsTypeMapper.updateByExample(record, example);
	}

	@Override
	public List<YyNewsType> selectByExample(YyNewsTypeExample example) {
		
		return yyNewsTypeMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyNewsTypeExample example) {
		
		return yyNewsTypeMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(YyNewsTypeExample example) {
		
		return yyNewsTypeMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyNewsType
	 * @return
	 */
	public int updateVisible(YyNewsType yyNewsType) {
		return yyNewsTypeMapper.updateByPrimaryKeySelective(yyNewsType);
	}


	public List<YyNewsType> selectByfw(String fw) {
		return yyNewsTypeMapper.selectByfw(fw);
	}
}
