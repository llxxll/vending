package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyKeshiMapper;
import com.fc.v2.mapper.auto.YyZhuanjiaMapper;
import com.fc.v2.model.auto.YyKeshi;
import com.fc.v2.model.auto.YyZhuanjia;
import com.fc.v2.model.auto.YyZhuanjiaExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院专家 YyZhuanjiaService
 * @Title: YyZhuanjiaService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:38  
 **/
@Service
public class YyZhuanjiaService implements BaseService<YyZhuanjia, YyZhuanjiaExample>{
	@Autowired
	private YyZhuanjiaMapper yyZhuanjiaMapper;
	@Autowired
	private YyKeshiMapper yyKeshiMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyZhuanjia> list(Tablepar tablepar,YyZhuanjia yyZhuanjia){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyZhuanjia> list= yyZhuanjiaMapper.getList(yyZhuanjia);
	        for (YyZhuanjia yyZhuanjia2 : list) {
	        	YyKeshi selectByPrimaryKey = yyKeshiMapper.selectByPrimaryKey(Long.valueOf(yyZhuanjia2.getKeshi()));
	        	if(selectByPrimaryKey!=null) {
	        		yyZhuanjia2.setKeshiname(selectByPrimaryKey.getName());
	        	}
	        	
			}
	        
	        PageInfo<YyZhuanjia> pageInfo = new PageInfo<YyZhuanjia>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyZhuanjiaExample example=new YyZhuanjiaExample();
			example.createCriteria().andIdIn(stringB);
			return yyZhuanjiaMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyZhuanjia selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyZhuanjiaMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyZhuanjia record) {
		return yyZhuanjiaMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyZhuanjia record) {
				
		record.setId(null);
		
				
		return yyZhuanjiaMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyZhuanjia record, YyZhuanjiaExample example) {
		
		return yyZhuanjiaMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyZhuanjia record, YyZhuanjiaExample example) {
		
		return yyZhuanjiaMapper.updateByExample(record, example);
	}

	@Override
	public List<YyZhuanjia> selectByExample(YyZhuanjiaExample example) {
		
		return yyZhuanjiaMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyZhuanjiaExample example) {
		
		return yyZhuanjiaMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyZhuanjiaExample example) {
		
		return yyZhuanjiaMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyZhuanjia
	 * @return
	 */
	@Transactional
	public int updateVisible(YyZhuanjia yyZhuanjia) {
		return yyZhuanjiaMapper.updateByPrimaryKeySelective(yyZhuanjia);
	}


    public int updateByPrimaryKey(YyZhuanjia yyZhuanjia) {
		return yyZhuanjiaMapper.updateByPrimaryKey(yyZhuanjia);
    }
}
