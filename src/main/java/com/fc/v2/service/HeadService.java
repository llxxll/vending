package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.HeadMapper;
import com.fc.v2.model.auto.Head;
import com.fc.v2.model.auto.HeadExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 负责人管理表 HeadService
 * @Title: HeadService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:37:59  
 **/
@Service
public class HeadService implements BaseService<Head, HeadExample>{
	@Autowired
	private HeadMapper headMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Head> list(Tablepar tablepar,Head head){
	        HeadExample testExample=new HeadExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(head);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Head> list= headMapper.selectByExample(testExample);
	        PageInfo<Head> pageInfo = new PageInfo<Head>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			HeadExample example=new HeadExample();
			example.createCriteria().andIdIn(stringB);
			return headMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Head selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return headMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Head record) {
		return headMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Head record) {
				
		record.setId(null);
		
				
		return headMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Head record, HeadExample example) {
		
		return headMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Head record, HeadExample example) {
		
		return headMapper.updateByExample(record, example);
	}

	@Override
	public List<Head> selectByExample(HeadExample example) {
		
		return headMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(HeadExample example) {
		
		return headMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(HeadExample example) {
		
		return headMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param head
	 * @return
	 */
	public int updateVisible(Head head) {
		return headMapper.updateByPrimaryKeySelective(head);
	}


}
