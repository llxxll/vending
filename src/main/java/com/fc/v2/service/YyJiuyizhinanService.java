package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyJiuyizhinanMapper;
import com.fc.v2.model.auto.YyJiuyizhinan;
import com.fc.v2.model.auto.YyJiuyizhinanExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 就医指南 YyJiuyizhinanService
 * @Title: YyJiuyizhinanService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-02-13 12:17:41  
 **/
@Service
public class YyJiuyizhinanService implements BaseService<YyJiuyizhinan, YyJiuyizhinanExample>{
	@Autowired
	private YyJiuyizhinanMapper yyJiuyizhinanMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyJiuyizhinan> list(Tablepar tablepar,YyJiuyizhinan yyJiuyizhinan){
	        YyJiuyizhinanExample testExample=new YyJiuyizhinanExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyJiuyizhinan);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyJiuyizhinan> list= yyJiuyizhinanMapper.selectByExample(testExample);
	        PageInfo<YyJiuyizhinan> pageInfo = new PageInfo<YyJiuyizhinan>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyJiuyizhinanExample example=new YyJiuyizhinanExample();
			example.createCriteria().andIdIn(stringB);
			return yyJiuyizhinanMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyJiuyizhinan selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyJiuyizhinanMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyJiuyizhinan record) {
		record.setUpdateTime(new Date());
		return yyJiuyizhinanMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyJiuyizhinan record) {
				
		record.setId(null);
		
				
		return yyJiuyizhinanMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyJiuyizhinan record, YyJiuyizhinanExample example) {
		
		return yyJiuyizhinanMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyJiuyizhinan record, YyJiuyizhinanExample example) {
		
		return yyJiuyizhinanMapper.updateByExample(record, example);
	}

	@Override
	public List<YyJiuyizhinan> selectByExample(YyJiuyizhinanExample example) {
		
		return yyJiuyizhinanMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyJiuyizhinanExample example) {
		
		return yyJiuyizhinanMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyJiuyizhinanExample example) {
		
		return yyJiuyizhinanMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyJiuyizhinan
	 * @return
	 */
	@Transactional
	public int updateVisible(YyJiuyizhinan yyJiuyizhinan) {
		return yyJiuyizhinanMapper.updateByPrimaryKeySelective(yyJiuyizhinan);
	}
	@Transactional
	public int updateStatusByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyJiuyizhinanMapper.updateStatusByPrimaryKey(aLong);
	}
	@Transactional
	public int shangjiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyJiuyizhinanMapper.shangjiaByPrimaryKey(aLong);
	}
	@Transactional
	public int xiajiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyJiuyizhinanMapper.xiajiaByPrimaryKey(aLong);
	}

    public int updateByPrimaryKey(YyJiuyizhinan yyJiuyizhinan) {
		return yyJiuyizhinanMapper.updateByPrimaryKey(yyJiuyizhinan);
    }
}
