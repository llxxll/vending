package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.PlanOutMapper;
import com.fc.v2.model.auto.PlanOut;
import com.fc.v2.model.auto.PlanOutExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 外协计划 PlanOutService
 * @Title: PlanOutService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:02  
 **/
@Service
public class PlanOutService implements BaseService<PlanOut, PlanOutExample>{
	@Autowired
	private PlanOutMapper planOutMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanOut> list(Tablepar tablepar,PlanOut planOut){
	        PlanOutExample testExample=new PlanOutExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(planOut);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanOut> list= planOutMapper.selectByExample(testExample);
	        PageInfo<PlanOut> pageInfo = new PageInfo<PlanOut>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			PlanOutExample example=new PlanOutExample();
			example.createCriteria().andIdIn(stringB);
			return planOutMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public PlanOut selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return planOutMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanOut record) {
		return planOutMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanOut record) {
				
		record.setId(null);
		
				
		return planOutMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanOut record, PlanOutExample example) {
		
		return planOutMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanOut record, PlanOutExample example) {
		
		return planOutMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanOut> selectByExample(PlanOutExample example) {
		
		return planOutMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanOutExample example) {
		
		return planOutMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanOutExample example) {
		
		return planOutMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planOut
	 * @return
	 */
	public int updateVisible(PlanOut planOut) {
		return planOutMapper.updateByPrimaryKeySelective(planOut);
	}


}
