package com.fc.v2.service;

import java.util.List;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshDepotMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanOudetailGongyingMapper;
import com.fc.v2.mapper.auto.PlanOudetailMapper;
import com.fc.v2.mapper.auto.PlanOustatementDetailMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsSonghuoMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanOudetailGongyingExample;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsSonghuo;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 委外单 PlanOudetailGongyingService
 * @Title: PlanOudetailGongyingService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:15  
 **/
@Service
@Transactional
public class PlanOudetailGongyingService implements BaseService<PlanOudetailGongying, PlanOudetailGongyingExample>{
	@Autowired
	private PlanOudetailGongyingMapper planOudetailGongyingMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private WmsSonghuoMapper wmsSonghuoMapper;
	@Autowired
	private JshDepotMapper jshDepotMapper;
	@Autowired
	private PlanOudetailMapper planOudetailMapper;
	@Autowired
	private PlanOustatementDetailMapper planOustatementDetailMapper;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanOudetailGongying> list(Tablepar tablepar,PlanOudetailGongying planOudetailGongying){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanOudetailGongying> list= planOudetailGongyingMapper.getList(planOudetailGongying);
	        for (PlanOudetailGongying planOudetailGongying2 : list) {
	        	TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planOudetailGongying2.getMaterialxxx()));
	        	planOudetailGongying2.setMaterialxxxName(selectByPrimaryKey.getNickname());
	        	String lingliaoku = planOudetailGongying2.getLingliaoku();
	        	if(lingliaoku!=null) {
	        		JshDepot selectByPrimaryKey2 = jshDepotMapper.selectByPrimaryKey(Long.valueOf(lingliaoku));
	        		if(selectByPrimaryKey2!=null) {
	        			planOudetailGongying2.setLingliaokuName(selectByPrimaryKey2.getName());
	        			
	        		}
	        	}
	        	
			}
	        PageInfo<PlanOudetailGongying> pageInfo = new PageInfo<PlanOudetailGongying>(list);
	        
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			int deleteByPrimaryKey =0;
			for (Long long1 : stringB) {
				PlanOudetailGongying selectByPrimaryKey = planOudetailGongyingMapper.selectByPrimaryKey(long1);
				if(selectByPrimaryKey.getField2().equals("1")) {
					if(!selectByPrimaryKey.getField1().equals("0")) {
						continue;
					}
				}else {
					if(!selectByPrimaryKey.getField1().equals("1")) {
						continue;
					}else {
						//查询 是否 存在送货
						List<WmsSonghuo> listByWXid = wmsSonghuoMapper.getListByWXid(selectByPrimaryKey.getId()+"");
						if(listByWXid.size()>0) {
							continue;
						}
					}
				}
				
				deleteByPrimaryKey= planOudetailGongyingMapper.deleteByPrimaryKey(selectByPrimaryKey.getId());
				if(deleteByPrimaryKey>0) {
					if(selectByPrimaryKey.getField2().equals("1")) {
						
						WmsWarehouseDeliveryDetail wms = new WmsWarehouseDeliveryDetail();
						wms.setHouseNo(selectByPrimaryKey.getStandard());
						wms.setUnitName(selectByPrimaryKey.getId()+"");
						wms.setQualityNo("1");
						WmsWarehouseDeliveryDetail wms2 =  wmsWarehouseDeliveryDetailMapper.getOnebyParams(wms);	
						deleteByPrimaryKey =wmsWarehouseDeliveryDetailMapper.deleteByPrimaryKey(wms2.getSid());
					}
				}
				
			}
		return deleteByPrimaryKey;
				
	}
	
	
	@Override
	public PlanOudetailGongying selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			PlanOudetailGongying planOudetailGongying2 = planOudetailGongyingMapper.selectByPrimaryKey(id1);
			return planOudetailGongying2;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanOudetailGongying record) {
		return planOudetailGongyingMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanOudetailGongying record) {
				
		record.setId(null);
		
				
		return planOudetailGongyingMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanOudetailGongying record, PlanOudetailGongyingExample example) {
		
		return planOudetailGongyingMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanOudetailGongying record, PlanOudetailGongyingExample example) {
		
		return planOudetailGongyingMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanOudetailGongying> selectByExample(PlanOudetailGongyingExample example) {
		
		return planOudetailGongyingMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanOudetailGongyingExample example) {
		
		return planOudetailGongyingMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanOudetailGongyingExample example) {
		
		return planOudetailGongyingMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planOudetailGongying
	 * @return
	 */
	public int updateVisible(PlanOudetailGongying planOudetailGongying) {
		
		return planOudetailGongyingMapper.updateByPrimaryKeySelective(planOudetailGongying);
	}

	public int songhuoSave(PlanOudetailGongying planOudetailGongying) {
		//获取委外单
		PlanOudetailGongying selectByPrimaryKey = planOudetailGongyingMapper.selectByPrimaryKey(planOudetailGongying.getId());
		
		if(selectByPrimaryKey.getField1().equals("0")||selectByPrimaryKey.getField1().equals("2")||selectByPrimaryKey.getField1().equals("4")) {
			
			return 4;
		}
		
		//判断委外单状态(不等3  更改状态为送货中)
		
		
		//添加入库记录
			
		
			//添加入库数据(入库数据)
			WmsWarehouseEntryDetail wmsWarehouseDeliveryDetail1 = new WmsWarehouseEntryDetail();
			wmsWarehouseDeliveryDetail1.setUnitName(selectByPrimaryKey.getId()+"");
			wmsWarehouseDeliveryDetail1.setDeliverySid(selectByPrimaryKey.getProduceNo());
			wmsWarehouseDeliveryDetail1.setHouseNo(planOudetailGongying.getStandard());
			wmsWarehouseDeliveryDetail1.setMatterNumber(Long.valueOf(planOudetailGongying.getCount()));
			wmsWarehouseDeliveryDetail1.setCreatedTime(new Date());
			wmsWarehouseDeliveryDetail1.setCreatedUserSid(ShiroUtils.getUserId());
			wmsWarehouseDeliveryDetail1.setQualityNo("1");
			wmsWarehouseDeliveryDetail1.setX(1);
			//获取委外单数据
			//PlanOutDetailGongying selectByPrimaryKey2 = planOutDetailGongyingMapperMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
			
			//获取委外计划数据
			PlanOudetail selectByPrimaryKey2 = planOudetailMapper.getOneBynO(selectByPrimaryKey.getProduceNo());
			wmsWarehouseDeliveryDetail1.setModifiedUserSid(Long.valueOf(selectByPrimaryKey2.getField3()));
			wmsWarehouseDeliveryDetail1.setMatterNo(selectByPrimaryKey.getMaterial());
			wmsWarehouseDeliveryDetail1.setMatterDesc(selectByPrimaryKey.getDepartmentOne());
			int insertSelective =wmsWarehouseEntryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail1);
			
			//获取入库数字
			Integer rukucount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey.getId()+"", "1",null);
			if(rukucount==null) {
				rukucount=0;
			}
			Integer lingcount =0;
			if(selectByPrimaryKey2.getField2().equals("0")) {
				//获取领料数据
				WmsWarehouseDeliveryDetail onebyUnitName = wmsWarehouseDeliveryDetailMapper.getOnebyUnitName(selectByPrimaryKey.getId()+"", "1");
				lingcount=onebyUnitName.getMatterNumber().intValue();
			}else {
				lingcount=selectByPrimaryKey.getCount();
			}
			if(rukucount>=lingcount) {
				//更改委外单状态
				selectByPrimaryKey.setField1("5");
			}else {
				selectByPrimaryKey.setField1("3");
				
			}
			
				
				insertSelective = planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
				
				if(selectByPrimaryKey2.getField1().equals("0")) {
					
					selectByPrimaryKey2.setField1("1");
					planOudetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey2);
				}
		
		
		
		return insertSelective;
	}

	public int tuiliaoSave(PlanOudetailGongying planOudetailGongying) {
		//获取委外单
				PlanOudetailGongying selectByPrimaryKey = planOudetailGongyingMapper.selectByPrimaryKey(planOudetailGongying.getId());
				
				if(selectByPrimaryKey.getField1().equals("0")||selectByPrimaryKey.getField1().equals("2")||selectByPrimaryKey.getField1().equals("4")) {
					
					return 4;
				}
				//根据工单类型
				//获取工单已入库数量
				Integer countbymattersc = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey.getId()+"", "1", null);
				if(countbymattersc==null) {
					countbymattersc=0;
				}
				//获取工单退料数量
				Integer countbymattersc2 = wmsWarehouseEntryDetailMapper.getCountbymattersc(selectByPrimaryKey.getId()+"", "5", null);
				if(countbymattersc2==null) {
					countbymattersc2=0;
				}
				Integer xxxx=0;
				if(selectByPrimaryKey.getField2().equals("1")) {
					Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByPrimaryKey.getId()+"", "1");
					if(countbyUnitName==null) {
						countbyUnitName=0;
					}
					xxxx=countbyUnitName;
				}else {
					Integer count = selectByPrimaryKey.getCount();
					xxxx=count;
				}
				Integer yyyy=xxxx-countbymattersc2-countbymattersc;
				if(yyyy<planOudetailGongying.getCount()) {
					return 3;
				}
				//添加入库数据(入库数据)
				WmsWarehouseEntryDetail wmsWarehouseDeliveryDetail1 = new WmsWarehouseEntryDetail();
				wmsWarehouseDeliveryDetail1.setMatterDesc(selectByPrimaryKey.getId()+"");
				wmsWarehouseDeliveryDetail1.setDeliverySid(selectByPrimaryKey.getStock()+"");
				wmsWarehouseDeliveryDetail1.setHouseNo(planOudetailGongying.getStandard());
				wmsWarehouseDeliveryDetail1.setMatterNumber(Long.valueOf(planOudetailGongying.getCount()));
				wmsWarehouseDeliveryDetail1.setCreatedTime(new Date());
				wmsWarehouseDeliveryDetail1.setCreatedUserSid(ShiroUtils.getUserId());
				wmsWarehouseDeliveryDetail1.setQualityNo("5");
				wmsWarehouseDeliveryDetail1.setX(1);
				//获取委外单数据
				//PlanOutDetailGongying selectByPrimaryKey2 = planOutDetailGongyingMapperMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
				
				//获取委外计划数据
				PlanOudetail selectByPrimaryKey2 = planOudetailMapper.selectByPrimaryKey(selectByPrimaryKey.getStock());
				wmsWarehouseDeliveryDetail1.setModifiedUserSid(Long.valueOf(selectByPrimaryKey.getDepartmentOne()));
				wmsWarehouseDeliveryDetail1.setMatterNo(selectByPrimaryKey2.getStandard());
				return wmsWarehouseEntryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail1);	
				
	}

	public int jiesuansave(PlanOudetailGongying planOudetailGongying) {
		//根据委外 单号获取委外单
		PlanOudetailGongying selectByPrimaryKey = planOudetailGongyingMapper.selectByPrimaryKey(planOudetailGongying.getId());
		if(!selectByPrimaryKey.getField1().equals("4")) {
			return 5;
		}
		PlanOudetail selectByPrimaryKey2 = planOudetailMapper.selectByPrimaryKey(selectByPrimaryKey.getStock());
		JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey2.getMateriaNo()));
		List<WmsSonghuo> wmssonghuo    =wmsSonghuoMapper.getListByWXid(selectByPrimaryKey.getId()+"");
		List<PlanOustatementDetail>  plistx  =planOustatementDetailMapper.getListBYwxID(selectByPrimaryKey.getId().intValue());
		if(plistx.size()>0) {
			
		for (PlanOustatementDetail planOustatementDetail : plistx) {
			planOustatementDetailMapper.deleteByPrimaryKey(planOustatementDetail.getId());
		}
		}
		int insertSelective=0;
		for (WmsSonghuo wmsSonghuo2 : wmssonghuo) {
			WmsWarehouseEntryDetail bypGid = wmsWarehouseEntryDetailMapper.getBypGid(wmsSonghuo2.getSid()+"", "1");
			if(bypGid==null) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return 4;
			}
			//根据 id 获取 委外结算
			PlanOustatementDetail ppp =planOustatementDetailMapper.selectOneBywsID(wmsSonghuo2.getSid()+"");
			if(ppp==null) {
				//插入一条新的
				ppp =new PlanOustatementDetail();
				//"委外单","物料号", "物料名称", "数量","不合格数量" ,"单价","不含税单价", "不含税总价" ,"供应商"
				
				ppp.setStatus(0);
				ppp.setPgId(Integer.valueOf(wmsSonghuo2.getUnitName()));
				ppp.setWorker(wmsSonghuo2.getSid()+"");
				ppp.setMaterialNo(selectByPrimaryKey3.getName());
				ppp.setMaterialName(selectByPrimaryKey3.getModel());
				ppp.setOrgName(selectByPrimaryKey.getDepartmentOne()+"");
				ppp.setCount(bypGid.getMatterNumber().intValue());
				ppp.setCreateTime(bypGid.getxTime());
				//不合格数
				ppp.setSpesc(wmsSonghuo2.getMatterNo());
				ppp.setUpdater(ShiroUtils.getUserId());
				ppp.setUpdateTime(new Date());
				//单价未完善 暂时不计算
				 insertSelective = planOustatementDetailMapper.insertSelective(ppp);
			}else {
				//更改 状态为 已结算
				ppp.setStatus(0);
				ppp.setUpdater(ShiroUtils.getUserId());
				ppp.setUpdateTime(new Date());
				ppp.setWorker(wmsSonghuo2.getSid()+"");
				insertSelective = planOustatementDetailMapper.updateByPrimaryKeySelective(ppp);
			}
		}
		
		if(insertSelective>0) {
			selectByPrimaryKey.setField1("2");
			selectByPrimaryKey.setUpdateTime(new Date());
			selectByPrimaryKey.setUpdater(ShiroUtils.getUserId());
			insertSelective = planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		}
		return insertSelective ;
	}

	public int batchjiesuan(String ids) {
		Long[] integers = ConvertUtil.toLongArray(",", ids);
		List<Long> stringB = Arrays.asList(integers);
		int insertSelective=0;
		for (Long long1 : stringB) {
			PlanOudetailGongying selectByPrimaryKey = planOudetailGongyingMapper.selectByPrimaryKey(long1);
			if(!selectByPrimaryKey.getField1().equals("4")) {
				continue;
			}
			PlanOudetail selectByPrimaryKey2 = planOudetailMapper.selectByPrimaryKey(selectByPrimaryKey.getStock());
			JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey2.getMateriaNo()));
			List<WmsSonghuo> wmssonghuo    =wmsSonghuoMapper.getListByWXid(selectByPrimaryKey.getId()+"");
			List<PlanOustatementDetail>  plistx  =planOustatementDetailMapper.getListBYwxID(selectByPrimaryKey.getId().intValue());
			if(plistx.size()>0) {
				
			for (PlanOustatementDetail planOustatementDetail : plistx) {
				planOustatementDetailMapper.deleteByPrimaryKey(planOustatementDetail.getId());
			}
			}
			
			for (WmsSonghuo wmsSonghuo2 : wmssonghuo) {
				WmsWarehouseEntryDetail bypGid = wmsWarehouseEntryDetailMapper.getBypGid(wmsSonghuo2.getSid()+"", "1");
				if(bypGid==null) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return 4;
				}
				//根据 id 获取 委外结算
				PlanOustatementDetail ppp =planOustatementDetailMapper.selectOneBywsID(wmsSonghuo2.getSid()+"");
				if(ppp==null) {
					//插入一条新的
					ppp =new PlanOustatementDetail();
					//"委外单","物料号", "物料名称", "数量","不合格数量" ,"单价","不含税单价", "不含税总价" ,"供应商"
					
					ppp.setStatus(0);
					ppp.setPgId(Integer.valueOf(wmsSonghuo2.getUnitName()));
					ppp.setWorker(wmsSonghuo2.getSid()+"");
					ppp.setMaterialNo(selectByPrimaryKey3.getName());
					ppp.setMaterialName(selectByPrimaryKey3.getModel());
					ppp.setOrgName(selectByPrimaryKey.getDepartmentOne()+"");
					ppp.setCount(bypGid.getMatterNumber().intValue());
					ppp.setCreateTime(bypGid.getxTime());
					//不合格数
					ppp.setSpesc(wmsSonghuo2.getMatterNo());
					ppp.setUpdater(ShiroUtils.getUserId());
					ppp.setUpdateTime(new Date());
					//单价未完善 暂时不计算
					 insertSelective = planOustatementDetailMapper.insertSelective(ppp);
				}else {
					//更改 状态为 已结算
					ppp.setStatus(0);
					ppp.setUpdater(ShiroUtils.getUserId());
					ppp.setUpdateTime(new Date());
					ppp.setWorker(wmsSonghuo2.getSid()+"");
					insertSelective = planOustatementDetailMapper.updateByPrimaryKeySelective(ppp);
				}
			}
			
			if(insertSelective>0) {
				selectByPrimaryKey.setField1("2");
				selectByPrimaryKey.setUpdateTime(new Date());
				selectByPrimaryKey.setUpdater(ShiroUtils.getUserId());
				insertSelective = planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
			}
		}
		return insertSelective;
	}

	public List<PlanOudetailGongying> excgetlist(PlanOudetailGongying ppm) {
		List<PlanOudetailGongying> list= planOudetailGongyingMapper.getList(ppm);
        for (PlanOudetailGongying planOudetailGongying2 : list) {
        	 if(planOudetailGongying2.getField1().equals("2")) {
        		//供应商
             	String departmentOne = planOudetailGongying2.getDepartmentOne();
             	if(departmentOne!=null) {
             		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(departmentOne));
             		planOudetailGongying2.setDepartmentOne(selectByPrimaryKey.getNickname());
             	}
             	// count2;不合格数
             	Integer count2 =wmsSonghuoMapper.getuncountBYpid(planOudetailGongying2.getId());
             	if(count2==null) {
             		count2=0;
             	}
             	planOudetailGongying2.setCount2(count2);
             	//获取结算数
             	Integer count = planOustatementDetailMapper.getcountBypGid(planOudetailGongying2.getId().intValue());
             	if(count==null) {
             		count=0;
             	}
             	planOudetailGongying2.setCount(count);
        	 }
        	
        	
		}
        return  list;
	}

	public List<PlanOudetailGongying> getListByIds(List<Long> stringB) {
		List<PlanOudetailGongying> x= new ArrayList<PlanOudetailGongying>();
		for (Long long1 : stringB) {
			PlanOudetailGongying planOudetailGongying2 = planOudetailGongyingMapper.selectByPrimaryKey(long1);
			if(planOudetailGongying2.getField1().equals("2")) {
        		//供应商
             	String departmentOne = planOudetailGongying2.getDepartmentOne();
             	if(departmentOne!=null) {
             		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(departmentOne));
             		planOudetailGongying2.setDepartmentOne(selectByPrimaryKey.getNickname());
             	}
             	// count2;不合格数
             	Integer count2 =wmsSonghuoMapper.getuncountBYpid(planOudetailGongying2.getId());
             	if(count2==null) {
             		count2=0;
             	}
             	planOudetailGongying2.setCount2(count2);
             	//获取结算数
             	Integer count = planOustatementDetailMapper.getcountBypGid(planOudetailGongying2.getId().intValue());
             	if(count==null) {
             		count=0;
             	}
             	planOudetailGongying2.setCount(count);
             	x.add(planOudetailGongying2);
        	 }
		}
		return x;
	}

	public int zhijianSave(PlanOudetailGongying planOudetailGongying) {
		PlanOudetailGongying planOudetailGongying2 = planOudetailGongyingMapper.selectByPrimaryKey(planOudetailGongying.getId());
		WmsSonghuo wmsSonghuo =wmsSonghuoMapper.getOneBYpid(planOudetailGongying2.getId());
		int insertSelective =0;
		if(wmsSonghuo!=null) {
			wmsSonghuo.setMatterNo(planOudetailGongying.getCount()+"");
			insertSelective = wmsSonghuoMapper.updateByPrimaryKeySelective(wmsSonghuo);
		}else {
			wmsSonghuo = new WmsSonghuo();
			wmsSonghuo.setMatterNo(planOudetailGongying.getCount()+"");
			wmsSonghuo.setDeliverySid(planOudetailGongying2.getProduceNo());
			wmsSonghuo.setUnitName(planOudetailGongying2.getId()+"");
			wmsSonghuo.setCreatedTime(new Date());
			wmsSonghuo.setCreatedUserSid(ShiroUtils.getUserId());
			insertSelective = wmsSonghuoMapper.insertSelective(wmsSonghuo);
		}
		PlanOudetail selectByPrimaryKey2 = planOudetailMapper.getOneBynO(wmsSonghuo.getDeliverySid());
		PlanOudetailGongying selectByPrimaryKey1 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(wmsSonghuo.getUnitName()));
		
		//获取委外单
		//获取退料数量
		Integer tuiliaoCount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","5",2);
		if(tuiliaoCount==null) {
			tuiliaoCount=0;
		}
		//获取入库数字
		Integer rukuCount = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","1",2);
		Integer rukuCount1 = wmsWarehouseEntryDetailMapper.getBypGidxx(selectByPrimaryKey1.getId()+"","1",3);
		if(rukuCount==null) {
			rukuCount=0;
		}
		if(rukuCount1==null) {
			rukuCount1=0;
		}
		//获取不合格数字
		Integer unCount =wmsSonghuoMapper.getcountBYpid(selectByPrimaryKey1.getId());
		Integer uuuuuCount =0;
		if(selectByPrimaryKey2.getField2().equals("0")) {
			Integer count = selectByPrimaryKey1.getCount();
			uuuuuCount =count-tuiliaoCount-rukuCount-unCount-rukuCount1;
		}else {
			//获取领料数量
			Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByPrimaryKey1.getId()+"","1");
			if(countbyUnitName==null) {
				countbyUnitName=0;
			}
			
			uuuuuCount =countbyUnitName-tuiliaoCount-rukuCount-unCount-rukuCount1;
		}
		if(uuuuuCount<=0) {
			//数字无问题 状态改为已完成
			selectByPrimaryKey1.setField1("4");
			planOudetailGongyingMapper.updateByPrimaryKeySelective(selectByPrimaryKey1);
		}
		
		
		
		return insertSelective;
	}


}
