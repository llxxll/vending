package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderLiushuiMapper;
import com.fc.v2.model.auto.OrderLiushui;
import com.fc.v2.model.auto.OrderLiushuiExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 *  OrderLiushuiService
 * @Title: OrderLiushuiService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-11 21:27:22  
 **/
@Service
public class OrderLiushuiService implements BaseService<OrderLiushui, OrderLiushuiExample>{
	@Autowired
	private OrderLiushuiMapper orderLiushuiMapper;
	
      	   	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OrderLiushui> list(Tablepar tablepar,OrderLiushui orderLiushui){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OrderLiushui> list= orderLiushuiMapper.getList(orderLiushui);
	        PageInfo<OrderLiushui> pageInfo = new PageInfo<OrderLiushui>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderLiushuiExample example=new OrderLiushuiExample();
			example.createCriteria().andIdIn(stringB);
			return orderLiushuiMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OrderLiushui selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderLiushuiMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OrderLiushui record) {
		return orderLiushuiMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OrderLiushui record) {
				
		record.setId(null);
		
				
		return orderLiushuiMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OrderLiushui record, OrderLiushuiExample example) {
		
		return orderLiushuiMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OrderLiushui record, OrderLiushuiExample example) {
		
		return orderLiushuiMapper.updateByExample(record, example);
	}

	@Override
	public List<OrderLiushui> selectByExample(OrderLiushuiExample example) {
		
		return orderLiushuiMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderLiushuiExample example) {
		
		return orderLiushuiMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderLiushuiExample example) {
		
		return orderLiushuiMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param orderLiushui
	 * @return
	 */
	public int updateVisible(OrderLiushui orderLiushui) {
		return orderLiushuiMapper.updateByPrimaryKeySelective(orderLiushui);
	}


}
