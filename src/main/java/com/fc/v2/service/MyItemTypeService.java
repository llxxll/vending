package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MyItemTypeMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.model.auto.MyItemType;
import com.fc.v2.model.auto.MyItemTypeExample;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 *  MyItemTypeService
 * @Title: MyItemTypeService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-10-16 22:58:25  
 **/
@Service
@Transactional
public class MyItemTypeService implements BaseService<MyItemType, MyItemTypeExample>{
	@Autowired
	private MyItemTypeMapper myItemTypeMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	
      	   	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MyItemType> list(Tablepar tablepar,MyItemType myItemType){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MyItemType> list= myItemTypeMapper.getList(myItemType);
	        for (MyItemType myItemType2 : list) {
	        	TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(myItemType2.getCreateUser());
	        	if(selectByPrimaryKey!=null) {
	        		myItemType2.setCreateUserName(selectByPrimaryKey.getNickname());
	        	}
			}
	        PageInfo<MyItemType> pageInfo = new PageInfo<MyItemType>(list);
	        return  pageInfo;
	 }
	 public List<MyItemType> getslist(MyItemType myItemType){
		 
		 List<MyItemType> list= myItemTypeMapper.getList(myItemType);
		 for (MyItemType myItemType2 : list) {
			 TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(myItemType2.getCreateUser());
			 if(selectByPrimaryKey!=null) {
				 myItemType2.setCreateUserName(selectByPrimaryKey.getNickname());
			 }
		 }
		 return  list;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			int deleteByPrimaryKey=0;
			for (Long long1 : stringB) {
				deleteByPrimaryKey = myItemTypeMapper.deleteByPrimaryKey(long1);
				
			}
		return deleteByPrimaryKey;
				
	}
	
	
	@Override
	public MyItemType selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return myItemTypeMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MyItemType record) {
		MyItemType recordx =myItemTypeMapper.selectByTypeName(record.getTypeName());
		if(recordx!=null) {
			return 5;
		}
		return myItemTypeMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MyItemType record) {
				//根据名称查询是否存在
		MyItemType recordx =myItemTypeMapper.selectByTypeName(record.getTypeName());
		if(recordx!=null) {
			return 5;
		}
		record.setId(null);
		record.setCreateTime(new Date());
		record.setCreateUser(ShiroUtils.getUserId());
				
		return myItemTypeMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MyItemType record, MyItemTypeExample example) {
		
		return myItemTypeMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MyItemType record, MyItemTypeExample example) {
		
		return myItemTypeMapper.updateByExample(record, example);
	}

	@Override
	public List<MyItemType> selectByExample(MyItemTypeExample example) {
		
		return myItemTypeMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MyItemTypeExample example) {
		
		return myItemTypeMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MyItemTypeExample example) {
		
		return myItemTypeMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param myItemType
	 * @return
	 */
	public int updateVisible(MyItemType myItemType) {
		return myItemTypeMapper.updateByPrimaryKeySelective(myItemType);
	}


}
