package com.fc.v2.service;

import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import com.fc.v2.mapper.auto.MdProducdetailMapper;
import com.fc.v2.mapper.auto.MdProducspecsMapper;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.auto.MdProducdetail;
import com.fc.v2.model.auto.MdProducspecs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MdProductMapper;
import com.fc.v2.model.auto.MdProduct;
import com.fc.v2.model.auto.MdProductExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 主数据-商品信息表 MdProductService
 * @Title: MdProductService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-05-21 23:28:15  
 **/
@Service
@Transactional
public class MdProductService implements BaseService<MdProduct, MdProductExample>{
	@Autowired
	private MdProductMapper mdProductMapper;
	@Autowired
	private MdProducdetailMapper mdProducdetailMapper;
	@Autowired
	private MdProducspecsMapper mdProducspecsMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MdProduct> list(Tablepar tablepar,MdProduct mdProduct){
	        MdProductExample testExample=new MdProductExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	//testExample.createCriteria().andLikeQuery(mdProduct);
				if(StringUtils.isNotEmpty(mdProduct.getProductTitle())){
					testExample.or().andProductTitleIsNotNull().andProductTitleLike(mdProduct.getProductTitle());
				}
				if(StringUtils.isNotEmpty(mdProduct.getSpuAssemble())){
					testExample.or().andSpuAssembleIsNotNull().andSpuAssembleLike(mdProduct.getSpuAssemble());
				}
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("sid ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MdProduct> list= mdProductMapper.selectByExample(testExample);
	        PageInfo<MdProduct> pageInfo = new PageInfo<MdProduct>(list);
	        return  pageInfo;
	 }
	 public PageInfo<MdProduct> getlist(Tablepar tablepar,MdProduct mdProduct){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MdProduct> list= mdProductMapper.getList(mdProduct);
	        for(MdProduct mdProduct1: list){
				List<MdProducdetail> mdProducdetails = mdProducdetailMapper.selectByBrandSid(mdProduct1.getSid());
				mdProduct1.setMdProducdetail(mdProducdetails);
			}
	        PageInfo<MdProduct> pageInfo = new PageInfo<MdProduct>(list);
	        return  pageInfo;
	 }
	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MdProductExample example=new MdProductExample();
			example.createCriteria().andSidIn(stringB);
			for(Long id : stringB){
				mdProducdetailMapper.deleteByBrandSid(id);
			}

			return mdProductMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MdProduct selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return mdProductMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MdProduct record) {
		MdProduct selectByPrimaryKey = mdProductMapper.selectByPrimaryKey(record.getSid());
		String productTitle = selectByPrimaryKey.getProductTitle();
		String productTitle2 = record.getProductTitle();
		if(!productTitle.equals(productTitle2)) {
		
			MdProduct selectByParams = mdProductMapper.selectByParams(productTitle2);
			if(selectByParams!=null) {
	
				return 3;
			}
		}
		int result = mdProductMapper.updateByPrimaryKeySelective(record);
		
		if(record.getMdProducdetail().size() > 0){
			Long sid = record.getSid();
			mdProducdetailMapper.deleteByBrandSid(sid);
			for(MdProducdetail mdProducdetail : record.getMdProducdetail()){
				mdProducdetail.setBrandSid(sid);
				mdProducdetailMapper.insertSelective(mdProducdetail);
			}
		}
		return result;
	}
	public int gongjiaSelective(MdProduct record) {
		
		int result = mdProductMapper.updateByPrimaryKeySelective(record);
		
		
		return result;
	}
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MdProduct record) {
				
		record.setSid(null);
		record.setCreatedTime(new Date());
		record.setCreatedUserSid(ShiroUtils.getUserId());
		MdProduct record1 =mdProductMapper.selectByParams(record.getProductTitle());
		if(record1==null) {
			if(record.getMdProducdetail().size() > 0){
				int i = mdProductMapper.insertSelective(record);
				Long sid = record.getSid();
				for(MdProducdetail mdProducdetail : record.getMdProducdetail()){
					mdProducdetail.setBrandSid(sid);
					mdProducdetailMapper.insertSelective(mdProducdetail);
				}
				return 1;
			}else{
				return 4;
			}
		}else {
			return 3;
		}
	}
	
	
	@Override
	public int updateByExampleSelective(MdProduct record, MdProductExample example) {
		
		return mdProductMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MdProduct record, MdProductExample example) {
		
		return mdProductMapper.updateByExample(record, example);
	}

	@Override
	public List<MdProduct> selectByExample(MdProductExample example) {
		
		return mdProductMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MdProductExample example) {
		
		return mdProductMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MdProductExample example) {
		
		return mdProductMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param mdProduct
	 * @return
	 */
	public int updateVisible(MdProduct mdProduct) {
		return mdProductMapper.updateByPrimaryKeySelective(mdProduct);
	}


    public List<MdProduct> getList(MdProduct mdProduct) {
		List<MdProduct> list= mdProductMapper.getList(mdProduct);
		return  list;
    }
	public Map<String,String> chaxunjiage(String productId, String lbidsids, String gugeids) {
		//根据商品iD
		Map<String,String> map = new HashMap<String,String>();
		MdProduct selectByPrimaryKey = mdProductMapper.selectByPrimaryKey(Long.valueOf(productId));
		String xxxpr="";
		Double x =0.0;
		if(!gugeids.trim().equals("")) {
			Long[] integers = ConvertUtil.toLongArray(",", gugeids);
			List<Long> stringB = Arrays.asList(integers);
			for (Long long1 : stringB) {
				MdProducspecs selectByPrimaryKey2 = mdProducspecsMapper.selectByPrimaryKey(long1);
				BigDecimal price = selectByPrimaryKey2.getPrice();
				if(price!=null) {
					BigDecimal price2 = selectByPrimaryKey2.getPrice();
					xxxpr+=price2+",";
					double doubleValue = price2.doubleValue();
					x +=doubleValue;
				}else {
					xxxpr+="0.000"+",";
				}
			}
		}
		String substring ="";
		if(!xxxpr.equals("")) {
			
			substring = xxxpr.substring(0, xxxpr.length()-1);
		}
		BigDecimal pricez = new BigDecimal(x);
		
		BigDecimal price = selectByPrimaryKey.getPrice();
		BigDecimal add =null;
		String xxx ="";
		if(price==null) {
			xxx ="0.000";
		}else {
			add= price.add(pricez);
			xxx=price+"";
		}
		map.put("productprice",xxx);
		map.put("spescprice", substring);
		map.put("zongprice", add.toString());
		return map;
	}
}
