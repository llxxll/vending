package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import java.util.Arrays;

import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanOudetailGongyingMapper;
import com.fc.v2.mapper.auto.PlanOudetailMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderDetailPzMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsWarehouseStockMapper;
import com.fc.v2.model.auto.*;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 出库单明细 WmsWarehouseDeliveryDetailService
 * @Title: WmsWarehouseDeliveryDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:28:43  
 **/
@Service
@Transactional
public class WmsWarehouseDeliveryDetailService implements BaseService<WmsWarehouseDeliveryDetail, WmsWarehouseDeliveryDetailExample>{
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private WmsWarehouseStockMapper wmsWarehouseStockMapper;
	@Autowired
	private PlanWorkOrderMapper planWorkOrderMapper;
	@Autowired
	private PlanWorkOrderDetailPzMapper planWorkOrderDetailPzMapper;
	@Autowired
	private PlanOudetailGongyingMapper planOudetailGongyingMapper;
	@Autowired
	private PlanOudetailMapper planOudetailMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private  TsysUserMapper  tsysUserMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<WmsWarehouseDeliveryDetail> list(Tablepar tablepar,WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		 PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		 List<WmsWarehouseDeliveryDetail> list= wmsWarehouseDeliveryDetailMapper.getList(wmsWarehouseDeliveryDetail);
		 
		 PageInfo<WmsWarehouseDeliveryDetail> pageInfo = new PageInfo<WmsWarehouseDeliveryDetail>(list);
		 
		 
		 return  pageInfo;
	 }
	 public PageInfo<WmsWarehouseDeliveryDetail> listweiwai(Tablepar tablepar,WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		 PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		 List<WmsWarehouseDeliveryDetail> list= wmsWarehouseDeliveryDetailMapper.getListlistweiwai(wmsWarehouseDeliveryDetail);
		 for (WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail2 : list) {
			 PlanOudetail planOudetail = planOudetailMapper.getOneBynO(wmsWarehouseDeliveryDetail2.getDeliverySid());
				 TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planOudetail.getField3()));
				 wmsWarehouseDeliveryDetail2.setLingliaoren(selectByPrimaryKey.getNickname());
		 }
		 
		 PageInfo<WmsWarehouseDeliveryDetail> pageInfo = new PageInfo<WmsWarehouseDeliveryDetail>(list);
		 
		 
		 return  pageInfo;
	 }
	 public PageInfo<WmsWarehouseDeliveryDetail> listchejian(Tablepar tablepar,WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		 PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		 List<WmsWarehouseDeliveryDetail> list= wmsWarehouseDeliveryDetailMapper.getListlistchejian(wmsWarehouseDeliveryDetail);
		 for (WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail2 : list) {
			 JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(wmsWarehouseDeliveryDetail2.getMatterNo()));
			 wmsWarehouseDeliveryDetail2.setChukuname(selectByPrimaryKey3.getName());
			 wmsWarehouseDeliveryDetail2.setChukuspesc(selectByPrimaryKey3.getModel());
			 //获取派工明细
			 PlanWorkOrderDetailPz selectOneByworno = planWorkOrderDetailPzMapper.selectOneByworno(wmsWarehouseDeliveryDetail2.getUnitName());
			 Integer worker = selectOneByworno.getWorker();
			 if(worker!=null) {
			 TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(worker));
				 
				 wmsWarehouseDeliveryDetail2.setLingliaoren(selectByPrimaryKey.getNickname());
			 }
		 }
		 
		 PageInfo<WmsWarehouseDeliveryDetail> pageInfo = new PageInfo<WmsWarehouseDeliveryDetail>(list);
		 
		 
		 return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			WmsWarehouseDeliveryDetailExample example=new WmsWarehouseDeliveryDetailExample();
			example.createCriteria().andSidIn(stringB);
			return wmsWarehouseDeliveryDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public WmsWarehouseDeliveryDetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			WmsWarehouseDeliveryDetail selectByPrimaryKey = wmsWarehouseDeliveryDetailMapper.selectByPrimaryKey(id1);
			
			/**
			 if(selectByPrimaryKey.getStockId()!=null) {
				 WmsWarehouseStock selectByPrimaryKey2 = wmsWarehouseStockMapper.selectByPrimaryKey(selectByPrimaryKey.getStockId());
				 JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey2.getMatterNo()));
				 selectByPrimaryKey.setStockName(selectByPrimaryKey3.getName());
				 selectByPrimaryKey.setStockNameModel(selectByPrimaryKey3.getModel());
				 selectByPrimaryKey.setStockNamegongyi(selectByPrimaryKey2.getXs());
				 selectByPrimaryKey.setStockNumber(selectByPrimaryKey2.getStockNumber()+"");
			 }
			*/
			
			return selectByPrimaryKey;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(WmsWarehouseDeliveryDetail record) {

		int updateByPrimaryKeySelective=0;
		//数量需小于库存
		WmsWarehouseStock selectByPrimaryKey2 = wmsWarehouseStockMapper.selectByPrimaryKey(Long.valueOf(record.getStockId()));
		if(selectByPrimaryKey2 == null){
			
			return 3;
		}
		if(selectByPrimaryKey2.getStockNumber() < record.getMatterNumber()){
			
			return 4;
		}
		
			//获取出库单 数据
			WmsWarehouseDeliveryDetail selectByPrimaryKey = wmsWarehouseDeliveryDetailMapper.selectByPrimaryKey(record.getSid());
			//原先的
			Long stockId2 = selectByPrimaryKey.getStockId();
			Long matterNumber2 = selectByPrimaryKey.getMatterNumber();
			
			
			Integer x = selectByPrimaryKey.getX();
			if(x==1) {

				//出库数量
				selectByPrimaryKey.setMatterNumber(record.getMatterNumber());
				//出库时间
				selectByPrimaryKey.setModifiedTime(new Date());
				//状态已出库
				selectByPrimaryKey.setX(2);
				//出库人
				selectByPrimaryKey.setModifiedUserSid(ShiroUtils.getUserId());
				//库存id
				selectByPrimaryKey.setStockId(record.getStockId());
				updateByPrimaryKeySelective = wmsWarehouseDeliveryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);

				if(updateByPrimaryKeySelective>0) {
					//修改库存数字
					Long stockNumber = selectByPrimaryKey2.getStockNumber();
					Long matterNumber = selectByPrimaryKey.getMatterNumber();
					Long matterNumberx =stockNumber - matterNumber;
					selectByPrimaryKey2.setStockNumber(matterNumberx);
					updateByPrimaryKeySelective = wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey2);
				}
				if(selectByPrimaryKey.getQualityNo().equals("2")) {
					
					//根据工单号变更状态
					PlanWorkOrder planWorkOrder = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getUnitName());
					planWorkOrder.setStatus(2);
					planWorkOrder.setCount(record.getMatterNumber().intValue());
					planWorkOrderMapper.updateByPrimaryKeySelective(planWorkOrder);
					PlanWorkOrderDetailPz planWorkOrderDetailPz = planWorkOrderDetailPzMapper.selectOneByworno(selectByPrimaryKey.getUnitName());
					planWorkOrderDetailPz.setStatus(2);
					planWorkOrderDetailPz.setCount(record.getMatterNumber().intValue());
					planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
				}else if(selectByPrimaryKey.getQualityNo().equals("1")) {
					PlanOudetailGongying planOudetailGongying = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
					planOudetailGongying.setField1("1");
					updateByPrimaryKeySelective = planOudetailGongyingMapper.updateByPrimaryKeySelective(planOudetailGongying);
				}
			}else {
				//出库数量
				selectByPrimaryKey.setMatterNumber(record.getMatterNumber());
				//库存id
				selectByPrimaryKey.setStockId(record.getStockId());
				
				updateByPrimaryKeySelective = wmsWarehouseDeliveryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);

				if(updateByPrimaryKeySelective>0) {
					//修改库存数字
					//原来的加上  
					WmsWarehouseStock selectByPrimaryKey3 = wmsWarehouseStockMapper.selectByPrimaryKey(stockId2);
					Long stockNumber2 = selectByPrimaryKey3.getStockNumber();
					selectByPrimaryKey3.setStockNumber(matterNumber2+stockNumber2);
					wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey3);
					
					//现在的减去
					Long stockNumber = selectByPrimaryKey2.getStockNumber();
					Long matterNumber = selectByPrimaryKey.getMatterNumber();
					Long matterNumberx =stockNumber - matterNumber;
					selectByPrimaryKey2.setStockNumber(matterNumberx);
					updateByPrimaryKeySelective = wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey2);
				}
				
					if(selectByPrimaryKey.getQualityNo().equals("2")) {
						//根据工单号变更状态
						PlanWorkOrder planWorkOrder = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getUnitName());
						Integer count2 = planWorkOrder.getCount();
						Integer intValue = record.getMatterNumber().intValue();
						planWorkOrder.setCount(record.getMatterNumber().intValue());
						planWorkOrderMapper.updateByPrimaryKeySelective(planWorkOrder);
						Integer xxx=intValue-count2;
						
					List<PlanWorkOrderDetailPz> listBYworkno = planWorkOrderDetailPzMapper.getListBYworkno(selectByPrimaryKey.getUnitName());
					for (PlanWorkOrderDetailPz planWorkOrderDetailPz2 : listBYworkno) {
						Integer count = planWorkOrderDetailPz2.getCount();
							
							planWorkOrderDetailPz2.setCount(count+xxx);
						planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz2);
				}
					}
				
			}
		
		return updateByPrimaryKeySelective;
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(WmsWarehouseDeliveryDetail record) {
				
		record.setSid(null);
		
				
		return wmsWarehouseDeliveryDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(WmsWarehouseDeliveryDetail record, WmsWarehouseDeliveryDetailExample example) {
		
		return wmsWarehouseDeliveryDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(WmsWarehouseDeliveryDetail record, WmsWarehouseDeliveryDetailExample example) {
		
		return wmsWarehouseDeliveryDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<WmsWarehouseDeliveryDetail> selectByExample(WmsWarehouseDeliveryDetailExample example) {
		
		return wmsWarehouseDeliveryDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(WmsWarehouseDeliveryDetailExample example) {
		
		return wmsWarehouseDeliveryDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(WmsWarehouseDeliveryDetailExample example) {
		
		return wmsWarehouseDeliveryDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param wmsWarehouseDeliveryDetail
	 * @return
	 */
	public int updateVisible(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail) {
		return wmsWarehouseDeliveryDetailMapper.updateByPrimaryKeySelective(wmsWarehouseDeliveryDetail);
	}


	public Integer getCountByStatus(Integer status) {
		// TODO Auto-generated method stub
		return wmsWarehouseDeliveryDetailMapper.getCountByStatus(status);
	}

	public Integer getCountbyUnitName(String unitName,String qualityNo) {
		// TODO Auto-generated method stub
		return wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(unitName,qualityNo);
	}
	public Integer getCountbydevlied(String unitName,String qualityNo) {
		//获取 所有车加工单的 领料数量之和
		// TODO Auto-generated method stub
		return wmsWarehouseDeliveryDetailMapper.getCountbydevliedByOrderType(unitName, qualityNo, "0");
	}
	public int savechuku(WmsWarehouseDeliveryDetail record) {
		int updateByPrimaryKeySelective=0;
		//获取出库单 数据
		WmsWarehouseDeliveryDetail selectByPrimaryKey = wmsWarehouseDeliveryDetailMapper.selectByPrimaryKey(record.getSid());
		Integer x = selectByPrimaryKey.getX();
		if(x!=1) {
			return 5;
		}
		//数量需小于库存
		//根据 名称和类型
		WmsWarehouseStock  wmsWarehouseStock = new WmsWarehouseStock();
		wmsWarehouseStock.setMatterNo(selectByPrimaryKey.getMatterNo());
		wmsWarehouseStock.setXs(record.getStockNamegongyi());
		wmsWarehouseStock.setHouseNo(selectByPrimaryKey.getHouseNo());
		
		WmsWarehouseStock oneByParams = wmsWarehouseStockMapper.getOneByParams(wmsWarehouseStock);
		if(oneByParams==null) {
			oneByParams= new WmsWarehouseStock();
			oneByParams.setMatterNo(selectByPrimaryKey.getMatterNo());
			oneByParams.setXs(record.getStockNamegongyi());
			oneByParams.setHouseNo(selectByPrimaryKey.getHouseNo());
			oneByParams.setStockNumber(0l);
			wmsWarehouseStockMapper.insertSelective(oneByParams);
			
		}
				//出库数量
				selectByPrimaryKey.setMatterNumber(record.getMatterNumber());
				//出库时间
				selectByPrimaryKey.setModifiedTime(new Date());
				//状态已出库
				selectByPrimaryKey.setX(2);
				//出库人
				selectByPrimaryKey.setModifiedUserSid(ShiroUtils.getUserId());
				//库存id
				selectByPrimaryKey.setStockId(oneByParams.getSid());
				updateByPrimaryKeySelective = wmsWarehouseDeliveryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);

				if(updateByPrimaryKeySelective>0) {
					//修改库存数字
					Long stockNumber = oneByParams.getStockNumber();
					Long matterNumber = selectByPrimaryKey.getMatterNumber();
					Long matterNumberx =stockNumber - matterNumber;
					if(matterNumberx<0) {
						matterNumberx=0l;
					}
					oneByParams.setStockNumber(matterNumberx);
					updateByPrimaryKeySelective = wmsWarehouseStockMapper.updateByPrimaryKeySelective(oneByParams);
				}
				
				
				if(selectByPrimaryKey.getQualityNo().equals("2")) {
					
					//根据工单号变更状态
					PlanWorkOrder planWorkOrder = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getUnitName());
					planWorkOrder.setStatus(2);
					//planWorkOrder.setCount(record.getMatterNumber().intValue());
					planWorkOrderMapper.updateByPrimaryKeySelective(planWorkOrder);
					PlanWorkOrderDetailPz planWorkOrderDetailPz = planWorkOrderDetailPzMapper.selectOneByworno(selectByPrimaryKey.getUnitName());
					planWorkOrderDetailPz.setStatus(2);
					planWorkOrderDetailPz.setCount(record.getMatterNumber().intValue());
					planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
				}else if(selectByPrimaryKey.getQualityNo().equals("1")) {
					PlanOudetailGongying planOudetailGongying = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
					planOudetailGongying.setField1("1");
					updateByPrimaryKeySelective = planOudetailGongyingMapper.updateByPrimaryKeySelective(planOudetailGongying);
				}
		
		return updateByPrimaryKeySelective;
	}
	public int editSave(WmsWarehouseDeliveryDetail record) {
		int updateByPrimaryKeySelective=0;
		WmsWarehouseDeliveryDetail selectByPrimaryKey = wmsWarehouseDeliveryDetailMapper.selectByPrimaryKey(record.getSid());
		String qualityNo = selectByPrimaryKey.getQualityNo();
		if(qualityNo.equals("1")) {
			PlanOudetailGongying selectByPrimaryKey2 = planOudetailGongyingMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getUnitName()));
			if(selectByPrimaryKey2.getField1().equals("2")||selectByPrimaryKey2.getField1().equals("4")||selectByPrimaryKey2.getField1().equals("5")) {
				return 4;
			}
		}else if(qualityNo.equals("2")) {
			PlanWorkOrder selectOneByworno = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getUnitName());
			if(selectOneByworno.getStatus()==3||selectOneByworno.getStatus()==4) {
				return 3;
			}
		}
		
		
		
		//获取出库单 数据
		Long xianzai = record.getMatterNumber();
				Long yuanlai = selectByPrimaryKey.getMatterNumber();
				selectByPrimaryKey.setMatterNumber(record.getMatterNumber());
				
				updateByPrimaryKeySelective = wmsWarehouseDeliveryDetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);

				if(updateByPrimaryKeySelective>0) {
					//修改库存数字
					//原来的加上  
					Long stockId = selectByPrimaryKey.getStockId();
					if(stockId!=null) {
						WmsWarehouseStock selectByPrimaryKey3 = wmsWarehouseStockMapper.selectByPrimaryKey(stockId);
						Long stockNumber2 = selectByPrimaryKey3.getStockNumber();
						Long xxxx =yuanlai+stockNumber2-xianzai;
						if(xxxx<0) {
							xxxx=0l;
						}
						selectByPrimaryKey3.setStockNumber(xxxx);
						wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey3);
						
					}
					
					
				}
				
					if(selectByPrimaryKey.getQualityNo().equals("2")) {
						//根据工单号变更状态
						Integer xxx=xianzai.intValue()-yuanlai.intValue();
						
					List<PlanWorkOrderDetailPz> listBYworkno = planWorkOrderDetailPzMapper.getListBYworkno(selectByPrimaryKey.getUnitName());
					for (PlanWorkOrderDetailPz planWorkOrderDetailPz2 : listBYworkno) {
						Integer count = planWorkOrderDetailPz2.getCount();
							
							planWorkOrderDetailPz2.setCount(count+xxx);
						planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz2);
				}
					}
				
				return updateByPrimaryKeySelective;
	}
}
