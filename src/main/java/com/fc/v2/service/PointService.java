package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.PointMapper;
import com.fc.v2.model.auto.Point;
import com.fc.v2.model.auto.PointExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 点位管理表 PointService
 * @Title: PointService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:36:52  
 **/
@Service
public class PointService implements BaseService<Point, PointExample>{
	@Autowired
	private PointMapper pointMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Point> list(Tablepar tablepar,Point point){
	        PointExample testExample=new PointExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(point);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Point> list= pointMapper.selectByExample(testExample);
	        PageInfo<Point> pageInfo = new PageInfo<Point>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			PointExample example=new PointExample();
			example.createCriteria().andIdIn(stringB);
			return pointMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Point selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return pointMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Point record) {
		return pointMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Point record) {
				
		record.setId(null);
		
				
		return pointMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Point record, PointExample example) {
		
		return pointMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Point record, PointExample example) {
		
		return pointMapper.updateByExample(record, example);
	}

	@Override
	public List<Point> selectByExample(PointExample example) {
		
		return pointMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PointExample example) {
		
		return pointMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PointExample example) {
		
		return pointMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param point
	 * @return
	 */
	public int updateVisible(Point point) {
		return pointMapper.updateByPrimaryKeySelective(point);
	}


}
