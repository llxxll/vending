package com.fc.v2.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.fc.v2.model.auto.OrderReimbursemendetail;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderReimbursementMapper;
import com.fc.v2.model.auto.OrderReimbursement;
import com.fc.v2.model.auto.OrderReimbursementExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 报销表 OrderReimbursementService
 * @Title: OrderReimbursementService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-09-18 21:11:58  
 **/
@Service
public class OrderReimbursementService implements BaseService<OrderReimbursement, OrderReimbursementExample>{
	@Autowired
	private OrderReimbursementMapper orderReimbursementMapper;
	@Autowired
	private OrderReimbursemendetailService orderReimbursemendetailService;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OrderReimbursement> list(Tablepar tablepar,OrderReimbursement orderReimbursement){
	        OrderReimbursementExample testExample=new OrderReimbursementExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(orderReimbursement);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("sid ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        //List<OrderReimbursement> list= orderReimbursementMapper.selectByExample(testExample);
		 	List<OrderReimbursement> list= orderReimbursementMapper.getList(orderReimbursement);
	        PageInfo<OrderReimbursement> pageInfo = new PageInfo<OrderReimbursement>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderReimbursementExample example=new OrderReimbursementExample();
			example.createCriteria().andSidIn(stringB);
			return orderReimbursementMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OrderReimbursement selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderReimbursementMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OrderReimbursement record) {
		orderReimbursemendetailService.deleteByOrderNo(record.getOrderNo());
		List<OrderReimbursemendetail> detailList = record.getDetailList();
		for (OrderReimbursemendetail orderReimbursemendetail: detailList) {
			OrderReimbursemendetail orderReimbursemendetailNew = new OrderReimbursemendetail();
			orderReimbursemendetailNew.setReiMoney(orderReimbursemendetail.getStr1());
			orderReimbursemendetailNew.setReiType(orderReimbursemendetail.getStr2());
			orderReimbursemendetailNew.setMoneyDetail(orderReimbursemendetail.getStr3());
			orderReimbursemendetailNew.setReiSid(record.getOrderNo());
			orderReimbursemendetailService.insertSelective(orderReimbursemendetailNew);
		}
		return orderReimbursementMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OrderReimbursement record) {
				
		record.setSid(null);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String format = simpleDateFormat.format(new Date());
		record.setCreatedUserSid(ShiroUtils.getUserId());
		record.setOrderNo(ShiroUtils.getUserId()+"-"+format);
		List<OrderReimbursemendetail> detailList = record.getDetailList();
		for (OrderReimbursemendetail orderReimbursemendetail: detailList) {
			OrderReimbursemendetail orderReimbursemendetailNew = new OrderReimbursemendetail();
			orderReimbursemendetailNew.setReiMoney(orderReimbursemendetail.getStr1());
			orderReimbursemendetailNew.setReiType(orderReimbursemendetail.getStr2());
			orderReimbursemendetailNew.setMoneyDetail(orderReimbursemendetail.getStr3());
			orderReimbursemendetailNew.setReiSid(record.getOrderNo());
			orderReimbursemendetailService.insertSelective(orderReimbursemendetailNew);
		}
		return orderReimbursementMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OrderReimbursement record, OrderReimbursementExample example) {
		
		return orderReimbursementMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OrderReimbursement record, OrderReimbursementExample example) {
		
		return orderReimbursementMapper.updateByExample(record, example);
	}

	@Override
	public List<OrderReimbursement> selectByExample(OrderReimbursementExample example) {
		
		return orderReimbursementMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderReimbursementExample example) {
		
		return orderReimbursementMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderReimbursementExample example) {
		
		return orderReimbursementMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param orderReimbursement
	 * @return
	 */
	public int updateVisible(OrderReimbursement orderReimbursement) {
		return orderReimbursementMapper.updateByPrimaryKeySelective(orderReimbursement);
	}


}
