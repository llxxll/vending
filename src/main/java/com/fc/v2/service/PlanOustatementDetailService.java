package com.fc.v2.service;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.PlanOustatementDetailMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.PlanOustatementDetailExample;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 委外单结算 PlanOustatementDetailService
 * @Title: PlanOustatementDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:33  
 **/
@Service
@Transactional
public class PlanOustatementDetailService implements BaseService<PlanOustatementDetail, PlanOustatementDetailExample>{
	@Autowired
	private PlanOustatementDetailMapper planOustatementDetailMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanOustatementDetail> list(Tablepar tablepar,PlanOustatementDetail planOustatementDetail){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanOustatementDetail> list= planOustatementDetailMapper.getList(planOustatementDetail);
	        for (PlanOustatementDetail planOustatementDetail2 : list) {
				//
	        	TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(planOustatementDetail2.getUpdater());
	        	TsysUser selectByPrimaryKey2 = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planOustatementDetail2.getOrgName()));
	        	if(selectByPrimaryKey!=null) {
	        		planOustatementDetail2.setJiesuanName(selectByPrimaryKey.getNickname());
	        	}
	        	if(selectByPrimaryKey2!=null) {
	        		planOustatementDetail2.setOrggName(selectByPrimaryKey2.getNickname());
	        		
	        	}
			}
	        PageInfo<PlanOustatementDetail> pageInfo = new PageInfo<PlanOustatementDetail>(list);
		/*
		 * List<Integer> listsx = new ArrayList<Integer>(); List<Long> lists =
		 * planOustatementDetailMapper.selcet(); for (Long long1 : lists) { //查询
		 * List<PlanOustatementDetail> listBYwxID =
		 * planOustatementDetailMapper.getListBYwxID(long1.intValue()); for
		 * (PlanOustatementDetail planOustatementDetailx : listBYwxID) {
		 * if(planOustatementDetailx.getStatus()==0) {
		 * listsx.add(planOustatementDetailx.getPgId()); continue; } } }
		 */
	        
	        return  pageInfo;
	 }
	 public List<PlanOustatementDetail> excgetlist(PlanOustatementDetail planOustatementDetail){
	       
	        List<PlanOustatementDetail> list= planOustatementDetailMapper.getList(planOustatementDetail);
	        for (PlanOustatementDetail planOustatementDetail2 : list) {
				//
	        	TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(planOustatementDetail2.getUpdater());
	        	TsysUser selectByPrimaryKey2 = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planOustatementDetail2.getOrgName()));
	        	if(selectByPrimaryKey!=null) {
	        		planOustatementDetail2.setJiesuanName(selectByPrimaryKey.getNickname());
	        	}
	        	if(selectByPrimaryKey2!=null) {
	        		planOustatementDetail2.setOrggName(selectByPrimaryKey2.getNickname());
	        		
	        	}
			}
	        return  list;
	 }
	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			PlanOustatementDetailExample example=new PlanOustatementDetailExample();
			example.createCriteria().andIdIn(stringB);
			return planOustatementDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public PlanOustatementDetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return planOustatementDetailMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanOustatementDetail record) {
		return planOustatementDetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanOustatementDetail record) {
				
		record.setId(null);
		
				
		return planOustatementDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanOustatementDetail record, PlanOustatementDetailExample example) {
		
		return planOustatementDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanOustatementDetail record, PlanOustatementDetailExample example) {
		
		return planOustatementDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanOustatementDetail> selectByExample(PlanOustatementDetailExample example) {
		
		return planOustatementDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanOustatementDetailExample example) {
		
		return planOustatementDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanOustatementDetailExample example) {
		
		return planOustatementDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planOustatementDetail
	 * @return
	 */
	public int updateVisible(PlanOustatementDetail planOustatementDetail) {
		return planOustatementDetailMapper.updateByPrimaryKeySelective(planOustatementDetail);
	}
	public List<PlanOustatementDetail> getListByIds(List<Long> stringB) {
		List<PlanOustatementDetail> listByIds = planOustatementDetailMapper.getListByIds(stringB);
		
		for (PlanOustatementDetail planOustatementDetail2 : listByIds) {
			//
        	TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(planOustatementDetail2.getUpdater());
        	TsysUser selectByPrimaryKey2 = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planOustatementDetail2.getOrgName()));
        	if(selectByPrimaryKey!=null) {
        		planOustatementDetail2.setJiesuanName(selectByPrimaryKey.getNickname());
        	}
        	if(selectByPrimaryKey2!=null) {
        		planOustatementDetail2.setOrggName(selectByPrimaryKey2.getNickname());
        		
        	}
		}
		return listByIds;
	}


}
