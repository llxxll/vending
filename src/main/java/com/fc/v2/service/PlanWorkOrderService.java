package com.fc.v2.service;

import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshDepotMapper;
import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanProduceMaterialDetailMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderDetailPzMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderPzStatementMapper;
import com.fc.v2.mapper.auto.SysDepartmentMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.model.auto.PlanWorkOrderDetailPz;
import com.fc.v2.model.auto.PlanWorkOrderExample;
import com.fc.v2.model.auto.PlanWorkOrderPzStatement;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 工单 PlanWorkOrderService
 * @Title: PlanWorkOrderService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:27:47  
 **/
@Service
@Transactional
public class PlanWorkOrderService implements BaseService<PlanWorkOrder, PlanWorkOrderExample>{
	@Autowired
	private PlanWorkOrderMapper planWorkOrderMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private JshDepotMapper jshDepotMapper;
	@Autowired
	private PlanProduceMaterialDetailMapper planProduceMaterialDetailMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private PlanWorkOrderDetailPzMapper planWorkOrderDetailPzMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wWmsWarehouseDeliveryDetailMapper;
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;
	@Autowired
	private PlanWorkOrderPzStatementMapper planWorkOrderPzStatementMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private SysDepartmentMapper sysDepartmentMapper;
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarfpriceMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanWorkOrder> list(Tablepar tablepar,PlanWorkOrder planWorkOrder){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanWorkOrder> list= planWorkOrderMapper.getList(planWorkOrder);
	        for (PlanWorkOrder planWorkOrder2 : list) {
	        	//根据计划id 查询计划
	        	//PlanProduceMaterialDetail selectByPrimaryKey3 = planProduceMaterialDetailMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getNo()));
	        	
				
	        	//根据材料id 查询材料
	        	JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getField2()));
	        	planWorkOrder2.setClName(selectByPrimaryKey.getName());
	        	planWorkOrder2.setClproperty(selectByPrimaryKey.getModel());
	        	if(planWorkOrder2.getField1()!=null) {
	        		JshDepot selectByPrimaryKey2 = jshDepotMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getField1()));
		        	planWorkOrder2.setHousename(selectByPrimaryKey2.getName());
	        	}
	        	
	        	
	        	//根据工单号 获取出库数量
	        	WmsWarehouseDeliveryDetail wms = new WmsWarehouseDeliveryDetail();
	        	wms.setHouseNo(planWorkOrder2.getField1());
	        	wms.setUnitName(planWorkOrder2.getWorkerOrderNo());
	        	WmsWarehouseDeliveryDetail wms2 =  wmsWarehouseDeliveryDetailMapper.getOnebyParams(wms);
	        	if(wms2!=null) {
	        		planWorkOrder2.setLingliaoCount(wms2.getMatterNumber()+"");
	        	}
	        		
			}
	        PageInfo<PlanWorkOrder> pageInfo = new PageInfo<PlanWorkOrder>(list);
	        return  pageInfo;
	 }

		public PageInfo<PlanWorkOrder> orderlist(Tablepar tablepar, PlanWorkOrder planWorkOrder) {
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String format2 = format.format(cal.getTime());
			String format3 =format2+" 23:59:59";
			planWorkOrder.setEndTime(format3);
			PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanWorkOrder> list= planWorkOrderMapper.orderlist(planWorkOrder);
	        for (PlanWorkOrder planWorkOrder2 : list) {
	        	//根据计划id 查询计划
	        	//PlanProduceMaterialDetail selectByPrimaryKey3 = planProduceMaterialDetailMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getNo()));
	        	
				
	        	//根据材料id 查询材料
	        	JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getField2()));
	        	planWorkOrder2.setClName(selectByPrimaryKey.getName());
	        	planWorkOrder2.setClproperty(selectByPrimaryKey.getModel());
	        	
	        	JshDepot selectByPrimaryKey2 = jshDepotMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getField1()));
	        	planWorkOrder2.setHousename(selectByPrimaryKey2.getName());
	        	
	        	//根据工单号 获取出库数量
	        	WmsWarehouseDeliveryDetail wms = new WmsWarehouseDeliveryDetail();
	        	wms.setHouseNo(planWorkOrder2.getField1());
	        	wms.setUnitName(planWorkOrder2.getWorkerOrderNo());
	        	WmsWarehouseDeliveryDetail wms2 =  wmsWarehouseDeliveryDetailMapper.getOnebyParams(wms);
	        	if(wms2!=null) {
	        		planWorkOrder2.setLingliaoCount(wms2.getMatterNumber()+"");
	        	}
	        		
			}
	        PageInfo<PlanWorkOrder> pageInfo = new PageInfo<PlanWorkOrder>(list);
	        return  pageInfo;
		}
	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			int deleteByPrimaryKey = 0;
			for (Long long1 : stringB) {
				PlanWorkOrder selectByPrimaryKey = planWorkOrderMapper.selectByPrimaryKey(long1);
				if(selectByPrimaryKey.getStatus()==1) {
					deleteByPrimaryKey = planWorkOrderMapper.deleteByPrimaryKey(long1);
					if(deleteByPrimaryKey>0) {
						//删除派工明细
						PlanWorkOrderDetailPz planWorkOrderDetailPz = planWorkOrderDetailPzMapper.selectOneByworno(selectByPrimaryKey.getWorkerOrderNo());
						planWorkOrderDetailPzMapper.deleteByPrimaryKey(planWorkOrderDetailPz.getId());
						
						WmsWarehouseDeliveryDetail wms = new WmsWarehouseDeliveryDetail();
				    	wms.setHouseNo(selectByPrimaryKey.getField1());
				    	wms.setUnitName(selectByPrimaryKey.getWorkerOrderNo());
				    	wms.setQualityNo("2");
				    	WmsWarehouseDeliveryDetail wms2 =  wmsWarehouseDeliveryDetailMapper.getOnebyParams(wms);	
				    	deleteByPrimaryKey =wmsWarehouseDeliveryDetailMapper.deleteByPrimaryKey(wms2.getSid());
					}
				}
			}
			
			return deleteByPrimaryKey;
		
				
	}
	
	
	@Override
	public PlanWorkOrder selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			PlanWorkOrder planWorkOrder2 = planWorkOrderMapper.selectByPrimaryKey(id1);
			
			//根据物料id 获取物料
			JshMaterial selectByPrimaryKey3 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getMaterialNo()));
        	planWorkOrder2.setTuhao(selectByPrimaryKey3.getName());
        	planWorkOrder2.setTuName(selectByPrimaryKey3.getModel());
			
			
			//根据材料ID 查询材料
			JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getField2()));
        	planWorkOrder2.setClName(selectByPrimaryKey.getName());
        	planWorkOrder2.setClproperty(selectByPrimaryKey.getModel());
        	JshDepot selectByPrimaryKey2 = jshDepotMapper.selectByPrimaryKey(Long.valueOf(planWorkOrder2.getField1()));
        	//根据 出库记录查询 仓库名称
        	planWorkOrder2.setHousename(selectByPrimaryKey2.getName());
        	
        	//根据工单号 获取出库数量
        	WmsWarehouseDeliveryDetail wms = new WmsWarehouseDeliveryDetail();
        	wms.setHouseNo(planWorkOrder2.getField1());
        	wms.setUnitName(planWorkOrder2.getWorkerOrderNo());
        	WmsWarehouseDeliveryDetail wms2 =  wmsWarehouseDeliveryDetailMapper.getOnebyParams(wms);
        	planWorkOrder2.setLingliaoCount(wms2.getMatterNumber()+"");
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			if(wms2.getModifiedTime()!=null) {
				Calendar ca = Calendar.getInstance();
				ca.setTime(wms2.getModifiedTime());
				//ca.add(Calendar.HOUR, 8);
				String format = sf.format(ca.getTime());
	        	planWorkOrder2.setLingliaoTime(format);
			}
			
        	planWorkOrder2.setLingliaoUser(wms2.getRukuName());
        	planWorkOrder2.setChukclName(wms2.getChukuname());
        	planWorkOrder2.setChukclproperty(wms2.getChukuspesc());
        	
        	
        	
        	//获取领料数据
    		Integer countling = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(planWorkOrder2.getWorkerOrderNo(), "2");
    		if(countling==null) {
    			planWorkOrder2.setYichangyuanyin("无领料数据,请谨慎结算");
    		}else {
    			//获取退料数据
        		Integer counttui = wmsWarehouseEntryDetailMapper.getCountbymattersc(planWorkOrder2.getWorkerOrderNo(), "5", null);
        		if(counttui==null) {
        			counttui=0;
        		}
        		Integer countbyruku = wmsWarehouseEntryDetailMapper.getCountbymattersc(planWorkOrder2.getWorkerOrderNo(), "2", 2);
        		if(countbyruku==null) {
        			countbyruku=0;
        		}
        		Integer countxxx=countling-counttui;
        		//先检测数据
        		Integer countwo=0;
        		List<Map<String, BigDecimal>> countGroupBycarfid = planWorkOrderDetailPzMapper.getCountGroupBycarfid(planWorkOrder2.getWorkerOrderNo());
        		for (Map<String, BigDecimal> map : countGroupBycarfid) {
        			Integer uncount = map.get("uncount").intValue();
        			countwo=uncount++;
        			
        		}
        		Integer xxxx=countxxx-countwo-countbyruku;
        		if(xxxx!=0) {
        			
        			planWorkOrder2.setYichangyuanyin("数量误差: -------请谨慎结算");
        		}else {
        			planWorkOrder2.setYichangyuanyin("数量正常");
        		}
    		}
    		
        	
			return planWorkOrder2;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanWorkOrder record) {
		PlanWorkOrder selectByPrimaryKey = planWorkOrderMapper.selectByPrimaryKey(record.getId());
		if(selectByPrimaryKey.getStatus()!=1) {
			return 0;
		}
		//根据工单号 获取出库记录
		WmsWarehouseDeliveryDetail wms = new WmsWarehouseDeliveryDetail();
		wms.setHouseNo(selectByPrimaryKey.getField1());
		wms.setUnitName(selectByPrimaryKey.getWorkerOrderNo());
		WmsWarehouseDeliveryDetail wms2 =  wmsWarehouseDeliveryDetailMapper.getOnebyParams(wms);
		
		PlanProduceMaterialDetail planProduceMaterialDetail = planProduceMaterialDetailMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getNo()));
		Integer countbydevlied = wWmsWarehouseDeliveryDetailMapper.getCountbydevlied(selectByPrimaryKey.getNo(), "2");
		Integer count = planProduceMaterialDetail.getCount();
		Integer zuidacount =count-countbydevlied-wms2.getMatterNumber().intValue();
		if(record.getCount()>zuidacount) {
			return 3;
		}
    	if(wms2.getX()==1) {
    		
    		wms2.setHouseNo(record.getField1());
        	wms2.setMatterNumber(Long.valueOf(record.getCount()));
        	wmsWarehouseDeliveryDetailMapper.updateByPrimaryKeySelective(wms2);
        	selectByPrimaryKey.setField1(record.getField1());
        	selectByPrimaryKey.setCount(record.getCount());
        	return planWorkOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
    	}else {
    		return 0;
    	}
    	
		
		
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanWorkOrder record) {
				
		record.setId(null);
		
				
		return planWorkOrderMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanWorkOrder record, PlanWorkOrderExample example) {
		
		return planWorkOrderMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanWorkOrder record, PlanWorkOrderExample example) {
		
		return planWorkOrderMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanWorkOrder> selectByExample(PlanWorkOrderExample example) {
		
		return planWorkOrderMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanWorkOrderExample example) {
		
		return planWorkOrderMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanWorkOrderExample example) {
		
		return planWorkOrderMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planWorkOrder
	 * @return
	 */
	public int updateVisible(PlanWorkOrder planWorkOrder) {
		
		planWorkOrder.setStatus(3);
		
		return planWorkOrderMapper.updateByPrimaryKeySelective(planWorkOrder);
	}

	public Integer getCountByStatus(Integer status) {
		
		return  planWorkOrderMapper.getCountByStatus(status);
	}

	public int tuiliaoSave(PlanWorkOrder planWorkOrder) {
		PlanWorkOrder selectByworkerOrderNo = planWorkOrderMapper.selectByPrimaryKey(planWorkOrder.getId());
		Integer counts =planWorkOrder.getCount();
		
		//获取该工单领料数量
		Integer countbyUnitName = wmsWarehouseDeliveryDetailMapper.getCountbyUnitName(selectByworkerOrderNo.getWorkerOrderNo(), "2");
		if(counts>countbyUnitName) {
			return 3;
		}
		
		int updateByPrimaryKeySelective =0;
		 List<PlanWorkOrderDetailPz> listByworno = planWorkOrderDetailPzMapper.getListBYworkno(selectByworkerOrderNo.getWorkerOrderNo());
			for (PlanWorkOrderDetailPz planWorkOrderDetail : listByworno) {
				Integer count = planWorkOrderDetail.getCount();
				
				Integer countx =count-counts;
				planWorkOrderDetail.setCount(countx);
				updateByPrimaryKeySelective =planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetail);
			}
			//添加入库明细(退料)
			WmsWarehouseEntryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseEntryDetail();
			wmsWarehouseDeliveryDetail.setMatterNo(selectByworkerOrderNo.getField2());
			wmsWarehouseDeliveryDetail.setMatterDesc(selectByworkerOrderNo.getWorkerOrderNo());
			wmsWarehouseDeliveryDetail.setModifiedUserSid(ShiroUtils.getUserId());
			wmsWarehouseDeliveryDetail.setDeliverySid(selectByworkerOrderNo.getNo());
			wmsWarehouseDeliveryDetail.setHouseNo(planWorkOrder.getField1());
			wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(counts));
			wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
			wmsWarehouseDeliveryDetail.setCreatedUserSid(ShiroUtils.getUserId());
			wmsWarehouseDeliveryDetail.setQualityNo("5");
			wmsWarehouseDeliveryDetail.setX(1);
			 updateByPrimaryKeySelective =wmsWarehouseEntryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
			 
			 return updateByPrimaryKeySelective;
	}

	public Integer getCountbydevlied(String no, String deleteFlag) {
		
		return planWorkOrderMapper.getCountbydevlied(no,deleteFlag);
	}

	public int jiesuansave(PlanWorkOrder planOudetailGongying) {
		PlanWorkOrder selectByPrimaryKey = planWorkOrderMapper.selectByPrimaryKey(planOudetailGongying.getId());
		Integer status = selectByPrimaryKey.getStatus();
		if(status!=3) {
			return 2;
		}
		
		int insertSelective =0;
		List<PlanWorkOrderDetailPz> listBYworkno = planWorkOrderDetailPzMapper.getListBYworkno(selectByPrimaryKey.getWorkerOrderNo());
		for (PlanWorkOrderDetailPz planWorkOrderDetailPz : listBYworkno) {
			PlanWorkOrderPzStatement planWorkOrderPzStatement = planWorkOrderPzStatementMapper.selectByPgid(planWorkOrderDetailPz.getId().intValue());
			if(planWorkOrderPzStatement==null) {
				planWorkOrderPzStatement= new PlanWorkOrderPzStatement();
			
			
			WmsWarehouseEntryDetail bypGid = wmsWarehouseEntryDetailMapper.getBypGid(planWorkOrderDetailPz.getId()+"", "2");
			if(bypGid==null) {
				Integer count = planWorkOrderDetailPz.getCount();
				Integer unqualifiedCount = planWorkOrderDetailPz.getUnqualifiedCount();
				planWorkOrderPzStatement.setCount(count-unqualifiedCount);
			}else {
				planWorkOrderPzStatement.setCount(bypGid.getMatterNumber().intValue());
			}
			TsysUser worker = tsysUserMapper.selectByPrimaryKey(Long.valueOf(planWorkOrderDetailPz.getWorker()));
			JshMaterialCarfprice jshMaterialCarfprice = jshMaterialCarfpriceMapper.selectByPrimaryKey(Long.valueOf(planWorkOrderDetailPz.getCarftId()));
			SysDepartment department = sysDepartmentMapper.selectByPrimaryKey(planWorkOrderDetailPz.getManager());
			planWorkOrderPzStatement.setCreateTime(new Date());
			planWorkOrderPzStatement.setCreator(ShiroUtils.getUserId());
			planWorkOrderPzStatement.setDistributionTime(planWorkOrderDetailPz.getFinshTime());
			planWorkOrderPzStatement.setMaterialNo(planWorkOrderDetailPz.getWorkerOrderNo());
			planWorkOrderPzStatement.setMaterialName(jshMaterialCarfprice.getTtxx());
			planWorkOrderPzStatement.setCarftName(jshMaterialCarfprice.getTtff());
			planWorkOrderPzStatement.setSpesc(jshMaterialCarfprice.getSpecs());
			planWorkOrderPzStatement.setOrgName(department.getDeptName());
			planWorkOrderPzStatement.setWorker(worker.getNickname());
			BigDecimal purchaseDecimal = jshMaterialCarfprice.getPurchaseDecimal();
			
			planWorkOrderPzStatement.setPrice(purchaseDecimal.doubleValue());
			planWorkOrderPzStatement.setAlAccount(planWorkOrderPzStatement.getCount()*planWorkOrderPzStatement.getPrice());
			planWorkOrderPzStatement.setStatus(1);
			planWorkOrderPzStatement.setPgId(planWorkOrderDetailPz.getId().intValue());
			if(planOudetailGongying.getYichangyuanyin().equals("数量正常")) {
				planWorkOrderPzStatement.setUpdater(2l);
			}else {
				planWorkOrderPzStatement.setUpdater(1l);
			}
			insertSelective = planWorkOrderPzStatementMapper.insertSelective(planWorkOrderPzStatement);
			}
		}
		
		
		
			selectByPrimaryKey.setStatus(4);
			insertSelective =planWorkOrderMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		
		return insertSelective;
	}


}
