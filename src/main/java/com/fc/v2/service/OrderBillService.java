package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderBillMapper;
import com.fc.v2.model.auto.OrderBill;
import com.fc.v2.model.auto.OrderBillExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 支付订单流水表 OrderBillService
 * @Title: OrderBillService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-08-30 22:11:47  
 **/
@Service
public class OrderBillService implements BaseService<OrderBill, OrderBillExample>{
	@Autowired
	private OrderBillMapper orderBillMapper;
	
      	   	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OrderBill> list(Tablepar tablepar,OrderBill orderBill){
	        OrderBillExample testExample=new OrderBillExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(orderBill);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("order_bill_id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OrderBill> list= orderBillMapper.selectByExample(testExample);
	        PageInfo<OrderBill> pageInfo = new PageInfo<OrderBill>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderBillExample example=new OrderBillExample();
			example.createCriteria().andOrderBillIdIn(stringB);
			return orderBillMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OrderBill selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderBillMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OrderBill record) {
		return orderBillMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OrderBill record) {
				
		record.setOrderBillId(null);
		
				
		return orderBillMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OrderBill record, OrderBillExample example) {
		
		return orderBillMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OrderBill record, OrderBillExample example) {
		
		return orderBillMapper.updateByExample(record, example);
	}

	@Override
	public List<OrderBill> selectByExample(OrderBillExample example) {
		
		return orderBillMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderBillExample example) {
		
		return orderBillMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderBillExample example) {
		
		return orderBillMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param orderBill
	 * @return
	 */
	public int updateVisible(OrderBill orderBill) {
		return orderBillMapper.updateByPrimaryKeySelective(orderBill);
	}


    public OrderBill getByOutTradeNo(String outTradeNo) {
		return orderBillMapper.getByOutTradeNo(outTradeNo);
    }

	public OrderBill selectByOutTradeNo(String outTradeNo) {
		return orderBillMapper.selectByOutTradeNo(outTradeNo);
	}
}
