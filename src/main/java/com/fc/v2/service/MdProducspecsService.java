package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import com.fc.v2.mapper.auto.MdProduccategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MdProducspecsMapper;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.auto.MdProducspecs;
import com.fc.v2.model.auto.MdProducspecsExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

import javax.transaction.xa.Xid;

/**
 * 主数据-商品信息表 MdProducspecsService
 * @Title: MdProducspecsService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-23 22:58:28  
 **/
@Service
public class MdProducspecsService implements BaseService<MdProducspecs, MdProducspecsExample>{
	@Autowired
	private MdProducspecsMapper mdProducspecsMapper;

	@Autowired
	private MdProduccategoryMapper mdProduccategoryMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MdProducspecs> list(Tablepar tablepar,MdProducspecs mdProducspecs){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MdProducspecs> list= mdProducspecsMapper.getList(mdProducspecs);
	        PageInfo<MdProducspecs> pageInfo = new PageInfo<MdProducspecs>(list);
	        return  pageInfo;
	 }
	 public List<MdProducspecs> getList(MdProducspecs mdProducspecs){
		 
		 List<MdProducspecs> list= mdProducspecsMapper.getList(mdProducspecs);
		 return  list;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MdProducspecsExample example=new MdProducspecsExample();
			example.createCriteria().andSidIn(stringB);
			return mdProducspecsMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MdProducspecs selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
		MdProducspecs mdProducspecs = mdProducspecsMapper.selectByPrimaryKey(id1);
		mdProducspecs.setBrandName(mdProduccategoryMapper.selectByPrimaryKey(mdProducspecs.getBrandSid()).getProductTitle());
		return mdProducspecs;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MdProducspecs record) {
		MdProducspecs selectByPrimaryKey = mdProducspecsMapper.selectByPrimaryKey(record.getSid());
		String productTitle = selectByPrimaryKey.getProductTitle();
		String productTitle2 = record.getProductTitle();
		Long brandSid = selectByPrimaryKey.getBrandSid();
		Long brandSid2 = record.getBrandSid();
		String ttxx =productTitle+brandSid;
		String ttxxff =productTitle2+brandSid2;
		if(!ttxx.equals(ttxxff)) {
			MdProducspecs selectByName = mdProducspecsMapper.selectByName(record.getProductTitle(),record.getBrandSid());
			if(selectByName!=null) {
				return 3;
			}
		}
		
		return mdProducspecsMapper.updateByPrimaryKeySelective(record);
	}
	
	public int gongjiaSelective(MdProducspecs record) {
	
		
		return mdProducspecsMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MdProducspecs record) {
				
		record.setSid(null);
		record.setCreatedTime(new Date());
		record.setCreatedUserSid(ShiroUtils.getUserId());		
		MdProducspecs record1 =mdProducspecsMapper.selectByName(record.getProductTitle(),record.getBrandSid());
		if(record1==null) {
			
			return mdProducspecsMapper.insertSelective(record);
		}else {
			return 3;
		}
	}
	
	
	@Override
	public int updateByExampleSelective(MdProducspecs record, MdProducspecsExample example) {
		
		return mdProducspecsMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MdProducspecs record, MdProducspecsExample example) {
		
		return mdProducspecsMapper.updateByExample(record, example);
	}

	@Override
	public List<MdProducspecs> selectByExample(MdProducspecsExample example) {
		
		return mdProducspecsMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MdProducspecsExample example) {
		
		return mdProducspecsMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MdProducspecsExample example) {
		
		return mdProducspecsMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param mdProducspecs
	 * @return
	 */
	public int updateVisible(MdProducspecs mdProducspecs) {
		return mdProducspecsMapper.updateByPrimaryKeySelective(mdProducspecs);
	}


    public List<MdProducspecs> getByBrandSid(String brandSid) {
		return mdProducspecsMapper.getByBrandSid(brandSid);
    }
	public List<MdProducspecs> excgetlist(MdProducspecs ppm) {
		List<MdProducspecs> list= mdProducspecsMapper.getList(ppm);
		
		return list;
	}
}
