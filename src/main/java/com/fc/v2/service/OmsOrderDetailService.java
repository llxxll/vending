package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MdProductMapper;
import com.fc.v2.mapper.auto.OmsOrderDetailMapper;
import com.fc.v2.mapper.auto.OmsOrderMapper;
import com.fc.v2.mapper.auto.OmsOrderRetreatMapper;
import com.fc.v2.mapper.auto.OrderFromCategoryMapper;
import com.fc.v2.model.auto.MdProduct;
import com.fc.v2.model.auto.OmsOrder;
import com.fc.v2.model.auto.OmsOrderDetail;
import com.fc.v2.model.auto.OmsOrderDetailExample;
import com.fc.v2.model.auto.OmsOrderRetreat;
import com.fc.v2.model.auto.OrderFromCategory;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.BeanUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 销售-订单明细 OmsOrderDetailService
 * @Title: OmsOrderDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-05-21 23:28:42  
 **/
@Service
public class OmsOrderDetailService implements BaseService<OmsOrderDetail, OmsOrderDetailExample>{
	@Autowired
	private OmsOrderDetailMapper omsOrderDetailMapper;
	@Autowired
	private MdProductMapper mdProductMapper;
	@Autowired
	private OrderFromCategoryMapper orderFromCategoryMapper;
	@Autowired
	private OmsOrderMapper omsOrderMapper;
	@Autowired
	private OmsOrderRetreatMapper omsOrderRetreatMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OmsOrderDetail> list(Tablepar tablepar,OmsOrderDetail omsOrderDetail){

		 PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OmsOrderDetail> list= omsOrderDetailMapper.getList(omsOrderDetail);
	        for (OmsOrderDetail omsOrderDetail2 : list) {
	        	OrderFromCategory selectByPrimaryKey = orderFromCategoryMapper.selectByPrimaryKey(omsOrderDetail2.getCategroysid());
	        	omsOrderDetail2.setCategroyName(selectByPrimaryKey.getProductTitle());
			}
	        PageInfo<OmsOrderDetail> pageInfo = new PageInfo<OmsOrderDetail>(list);
	        return  pageInfo;
	 }
	 public List<OmsOrderDetail> excgetlist(OmsOrderDetail omsOrderDetail) {
		 List<OmsOrderDetail> list= omsOrderDetailMapper.getList(omsOrderDetail);
	        for (OmsOrderDetail omsOrderDetail2 : list) {
	        	OrderFromCategory selectByPrimaryKey = orderFromCategoryMapper.selectByPrimaryKey(omsOrderDetail2.getCategroysid());
	        	omsOrderDetail2.setCategroyName(selectByPrimaryKey.getProductTitle());
			}
			return list;
		}
	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OmsOrderDetailExample example=new OmsOrderDetailExample();
			example.createCriteria().andSidIn(stringB);
			return omsOrderDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OmsOrderDetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			OmsOrderDetail selectByPrimaryKey = omsOrderDetailMapper.selectByPrimaryKey(id1);
			//获取退货在途数
			Integer count =omsOrderRetreatMapper.getcountbymingxiID(selectByPrimaryKey.getSid(),null);
			if(count==null) {
				count =0;
			}
			selectByPrimaryKey.setTuihuonumber(Long.valueOf(count));
			return selectByPrimaryKey;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OmsOrderDetail record) {
		return omsOrderDetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OmsOrderDetail record) {
				
		record.setSid(null);
		return omsOrderDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(OmsOrderDetail record, OmsOrderDetailExample example) {
		
		return omsOrderDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OmsOrderDetail record, OmsOrderDetailExample example) {
		
		return omsOrderDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<OmsOrderDetail> selectByExample(OmsOrderDetailExample example) {
		
		return omsOrderDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OmsOrderDetailExample example) {
		
		return omsOrderDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OmsOrderDetailExample example) {
		
		return omsOrderDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param omsOrderDetail
	 * @return
	 */
	public int updateVisible(OmsOrderDetail omsOrderDetail) {
		return omsOrderDetailMapper.updateByPrimaryKeySelective(omsOrderDetail);
	}


    public List<OmsOrderDetail> selectByorderNo(String orderNo) {
    	List<OmsOrderDetail> selectByorderNo = omsOrderDetailMapper.selectByorderNo(orderNo);
    	for (OmsOrderDetail omsOrderDetail : selectByorderNo) {
    		MdProduct selectByPrimaryKey = mdProductMapper.selectByPrimaryKey(omsOrderDetail.getProductSid());
    		if(selectByPrimaryKey!=null) {
    			omsOrderDetail.setImgUrl(selectByPrimaryKey.getBarcode());
    		}
		}
		return omsOrderDetailMapper.selectByorderNo(orderNo);
    }

	public void deleteByOrderNo(String orderNo) {
		omsOrderDetailMapper.deleteByOrderNo(orderNo);
	}

	public List<OmsOrderDetail> getList(String orderNo) {
		OmsOrderDetail omsOrderDetail = new OmsOrderDetail();
		omsOrderDetail.setOrderNo(orderNo);
		return omsOrderDetailMapper.getList(omsOrderDetail);
	}

	public int tuihuo(OmsOrderDetail omsOrderDetail) {
		OmsOrder order = omsOrderMapper.selectByorderNo(omsOrderDetail.getOrderNo());
		
		if(order.getOrderStatus()==5||order.getOrderStatus()==6) {
			return 4;
		}
		
		OmsOrderRetreat omsOrderRetreat = new OmsOrderRetreat();
		omsOrderRetreat.setSid(null);
		omsOrderRetreat.setBuySupplySid(omsOrderDetail.getSid());
		omsOrderRetreat.setOrderStatus(1);
		omsOrderRetreat.setOrderNo(omsOrderDetail.getOrderNo());
		omsOrderRetreat.setCreateTime(new Date());
		omsOrderRetreat.setUserSid(ShiroUtils.getUserId());
		omsOrderRetreat.setOrderSourceSid(omsOrderDetail.getYutuihuonumber());
		return omsOrderRetreatMapper.insertSelective(omsOrderRetreat);
	}
}
