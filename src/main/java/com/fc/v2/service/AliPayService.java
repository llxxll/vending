package com.fc.v2.service;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayFundTransToaccountTransferModel;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fc.v2.controller.yiyuan.YiyuanController;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.Result;
import com.fc.v2.model.auto.Order;
import com.fc.v2.model.auto.OrderBill;
import com.fc.v2.model.enums.status.OrderStatus;
import com.fc.v2.model.exception.ExceptionMessage;
import com.fc.v2.model.exception.ValidateException;
import com.fc.v2.model.property.AppProperties;
import com.fc.v2.util.PaymentUtils;
import com.fc.v2.util.RandomUtil;
import com.fc.v2.util.ResultUtils;
import com.fc.v2.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p> 支付宝支付
 *
 * @author leone
 * @since 2018-05-27
 **/
@Slf4j
@Service
public class AliPayService {

    @Resource
    private OrderService orderService;


    @Resource
    private AppProperties appProperties;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private OrderBillService orderBillService;

    @Resource
    private YiyuanController yiyuanController;

    // 阿里 sdk 封装
    private AlipayClient alipayClient;

    @PostConstruct
    public void initMethod() {
        alipayClient = new DefaultAlipayClient(
                appProperties.getAli().getServer_url(),
                appProperties.getAli().getApp_id(),
                appProperties.getAli().getAlipay_private_key(),
                appProperties.getAli().getFormat(),
                appProperties.getAli().getCharset(),
                appProperties.getAli().getAlipay_public_key(),
                appProperties.getAli().getSign_type());
    }

    /**
     * 支付宝提现
     *
     * @param orderId
     */
    public Result deposit(Long orderId) {
        // 校验订单信息
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.CREATE.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }

        AlipayFundTransToaccountTransferModel transferModel = new AlipayFundTransToaccountTransferModel();

        transferModel.setOutBizNo(1 + RandomUtil.randomNum(15));
        transferModel.setAmount(order.getTotalFee().toString());
        transferModel.setPayeeAccount("real account");
        transferModel.setPayeeRealName("real name");
        transferModel.setPayerShowName("from name");
        transferModel.setRemark("remark");
        transferModel.setPayeeType("ALIPAY_LOGONID");
        try {
            AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
            request.setBizModel(transferModel);
            AlipayFundTransToaccountTransferResponse response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            log.info("ali deposit error message:{}", e.getMessage());
            return Result.success(ExceptionMessage.ALI_DEPOSIT_SUCCESS);
        }
        return Result.error(ExceptionMessage.ALI_DEPOSIT_FAILED);
    }
    /**
     * 支付宝扫码支付生成二维码响应到浏览器
     *
     * @param orderId
     * @param response
     * @return
     */
    public Result aliQrCodePay(Long orderId, HttpServletResponse response) throws Exception {
        // 校验订单信息
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.CREATE.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }

        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
        Map<String, String> params = new TreeMap<>();
        params.put("out_trade_no", order.getOutTradeNo());
        params.put("total_amount", order.getTotalFee().toString());
        params.put("subject", "挂号费");
        //params.put("product_code", "FAST_INSTANT_TRADE_PAY");
        request.setBizContent(objectMapper.writeValueAsString(params));
        request.setNotifyUrl(appProperties.getAli().getNotify_url());
        request.setReturnUrl(appProperties.getAli().getReturn_url());
        AlipayTradePrecreateResponse responseData = alipayClient.execute(request);
        log.info("response:{}", responseData.getBody());
        String qrCode = responseData.getQrCode();
        if (!ObjectUtils.isEmpty(qrCode)) {
            PaymentUtils.createQRCode(qrCode, response);
            return Result.success(ExceptionMessage.SUCCESS);
        }
        return Result.error(ExceptionMessage.ALI_PAY_CREATE_QR_CODE_SUCCESS);
    }

    public Result aliRefund2(Long orderId, HttpServletRequest servletRequest) throws Exception {
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.PAY.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        AlipayClient alipayClient2 = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",appProperties.getAli().getApp_id(),appProperties.getAli().getAlipay_private_key(),"json","UTF-8",appProperties.getAli().getAlipay_public_key(),"RSA2");
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("trade_no", order.getTransactionId());
        bizContent.put("refund_amount", 0.01);
        //bizContent.put("out_request_no", "HZ01RF001");

        request.setBizContent(bizContent.toString());
        AlipayTradeRefundResponse response = alipayClient2.execute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
            order.setDeleteFlag(1);
            orderService.updateByPrimaryKeySelective(order);
            String orders = String.valueOf(order.getOrderId());
            String substring = orders.substring(0, 4);
            yiyuanController.appointmentPayCancel(substring,   String.valueOf(order.getUserId()), order.getRemark());
        } else {
            System.out.println("调用失败");
        }
        return Result.success(ExceptionMessage.SUCCESS);
    }
    public ApiResult aliRefundByoutOrderNumber(String outOrderNumber) throws Exception {
        Order order = orderService.findByOutTradeNo(outOrderNumber);
        if (order.getStatus() != OrderStatus.PAY.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " outOrderNumber: {}", outOrderNumber);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        AlipayClient alipayClient2 = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",appProperties.getAli().getApp_id(),appProperties.getAli().getAlipay_private_key(),"json","UTF-8",appProperties.getAli().getAlipay_public_key(),"RSA2");
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("trade_no", order.getTransactionId());
        bizContent.put("refund_amount", 0.01);
        //bizContent.put("out_request_no", "HZ01RF001");

        request.setBizContent(bizContent.toString());
        AlipayTradeRefundResponse response = alipayClient2.execute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
            order.setDeleteFlag(1);
            orderService.updateByPrimaryKeySelective(order);
            String orders = String.valueOf(order.getOrderId());
            String substring = orders.substring(0, 4);
            yiyuanController.appointmentPayCancel(substring,   String.valueOf(order.getUserId()), order.getRemark());
             return ResultUtils.ok("调用成功");
        } else {
            System.out.println("调用失败");
            return ResultUtils.ok("调用失败");
        }
    }
    public static void main(String[] args) {
        System.out.println(RandomUtil.randomNum(32));
    }
    /**
     * 支付宝退款
     *
     * @param orderId
     * @param servletRequest
     * @return
     */
    public Result aliRefund(Long orderId, HttpServletRequest servletRequest) throws Exception {
        // 校验订单信息
        Order order = orderService.findOne(orderId);
        if (order.getStatus() < OrderStatus.PAY.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        // 创建退款请求builder，设置请求参数
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        Map<String, String> params = new TreeMap<>();
        // 必须 商户订单号
        params.put("out_trade_no", order.getOutTradeNo());
        // 必须 支付宝交易号
        params.put("trade_no", order.getTotalFee().toString());
        // 必须 退款金额
        params.put("refund_amount", order.getTotalFee().toString());
        // 可选 代表 退款的原因说明
        params.put("refund_reason", "退款的原因说明");
        // 可选 标识一次退款请求，同一笔交易多次退款需要保证唯一（就是out_request_no在2次退款一笔交易时，要不一样），如需部分退款，则此参数必传
        params.put("out_request_no", 1 + RandomUtil.randomNum(11));
        // 可选 代表 商户的门店编号
        params.put("store_id", "90m");
        request.setBizContent(objectMapper.writeValueAsString(params));
        AlipayTradeRefundResponse responseData = alipayClient.execute(request);
        if (responseData.isSuccess()) {
            log.info("ali refund success tradeNo:{}", order.getOutTradeNo());
            return Result.success(ExceptionMessage.SUCCESS);
        }
        log.info("ali refund failed tradeNo:{}", order.getOutTradeNo());
        return Result.error(ExceptionMessage.ALI_PAY_REFUND_FAILED);
    }


    /**
     * 阿里pc支付
     *
     * @param orderId
     * @param servletRequest
     * @return
     */
    public String aliPcPay(Long orderId, HttpServletRequest servletRequest) throws Exception {
        Order order = orderService.findOne(orderId);
        AlipayTradePagePayRequest payRequest = new AlipayTradePagePayRequest();
        // 前台通知
        payRequest.setReturnUrl(appProperties.getAli().getReturn_url());
        // 后台回调
        payRequest.setNotifyUrl(appProperties.getAli().getNotify_url());
        Map<String, String> params = new TreeMap<>();
        params.put("out_trade_no", order.getOutTradeNo());
        // 订单金额:元
        params.put("total_amount", order.getTotalFee().toString());
        params.put("subject", "订单标题");
        // 实际收款账号，一般填写商户PID即可
        params.put("seller_id", appProperties.getAli().getMch_id());
        // 电脑网站支付
        params.put("product_code", "FAST_INSTANT_TRADE_PAY");
        params.put("body", "两个橘子");
        payRequest.setBizContent(objectMapper.writeValueAsString(params));
        log.info("业务参数:" + payRequest.getBizContent());
        String result = ExceptionMessage.ERROR.getMessage();
        try {
            result = alipayClient.pageExecute(payRequest).getBody();
        } catch (AlipayApiException e) {
            log.error("ali pay error message:{}", e.getMessage());
        }
        return result;
    }

    /**
     * 支付宝App支付
     *
     * @param orderId
     * @param servletRequest
     * @return
     */
    public String appPay(Long orderId, HttpServletRequest servletRequest) {
        Order order = orderService.findOne(orderId);
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        // SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody("描述");
        model.setSubject("商品名称");
        model.setOutTradeNo(order.getOutTradeNo());
        model.setTimeoutExpress("30m");
        model.setTotalAmount(order.getTotalFee().toString());
        model.setProductCode("QUICK_MSECURITY_PAY");
        request.setBizModel(model);
        request.setNotifyUrl(appProperties.getAli().getNotify_url());
        try {
            // 这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            //就是orderString 可以直接给客户端请求，无需再做处理。
            log.info("orderString:{}", response.getBody());
            return response.getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }
    public Result aliNotify2(HttpServletRequest request) throws Exception {
        System.out.println("支付宝支付回调开始");
        Map<String, Object> map  = getNoticeParams(request);
        System.out.println(JSONObject.toJSONString(map));
        if("TRADE_SUCCESS".equals(map.get("trade_status"))){
            Order order = orderService.findByOutTradeNo((String)map.get("out_trade_no"));
            System.out.println("支付宝支付回调成功");

            if(order != null){
                if (order.getStatus() == OrderStatus.PAY.getStatus()) {
                    return Result.build("支付宝支付回调成功",200,map);
                }
                order.setStatus((byte) 1);
                order.setFinishTime(new Date());
                order.setTransactionId((String)map.get("trade_no"));
                orderService.updateByPrimaryKeySelective(order);
                SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
                String fabuData = sdf.format(order.getFinishTime());
                System.out.println("支付宝支付去通知his");
                yiyuanController.appointmentPay(order.getOutTradeNo(), null, String.valueOf(order.getUserId()),  fabuData, "1", "", order.getConsignee(), "",order.getCreateIp(),order.getOrderId(),String.valueOf(order.getTotalFee()));
                return Result.build("支付宝支付回调成功",200,map);
            }else{
                return Result.build("支付宝查询订单事变",201,map);
            }
        }else{
            return Result.build("支付宝支付失败",201,map);
        }
    }
    private Charset inputCharset = Consts.UTF_8;
    public Map<String, Object> getNoticeParams(HttpServletRequest request) {
        final Map<String, String[]> parameterMap = request.getParameterMap();

        Map<String, Object> params = new TreeMap<>();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            StringBuilder sb = new StringBuilder();
            for (int i = 0, len = values.length; i < len; i++) {
                sb.append(values[i]).append((i == len - 1) ? "" : ',');
            }
            String valueStr = sb.toString();
            if (!valueStr.matches("\\w+")) {
                if (valueStr.equals(new String(valueStr.getBytes(Consts.ISO_8859_1), Consts.ISO_8859_1))) {
                    valueStr = new String(valueStr.getBytes(Consts.ISO_8859_1), inputCharset);
                }
            }
            params.put(name, valueStr);
        }
        return params;
    }
    /**
     * 支付回调通知
     *
     * @param request
     * @param response
     */
    public Result aliNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestXml = PaymentUtils.getRequestData(request);
        Map requestMap = PaymentUtils.xmlToMap(requestXml);
        Assert.notNull(requestMap, ExceptionMessage.XML_DATA_INCORRECTNESS.getMessage());
        System.out.println("支付回调通知");
        System.out.println(JSONObject.toJSONString(requestMap));
        // 当返回的return_code为SUCCESS则回调成功
        if (requestMap.get("code").equals(10000)) {
            // TODO 保存订单流水 具体细节待实现
            OrderBill orderBill = new OrderBill();
            orderBillService.updateByPrimaryKeySelective(orderBill);
            log.info("notify success");
            return Result.success("支付回调成功");
        } else {
            log.error("notify failed");
            return Result.success("支付回调失败");
        }
    }

    public Result cha(String outTradeNo) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",appProperties.getAli().getApp_id(),appProperties.getAli().getAlipay_private_key(),"json","UTF-8",appProperties.getAli().getAlipay_public_key(),"RSA2");
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", outTradeNo);
//bizContent.put("trade_no", "2014112611001004680073956707");
        request.setBizContent(bizContent.toString());
        AlipayTradeQueryResponse response = alipayClient.execute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
        return Result.success(response);
    }


    public ApiResult  qrcoe(Long orderId) throws AlipayApiException {
        System.out.println("支付宝扫码支付开始，orderid："+orderId);
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.CREATE.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        System.out.println("支付宝扫码支付开始，order信息："+JSONObject.toJSONString(order));
        AlipayClient alipayClient2 = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",appProperties.getAli().getApp_id(),appProperties.getAli().getAlipay_private_key(),"json","UTF-8",appProperties.getAli().getAlipay_public_key(),"RSA2");
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        request.setNotifyUrl("http://lxlcode.vicp.fun/api/pay/ali/notify");
        request.setReturnUrl("http://www.jhshzyy.com/");
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", order.getOutTradeNo());
        bizContent.put("total_amount", order.getTotalFee());
        bizContent.put("subject", "挂号费");
        bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");
        request.setBizContent(bizContent.toString());
        System.out.println("支付宝支付调用:"+JSONObject.toJSONString(request));
        AlipayTradePagePayResponse responsedata = alipayClient2.pageExecute(request);
        System.out.println("支付宝支付调用返回:"+responsedata.getBody());
        if(responsedata.isSuccess()){
            System.out.println("调用成功");
            return ResultUtils.ok(responsedata);
        } else {
            System.out.println("调用失败");
            return ResultUtils.ok(responsedata);
        }
    }
}
