package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshInvoiceMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.model.auto.JshInvoice;
import com.fc.v2.model.auto.JshInvoiceExample;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 财务发票对账单 JshInvoiceService
 * @Title: JshInvoiceService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-31 14:47:05  
 **/
@Service
public class JshInvoiceService implements BaseService<JshInvoice, JshInvoiceExample>{
	@Autowired
	private JshInvoiceMapper jshInvoiceMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshInvoice> list(Tablepar tablepar,JshInvoice jshInvoice){
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshInvoice> list= jshInvoiceMapper.getList(jshInvoice);
	        for (JshInvoice jshInvoice2 : list) {
				TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(jshInvoice2.getCreator());
				jshInvoice2.setCreateName(selectByPrimaryKey.getNickname());
			}
	        PageInfo<JshInvoice> pageInfo = new PageInfo<JshInvoice>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshInvoiceExample example=new JshInvoiceExample();
			example.createCriteria().andIdIn(stringB);
			return jshInvoiceMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshInvoice selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshInvoiceMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshInvoice record) {
		return jshInvoiceMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshInvoice record) {
				
		record.setId(null);
		
				
		return jshInvoiceMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(JshInvoice record, JshInvoiceExample example) {
		
		return jshInvoiceMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshInvoice record, JshInvoiceExample example) {
		
		return jshInvoiceMapper.updateByExample(record, example);
	}

	@Override
	public List<JshInvoice> selectByExample(JshInvoiceExample example) {
		
		return jshInvoiceMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshInvoiceExample example) {
		
		return jshInvoiceMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshInvoiceExample example) {
		
		return jshInvoiceMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshInvoice
	 * @return
	 */
	public int updateVisible(JshInvoice jshInvoice) {
		return jshInvoiceMapper.updateByPrimaryKeySelective(jshInvoice);
	}

	public int upload(List<JshInvoice> execltoList) {
		int insertSelective =0;
		for (JshInvoice jshInvoice : execltoList) {
			//获取最近的一条 数据 比对价格  如果不一样  赋值1
			JshInvoice jshInvoicex =jshInvoiceMapper.getOneNew(jshInvoice.getMaterialNo(),jshInvoice.getOrgName());
			Double price = jshInvoicex.getPrice();
			Double price2 = jshInvoice.getPrice();
			if(price.compareTo(price2)!=0) {
				jshInvoice.setStatus(1);
			}else {
				jshInvoice.setStatus(0);
			}
			jshInvoice.setCreator(ShiroUtils.getUserId());
			jshInvoice.setCreateTime(new Date());
			
			insertSelective = jshInvoiceMapper.insertSelective(jshInvoice);
		}
		return insertSelective;
	}


}
