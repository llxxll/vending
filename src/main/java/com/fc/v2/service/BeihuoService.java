package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.BeihuoMapper;
import com.fc.v2.model.auto.Beihuo;
import com.fc.v2.model.auto.BeihuoExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 备货管理 BeihuoService
 * @Title: BeihuoService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:48  
 **/
@Service
public class BeihuoService implements BaseService<Beihuo, BeihuoExample>{
	@Autowired
	private BeihuoMapper beihuoMapper;
	
      	   	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Beihuo> list(Tablepar tablepar,Beihuo beihuo){
	        BeihuoExample testExample=new BeihuoExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(beihuo);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Beihuo> list= beihuoMapper.selectByExample(testExample);
	        PageInfo<Beihuo> pageInfo = new PageInfo<Beihuo>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			BeihuoExample example=new BeihuoExample();
			example.createCriteria().andIdIn(stringB);
			return beihuoMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Beihuo selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return beihuoMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Beihuo record) {
		return beihuoMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Beihuo record) {
				
		record.setId(null);
		
				
		return beihuoMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Beihuo record, BeihuoExample example) {
		
		return beihuoMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Beihuo record, BeihuoExample example) {
		
		return beihuoMapper.updateByExample(record, example);
	}

	@Override
	public List<Beihuo> selectByExample(BeihuoExample example) {
		
		return beihuoMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(BeihuoExample example) {
		
		return beihuoMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(BeihuoExample example) {
		
		return beihuoMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param beihuo
	 * @return
	 */
	public int updateVisible(Beihuo beihuo) {
		return beihuoMapper.updateByPrimaryKeySelective(beihuo);
	}


}
