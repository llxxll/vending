package com.fc.v2.service;

import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.PlanWorkOrderPzStatementMapper;
import com.fc.v2.model.auto.PlanWorkOrderPzStatement;
import com.fc.v2.model.auto.PlanWorkOrderPzStatementExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 工单明细结算表(结算明细) PlanWorkOrderPzStatementService
 * @Title: PlanWorkOrderPzStatementService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-02-26 16:24:34  
 **/
@Service
@Transactional
public class PlanWorkOrderPzStatementService implements BaseService<PlanWorkOrderPzStatement, PlanWorkOrderPzStatementExample>{
	@Autowired
	private PlanWorkOrderPzStatementMapper planWorkOrderPzStatementMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 * @throws ParseException 
	 */
	 public PageInfo<PlanWorkOrderPzStatement> list(Tablepar tablepar,PlanWorkOrderPzStatement planWorkOrderPzStatement) throws ParseException{
		 
		 String beginTime = planWorkOrderPzStatement.getBeginTime();
			String endTime = planWorkOrderPzStatement.getEndTime();
			if(beginTime!=null) {
				 
				 if(!beginTime.trim().equals("")) {
					 beginTime=beginTime+" 00:00:00";
					 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					 
					 Date date =sdf.parse(beginTime);
					  
					 Calendar calendar = Calendar.getInstance();
					  
					 calendar.setTime(date);
					 calendar.add(Calendar.HOUR, -8);
					 
					 String format = sdf.format(calendar.getTime());
					 planWorkOrderPzStatement.setBeginTime(format);
				 }
			 }
			 
			 if(endTime!=null) {
				 if(!endTime.trim().equals("")) {
					 endTime=endTime+" 23:59:59";
					 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					 
					 Date date =sdf.parse(endTime);
					  
					 Calendar calendar = Calendar.getInstance();
					  
					 calendar.setTime(date);
					 calendar.add(Calendar.HOUR, -8);
					 
					 String format = sdf.format(calendar.getTime());
					 planWorkOrderPzStatement.setEndTime(format);
				 }
			 }
		 
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanWorkOrderPzStatement> list= planWorkOrderPzStatementMapper.getList(planWorkOrderPzStatement);
	        PageInfo<PlanWorkOrderPzStatement> pageInfo = new PageInfo<PlanWorkOrderPzStatement>(list);
	        return  pageInfo;
	 }
	 public List<PlanWorkOrderPzStatement> exclist(PlanWorkOrderPzStatement planWorkOrderPzStatement) throws ParseException{
		 
		 String beginTime = planWorkOrderPzStatement.getBeginTime();
		 String endTime = planWorkOrderPzStatement.getEndTime();
		 if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planWorkOrderPzStatement.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planWorkOrderPzStatement.setEndTime(format);
			 }
		 }
		 
		 List<PlanWorkOrderPzStatement> list= planWorkOrderPzStatementMapper.getList(planWorkOrderPzStatement);
		 return  list;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			PlanWorkOrderPzStatementExample example=new PlanWorkOrderPzStatementExample();
			example.createCriteria().andIdIn(stringB);
			return planWorkOrderPzStatementMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public PlanWorkOrderPzStatement selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return planWorkOrderPzStatementMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanWorkOrderPzStatement record) {
		
		PlanWorkOrderPzStatement selectByPrimaryKey = planWorkOrderPzStatementMapper.selectByPrimaryKey(record.getId());
		selectByPrimaryKey.setPrice(record.getPrice());
		selectByPrimaryKey.setAlAccount(record.getPrice()*selectByPrimaryKey.getCount());
		
		return planWorkOrderPzStatementMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanWorkOrderPzStatement record) {
				
		record.setId(null);
		
				
		return planWorkOrderPzStatementMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanWorkOrderPzStatement record, PlanWorkOrderPzStatementExample example) {
		
		return planWorkOrderPzStatementMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanWorkOrderPzStatement record, PlanWorkOrderPzStatementExample example) {
		
		return planWorkOrderPzStatementMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanWorkOrderPzStatement> selectByExample(PlanWorkOrderPzStatementExample example) {
		
		return planWorkOrderPzStatementMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanWorkOrderPzStatementExample example) {
		
		return planWorkOrderPzStatementMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanWorkOrderPzStatementExample example) {
		
		return planWorkOrderPzStatementMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planWorkOrderPzStatement
	 * @return
	 */
	public int updateVisible(PlanWorkOrderPzStatement planWorkOrderPzStatement) {
		return planWorkOrderPzStatementMapper.updateByPrimaryKeySelective(planWorkOrderPzStatement);
	}


}
