package com.fc.v2.service;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyDetailMapper;
import com.fc.v2.model.auto.YyDetail;
import com.fc.v2.model.auto.YyDetailExample;
import com.fc.v2.model.custom.Tablepar;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 *  YyDetailService
 * @Title: YyDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-21 18:33:41  
 **/
@Service
public class YyDetailService implements BaseService<YyDetail, YyDetailExample> {
	@Autowired
	private YyDetailMapper yyDetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyDetail> list(Tablepar tablepar, YyDetail yyDetail){
	        YyDetailExample testExample=new YyDetailExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyDetail);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyDetail> list= yyDetailMapper.selectByExample(testExample);
	        PageInfo<YyDetail> pageInfo = new PageInfo<YyDetail>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyDetailExample example=new YyDetailExample();
			example.createCriteria().andIdIn(stringB);
			return yyDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyDetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyDetailMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyDetail record) {
		return yyDetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyDetail record) {
				
		record.setId(null);
		
				
		return yyDetailMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyDetail record, YyDetailExample example) {
		
		return yyDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyDetail record, YyDetailExample example) {
		
		return yyDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<YyDetail> selectByExample(YyDetailExample example) {
		
		return yyDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyDetailExample example) {
		
		return yyDetailMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyDetailExample example) {
		
		return yyDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyDetail
	 * @return
	 */
	@Transactional
	public int updateVisible(YyDetail yyDetail) {
		return yyDetailMapper.updateByPrimaryKeySelective(yyDetail);
	}


	public List<YyDetail> selectByYyIdAndYyType(Long id, String type) {
		return yyDetailMapper.selectByYyIdAndYyType(id, type);
	}

    public void deleteByYyIdAndYyType(Long id, String type) {
		 yyDetailMapper.deleteByYyIdAndYyType(id, type);
    }
}
