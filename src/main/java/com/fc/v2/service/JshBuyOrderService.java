package com.fc.v2.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.fc.v2.mapper.auto.JshBuyOrderPzMapper;
import com.fc.v2.model.auto.JshBuyOrderPz;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshBuyOrderMapper;
import com.fc.v2.model.auto.JshBuyOrder;
import com.fc.v2.model.auto.JshBuyOrderExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 采购单 JshBuyOrderService
 * @Title: JshBuyOrderService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-26 23:13:09  
 **/
@Service
@Transactional
public class JshBuyOrderService implements BaseService<JshBuyOrder, JshBuyOrderExample>{
	@Autowired
	private JshBuyOrderMapper jshBuyOrderMapper;
	@Autowired
	private JshBuyOrderPzMapper jshBuyOrderPzMapper;
      	   	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshBuyOrder> list(Tablepar tablepar,JshBuyOrder jshBuyOrder){
	        JshBuyOrderExample testExample=new JshBuyOrderExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(jshBuyOrder);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshBuyOrder> list= jshBuyOrderMapper.selectByExample(testExample);
	        PageInfo<JshBuyOrder> pageInfo = new PageInfo<JshBuyOrder>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshBuyOrderExample example=new JshBuyOrderExample();
			example.createCriteria().andIdIn(stringB);
			return jshBuyOrderMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshBuyOrder selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshBuyOrderMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshBuyOrder record) {
		return jshBuyOrderMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshBuyOrder record) {
		if(record.getMaterials().size() <=0){
			return 0;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String no = "CG" + simpleDateFormat.format(new Date());
		Double orderPrice = 0.0;
		List<JshBuyOrderPz> materials = record.getMaterials();
		
		for (JshBuyOrderPz jshBuyOrderPz : materials) {
			jshBuyOrderPz.setId(null);
			jshBuyOrderPz.setBuyNo(no);
			jshBuyOrderPz.setCreateTime(new Date());
			jshBuyOrderPz.setCreator(ShiroUtils.getUserId());
			jshBuyOrderPz.setStatus(1);
			Integer count = jshBuyOrderPz.getCount();
			String canpinPrice = jshBuyOrderPz.getCanpinPrice();
			Double valueOf = Double.valueOf(canpinPrice);
			Double orderPrice1 =valueOf*count;
			orderPrice =orderPrice +orderPrice1;
			int insertSelective = jshBuyOrderPzMapper.insertSelective(jshBuyOrderPz);
			
		}
		 
		 
		record.setOrderPrice(orderPrice+"");
		record.setId(null);
		record.setNo(no);
		record.setStatus(1);
		record.setCreateTime(new Date());
		record.setCreator(ShiroUtils.getUserId());
		return jshBuyOrderMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(JshBuyOrder record, JshBuyOrderExample example) {
		
		return jshBuyOrderMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshBuyOrder record, JshBuyOrderExample example) {
		
		return jshBuyOrderMapper.updateByExample(record, example);
	}

	@Override
	public List<JshBuyOrder> selectByExample(JshBuyOrderExample example) {
		
		return jshBuyOrderMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshBuyOrderExample example) {
		
		return jshBuyOrderMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshBuyOrderExample example) {
		
		return jshBuyOrderMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshBuyOrder
	 * @return
	 */
	public int updateVisible(JshBuyOrder jshBuyOrder) {
		return jshBuyOrderMapper.updateByPrimaryKeySelective(jshBuyOrder);
	}


}
