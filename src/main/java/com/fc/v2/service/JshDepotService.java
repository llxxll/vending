package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshDepotMapper;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshDepotExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 仓库表 JshDepotService
 * @Title: JshDepotService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 23:06:31  
 **/
@Service
public class JshDepotService implements BaseService<JshDepot, JshDepotExample>{
	@Autowired
	private JshDepotMapper jshDepotMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshDepot> list(Tablepar tablepar,JshDepot jshDepot){
	        JshDepotExample testExample=new JshDepotExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(jshDepot);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshDepot> list= jshDepotMapper.selectByExample(testExample);
	        PageInfo<JshDepot> pageInfo = new PageInfo<JshDepot>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshDepotExample example=new JshDepotExample();
			example.createCriteria().andIdIn(stringB);
			return jshDepotMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshDepot selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshDepotMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshDepot record) {
		return jshDepotMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshDepot record) {
				
		record.setId(null);
		
				
		return jshDepotMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(JshDepot record, JshDepotExample example) {
		
		return jshDepotMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshDepot record, JshDepotExample example) {
		
		return jshDepotMapper.updateByExample(record, example);
	}

	@Override
	public List<JshDepot> selectByExample(JshDepotExample example) {
		
		return jshDepotMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshDepotExample example) {
		
		return jshDepotMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshDepotExample example) {
		
		return jshDepotMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshDepot
	 * @return
	 */
	public int updateVisible(JshDepot jshDepot) {
		return jshDepotMapper.updateByPrimaryKeySelective(jshDepot);
	}

	public List<JshDepot> getHouseList() {
		
		return jshDepotMapper.findAll();
	}


}
