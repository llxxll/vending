package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.CompanyReviewMapper;
import com.fc.v2.model.auto.CompanyReview;
import com.fc.v2.model.auto.CompanyReviewExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 公司审核 CompanyReviewService
 * @Title: CompanyReviewService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2024-01-23 22:03:22  
 **/
@Service
public class CompanyReviewService implements BaseService<CompanyReview, CompanyReviewExample>{
	@Autowired
	private CompanyReviewMapper companyReviewMapper;
	
      	   	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<CompanyReview> list(Tablepar tablepar,CompanyReview companyReview){
	        CompanyReviewExample testExample=new CompanyReviewExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(companyReview);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<CompanyReview> list= companyReviewMapper.selectByExample(testExample);
	        PageInfo<CompanyReview> pageInfo = new PageInfo<CompanyReview>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			CompanyReviewExample example=new CompanyReviewExample();
			example.createCriteria().andIdIn(stringB);
			return companyReviewMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public CompanyReview selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return companyReviewMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(CompanyReview record) {
		return companyReviewMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(CompanyReview record) {
				
		record.setId(null);
		
				
		return companyReviewMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(CompanyReview record, CompanyReviewExample example) {
		
		return companyReviewMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(CompanyReview record, CompanyReviewExample example) {
		
		return companyReviewMapper.updateByExample(record, example);
	}

	@Override
	public List<CompanyReview> selectByExample(CompanyReviewExample example) {
		
		return companyReviewMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(CompanyReviewExample example) {
		
		return companyReviewMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(CompanyReviewExample example) {
		
		return companyReviewMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param companyReview
	 * @return
	 */
	public int updateVisible(CompanyReview companyReview) {
		return companyReviewMapper.updateByPrimaryKeySelective(companyReview);
	}


}
