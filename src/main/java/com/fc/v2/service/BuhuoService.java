package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.BuhuoMapper;
import com.fc.v2.model.auto.Buhuo;
import com.fc.v2.model.auto.BuhuoExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 补货管理 BuhuoService
 * @Title: BuhuoService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:39  
 **/
@Service
public class BuhuoService implements BaseService<Buhuo, BuhuoExample>{
	@Autowired
	private BuhuoMapper buhuoMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Buhuo> list(Tablepar tablepar,Buhuo buhuo){
	        BuhuoExample testExample=new BuhuoExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(buhuo);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Buhuo> list= buhuoMapper.selectByExample(testExample);
	        PageInfo<Buhuo> pageInfo = new PageInfo<Buhuo>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			BuhuoExample example=new BuhuoExample();
			example.createCriteria().andIdIn(stringB);
			return buhuoMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Buhuo selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return buhuoMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Buhuo record) {
		return buhuoMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Buhuo record) {
				
		record.setId(null);
		
				
		return buhuoMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Buhuo record, BuhuoExample example) {
		
		return buhuoMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Buhuo record, BuhuoExample example) {
		
		return buhuoMapper.updateByExample(record, example);
	}

	@Override
	public List<Buhuo> selectByExample(BuhuoExample example) {
		
		return buhuoMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(BuhuoExample example) {
		
		return buhuoMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(BuhuoExample example) {
		
		return buhuoMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param buhuo
	 * @return
	 */
	public int updateVisible(Buhuo buhuo) {
		return buhuoMapper.updateByPrimaryKeySelective(buhuo);
	}


}
