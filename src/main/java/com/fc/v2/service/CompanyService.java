package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.CompanyMapper;
import com.fc.v2.model.auto.Company;
import com.fc.v2.model.auto.CompanyExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 公司管理表 CompanyService
 * @Title: CompanyService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-12-01 16:31:41  
 **/
@Service
public class CompanyService implements BaseService<Company, CompanyExample>{
	@Autowired
	private CompanyMapper companyMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Company> list(Tablepar tablepar,Company company){
	        CompanyExample testExample=new CompanyExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(company);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Company> list= companyMapper.selectByExample(testExample);
	        PageInfo<Company> pageInfo = new PageInfo<Company>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			CompanyExample example=new CompanyExample();
			example.createCriteria().andIdIn(stringB);
			return companyMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Company selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return companyMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Company record) {
		return companyMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Company record) {
				
		record.setId(null);
		
				
		return companyMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Company record, CompanyExample example) {
		
		return companyMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Company record, CompanyExample example) {
		
		return companyMapper.updateByExample(record, example);
	}

	@Override
	public List<Company> selectByExample(CompanyExample example) {
		
		return companyMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(CompanyExample example) {
		
		return companyMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(CompanyExample example) {
		
		return companyMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param company
	 * @return
	 */
	public int updateVisible(Company company) {
		return companyMapper.updateByPrimaryKeySelective(company);
	}


}
