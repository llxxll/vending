package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.fc.v2.model.auto.*;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyGonggaoMapper;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院公告 YyGonggaoService
 * @Title: YyGonggaoService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:27:28  
 **/
@Service
public class YyGonggaoService implements BaseService<YyGonggao, YyGonggaoExample>{
	@Autowired
	private YyGonggaoMapper yyGonggaoMapper;
	@Autowired
	private YyDetailService yyDetailService;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyGonggao> list(Tablepar tablepar,YyGonggao yyGonggao){
	        YyGonggaoExample testExample=new YyGonggaoExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyGonggao);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyGonggao> list= yyGonggaoMapper.selectByExample(testExample);
		 if(list.size()>0){
			 for(YyGonggao yy: list){
				 List<YyDetail> yyDetails = yyDetailService.selectByYyIdAndYyType(yy.getId(),"gg");
				 yy.setYyDetailList(yyDetails);
			 }
		 }
	        PageInfo<YyGonggao> pageInfo = new PageInfo<YyGonggao>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyGonggaoExample example=new YyGonggaoExample();
			example.createCriteria().andIdIn(stringB);
			return yyGonggaoMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyGonggao selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyGonggaoMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyGonggao record) {
		record.setUpdateTime(new Date());
		if(StringUtils.isNotEmpty(record.getField4()) && StringUtils.isNotEmpty(record.getField5())){
			yyDetailService.deleteByYyIdAndYyType(record.getId(),"gg");
			String field4 = record.getField4();
			String field5 = record.getField5();
			String[] split4new = field4.split(",");
			String[] split5new = field5.split(",");
			for(int i=0;i<split4new.length;i++){
				TsysUser user = ShiroUtils.getUser();
				YyDetail yyDetail = new YyDetail();
				yyDetail.setCreateTime(new Date());
				yyDetail.setDeleteFlag(0);
				yyDetail.setCreator(user.getId());
				yyDetail.setField3(user.getNickname());
				yyDetail.setUpdateTime(new Date());
				yyDetail.setUpdater(user.getId());
				yyDetail.setName(split4new[i]);
				yyDetail.setUrl(split5new[i]);
				yyDetail.setYyId(record.getId());
				yyDetail.setYyType("gg");
				yyDetailService.insertSelective(yyDetail);
			}
		}
		return yyGonggaoMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyGonggao record) {
				
		record.setId(null);
		int b = yyGonggaoMapper.insertSelective(record);
		if(StringUtils.isNotEmpty(record.getField4()) && StringUtils.isNotEmpty(record.getField5())){
			String field4 = record.getField4();
			String field5 = record.getField5();
			String[] split4new = field4.split(",");
			String[] split5new = field5.split(",");
			for(int i=0;i<split4new.length;i++){
				TsysUser user = ShiroUtils.getUser();
				YyDetail yyDetail = new YyDetail();
				yyDetail.setCreateTime(new Date());
				yyDetail.setDeleteFlag(0);
				yyDetail.setCreator(user.getId());
				yyDetail.setField3(user.getNickname());
				yyDetail.setUpdateTime(new Date());
				yyDetail.setUpdater(user.getId());
				yyDetail.setName(split4new[i]);
				yyDetail.setUrl(split5new[i]);
				yyDetail.setYyId(record.getId());
				yyDetail.setYyType("gg");
				yyDetailService.insertSelective(yyDetail);
			}
		}
		return b;
	}
	
	
	@Override
	public int updateByExampleSelective(YyGonggao record, YyGonggaoExample example) {
		
		return yyGonggaoMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(YyGonggao record, YyGonggaoExample example) {
		
		return yyGonggaoMapper.updateByExample(record, example);
	}

	@Override
	public List<YyGonggao> selectByExample(YyGonggaoExample example) {
		
		return yyGonggaoMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyGonggaoExample example) {
		
		return yyGonggaoMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(YyGonggaoExample example) {
		
		return yyGonggaoMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyGonggao
	 * @return
	 */
	@Transactional
	public int updateVisible(YyGonggao yyGonggao) {
		return yyGonggaoMapper.updateByPrimaryKeySelective(yyGonggao);
	}

	@Transactional
	public int updateStatusByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyGonggaoMapper.updateStatusByPrimaryKey(aLong);
	}
	@Transactional
	public int shangjiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyGonggaoMapper.shangjiaByPrimaryKey(aLong);
	}

	@Transactional
	public int xiajiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyGonggaoMapper.xiajiaByPrimaryKey(aLong);
	}

    public int updateByPrimaryKey(YyGonggao yyGonggao) {
		return yyGonggaoMapper.updateByPrimaryKey(yyGonggao);
    }
}
