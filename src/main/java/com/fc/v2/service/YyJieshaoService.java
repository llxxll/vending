package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyJieshaoMapper;
import com.fc.v2.model.auto.YyJieshao;
import com.fc.v2.model.auto.YyJieshaoExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 医院介绍 YyJieshaoService
 * @Title: YyJieshaoService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-05-22 19:46:03  
 **/
@Service
public class YyJieshaoService implements BaseService<YyJieshao, YyJieshaoExample>{
	@Autowired
	private YyJieshaoMapper yyJieshaoMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyJieshao> list(Tablepar tablepar,YyJieshao yyJieshao){
	        YyJieshaoExample testExample=new YyJieshaoExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyJieshao);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyJieshao> list= yyJieshaoMapper.selectByExample(testExample);
	        PageInfo<YyJieshao> pageInfo = new PageInfo<YyJieshao>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyJieshaoExample example=new YyJieshaoExample();
			example.createCriteria().andIdIn(stringB);
			return yyJieshaoMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyJieshao selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyJieshaoMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(YyJieshao record) {
		record.setUpdateTime(new Date());
		return yyJieshaoMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(YyJieshao record) {
				
		record.setId(null);
		
				
		return yyJieshaoMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(YyJieshao record, YyJieshaoExample example) {
		
		return yyJieshaoMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(YyJieshao record, YyJieshaoExample example) {
		
		return yyJieshaoMapper.updateByExample(record, example);
	}

	@Override
	public List<YyJieshao> selectByExample(YyJieshaoExample example) {
		
		return yyJieshaoMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyJieshaoExample example) {
		
		return yyJieshaoMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(YyJieshaoExample example) {
		
		return yyJieshaoMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyJieshao
	 * @return
	 */
	public int updateVisible(YyJieshao yyJieshao) {
		return yyJieshaoMapper.updateByPrimaryKeySelective(yyJieshao);
	}


}
