package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.PlanOudetailMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsSonghuoMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsSonghuo;
import com.fc.v2.model.auto.WmsSonghuoExample;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 出库单明细 WmsSonghuoService
 * @Title: WmsSonghuoService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:44  
 **/
@Service
@Transactional
public class WmsSonghuoService implements BaseService<WmsSonghuo, WmsSonghuoExample>{
	@Autowired
	private WmsSonghuoMapper wmsSonghuoMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private PlanOudetailMapper planOudetailMapper;
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<WmsSonghuo> list(Tablepar tablepar,WmsSonghuo wmsSonghuo){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<WmsSonghuo> list= wmsSonghuoMapper.getList(wmsSonghuo);
	        for (WmsSonghuo wmsSonghuo2 : list) {
	        	Long xUserSid = wmsSonghuo2.getXUserSid();
	        	if(xUserSid!=null) {
	        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(xUserSid);
	        		String nickname = selectByPrimaryKey.getNickname();
	        		wmsSonghuo2.setxName(nickname);
	        	}
			}
	        
	        
	        PageInfo<WmsSonghuo> pageInfo = new PageInfo<WmsSonghuo>(list);
	        return  pageInfo;
	 }
	 public PageInfo<WmsSonghuo> getList(Tablepar tablepar,WmsSonghuo wmsSonghuo){
		 
		// PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		 List<WmsSonghuo> list= wmsSonghuoMapper.getListByWXid(wmsSonghuo.getUnitName());
		 for (WmsSonghuo wmsSonghuo2 : list) {
			 Long xUserSid = wmsSonghuo2.getXUserSid();
			 if(xUserSid!=null) {
				 TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(xUserSid);
				 String nickname = selectByPrimaryKey.getNickname();
				 wmsSonghuo2.setxName(nickname);
			 }
		 }
		 
		 
		 PageInfo<WmsSonghuo> pageInfo = new PageInfo<WmsSonghuo>(list);
		 return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
		WmsSonghuo selectByPrimaryKey = wmsSonghuoMapper.selectByPrimaryKey(Long.valueOf(ids));
		int deleteByPrimaryKey =0;
		if(selectByPrimaryKey.getHouseNo().equals("1")) {
			
		
				WmsWarehouseEntryDetail wms =wmsWarehouseEntryDetailMapper.getBypGid(ids,"1");
				if(wms.getX()==2) {
					return 0;
				}
				deleteByPrimaryKey = wmsWarehouseEntryDetailMapper.deleteByPrimaryKey(wms.getSid());
				if(deleteByPrimaryKey>0) {
					
					deleteByPrimaryKey = wmsSonghuoMapper.deleteByPrimaryKey(Long.valueOf(ids));
				}
		}else if(selectByPrimaryKey.getHouseNo().equals("0")){
			deleteByPrimaryKey = wmsSonghuoMapper.deleteByPrimaryKey(Long.valueOf(ids));
		}
		return deleteByPrimaryKey;
				
	}
	
	
	@Override
	public WmsSonghuo selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return wmsSonghuoMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(WmsSonghuo record) {
		
		return wmsSonghuoMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(WmsSonghuo record) {
				
		record.setSid(null);
		
				
		return wmsSonghuoMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(WmsSonghuo record, WmsSonghuoExample example) {
		
		return wmsSonghuoMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(WmsSonghuo record, WmsSonghuoExample example) {
		
		return wmsSonghuoMapper.updateByExample(record, example);
	}

	@Override
	public List<WmsSonghuo> selectByExample(WmsSonghuoExample example) {
		
		return wmsSonghuoMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(WmsSonghuoExample example) {
		
		return wmsSonghuoMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(WmsSonghuoExample example) {
		
		return wmsSonghuoMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param wmsSonghuo
	 * @return
	 */
	public int updateVisible(WmsSonghuo wmsSonghuo) {
		return wmsSonghuoMapper.updateByPrimaryKeySelective(wmsSonghuo);
	}

	public int zhijianSave(WmsSonghuo wmsSonghuo) {
		WmsSonghuo selectByPrimaryKey = wmsSonghuoMapper.selectByPrimaryKey(wmsSonghuo.getSid());
		Long matterNo = Long.valueOf(wmsSonghuo.getMatterNo());
		Long matterNumber = selectByPrimaryKey.getMatterNumber();
		if(matterNo>matterNumber) {
			return 3;
		}
		
		wmsSonghuo.setHouseNo("1");
		wmsSonghuo.setXTime(new Date());
		wmsSonghuo.setXUserSid(ShiroUtils.getUserId());
		int updateByPrimaryKeySelective = wmsSonghuoMapper.updateByPrimaryKeySelective(wmsSonghuo);
		if(updateByPrimaryKeySelective>0) {
			//生产入库明细
			PlanOudetail selectByPrimaryKey2 = planOudetailMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getDeliverySid()));
			WmsWarehouseEntryDetail wmsWarehouseEntryDetail = new WmsWarehouseEntryDetail();
			wmsWarehouseEntryDetail.setUnitName(selectByPrimaryKey.getSid()+"");
			wmsWarehouseEntryDetail.setMatterDesc(selectByPrimaryKey.getUnitName());
			wmsWarehouseEntryDetail.setDeliverySid(selectByPrimaryKey.getDeliverySid());
			//送货数量-不合格
			wmsWarehouseEntryDetail.setMatterNumber(matterNumber-matterNo);
			wmsWarehouseEntryDetail.setCreatedTime(new Date());
			wmsWarehouseEntryDetail.setCreatedUserSid(ShiroUtils.getUserId());
			wmsWarehouseEntryDetail.setQualityNo("1");
			wmsWarehouseEntryDetail.setX(1);
			wmsWarehouseEntryDetail.setMatterNo(selectByPrimaryKey2.getMateriaNo());
			wmsWarehouseEntryDetail.setHouseNo(selectByPrimaryKey.getX()+"");
			wmsWarehouseEntryDetail.setModifiedUserSid(selectByPrimaryKey.getCreatedUserSid());
			updateByPrimaryKeySelective = wmsWarehouseEntryDetailMapper.insertSelective(wmsWarehouseEntryDetail);
		}
		return updateByPrimaryKeySelective;
	}


}
