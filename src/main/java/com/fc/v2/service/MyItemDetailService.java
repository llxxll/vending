package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MyItemDetailMapper;
import com.fc.v2.mapper.auto.MyItemTypeMapper;
import com.fc.v2.model.auto.MyItemDetail;
import com.fc.v2.model.auto.MyItemDetailExample;
import com.fc.v2.model.auto.MyItemType;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 *  MyItemDetailService
 * @Title: MyItemDetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-08-28 21:49:20  
 **/
@Service
public class MyItemDetailService implements BaseService<MyItemDetail, MyItemDetailExample>{
	@Autowired
	private MyItemDetailMapper myItemDetailMapper;
	@Autowired
	private MyItemTypeMapper myItemTypeMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MyItemDetail> list(Tablepar tablepar,MyItemDetail myItemDetail){
			
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MyItemDetail> list= myItemDetailMapper.getList(myItemDetail);
	       for (MyItemDetail myItemDetail2 : list) {
	    	   MyItemType selectByPrimaryKey = myItemTypeMapper.selectByPrimaryKey(Long.valueOf(myItemDetail2.getItemtype()));
	    	   if(selectByPrimaryKey!=null) {
	    		   myItemDetail2.setItemtypeName(selectByPrimaryKey.getTypeName());
	    	   }
			
		}
	        PageInfo<MyItemDetail> pageInfo = new PageInfo<MyItemDetail>(list);
	        return  pageInfo;
	 }
	 public PageInfo<MyItemDetail> list1(Tablepar tablepar,MyItemDetail myItemDetail){
			
	       // PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MyItemDetail> list= myItemDetailMapper.getList1(myItemDetail);
	        PageInfo<MyItemDetail> pageInfo = new PageInfo<MyItemDetail>(list);
	        return  pageInfo;
	 }
	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MyItemDetailExample example=new MyItemDetailExample();
			example.createCriteria().andIdIn(stringB);
			return myItemDetailMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MyItemDetail selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
		MyItemDetail myItemDetail1 = myItemDetailMapper.selectByPrimaryKey2(id1);
		if(myItemDetail1.getType().equals("1")){
			myItemDetail1.setType("项目主管");
		}
		if(myItemDetail1.getType().equals("2")){
			myItemDetail1.setType("项目开发");
		}
		if(myItemDetail1.getType().equals("3")){
			myItemDetail1.setType("项目设计");
		}
		if(myItemDetail1.getType().equals("4")){
			myItemDetail1.setType("项目制造");
		}
		if(myItemDetail1.getType().equals("5")){
			myItemDetail1.setType("项目图纸");
		}
		if(myItemDetail1.getType().equals("6")){
			myItemDetail1.setType("项目档案");
		}
		if(myItemDetail1.getType().equals("7")){
			myItemDetail1.setType("项目协助");
		}
		return myItemDetail1;
	}

	
	@Override
	public int updateByPrimaryKeySelective(MyItemDetail record) {
		return myItemDetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MyItemDetail record) {
				
		record.setId(null);
		
				
		return myItemDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MyItemDetail record, MyItemDetailExample example) {
		
		return myItemDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MyItemDetail record, MyItemDetailExample example) {
		
		return myItemDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<MyItemDetail> selectByExample(MyItemDetailExample example) {
		
		return myItemDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MyItemDetailExample example) {
		
		return myItemDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MyItemDetailExample example) {
		
		return myItemDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param myItemDetail
	 * @return
	 */
	public int updateVisible(MyItemDetail myItemDetail) {
		return myItemDetailMapper.updateByPrimaryKeySelective(myItemDetail);
	}


}
