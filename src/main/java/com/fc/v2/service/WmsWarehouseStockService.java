package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.fc.v2.mapper.auto.JshDepotMapper;
import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.WmsWarehouseStockMapper;
import com.fc.v2.model.auto.WmsWarehouseStock;
import com.fc.v2.model.auto.WmsWarehouseStockExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 主数据-库存 WmsWarehouseStockService
 * @Title: WmsWarehouseStockService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:38:35  
 **/
@Service
@Transactional
public class WmsWarehouseStockService implements BaseService<WmsWarehouseStock, WmsWarehouseStockExample>{
	@Autowired
	private WmsWarehouseStockMapper wmsWarehouseStockMapper;
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarfpriceMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;  	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	@Autowired
	private JshDepotMapper jshDepotMapper;  	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;  	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;  	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<WmsWarehouseStock> list(Tablepar tablepar,WmsWarehouseStock wmsWarehouseStock){
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<WmsWarehouseStock> list= wmsWarehouseStockMapper.getList(wmsWarehouseStock);
	        PageInfo<WmsWarehouseStock> pageInfo = new PageInfo<WmsWarehouseStock>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			WmsWarehouseStockExample example=new WmsWarehouseStockExample();
			example.createCriteria().andSidIn(stringB);
			return wmsWarehouseStockMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public WmsWarehouseStock selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return wmsWarehouseStockMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(WmsWarehouseStock record) {
		return wmsWarehouseStockMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(WmsWarehouseStock record) {
		int insertSelective=0;
		String xs = record.getXs();
		if(xs.trim().equals("")) {
			record.setXs(null);
		}
		
		WmsWarehouseStock oneByParams = wmsWarehouseStockMapper.getOneByParams(record);
		if(oneByParams==null) {
			record.setSid(null);
			record.setCreatedTime(new Date());
			record.setCreatedUserSid(ShiroUtils.getUserId());
			insertSelective= wmsWarehouseStockMapper.insertSelective(record);
		}
		
		return insertSelective;
	}
	
	
	@Override
	public int updateByExampleSelective(WmsWarehouseStock record, WmsWarehouseStockExample example) {
		
		return wmsWarehouseStockMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(WmsWarehouseStock record, WmsWarehouseStockExample example) {
		
		return wmsWarehouseStockMapper.updateByExample(record, example);
	}

	@Override
	public List<WmsWarehouseStock> selectByExample(WmsWarehouseStockExample example) {
		
		return wmsWarehouseStockMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(WmsWarehouseStockExample example) {
		
		return wmsWarehouseStockMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(WmsWarehouseStockExample example) {
		
		return wmsWarehouseStockMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param wmsWarehouseStock
	 * @return
	 */
	public int updateVisible(WmsWarehouseStock wmsWarehouseStock) {
		return wmsWarehouseStockMapper.updateByPrimaryKeySelective(wmsWarehouseStock);
	}

	public int upload(List<WmsWarehouseStock> execltoList) {
		int insertSelective = 0;
		for (WmsWarehouseStock wmsWarehouseStock : execltoList) {
			String materialName = wmsWarehouseStock.getMatterNo();
			if(materialName!=null) {
				String trim = materialName.trim();
				if(trim.equals("")) {
					materialName=null;
				}
			}
			String spesc = wmsWarehouseStock.getMatterDescribe();
			if(spesc!=null) {
				String trim = spesc.trim();
				if(trim.equals("")) {
					spesc=null;
				}
			}
			Long stockNumber = wmsWarehouseStock.getStockNumber();
			
			String houseName = wmsWarehouseStock.getHouseName();
			if(houseName!=null) {
				String trim = houseName.trim();
				if(trim.equals("")) {
					houseName=null;
				}
			}
			String type = wmsWarehouseStock.getXs();
			if(type!=null) {
				String trim = type.trim();
				if(trim.equals("")) {
					type=null;
				}else {
					if(type.equals("成品")) {
						type="1";
					}else if(type.equals("半成品")) {
						type="2";
					}else if(type.equals("材料")) {
						type="3";
					}else if(type.equals("辅料")) {
						type="4";
					}
				}
			}
			String stockLocation = wmsWarehouseStock.getStockLocation();
			if(stockLocation!=null) {
				String trim = stockLocation.trim();
				if(trim.equals("")) {
					stockLocation=null;
				}
			}
			if(materialName==null||spesc==null||stockNumber==null||houseName==null) {
				continue;
			}
			wmsWarehouseStock.setXs(type);
			WmsWarehouseStock wmsWarehouseStock1 =wmsWarehouseStockMapper.getOneByParams(wmsWarehouseStock);
			if(wmsWarehouseStock1==null) {
				wmsWarehouseStock.setCreatedTime(new Date());
				wmsWarehouseStock.setCreatedUserSid(ShiroUtils.getUserId());
				
				//
				JshDepot jdp =jshDepotMapper.getByName(houseName);
				if(jdp!=null) {
					
					wmsWarehouseStock.setHouseNo(jdp.getId()+"");
					insertSelective = wmsWarehouseStockMapper.insertSelective(wmsWarehouseStock);
				}else {
					continue;
				}
			}
		}
		return insertSelective;
	}

	public int chukuSave(WmsWarehouseStock wmsWarehouseStock) {
		//修改库存数量
		int updateByPrimaryKeySelective =0;
		WmsWarehouseStock selectByPrimaryKey = wmsWarehouseStockMapper.selectByPrimaryKey(wmsWarehouseStock.getSid());
		if(selectByPrimaryKey!=null) {
			Long stockNumber = selectByPrimaryKey.getStockNumber();
			Long stockNumber2 = wmsWarehouseStock.getStockNumber();
			Long stockNumberx = stockNumber-stockNumber2;
			if(stockNumberx<0) {
				return 3;
			}
			selectByPrimaryKey.setStockNumber(stockNumberx);
			 updateByPrimaryKeySelective = wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
			//创建出库明细
			if(updateByPrimaryKeySelective>0) {
				WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail= new WmsWarehouseDeliveryDetail();
				wmsWarehouseDeliveryDetail.setMatterNo(selectByPrimaryKey.getMatterNo());
				wmsWarehouseDeliveryDetail.setMatterDesc(selectByPrimaryKey.getMatterDescribe());
				wmsWarehouseDeliveryDetail.setModifiedTime(new Date());
				wmsWarehouseDeliveryDetail.setModifiedUserSid(ShiroUtils.getUserId());
				wmsWarehouseDeliveryDetail.setMatterNumber(wmsWarehouseStock.getStockNumber());
				wmsWarehouseDeliveryDetail.setStockId(selectByPrimaryKey.getSid());
				wmsWarehouseDeliveryDetail.setHouseNo(selectByPrimaryKey.getHouseNo());
				wmsWarehouseDeliveryDetail.setQualityNo(wmsWarehouseStock.getX());
				wmsWarehouseDeliveryDetail.setX(2);
				wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
			}
		}
		return updateByPrimaryKeySelective;
		
		
	}

	public int rukuSave(WmsWarehouseStock wmsWarehouseStock) {
		//修改库存数量
		int updateByPrimaryKeySelective =0;
		WmsWarehouseStock selectByPrimaryKey = wmsWarehouseStockMapper.selectByPrimaryKey(wmsWarehouseStock.getSid());
		if(selectByPrimaryKey!=null) {
			Long stockNumber = selectByPrimaryKey.getStockNumber();
			Long stockNumber2 = wmsWarehouseStock.getStockNumber();
			Long stockNumberx = stockNumber+stockNumber2;
			selectByPrimaryKey.setStockNumber(stockNumberx);
			 updateByPrimaryKeySelective = wmsWarehouseStockMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
			//创建出库明细
			if(updateByPrimaryKeySelective>0) {
				WmsWarehouseEntryDetail wmsWarehouseDeliveryDetail= new WmsWarehouseEntryDetail();
				wmsWarehouseDeliveryDetail.setMatterNo(selectByPrimaryKey.getMatterNo());
				wmsWarehouseDeliveryDetail.setMatterDesc(selectByPrimaryKey.getMatterDescribe());
				wmsWarehouseDeliveryDetail.setModifiedTime(new Date());
				wmsWarehouseDeliveryDetail.setModifiedUserSid(ShiroUtils.getUserId());
				wmsWarehouseDeliveryDetail.setMatterNumber(wmsWarehouseStock.getStockNumber());
				wmsWarehouseDeliveryDetail.setHouseNo(selectByPrimaryKey.getHouseNo());
				wmsWarehouseDeliveryDetail.setStockId(selectByPrimaryKey.getSid());
				wmsWarehouseDeliveryDetail.setQualityNo(wmsWarehouseStock.getX());
				wmsWarehouseDeliveryDetail.setX(2);
				wmsWarehouseEntryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
			}
		}
		return updateByPrimaryKeySelective;
	}


}
