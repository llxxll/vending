package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MoneyReviewMapper;
import com.fc.v2.model.auto.MoneyReview;
import com.fc.v2.model.auto.MoneyReviewExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 提现审核 MoneyReviewService
 * @Title: MoneyReviewService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2024-01-23 22:17:52  
 **/
@Service
public class MoneyReviewService implements BaseService<MoneyReview, MoneyReviewExample>{
	@Autowired
	private MoneyReviewMapper moneyReviewMapper;
	
      	   	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MoneyReview> list(Tablepar tablepar,MoneyReview moneyReview){
	        MoneyReviewExample testExample=new MoneyReviewExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(moneyReview);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MoneyReview> list= moneyReviewMapper.selectByExample(testExample);
	        PageInfo<MoneyReview> pageInfo = new PageInfo<MoneyReview>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MoneyReviewExample example=new MoneyReviewExample();
			example.createCriteria().andIdIn(stringB);
			return moneyReviewMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MoneyReview selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return moneyReviewMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MoneyReview record) {
		return moneyReviewMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MoneyReview record) {
				
		record.setId(null);
		
				
		return moneyReviewMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MoneyReview record, MoneyReviewExample example) {
		
		return moneyReviewMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MoneyReview record, MoneyReviewExample example) {
		
		return moneyReviewMapper.updateByExample(record, example);
	}

	@Override
	public List<MoneyReview> selectByExample(MoneyReviewExample example) {
		
		return moneyReviewMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MoneyReviewExample example) {
		
		return moneyReviewMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MoneyReviewExample example) {
		
		return moneyReviewMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param moneyReview
	 * @return
	 */
	public int updateVisible(MoneyReview moneyReview) {
		return moneyReviewMapper.updateByPrimaryKeySelective(moneyReview);
	}


}
