package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyYiyuangaikuangMapper;
import com.fc.v2.model.auto.YyYiyuangaikuang;
import com.fc.v2.model.auto.YyYiyuangaikuangExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院概况 YyYiyuangaikuangService
 * @Title: YyYiyuangaikuangService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-02-13 12:17:30  
 **/
@Service
public class YyYiyuangaikuangService implements BaseService<YyYiyuangaikuang, YyYiyuangaikuangExample>{
	@Autowired
	private YyYiyuangaikuangMapper yyYiyuangaikuangMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyYiyuangaikuang> list(Tablepar tablepar,YyYiyuangaikuang yyYiyuangaikuang){
	        YyYiyuangaikuangExample testExample=new YyYiyuangaikuangExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyYiyuangaikuang);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyYiyuangaikuang> list= yyYiyuangaikuangMapper.selectByExample(testExample);
	        PageInfo<YyYiyuangaikuang> pageInfo = new PageInfo<YyYiyuangaikuang>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyYiyuangaikuangExample example=new YyYiyuangaikuangExample();
			example.createCriteria().andIdIn(stringB);
			return yyYiyuangaikuangMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyYiyuangaikuang selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyYiyuangaikuangMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyYiyuangaikuang record) {
		record.setUpdateTime(new Date());
		return yyYiyuangaikuangMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyYiyuangaikuang record) {
				
		record.setId(null);
		
				
		return yyYiyuangaikuangMapper.insertSelective(record);
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyYiyuangaikuang record, YyYiyuangaikuangExample example) {
		
		return yyYiyuangaikuangMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyYiyuangaikuang record, YyYiyuangaikuangExample example) {
		
		return yyYiyuangaikuangMapper.updateByExample(record, example);
	}

	@Override
	public List<YyYiyuangaikuang> selectByExample(YyYiyuangaikuangExample example) {
		
		return yyYiyuangaikuangMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyYiyuangaikuangExample example) {
		
		return yyYiyuangaikuangMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyYiyuangaikuangExample example) {
		
		return yyYiyuangaikuangMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyYiyuangaikuang
	 * @return
	 */
	@Transactional
	public int updateVisible(YyYiyuangaikuang yyYiyuangaikuang) {
		return yyYiyuangaikuangMapper.updateByPrimaryKeySelective(yyYiyuangaikuang);
	}


	@Transactional
	public int updateStatusByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyYiyuangaikuangMapper.updateStatusByPrimaryKey(aLong);
	}

	@Transactional
	public int shangjiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyYiyuangaikuangMapper.shangjiaByPrimaryKey(aLong);
	}

	@Transactional
	public int xiajiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyYiyuangaikuangMapper.xiajiaByPrimaryKey(aLong);
	}

    public YyYiyuangaikuang selectOne() {
		return yyYiyuangaikuangMapper.selectOne();
    }

    public int updateByPrimaryKey(YyYiyuangaikuang yyYiyuangaikuang) {
		return yyYiyuangaikuangMapper.updateByPrimaryKey(yyYiyuangaikuang);
    }
}
