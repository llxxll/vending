package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;

import com.fc.v2.model.auto.OrderBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderMapper;
import com.fc.v2.model.auto.Order;
import com.fc.v2.model.auto.OrderExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 扫码支付订单 OrderService
 * @Title: OrderService.java 
 * @Package com.fc.v2.service 
 * @author lxl_自动生成
 * @email ${email}
 * @date 2023-08-30 22:03:02  
 **/
@Service
public class OrderService implements BaseService<Order, OrderExample>{
	@Autowired
	private OrderMapper orderMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Order> list(Tablepar tablepar,Order order){
	        OrderExample testExample=new OrderExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(order);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("order_id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Order> list= orderMapper.selectByExample(testExample);
	        PageInfo<Order> pageInfo = new PageInfo<Order>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderExample example=new OrderExample();
			example.createCriteria().andOrderIdIn(stringB);
			return orderMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public Order selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Order record) {
		return orderMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Order record) {
		if(record.getOrderId()==null){
			record.setOrderId(null);
		}
		return orderMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Order record, OrderExample example) {
		
		return orderMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Order record, OrderExample example) {
		
		return orderMapper.updateByExample(record, example);
	}

	@Override
	public List<Order> selectByExample(OrderExample example) {
		
		return orderMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderExample example) {
		
		return orderMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderExample example) {
		
		return orderMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param order
	 * @return
	 */
	public int updateVisible(Order order) {
		return orderMapper.updateByPrimaryKeySelective(order);
	}


	public Order findOne(Long orderId) {
		return orderMapper.selectByPrimaryKey(orderId);
	}
	public Order findByOutTradeNo(String outTradeNo) {
		return orderMapper.findByOutTradeNo(outTradeNo);
	}


}
