package com.fc.v2.service;

import java.util.*;
import java.text.SimpleDateFormat;

import com.fc.v2.model.auto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshInvoiceMapper;
import com.fc.v2.mapper.auto.MyItemDetailMapper;
import com.fc.v2.mapper.auto.MyItemMapper;
import com.fc.v2.mapper.auto.MyItemReportMapper;
import com.fc.v2.mapper.auto.MyItemTypeMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 *  MyItemService
 * @Title: MyItemService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-08-28 21:49:04  
 **/
@Service
@Transactional
public class MyItemService implements BaseService<MyItem, MyItemExample>{
	@Autowired
	private MyItemMapper myItemMapper;
	@Autowired
	private MyItemDetailMapper myItemDetailMapper;
	@Autowired
	private JshInvoiceMapper jshInvoiceMapper;
	@Autowired
	private MyItemReportMapper myItemReportMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private MyItemTypeMapper myItemTypeMapper;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MyItem> list(Tablepar tablepar,MyItem myItem){
	        
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MyItem> list= myItemMapper.getList(myItem);
	        for (MyItem myItem2 : list) {
				if(myItem2.getManager()!=null) {
					TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(myItem2.getManager());
					if(selectByPrimaryKey!=null) {
						myItem2.setMangerName(selectByPrimaryKey.getNickname());
					}
				}
				
			}
	        
	        
	        PageInfo<MyItem> pageInfo = new PageInfo<MyItem>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				int result=0;
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			for (Long long1 : stringB) {
				MyItem selectByPrimaryKey = myItemMapper.selectByPrimaryKey(long1);
				//获取明细
				result = myItemDetailMapper.deleteByItemNo(selectByPrimaryKey.getItemNo());
				//获取报表明细
				if(result>0) {
					result = myItemReportMapper.deleteByItemNo(selectByPrimaryKey.getItemNo());
					if(result>0) {
					result = myItemMapper.deleteByPrimaryKey(long1);
					}
				}
				
			}
			
		return result;
				
	}
	
	
	@Override
	public MyItem selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			
			MyItem selectByPrimaryKey2 = myItemMapper.selectByPrimaryKey(id1);
			if(selectByPrimaryKey2.getManager()!=null) {
				TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(selectByPrimaryKey2.getManager());
				if(selectByPrimaryKey!=null) {
					selectByPrimaryKey2.setMangerName(selectByPrimaryKey.getNickname());
				}
			}
			MyItemType selectByPrimaryKey = myItemTypeMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey2.getItemtype()));
			if(selectByPrimaryKey!=null) {
				
				selectByPrimaryKey2.setItemtypeName(selectByPrimaryKey.getTypeName());
			}
			return selectByPrimaryKey2;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MyItem record) {
		List<MyItemDetail> detailList = record.getDetailList();
		List<Long> delId = record.getDelId();
		if(delId!=null) {
			if(delId.size()>0) {
				for (Long long1 : delId) {
					if(long1!=null) {
						
						myItemDetailMapper.deleteByPrimaryKey(long1);
					}
				}
			}
		}
		
		
		
		if(detailList!=null) {
			if(detailList.size()>0) {
				for (MyItemDetail myItemDetail : detailList) {
					//根据id 查询 如果存在 则更新 不存在则插入
					if(myItemDetail.getId()==null) {
						myItemDetail.setItemNo(record.getItemNo());
						myItemDetail.setCreater(ShiroUtils.getUserId());
						myItemDetail.setCreateTime(new Date());
						myItemDetailMapper.insertSelective(myItemDetail);
					}else {
						myItemDetailMapper.updateByPrimaryKeySelective(myItemDetail);
					}
					
				}
			}
		}
		return myItemMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MyItem record) {
				
		record.setId(null);
		//生成编号
		JshInvoice jshin = new JshInvoice();
		jshin.setOrderNo("项目管理");
		jshInvoiceMapper.insertSelective(jshin);
		//获取当前时间
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		String format = simpleDateFormat.format(new Date());
		record.setItemNo("XM"+"-"+format+"-"+jshin.getId());
		record.setCreater(ShiroUtils.getUserId());
		record.setCreateTime(new Date());
		record.setTf("2");
		int insertSelective = myItemMapper.insertSelective(record);
		if(insertSelective>0) {
			List<MyItemDetail> detailList = record.getDetailList();
			if(detailList!=null) {
				if(detailList.size()>0) {
					for (MyItemDetail myItemDetail : detailList) {
						myItemDetailMapper.deleteByPrimaryKey(myItemDetail.getId());
						myItemDetail.setItemNo(record.getItemNo());
						myItemDetail.setCreater(ShiroUtils.getUserId());
						myItemDetail.setCreateTime(new Date());
						myItemDetailMapper.insertSelective(myItemDetail);
					}
				}
			}
			
			
		}
				
		return insertSelective;
	}
	
	
	@Override
	public int updateByExampleSelective(MyItem record, MyItemExample example) {
		
		return myItemMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MyItem record, MyItemExample example) {
		
		return myItemMapper.updateByExample(record, example);
	}

	@Override
	public List<MyItem> selectByExample(MyItemExample example) {
		
		return myItemMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MyItemExample example) {
		
		return myItemMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MyItemExample example) {
		
		return myItemMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param myItem
	 * @return
	 */
	public int updateVisible(MyItem myItem) {
		return myItemMapper.updateByPrimaryKeySelective(myItem);
	}

	public int wancheng(String ids) {
		MyItem myItem = new MyItem();
		myItem.setId(Long.valueOf(ids));
		myItem.setStatus("3");
		myItem.setWanchengtime(new Date());
		return myItemMapper.updateByPrimaryKeySelective(myItem);
	}

	public int kaishi(String ids) {
		MyItem myItem = new MyItem();
		myItem.setId(Long.valueOf(ids));
		myItem.setStatus("2");
		myItem.setKaishitime(new Date());
		return myItemMapper.updateByPrimaryKeySelective(myItem);
	}

	public int chexiao(String ids) {
		MyItem selectByPrimaryKey = myItemMapper.selectByPrimaryKey(Long.valueOf(ids));
		int updateByPrimaryKeySelective =0;
		if(selectByPrimaryKey.getStatus().equals("2")) {
			selectByPrimaryKey.setStatus("1");
			selectByPrimaryKey.setKaishitime(null);
			updateByPrimaryKeySelective =myItemMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		}else if(selectByPrimaryKey.getStatus().equals("3")) {
			selectByPrimaryKey.setStatus("2");
			selectByPrimaryKey.setWanchengtime(null);
			updateByPrimaryKeySelective = myItemMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		}
		return updateByPrimaryKeySelective;
	}

	public int huibaoSave(MyItemReport myItem) {
		if(myItem.getTf().trim().equals("")) {
			myItem.setTf(null);
		}
		myItem.setCreater(ShiroUtils.getUserId());
		myItem.setCreateTime(new Date());
		return myItemReportMapper.insertSelective(myItem);
	}

	public int jiaji(String ids) {
		MyItem selectByPrimaryKey = myItemMapper.selectByPrimaryKey(Long.valueOf(ids));
		selectByPrimaryKey.setTf("1");
		return myItemMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
	}


}
