package com.fc.v2.service;

import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanOudetailGongyingMapper;
import com.fc.v2.mapper.auto.PlanOudetailMapper;
import com.fc.v2.mapper.auto.PlanOutMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsSonghuoMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOudetailExample;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanOut;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 生产计划明细 PlanOudetailService
 * @Title: PlanOudetailService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-03-02 14:13:11  
 **/

@Service
@Transactional
public class PlanOudetailService implements BaseService<PlanOudetail, PlanOudetailExample>{
	@Autowired
	private PlanOudetailMapper planOudetailMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private WmsSonghuoMapper wmsSonghuoMapper;
	@Autowired
	private PlanOudetailGongyingMapper planOudetailGongyingMapper;
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarfpriceMapper;
	@Autowired
	private PlanOutMapper planOutMapper;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanOudetail> list(Tablepar tablepar,PlanOudetail planOudetail){
	       
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanOudetail> list= planOudetailMapper.getList(planOudetail);
	        for (PlanOudetail planOudetail2 : list) {
	        	String departmentOne = planOudetail2.getField3();
	        	TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(departmentOne));
	        	planOudetail2.setDepartmentOneName(selectByPrimaryKey.getNickname());
			}
	        PageInfo<PlanOudetail> pageInfo = new PageInfo<PlanOudetail>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
		int deleteByPrimaryKey=0;
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			for (Long long1 : stringB) {
				PlanOudetail planOudetail = planOudetailMapper.selectByPrimaryKey(long1);
				if(planOudetail.getField1().equals("0")) {
					deleteByPrimaryKey = planOudetailMapper.deleteByPrimaryKey(long1);
				}
			}
		return deleteByPrimaryKey;
				
	}
	
	
	@Override
	public PlanOudetail selectByPrimaryKey(String id) {
		PlanOudetail planOudetail = planOudetailMapper.selectByPrimaryKey(Long.valueOf(id));
			JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planOudetail.getMateriaNo()));
			planOudetail.setTuName(selectByPrimaryKey.getName());
			planOudetail.setTuNo(selectByPrimaryKey.getModel());
			JshMaterial selectByPrimaryKey2 = jshMaterialMapper.selectByPrimaryKey(Long.valueOf(planOudetail.getStandard()));
			planOudetail.setClName(selectByPrimaryKey2.getName());
			planOudetail.setClProperty(selectByPrimaryKey2.getModel());
			//获取领料 数量
			//获取开单数量
			Integer lingliaoCount = wmsWarehouseDeliveryDetailMapper.getCountbydevlied(planOudetail.getId()+"", "1");
        	if(lingliaoCount==null) {
        		lingliaoCount=0;
        	}
        	planOudetail.setLingliaoCount(lingliaoCount);
        	// count5;已开单
        	Integer count5 = planOudetailGongyingMapper.getCountbydevlied(planOudetail.getId()+"");
        	if(count5==null) {
        		count5=0;
        	}
        	planOudetail.setCount5(count5);
        	Integer count = planOudetail.getCount();
        	if(count==null) {
        		
        		planOudetail.setZuidaCount(0);
        	}else {
        		if(lingliaoCount==0) {
        			
        			planOudetail.setZuidaCount(planOudetail.getCount()-count5);
        		}else {
        			planOudetail.setZuidaCount(planOudetail.getCount()-lingliaoCount);
        			
        		}
        	}
        	
        	//该物料 在途数
        	//备料  开单数-入库数  条件  物料号  供应商 1,3
        	Integer zaitucount =planOudetailGongyingMapper.getzaitucount(planOudetail.getMateriaNo(),planOudetail.getDepartmentOne());
        	Integer zaitucountxx =planOudetailGongyingMapper.getzaitucountzzz(planOudetail.getMateriaNo(),planOudetail.getDepartmentOne());
        	if(zaitucount==null) {
        		zaitucount=0;
        	}
        	if(zaitucountxx==null) {
        		zaitucountxx=0;
        	}
        	Integer x1 =zaitucount-zaitucountxx;
        	
        	planOudetail.setZaitucount(x1);
        	//去料  领料数 -已入库数 条件 物料号 供应商  (0,1,3)
        	Integer zaitucount1 =planOudetailGongyingMapper.getzaitulingcountquliao(planOudetail.getMateriaNo(),planOudetail.getDepartmentOne());
        	Integer zaitucount2 =planOudetailGongyingMapper.getzaiturucountquliao(planOudetail.getMateriaNo(),planOudetail.getDepartmentOne());
        	if(zaitucount1==null) {
        		zaitucount1=0;
        	}
        	if(zaitucount2==null) {
        		zaitucount2=0;
        	}
        	Integer x2 =zaitucount1-zaitucount2;
        	planOudetail.setZaitucountqu(x2);
        	planOudetail.setZaitucountzong(x1+x2);
			return planOudetail;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanOudetail record) {
		return planOudetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanOudetail record) {
				
		record.setId(null);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");

		// 复制保留无需修改的数据
		record.setProduceNo("WWGD"+ShiroUtils.getUserId()+ simpleDateFormat.format(new Date()));
		record.setCreateTime(new Date());
		record.setCreator(ShiroUtils.getUserId());
		int insertSelective = planOudetailMapper.insertSelective(record);
		if(insertSelective>0) {
			List<PlanOudetailGongying> planOudetailGongyingList = record.getPlanOudetailGongyingList();
			for (PlanOudetailGongying planOudetailGongying : planOudetailGongyingList) {
				String departmentOne = planOudetailGongying.getDepartmentOne();
				if(departmentOne!=null) {
					String trim = departmentOne.trim();
					if(trim.equals("")) {
						departmentOne=null;
					}
				}
				String departmentTwo = planOudetailGongying.getDepartmentTwo();
				if(departmentTwo!=null) {
					String trim = departmentTwo.trim();
					if(trim.equals("")) {
						departmentTwo=null;
					}
				}
				if(departmentOne!=null) {
					
					JshMaterialCarfprice jshMaterialCarfprice =jshMaterialCarfpriceMapper.selectByNameAndMatID(planOudetailGongying.getDepartmentOne(),planOudetailGongying.getMaterial());
					if(jshMaterialCarfprice==null) {
						jshMaterialCarfprice= new JshMaterialCarfprice();
						jshMaterialCarfprice.setTtxx(planOudetailGongying.getDepartmentOne());
						jshMaterialCarfprice.setMaterialId(Long.valueOf(planOudetailGongying.getMaterial()));
						jshMaterialCarfprice.setCreateUser(ShiroUtils.getUserId());
						jshMaterialCarfprice.setCreateTime(new Date());
						jshMaterialCarfpriceMapper.insertSelective(jshMaterialCarfprice);
					}
				}
				if(departmentTwo!=null) {
					
					PlanOut planOut =planOutMapper.selectByNameAndMatID(planOudetailGongying.getDepartmentTwo(),planOudetailGongying.getMaterial());
					if(planOut==null) {
						planOut= new PlanOut();
						planOut.setName(planOudetailGongying.getDepartmentTwo());
						planOut.setUpdater(Long.valueOf(planOudetailGongying.getMaterial()));
						planOut.setCreator(ShiroUtils.getUserId());
						planOut.setCreateTime(new Date());
						planOutMapper.insertSelective(planOut);
					}
				}
				planOudetailGongying.setProduceNo(record.getProduceNo());
				if(record.getField2().equals("1")) {
					
					planOudetailGongying.setField1("1");
				}else {
					planOudetailGongying.setField1("0");
					
				}
				planOudetailGongying.setCreateTime(new Date());
				planOudetailGongying.setCreator(ShiroUtils.getUserId());
				insertSelective = planOudetailGongyingMapper.insertSelective(planOudetailGongying);
				
				if(record.getField2().equals("0")) {
					
					//创建 出库单
					WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseDeliveryDetail();
					wmsWarehouseDeliveryDetail.setHouseNo(record.getStandard());
					wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(planOudetailGongying.getCount()));
					
					wmsWarehouseDeliveryDetail.setDeliverySid(record.getProduceNo());
					wmsWarehouseDeliveryDetail.setMatterNo(planOudetailGongying.getMaterial());
					wmsWarehouseDeliveryDetail.setMatterDesc(planOudetailGongying.getDepartmentTwo());
					wmsWarehouseDeliveryDetail.setX(1);
					wmsWarehouseDeliveryDetail.setQualityNo("1");
					wmsWarehouseDeliveryDetail.setUnitName(planOudetailGongying.getId()+"");
					wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
					wmsWarehouseDeliveryDetail.setCreatedUserSid(ShiroUtils.getUserId());
					insertSelective = wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
					
				}	
				
			}
		}
		return insertSelective;
	}
	
	
	@Override
	public int updateByExampleSelective(PlanOudetail record, PlanOudetailExample example) {
		
		return planOudetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanOudetail record, PlanOudetailExample example) {
		
		return planOudetailMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanOudetail> selectByExample(PlanOudetailExample example) {
		
		return planOudetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanOudetailExample example) {
		
		return planOudetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanOudetailExample example) {
		
		return planOudetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planOudetail
	 * @return
	 */
	public int updateVisible(PlanOudetail planOudetail) {
		return planOudetailMapper.updateByPrimaryKeySelective(planOudetail);
	}

	public int kaidanSave(PlanOudetail planOudetail) {
		PlanOudetail selectByPrimaryKey = planOudetailMapper.selectByPrimaryKey(planOudetail.getId());
		selectByPrimaryKey.setField1("1");
		
		//修改成功以后 添加委外单
		PlanOudetailGongying planOudetailGongying = new PlanOudetailGongying();
		Integer songhuocount = planOudetail.getSonghuocount();
			//BeanUtils.copyProperties(selectByPrimaryKey, record);
			//record.set
			planOudetailGongying.setProduceNo(selectByPrimaryKey.getProduceNo());
			planOudetailGongying.setStock(selectByPrimaryKey.getId());
			planOudetailGongying.setId(null);
			planOudetailGongying.setCount(songhuocount);
			planOudetailGongying.setCreateTime(new Date());
			planOudetailGongying.setCreator(ShiroUtils.getUserId());
//			planOudetailGongying.setMaterial(selectByPrimaryKey.getStandard());
//			planOudetailGongying.setDepartmentTwo(selectByPrimaryKey.getDepartmentTwo());
			//planOudetailGongying.setField3(planOudetail.getMaterialName());
			planOudetailGongying.setField2(planOudetail.getField2());
			planOudetailGongying.setDepartmentOne(planOudetail.getDepartmentOne());
			
			if(planOudetail.getField2().equals("1")) {
				planOudetailGongying.setStandard(planOudetail.getStandard());
				planOudetailGongying.setField1("0");
				
			}else {
				//已领料
				planOudetailGongying.setStandard(null);
				planOudetailGongying.setField1("1");
			}
			int insertSelective = planOudetailGongyingMapper.insertSelective(planOudetailGongying);
			
			
			if(insertSelective>0) {
				if(planOudetail.getField2().equals("1")) {
					
					//创建 出库单
					WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseDeliveryDetail();
					wmsWarehouseDeliveryDetail.setHouseNo(planOudetail.getStandard());
					wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(songhuocount));
					
					wmsWarehouseDeliveryDetail.setDeliverySid(selectByPrimaryKey.getId()+"");
					wmsWarehouseDeliveryDetail.setMatterNo(selectByPrimaryKey.getStandard());
					wmsWarehouseDeliveryDetail.setX(1);
					wmsWarehouseDeliveryDetail.setQualityNo("1");
					wmsWarehouseDeliveryDetail.setUnitName(planOudetailGongying.getId()+"");
					wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
					wmsWarehouseDeliveryDetail.setCreatedUserSid(ShiroUtils.getUserId());
					insertSelective = wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
					
				}	
			}
			if(insertSelective>0) {
				insertSelective = planOudetailMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
				
			}
			return insertSelective;
					
	}

	public int upload(List<PlanOudetail> execltoList) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String no ="WX"+ simpleDateFormat.format(new Date());
		int insertSelective =0;
		for (PlanOudetail plan : execltoList) {
			//根据物料号 获取物料数据
			String materialName = plan.getMaterialName();
			if(materialName!=null) {
				String trim = materialName.trim();
				if(trim.equals("")) {
					materialName=null;
				}
			}
			//材料
			String standardName = plan.getStandard();
			if(standardName!=null) {
				String trim = standardName.trim();
				if(trim.equals("")) {
					standardName="/";
				}
			}else {
				standardName="/";
			}
			//材料规格
			String field1 = plan.getDepartmentTwo();
			if(field1!=null) {
				String trim = field1.trim();
				if(trim.equals("")) {
					field1="/";
				}
			}else {
				field1="/";
			}
			// 计划数量
			Integer planNumber = plan.getCount();
			
			// 备注
			String material = plan.getMaterial();
			if(material!=null) {
				String trim = material.trim();
				if(trim.equals("")) {
					material=null;
				}
			}
			String field3 = plan.getField3();
			if(field3!=null) {
				String trim = field3.trim();
				if(trim.equals("")) {
					field3=null;
				}
			}
			
			String materialNo = plan.getMateriaNo();
			if(materialNo!=null) {
				String trim = materialNo.trim();
				if(trim.equals("")) {
					materialNo=null;
				}
			}
			String departmentOne = plan.getDepartmentOne();
			if(departmentOne!=null) {
				String trim = departmentOne.trim();
				if(trim.equals("")) {
					departmentOne=null;
				}else {
					TsysUser selectByname = tsysUserMapper.selectByname(departmentOne);
					if(selectByname!=null) {
						
						departmentOne=selectByname.getId()+"";
					}else {
						departmentOne=null;
					}
				}
			}
			
			
			if(materialName==null||standardName==null||field1==null||planNumber==null) {
				continue;
			}

			JshMaterial findByTitle = jshMaterialMapper.selectByName(materialName);
			if(findByTitle==null) {
				findByTitle =new JshMaterial();
				findByTitle.setName(materialName);
				findByTitle.setModel(materialNo);
				findByTitle.setCategoryId(1l);
				jshMaterialMapper.insertSelective(findByTitle);
			}
			
			plan.setMateriaNo(String.valueOf(findByTitle.getId()));
			//根据 材料 和规格获取 材料数据
			JshMaterial findByRawNameAndRawSpec = jshMaterialMapper.selectByNameandmodel(standardName,field1);
			if(findByRawNameAndRawSpec==null) {
				findByRawNameAndRawSpec = new JshMaterial();
				findByRawNameAndRawSpec.setName(standardName);
				findByRawNameAndRawSpec.setModel(field1);
				findByRawNameAndRawSpec.setCategoryId(2l);
				jshMaterialMapper.insertSelective(findByRawNameAndRawSpec);
			}
			
			
			
			
			plan.setDepartmentOne(departmentOne);
			plan.setStandard(String.valueOf(findByRawNameAndRawSpec.getId()));
			plan.setProduceNo(no);
			plan.setMaterial(material);
			plan.setCreator(ShiroUtils.getUserId());
			plan.setCreateTime(new Date());
			plan.setField3(field3);
			plan.setMaterial(material);
			plan.setMaterialName(null);
			plan.setDepartmentTwo(null);
			insertSelective = planOudetailMapper.insertSelective(plan);

		}
		return insertSelective;
	}

	public int piliangkaidansave(String ids,String departmentOne) {
		int deleteByPrimaryKey=0;
		Long[] integers = ConvertUtil.toLongArray(",", ids);
		List<Long> stringB = Arrays.asList(integers);
		for (Long long1 : stringB) {
			PlanOudetail planOudetail = planOudetailMapper.selectByPrimaryKey(long1);
			Integer countbydevlied = planOudetailGongyingMapper.getCountbydevlied(planOudetail.getId()+"");
			if(countbydevlied==null) {
				countbydevlied=0;
			}
			
			Integer count = planOudetail.getCount();
			Integer countx=count-countbydevlied;
			if(countx>0) {
				
			PlanOudetailGongying planOudetailGongying = new PlanOudetailGongying();
				planOudetailGongying.setProduceNo(planOudetail.getProduceNo());
				planOudetailGongying.setStock(planOudetail.getId());
				planOudetailGongying.setId(null);
				planOudetailGongying.setCount(countx);
				planOudetailGongying.setCreateTime(new Date());
				planOudetailGongying.setCreator(ShiroUtils.getUserId());
//				planOudetailGongying.setMaterial(planOudetail.getStandard());
//				planOudetailGongying.setDepartmentTwo(planOudetail.getDepartmentTwo());
//				planOudetailGongying.setField3(planOudetail.getMaterialName());
				planOudetailGongying.setField2("0");
				planOudetailGongying.setDepartmentOne(departmentOne);
					planOudetailGongying.setStandard(null);
					planOudetailGongying.setField1("1");
					deleteByPrimaryKey = planOudetailGongyingMapper.insertSelective(planOudetailGongying);
			
				planOudetail.setField1("1");
				deleteByPrimaryKey =planOudetailMapper.updateByPrimaryKeySelective(planOudetail);
			}
		}
	return deleteByPrimaryKey;
	}

	public int piliangquliaosave(String ids, String departmentOne, String standard) {
		int deleteByPrimaryKey=0;
		Long[] integers = ConvertUtil.toLongArray(",", ids);
		List<Long> stringB = Arrays.asList(integers);
		for (Long long1 : stringB) {
			PlanOudetail planOudetail = planOudetailMapper.selectByPrimaryKey(long1);
			Integer countbydevlied = wmsWarehouseDeliveryDetailMapper.getCountbydevlied(long1+"", "1");
			if(countbydevlied==null) {
				countbydevlied=0;
			}
			
			Integer count = planOudetail.getCount();
			Integer countx=count-countbydevlied;
			if(countx>0) {
				
			PlanOudetailGongying planOudetailGongying = new PlanOudetailGongying();
				planOudetailGongying.setProduceNo(planOudetail.getProduceNo());
				planOudetailGongying.setStock(planOudetail.getId());
				planOudetailGongying.setId(null);
				planOudetailGongying.setCount(countx);
				planOudetailGongying.setCreateTime(new Date());
				planOudetailGongying.setCreator(ShiroUtils.getUserId());
//				planOudetailGongying.setMaterial(planOudetail.getStandard());
//				planOudetailGongying.setDepartmentTwo(planOudetail.getDepartmentTwo());
//				planOudetailGongying.setField3(planOudetail.getMaterialName());
				planOudetailGongying.setField2("1");
				planOudetailGongying.setDepartmentOne(departmentOne);
				planOudetailGongying.setStandard(planOudetail.getStandard());
					planOudetailGongying.setStandard(null);
					planOudetailGongying.setField1("0");
					deleteByPrimaryKey = planOudetailGongyingMapper.insertSelective(planOudetailGongying);
					WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail = new WmsWarehouseDeliveryDetail();
					wmsWarehouseDeliveryDetail.setHouseNo(standard);
					wmsWarehouseDeliveryDetail.setMatterNumber(Long.valueOf(countx));
					
					wmsWarehouseDeliveryDetail.setDeliverySid(planOudetail.getId()+"");
					wmsWarehouseDeliveryDetail.setMatterNo(planOudetail.getStandard());
					wmsWarehouseDeliveryDetail.setX(1);
					wmsWarehouseDeliveryDetail.setQualityNo("1");
					wmsWarehouseDeliveryDetail.setUnitName(planOudetailGongying.getId()+"");
					wmsWarehouseDeliveryDetail.setCreatedTime(new Date());
					wmsWarehouseDeliveryDetail.setCreatedUserSid(ShiroUtils.getUserId());
					deleteByPrimaryKey = wmsWarehouseDeliveryDetailMapper.insertSelective(wmsWarehouseDeliveryDetail);
				planOudetail.setField1("1");
				deleteByPrimaryKey =planOudetailMapper.updateByPrimaryKeySelective(planOudetail);
			}
		}
	return deleteByPrimaryKey;
	}
}
