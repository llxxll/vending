package com.fc.v2.service;

import java.util.List;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshDepotMapper;
import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanProduceMaterialDetailMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderDetailPzMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderMapper;
import com.fc.v2.mapper.auto.PlanWorkOrderPzStatementMapper;
import com.fc.v2.mapper.auto.SysDepartmentMapper;
import com.fc.v2.mapper.auto.TsysUserMapper;
import com.fc.v2.mapper.auto.WmsWarehouseDeliveryDetailMapper;
import com.fc.v2.mapper.auto.WmsWarehouseEntryDetailMapper;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.model.auto.PlanWorkOrderDetailPz;
import com.fc.v2.model.auto.PlanWorkOrderDetailPzExample;
import com.fc.v2.model.auto.PlanWorkOrderPzStatement;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 工单明细 PlanWorkOrderDetailPzService
 * @Title: PlanWorkOrderDetailPzService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:39:43  
 **/
@Service
@Transactional
public class PlanWorkOrderDetailPzService implements BaseService<PlanWorkOrderDetailPz, PlanWorkOrderDetailPzExample>{
	@Autowired
	private PlanWorkOrderDetailPzMapper planWorkOrderDetailPzMapper;
	@Autowired
	private PlanWorkOrderMapper planWorkOrderMapper;
	@Autowired
	private WmsWarehouseEntryDetailMapper wmsWarehouseEntryDetailMapper;
	@Autowired
	private WmsWarehouseDeliveryDetailMapper wmsWarehouseDeliveryDetailMapper;
	@Autowired
	private TsysUserMapper tsysUserMapper;
	@Autowired
	private PlanWorkOrderPzStatementMapper planWorkOrderPzStatementMapper;
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarftPriceMapper;
	@Autowired
	private SysDepartmentMapper sysDepartmentMapper;
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private JshDepotMapper jshDepotMapper;
	@Autowired
	private PlanProduceMaterialDetailMapper planProduceMaterialDetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<PlanWorkOrderDetailPz> list(Tablepar tablepar,PlanWorkOrderDetailPz planWorkOrderDetailPz){
		
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanWorkOrderDetailPz> list= planWorkOrderDetailPzMapper.getList(planWorkOrderDetailPz);
	        for (PlanWorkOrderDetailPz planWorkOrderDetailPz2 : list) {
	        	//获取转序车间名称
	        	PlanWorkOrder selectOneByworno = planWorkOrderMapper.selectOneByworno(planWorkOrderDetailPz2.getWorkerOrderNo());
	        	
	        	planWorkOrderDetailPz2.setPlanworkstatus(selectOneByworno.getStatus());
	        	
	        		String field3 = planWorkOrderDetailPz2.getField3();
	        		if(field3!=null) {
	        			SysDepartment selectByPrimaryKey2 = sysDepartmentMapper.selectByPrimaryKey(Integer.valueOf(field3));
			        	if(selectByPrimaryKey2!=null) {
			        		planWorkOrderDetailPz2.setZhuanxuchejian(selectByPrimaryKey2.getDeptName());
			        	}
	        		}
	        		 Integer manager = planWorkOrderDetailPz2.getManager();
	        		if(manager!=null) {
	        			SysDepartment selectByPrimaryKey2 = sysDepartmentMapper.selectByPrimaryKey(Integer.valueOf(manager));
			        	if(selectByPrimaryKey2!=null) {
			        		planWorkOrderDetailPz2.setChejianname(selectByPrimaryKey2.getDeptName());
			        	}
	        		}
	        		
	        		
	        		String stockCount = planWorkOrderDetailPz2.getField1();
	        	if(stockCount!=null) {
	        		JshDepot selectByPrimaryKey3 = jshDepotMapper.selectByPrimaryKey(Long.valueOf(planWorkOrderDetailPz2.getField1()));
		        	//获取入库仓库名称
		        	if(selectByPrimaryKey3!=null) {
		        		planWorkOrderDetailPz2.setRukuhousename(selectByPrimaryKey3.getName());
		        	}
	        	}
	        	Integer worker = planWorkOrderDetailPz2.getWorker();
	        	if(worker!=null) {
	        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(worker));
	        		if(selectByPrimaryKey!=null) {
	        			planWorkOrderDetailPz2.setWorkername(selectByPrimaryKey.getNickname());
	        		}
	        	}
	        	
	        	//获取材料
	        	JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(planWorkOrderDetailPz2.getClID());
	        	if(selectByPrimaryKey!=null) {
	        		planWorkOrderDetailPz2.setClNo(selectByPrimaryKey.getName());
	        		planWorkOrderDetailPz2.setClName(selectByPrimaryKey.getModel());
	        		
	        	}
			}
	        
	        PageInfo<PlanWorkOrderDetailPz> pageInfo = new PageInfo<PlanWorkOrderDetailPz>(list);
	        return  pageInfo;
	 }

	 
	 public PageInfo<PlanWorkOrderDetailPz> getlist(Tablepar tablepar,PlanWorkOrderDetailPz planWorkOrderDetailPz){
	       // PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PlanWorkOrderDetailPz> list= planWorkOrderDetailPzMapper.getListBYParams(planWorkOrderDetailPz);
	        
	        for (PlanWorkOrderDetailPz planWorkOrderDetailPz2 : list) {
	        	Integer worker = planWorkOrderDetailPz2.getWorker();
	        	if(worker!=null) {
	        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(worker));
	        		if(selectByPrimaryKey!=null) {
	        			planWorkOrderDetailPz2.setWorkername(selectByPrimaryKey.getNickname());
	        		}
	        	}
	        	 Integer manager = planWorkOrderDetailPz2.getManager();
	        		if(manager!=null) {
	        			SysDepartment selectByPrimaryKey2 = sysDepartmentMapper.selectByPrimaryKey(Integer.valueOf(manager));
			        	if(selectByPrimaryKey2!=null) {
			        		planWorkOrderDetailPz2.setChejianname(selectByPrimaryKey2.getDeptName());
			        	}
	        		}
				
			}
	        PageInfo<PlanWorkOrderDetailPz> pageInfo = new PageInfo<PlanWorkOrderDetailPz>(list);
	        return  pageInfo;
	 }

	 
	@Override
	public int deleteByPrimaryKey(String ids) {
				
//			Long[] integers = ConvertUtil.toLongArray(",", ids);
//			List<Long> stringB = Arrays.asList(integers);
			
			return planWorkOrderDetailPzMapper.deleteByPrimaryKey(Long.valueOf(ids));
		
				
	}
	
	
	@Override
	public PlanWorkOrderDetailPz selectByPrimaryKey(String id) {
		//获取
		Long id1 = Long.valueOf(id);
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzMapper.selectByPrimaryKey(id1);
		String carftId = selectByPrimaryKey.getCarftId();
		if(carftId!=null) {
			JshMaterialCarfprice selectByPrimaryKey2 = jshMaterialCarftPriceMapper.selectByPrimaryKey(Long.valueOf(carftId));
			selectByPrimaryKey.setCarftname(selectByPrimaryKey2.getTtff());
			selectByPrimaryKey.setCarftspecs(selectByPrimaryKey2.getSpecs());
			selectByPrimaryKey.setCarftttxx(selectByPrimaryKey2.getTtxx());
			
		}
		
			//获取工单数据
		PlanWorkOrder selectOneByworno = planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getWorkerOrderNo());
		selectByPrimaryKey.setMaterialId(selectOneByworno.getMaterialNo());
		
		selectByPrimaryKey.setType(selectOneByworno.getDeleteFlag());
			return selectByPrimaryKey;
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PlanWorkOrderDetailPz record) {
		return planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PlanWorkOrderDetailPz record) {
				
		record.setId(null);
		
				
		return planWorkOrderDetailPzMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PlanWorkOrderDetailPz record, PlanWorkOrderDetailPzExample example) {
		
		return planWorkOrderDetailPzMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PlanWorkOrderDetailPz record, PlanWorkOrderDetailPzExample example) {
		
		return planWorkOrderDetailPzMapper.updateByExample(record, example);
	}

	@Override
	public List<PlanWorkOrderDetailPz> selectByExample(PlanWorkOrderDetailPzExample example) {
		
		return planWorkOrderDetailPzMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PlanWorkOrderDetailPzExample example) {
		
		return planWorkOrderDetailPzMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(PlanWorkOrderDetailPzExample example) {
		
		return planWorkOrderDetailPzMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param planWorkOrderDetailPz
	 * @return
	 */
	public int updateVisible(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		return planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
	}

	public Integer getCountByStatus(Integer status) {
		// TODO Auto-generated method stub
		return planWorkOrderDetailPzMapper.getCountByStatus(status);
	}

	public int savezhijian(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		int result = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
		
		return result;
	}

	public int saveruku(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		planWorkOrderDetailPz.setStatus(4);
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzMapper.selectByPrimaryKey(planWorkOrderDetailPz.getId());
		//判断状态 是否为已质检
		int result=0;
		if(selectByPrimaryKey.getStatus()==3){
		planWorkOrderDetailPz.setFinshTime(new Date());
		result = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
		if(result>0) {
			//根据工单号 获取工单数据
			PlanWorkOrder planWorkOrder =planWorkOrderMapper.selectOneByworno(selectByPrimaryKey.getWorkerOrderNo());
			//插入 入库数据
			WmsWarehouseEntryDetail wmsWarehouseEntryDetail = new WmsWarehouseEntryDetail();
			
			// 关联单号
			wmsWarehouseEntryDetail.setUnitName(selectByPrimaryKey.getId()+"");
			wmsWarehouseEntryDetail.setModifiedUserSid(Long.valueOf(selectByPrimaryKey.getWorker()));
			wmsWarehouseEntryDetail.setCreatedTime(new Date());
			wmsWarehouseEntryDetail.setZhuxuOrg(planWorkOrderDetailPz.getZhuxuOrg());
			wmsWarehouseEntryDetail.setCreatedUserSid(ShiroUtils.getUserId());
			//明细id
			wmsWarehouseEntryDetail.setMatterNumber(Long.valueOf(planWorkOrderDetailPz.getRukuCount()));
			//图号id
			wmsWarehouseEntryDetail.setMatterNo(planWorkOrder.getMaterialNo());
			//数量
			wmsWarehouseEntryDetail.setMatterDesc(selectByPrimaryKey.getWorkerOrderNo());
			//仓库
			wmsWarehouseEntryDetail.setHouseNo(planWorkOrderDetailPz.getField1());
			//类型(工单)
			wmsWarehouseEntryDetail.setQualityNo("2");
			//状态(待入库)
			wmsWarehouseEntryDetail.setX(1);
			//计划
			wmsWarehouseEntryDetail.setDeliverySid(planWorkOrder.getNo());
			wmsWarehouseEntryDetailMapper.insertSelective(wmsWarehouseEntryDetail);
		}
		}
		return result;
	}

	public int savefendan(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		Integer fenchuCount = planWorkOrderDetailPz.getFenchuCount();
		Integer count = planWorkOrderDetailPz.getCount();
		Integer xiancount =count-fenchuCount;
		//更改之前的明细数量
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzMapper.selectByPrimaryKey(planWorkOrderDetailPz.getId());
		selectByPrimaryKey.setCount(xiancount);
		int updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		if(updateByPrimaryKeySelective>0) {
			//添加新明细
			PlanWorkOrderDetailPz newPrimaryKey = new PlanWorkOrderDetailPz();
			newPrimaryKey.setCarftId(selectByPrimaryKey.getCarftId());
			newPrimaryKey.setCount(fenchuCount);
			newPrimaryKey.setWorkerOrderNo(selectByPrimaryKey.getWorkerOrderNo());
			newPrimaryKey.setWorker(Integer.valueOf(planWorkOrderDetailPz.getFenchuWork()));
			newPrimaryKey.setStatus(selectByPrimaryKey.getStatus());
			newPrimaryKey.setManager(selectByPrimaryKey.getManager());
			newPrimaryKey.setCreator(ShiroUtils.getUserId());
			newPrimaryKey.setCreateTime(new Date());
			newPrimaryKey.setDistributionTime(new Date());
			updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.insertSelective(newPrimaryKey);
		}
		
		return updateByPrimaryKeySelective;
	}

	public int savezhuanxu(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		//原明细 状态 变更
		int updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(planWorkOrderDetailPz);
		if(updateByPrimaryKeySelective>0) {
			//添加 新明细 状态 为 待分工
			PlanWorkOrderDetailPz newpz = new PlanWorkOrderDetailPz();
			newpz.setManager(planWorkOrderDetailPz.getXianManger());
			newpz.setWorkerOrderNo(planWorkOrderDetailPz.getWorkerOrderNo());
			newpz.setStatus(6);
			newpz.setCount(planWorkOrderDetailPz.getXianCount());
			newpz.setCreateTime(new Date());
			newpz.setCreator(ShiroUtils.getUserId());
			newpz.setStockCount(planWorkOrderDetailPz.getId().intValue());
			updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.insertSelective(newpz);
		}
		return updateByPrimaryKeySelective;
	}

	public int savepaigong(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzMapper.selectByPrimaryKey(planWorkOrderDetailPz.getId());
		selectByPrimaryKey.setStatus(2);
		selectByPrimaryKey.setDistributionTime(new Date());
		selectByPrimaryKey.setWorker(planWorkOrderDetailPz.getWorker());
		selectByPrimaryKey.setCarftId(planWorkOrderDetailPz.getCarftId());
		int updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
		return updateByPrimaryKeySelective;
	}


	public int jiesuan(String ids) {
		//获取所有派工明细
		Long[] integers = ConvertUtil.toLongArray(",", ids);
		List<Long> stringB = Arrays.asList(integers);
		int insertSelective =0;
		String workorderNo=null;
		for (Long long1 : stringB) {
			//根据id 获取结算明细
			PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzMapper.selectByPrimaryKey(long1);
			workorderNo=selectByPrimaryKey.getWorkerOrderNo();
			PlanWorkOrderPzStatement ppz =planWorkOrderPzStatementMapper.selectByPgid(Integer.valueOf(long1+""));
			if(ppz==null) {
				int jiesuancount =0;
				//根据id  获取入库数字
				WmsWarehouseEntryDetail wmsWarehouseEntryDetail =wmsWarehouseEntryDetailMapper.getBypGid(long1+"","2");
				if(wmsWarehouseEntryDetail==null) {
					jiesuancount=selectByPrimaryKey.getCount();
					
				}else {
					jiesuancount=Integer.valueOf(wmsWarehouseEntryDetail.getMatterDesc()) ;
				}
				ppz= new PlanWorkOrderPzStatement();
				TsysUser user = tsysUserMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getWorker()));
				ppz.setWorker(user.getNickname());
				
				SysDepartment selectByPrimaryKey2 = sysDepartmentMapper.selectByPrimaryKey(selectByPrimaryKey.getManager());
				ppz.setOrgName(selectByPrimaryKey2.getDeptName());
				ppz.setCount(jiesuancount);
					JshMaterialCarfprice jshprice = jshMaterialCarftPriceMapper.selectByPrimaryKey(Long.valueOf(selectByPrimaryKey.getCarftId()));
					ppz.setSpesc(jshprice.getSpecs());
					ppz.setCarftName(jshprice.getTtff());
					BigDecimal purchaseDecimal = jshprice.getPurchaseDecimal();
					BigDecimal countDecimal = new BigDecimal(jiesuancount); 
					BigDecimal multiplyAll = purchaseDecimal.multiply(countDecimal);
					ppz.setPrice(purchaseDecimal.doubleValue());
					ppz.setAlAccount(multiplyAll.doubleValue());
					ppz.setMaterialName(jshprice.getTtxx());
				//工单号
				ppz.setMaterialNo(selectByPrimaryKey.getWorkerOrderNo());
				//派工ID
				ppz.setDistributionTime(selectByPrimaryKey.getFinshTime());
				ppz.setPgId(long1.intValue());
				ppz.setStatus(1);
				ppz.setCreateTime(new Date());
				ppz.setCreator(ShiroUtils.getUserId());
				//添加结算明细
				insertSelective= planWorkOrderPzStatementMapper.insertSelective(ppz);
				
				if(insertSelective>0) {
					//变更派工 状态
					selectByPrimaryKey.setStatus(8);
					insertSelective= planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
					
				}
			}
		}
		
		
		
		if(insertSelective>0) {
			
		
		//变更工单状态
		//检测该工单下 所有派工明细是否都结算了 如果都结算了 那么 状态为已完成
		List<PlanWorkOrderDetailPz> listx =planWorkOrderDetailPzMapper.getListBYworkno(workorderNo);
		int x=1;
		for (PlanWorkOrderDetailPz planWorkOrderDetailPz : listx) {
			if(planWorkOrderDetailPz.getStatus()!=8) {
				x=0;
			}
		}
		if(x==1) {
			
			PlanWorkOrder selectOneByworno = planWorkOrderMapper.selectOneByworno(workorderNo);
			selectOneByworno.setStatus(4);
			insertSelective = planWorkOrderMapper.updateByPrimaryKeySelective(selectOneByworno);
		}
		
		}
		return insertSelective;
	}


	public int chexiao(Long ids) {
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzMapper.selectByPrimaryKey(ids);
		int updateByPrimaryKeySelective =0;
		if(selectByPrimaryKey.getStatus()==3) {
			//质检 状态变为待质检
			selectByPrimaryKey.setUpdateTime(null);
			selectByPrimaryKey.setField2(null);
			selectByPrimaryKey.setStatus(2);
			selectByPrimaryKey.setUnqualifiedCount(0);
			updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
			
		}else if(selectByPrimaryKey.getStatus()==4) {
			//删除待入库数据
			WmsWarehouseEntryDetail bypGid = wmsWarehouseEntryDetailMapper.getBypGid(selectByPrimaryKey.getId()+"","2");
				int deleteByPrimaryKey = wmsWarehouseEntryDetailMapper.deleteByPrimaryKey(bypGid.getSid());
				if(deleteByPrimaryKey>0) {
					
					//待入库状态变为 已质检
					selectByPrimaryKey.setStatus(3);
					selectByPrimaryKey.setFinshTime(null);
					selectByPrimaryKey.setField1(null);
					updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
				}
			
			
			
		}else if(selectByPrimaryKey.getStatus()==7) {
			//下道删除
			List<PlanWorkOrderDetailPz> selectByPrimaryKey2 = planWorkOrderDetailPzMapper.selectBystockcount(selectByPrimaryKey.getId().intValue());
			if(selectByPrimaryKey2.size()>1) {
				return 0;
			}
			int deleteByPrimaryKey =0;
			for (PlanWorkOrderDetailPz planWorkOrderDetailPz : selectByPrimaryKey2) {
				if(planWorkOrderDetailPz.getStatus()==4||planWorkOrderDetailPz.getStatus()==5||planWorkOrderDetailPz.getStatus()==7||planWorkOrderDetailPz.getStatus()==8) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return 0;
				}
				deleteByPrimaryKey = planWorkOrderDetailPzMapper.deleteByPrimaryKey(planWorkOrderDetailPz.getId());
			}
			//已转序 状态变为 已质检 
			if(deleteByPrimaryKey>0) {
				selectByPrimaryKey.setStatus(3);
				selectByPrimaryKey.setFinshTime(null);
				selectByPrimaryKey.setField3(null);
				updateByPrimaryKeySelective = planWorkOrderDetailPzMapper.updateByPrimaryKeySelective(selectByPrimaryKey);
			}
		}
		return updateByPrimaryKeySelective;
	}


	public List<PlanWorkOrderDetailPz> excgetlist(PlanWorkOrderDetailPz planWorkOrderDetailPz) {
		List<PlanWorkOrderDetailPz> list= planWorkOrderDetailPzMapper.getList(planWorkOrderDetailPz);
        for (PlanWorkOrderDetailPz planWorkOrderDetailPz2 : list) {
        	//获取转序车间名称
        	
        		
        		 Integer manager = planWorkOrderDetailPz2.getManager();
        		if(manager!=null) {
        			SysDepartment selectByPrimaryKey2 = sysDepartmentMapper.selectByPrimaryKey(Integer.valueOf(manager));
		        	if(selectByPrimaryKey2!=null) {
		        		planWorkOrderDetailPz2.setChejianname(selectByPrimaryKey2.getDeptName());
		        	}
        		}
        		
        	
        	Integer worker = planWorkOrderDetailPz2.getWorker();
        	if(worker!=null) {
        		TsysUser selectByPrimaryKey = tsysUserMapper.selectByPrimaryKey(Long.valueOf(worker));
        		if(selectByPrimaryKey!=null) {
        			planWorkOrderDetailPz2.setWorkername(selectByPrimaryKey.getNickname());
        		}
        	}
        	
        	
		}
        
        return list;
	}


}
