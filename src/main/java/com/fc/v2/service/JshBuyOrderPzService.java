package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshBuyOrderPzMapper;
import com.fc.v2.model.auto.JshBuyOrderPz;
import com.fc.v2.model.auto.JshBuyOrderPzExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 工单明细扩展表(派工,质检明细) JshBuyOrderPzService
 * @Title: JshBuyOrderPzService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-26 23:13:28  
 **/
@Service
public class JshBuyOrderPzService implements BaseService<JshBuyOrderPz, JshBuyOrderPzExample>{
	@Autowired
	private JshBuyOrderPzMapper jshBuyOrderPzMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshBuyOrderPz> list(Tablepar tablepar,JshBuyOrderPz jshBuyOrderPz){
	        JshBuyOrderPzExample testExample=new JshBuyOrderPzExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(jshBuyOrderPz);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshBuyOrderPz> list= jshBuyOrderPzMapper.selectByExample(testExample);
	        PageInfo<JshBuyOrderPz> pageInfo = new PageInfo<JshBuyOrderPz>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshBuyOrderPzExample example=new JshBuyOrderPzExample();
			example.createCriteria().andIdIn(stringB);
			return jshBuyOrderPzMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshBuyOrderPz selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshBuyOrderPzMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshBuyOrderPz record) {
		return jshBuyOrderPzMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshBuyOrderPz record) {
				
		record.setId(null);
		
				
		return jshBuyOrderPzMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(JshBuyOrderPz record, JshBuyOrderPzExample example) {
		
		return jshBuyOrderPzMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshBuyOrderPz record, JshBuyOrderPzExample example) {
		
		return jshBuyOrderPzMapper.updateByExample(record, example);
	}

	@Override
	public List<JshBuyOrderPz> selectByExample(JshBuyOrderPzExample example) {
		
		return jshBuyOrderPzMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshBuyOrderPzExample example) {
		
		return jshBuyOrderPzMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshBuyOrderPzExample example) {
		
		return jshBuyOrderPzMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshBuyOrderPz
	 * @return
	 */
	public int updateVisible(JshBuyOrderPz jshBuyOrderPz) {
		return jshBuyOrderPzMapper.updateByPrimaryKeySelective(jshBuyOrderPz);
	}


}
