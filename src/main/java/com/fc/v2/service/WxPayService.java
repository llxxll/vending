package com.fc.v2.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alipay.api.internal.util.file.IOUtils;
import com.fc.v2.controller.yiyuan.YiyuanController;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.Result;
import com.fc.v2.model.auto.Order;
import com.fc.v2.model.enums.status.OrderStatus;
import com.fc.v2.model.exception.ExceptionMessage;
import com.fc.v2.model.exception.ValidateException;
import com.fc.v2.model.property.AppProperties;
import com.fc.v2.util.HttpUtil;
import com.fc.v2.util.PaymentUtils;
import com.fc.v2.util.RandomUtil;
import com.fc.v2.util.ResultUtils;
import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.core.exception.HttpException;
import com.wechat.pay.java.core.exception.ValidationException;
import com.wechat.pay.java.core.http.*;
import com.wechat.pay.java.core.notification.NotificationConfig;
import com.wechat.pay.java.core.notification.NotificationParser;
import com.wechat.pay.java.core.notification.RequestParam;
import com.wechat.pay.java.service.payments.model.Transaction;
import com.wechat.pay.java.service.payments.nativepay.NativePayService;
import com.wechat.pay.java.service.payments.nativepay.model.*;
import com.wechat.pay.java.service.refund.RefundService;
import com.wechat.pay.java.service.refund.model.AmountReq;
import com.wechat.pay.java.service.refund.model.CreateRequest;
import com.wechat.pay.java.service.refund.model.QueryByOutRefundNoRequest;
import com.wechat.pay.java.service.refund.model.Refund;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.wechat.pay.java.core.util.GsonUtil.toJson;
import static java.util.Objects.requireNonNull;

/**
 * @author leone
 * @since 2018-05-22
 **/
@Slf4j
@Service
public class WxPayService {

    @Resource
    private OrderService orderService;

    @Resource
    private OrderBillService orderBillService;

    @Resource
    private YiyuanController yiyuanController;

    @Resource
    private AppProperties appProperties;



    /**
     * 微信App支付，请求微信下单接口生成预付款信息返回 prepay_id 给客户端
     *
     * @param request
     * @param orderId
     */
    public Map<String, Object> appPay(HttpServletRequest request, Long orderId) {
        // 校验订单信息
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.CREATE.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }

        // 设置客户端的ip地址
        String spbill_create_ip = PaymentUtils.getIpAddress(request);
        if (!PaymentUtils.isIp(spbill_create_ip)) {
            spbill_create_ip = "127.0.0.1";
        }

        String nonce_str = 1 + RandomUtil.randomStr(15);

        // 微信app支付十个必须要传入的参数
        Map<String, Object> params = new HashMap<>();

        // 应用ID
        params.put("appid", appProperties.getWx().getApp_id());
        // 商户号
        params.put("mch_id", appProperties.getWx().getMch_id());
        // 随机字符串
        params.put("nonce_str", nonce_str);
        // 商品描述
        params.put("body", "App weChat pay!");
        // 商户订单号
        params.put("out_trade_no", order.getOutTradeNo());
        // 总金额(分)
        params.put("total_fee", order.getTotalFee().toString());
        // 终端IP
        params.put("spbill_create_ip", spbill_create_ip);
        // 通知地址
        params.put("notify_url", appProperties.getWx().getNotify_url());
        // 交易类型:JS_API=公众号支付、NATIVE=扫码支付、APP=app支付
        params.put("trade_type", "APP");
        // 签名
        String sign = PaymentUtils.sign(params, appProperties.getWx().getApi_key());
        params.put("sign", "sign");

        String xmlData = PaymentUtils.mapToXml(params);

        // 向微信发起预支付
        String wxRetXmlData = HttpUtil.sendPostXml(appProperties.getWx().getCreate_order_url(), xmlData, null);

        Map wxRetMapData = PaymentUtils.xmlToMap(wxRetXmlData);
        Assert.notNull(wxRetMapData, ExceptionMessage.XML_DATA_INCORRECTNESS.getMessage());
        log.info("weChat pre pay result data: {}", wxRetMapData);

        // 封装参数返回App端
        Map<String, Object> result = new HashMap<>();
        result.put("appid", appProperties.getWx().getApp_id());
        result.put("partnerid", appProperties.getWx().getMch_id());
        result.put("prepayid", wxRetMapData.get("prepay_id").toString());
        result.put("noncestr", nonce_str);
        result.put("timestamp", RandomUtil.getDateStr(13));
        result.put("package", "Sign=WXPay");
        // 对返回给App端的数据进行签名
        result.put("sign", PaymentUtils.sign(result, appProperties.getWx().getApi_key()));
        return result;
    }

    public ApiResult notify2(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("微信支付回调开始");
        // 从请求头中获取信息
        String timestamp                        = request.getHeader("Wechatpay-Timestamp");
        String nonce                            = request.getHeader("Wechatpay-Nonce");
        String signature                        = request.getHeader("Wechatpay-Signature");
        String singType                         = request.getHeader("Wechatpay-Signature-Type");
        String wechatPayCertificateSerialNumber = request.getHeader("Wechatpay-Serial");
        InputStream is = request.getInputStream();
        String string = IOUtils.toString(is, StandardCharsets.UTF_8);
            RequestParam requestParam = new RequestParam.Builder()
                    .serialNumber(wechatPayCertificateSerialNumber)
                    .nonce(nonce)
                    .signature(signature)
                    .timestamp(timestamp)
                    .body(string)
                    .build();
            NotificationConfig config = new RSAAutoCertificateConfig.Builder()
                    .merchantId(appProperties.getWx().getMch_id())
                    .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                    .merchantSerialNumber(appProperties.getWx().getApp_secret())
                    .apiV3Key(appProperties.getWx().getApi_key())
                    .build();
            NotificationParser parser = new NotificationParser(config);
            Map transaction = parser.parse(requestParam, Map.class);
        System.out.println("微信支付返回数据解析："+JSONObject.toJSONString(transaction));
        if("SUCCESS".equals(transaction.get("trade_state"))){
            System.out.println("微信支付回调成功");
            Order order = orderService.findByOutTradeNo((String)transaction.get("out_trade_no"));
            if(order != null){
                if (order.getStatus() == OrderStatus.PAY.getStatus()) {
                    System.out.println("微信支付回调返回");
                    return ResultUtils.ok(transaction);
                }
                order.setStatus((byte) 1);
                order.setFinishTime(new Date());
                order.setTransactionId((String)transaction.get("transaction_id"));
                orderService.updateByPrimaryKeySelective(order);
                SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
                String fabuData = sdf.format(order.getFinishTime());
                System.out.println("微信支付去通知his");
                yiyuanController.appointmentPay(order.getOutTradeNo(), null, String.valueOf(order.getUserId()),  fabuData, "2", "", order.getConsignee(), "",order.getCreateIp(),order.getOrderId(),String.valueOf(order.getTotalFee()));
                return ResultUtils.ok(transaction);
            }else{
                System.out.println("查询订单失败");
                return ResultUtils.ok(transaction);
            }
        }else{
            System.out.println("支付失败返回");
            //return Result.build("支付失败",201);
            return ResultUtils.ok(transaction);
        }

    }



    private Charset inputCharset = Consts.UTF_8;
    public Map<String, Object> getNoticeParams(HttpServletRequest request) {
        final Map<String, String[]> parameterMap = request.getParameterMap();

        Map<String, Object> params = new TreeMap<>();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            StringBuilder sb = new StringBuilder();
            for (int i = 0, len = values.length; i < len; i++) {
                sb.append(values[i]).append((i == len - 1) ? "" : ',');
            }
            String valueStr = sb.toString();
            if (!valueStr.matches("\\w+")) {
                if (valueStr.equals(new String(valueStr.getBytes(Consts.ISO_8859_1), Consts.ISO_8859_1))) {
                    valueStr = new String(valueStr.getBytes(Consts.ISO_8859_1), inputCharset);
                }
            }
            params.put(name, valueStr);
        }
        return params;
    }
    /**
     * 微信扫码支付
     *
     * @param orderId
     * @return
     * @throws Exception
     */
    public ApiResult qrCodePay(Long orderId) {
        System.out.println("微信扫码支付开始，orderid："+orderId);
        // 校验订单信息
        Order order = orderService.findOne(orderId);
        if (order!= null && order.getStatus() != OrderStatus.CREATE.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        System.out.println("微信扫码支付开始，order信息："+JSONObject.toJSONString(order));
// 使用自动更新平台证书的RSA配置
        // 一个商户号只能初始化一个配置，否则会因为重复的下载任务报错
        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(appProperties.getWx().getMch_id())
                        //http://112.2.13.44:8085/upload/eeb1353e-49e5-4dfe-a1bb-cc7a307ab205.png
                        //.privateKeyFromPath("src/main/resources/1650971467_20230905_cert/apiclient_key.pem")
                        .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                        .merchantSerialNumber(appProperties.getWx().getApp_secret())
                        .apiV3Key(appProperties.getWx().getApi_key())
                        .build();
        // 构建service
        NativePayService service = new NativePayService.Builder().config(config).build();
        // request.setXxx(val)设置所需参数，具体参数可见Request定义
        PrepayRequest request = new PrepayRequest();
        Amount amount = new Amount();
        //amount.setTotal(order.getTotalFee()*100);
        amount.setTotal(order.getTotalFee()*100);
        request.setAmount(amount);
        request.setAppid(appProperties.getWx().getApp_id());
        request.setMchid(appProperties.getWx().getMch_id());
        request.setDescription("挂号费");
        request.setNotifyUrl(appProperties.getWx().getNotify_url());
        request.setOutTradeNo(order.getOutTradeNo());
        // 调用下单方法，得到应答
        System.out.println("微信支付调用:"+JSONObject.toJSONString(request));
        PrepayResponse response = service.prepay(request);
        // 使用微信扫描 code_url 对应的二维码，即可体验Native支付
        System.out.println("微信支付回调:"+response);
        System.out.println(response.getCodeUrl());
        return ResultUtils.ok(response);
    }


    public ApiResult wxRefund2(Long orderId) throws Exception {
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.PAY.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        System.out.println("微信退款:"+orderId);

        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(appProperties.getWx().getMch_id())
                        .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                        .merchantSerialNumber(appProperties.getWx().getApp_secret())
                        .apiV3Key(appProperties.getWx().getApi_key())
                        .build();
        RefundService refundService = new RefundService.Builder().config(config).build();
        CreateRequest request = new CreateRequest();
        AmountReq amount = new AmountReq();
        amount.setRefund((long) (order.getTotalFee()*100));
        amount.setTotal((long) (order.getTotalFee()*100));
        amount.setCurrency("CNY");
        request.setOutTradeNo(order.getOutTradeNo());
        request.setAmount(amount);
        request.setOutRefundNo(RandomUtil.randomNum(32));
        Refund refund = refundService.create(request);
        System.out.println("微信退款回调:"+JSONObject.toJSONString(refund));
        if("SUCCESS".equals(refund.getStatus().name()) || "PROCESSING".equals(refund.getStatus().name())){
            System.out.println("微信退款回调调用成功");
            order.setDeleteFlag(1);
            orderService.updateByPrimaryKeySelective(order);
            String orders = String.valueOf(order.getOrderId());
            String substring = orders.substring(0, 4);
            yiyuanController.appointmentPayCancel(substring,   String.valueOf(order.getUserId()), order.getRemark());
        } else {
            System.out.println("微信退款回调调用失败");
        }
        return ResultUtils.ok(refund);
    }
    public ApiResult wxRefundByoutOrderNumber(String outOrderNumber) throws Exception {
        Order order = orderService.findByOutTradeNo(outOrderNumber);
        if (order.getStatus() != OrderStatus.PAY.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " outOrderNumber: {}", outOrderNumber);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        System.out.println("微信退款:"+outOrderNumber);

        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(appProperties.getWx().getMch_id())
                        .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                        .merchantSerialNumber(appProperties.getWx().getApp_secret())
                        .apiV3Key(appProperties.getWx().getApi_key())
                        .build();
        RefundService refundService = new RefundService.Builder().config(config).build();
        CreateRequest request = new CreateRequest();
        AmountReq amount = new AmountReq();
        amount.setRefund((long) (order.getTotalFee()*100));
        amount.setTotal((long) (order.getTotalFee()*100));
        amount.setCurrency("CNY");
        request.setOutTradeNo(order.getOutTradeNo());
        request.setAmount(amount);
        request.setOutRefundNo(RandomUtil.randomNum(32));
        Refund refund = refundService.create(request);
        System.out.println("微信退款回调:"+JSONObject.toJSONString(refund));
        if("SUCCESS".equals(refund.getStatus().name()) || "PROCESSING".equals(refund.getStatus().name())){
            System.out.println("微信退款回调调用成功");
            order.setDeleteFlag(1);
            orderService.updateByPrimaryKeySelective(order);
            String orders = String.valueOf(order.getOrderId());
            String substring = orders.substring(0, 4);
            yiyuanController.appointmentPayCancel(substring,   String.valueOf(order.getUserId()), order.getRemark());
            return ResultUtils.ok("调用成功");
        } else {
            System.out.println("微信退款回调调用失败");
            return ResultUtils.ok("调用失败");
        }
    }
    /**
     * 微信退款
     *
     * @param orderId
     * @return
     * @throws Exception
     */
    public ApiResult wxRefund(Long orderId) throws Exception {
        Order order = orderService.findOne(orderId);
        if (order.getStatus() != OrderStatus.PAY.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        System.out.println("微信退款:"+orderId);

        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(appProperties.getWx().getMch_id())
                        .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                        .merchantSerialNumber(appProperties.getWx().getApp_secret())
                        .apiV3Key(appProperties.getWx().getApi_key())
                        .build();
        RefundService refundService = new RefundService.Builder().config(config).build();
        QueryByOutRefundNoRequest request = new QueryByOutRefundNoRequest();
        AmountReq amount = new AmountReq();
        amount.setTotal((long) 10);
        //amount.setTotal(order.getTotalFee()*100);
        request.setOutRefundNo(order.getOutTradeNo());
        Refund refund = refundService.queryByOutRefundNo(request);
        System.out.println("微信退款回调:"+JSONObject.toJSONString(refund));
        if("SUCCESS".equals(refund.getStatus()) || "PROCESSING".equals(refund.getStatus())){
            System.out.println("微信退款回调调用成功");
            order.setDeleteFlag(1);
            orderService.updateByPrimaryKeySelective(order);
            yiyuanController.appointmentPayCancel(String.valueOf(order.getOrderId()),   String.valueOf(order.getUserId()), order.getCreateIp());
        } else {
            System.out.println("微信退款回调调用失败");
        }
        return ResultUtils.ok(refund);
    }
    private RequestBody createRequestBody(Object request) {
        return new JsonRequestBody.Builder().body(toJson(request)).build();
    }
    /**
     * 微信小程序支付
     *
     * @param orderId
     * @param request
     */
    public Result xcxPay(Long orderId, HttpServletRequest request) {
        Order order = orderService.findOne(orderId);
       // User user = userService.findOne(order.getUserId());
        String nonce_str = RandomUtil.randomNum(12);
        String outTradeNo = 1 + RandomUtil.randomNum(11);
        String spbill_create_ip = PaymentUtils.getIpAddress(request);
        if (!PaymentUtils.isIp(spbill_create_ip)) {
            spbill_create_ip = "127.0.0.1";
        }
        // 小程序支付需要参数
        SortedMap<String, Object> reqMap = new TreeMap<>();
        reqMap.put("appid", appProperties.getWx().getApp_id());
        reqMap.put("mch_id", appProperties.getWx().getMch_id());
        reqMap.put("nonce_str", nonce_str);
        reqMap.put("body", "Mimi Programing Pay");
        reqMap.put("out_trade_no", outTradeNo);
        reqMap.put("total_fee", order.getTotalFee().toString());
        reqMap.put("spbill_create_ip", spbill_create_ip);
        reqMap.put("notify_url", appProperties.getWx().getNotify_url());
        reqMap.put("trade_type", appProperties.getWx().getTrade_type());
        reqMap.put("openid", "Openid()");
        String sign = PaymentUtils.sign(reqMap, appProperties.getWx().getApi_key());
        reqMap.put("sign", sign);
        String xml = PaymentUtils.mapToXml(reqMap);
        String result = HttpUtil.sendPostXml(appProperties.getWx().getCreate_order_url(), xml, null);
        Map<String, String> resData = PaymentUtils.xmlToMap(result);
        log.info("resData:{}", resData);
        if ("SUCCESS".equals(resData.get("return_code"))) {
            Map<String, Object> resultMap = new LinkedHashMap<>();
            //返回的预付单信息
            String prepay_id = resData.get("prepay_id");
            resultMap.put("appId", appProperties.getWx().getApp_id());
            resultMap.put("nonceStr", nonce_str);
            resultMap.put("package", "prepay_id=" + prepay_id);
            resultMap.put("signType", "MD5");
            resultMap.put("timeStamp", RandomUtil.getDateStr(14));
            String paySign = PaymentUtils.sign(resultMap, appProperties.getWx().getApi_key());
            resultMap.put("paySign", paySign);
            log.info("return data:{}", resultMap);
            return Result.success(resultMap);
        }
        return Result.error(ExceptionMessage.WEI_XIN_PAY_FAIL);
    }

    /**
     * 微信支付订单号查询订单
     *
     * @param request 请求参数
     * @return Transaction
     * @throws HttpException 发送HTTP请求失败。例如构建请求参数失败、发送请求失败、I/O错误等。包含请求信息。
     * @throws ValidationException 发送HTTP请求成功，验证微信支付返回签名失败。
     * @throws ServiceException 发送HTTP请求成功，服务返回异常。例如返回状态码小于200或大于等于300。
     * @throws MalformedMessageException 服务返回成功，content-type不为application/json、解析返回体失败。
     */
    public ApiResult queryOrderById(Long orderId) {
        // 校验订单信息
        Order order = orderService.findOne(orderId);
        if (order!= null && order.getStatus() != OrderStatus.CREATE.getStatus()) {
            log.error(ExceptionMessage.ORDER_STATUS_INCORRECTNESS + " orderId: {}", orderId);
            throw new ValidateException(ExceptionMessage.ORDER_STATUS_INCORRECTNESS);
        }
        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(appProperties.getWx().getMch_id())
                        .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                        .merchantSerialNumber(appProperties.getWx().getApp_secret())
                        .apiV3Key(appProperties.getWx().getApi_key())
                        .build();
        NativePayService service = new NativePayService.Builder().config(config).build();
        QueryOrderByIdRequest request = new QueryOrderByIdRequest();
        request.setMchid(appProperties.getWx().getMch_id());
        request.setTransactionId(order.getOutTradeNo());
        // 调用下单方法，得到应答
        Transaction response = service.queryOrderById(request);
        // 使用微信扫描 code_url 对应的二维码，即可体验Native支付
        System.out.println("微信支付回调:"+JSONObject.toJSONString(response));
        return ResultUtils.ok(response);
    }

    public ApiResult queryOut(Long orderId) {
        // 校验订单信息
        Order order = orderService.findOne(orderId);

        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(appProperties.getWx().getMch_id())
                        .privateKeyFromPath("C:\\Users\\Administrator\\Desktop\\java\\upload\\apiclient_key.pem")
                        .merchantSerialNumber(appProperties.getWx().getApp_secret())
                        .apiV3Key(appProperties.getWx().getApi_key())
                        .build();
        NativePayService service = new NativePayService.Builder().config(config).build();
        QueryOrderByOutTradeNoRequest request = new QueryOrderByOutTradeNoRequest();
        request.setMchid(appProperties.getWx().getMch_id());
        request.setOutTradeNo(order.getOutTradeNo());
        // 调用下单方法，得到应答
        Transaction response = service.queryOrderByOutTradeNo(request);
        // 使用微信扫描 code_url 对应的二维码，即可体验Native支付
        System.out.println("微信支付回调:"+JSONObject.toJSONString(response));
        return ResultUtils.ok(response);
    }

}
