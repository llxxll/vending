package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshSupplierMapper;
import com.fc.v2.model.auto.JshSupplier;
import com.fc.v2.model.auto.JshSupplierExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 供应商/客户信息表 JshSupplierService
 * @Title: JshSupplierService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 23:17:26  
 **/
@Service
public class JshSupplierService implements BaseService<JshSupplier, JshSupplierExample>{
	@Autowired
	private JshSupplierMapper jshSupplierMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshSupplier> list(Tablepar tablepar,JshSupplier jshSupplier){
	        JshSupplierExample testExample=new JshSupplierExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(jshSupplier);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshSupplier> list= jshSupplierMapper.selectByExample(testExample);
	        PageInfo<JshSupplier> pageInfo = new PageInfo<JshSupplier>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshSupplierExample example=new JshSupplierExample();
			example.createCriteria().andIdIn(stringB);
			return jshSupplierMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshSupplier selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshSupplierMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshSupplier record) {
		return jshSupplierMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshSupplier record) {
				
		record.setId(null);
		
				
		return jshSupplierMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(JshSupplier record, JshSupplierExample example) {
		
		return jshSupplierMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshSupplier record, JshSupplierExample example) {
		
		return jshSupplierMapper.updateByExample(record, example);
	}

	@Override
	public List<JshSupplier> selectByExample(JshSupplierExample example) {
		
		return jshSupplierMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshSupplierExample example) {
		
		return jshSupplierMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshSupplierExample example) {
		
		return jshSupplierMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshSupplier
	 * @return
	 */
	public int updateVisible(JshSupplier jshSupplier) {
		return jshSupplierMapper.updateByPrimaryKeySelective(jshSupplier);
	}


	public List<JshSupplier> selectSupplierList() {
		return jshSupplierMapper.findAll();
	}
}
