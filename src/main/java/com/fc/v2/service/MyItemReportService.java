package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.MyItemReportMapper;
import com.fc.v2.mapper.auto.MyItemTypeMapper;
import com.fc.v2.model.auto.MyItemDetail;
import com.fc.v2.model.auto.MyItemReport;
import com.fc.v2.model.auto.MyItemReportExample;
import com.fc.v2.model.auto.MyItemType;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 *  MyItemReportService
 * @Title: MyItemReportService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-08-28 21:49:32  
 **/
@Service
public class MyItemReportService implements BaseService<MyItemReport, MyItemReportExample>{
	@Autowired
	private MyItemReportMapper myItemReportMapper;
	
	@Autowired
	private MyItemTypeMapper myItemTypeMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MyItemReport> list(Tablepar tablepar,MyItemReport myItemReport){
			
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MyItemReport> list= myItemReportMapper.getList(myItemReport);
	        for (MyItemReport myItemDetail2 : list) {
		    	   MyItemType selectByPrimaryKey = myItemTypeMapper.selectByPrimaryKey(Long.valueOf(myItemDetail2.getItemtype()));
		    	   if(selectByPrimaryKey!=null) {
		    		   myItemDetail2.setItemtypeName(selectByPrimaryKey.getTypeName());
		    	   }
	        }
	        PageInfo<MyItemReport> pageInfo = new PageInfo<MyItemReport>(list);
	        return  pageInfo;
	 }
	 public PageInfo<MyItemReport> list1(Tablepar tablepar,MyItemReport myItemReport){
		 
		// PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		 List<MyItemReport> list= myItemReportMapper.getList1(myItemReport);
		 PageInfo<MyItemReport> pageInfo = new PageInfo<MyItemReport>(list);
		 return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			MyItemReportExample example=new MyItemReportExample();
			example.createCriteria().andIdIn(stringB);
			return myItemReportMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public MyItemReport selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return myItemReportMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MyItemReport record) {
		return myItemReportMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MyItemReport record) {
				
		record.setId(null);
		
				
		return myItemReportMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MyItemReport record, MyItemReportExample example) {
		
		return myItemReportMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MyItemReport record, MyItemReportExample example) {
		
		return myItemReportMapper.updateByExample(record, example);
	}

	@Override
	public List<MyItemReport> selectByExample(MyItemReportExample example) {
		
		return myItemReportMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MyItemReportExample example) {
		
		return myItemReportMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MyItemReportExample example) {
		
		return myItemReportMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param myItemReport
	 * @return
	 */
	public int updateVisible(MyItemReport myItemReport) {
		return myItemReportMapper.updateByPrimaryKeySelective(myItemReport);
	}


}
