package com.fc.v2.service;

import java.util.*;

import com.fc.v2.mapper.auto.YyDetailMapper;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.YyDetail;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyDangjianMapper;
import com.fc.v2.model.auto.YyDangjian;
import com.fc.v2.model.auto.YyDangjianExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 *  YyDangjianService
 * @Title: YyDangjianService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-03 21:19:10  
 **/
@Service
public class YyDangjianService implements BaseService<YyDangjian, YyDangjianExample>{
	@Autowired
	private YyDangjianMapper yyDangjianMapper;
	@Autowired
	private YyDetailService yyDetailService;
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyDangjian> list(Tablepar tablepar,YyDangjian yyDangjian){
	        YyDangjianExample testExample=new YyDangjianExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyDangjian);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyDangjian> list= yyDangjianMapper.selectByExample(testExample);
	        if(list.size()>0){
				for(YyDangjian yy: list){
					List<YyDetail> yyDetails = yyDetailService.selectByYyIdAndYyType(yy.getId(),"dj");
					yy.setYyDetailList(yyDetails);
				}
			}
	        PageInfo<YyDangjian> pageInfo = new PageInfo<YyDangjian>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyDangjianExample example=new YyDangjianExample();
			example.createCriteria().andIdIn(stringB);
			return yyDangjianMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyDangjian selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyDangjianMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyDangjian record) {
		record.setUpdateTime(new Date());
		if(StringUtils.isNotEmpty(record.getField4()) && StringUtils.isNotEmpty(record.getField5())){
			yyDetailService.deleteByYyIdAndYyType(record.getId(),"dj");
			String field4 = record.getField4();
			String field5 = record.getField5();
			String[] split4new = field4.split(",");
			String[] split5new = field5.split(",");
			for(int i=0;i<split4new.length;i++){
				TsysUser user = ShiroUtils.getUser();
				YyDetail yyDetail = new YyDetail();
				yyDetail.setCreateTime(new Date());
				yyDetail.setDeleteFlag(0);
				yyDetail.setCreator(user.getId());
				yyDetail.setField3(user.getNickname());
				yyDetail.setUpdateTime(new Date());
				yyDetail.setUpdater(user.getId());
				yyDetail.setName(split4new[i]);
				yyDetail.setUrl(split5new[i]);
				yyDetail.setYyId(record.getId());
				yyDetail.setYyType("dj");
				yyDetailService.insertSelective(yyDetail);
			}
		}
		return yyDangjianMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyDangjian record) {
		record.setId(null);
		int b = yyDangjianMapper.insertSelective(record);
		if(StringUtils.isNotEmpty(record.getField4()) && StringUtils.isNotEmpty(record.getField5())){
			String field4 = record.getField4();
			String field5 = record.getField5();
			String[] split4new = field4.split(",");
			String[] split5new = field5.split(",");
			for(int i=0;i<split4new.length;i++){
				TsysUser user = ShiroUtils.getUser();
				YyDetail yyDetail = new YyDetail();
				yyDetail.setCreateTime(new Date());
				yyDetail.setDeleteFlag(0);
				yyDetail.setCreator(user.getId());
				yyDetail.setField3(user.getNickname());
				yyDetail.setUpdateTime(new Date());
				yyDetail.setUpdater(user.getId());
				yyDetail.setName(split4new[i]);
				yyDetail.setUrl(split5new[i]);
				yyDetail.setYyId(record.getId());
				yyDetail.setYyType("dj");
				yyDetailService.insertSelective(yyDetail);
			}
		}
		return b;
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyDangjian record, YyDangjianExample example) {
		
		return yyDangjianMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyDangjian record, YyDangjianExample example) {
		
		return yyDangjianMapper.updateByExample(record, example);
	}

	@Override
	public List<YyDangjian> selectByExample(YyDangjianExample example) {
		
		return yyDangjianMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyDangjianExample example) {
		
		return yyDangjianMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyDangjianExample example) {
		
		return yyDangjianMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyDangjian
	 * @return
	 */
	@Transactional
	public int updateVisible(YyDangjian yyDangjian) {
		return yyDangjianMapper.updateByPrimaryKeySelective(yyDangjian);
	}

	@Transactional
    public int updateStatusByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyDangjianMapper.updateStatusByPrimaryKey(aLong);
    }
	@Transactional
	public int shangjiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyDangjianMapper.shangjiaByPrimaryKey(aLong);
	}
	@Transactional
	public int xiajiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyDangjianMapper.xiajiaByPrimaryKey(aLong);
	}

    public int updateByPrimaryKey(YyDangjian yyDangjian) {
		return yyDangjianMapper.updateByPrimaryKey(yyDangjian);
    }
}
