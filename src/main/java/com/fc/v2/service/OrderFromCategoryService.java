package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Map;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.OrderFromCategoryMapper;
import com.fc.v2.model.auto.OrderFromCategory;
import com.fc.v2.model.auto.OrderFromCategoryExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 主数据-商品信息表 OrderFromCategoryService
 * @Title: OrderFromCategoryService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2022-06-23 22:58:10  
 **/
@Service
public class OrderFromCategoryService implements BaseService<OrderFromCategory, OrderFromCategoryExample>{
	@Autowired
	private OrderFromCategoryMapper orderFromCategoryMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<OrderFromCategory> list(Tablepar tablepar,OrderFromCategory orderFromCategory){


	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<OrderFromCategory> list= orderFromCategoryMapper.getList(orderFromCategory);
	        PageInfo<OrderFromCategory> pageInfo = new PageInfo<OrderFromCategory>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			OrderFromCategoryExample example=new OrderFromCategoryExample();
			example.createCriteria().andSidIn(stringB);
			return orderFromCategoryMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public OrderFromCategory selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return orderFromCategoryMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(OrderFromCategory record) {
		OrderFromCategory selectByName = orderFromCategoryMapper.selectByName(record.getProductTitle());
		if(selectByName!=null) {
			return 3;
		}
		return orderFromCategoryMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(OrderFromCategory record) {
				
		record.setSid(null);
		record.setCreatedTime(new Date());
		record.setCreatedUserSid(ShiroUtils.getUserId());
		OrderFromCategory record1 =orderFromCategoryMapper.selectByName(record.getProductTitle());	
		if(record1==null) {
			
			return orderFromCategoryMapper.insertSelective(record);
		}else {
			return 3;
		}
	}
	
	
	@Override
	public int updateByExampleSelective(OrderFromCategory record, OrderFromCategoryExample example) {
		
		return orderFromCategoryMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(OrderFromCategory record, OrderFromCategoryExample example) {
		
		return orderFromCategoryMapper.updateByExample(record, example);
	}

	@Override
	public List<OrderFromCategory> selectByExample(OrderFromCategoryExample example) {
		
		return orderFromCategoryMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(OrderFromCategoryExample example) {
		
		return orderFromCategoryMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(OrderFromCategoryExample example) {
		
		return orderFromCategoryMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param orderFromCategory
	 * @return
	 */
	public int updateVisible(OrderFromCategory orderFromCategory) {
		return orderFromCategoryMapper.updateByPrimaryKeySelective(orderFromCategory);
	}


    public List<Map<String, Object>> selectorderFromCategorylist() {
		return orderFromCategoryMapper.selectorderFromCategorylist();
    }

	public List<OrderFromCategory> getorderFromCategoryList() {
		return orderFromCategoryMapper.findAll();
	}
}
