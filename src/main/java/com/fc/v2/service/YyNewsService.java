package com.fc.v2.service;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.fc.v2.model.auto.*;
import com.fc.v2.shiro.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.YyNewsMapper;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 医院新闻 YyNewsService
 * @Title: YyNewsService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-02-04 14:29:44  
 **/
@Service
public class YyNewsService implements BaseService<YyNews, YyNewsExample>{
	@Autowired
	private YyNewsMapper yyNewsMapper;
	@Autowired
	private YyDetailService yyDetailService;
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<YyNews> list(Tablepar tablepar,YyNews yyNews){
	        YyNewsExample testExample=new YyNewsExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(yyNews);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<YyNews> list= yyNewsMapper.selectByExample(testExample);
		 if(list.size()>0){
			 for(YyNews yy: list){
				 List<YyDetail> yyDetails = yyDetailService.selectByYyIdAndYyType(yy.getId(),"new");
				 yy.setYyDetailList(yyDetails);
			 }
		 }
	        PageInfo<YyNews> pageInfo = new PageInfo<YyNews>(list);
	        return  pageInfo;
	 }

	@Override
	@Transactional
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			YyNewsExample example=new YyNewsExample();
			example.createCriteria().andIdIn(stringB);
			return yyNewsMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public YyNews selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return yyNewsMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	@Transactional
	public int updateByPrimaryKeySelective(YyNews record) {
		record.setUpdateTime(new Date());
		if(StringUtils.isNotEmpty(record.getField4()) && StringUtils.isNotEmpty(record.getField5())){
			yyDetailService.deleteByYyIdAndYyType(record.getId(),"new");
			String field4 = record.getField4();
			String field5 = record.getField5();
			String[] split4new = field4.split(",");
			String[] split5new = field5.split(",");
			for(int i=0;i<split4new.length;i++){
				TsysUser user = ShiroUtils.getUser();
				YyDetail yyDetail = new YyDetail();
				yyDetail.setCreateTime(new Date());
				yyDetail.setDeleteFlag(0);
				yyDetail.setCreator(user.getId());
				yyDetail.setField3(user.getNickname());
				yyDetail.setUpdateTime(new Date());
				yyDetail.setUpdater(user.getId());
				yyDetail.setName(split4new[i]);
				yyDetail.setUrl(split5new[i]);
				yyDetail.setYyId(record.getId());
				yyDetail.setYyType("new");
				yyDetailService.insertSelective(yyDetail);
			}
		}
		return yyNewsMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	@Transactional
	public int insertSelective(YyNews record) {
				
		record.setId(null);
		int b = yyNewsMapper.insertSelective(record);
		if(StringUtils.isNotEmpty(record.getField4()) && StringUtils.isNotEmpty(record.getField5())){
			String field4 = record.getField4();
			String field5 = record.getField5();
			String[] split4new = field4.split(",");
			String[] split5new = field5.split(",");
			for(int i=0;i<split4new.length;i++){
				TsysUser user = ShiroUtils.getUser();
				YyDetail yyDetail = new YyDetail();
				yyDetail.setCreateTime(new Date());
				yyDetail.setDeleteFlag(0);
				yyDetail.setCreator(user.getId());
				yyDetail.setField3(user.getNickname());
				yyDetail.setUpdateTime(new Date());
				yyDetail.setUpdater(user.getId());
				yyDetail.setName(split4new[i]);
				yyDetail.setUrl(split5new[i]);
				yyDetail.setYyId(record.getId());
				yyDetail.setYyType("new");
				yyDetailService.insertSelective(yyDetail);
			}
		}
		return b;
	}
	
	
	@Override
	@Transactional
	public int updateByExampleSelective(YyNews record, YyNewsExample example) {
		
		return yyNewsMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	@Transactional
	public int updateByExample(YyNews record, YyNewsExample example) {
		
		return yyNewsMapper.updateByExample(record, example);
	}

	@Override
	public List<YyNews> selectByExample(YyNewsExample example) {
		
		return yyNewsMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(YyNewsExample example) {
		
		return yyNewsMapper.countByExample(example);
	}

	
	@Override
	@Transactional
	public int deleteByExample(YyNewsExample example) {
		
		return yyNewsMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param yyNews
	 * @return
	 */
	@Transactional
	public int updateVisible(YyNews yyNews) {
		return yyNewsMapper.updateByPrimaryKeySelective(yyNews);
	}



	@Transactional
	public int updateStatusByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyNewsMapper.updateStatusByPrimaryKey(aLong);
	}

	@Transactional
	public int shangjiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyNewsMapper.shangjiaByPrimaryKey(aLong);
	}

	@Transactional
	public int xiajiaByPrimaryKey(String ids) {
		Long aLong = Long.valueOf(ids);
		return yyNewsMapper.xiajiaByPrimaryKey(aLong);
	}

	public int updateByPrimaryKey(YyNews yyNews) {
		return yyNewsMapper.updateByPrimaryKey(yyNews);
	}
}
