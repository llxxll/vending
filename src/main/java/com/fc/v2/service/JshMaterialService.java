package com.fc.v2.service;

import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.v2.common.base.BaseService;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.mapper.auto.JshMaterialCarfpriceMapper;
import com.fc.v2.mapper.auto.JshMaterialMapper;
import com.fc.v2.mapper.auto.PlanOudetailMapper;
import com.fc.v2.mapper.auto.PlanOutMapper;
import com.fc.v2.mapper.auto.PlanProduceMaterialDetailMapper;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.JshMaterialExample;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOut;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;

/**
 * 产品表 JshMaterialService
 * @Title: JshMaterialService.java 
 * @Package com.fc.v2.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2021-12-19 21:40:36  
 **/
@Service
@Transactional
public class JshMaterialService implements BaseService<JshMaterial, JshMaterialExample>{
	@Autowired
	private JshMaterialMapper jshMaterialMapper;
	@Autowired
	private PlanOudetailMapper planOudetailMapper;
	@Autowired
	private JshMaterialCarfpriceMapper jshMaterialCarfpriceMapper;
	@Autowired
	private PlanProduceMaterialDetailMapper planProduceMaterialDetailMapper;
	@Autowired
	private  PlanOutMapper  planOutMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<JshMaterial> list(Tablepar tablepar,JshMaterial jshMaterial){
		 PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<JshMaterial> list= jshMaterialMapper.getList(jshMaterial);
	       for (JshMaterial jshMaterial2 : list) {
	    	 //获取该物料下的工艺
	    	   if(jshMaterial2.getCategoryId()==1) {
	    		   List<String> listByMaterid = jshMaterialCarfpriceMapper.getListNameByMaterid(jshMaterial2.getId());
	    		   jshMaterial2.setJshMaterialCarfpriceNameList(listByMaterid);
	    		   //获取该物料对应的材质
	    		   List<String> planoutList = planOutMapper.getListNameByMaterid(jshMaterial2.getId());
	    		   jshMaterial2.setPlanoutNameList(planoutList);
	    	   }
		}
	        PageInfo<JshMaterial> pageInfo = new PageInfo<JshMaterial>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			JshMaterialExample example=new JshMaterialExample();
			example.createCriteria().andIdIn(stringB);
			return jshMaterialMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public JshMaterial selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return jshMaterialMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(JshMaterial record) {
		JshMaterial selectByPrimaryKey = jshMaterialMapper.selectByPrimaryKey(record.getId());
		
		
		
		
		
		int insertSelective =0;
		if(record.getCategoryId()==1l) {
			JshMaterial jsh =jshMaterialMapper.selectByName(record.getName());
			if(jsh==null) {
				
				insertSelective= jshMaterialMapper.updateByPrimaryKeySelective(record);
			}else {
				if(record.getName().equals(selectByPrimaryKey.getName())) {
					
					insertSelective= jshMaterialMapper.updateByPrimaryKeySelective(record);
				}
			}
		}else {
			
			JshMaterial jsh  =jshMaterialMapper.selectByParams(record);
			if(jsh==null) {
				insertSelective= jshMaterialMapper.updateByPrimaryKeySelective(record);
			}
		}
		return insertSelective;
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(JshMaterial record) {
		record.setId(null);
		int insertSelective =0;
		if(record.getCategoryId()==1l) {
			JshMaterial jsh =jshMaterialMapper.selectByName(record.getName());
			if(jsh==null) {
				insertSelective= jshMaterialMapper.insertSelective(record);
				JshMaterialCarfprice jshMaterialCarfprice= new JshMaterialCarfprice();
				jshMaterialCarfprice.setTtff("成品");
				jshMaterialCarfprice.setMaterialId(record.getId());
				jshMaterialCarfprice.setCreateTime(new Date());
				jshMaterialCarfprice.setCreateUser(ShiroUtils.getUserId());
				jshMaterialCarfpriceMapper.insertSelective(jshMaterialCarfprice);
			}
		}else {
			
			JshMaterial jsh  =jshMaterialMapper.selectByParams(record);
			if(jsh==null) {
				insertSelective= jshMaterialMapper.insertSelective(record);
				
			}
		}
				
		return insertSelective;
	}
	
	
	@Override
	public int updateByExampleSelective(JshMaterial record, JshMaterialExample example) {
		
		return jshMaterialMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(JshMaterial record, JshMaterialExample example) {
		
		return jshMaterialMapper.updateByExample(record, example);
	}

	@Override
	public List<JshMaterial> selectByExample(JshMaterialExample example) {
		
		return jshMaterialMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(JshMaterialExample example) {
		
		return jshMaterialMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(JshMaterialExample example) {
		
		return jshMaterialMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param jshMaterial
	 * @return
	 */
	public int updateVisible(JshMaterial jshMaterial) {
		return jshMaterialMapper.updateByPrimaryKeySelective(jshMaterial);
	}

	public int upload(List<JshMaterial> execltoList) {
		int insertSelective =0;
		for (JshMaterial jshMaterial : execltoList) {
			if(jshMaterial.getName()==null) {
				continue;
			}
			if(jshMaterial.getName().trim().isEmpty()) {
				continue;
			}
			String categoryName = jshMaterial.getCategoryName();
			if(categoryName==null) {
				continue;
			}
			if(jshMaterial.getModel()!=null) {
				if(jshMaterial.getModel().trim().isEmpty()) {
					jshMaterial.setModel("/");
				}
			}
			
			
			
			if(categoryName.equals("物料")) {
				jshMaterial.setCategoryId(1l);
			}else if(categoryName.equals("材料")) {
				jshMaterial.setCategoryId(2l);
			}else if(categoryName.equals("辅料")) {
				jshMaterial.setCategoryId(3l);
			}else {
				continue;
			}
			if(jshMaterial.getCategoryId()==1l) {
				JshMaterial jsh =jshMaterialMapper.selectByName(jshMaterial.getName());
				if(jsh==null) {
					insertSelective= jshMaterialMapper.insertSelective(jshMaterial);
				}
			}else {
				
				JshMaterial jsh  =jshMaterialMapper.selectByParams(jshMaterial);
				if(jsh==null) {
					insertSelective= jshMaterialMapper.insertSelective(jshMaterial);
				}
			}
			
		}
		return insertSelective;
	}


}
