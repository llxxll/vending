package com.fc.v2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacadeResponse implements Serializable {
    // url, 包含?参数
    private String url;
    // 返回状态码
    private int statusCode;
    // 返回头信息
    private Map<String, List<String>> headers;
    // 返回entity内容
    private String entityContent;
}
