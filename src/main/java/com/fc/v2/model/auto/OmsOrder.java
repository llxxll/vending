package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;
import java.util.List;

public class OmsOrder implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	
	@ApiModelProperty(value = "订单号")
	private String orderNo;
	
	@ApiModelProperty(value = "买方")
	private Long buySupplySid;
	
	@ApiModelProperty(value = "")
	private String buySupplyName;
	
	@ApiModelProperty(value = "卖方")
	private Long sellSupplySid;
	
	@ApiModelProperty(value = "")
	private String sellSupplyName;
	
	@ApiModelProperty(value = "购买人")
	private Long userSid;
	
	@ApiModelProperty(value = "包装件数")
	private Long orderSourceSid;
	
	@ApiModelProperty(value = "状态1待付款,2待审核,3待排产4待结算,5待出库,6已完成")
	private Integer orderStatus;
	
	@ApiModelProperty(value = "1正常；-1挂起")
	private Integer hangBit;
	
	@ApiModelProperty(value = "商品件数")
	private Integer productNum;
	
	@ApiModelProperty(value = "邮费")
	private Long postage;
	
	@ApiModelProperty(value = "商品总金额")
	private BigDecimal productMoney;
	
	@ApiModelProperty(value = "订单金额（postage+product_money）")
	private Long orderMoney;
	
	@ApiModelProperty(value = "议价金额")
	private BigDecimal barginMoney;
	
	@ApiModelProperty(value = "应付金额")
	private BigDecimal payMoney;
	
	@ApiModelProperty(value = "来源")
	private Integer deliveryTypeSid;
	
	@ApiModelProperty(value = "0:无退货，1部分退货，2全部退货")
	private Integer refundFlag;
	
	@ApiModelProperty(value = "1：未删除，-1：回收站(扩展使用)")
	private Integer deleteFlag;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "技术字段")
	private Date ts;
	
	@ApiModelProperty(value = "打印次数计数")
	private String printNumber;

	@ApiModelProperty(value = "备注")
	private String remark;

	@ApiModelProperty(value = "快递")
	private String express;

	private List<OmsOrderDetail> mdProduct;

	private String openId;

	private String address;

	private String ly;

	private String ids;
	private String str1;
	private String str2;
	private String deliveryTypeName;
	private String createTimes;
	private String userName;
	private String updateTimes;

	public String getExpress() {
		return express;
	}

	public void setExpress(String express) {
		this.express = express;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRemark() {
		return remark;
	}

	public String getUpdateTimes() {
		return updateTimes;
	}

	public void setUpdateTimes(String updateTimes) {
		this.updateTimes = updateTimes;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateTimes() {
		return createTimes;
	}

	public void setCreateTimes(String createTimes) {
		this.createTimes = createTimes;
	}

	public String getDeliveryTypeName() {
		return deliveryTypeName;
	}

	public void setDeliveryTypeName(String deliveryTypeName) {
		this.deliveryTypeName = deliveryTypeName;
	}

	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 = str1;
	}

	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 = str2;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getLy() {
		return ly;
	}

	public void setLy(String ly) {
		this.ly = ly;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<OmsOrderDetail> getMdProduct() {
		return mdProduct;
	}

	public void setMdProduct(List<OmsOrderDetail> mdProduct) {
		this.mdProduct = mdProduct;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("orderNo")
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo =  orderNo;
	}
	@JsonProperty("buySupplySid")
	public Long getBuySupplySid() {
		return buySupplySid;
	}

	public void setBuySupplySid(Long buySupplySid) {
		this.buySupplySid =  buySupplySid;
	}
	@JsonProperty("buySupplyName")
	public String getBuySupplyName() {
		return buySupplyName;
	}

	public void setBuySupplyName(String buySupplyName) {
		this.buySupplyName =  buySupplyName;
	}
	@JsonProperty("sellSupplySid")
	public Long getSellSupplySid() {
		return sellSupplySid;
	}

	public void setSellSupplySid(Long sellSupplySid) {
		this.sellSupplySid =  sellSupplySid;
	}
	@JsonProperty("sellSupplyName")
	public String getSellSupplyName() {
		return sellSupplyName;
	}

	public void setSellSupplyName(String sellSupplyName) {
		this.sellSupplyName =  sellSupplyName;
	}
	@JsonProperty("userSid")
	public Long getUserSid() {
		return userSid;
	}

	public void setUserSid(Long userSid) {
		this.userSid =  userSid;
	}
	@JsonProperty("orderSourceSid")
	public Long getOrderSourceSid() {
		return orderSourceSid;
	}

	public void setOrderSourceSid(Long orderSourceSid) {
		this.orderSourceSid =  orderSourceSid;
	}
	@JsonProperty("orderStatus")
	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus =  orderStatus;
	}
	@JsonProperty("hangBit")
	public Integer getHangBit() {
		return hangBit;
	}

	public void setHangBit(Integer hangBit) {
		this.hangBit =  hangBit;
	}
	@JsonProperty("productNum")
	public Integer getProductNum() {
		return productNum;
	}

	public void setProductNum(Integer productNum) {
		this.productNum =  productNum;
	}
	@JsonProperty("postage")
	public Long getPostage() {
		return postage;
	}

	public void setPostage(Long postage) {
		this.postage =  postage;
	}
	@JsonProperty("productMoney")
	public BigDecimal getProductMoney() {
		return productMoney;
	}

	public void setProductMoney(BigDecimal productMoney) {
		this.productMoney =  productMoney;
	}
	@JsonProperty("orderMoney")
	public Long getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(Long orderMoney) {
		this.orderMoney =  orderMoney;
	}
	@JsonProperty("barginMoney")
	public BigDecimal getBarginMoney() {
		return barginMoney;
	}

	public void setBarginMoney(BigDecimal barginMoney) {
		this.barginMoney =  barginMoney;
	}
	@JsonProperty("payMoney")
	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney =  payMoney;
	}
	@JsonProperty("deliveryTypeSid")
	public Integer getDeliveryTypeSid() {
		return deliveryTypeSid;
	}

	public void setDeliveryTypeSid(Integer deliveryTypeSid) {
		this.deliveryTypeSid =  deliveryTypeSid;
	}
	@JsonProperty("refundFlag")
	public Integer getRefundFlag() {
		return refundFlag;
	}

	public void setRefundFlag(Integer refundFlag) {
		this.refundFlag =  refundFlag;
	}
	@JsonProperty("deleteFlag")
	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("ts")
	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts =  ts;
	}
	@JsonProperty("printNumber")
	public String getPrintNumber() {
		return printNumber;
	}

	public void setPrintNumber(String printNumber) {
		this.printNumber =  printNumber;
	}

																																												
	public OmsOrder(Long sid,String orderNo,Long buySupplySid,String buySupplyName,Long sellSupplySid,String sellSupplyName,Long userSid,Long orderSourceSid,Integer orderStatus,Integer hangBit,Integer productNum,Long postage,BigDecimal productMoney,Long orderMoney,BigDecimal barginMoney,BigDecimal payMoney,Integer deliveryTypeSid,Integer refundFlag,Integer deleteFlag,Date createTime,Date ts,String printNumber,List<OmsOrderDetail> mdProduct,String deliveryTypeName,String createTimes,String remark, String express) {
				
		this.sid = sid;
				
		this.orderNo = orderNo;
				
		this.buySupplySid = buySupplySid;
				
		this.buySupplyName = buySupplyName;
				
		this.sellSupplySid = sellSupplySid;
				
		this.sellSupplyName = sellSupplyName;
				
		this.userSid = userSid;
				
		this.orderSourceSid = orderSourceSid;
				
		this.orderStatus = orderStatus;
				
		this.hangBit = hangBit;
				
		this.productNum = productNum;
				
		this.postage = postage;
				
		this.productMoney = productMoney;
				
		this.orderMoney = orderMoney;
				
		this.barginMoney = barginMoney;
				
		this.payMoney = payMoney;
				
		this.deliveryTypeSid = deliveryTypeSid;
				
		this.refundFlag = refundFlag;
				
		this.deleteFlag = deleteFlag;
				
		this.createTime = createTime;
				
		this.ts = ts;
				
		this.printNumber = printNumber;

		this.mdProduct = mdProduct;

		this.deliveryTypeName = deliveryTypeName;

		this.createTimes = createTimes;

		this.remark = remark;

		this.express = express;

	}

	public OmsOrder() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private List<Long> delId;

	public List<Long> getDelId() {
		return delId;
	}

	public void setDelId(List<Long> delId) {
		this.delId = delId;
	}
	
	private String beginTime;
	private String endTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}