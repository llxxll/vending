package com.fc.v2.model.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 产品表 JshMaterialExample
 * @author fuce_自动生成
 * @date 2021-12-19 21:40:36
 */
public class JshMaterialExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public JshMaterialExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("Id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("Id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("Id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("Id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("Id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("Id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("Id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("Id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("Id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("Id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("Id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("Id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("Id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andCategoryIdIsNull() {
            addCriterion("CategoryId is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIsNotNull() {
            addCriterion("CategoryId is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdEqualTo(Long value) {
            addCriterion("CategoryId =", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotEqualTo(Long value) {
            addCriterion("CategoryId <>", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThan(Long value) {
            addCriterion("CategoryId >", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThanOrEqualTo(Long value) {
            addCriterion("CategoryId >=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThan(Long value) {
            addCriterion("CategoryId <", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThanOrEqualTo(Long value) {
            addCriterion("CategoryId <=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLike(Long value) {
            addCriterion("CategoryId like", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotLike(Long value) {
            addCriterion("CategoryId not like", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIn(List<Long> values) {
            addCriterion("CategoryId in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotIn(List<Long> values) {
            addCriterion("CategoryId not in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdBetween(Long value1, Long value2) {
            addCriterion("CategoryId between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotBetween(Long value1, Long value2) {
            addCriterion("CategoryId not between", value1, value2, "categoryId");
            return (Criteria) this;
        }
        
				
        public Criteria andNameIsNull() {
            addCriterion("Name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("Name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("Name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("Name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("Name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("Name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("Name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("Name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("Name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("Name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("Name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("Name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("Name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("Name not between", value1, value2, "name");
            return (Criteria) this;
        }
        
				
        public Criteria andMfrsIsNull() {
            addCriterion("Mfrs is null");
            return (Criteria) this;
        }

        public Criteria andMfrsIsNotNull() {
            addCriterion("Mfrs is not null");
            return (Criteria) this;
        }

        public Criteria andMfrsEqualTo(String value) {
            addCriterion("Mfrs =", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsNotEqualTo(String value) {
            addCriterion("Mfrs <>", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsGreaterThan(String value) {
            addCriterion("Mfrs >", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsGreaterThanOrEqualTo(String value) {
            addCriterion("Mfrs >=", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsLessThan(String value) {
            addCriterion("Mfrs <", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsLessThanOrEqualTo(String value) {
            addCriterion("Mfrs <=", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsLike(String value) {
            addCriterion("Mfrs like", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsNotLike(String value) {
            addCriterion("Mfrs not like", value, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsIn(List<String> values) {
            addCriterion("Mfrs in", values, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsNotIn(List<String> values) {
            addCriterion("Mfrs not in", values, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsBetween(String value1, String value2) {
            addCriterion("Mfrs between", value1, value2, "mfrs");
            return (Criteria) this;
        }

        public Criteria andMfrsNotBetween(String value1, String value2) {
            addCriterion("Mfrs not between", value1, value2, "mfrs");
            return (Criteria) this;
        }
        
				
        public Criteria andPackingIsNull() {
            addCriterion("Packing is null");
            return (Criteria) this;
        }

        public Criteria andPackingIsNotNull() {
            addCriterion("Packing is not null");
            return (Criteria) this;
        }

        public Criteria andPackingEqualTo(BigDecimal value) {
            addCriterion("Packing =", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotEqualTo(BigDecimal value) {
            addCriterion("Packing <>", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThan(BigDecimal value) {
            addCriterion("Packing >", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("Packing >=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThan(BigDecimal value) {
            addCriterion("Packing <", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThanOrEqualTo(BigDecimal value) {
            addCriterion("Packing <=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLike(BigDecimal value) {
            addCriterion("Packing like", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotLike(BigDecimal value) {
            addCriterion("Packing not like", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingIn(List<BigDecimal> values) {
            addCriterion("Packing in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotIn(List<BigDecimal> values) {
            addCriterion("Packing not in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("Packing between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("Packing not between", value1, value2, "packing");
            return (Criteria) this;
        }
        
				
        public Criteria andSafetyStockIsNull() {
            addCriterion("SafetyStock is null");
            return (Criteria) this;
        }

        public Criteria andSafetyStockIsNotNull() {
            addCriterion("SafetyStock is not null");
            return (Criteria) this;
        }

        public Criteria andSafetyStockEqualTo(BigDecimal value) {
            addCriterion("SafetyStock =", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockNotEqualTo(BigDecimal value) {
            addCriterion("SafetyStock <>", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockGreaterThan(BigDecimal value) {
            addCriterion("SafetyStock >", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SafetyStock >=", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockLessThan(BigDecimal value) {
            addCriterion("SafetyStock <", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SafetyStock <=", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockLike(BigDecimal value) {
            addCriterion("SafetyStock like", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockNotLike(BigDecimal value) {
            addCriterion("SafetyStock not like", value, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockIn(List<BigDecimal> values) {
            addCriterion("SafetyStock in", values, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockNotIn(List<BigDecimal> values) {
            addCriterion("SafetyStock not in", values, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SafetyStock between", value1, value2, "safetyStock");
            return (Criteria) this;
        }

        public Criteria andSafetyStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SafetyStock not between", value1, value2, "safetyStock");
            return (Criteria) this;
        }
        
				
        public Criteria andModelIsNull() {
            addCriterion("Model is null");
            return (Criteria) this;
        }

        public Criteria andModelIsNotNull() {
            addCriterion("Model is not null");
            return (Criteria) this;
        }

        public Criteria andModelEqualTo(String value) {
            addCriterion("Model =", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotEqualTo(String value) {
            addCriterion("Model <>", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelGreaterThan(String value) {
            addCriterion("Model >", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelGreaterThanOrEqualTo(String value) {
            addCriterion("Model >=", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLessThan(String value) {
            addCriterion("Model <", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLessThanOrEqualTo(String value) {
            addCriterion("Model <=", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLike(String value) {
            addCriterion("Model like", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotLike(String value) {
            addCriterion("Model not like", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelIn(List<String> values) {
            addCriterion("Model in", values, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotIn(List<String> values) {
            addCriterion("Model not in", values, "model");
            return (Criteria) this;
        }

        public Criteria andModelBetween(String value1, String value2) {
            addCriterion("Model between", value1, value2, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotBetween(String value1, String value2) {
            addCriterion("Model not between", value1, value2, "model");
            return (Criteria) this;
        }
        
				
        public Criteria andStandardIsNull() {
            addCriterion("Standard is null");
            return (Criteria) this;
        }

        public Criteria andStandardIsNotNull() {
            addCriterion("Standard is not null");
            return (Criteria) this;
        }

        public Criteria andStandardEqualTo(String value) {
            addCriterion("Standard =", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotEqualTo(String value) {
            addCriterion("Standard <>", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardGreaterThan(String value) {
            addCriterion("Standard >", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardGreaterThanOrEqualTo(String value) {
            addCriterion("Standard >=", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLessThan(String value) {
            addCriterion("Standard <", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLessThanOrEqualTo(String value) {
            addCriterion("Standard <=", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLike(String value) {
            addCriterion("Standard like", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotLike(String value) {
            addCriterion("Standard not like", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardIn(List<String> values) {
            addCriterion("Standard in", values, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotIn(List<String> values) {
            addCriterion("Standard not in", values, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardBetween(String value1, String value2) {
            addCriterion("Standard between", value1, value2, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotBetween(String value1, String value2) {
            addCriterion("Standard not between", value1, value2, "standard");
            return (Criteria) this;
        }
        
				
        public Criteria andColorIsNull() {
            addCriterion("Color is null");
            return (Criteria) this;
        }

        public Criteria andColorIsNotNull() {
            addCriterion("Color is not null");
            return (Criteria) this;
        }

        public Criteria andColorEqualTo(String value) {
            addCriterion("Color =", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorNotEqualTo(String value) {
            addCriterion("Color <>", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorGreaterThan(String value) {
            addCriterion("Color >", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorGreaterThanOrEqualTo(String value) {
            addCriterion("Color >=", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorLessThan(String value) {
            addCriterion("Color <", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorLessThanOrEqualTo(String value) {
            addCriterion("Color <=", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorLike(String value) {
            addCriterion("Color like", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorNotLike(String value) {
            addCriterion("Color not like", value, "color");
            return (Criteria) this;
        }

        public Criteria andColorIn(List<String> values) {
            addCriterion("Color in", values, "color");
            return (Criteria) this;
        }

        public Criteria andColorNotIn(List<String> values) {
            addCriterion("Color not in", values, "color");
            return (Criteria) this;
        }

        public Criteria andColorBetween(String value1, String value2) {
            addCriterion("Color between", value1, value2, "color");
            return (Criteria) this;
        }

        public Criteria andColorNotBetween(String value1, String value2) {
            addCriterion("Color not between", value1, value2, "color");
            return (Criteria) this;
        }
        
				
        public Criteria andUnitIsNull() {
            addCriterion("Unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("Unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(String value) {
            addCriterion("Unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(String value) {
            addCriterion("Unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(String value) {
            addCriterion("Unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(String value) {
            addCriterion("Unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(String value) {
            addCriterion("Unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(String value) {
            addCriterion("Unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLike(String value) {
            addCriterion("Unit like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotLike(String value) {
            addCriterion("Unit not like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<String> values) {
            addCriterion("Unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<String> values) {
            addCriterion("Unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(String value1, String value2) {
            addCriterion("Unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(String value1, String value2) {
            addCriterion("Unit not between", value1, value2, "unit");
            return (Criteria) this;
        }
        
				
        public Criteria andRemarkIsNull() {
            addCriterion("Remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("Remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("Remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("Remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("Remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("Remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("Remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("Remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("Remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("Remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("Remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("Remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("Remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("Remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
        
				
        public Criteria andRetailPriceIsNull() {
            addCriterion("RetailPrice is null");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIsNotNull() {
            addCriterion("RetailPrice is not null");
            return (Criteria) this;
        }

        public Criteria andRetailPriceEqualTo(BigDecimal value) {
            addCriterion("RetailPrice =", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotEqualTo(BigDecimal value) {
            addCriterion("RetailPrice <>", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceGreaterThan(BigDecimal value) {
            addCriterion("RetailPrice >", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RetailPrice >=", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLessThan(BigDecimal value) {
            addCriterion("RetailPrice <", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("RetailPrice <=", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLike(BigDecimal value) {
            addCriterion("RetailPrice like", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotLike(BigDecimal value) {
            addCriterion("RetailPrice not like", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIn(List<BigDecimal> values) {
            addCriterion("RetailPrice in", values, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotIn(List<BigDecimal> values) {
            addCriterion("RetailPrice not in", values, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RetailPrice between", value1, value2, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RetailPrice not between", value1, value2, "retailPrice");
            return (Criteria) this;
        }
        
				
        public Criteria andLowPriceIsNull() {
            addCriterion("LowPrice is null");
            return (Criteria) this;
        }

        public Criteria andLowPriceIsNotNull() {
            addCriterion("LowPrice is not null");
            return (Criteria) this;
        }

        public Criteria andLowPriceEqualTo(BigDecimal value) {
            addCriterion("LowPrice =", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotEqualTo(BigDecimal value) {
            addCriterion("LowPrice <>", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceGreaterThan(BigDecimal value) {
            addCriterion("LowPrice >", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LowPrice >=", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLessThan(BigDecimal value) {
            addCriterion("LowPrice <", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LowPrice <=", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLike(BigDecimal value) {
            addCriterion("LowPrice like", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotLike(BigDecimal value) {
            addCriterion("LowPrice not like", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceIn(List<BigDecimal> values) {
            addCriterion("LowPrice in", values, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotIn(List<BigDecimal> values) {
            addCriterion("LowPrice not in", values, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LowPrice between", value1, value2, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LowPrice not between", value1, value2, "lowPrice");
            return (Criteria) this;
        }
        
				
        public Criteria andPresetPriceOneIsNull() {
            addCriterion("PresetPriceOne is null");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneIsNotNull() {
            addCriterion("PresetPriceOne is not null");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneEqualTo(BigDecimal value) {
            addCriterion("PresetPriceOne =", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneNotEqualTo(BigDecimal value) {
            addCriterion("PresetPriceOne <>", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneGreaterThan(BigDecimal value) {
            addCriterion("PresetPriceOne >", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PresetPriceOne >=", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneLessThan(BigDecimal value) {
            addCriterion("PresetPriceOne <", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PresetPriceOne <=", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneLike(BigDecimal value) {
            addCriterion("PresetPriceOne like", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneNotLike(BigDecimal value) {
            addCriterion("PresetPriceOne not like", value, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneIn(List<BigDecimal> values) {
            addCriterion("PresetPriceOne in", values, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneNotIn(List<BigDecimal> values) {
            addCriterion("PresetPriceOne not in", values, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PresetPriceOne between", value1, value2, "presetPriceOne");
            return (Criteria) this;
        }

        public Criteria andPresetPriceOneNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PresetPriceOne not between", value1, value2, "presetPriceOne");
            return (Criteria) this;
        }
        
				
        public Criteria andPresetPriceTwoIsNull() {
            addCriterion("PresetPriceTwo is null");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoIsNotNull() {
            addCriterion("PresetPriceTwo is not null");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoEqualTo(BigDecimal value) {
            addCriterion("PresetPriceTwo =", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoNotEqualTo(BigDecimal value) {
            addCriterion("PresetPriceTwo <>", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoGreaterThan(BigDecimal value) {
            addCriterion("PresetPriceTwo >", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PresetPriceTwo >=", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoLessThan(BigDecimal value) {
            addCriterion("PresetPriceTwo <", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PresetPriceTwo <=", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoLike(BigDecimal value) {
            addCriterion("PresetPriceTwo like", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoNotLike(BigDecimal value) {
            addCriterion("PresetPriceTwo not like", value, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoIn(List<BigDecimal> values) {
            addCriterion("PresetPriceTwo in", values, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoNotIn(List<BigDecimal> values) {
            addCriterion("PresetPriceTwo not in", values, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PresetPriceTwo between", value1, value2, "presetPriceTwo");
            return (Criteria) this;
        }

        public Criteria andPresetPriceTwoNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PresetPriceTwo not between", value1, value2, "presetPriceTwo");
            return (Criteria) this;
        }
        
				
        public Criteria andUnitIdIsNull() {
            addCriterion("UnitId is null");
            return (Criteria) this;
        }

        public Criteria andUnitIdIsNotNull() {
            addCriterion("UnitId is not null");
            return (Criteria) this;
        }

        public Criteria andUnitIdEqualTo(Long value) {
            addCriterion("UnitId =", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdNotEqualTo(Long value) {
            addCriterion("UnitId <>", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdGreaterThan(Long value) {
            addCriterion("UnitId >", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdGreaterThanOrEqualTo(Long value) {
            addCriterion("UnitId >=", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdLessThan(Long value) {
            addCriterion("UnitId <", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdLessThanOrEqualTo(Long value) {
            addCriterion("UnitId <=", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdLike(Long value) {
            addCriterion("UnitId like", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdNotLike(Long value) {
            addCriterion("UnitId not like", value, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdIn(List<Long> values) {
            addCriterion("UnitId in", values, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdNotIn(List<Long> values) {
            addCriterion("UnitId not in", values, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdBetween(Long value1, Long value2) {
            addCriterion("UnitId between", value1, value2, "unitId");
            return (Criteria) this;
        }

        public Criteria andUnitIdNotBetween(Long value1, Long value2) {
            addCriterion("UnitId not between", value1, value2, "unitId");
            return (Criteria) this;
        }
        
				
        public Criteria andFirstOutUnitIsNull() {
            addCriterion("FirstOutUnit is null");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitIsNotNull() {
            addCriterion("FirstOutUnit is not null");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitEqualTo(String value) {
            addCriterion("FirstOutUnit =", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitNotEqualTo(String value) {
            addCriterion("FirstOutUnit <>", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitGreaterThan(String value) {
            addCriterion("FirstOutUnit >", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitGreaterThanOrEqualTo(String value) {
            addCriterion("FirstOutUnit >=", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitLessThan(String value) {
            addCriterion("FirstOutUnit <", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitLessThanOrEqualTo(String value) {
            addCriterion("FirstOutUnit <=", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitLike(String value) {
            addCriterion("FirstOutUnit like", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitNotLike(String value) {
            addCriterion("FirstOutUnit not like", value, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitIn(List<String> values) {
            addCriterion("FirstOutUnit in", values, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitNotIn(List<String> values) {
            addCriterion("FirstOutUnit not in", values, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitBetween(String value1, String value2) {
            addCriterion("FirstOutUnit between", value1, value2, "firstOutUnit");
            return (Criteria) this;
        }

        public Criteria andFirstOutUnitNotBetween(String value1, String value2) {
            addCriterion("FirstOutUnit not between", value1, value2, "firstOutUnit");
            return (Criteria) this;
        }
        
				
        public Criteria andFirstInUnitIsNull() {
            addCriterion("FirstInUnit is null");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitIsNotNull() {
            addCriterion("FirstInUnit is not null");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitEqualTo(String value) {
            addCriterion("FirstInUnit =", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitNotEqualTo(String value) {
            addCriterion("FirstInUnit <>", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitGreaterThan(String value) {
            addCriterion("FirstInUnit >", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitGreaterThanOrEqualTo(String value) {
            addCriterion("FirstInUnit >=", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitLessThan(String value) {
            addCriterion("FirstInUnit <", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitLessThanOrEqualTo(String value) {
            addCriterion("FirstInUnit <=", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitLike(String value) {
            addCriterion("FirstInUnit like", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitNotLike(String value) {
            addCriterion("FirstInUnit not like", value, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitIn(List<String> values) {
            addCriterion("FirstInUnit in", values, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitNotIn(List<String> values) {
            addCriterion("FirstInUnit not in", values, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitBetween(String value1, String value2) {
            addCriterion("FirstInUnit between", value1, value2, "firstInUnit");
            return (Criteria) this;
        }

        public Criteria andFirstInUnitNotBetween(String value1, String value2) {
            addCriterion("FirstInUnit not between", value1, value2, "firstInUnit");
            return (Criteria) this;
        }
        
				
        public Criteria andPriceStrategyIsNull() {
            addCriterion("PriceStrategy is null");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyIsNotNull() {
            addCriterion("PriceStrategy is not null");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyEqualTo(String value) {
            addCriterion("PriceStrategy =", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyNotEqualTo(String value) {
            addCriterion("PriceStrategy <>", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyGreaterThan(String value) {
            addCriterion("PriceStrategy >", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyGreaterThanOrEqualTo(String value) {
            addCriterion("PriceStrategy >=", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyLessThan(String value) {
            addCriterion("PriceStrategy <", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyLessThanOrEqualTo(String value) {
            addCriterion("PriceStrategy <=", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyLike(String value) {
            addCriterion("PriceStrategy like", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyNotLike(String value) {
            addCriterion("PriceStrategy not like", value, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyIn(List<String> values) {
            addCriterion("PriceStrategy in", values, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyNotIn(List<String> values) {
            addCriterion("PriceStrategy not in", values, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyBetween(String value1, String value2) {
            addCriterion("PriceStrategy between", value1, value2, "priceStrategy");
            return (Criteria) this;
        }

        public Criteria andPriceStrategyNotBetween(String value1, String value2) {
            addCriterion("PriceStrategy not between", value1, value2, "priceStrategy");
            return (Criteria) this;
        }
        
				
        public Criteria andEnabledIsNull() {
            addCriterion("Enabled is null");
            return (Criteria) this;
        }

        public Criteria andEnabledIsNotNull() {
            addCriterion("Enabled is not null");
            return (Criteria) this;
        }

        public Criteria andEnabledEqualTo(Byte value) {
            addCriterion("Enabled =", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotEqualTo(Byte value) {
            addCriterion("Enabled <>", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledGreaterThan(Byte value) {
            addCriterion("Enabled >", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledGreaterThanOrEqualTo(Byte value) {
            addCriterion("Enabled >=", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledLessThan(Byte value) {
            addCriterion("Enabled <", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledLessThanOrEqualTo(Byte value) {
            addCriterion("Enabled <=", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledLike(Byte value) {
            addCriterion("Enabled like", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotLike(Byte value) {
            addCriterion("Enabled not like", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledIn(List<Byte> values) {
            addCriterion("Enabled in", values, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotIn(List<Byte> values) {
            addCriterion("Enabled not in", values, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledBetween(Byte value1, Byte value2) {
            addCriterion("Enabled between", value1, value2, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotBetween(Byte value1, Byte value2) {
            addCriterion("Enabled not between", value1, value2, "enabled");
            return (Criteria) this;
        }
        
				
        public Criteria andOtherField1IsNull() {
            addCriterion("OtherField1 is null");
            return (Criteria) this;
        }

        public Criteria andOtherField1IsNotNull() {
            addCriterion("OtherField1 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherField1EqualTo(String value) {
            addCriterion("OtherField1 =", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1NotEqualTo(String value) {
            addCriterion("OtherField1 <>", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1GreaterThan(String value) {
            addCriterion("OtherField1 >", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1GreaterThanOrEqualTo(String value) {
            addCriterion("OtherField1 >=", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1LessThan(String value) {
            addCriterion("OtherField1 <", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1LessThanOrEqualTo(String value) {
            addCriterion("OtherField1 <=", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1Like(String value) {
            addCriterion("OtherField1 like", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1NotLike(String value) {
            addCriterion("OtherField1 not like", value, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1In(List<String> values) {
            addCriterion("OtherField1 in", values, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1NotIn(List<String> values) {
            addCriterion("OtherField1 not in", values, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1Between(String value1, String value2) {
            addCriterion("OtherField1 between", value1, value2, "otherField1");
            return (Criteria) this;
        }

        public Criteria andOtherField1NotBetween(String value1, String value2) {
            addCriterion("OtherField1 not between", value1, value2, "otherField1");
            return (Criteria) this;
        }
        
				
        public Criteria andOtherField2IsNull() {
            addCriterion("OtherField2 is null");
            return (Criteria) this;
        }

        public Criteria andOtherField2IsNotNull() {
            addCriterion("OtherField2 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherField2EqualTo(String value) {
            addCriterion("OtherField2 =", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2NotEqualTo(String value) {
            addCriterion("OtherField2 <>", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2GreaterThan(String value) {
            addCriterion("OtherField2 >", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2GreaterThanOrEqualTo(String value) {
            addCriterion("OtherField2 >=", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2LessThan(String value) {
            addCriterion("OtherField2 <", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2LessThanOrEqualTo(String value) {
            addCriterion("OtherField2 <=", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2Like(String value) {
            addCriterion("OtherField2 like", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2NotLike(String value) {
            addCriterion("OtherField2 not like", value, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2In(List<String> values) {
            addCriterion("OtherField2 in", values, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2NotIn(List<String> values) {
            addCriterion("OtherField2 not in", values, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2Between(String value1, String value2) {
            addCriterion("OtherField2 between", value1, value2, "otherField2");
            return (Criteria) this;
        }

        public Criteria andOtherField2NotBetween(String value1, String value2) {
            addCriterion("OtherField2 not between", value1, value2, "otherField2");
            return (Criteria) this;
        }
        
				
        public Criteria andOtherField3IsNull() {
            addCriterion("OtherField3 is null");
            return (Criteria) this;
        }

        public Criteria andOtherField3IsNotNull() {
            addCriterion("OtherField3 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherField3EqualTo(String value) {
            addCriterion("OtherField3 =", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3NotEqualTo(String value) {
            addCriterion("OtherField3 <>", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3GreaterThan(String value) {
            addCriterion("OtherField3 >", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3GreaterThanOrEqualTo(String value) {
            addCriterion("OtherField3 >=", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3LessThan(String value) {
            addCriterion("OtherField3 <", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3LessThanOrEqualTo(String value) {
            addCriterion("OtherField3 <=", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3Like(String value) {
            addCriterion("OtherField3 like", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3NotLike(String value) {
            addCriterion("OtherField3 not like", value, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3In(List<String> values) {
            addCriterion("OtherField3 in", values, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3NotIn(List<String> values) {
            addCriterion("OtherField3 not in", values, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3Between(String value1, String value2) {
            addCriterion("OtherField3 between", value1, value2, "otherField3");
            return (Criteria) this;
        }

        public Criteria andOtherField3NotBetween(String value1, String value2) {
            addCriterion("OtherField3 not between", value1, value2, "otherField3");
            return (Criteria) this;
        }
        
				
        public Criteria andEnableSerialNumberIsNull() {
            addCriterion("enableSerialNumber is null");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberIsNotNull() {
            addCriterion("enableSerialNumber is not null");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberEqualTo(String value) {
            addCriterion("enableSerialNumber =", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberNotEqualTo(String value) {
            addCriterion("enableSerialNumber <>", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberGreaterThan(String value) {
            addCriterion("enableSerialNumber >", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberGreaterThanOrEqualTo(String value) {
            addCriterion("enableSerialNumber >=", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberLessThan(String value) {
            addCriterion("enableSerialNumber <", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberLessThanOrEqualTo(String value) {
            addCriterion("enableSerialNumber <=", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberLike(String value) {
            addCriterion("enableSerialNumber like", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberNotLike(String value) {
            addCriterion("enableSerialNumber not like", value, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberIn(List<String> values) {
            addCriterion("enableSerialNumber in", values, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberNotIn(List<String> values) {
            addCriterion("enableSerialNumber not in", values, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberBetween(String value1, String value2) {
            addCriterion("enableSerialNumber between", value1, value2, "enableSerialNumber");
            return (Criteria) this;
        }

        public Criteria andEnableSerialNumberNotBetween(String value1, String value2) {
            addCriterion("enableSerialNumber not between", value1, value2, "enableSerialNumber");
            return (Criteria) this;
        }
        
				
        public Criteria andTenantIdIsNull() {
            addCriterion("tenant_id is null");
            return (Criteria) this;
        }

        public Criteria andTenantIdIsNotNull() {
            addCriterion("tenant_id is not null");
            return (Criteria) this;
        }

        public Criteria andTenantIdEqualTo(Long value) {
            addCriterion("tenant_id =", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotEqualTo(Long value) {
            addCriterion("tenant_id <>", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdGreaterThan(Long value) {
            addCriterion("tenant_id >", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdGreaterThanOrEqualTo(Long value) {
            addCriterion("tenant_id >=", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLessThan(Long value) {
            addCriterion("tenant_id <", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLessThanOrEqualTo(Long value) {
            addCriterion("tenant_id <=", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLike(Long value) {
            addCriterion("tenant_id like", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotLike(Long value) {
            addCriterion("tenant_id not like", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdIn(List<Long> values) {
            addCriterion("tenant_id in", values, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotIn(List<Long> values) {
            addCriterion("tenant_id not in", values, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdBetween(Long value1, Long value2) {
            addCriterion("tenant_id between", value1, value2, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotBetween(Long value1, Long value2) {
            addCriterion("tenant_id not between", value1, value2, "tenantId");
            return (Criteria) this;
        }
        
				
        public Criteria andDeleteFlagIsNull() {
            addCriterion("delete_Flag is null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIsNotNull() {
            addCriterion("delete_Flag is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagEqualTo(String value) {
            addCriterion("delete_Flag =", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotEqualTo(String value) {
            addCriterion("delete_Flag <>", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThan(String value) {
            addCriterion("delete_Flag >", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThanOrEqualTo(String value) {
            addCriterion("delete_Flag >=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThan(String value) {
            addCriterion("delete_Flag <", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThanOrEqualTo(String value) {
            addCriterion("delete_Flag <=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLike(String value) {
            addCriterion("delete_Flag like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotLike(String value) {
            addCriterion("delete_Flag not like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIn(List<String> values) {
            addCriterion("delete_Flag in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotIn(List<String> values) {
            addCriterion("delete_Flag not in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagBetween(String value1, String value2) {
            addCriterion("delete_Flag between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotBetween(String value1, String value2) {
            addCriterion("delete_Flag not between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(JshMaterial record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(Id,'')");
    		}
			if(record.getCategoryId()!=null&&StrUtil.isNotEmpty(record.getCategoryId().toString())) {
    			 list.add("ifnull(CategoryId,'')");
    		}
			if(record.getName()!=null&&StrUtil.isNotEmpty(record.getName().toString())) {
    			 list.add("ifnull(Name,'')");
    		}
			if(record.getMfrs()!=null&&StrUtil.isNotEmpty(record.getMfrs().toString())) {
    			 list.add("ifnull(Mfrs,'')");
    		}
			if(record.getPacking()!=null&&StrUtil.isNotEmpty(record.getPacking().toString())) {
    			 list.add("ifnull(Packing,'')");
    		}
			if(record.getSafetyStock()!=null&&StrUtil.isNotEmpty(record.getSafetyStock().toString())) {
    			 list.add("ifnull(SafetyStock,'')");
    		}
			if(record.getModel()!=null&&StrUtil.isNotEmpty(record.getModel().toString())) {
    			 list.add("ifnull(Model,'')");
    		}
			if(record.getStandard()!=null&&StrUtil.isNotEmpty(record.getStandard().toString())) {
    			 list.add("ifnull(Standard,'')");
    		}
			if(record.getColor()!=null&&StrUtil.isNotEmpty(record.getColor().toString())) {
    			 list.add("ifnull(Color,'')");
    		}
			if(record.getUnit()!=null&&StrUtil.isNotEmpty(record.getUnit().toString())) {
    			 list.add("ifnull(Unit,'')");
    		}
			if(record.getRemark()!=null&&StrUtil.isNotEmpty(record.getRemark().toString())) {
    			 list.add("ifnull(Remark,'')");
    		}
			if(record.getRetailPrice()!=null&&StrUtil.isNotEmpty(record.getRetailPrice().toString())) {
    			 list.add("ifnull(RetailPrice,'')");
    		}
			if(record.getLowPrice()!=null&&StrUtil.isNotEmpty(record.getLowPrice().toString())) {
    			 list.add("ifnull(LowPrice,'')");
    		}
			if(record.getPresetPriceOne()!=null&&StrUtil.isNotEmpty(record.getPresetPriceOne().toString())) {
    			 list.add("ifnull(PresetPriceOne,'')");
    		}
			if(record.getPresetPriceTwo()!=null&&StrUtil.isNotEmpty(record.getPresetPriceTwo().toString())) {
    			 list.add("ifnull(PresetPriceTwo,'')");
    		}
			if(record.getUnitId()!=null&&StrUtil.isNotEmpty(record.getUnitId().toString())) {
    			 list.add("ifnull(UnitId,'')");
    		}
			if(record.getFirstOutUnit()!=null&&StrUtil.isNotEmpty(record.getFirstOutUnit().toString())) {
    			 list.add("ifnull(FirstOutUnit,'')");
    		}
			if(record.getFirstInUnit()!=null&&StrUtil.isNotEmpty(record.getFirstInUnit().toString())) {
    			 list.add("ifnull(FirstInUnit,'')");
    		}
			if(record.getPriceStrategy()!=null&&StrUtil.isNotEmpty(record.getPriceStrategy().toString())) {
    			 list.add("ifnull(PriceStrategy,'')");
    		}
			if(record.getEnabled()!=null&&StrUtil.isNotEmpty(record.getEnabled().toString())) {
    			 list.add("ifnull(Enabled,'')");
    		}
			if(record.getOtherField1()!=null&&StrUtil.isNotEmpty(record.getOtherField1().toString())) {
    			 list.add("ifnull(OtherField1,'')");
    		}
			if(record.getOtherField2()!=null&&StrUtil.isNotEmpty(record.getOtherField2().toString())) {
    			 list.add("ifnull(OtherField2,'')");
    		}
			if(record.getOtherField3()!=null&&StrUtil.isNotEmpty(record.getOtherField3().toString())) {
    			 list.add("ifnull(OtherField3,'')");
    		}
			if(record.getEnableSerialNumber()!=null&&StrUtil.isNotEmpty(record.getEnableSerialNumber().toString())) {
    			 list.add("ifnull(enableSerialNumber,'')");
    		}
			if(record.getTenantId()!=null&&StrUtil.isNotEmpty(record.getTenantId().toString())) {
    			 list.add("ifnull(tenant_id,'')");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			 list.add("ifnull(delete_Flag,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getCategoryId()!=null&&StrUtil.isNotEmpty(record.getCategoryId().toString())) {
    			list2.add("'%"+record.getCategoryId()+"%'");
    		}
			if(record.getName()!=null&&StrUtil.isNotEmpty(record.getName().toString())) {
    			list2.add("'%"+record.getName()+"%'");
    		}
			if(record.getMfrs()!=null&&StrUtil.isNotEmpty(record.getMfrs().toString())) {
    			list2.add("'%"+record.getMfrs()+"%'");
    		}
			if(record.getPacking()!=null&&StrUtil.isNotEmpty(record.getPacking().toString())) {
    			list2.add("'%"+record.getPacking()+"%'");
    		}
			if(record.getSafetyStock()!=null&&StrUtil.isNotEmpty(record.getSafetyStock().toString())) {
    			list2.add("'%"+record.getSafetyStock()+"%'");
    		}
			if(record.getModel()!=null&&StrUtil.isNotEmpty(record.getModel().toString())) {
    			list2.add("'%"+record.getModel()+"%'");
    		}
			if(record.getStandard()!=null&&StrUtil.isNotEmpty(record.getStandard().toString())) {
    			list2.add("'%"+record.getStandard()+"%'");
    		}
			if(record.getColor()!=null&&StrUtil.isNotEmpty(record.getColor().toString())) {
    			list2.add("'%"+record.getColor()+"%'");
    		}
			if(record.getUnit()!=null&&StrUtil.isNotEmpty(record.getUnit().toString())) {
    			list2.add("'%"+record.getUnit()+"%'");
    		}
			if(record.getRemark()!=null&&StrUtil.isNotEmpty(record.getRemark().toString())) {
    			list2.add("'%"+record.getRemark()+"%'");
    		}
			if(record.getRetailPrice()!=null&&StrUtil.isNotEmpty(record.getRetailPrice().toString())) {
    			list2.add("'%"+record.getRetailPrice()+"%'");
    		}
			if(record.getLowPrice()!=null&&StrUtil.isNotEmpty(record.getLowPrice().toString())) {
    			list2.add("'%"+record.getLowPrice()+"%'");
    		}
			if(record.getPresetPriceOne()!=null&&StrUtil.isNotEmpty(record.getPresetPriceOne().toString())) {
    			list2.add("'%"+record.getPresetPriceOne()+"%'");
    		}
			if(record.getPresetPriceTwo()!=null&&StrUtil.isNotEmpty(record.getPresetPriceTwo().toString())) {
    			list2.add("'%"+record.getPresetPriceTwo()+"%'");
    		}
			if(record.getUnitId()!=null&&StrUtil.isNotEmpty(record.getUnitId().toString())) {
    			list2.add("'%"+record.getUnitId()+"%'");
    		}
			if(record.getFirstOutUnit()!=null&&StrUtil.isNotEmpty(record.getFirstOutUnit().toString())) {
    			list2.add("'%"+record.getFirstOutUnit()+"%'");
    		}
			if(record.getFirstInUnit()!=null&&StrUtil.isNotEmpty(record.getFirstInUnit().toString())) {
    			list2.add("'%"+record.getFirstInUnit()+"%'");
    		}
			if(record.getPriceStrategy()!=null&&StrUtil.isNotEmpty(record.getPriceStrategy().toString())) {
    			list2.add("'%"+record.getPriceStrategy()+"%'");
    		}
			if(record.getEnabled()!=null&&StrUtil.isNotEmpty(record.getEnabled().toString())) {
    			list2.add("'%"+record.getEnabled()+"%'");
    		}
			if(record.getOtherField1()!=null&&StrUtil.isNotEmpty(record.getOtherField1().toString())) {
    			list2.add("'%"+record.getOtherField1()+"%'");
    		}
			if(record.getOtherField2()!=null&&StrUtil.isNotEmpty(record.getOtherField2().toString())) {
    			list2.add("'%"+record.getOtherField2()+"%'");
    		}
			if(record.getOtherField3()!=null&&StrUtil.isNotEmpty(record.getOtherField3().toString())) {
    			list2.add("'%"+record.getOtherField3()+"%'");
    		}
			if(record.getEnableSerialNumber()!=null&&StrUtil.isNotEmpty(record.getEnableSerialNumber().toString())) {
    			list2.add("'%"+record.getEnableSerialNumber()+"%'");
    		}
			if(record.getTenantId()!=null&&StrUtil.isNotEmpty(record.getTenantId().toString())) {
    			list2.add("'%"+record.getTenantId()+"%'");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			list2.add("'%"+record.getDeleteFlag()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(Id,'')");
    		list.add("ifnull(CategoryId,'')");
    		list.add("ifnull(Name,'')");
    		list.add("ifnull(Mfrs,'')");
    		list.add("ifnull(Packing,'')");
    		list.add("ifnull(SafetyStock,'')");
    		list.add("ifnull(Model,'')");
    		list.add("ifnull(Standard,'')");
    		list.add("ifnull(Color,'')");
    		list.add("ifnull(Unit,'')");
    		list.add("ifnull(Remark,'')");
    		list.add("ifnull(RetailPrice,'')");
    		list.add("ifnull(LowPrice,'')");
    		list.add("ifnull(PresetPriceOne,'')");
    		list.add("ifnull(PresetPriceTwo,'')");
    		list.add("ifnull(UnitId,'')");
    		list.add("ifnull(FirstOutUnit,'')");
    		list.add("ifnull(FirstInUnit,'')");
    		list.add("ifnull(PriceStrategy,'')");
    		list.add("ifnull(Enabled,'')");
    		list.add("ifnull(OtherField1,'')");
    		list.add("ifnull(OtherField2,'')");
    		list.add("ifnull(OtherField3,'')");
    		list.add("ifnull(enableSerialNumber,'')");
    		list.add("ifnull(tenant_id,'')");
    		list.add("ifnull(delete_Flag,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}