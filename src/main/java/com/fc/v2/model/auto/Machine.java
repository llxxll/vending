package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class Machine implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "机器名称")
	private String machineName;
	
	@ApiModelProperty(value = "机器编码")
	private String machineCode;
	
	@ApiModelProperty(value = "分组")
	private String machineGrouping;
	
	@ApiModelProperty(value = "补货员id")
	private String replenishmanId;
	
	@ApiModelProperty(value = "补货员姓名")
	private String replenishmanName;
	
	@ApiModelProperty(value = "数据员id")
	private String dataOfficer;
	
	@ApiModelProperty(value = "数据员姓名")
	private String dataName;
	
	@ApiModelProperty(value = "商户")
	private String merchant;
	
	@ApiModelProperty(value = "资产编号")
	private String assetNumber;
	
	@ApiModelProperty(value = "柜体码")
	private String cabinetCode;
	
	@ApiModelProperty(value = "部门id")
	private String departmentId;
	
	@ApiModelProperty(value = "部门名称")
	private String departmentName;
	
	@ApiModelProperty(value = "sim卡")
	private String sim;
	
	@ApiModelProperty(value = "摄像头状态")
	private String cameraStatus;
	
	@ApiModelProperty(value = "网络信号")
	private String networkSignal;
	
	@ApiModelProperty(value = "投放场景")
	private String launchScenario;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@ApiModelProperty(value = "地址")
	private String address;
	
	@ApiModelProperty(value = "备注")
	private String pointNotes;
	
	@ApiModelProperty(value = "描述")
	private String pointDesc;
	
	@ApiModelProperty(value = "经度")
	private String longitude;
	
	@ApiModelProperty(value = "纬度")
	private String latitude;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	
	@ApiModelProperty(value = "1：默认正常数据；-1表示数据被删除")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	
	@ApiModelProperty(value = "")
	private String str1;
	
	@ApiModelProperty(value = "")
	private String str2;
	
	@ApiModelProperty(value = "")
	private String str3;
	
	@ApiModelProperty(value = "")
	private Long num1;
	
	@ApiModelProperty(value = "")
	private Integer num2;
	
	@ApiModelProperty(value = "")
	private Integer num3;
	
	@ApiModelProperty(value = "省份")
	private String provinceCode;
	
	@ApiModelProperty(value = "城市")
	private String cityCode;
	
	@ApiModelProperty(value = "区")
	private String areaCode;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("machineName")
	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName =  machineName;
	}
	@JsonProperty("machineCode")
	public String getMachineCode() {
		return machineCode;
	}

	public void setMachineCode(String machineCode) {
		this.machineCode =  machineCode;
	}
	@JsonProperty("machineGrouping")
	public String getMachineGrouping() {
		return machineGrouping;
	}

	public void setMachineGrouping(String machineGrouping) {
		this.machineGrouping =  machineGrouping;
	}
	@JsonProperty("replenishmanId")
	public String getReplenishmanId() {
		return replenishmanId;
	}

	public void setReplenishmanId(String replenishmanId) {
		this.replenishmanId =  replenishmanId;
	}
	@JsonProperty("replenishmanName")
	public String getReplenishmanName() {
		return replenishmanName;
	}

	public void setReplenishmanName(String replenishmanName) {
		this.replenishmanName =  replenishmanName;
	}
	@JsonProperty("dataOfficer")
	public String getDataOfficer() {
		return dataOfficer;
	}

	public void setDataOfficer(String dataOfficer) {
		this.dataOfficer =  dataOfficer;
	}
	@JsonProperty("dataName")
	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName =  dataName;
	}
	@JsonProperty("merchant")
	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant =  merchant;
	}
	@JsonProperty("assetNumber")
	public String getAssetNumber() {
		return assetNumber;
	}

	public void setAssetNumber(String assetNumber) {
		this.assetNumber =  assetNumber;
	}
	@JsonProperty("cabinetCode")
	public String getCabinetCode() {
		return cabinetCode;
	}

	public void setCabinetCode(String cabinetCode) {
		this.cabinetCode =  cabinetCode;
	}
	@JsonProperty("departmentId")
	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId =  departmentId;
	}
	@JsonProperty("departmentName")
	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName =  departmentName;
	}
	@JsonProperty("sim")
	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim =  sim;
	}
	@JsonProperty("cameraStatus")
	public String getCameraStatus() {
		return cameraStatus;
	}

	public void setCameraStatus(String cameraStatus) {
		this.cameraStatus =  cameraStatus;
	}
	@JsonProperty("networkSignal")
	public String getNetworkSignal() {
		return networkSignal;
	}

	public void setNetworkSignal(String networkSignal) {
		this.networkSignal =  networkSignal;
	}
	@JsonProperty("launchScenario")
	public String getLaunchScenario() {
		return launchScenario;
	}

	public void setLaunchScenario(String launchScenario) {
		this.launchScenario =  launchScenario;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address =  address;
	}
	@JsonProperty("pointNotes")
	public String getPointNotes() {
		return pointNotes;
	}

	public void setPointNotes(String pointNotes) {
		this.pointNotes =  pointNotes;
	}
	@JsonProperty("pointDesc")
	public String getPointDesc() {
		return pointDesc;
	}

	public void setPointDesc(String pointDesc) {
		this.pointDesc =  pointDesc;
	}
	@JsonProperty("longitude")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude =  longitude;
	}
	@JsonProperty("latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude =  latitude;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	@JsonProperty("str1")
	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 =  str1;
	}
	@JsonProperty("str2")
	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 =  str2;
	}
	@JsonProperty("str3")
	public String getStr3() {
		return str3;
	}

	public void setStr3(String str3) {
		this.str3 =  str3;
	}
	@JsonProperty("num1")
	public Long getNum1() {
		return num1;
	}

	public void setNum1(Long num1) {
		this.num1 =  num1;
	}
	@JsonProperty("num2")
	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 =  num2;
	}
	@JsonProperty("num3")
	public Integer getNum3() {
		return num3;
	}

	public void setNum3(Integer num3) {
		this.num3 =  num3;
	}
	@JsonProperty("provinceCode")
	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode =  provinceCode;
	}
	@JsonProperty("cityCode")
	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode =  cityCode;
	}
	@JsonProperty("areaCode")
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode =  areaCode;
	}

																																																																														
	public Machine(Long id,String machineName,String machineCode,String machineGrouping,String replenishmanId,String replenishmanName,String dataOfficer,String dataName,String merchant,String assetNumber,String cabinetCode,String departmentId,String departmentName,String sim,String cameraStatus,String networkSignal,String launchScenario,Integer status,String address,String pointNotes,String pointDesc,String longitude,String latitude,Date createdTime,Long createdUserSid,Date modifiedTime,Long modifiedUserSid,Integer x,Date xTime,Long xUserSid,String str1,String str2,String str3,Long num1,Integer num2,Integer num3,String provinceCode,String cityCode,String areaCode) {
				
		this.id = id;
				
		this.machineName = machineName;
				
		this.machineCode = machineCode;
				
		this.machineGrouping = machineGrouping;
				
		this.replenishmanId = replenishmanId;
				
		this.replenishmanName = replenishmanName;
				
		this.dataOfficer = dataOfficer;
				
		this.dataName = dataName;
				
		this.merchant = merchant;
				
		this.assetNumber = assetNumber;
				
		this.cabinetCode = cabinetCode;
				
		this.departmentId = departmentId;
				
		this.departmentName = departmentName;
				
		this.sim = sim;
				
		this.cameraStatus = cameraStatus;
				
		this.networkSignal = networkSignal;
				
		this.launchScenario = launchScenario;
				
		this.status = status;
				
		this.address = address;
				
		this.pointNotes = pointNotes;
				
		this.pointDesc = pointDesc;
				
		this.longitude = longitude;
				
		this.latitude = latitude;
				
		this.createdTime = createdTime;
				
		this.createdUserSid = createdUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.str1 = str1;
				
		this.str2 = str2;
				
		this.str3 = str3;
				
		this.num1 = num1;
				
		this.num2 = num2;
				
		this.num3 = num3;
				
		this.provinceCode = provinceCode;
				
		this.cityCode = cityCode;
				
		this.areaCode = areaCode;
				
	}

	public Machine() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}