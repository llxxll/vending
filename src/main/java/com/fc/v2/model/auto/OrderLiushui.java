package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class OrderLiushui implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "")
	private Long userid;
	
	@ApiModelProperty(value = "")
	private String title;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private String type;
	
	@ApiModelProperty(value = "")
	private String ttxx;
	
	@ApiModelProperty(value = "")
	private String ttff;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("userid")
	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid =  userid;
	}
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title =  title;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type =  type;
	}
	@JsonProperty("ttxx")
	public String getTtxx() {
		return ttxx;
	}

	public void setTtxx(String ttxx) {
		this.ttxx =  ttxx;
	}
	@JsonProperty("ttff")
	public String getTtff() {
		return ttff;
	}

	public void setTtff(String ttff) {
		this.ttff =  ttff;
	}

														
	public OrderLiushui(Long id,Long userid,String title,Date createTime,String type,String ttxx,String ttff) {
				
		this.id = id;
				
		this.userid = userid;
				
		this.title = title;
				
		this.createTime = createTime;
				
		this.type = type;
				
		this.ttxx = ttxx;
				
		this.ttff = ttff;
				
	}

	public OrderLiushui() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
	
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}