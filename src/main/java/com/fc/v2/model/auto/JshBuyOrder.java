package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;
import java.util.List;

public class JshBuyOrder implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "采购单号")
	private String no;
	
	@ApiModelProperty(value = "总数量")
	private Integer count;
	
	@ApiModelProperty(value = "状态1入库2已入库3已完成4已结算")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date updateTime;
	
	@ApiModelProperty(value = "")
	private Long updater;
	
	@ApiModelProperty(value = "总金额")
	private String orderPrice;
	
	@ApiModelProperty(value = "供货商")
	private Long buyUser;
	
	@ApiModelProperty(value = "预留字段")
	private String field3;

	private List<JshBuyOrderPz> materials;

	private String houseNo;

	public List<JshBuyOrderPz> getMaterials() {
		return materials;
	}

	public void setMaterials(List<JshBuyOrderPz> materials) {
		this.materials = materials;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("no")
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no =  no;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("orderPrice")
	public String getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(String orderPrice) {
		this.orderPrice =  orderPrice;
	}
	@JsonProperty("buyUser")
	public Long getBuyUser() {
		return buyUser;
	}

	public void setBuyUser(Long buyUser) {
		this.buyUser =  buyUser;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}

																						
	public JshBuyOrder(Long id,String no,Integer count,Integer status,Date createTime,Long creator,Date updateTime,Long updater,String orderPrice,Long buyUser,String field3) {
				
		this.id = id;
				
		this.no = no;
				
		this.count = count;
				
		this.status = status;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
		this.orderPrice = orderPrice;
				
		this.buyUser = buyUser;
				
		this.field3 = field3;
				
	}

	public JshBuyOrder() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}