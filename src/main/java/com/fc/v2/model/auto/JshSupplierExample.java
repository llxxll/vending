package com.fc.v2.model.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 供应商/客户信息表 JshSupplierExample
 * @author fuce_自动生成
 * @date 2021-12-19 23:17:26
 */
public class JshSupplierExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public JshSupplierExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andSupplierIsNull() {
            addCriterion("supplier is null");
            return (Criteria) this;
        }

        public Criteria andSupplierIsNotNull() {
            addCriterion("supplier is not null");
            return (Criteria) this;
        }

        public Criteria andSupplierEqualTo(String value) {
            addCriterion("supplier =", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotEqualTo(String value) {
            addCriterion("supplier <>", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierGreaterThan(String value) {
            addCriterion("supplier >", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierGreaterThanOrEqualTo(String value) {
            addCriterion("supplier >=", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierLessThan(String value) {
            addCriterion("supplier <", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierLessThanOrEqualTo(String value) {
            addCriterion("supplier <=", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierLike(String value) {
            addCriterion("supplier like", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotLike(String value) {
            addCriterion("supplier not like", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierIn(List<String> values) {
            addCriterion("supplier in", values, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotIn(List<String> values) {
            addCriterion("supplier not in", values, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierBetween(String value1, String value2) {
            addCriterion("supplier between", value1, value2, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotBetween(String value1, String value2) {
            addCriterion("supplier not between", value1, value2, "supplier");
            return (Criteria) this;
        }
        
				
        public Criteria andContactsIsNull() {
            addCriterion("contacts is null");
            return (Criteria) this;
        }

        public Criteria andContactsIsNotNull() {
            addCriterion("contacts is not null");
            return (Criteria) this;
        }

        public Criteria andContactsEqualTo(String value) {
            addCriterion("contacts =", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsNotEqualTo(String value) {
            addCriterion("contacts <>", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsGreaterThan(String value) {
            addCriterion("contacts >", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsGreaterThanOrEqualTo(String value) {
            addCriterion("contacts >=", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsLessThan(String value) {
            addCriterion("contacts <", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsLessThanOrEqualTo(String value) {
            addCriterion("contacts <=", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsLike(String value) {
            addCriterion("contacts like", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsNotLike(String value) {
            addCriterion("contacts not like", value, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsIn(List<String> values) {
            addCriterion("contacts in", values, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsNotIn(List<String> values) {
            addCriterion("contacts not in", values, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsBetween(String value1, String value2) {
            addCriterion("contacts between", value1, value2, "contacts");
            return (Criteria) this;
        }

        public Criteria andContactsNotBetween(String value1, String value2) {
            addCriterion("contacts not between", value1, value2, "contacts");
            return (Criteria) this;
        }
        
				
        public Criteria andPhonenumIsNull() {
            addCriterion("phonenum is null");
            return (Criteria) this;
        }

        public Criteria andPhonenumIsNotNull() {
            addCriterion("phonenum is not null");
            return (Criteria) this;
        }

        public Criteria andPhonenumEqualTo(String value) {
            addCriterion("phonenum =", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotEqualTo(String value) {
            addCriterion("phonenum <>", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumGreaterThan(String value) {
            addCriterion("phonenum >", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumGreaterThanOrEqualTo(String value) {
            addCriterion("phonenum >=", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumLessThan(String value) {
            addCriterion("phonenum <", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumLessThanOrEqualTo(String value) {
            addCriterion("phonenum <=", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumLike(String value) {
            addCriterion("phonenum like", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotLike(String value) {
            addCriterion("phonenum not like", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumIn(List<String> values) {
            addCriterion("phonenum in", values, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotIn(List<String> values) {
            addCriterion("phonenum not in", values, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumBetween(String value1, String value2) {
            addCriterion("phonenum between", value1, value2, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotBetween(String value1, String value2) {
            addCriterion("phonenum not between", value1, value2, "phonenum");
            return (Criteria) this;
        }
        
				
        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }
        
				
        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }
        
				
        public Criteria andIsystemIsNull() {
            addCriterion("isystem is null");
            return (Criteria) this;
        }

        public Criteria andIsystemIsNotNull() {
            addCriterion("isystem is not null");
            return (Criteria) this;
        }

        public Criteria andIsystemEqualTo(Integer value) {
            addCriterion("isystem =", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemNotEqualTo(Integer value) {
            addCriterion("isystem <>", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemGreaterThan(Integer value) {
            addCriterion("isystem >", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemGreaterThanOrEqualTo(Integer value) {
            addCriterion("isystem >=", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemLessThan(Integer value) {
            addCriterion("isystem <", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemLessThanOrEqualTo(Integer value) {
            addCriterion("isystem <=", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemLike(Integer value) {
            addCriterion("isystem like", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemNotLike(Integer value) {
            addCriterion("isystem not like", value, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemIn(List<Integer> values) {
            addCriterion("isystem in", values, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemNotIn(List<Integer> values) {
            addCriterion("isystem not in", values, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemBetween(Integer value1, Integer value2) {
            addCriterion("isystem between", value1, value2, "isystem");
            return (Criteria) this;
        }

        public Criteria andIsystemNotBetween(Integer value1, Integer value2) {
            addCriterion("isystem not between", value1, value2, "isystem");
            return (Criteria) this;
        }
        
				
        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }
        
				
        public Criteria andEnabledIsNull() {
            addCriterion("enabled is null");
            return (Criteria) this;
        }

        public Criteria andEnabledIsNotNull() {
            addCriterion("enabled is not null");
            return (Criteria) this;
        }

        public Criteria andEnabledEqualTo(Byte value) {
            addCriterion("enabled =", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotEqualTo(Byte value) {
            addCriterion("enabled <>", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledGreaterThan(Byte value) {
            addCriterion("enabled >", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledGreaterThanOrEqualTo(Byte value) {
            addCriterion("enabled >=", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledLessThan(Byte value) {
            addCriterion("enabled <", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledLessThanOrEqualTo(Byte value) {
            addCriterion("enabled <=", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledLike(Byte value) {
            addCriterion("enabled like", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotLike(Byte value) {
            addCriterion("enabled not like", value, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledIn(List<Byte> values) {
            addCriterion("enabled in", values, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotIn(List<Byte> values) {
            addCriterion("enabled not in", values, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledBetween(Byte value1, Byte value2) {
            addCriterion("enabled between", value1, value2, "enabled");
            return (Criteria) this;
        }

        public Criteria andEnabledNotBetween(Byte value1, Byte value2) {
            addCriterion("enabled not between", value1, value2, "enabled");
            return (Criteria) this;
        }
        
				
        public Criteria andAdvanceInIsNull() {
            addCriterion("AdvanceIn is null");
            return (Criteria) this;
        }

        public Criteria andAdvanceInIsNotNull() {
            addCriterion("AdvanceIn is not null");
            return (Criteria) this;
        }

        public Criteria andAdvanceInEqualTo(BigDecimal value) {
            addCriterion("AdvanceIn =", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInNotEqualTo(BigDecimal value) {
            addCriterion("AdvanceIn <>", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInGreaterThan(BigDecimal value) {
            addCriterion("AdvanceIn >", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("AdvanceIn >=", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInLessThan(BigDecimal value) {
            addCriterion("AdvanceIn <", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInLessThanOrEqualTo(BigDecimal value) {
            addCriterion("AdvanceIn <=", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInLike(BigDecimal value) {
            addCriterion("AdvanceIn like", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInNotLike(BigDecimal value) {
            addCriterion("AdvanceIn not like", value, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInIn(List<BigDecimal> values) {
            addCriterion("AdvanceIn in", values, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInNotIn(List<BigDecimal> values) {
            addCriterion("AdvanceIn not in", values, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AdvanceIn between", value1, value2, "advanceIn");
            return (Criteria) this;
        }

        public Criteria andAdvanceInNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AdvanceIn not between", value1, value2, "advanceIn");
            return (Criteria) this;
        }
        
				
        public Criteria andBeginNeedGetIsNull() {
            addCriterion("BeginNeedGet is null");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetIsNotNull() {
            addCriterion("BeginNeedGet is not null");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetEqualTo(BigDecimal value) {
            addCriterion("BeginNeedGet =", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetNotEqualTo(BigDecimal value) {
            addCriterion("BeginNeedGet <>", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetGreaterThan(BigDecimal value) {
            addCriterion("BeginNeedGet >", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("BeginNeedGet >=", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetLessThan(BigDecimal value) {
            addCriterion("BeginNeedGet <", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("BeginNeedGet <=", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetLike(BigDecimal value) {
            addCriterion("BeginNeedGet like", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetNotLike(BigDecimal value) {
            addCriterion("BeginNeedGet not like", value, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetIn(List<BigDecimal> values) {
            addCriterion("BeginNeedGet in", values, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetNotIn(List<BigDecimal> values) {
            addCriterion("BeginNeedGet not in", values, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BeginNeedGet between", value1, value2, "beginNeedGet");
            return (Criteria) this;
        }

        public Criteria andBeginNeedGetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BeginNeedGet not between", value1, value2, "beginNeedGet");
            return (Criteria) this;
        }
        
				
        public Criteria andBeginNeedPayIsNull() {
            addCriterion("BeginNeedPay is null");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayIsNotNull() {
            addCriterion("BeginNeedPay is not null");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayEqualTo(BigDecimal value) {
            addCriterion("BeginNeedPay =", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayNotEqualTo(BigDecimal value) {
            addCriterion("BeginNeedPay <>", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayGreaterThan(BigDecimal value) {
            addCriterion("BeginNeedPay >", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("BeginNeedPay >=", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayLessThan(BigDecimal value) {
            addCriterion("BeginNeedPay <", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("BeginNeedPay <=", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayLike(BigDecimal value) {
            addCriterion("BeginNeedPay like", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayNotLike(BigDecimal value) {
            addCriterion("BeginNeedPay not like", value, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayIn(List<BigDecimal> values) {
            addCriterion("BeginNeedPay in", values, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayNotIn(List<BigDecimal> values) {
            addCriterion("BeginNeedPay not in", values, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BeginNeedPay between", value1, value2, "beginNeedPay");
            return (Criteria) this;
        }

        public Criteria andBeginNeedPayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BeginNeedPay not between", value1, value2, "beginNeedPay");
            return (Criteria) this;
        }
        
				
        public Criteria andAllNeedGetIsNull() {
            addCriterion("AllNeedGet is null");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetIsNotNull() {
            addCriterion("AllNeedGet is not null");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetEqualTo(BigDecimal value) {
            addCriterion("AllNeedGet =", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetNotEqualTo(BigDecimal value) {
            addCriterion("AllNeedGet <>", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetGreaterThan(BigDecimal value) {
            addCriterion("AllNeedGet >", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("AllNeedGet >=", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetLessThan(BigDecimal value) {
            addCriterion("AllNeedGet <", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("AllNeedGet <=", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetLike(BigDecimal value) {
            addCriterion("AllNeedGet like", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetNotLike(BigDecimal value) {
            addCriterion("AllNeedGet not like", value, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetIn(List<BigDecimal> values) {
            addCriterion("AllNeedGet in", values, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetNotIn(List<BigDecimal> values) {
            addCriterion("AllNeedGet not in", values, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AllNeedGet between", value1, value2, "allNeedGet");
            return (Criteria) this;
        }

        public Criteria andAllNeedGetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AllNeedGet not between", value1, value2, "allNeedGet");
            return (Criteria) this;
        }
        
				
        public Criteria andAllNeedPayIsNull() {
            addCriterion("AllNeedPay is null");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayIsNotNull() {
            addCriterion("AllNeedPay is not null");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayEqualTo(BigDecimal value) {
            addCriterion("AllNeedPay =", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayNotEqualTo(BigDecimal value) {
            addCriterion("AllNeedPay <>", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayGreaterThan(BigDecimal value) {
            addCriterion("AllNeedPay >", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("AllNeedPay >=", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayLessThan(BigDecimal value) {
            addCriterion("AllNeedPay <", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("AllNeedPay <=", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayLike(BigDecimal value) {
            addCriterion("AllNeedPay like", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayNotLike(BigDecimal value) {
            addCriterion("AllNeedPay not like", value, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayIn(List<BigDecimal> values) {
            addCriterion("AllNeedPay in", values, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayNotIn(List<BigDecimal> values) {
            addCriterion("AllNeedPay not in", values, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AllNeedPay between", value1, value2, "allNeedPay");
            return (Criteria) this;
        }

        public Criteria andAllNeedPayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AllNeedPay not between", value1, value2, "allNeedPay");
            return (Criteria) this;
        }
        
				
        public Criteria andFaxIsNull() {
            addCriterion("fax is null");
            return (Criteria) this;
        }

        public Criteria andFaxIsNotNull() {
            addCriterion("fax is not null");
            return (Criteria) this;
        }

        public Criteria andFaxEqualTo(String value) {
            addCriterion("fax =", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotEqualTo(String value) {
            addCriterion("fax <>", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxGreaterThan(String value) {
            addCriterion("fax >", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxGreaterThanOrEqualTo(String value) {
            addCriterion("fax >=", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLessThan(String value) {
            addCriterion("fax <", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLessThanOrEqualTo(String value) {
            addCriterion("fax <=", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLike(String value) {
            addCriterion("fax like", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotLike(String value) {
            addCriterion("fax not like", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxIn(List<String> values) {
            addCriterion("fax in", values, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotIn(List<String> values) {
            addCriterion("fax not in", values, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxBetween(String value1, String value2) {
            addCriterion("fax between", value1, value2, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotBetween(String value1, String value2) {
            addCriterion("fax not between", value1, value2, "fax");
            return (Criteria) this;
        }
        
				
        public Criteria andTelephoneIsNull() {
            addCriterion("telephone is null");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNotNull() {
            addCriterion("telephone is not null");
            return (Criteria) this;
        }

        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("telephone =", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("telephone <>", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("telephone >", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("telephone >=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThan(String value) {
            addCriterion("telephone <", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("telephone <=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLike(String value) {
            addCriterion("telephone like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotLike(String value) {
            addCriterion("telephone not like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("telephone in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("telephone not in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("telephone between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("telephone not between", value1, value2, "telephone");
            return (Criteria) this;
        }
        
				
        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }
        
				
        public Criteria andTaxNumIsNull() {
            addCriterion("taxNum is null");
            return (Criteria) this;
        }

        public Criteria andTaxNumIsNotNull() {
            addCriterion("taxNum is not null");
            return (Criteria) this;
        }

        public Criteria andTaxNumEqualTo(String value) {
            addCriterion("taxNum =", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumNotEqualTo(String value) {
            addCriterion("taxNum <>", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumGreaterThan(String value) {
            addCriterion("taxNum >", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumGreaterThanOrEqualTo(String value) {
            addCriterion("taxNum >=", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumLessThan(String value) {
            addCriterion("taxNum <", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumLessThanOrEqualTo(String value) {
            addCriterion("taxNum <=", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumLike(String value) {
            addCriterion("taxNum like", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumNotLike(String value) {
            addCriterion("taxNum not like", value, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumIn(List<String> values) {
            addCriterion("taxNum in", values, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumNotIn(List<String> values) {
            addCriterion("taxNum not in", values, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumBetween(String value1, String value2) {
            addCriterion("taxNum between", value1, value2, "taxNum");
            return (Criteria) this;
        }

        public Criteria andTaxNumNotBetween(String value1, String value2) {
            addCriterion("taxNum not between", value1, value2, "taxNum");
            return (Criteria) this;
        }
        
				
        public Criteria andBankNameIsNull() {
            addCriterion("bankName is null");
            return (Criteria) this;
        }

        public Criteria andBankNameIsNotNull() {
            addCriterion("bankName is not null");
            return (Criteria) this;
        }

        public Criteria andBankNameEqualTo(String value) {
            addCriterion("bankName =", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameNotEqualTo(String value) {
            addCriterion("bankName <>", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameGreaterThan(String value) {
            addCriterion("bankName >", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameGreaterThanOrEqualTo(String value) {
            addCriterion("bankName >=", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameLessThan(String value) {
            addCriterion("bankName <", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameLessThanOrEqualTo(String value) {
            addCriterion("bankName <=", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameLike(String value) {
            addCriterion("bankName like", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameNotLike(String value) {
            addCriterion("bankName not like", value, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameIn(List<String> values) {
            addCriterion("bankName in", values, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameNotIn(List<String> values) {
            addCriterion("bankName not in", values, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameBetween(String value1, String value2) {
            addCriterion("bankName between", value1, value2, "bankName");
            return (Criteria) this;
        }

        public Criteria andBankNameNotBetween(String value1, String value2) {
            addCriterion("bankName not between", value1, value2, "bankName");
            return (Criteria) this;
        }
        
				
        public Criteria andAccountNumberIsNull() {
            addCriterion("accountNumber is null");
            return (Criteria) this;
        }

        public Criteria andAccountNumberIsNotNull() {
            addCriterion("accountNumber is not null");
            return (Criteria) this;
        }

        public Criteria andAccountNumberEqualTo(String value) {
            addCriterion("accountNumber =", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberNotEqualTo(String value) {
            addCriterion("accountNumber <>", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberGreaterThan(String value) {
            addCriterion("accountNumber >", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberGreaterThanOrEqualTo(String value) {
            addCriterion("accountNumber >=", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberLessThan(String value) {
            addCriterion("accountNumber <", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberLessThanOrEqualTo(String value) {
            addCriterion("accountNumber <=", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberLike(String value) {
            addCriterion("accountNumber like", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberNotLike(String value) {
            addCriterion("accountNumber not like", value, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberIn(List<String> values) {
            addCriterion("accountNumber in", values, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberNotIn(List<String> values) {
            addCriterion("accountNumber not in", values, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberBetween(String value1, String value2) {
            addCriterion("accountNumber between", value1, value2, "accountNumber");
            return (Criteria) this;
        }

        public Criteria andAccountNumberNotBetween(String value1, String value2) {
            addCriterion("accountNumber not between", value1, value2, "accountNumber");
            return (Criteria) this;
        }
        
				
        public Criteria andTaxRateIsNull() {
            addCriterion("taxRate is null");
            return (Criteria) this;
        }

        public Criteria andTaxRateIsNotNull() {
            addCriterion("taxRate is not null");
            return (Criteria) this;
        }

        public Criteria andTaxRateEqualTo(BigDecimal value) {
            addCriterion("taxRate =", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateNotEqualTo(BigDecimal value) {
            addCriterion("taxRate <>", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateGreaterThan(BigDecimal value) {
            addCriterion("taxRate >", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("taxRate >=", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateLessThan(BigDecimal value) {
            addCriterion("taxRate <", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("taxRate <=", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateLike(BigDecimal value) {
            addCriterion("taxRate like", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateNotLike(BigDecimal value) {
            addCriterion("taxRate not like", value, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateIn(List<BigDecimal> values) {
            addCriterion("taxRate in", values, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateNotIn(List<BigDecimal> values) {
            addCriterion("taxRate not in", values, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxRate between", value1, value2, "taxRate");
            return (Criteria) this;
        }

        public Criteria andTaxRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxRate not between", value1, value2, "taxRate");
            return (Criteria) this;
        }
        
				
        public Criteria andTenantIdIsNull() {
            addCriterion("tenant_id is null");
            return (Criteria) this;
        }

        public Criteria andTenantIdIsNotNull() {
            addCriterion("tenant_id is not null");
            return (Criteria) this;
        }

        public Criteria andTenantIdEqualTo(Long value) {
            addCriterion("tenant_id =", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotEqualTo(Long value) {
            addCriterion("tenant_id <>", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdGreaterThan(Long value) {
            addCriterion("tenant_id >", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdGreaterThanOrEqualTo(Long value) {
            addCriterion("tenant_id >=", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLessThan(Long value) {
            addCriterion("tenant_id <", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLessThanOrEqualTo(Long value) {
            addCriterion("tenant_id <=", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLike(Long value) {
            addCriterion("tenant_id like", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotLike(Long value) {
            addCriterion("tenant_id not like", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdIn(List<Long> values) {
            addCriterion("tenant_id in", values, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotIn(List<Long> values) {
            addCriterion("tenant_id not in", values, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdBetween(Long value1, Long value2) {
            addCriterion("tenant_id between", value1, value2, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotBetween(Long value1, Long value2) {
            addCriterion("tenant_id not between", value1, value2, "tenantId");
            return (Criteria) this;
        }
        
				
        public Criteria andDeleteFlagIsNull() {
            addCriterion("delete_Flag is null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIsNotNull() {
            addCriterion("delete_Flag is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagEqualTo(String value) {
            addCriterion("delete_Flag =", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotEqualTo(String value) {
            addCriterion("delete_Flag <>", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThan(String value) {
            addCriterion("delete_Flag >", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThanOrEqualTo(String value) {
            addCriterion("delete_Flag >=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThan(String value) {
            addCriterion("delete_Flag <", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThanOrEqualTo(String value) {
            addCriterion("delete_Flag <=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLike(String value) {
            addCriterion("delete_Flag like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotLike(String value) {
            addCriterion("delete_Flag not like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIn(List<String> values) {
            addCriterion("delete_Flag in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotIn(List<String> values) {
            addCriterion("delete_Flag not in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagBetween(String value1, String value2) {
            addCriterion("delete_Flag between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotBetween(String value1, String value2) {
            addCriterion("delete_Flag not between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(JshSupplier record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getSupplier()!=null&&StrUtil.isNotEmpty(record.getSupplier().toString())) {
    			 list.add("ifnull(supplier,'')");
    		}
			if(record.getContacts()!=null&&StrUtil.isNotEmpty(record.getContacts().toString())) {
    			 list.add("ifnull(contacts,'')");
    		}
			if(record.getPhonenum()!=null&&StrUtil.isNotEmpty(record.getPhonenum().toString())) {
    			 list.add("ifnull(phonenum,'')");
    		}
			if(record.getEmail()!=null&&StrUtil.isNotEmpty(record.getEmail().toString())) {
    			 list.add("ifnull(email,'')");
    		}
			if(record.getDescription()!=null&&StrUtil.isNotEmpty(record.getDescription().toString())) {
    			 list.add("ifnull(description,'')");
    		}
			if(record.getIsystem()!=null&&StrUtil.isNotEmpty(record.getIsystem().toString())) {
    			 list.add("ifnull(isystem,'')");
    		}
			if(record.getType()!=null&&StrUtil.isNotEmpty(record.getType().toString())) {
    			 list.add("ifnull(type,'')");
    		}
			if(record.getEnabled()!=null&&StrUtil.isNotEmpty(record.getEnabled().toString())) {
    			 list.add("ifnull(enabled,'')");
    		}
			if(record.getAdvanceIn()!=null&&StrUtil.isNotEmpty(record.getAdvanceIn().toString())) {
    			 list.add("ifnull(AdvanceIn,'')");
    		}
			if(record.getBeginNeedGet()!=null&&StrUtil.isNotEmpty(record.getBeginNeedGet().toString())) {
    			 list.add("ifnull(BeginNeedGet,'')");
    		}
			if(record.getBeginNeedPay()!=null&&StrUtil.isNotEmpty(record.getBeginNeedPay().toString())) {
    			 list.add("ifnull(BeginNeedPay,'')");
    		}
			if(record.getAllNeedGet()!=null&&StrUtil.isNotEmpty(record.getAllNeedGet().toString())) {
    			 list.add("ifnull(AllNeedGet,'')");
    		}
			if(record.getAllNeedPay()!=null&&StrUtil.isNotEmpty(record.getAllNeedPay().toString())) {
    			 list.add("ifnull(AllNeedPay,'')");
    		}
			if(record.getFax()!=null&&StrUtil.isNotEmpty(record.getFax().toString())) {
    			 list.add("ifnull(fax,'')");
    		}
			if(record.getTelephone()!=null&&StrUtil.isNotEmpty(record.getTelephone().toString())) {
    			 list.add("ifnull(telephone,'')");
    		}
			if(record.getAddress()!=null&&StrUtil.isNotEmpty(record.getAddress().toString())) {
    			 list.add("ifnull(address,'')");
    		}
			if(record.getTaxNum()!=null&&StrUtil.isNotEmpty(record.getTaxNum().toString())) {
    			 list.add("ifnull(taxNum,'')");
    		}
			if(record.getBankName()!=null&&StrUtil.isNotEmpty(record.getBankName().toString())) {
    			 list.add("ifnull(bankName,'')");
    		}
			if(record.getAccountNumber()!=null&&StrUtil.isNotEmpty(record.getAccountNumber().toString())) {
    			 list.add("ifnull(accountNumber,'')");
    		}
			if(record.getTaxRate()!=null&&StrUtil.isNotEmpty(record.getTaxRate().toString())) {
    			 list.add("ifnull(taxRate,'')");
    		}
			if(record.getTenantId()!=null&&StrUtil.isNotEmpty(record.getTenantId().toString())) {
    			 list.add("ifnull(tenant_id,'')");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			 list.add("ifnull(delete_Flag,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getSupplier()!=null&&StrUtil.isNotEmpty(record.getSupplier().toString())) {
    			list2.add("'%"+record.getSupplier()+"%'");
    		}
			if(record.getContacts()!=null&&StrUtil.isNotEmpty(record.getContacts().toString())) {
    			list2.add("'%"+record.getContacts()+"%'");
    		}
			if(record.getPhonenum()!=null&&StrUtil.isNotEmpty(record.getPhonenum().toString())) {
    			list2.add("'%"+record.getPhonenum()+"%'");
    		}
			if(record.getEmail()!=null&&StrUtil.isNotEmpty(record.getEmail().toString())) {
    			list2.add("'%"+record.getEmail()+"%'");
    		}
			if(record.getDescription()!=null&&StrUtil.isNotEmpty(record.getDescription().toString())) {
    			list2.add("'%"+record.getDescription()+"%'");
    		}
			if(record.getIsystem()!=null&&StrUtil.isNotEmpty(record.getIsystem().toString())) {
    			list2.add("'%"+record.getIsystem()+"%'");
    		}
			if(record.getType()!=null&&StrUtil.isNotEmpty(record.getType().toString())) {
    			list2.add("'%"+record.getType()+"%'");
    		}
			if(record.getEnabled()!=null&&StrUtil.isNotEmpty(record.getEnabled().toString())) {
    			list2.add("'%"+record.getEnabled()+"%'");
    		}
			if(record.getAdvanceIn()!=null&&StrUtil.isNotEmpty(record.getAdvanceIn().toString())) {
    			list2.add("'%"+record.getAdvanceIn()+"%'");
    		}
			if(record.getBeginNeedGet()!=null&&StrUtil.isNotEmpty(record.getBeginNeedGet().toString())) {
    			list2.add("'%"+record.getBeginNeedGet()+"%'");
    		}
			if(record.getBeginNeedPay()!=null&&StrUtil.isNotEmpty(record.getBeginNeedPay().toString())) {
    			list2.add("'%"+record.getBeginNeedPay()+"%'");
    		}
			if(record.getAllNeedGet()!=null&&StrUtil.isNotEmpty(record.getAllNeedGet().toString())) {
    			list2.add("'%"+record.getAllNeedGet()+"%'");
    		}
			if(record.getAllNeedPay()!=null&&StrUtil.isNotEmpty(record.getAllNeedPay().toString())) {
    			list2.add("'%"+record.getAllNeedPay()+"%'");
    		}
			if(record.getFax()!=null&&StrUtil.isNotEmpty(record.getFax().toString())) {
    			list2.add("'%"+record.getFax()+"%'");
    		}
			if(record.getTelephone()!=null&&StrUtil.isNotEmpty(record.getTelephone().toString())) {
    			list2.add("'%"+record.getTelephone()+"%'");
    		}
			if(record.getAddress()!=null&&StrUtil.isNotEmpty(record.getAddress().toString())) {
    			list2.add("'%"+record.getAddress()+"%'");
    		}
			if(record.getTaxNum()!=null&&StrUtil.isNotEmpty(record.getTaxNum().toString())) {
    			list2.add("'%"+record.getTaxNum()+"%'");
    		}
			if(record.getBankName()!=null&&StrUtil.isNotEmpty(record.getBankName().toString())) {
    			list2.add("'%"+record.getBankName()+"%'");
    		}
			if(record.getAccountNumber()!=null&&StrUtil.isNotEmpty(record.getAccountNumber().toString())) {
    			list2.add("'%"+record.getAccountNumber()+"%'");
    		}
			if(record.getTaxRate()!=null&&StrUtil.isNotEmpty(record.getTaxRate().toString())) {
    			list2.add("'%"+record.getTaxRate()+"%'");
    		}
			if(record.getTenantId()!=null&&StrUtil.isNotEmpty(record.getTenantId().toString())) {
    			list2.add("'%"+record.getTenantId()+"%'");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			list2.add("'%"+record.getDeleteFlag()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(supplier,'')");
    		list.add("ifnull(contacts,'')");
    		list.add("ifnull(phonenum,'')");
    		list.add("ifnull(email,'')");
    		list.add("ifnull(description,'')");
    		list.add("ifnull(isystem,'')");
    		list.add("ifnull(type,'')");
    		list.add("ifnull(enabled,'')");
    		list.add("ifnull(AdvanceIn,'')");
    		list.add("ifnull(BeginNeedGet,'')");
    		list.add("ifnull(BeginNeedPay,'')");
    		list.add("ifnull(AllNeedGet,'')");
    		list.add("ifnull(AllNeedPay,'')");
    		list.add("ifnull(fax,'')");
    		list.add("ifnull(telephone,'')");
    		list.add("ifnull(address,'')");
    		list.add("ifnull(taxNum,'')");
    		list.add("ifnull(bankName,'')");
    		list.add("ifnull(accountNumber,'')");
    		list.add("ifnull(taxRate,'')");
    		list.add("ifnull(tenant_id,'')");
    		list.add("ifnull(delete_Flag,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}