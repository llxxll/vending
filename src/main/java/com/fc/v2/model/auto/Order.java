package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long orderId;
	
	@ApiModelProperty(value = "用户id")
	private Long userId;
	
	@ApiModelProperty(value = "收货人")
	private String consignee;
	
	@ApiModelProperty(value = "邮政费")
	private Integer postFee;
	
	@ApiModelProperty(value = "总费用")
	private Integer totalFee;
	
	@ApiModelProperty(value = "状态")
	private Byte status;
	
	@ApiModelProperty(value = "备注")
	private String remark;
	
	@ApiModelProperty(value = "场外交易编号")
	private String outTradeNo;
	
	@ApiModelProperty(value = "交易编号")
	private String transactionId;
	
	@ApiModelProperty(value = "createIp")
	private String createIp;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private Integer deleteFlag;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "payTime")
	private Date payTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "finishTime")
	private Date finishTime;
	
	@JsonProperty("orderId")
	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId =  orderId;
	}
	@JsonProperty("userId")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId =  userId;
	}
	@JsonProperty("consignee")
	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee =  consignee;
	}
	@JsonProperty("postFee")
	public Integer getPostFee() {
		return postFee;
	}

	public void setPostFee(Integer postFee) {
		this.postFee =  postFee;
	}
	@JsonProperty("totalFee")
	public Integer getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Integer totalFee) {
		this.totalFee =  totalFee;
	}
	@JsonProperty("status")
	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status =  status;
	}
	@JsonProperty("remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark =  remark;
	}
	@JsonProperty("outTradeNo")
	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo =  outTradeNo;
	}
	@JsonProperty("transactionId")
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId =  transactionId;
	}
	@JsonProperty("createIp")
	public String getCreateIp() {
		return createIp;
	}

	public void setCreateIp(String createIp) {
		this.createIp =  createIp;
	}
	@JsonProperty("deleteFlag")
	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("payTime")
	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime =  payTime;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("finishTime")
	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime =  finishTime;
	}

																												
	public Order(Long orderId,Long userId,String consignee,Integer postFee,Integer totalFee,Byte status,String remark,String outTradeNo,String transactionId,String createIp,Integer deleteFlag,Date payTime,Date createTime,Date finishTime) {
				
		this.orderId = orderId;
				
		this.userId = userId;
				
		this.consignee = consignee;
				
		this.postFee = postFee;
				
		this.totalFee = totalFee;
				
		this.status = status;
				
		this.remark = remark;
				
		this.outTradeNo = outTradeNo;
				
		this.transactionId = transactionId;
				
		this.createIp = createIp;
				
		this.deleteFlag = deleteFlag;
				
		this.payTime = payTime;
				
		this.createTime = createTime;
				
		this.finishTime = finishTime;
				
	}

	public Order() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}