package com.fc.v2.model.auto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class AppUser implements Serializable{

	
	/** 主键 **/
	@ApiModelProperty(value = "主键")
	private Long id;
		
	/** 用户账号 **/
	@ApiModelProperty(value = "用户账号")
	private String username;
		
	/** 用户密码 **/
	@ApiModelProperty(value = "用户密码")
	private String password;
		
	/** 昵称 **/
	@ApiModelProperty(value = "昵称")
	private String nickname;
		
	/** 岗位id **/
	@ApiModelProperty(value = "岗位id")
	private String posId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	@Override
	public String toString() {
		return "AppUser [id=" + id + ", username=" + username + ", password=" + password + ", nickname=" + nickname
				+ ", posId=" + posId + "]";
	}
	
	
	private TokenData tokendata;

	public TokenData getTokendata() {
		return tokendata;
	}

	public void setTokendata(TokenData tokendata) {
		this.tokendata = tokendata;
	}
	
	
}
