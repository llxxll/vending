package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class OrderBill implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long orderBillId;
	
	@ApiModelProperty(value = "用户id")
	private Long userId;
	
	@ApiModelProperty(value = "收货人")
	private String orderType;
	
	@ApiModelProperty(value = "买方")
	private Long payAmount;
	
	@ApiModelProperty(value = "场外交易编号")
	private String outTradeNo;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private Integer deleteFlag;
	
	@JsonProperty("orderBillId")
	public Long getOrderBillId() {
		return orderBillId;
	}

	public void setOrderBillId(Long orderBillId) {
		this.orderBillId =  orderBillId;
	}
	@JsonProperty("userId")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId =  userId;
	}
	@JsonProperty("orderType")
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType =  orderType;
	}
	@JsonProperty("payAmount")
	public Long getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Long payAmount) {
		this.payAmount =  payAmount;
	}
	@JsonProperty("outTradeNo")
	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo =  outTradeNo;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("deleteFlag")
	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}

																
	public OrderBill(Long orderBillId,Long userId,String orderType,Long payAmount,String outTradeNo,Date createTime,Date updateTime,Integer deleteFlag) {
				
		this.orderBillId = orderBillId;
				
		this.userId = userId;
				
		this.orderType = orderType;
				
		this.payAmount = payAmount;
				
		this.outTradeNo = outTradeNo;
				
		this.createTime = createTime;
				
		this.updateTime = updateTime;
				
		this.deleteFlag = deleteFlag;
				
	}

	public OrderBill() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}