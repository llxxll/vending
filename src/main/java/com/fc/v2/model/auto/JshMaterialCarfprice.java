package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class JshMaterialCarfprice implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "产品id")
	private Long materialId;
	
	@ApiModelProperty(value = "工艺顺序")
	private String orderCode;
	
	@ApiModelProperty(value = "工艺规格")
	private String specs;
	
	@ApiModelProperty(value = "工序名称")
	private Long carftId;
	
	@ApiModelProperty(value = "加工价格")
	private BigDecimal purchaseDecimal;
	
	@ApiModelProperty(value = "最低售价")
	private BigDecimal lowDecimal;
	
	@ApiModelProperty(value = "车间id")
	private Long tenantId;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private String deleteFlag;
	
	@ApiModelProperty(value = "工艺名称")
	private String ttff;
	
	@ApiModelProperty(value = "")
	private String ttxx;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private Long createUser;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("materialId")
	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId =  materialId;
	}
	@JsonProperty("orderCode")
	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode =  orderCode;
	}
	@JsonProperty("specs")
	public String getSpecs() {
		return specs;
	}

	public void setSpecs(String specs) {
		this.specs =  specs;
	}
	@JsonProperty("carftId")
	public Long getCarftId() {
		return carftId;
	}

	public void setCarftId(Long carftId) {
		this.carftId =  carftId;
	}
	@JsonProperty("purchaseDecimal")
	public BigDecimal getPurchaseDecimal() {
		return purchaseDecimal;
	}

	public void setPurchaseDecimal(BigDecimal purchaseDecimal) {
		this.purchaseDecimal =  purchaseDecimal;
	}
	@JsonProperty("lowDecimal")
	public BigDecimal getLowDecimal() {
		return lowDecimal;
	}

	public void setLowDecimal(BigDecimal lowDecimal) {
		this.lowDecimal =  lowDecimal;
	}
	@JsonProperty("tenantId")
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId =  tenantId;
	}
	@JsonProperty("deleteFlag")
	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("ttff")
	public String getTtff() {
		return ttff;
	}

	public void setTtff(String ttff) {
		this.ttff =  ttff;
	}
	@JsonProperty("ttxx")
	public String getTtxx() {
		return ttxx;
	}

	public void setTtxx(String ttxx) {
		this.ttxx =  ttxx;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("createUser")
	public Long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Long createUser) {
		this.createUser =  createUser;
	}

																										
	public JshMaterialCarfprice(Long id,Long materialId,String orderCode,String specs,Long carftId,BigDecimal purchaseDecimal,BigDecimal lowDecimal,Long tenantId,String deleteFlag,String ttff,String ttxx,Date createTime,Long createUser) {
				
		this.id = id;
				
		this.materialId = materialId;
				
		this.orderCode = orderCode;
				
		this.specs = specs;
				
		this.carftId = carftId;
				
		this.purchaseDecimal = purchaseDecimal;
				
		this.lowDecimal = lowDecimal;
				
		this.tenantId = tenantId;
				
		this.deleteFlag = deleteFlag;
				
		this.ttff = ttff;
				
		this.ttxx = ttxx;
				
		this.createTime = createTime;
				
		this.createUser = createUser;
				
	}

	public JshMaterialCarfprice() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	//辅助字段
	private String tuname;
	private String tuno;
	private String createname;
	private String ttffxxspesc;
	private String deptName;
	

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getTtffxxspesc() {
		return ttffxxspesc;
	}

	public void setTtffxxspesc(String ttffxxspesc) {
		this.ttffxxspesc = ttffxxspesc;
	}

	public String getTuno() {
		return tuno;
	}

	public void setTuno(String tuno) {
		this.tuno = tuno;
	}

	public String getTuname() {
		return tuname;
	}

	public void setTuname(String tuname) {
		this.tuname = tuname;
	}

	public String getCreatename() {
		return createname;
	}

	public void setCreatename(String createname) {
		this.createname = createname;
	}
	
	

}