package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class JshDepot implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "仓库名称")
	private String name;
	
	@ApiModelProperty(value = "仓库地址")
	private String address;
	
	@ApiModelProperty(value = "仓储费")
	private BigDecimal warehousing;
	
	@ApiModelProperty(value = "搬运费")
	private BigDecimal truckage;
	
	@ApiModelProperty(value = "类型")
	private Integer type;
	
	@ApiModelProperty(value = "排序")
	private String sort;
	
	@ApiModelProperty(value = "描述")
	private String remark;
	
	@ApiModelProperty(value = "负责人")
	private Long principal;
	
	@ApiModelProperty(value = "租户id")
	private Long tenantId;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private String deleteFlag;
	
	@ApiModelProperty(value = "是否默认")
	private Byte isDefault;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name =  name;
	}
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address =  address;
	}
	@JsonProperty("warehousing")
	public BigDecimal getWarehousing() {
		return warehousing;
	}

	public void setWarehousing(BigDecimal warehousing) {
		this.warehousing =  warehousing;
	}
	@JsonProperty("truckage")
	public BigDecimal getTruckage() {
		return truckage;
	}

	public void setTruckage(BigDecimal truckage) {
		this.truckage =  truckage;
	}
	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type =  type;
	}
	@JsonProperty("sort")
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort =  sort;
	}
	@JsonProperty("remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark =  remark;
	}
	@JsonProperty("principal")
	public Long getPrincipal() {
		return principal;
	}

	public void setPrincipal(Long principal) {
		this.principal =  principal;
	}
	@JsonProperty("tenantId")
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId =  tenantId;
	}
	@JsonProperty("deleteFlag")
	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("isDefault")
	public Byte getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Byte isDefault) {
		this.isDefault =  isDefault;
	}

																								
	public JshDepot(Long id,String name,String address,BigDecimal warehousing,BigDecimal truckage,Integer type,String sort,String remark,Long principal,Long tenantId,String deleteFlag,Byte isDefault) {
				
		this.id = id;
				
		this.name = name;
				
		this.address = address;
				
		this.warehousing = warehousing;
				
		this.truckage = truckage;
				
		this.type = type;
				
		this.sort = sort;
				
		this.remark = remark;
				
		this.principal = principal;
				
		this.tenantId = tenantId;
				
		this.deleteFlag = deleteFlag;
				
		this.isDefault = isDefault;
				
	}

	public JshDepot() {
	    super();
	}

	

}