package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;

import java.math.BigDecimal;
import java.util.Date;

public class Commodity implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "供应商id")
	private String supplierId;
	
	@ApiModelProperty(value = "供应商名称")
	private String supplierName;
	
	@ApiModelProperty(value = "商品类型")
	private String commodityType;
	
	@ApiModelProperty(value = "商品分类")
	private String classification;
	
	@ApiModelProperty(value = "商品名称")
	private String commodityName;
	
	@ApiModelProperty(value = "成本价")
	private BigDecimal costPrice;
	
	@ApiModelProperty(value = "零售价")
	private BigDecimal retailPrice;
	
	@ApiModelProperty(value = "条形码")
	private String barCode;
	
	@ApiModelProperty(value = "保质期")
	private String qualityGuarantee;
	
	@ApiModelProperty(value = "温度上")
	private String temperatureUp;
	
	@ApiModelProperty(value = "温度下")
	private String temperatureDo;
	
	@ApiModelProperty(value = "配料表")
	private String ingredients;
	
	@ApiModelProperty(value = "备注")
	private String notes;
	
	@ApiModelProperty(value = "描述")
	private String companyDesc;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	
	@ApiModelProperty(value = "1：默认正常数据；-1表示数据被删除")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	
	@ApiModelProperty(value = "")
	private String str1;
	
	@ApiModelProperty(value = "")
	private String str2;
	
	@ApiModelProperty(value = "")
	private String str3;
	
	@ApiModelProperty(value = "")
	private Long num1;
	
	@ApiModelProperty(value = "")
	private Integer num2;
	
	@ApiModelProperty(value = "")
	private Integer num3;
	
	@ApiModelProperty(value = "省份")
	private String provinceCode;
	
	@ApiModelProperty(value = "城市")
	private String cityCode;
	
	@ApiModelProperty(value = "区")
	private String areaCode;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("supplierId")
	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId =  supplierId;
	}
	@JsonProperty("supplierName")
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName =  supplierName;
	}
	@JsonProperty("commodityType")
	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType =  commodityType;
	}
	@JsonProperty("classification")
	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification =  classification;
	}
	@JsonProperty("commodityName")
	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName =  commodityName;
	}
	@JsonProperty("costPrice")
	public BigDecimal getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice =  costPrice;
	}
	@JsonProperty("retailPrice")
	public BigDecimal getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice =  retailPrice;
	}
	@JsonProperty("barCode")
	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode =  barCode;
	}
	@JsonProperty("qualityGuarantee")
	public String getQualityGuarantee() {
		return qualityGuarantee;
	}

	public void setQualityGuarantee(String qualityGuarantee) {
		this.qualityGuarantee =  qualityGuarantee;
	}
	@JsonProperty("temperatureUp")
	public String getTemperatureUp() {
		return temperatureUp;
	}

	public void setTemperatureUp(String temperatureUp) {
		this.temperatureUp =  temperatureUp;
	}
	@JsonProperty("temperatureDo")
	public String getTemperatureDo() {
		return temperatureDo;
	}

	public void setTemperatureDo(String temperatureDo) {
		this.temperatureDo =  temperatureDo;
	}
	@JsonProperty("ingredients")
	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients =  ingredients;
	}
	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes =  notes;
	}
	@JsonProperty("companyDesc")
	public String getCompanyDesc() {
		return companyDesc;
	}

	public void setCompanyDesc(String companyDesc) {
		this.companyDesc =  companyDesc;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	@JsonProperty("str1")
	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 =  str1;
	}
	@JsonProperty("str2")
	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 =  str2;
	}
	@JsonProperty("str3")
	public String getStr3() {
		return str3;
	}

	public void setStr3(String str3) {
		this.str3 =  str3;
	}
	@JsonProperty("num1")
	public Long getNum1() {
		return num1;
	}

	public void setNum1(Long num1) {
		this.num1 =  num1;
	}
	@JsonProperty("num2")
	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 =  num2;
	}
	@JsonProperty("num3")
	public Integer getNum3() {
		return num3;
	}

	public void setNum3(Integer num3) {
		this.num3 =  num3;
	}
	@JsonProperty("provinceCode")
	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode =  provinceCode;
	}
	@JsonProperty("cityCode")
	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode =  cityCode;
	}
	@JsonProperty("areaCode")
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode =  areaCode;
	}

																																																														
	public Commodity(Long id,String supplierId,String supplierName,String commodityType,String classification,String commodityName,BigDecimal costPrice,BigDecimal retailPrice,String barCode,String qualityGuarantee,String temperatureUp,String temperatureDo,String ingredients,String notes,String companyDesc,Date createdTime,Long createdUserSid,Date modifiedTime,Long modifiedUserSid,Integer x,Date xTime,Long xUserSid,String str1,String str2,String str3,Long num1,Integer num2,Integer num3,String provinceCode,String cityCode,String areaCode) {
				
		this.id = id;
				
		this.supplierId = supplierId;
				
		this.supplierName = supplierName;
				
		this.commodityType = commodityType;
				
		this.classification = classification;
				
		this.commodityName = commodityName;
				
		this.costPrice = costPrice;
				
		this.retailPrice = retailPrice;
				
		this.barCode = barCode;
				
		this.qualityGuarantee = qualityGuarantee;
				
		this.temperatureUp = temperatureUp;
				
		this.temperatureDo = temperatureDo;
				
		this.ingredients = ingredients;
				
		this.notes = notes;
				
		this.companyDesc = companyDesc;
				
		this.createdTime = createdTime;
				
		this.createdUserSid = createdUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.str1 = str1;
				
		this.str2 = str2;
				
		this.str3 = str3;
				
		this.num1 = num1;
				
		this.num2 = num2;
				
		this.num3 = num3;
				
		this.provinceCode = provinceCode;
				
		this.cityCode = cityCode;
				
		this.areaCode = areaCode;
				
	}

	public Commodity() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private String deviceCode;
	private Integer row;
	private Integer col;

	public String getDeviceCode() {
		return deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer col) {
		this.col = col;
	}
}