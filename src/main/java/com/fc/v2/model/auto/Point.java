package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class Point implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "点位名称")
	private String pointName;
	
	@ApiModelProperty(value = "点位类型")
	private String pointType;
	
	@ApiModelProperty(value = "线路id")
	private String lineId;
	
	@ApiModelProperty(value = "线路名称")
	private String lineName;
	
	@ApiModelProperty(value = "地址")
	private String address;
	
	@ApiModelProperty(value = "备注")
	private String pointNotes;
	
	@ApiModelProperty(value = "描述")
	private String pointDesc;
	
	@ApiModelProperty(value = "经度")
	private String longitude;
	
	@ApiModelProperty(value = "纬度")
	private String latitude;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	
	@ApiModelProperty(value = "1：默认正常数据；-1表示数据被删除")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	
	@ApiModelProperty(value = "")
	private String str1;
	
	@ApiModelProperty(value = "")
	private String str2;
	
	@ApiModelProperty(value = "")
	private String str3;
	
	@ApiModelProperty(value = "")
	private Long num1;
	
	@ApiModelProperty(value = "")
	private Integer num2;
	
	@ApiModelProperty(value = "")
	private Integer num3;
	
	@ApiModelProperty(value = "省份")
	private String provinceCode;
	
	@ApiModelProperty(value = "城市")
	private String cityCode;
	
	@ApiModelProperty(value = "区")
	private String areaCode;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("pointName")
	public String getPointName() {
		return pointName;
	}

	public void setPointName(String pointName) {
		this.pointName =  pointName;
	}
	@JsonProperty("pointType")
	public String getPointType() {
		return pointType;
	}

	public void setPointType(String pointType) {
		this.pointType =  pointType;
	}
	@JsonProperty("lineId")
	public String getLineId() {
		return lineId;
	}

	public void setLineId(String lineId) {
		this.lineId =  lineId;
	}
	@JsonProperty("lineName")
	public String getLineName() {
		return lineName;
	}

	public void setLineName(String lineName) {
		this.lineName =  lineName;
	}
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address =  address;
	}
	@JsonProperty("pointNotes")
	public String getPointNotes() {
		return pointNotes;
	}

	public void setPointNotes(String pointNotes) {
		this.pointNotes =  pointNotes;
	}
	@JsonProperty("pointDesc")
	public String getPointDesc() {
		return pointDesc;
	}

	public void setPointDesc(String pointDesc) {
		this.pointDesc =  pointDesc;
	}
	@JsonProperty("longitude")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude =  longitude;
	}
	@JsonProperty("latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude =  latitude;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	@JsonProperty("str1")
	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 =  str1;
	}
	@JsonProperty("str2")
	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 =  str2;
	}
	@JsonProperty("str3")
	public String getStr3() {
		return str3;
	}

	public void setStr3(String str3) {
		this.str3 =  str3;
	}
	@JsonProperty("num1")
	public Long getNum1() {
		return num1;
	}

	public void setNum1(Long num1) {
		this.num1 =  num1;
	}
	@JsonProperty("num2")
	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 =  num2;
	}
	@JsonProperty("num3")
	public Integer getNum3() {
		return num3;
	}

	public void setNum3(Integer num3) {
		this.num3 =  num3;
	}
	@JsonProperty("provinceCode")
	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode =  provinceCode;
	}
	@JsonProperty("cityCode")
	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode =  cityCode;
	}
	@JsonProperty("areaCode")
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode =  areaCode;
	}

																																																				
	public Point(Long id,String pointName,String pointType,String lineId,String lineName,String address,String pointNotes,String pointDesc,String longitude,String latitude,Date createdTime,Long createdUserSid,Date modifiedTime,Long modifiedUserSid,Integer x,Date xTime,Long xUserSid,String str1,String str2,String str3,Long num1,Integer num2,Integer num3,String provinceCode,String cityCode,String areaCode) {
				
		this.id = id;
				
		this.pointName = pointName;
				
		this.pointType = pointType;
				
		this.lineId = lineId;
				
		this.lineName = lineName;
				
		this.address = address;
				
		this.pointNotes = pointNotes;
				
		this.pointDesc = pointDesc;
				
		this.longitude = longitude;
				
		this.latitude = latitude;
				
		this.createdTime = createdTime;
				
		this.createdUserSid = createdUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.str1 = str1;
				
		this.str2 = str2;
				
		this.str3 = str3;
				
		this.num1 = num1;
				
		this.num2 = num2;
				
		this.num3 = num3;
				
		this.provinceCode = provinceCode;
				
		this.cityCode = cityCode;
				
		this.areaCode = areaCode;
				
	}

	public Point() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}