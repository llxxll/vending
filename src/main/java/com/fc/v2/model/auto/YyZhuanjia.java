package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class YyZhuanjia implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "专家名称")
	private String name;
	
	@ApiModelProperty(value = "专家介绍")
	private String introduce;
	
	@ApiModelProperty(value = "专家图片")
	private String url;
	
	@ApiModelProperty(value = "年龄")
	private String age;
	
	@ApiModelProperty(value = "性别")
	private String gender;
	
	@ApiModelProperty(value = "科室")
	private String keshi;
	
	@ApiModelProperty(value = "职称")
	private String zhicheng;
	
	@ApiModelProperty(value = "职务")
	private String zhiwu;
	
	@ApiModelProperty(value = "出诊时间")
	private String outTime;
	
	@ApiModelProperty(value = "擅长方向")
	private String shanchang;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private Integer deleteFlag;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	
	@ApiModelProperty(value = "预留字段1")
	private String field1;
	
	@ApiModelProperty(value = "预留字段2")
	private String field2;
	
	@ApiModelProperty(value = "预留字段3")
	private String field3;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name =  name;
	}
	@JsonProperty("introduce")
	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce =  introduce;
	}
	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url =  url;
	}
	@JsonProperty("age")
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age =  age;
	}
	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender =  gender;
	}
	@JsonProperty("keshi")
	public String getKeshi() {
		return keshi;
	}

	public void setKeshi(String keshi) {
		this.keshi =  keshi;
	}
	@JsonProperty("zhicheng")
	public String getZhicheng() {
		return zhicheng;
	}

	public void setZhicheng(String zhicheng) {
		this.zhicheng =  zhicheng;
	}
	@JsonProperty("zhiwu")
	public String getZhiwu() {
		return zhiwu;
	}

	public void setZhiwu(String zhiwu) {
		this.zhiwu =  zhiwu;
	}
	@JsonProperty("outTime")
	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime =  outTime;
	}
	@JsonProperty("shanchang")
	public String getShanchang() {
		return shanchang;
	}

	public void setShanchang(String shanchang) {
		this.shanchang =  shanchang;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("deleteFlag")
	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 =  field1;
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 =  field2;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}

																																								
	public YyZhuanjia(Long id,String name,String introduce,String url,String age,String gender,String keshi,String zhicheng,String zhiwu,String outTime,String shanchang,Date createTime,Integer deleteFlag,Date updateTime,Long creator,String field1,String field2,String field3,Long updater,Integer status) {
				
		this.id = id;
				
		this.name = name;
				
		this.introduce = introduce;
				
		this.url = url;
				
		this.age = age;
				
		this.gender = gender;
				
		this.keshi = keshi;
				
		this.zhicheng = zhicheng;
				
		this.zhiwu = zhiwu;
				
		this.outTime = outTime;
				
		this.shanchang = shanchang;
				
		this.createTime = createTime;
				
		this.deleteFlag = deleteFlag;
				
		this.updateTime = updateTime;
				
		this.creator = creator;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.updater = updater;
				
		this.status = status;
				
	}

	public YyZhuanjia() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private String  keshiname;

	public String getKeshiname() {
		return keshiname;
	}

	public void setKeshiname(String keshiname) {
		this.keshiname = keshiname;
	}

}