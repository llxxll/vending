package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class YyJieshao implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "标题")
	private String title;
	
	@ApiModelProperty(value = "内容")
	private String msg;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private Integer deleteFlag;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	
	@ApiModelProperty(value = "预留字段1")
	private String field1;
	
	@ApiModelProperty(value = "预留字段2")
	private String field2;
	
	@ApiModelProperty(value = "预留字段3")
	private String field3;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@ApiModelProperty(value = "发布范围:web")
	private String releaseWeb;
	
	@ApiModelProperty(value = "发布范围:小程序")
	private String releaseXcx;
	
	@ApiModelProperty(value = "预留字段4")
	private String field4;
	
	@ApiModelProperty(value = "预留字段5")
	private String field5;
	
	@ApiModelProperty(value = "预留字段6")
	private String field6;
	
	@ApiModelProperty(value = "预留字段7")
	private String field7;
	
	@ApiModelProperty(value = "预留字段8")
	private String field8;
	
	@ApiModelProperty(value = "预留字段9")
	private String field9;
	
	@ApiModelProperty(value = "预留字段10")
	private String field10;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title =  title;
	}
	@JsonProperty("msg")
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg =  msg;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("deleteFlag")
	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 =  field1;
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 =  field2;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("releaseWeb")
	public String getReleaseWeb() {
		return releaseWeb;
	}

	public void setReleaseWeb(String releaseWeb) {
		this.releaseWeb =  releaseWeb;
	}
	@JsonProperty("releaseXcx")
	public String getReleaseXcx() {
		return releaseXcx;
	}

	public void setReleaseXcx(String releaseXcx) {
		this.releaseXcx =  releaseXcx;
	}
	@JsonProperty("field4")
	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 =  field4;
	}
	@JsonProperty("field5")
	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 =  field5;
	}
	@JsonProperty("field6")
	public String getField6() {
		return field6;
	}

	public void setField6(String field6) {
		this.field6 =  field6;
	}
	@JsonProperty("field7")
	public String getField7() {
		return field7;
	}

	public void setField7(String field7) {
		this.field7 =  field7;
	}
	@JsonProperty("field8")
	public String getField8() {
		return field8;
	}

	public void setField8(String field8) {
		this.field8 =  field8;
	}
	@JsonProperty("field9")
	public String getField9() {
		return field9;
	}

	public void setField9(String field9) {
		this.field9 =  field9;
	}
	@JsonProperty("field10")
	public String getField10() {
		return field10;
	}

	public void setField10(String field10) {
		this.field10 =  field10;
	}

																																										
	public YyJieshao(Long id,String title,String msg,Date createTime,Integer deleteFlag,Date updateTime,Long creator,String field1,String field2,String field3,Long updater,Integer status,String releaseWeb,String releaseXcx,String field4,String field5,String field6,String field7,String field8,String field9,String field10) {
				
		this.id = id;
				
		this.title = title;
				
		this.msg = msg;
				
		this.createTime = createTime;
				
		this.deleteFlag = deleteFlag;
				
		this.updateTime = updateTime;
				
		this.creator = creator;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.updater = updater;
				
		this.status = status;
				
		this.releaseWeb = releaseWeb;
				
		this.releaseXcx = releaseXcx;
				
		this.field4 = field4;
				
		this.field5 = field5;
				
		this.field6 = field6;
				
		this.field7 = field7;
				
		this.field8 = field8;
				
		this.field9 = field9;
				
		this.field10 = field10;
				
	}

	public YyJieshao() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}