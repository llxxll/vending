package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 出库单明细 WmsWarehouseDeliveryDetailExample
 * @author fuce_自动生成
 * @date 2021-12-19 21:28:43
 */
public class WmsWarehouseDeliveryDetailExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WmsWarehouseDeliveryDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andSidIsNull() {
            addCriterion("sid is null");
            return (Criteria) this;
        }

        public Criteria andSidIsNotNull() {
            addCriterion("sid is not null");
            return (Criteria) this;
        }

        public Criteria andSidEqualTo(Long value) {
            addCriterion("sid =", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotEqualTo(Long value) {
            addCriterion("sid <>", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThan(Long value) {
            addCriterion("sid >", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThanOrEqualTo(Long value) {
            addCriterion("sid >=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThan(Long value) {
            addCriterion("sid <", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThanOrEqualTo(Long value) {
            addCriterion("sid <=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLike(Long value) {
            addCriterion("sid like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotLike(Long value) {
            addCriterion("sid not like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidIn(List<Long> values) {
            addCriterion("sid in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotIn(List<Long> values) {
            addCriterion("sid not in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidBetween(Long value1, Long value2) {
            addCriterion("sid between", value1, value2, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotBetween(Long value1, Long value2) {
            addCriterion("sid not between", value1, value2, "sid");
            return (Criteria) this;
        }
        
				
        public Criteria andDeliverySidIsNull() {
            addCriterion("delivery_sid is null");
            return (Criteria) this;
        }

        public Criteria andDeliverySidIsNotNull() {
            addCriterion("delivery_sid is not null");
            return (Criteria) this;
        }

        public Criteria andDeliverySidEqualTo(String value) {
            addCriterion("delivery_sid =", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidNotEqualTo(String value) {
            addCriterion("delivery_sid <>", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidGreaterThan(String value) {
            addCriterion("delivery_sid >", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidGreaterThanOrEqualTo(String value) {
            addCriterion("delivery_sid >=", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidLessThan(String value) {
            addCriterion("delivery_sid <", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidLessThanOrEqualTo(String value) {
            addCriterion("delivery_sid <=", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidLike(String value) {
            addCriterion("delivery_sid like", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidNotLike(String value) {
            addCriterion("delivery_sid not like", value, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidIn(List<String> values) {
            addCriterion("delivery_sid in", values, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidNotIn(List<String> values) {
            addCriterion("delivery_sid not in", values, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidBetween(String value1, String value2) {
            addCriterion("delivery_sid between", value1, value2, "deliverySid");
            return (Criteria) this;
        }

        public Criteria andDeliverySidNotBetween(String value1, String value2) {
            addCriterion("delivery_sid not between", value1, value2, "deliverySid");
            return (Criteria) this;
        }
        
				
        public Criteria andMatterNoIsNull() {
            addCriterion("matter_no is null");
            return (Criteria) this;
        }

        public Criteria andMatterNoIsNotNull() {
            addCriterion("matter_no is not null");
            return (Criteria) this;
        }

        public Criteria andMatterNoEqualTo(String value) {
            addCriterion("matter_no =", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoNotEqualTo(String value) {
            addCriterion("matter_no <>", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoGreaterThan(String value) {
            addCriterion("matter_no >", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoGreaterThanOrEqualTo(String value) {
            addCriterion("matter_no >=", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoLessThan(String value) {
            addCriterion("matter_no <", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoLessThanOrEqualTo(String value) {
            addCriterion("matter_no <=", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoLike(String value) {
            addCriterion("matter_no like", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoNotLike(String value) {
            addCriterion("matter_no not like", value, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoIn(List<String> values) {
            addCriterion("matter_no in", values, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoNotIn(List<String> values) {
            addCriterion("matter_no not in", values, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoBetween(String value1, String value2) {
            addCriterion("matter_no between", value1, value2, "matterNo");
            return (Criteria) this;
        }

        public Criteria andMatterNoNotBetween(String value1, String value2) {
            addCriterion("matter_no not between", value1, value2, "matterNo");
            return (Criteria) this;
        }
        
				
        public Criteria andMatterDescIsNull() {
            addCriterion("matter_desc is null");
            return (Criteria) this;
        }

        public Criteria andMatterDescIsNotNull() {
            addCriterion("matter_desc is not null");
            return (Criteria) this;
        }

        public Criteria andMatterDescEqualTo(String value) {
            addCriterion("matter_desc =", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescNotEqualTo(String value) {
            addCriterion("matter_desc <>", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescGreaterThan(String value) {
            addCriterion("matter_desc >", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescGreaterThanOrEqualTo(String value) {
            addCriterion("matter_desc >=", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescLessThan(String value) {
            addCriterion("matter_desc <", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescLessThanOrEqualTo(String value) {
            addCriterion("matter_desc <=", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescLike(String value) {
            addCriterion("matter_desc like", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescNotLike(String value) {
            addCriterion("matter_desc not like", value, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescIn(List<String> values) {
            addCriterion("matter_desc in", values, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescNotIn(List<String> values) {
            addCriterion("matter_desc not in", values, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescBetween(String value1, String value2) {
            addCriterion("matter_desc between", value1, value2, "matterDesc");
            return (Criteria) this;
        }

        public Criteria andMatterDescNotBetween(String value1, String value2) {
            addCriterion("matter_desc not between", value1, value2, "matterDesc");
            return (Criteria) this;
        }
        
				
        public Criteria andMatterNumberIsNull() {
            addCriterion("matter_number is null");
            return (Criteria) this;
        }

        public Criteria andMatterNumberIsNotNull() {
            addCriterion("matter_number is not null");
            return (Criteria) this;
        }

        public Criteria andMatterNumberEqualTo(Long value) {
            addCriterion("matter_number =", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberNotEqualTo(Long value) {
            addCriterion("matter_number <>", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberGreaterThan(Long value) {
            addCriterion("matter_number >", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberGreaterThanOrEqualTo(Long value) {
            addCriterion("matter_number >=", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberLessThan(Long value) {
            addCriterion("matter_number <", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberLessThanOrEqualTo(Long value) {
            addCriterion("matter_number <=", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberLike(Long value) {
            addCriterion("matter_number like", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberNotLike(Long value) {
            addCriterion("matter_number not like", value, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberIn(List<Long> values) {
            addCriterion("matter_number in", values, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberNotIn(List<Long> values) {
            addCriterion("matter_number not in", values, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberBetween(Long value1, Long value2) {
            addCriterion("matter_number between", value1, value2, "matterNumber");
            return (Criteria) this;
        }

        public Criteria andMatterNumberNotBetween(Long value1, Long value2) {
            addCriterion("matter_number not between", value1, value2, "matterNumber");
            return (Criteria) this;
        }
        
				
        public Criteria andUnitNameIsNull() {
            addCriterion("unit_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitNameIsNotNull() {
            addCriterion("unit_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitNameEqualTo(String value) {
            addCriterion("unit_name =", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotEqualTo(String value) {
            addCriterion("unit_name <>", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameGreaterThan(String value) {
            addCriterion("unit_name >", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_name >=", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameLessThan(String value) {
            addCriterion("unit_name <", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameLessThanOrEqualTo(String value) {
            addCriterion("unit_name <=", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameLike(String value) {
            addCriterion("unit_name like", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotLike(String value) {
            addCriterion("unit_name not like", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameIn(List<String> values) {
            addCriterion("unit_name in", values, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotIn(List<String> values) {
            addCriterion("unit_name not in", values, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameBetween(String value1, String value2) {
            addCriterion("unit_name between", value1, value2, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotBetween(String value1, String value2) {
            addCriterion("unit_name not between", value1, value2, "unitName");
            return (Criteria) this;
        }
        
				
        public Criteria andHouseNoIsNull() {
            addCriterion("house_no is null");
            return (Criteria) this;
        }

        public Criteria andHouseNoIsNotNull() {
            addCriterion("house_no is not null");
            return (Criteria) this;
        }

        public Criteria andHouseNoEqualTo(String value) {
            addCriterion("house_no =", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotEqualTo(String value) {
            addCriterion("house_no <>", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoGreaterThan(String value) {
            addCriterion("house_no >", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoGreaterThanOrEqualTo(String value) {
            addCriterion("house_no >=", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoLessThan(String value) {
            addCriterion("house_no <", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoLessThanOrEqualTo(String value) {
            addCriterion("house_no <=", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoLike(String value) {
            addCriterion("house_no like", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotLike(String value) {
            addCriterion("house_no not like", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoIn(List<String> values) {
            addCriterion("house_no in", values, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotIn(List<String> values) {
            addCriterion("house_no not in", values, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoBetween(String value1, String value2) {
            addCriterion("house_no between", value1, value2, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotBetween(String value1, String value2) {
            addCriterion("house_no not between", value1, value2, "houseNo");
            return (Criteria) this;
        }
        
				
        public Criteria andQualityNoIsNull() {
            addCriterion("quality_no is null");
            return (Criteria) this;
        }

        public Criteria andQualityNoIsNotNull() {
            addCriterion("quality_no is not null");
            return (Criteria) this;
        }

        public Criteria andQualityNoEqualTo(String value) {
            addCriterion("quality_no =", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoNotEqualTo(String value) {
            addCriterion("quality_no <>", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoGreaterThan(String value) {
            addCriterion("quality_no >", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoGreaterThanOrEqualTo(String value) {
            addCriterion("quality_no >=", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoLessThan(String value) {
            addCriterion("quality_no <", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoLessThanOrEqualTo(String value) {
            addCriterion("quality_no <=", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoLike(String value) {
            addCriterion("quality_no like", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoNotLike(String value) {
            addCriterion("quality_no not like", value, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoIn(List<String> values) {
            addCriterion("quality_no in", values, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoNotIn(List<String> values) {
            addCriterion("quality_no not in", values, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoBetween(String value1, String value2) {
            addCriterion("quality_no between", value1, value2, "qualityNo");
            return (Criteria) this;
        }

        public Criteria andQualityNoNotBetween(String value1, String value2) {
            addCriterion("quality_no not between", value1, value2, "qualityNo");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedUserSidIsNull() {
            addCriterion("created_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIsNotNull() {
            addCriterion("created_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidEqualTo(Long value) {
            addCriterion("created_user_sid =", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotEqualTo(Long value) {
            addCriterion("created_user_sid <>", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThan(Long value) {
            addCriterion("created_user_sid >", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("created_user_sid >=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThan(Long value) {
            addCriterion("created_user_sid <", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("created_user_sid <=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLike(Long value) {
            addCriterion("created_user_sid like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotLike(Long value) {
            addCriterion("created_user_sid not like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIn(List<Long> values) {
            addCriterion("created_user_sid in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotIn(List<Long> values) {
            addCriterion("created_user_sid not in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidBetween(Long value1, Long value2) {
            addCriterion("created_user_sid between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("created_user_sid not between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLike(Date value) {
            addCriterion("created_time like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotLike(Date value) {
            addCriterion("created_time not like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }
        
				
        public Criteria andXIsNull() {
            addCriterion("x is null");
            return (Criteria) this;
        }

        public Criteria andXIsNotNull() {
            addCriterion("x is not null");
            return (Criteria) this;
        }

        public Criteria andXEqualTo(Integer value) {
            addCriterion("x =", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotEqualTo(Integer value) {
            addCriterion("x <>", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThan(Integer value) {
            addCriterion("x >", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThanOrEqualTo(Integer value) {
            addCriterion("x >=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThan(Integer value) {
            addCriterion("x <", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThanOrEqualTo(Integer value) {
            addCriterion("x <=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLike(Integer value) {
            addCriterion("x like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotLike(Integer value) {
            addCriterion("x not like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXIn(List<Integer> values) {
            addCriterion("x in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXNotIn(List<Integer> values) {
            addCriterion("x not in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXBetween(Integer value1, Integer value2) {
            addCriterion("x between", value1, value2, "x");
            return (Criteria) this;
        }

        public Criteria andXNotBetween(Integer value1, Integer value2) {
            addCriterion("x not between", value1, value2, "x");
            return (Criteria) this;
        }
        
				
        public Criteria andXTimeIsNull() {
            addCriterion("x_time is null");
            return (Criteria) this;
        }

        public Criteria andXTimeIsNotNull() {
            addCriterion("x_time is not null");
            return (Criteria) this;
        }

        public Criteria andXTimeEqualTo(Date value) {
            addCriterion("x_time =", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotEqualTo(Date value) {
            addCriterion("x_time <>", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThan(Date value) {
            addCriterion("x_time >", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("x_time >=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThan(Date value) {
            addCriterion("x_time <", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThanOrEqualTo(Date value) {
            addCriterion("x_time <=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLike(Date value) {
            addCriterion("x_time like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotLike(Date value) {
            addCriterion("x_time not like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeIn(List<Date> values) {
            addCriterion("x_time in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotIn(List<Date> values) {
            addCriterion("x_time not in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeBetween(Date value1, Date value2) {
            addCriterion("x_time between", value1, value2, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotBetween(Date value1, Date value2) {
            addCriterion("x_time not between", value1, value2, "xTime");
            return (Criteria) this;
        }
        
				
        public Criteria andXUserSidIsNull() {
            addCriterion("x_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andXUserSidIsNotNull() {
            addCriterion("x_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andXUserSidEqualTo(Long value) {
            addCriterion("x_user_sid =", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotEqualTo(Long value) {
            addCriterion("x_user_sid <>", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThan(Long value) {
            addCriterion("x_user_sid >", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("x_user_sid >=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThan(Long value) {
            addCriterion("x_user_sid <", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThanOrEqualTo(Long value) {
            addCriterion("x_user_sid <=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLike(Long value) {
            addCriterion("x_user_sid like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotLike(Long value) {
            addCriterion("x_user_sid not like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidIn(List<Long> values) {
            addCriterion("x_user_sid in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotIn(List<Long> values) {
            addCriterion("x_user_sid not in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidBetween(Long value1, Long value2) {
            addCriterion("x_user_sid between", value1, value2, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotBetween(Long value1, Long value2) {
            addCriterion("x_user_sid not between", value1, value2, "xUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedTimeIsNull() {
            addCriterion("modified_time is null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIsNotNull() {
            addCriterion("modified_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeEqualTo(Date value) {
            addCriterion("modified_time =", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotEqualTo(Date value) {
            addCriterion("modified_time <>", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThan(Date value) {
            addCriterion("modified_time >", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modified_time >=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThan(Date value) {
            addCriterion("modified_time <", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThanOrEqualTo(Date value) {
            addCriterion("modified_time <=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLike(Date value) {
            addCriterion("modified_time like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotLike(Date value) {
            addCriterion("modified_time not like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIn(List<Date> values) {
            addCriterion("modified_time in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotIn(List<Date> values) {
            addCriterion("modified_time not in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeBetween(Date value1, Date value2) {
            addCriterion("modified_time between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotBetween(Date value1, Date value2) {
            addCriterion("modified_time not between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedUserSidIsNull() {
            addCriterion("modified_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIsNotNull() {
            addCriterion("modified_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidEqualTo(Long value) {
            addCriterion("modified_user_sid =", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotEqualTo(Long value) {
            addCriterion("modified_user_sid <>", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThan(Long value) {
            addCriterion("modified_user_sid >", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid >=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThan(Long value) {
            addCriterion("modified_user_sid <", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid <=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLike(Long value) {
            addCriterion("modified_user_sid like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotLike(Long value) {
            addCriterion("modified_user_sid not like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIn(List<Long> values) {
            addCriterion("modified_user_sid in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotIn(List<Long> values) {
            addCriterion("modified_user_sid not in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid not between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(WmsWarehouseDeliveryDetail record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			 list.add("ifnull(sid,'')");
    		}
			if(record.getDeliverySid()!=null&&StrUtil.isNotEmpty(record.getDeliverySid().toString())) {
    			 list.add("ifnull(delivery_sid,'')");
    		}
			if(record.getMatterNo()!=null&&StrUtil.isNotEmpty(record.getMatterNo().toString())) {
    			 list.add("ifnull(matter_no,'')");
    		}
			if(record.getMatterDesc()!=null&&StrUtil.isNotEmpty(record.getMatterDesc().toString())) {
    			 list.add("ifnull(matter_desc,'')");
    		}
			if(record.getMatterNumber()!=null&&StrUtil.isNotEmpty(record.getMatterNumber().toString())) {
    			 list.add("ifnull(matter_number,'')");
    		}
			if(record.getUnitName()!=null&&StrUtil.isNotEmpty(record.getUnitName().toString())) {
    			 list.add("ifnull(unit_name,'')");
    		}
			if(record.getHouseNo()!=null&&StrUtil.isNotEmpty(record.getHouseNo().toString())) {
    			 list.add("ifnull(house_no,'')");
    		}
			if(record.getQualityNo()!=null&&StrUtil.isNotEmpty(record.getQualityNo().toString())) {
    			 list.add("ifnull(quality_no,'')");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			 list.add("ifnull(created_user_sid,'')");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			 list.add("ifnull(created_time,'')");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			 list.add("ifnull(x,'')");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			 list.add("ifnull(x_time,'')");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			 list.add("ifnull(x_user_sid,'')");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			 list.add("ifnull(modified_time,'')");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			 list.add("ifnull(modified_user_sid,'')");
    		}
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			list2.add("'%"+record.getSid()+"%'");
    		}
			if(record.getDeliverySid()!=null&&StrUtil.isNotEmpty(record.getDeliverySid().toString())) {
    			list2.add("'%"+record.getDeliverySid()+"%'");
    		}
			if(record.getMatterNo()!=null&&StrUtil.isNotEmpty(record.getMatterNo().toString())) {
    			list2.add("'%"+record.getMatterNo()+"%'");
    		}
			if(record.getMatterDesc()!=null&&StrUtil.isNotEmpty(record.getMatterDesc().toString())) {
    			list2.add("'%"+record.getMatterDesc()+"%'");
    		}
			if(record.getMatterNumber()!=null&&StrUtil.isNotEmpty(record.getMatterNumber().toString())) {
    			list2.add("'%"+record.getMatterNumber()+"%'");
    		}
			if(record.getUnitName()!=null&&StrUtil.isNotEmpty(record.getUnitName().toString())) {
    			list2.add("'%"+record.getUnitName()+"%'");
    		}
			if(record.getHouseNo()!=null&&StrUtil.isNotEmpty(record.getHouseNo().toString())) {
    			list2.add("'%"+record.getHouseNo()+"%'");
    		}
			if(record.getQualityNo()!=null&&StrUtil.isNotEmpty(record.getQualityNo().toString())) {
    			list2.add("'%"+record.getQualityNo()+"%'");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			list2.add("'%"+record.getCreatedUserSid()+"%'");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			list2.add("'%"+record.getCreatedTime()+"%'");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			list2.add("'%"+record.getX()+"%'");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			list2.add("'%"+record.getXTime()+"%'");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			list2.add("'%"+record.getXUserSid()+"%'");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			list2.add("'%"+record.getModifiedTime()+"%'");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			list2.add("'%"+record.getModifiedUserSid()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(sid,'')");
    		list.add("ifnull(delivery_sid,'')");
    		list.add("ifnull(matter_no,'')");
    		list.add("ifnull(matter_desc,'')");
    		list.add("ifnull(matter_number,'')");
    		list.add("ifnull(unit_name,'')");
    		list.add("ifnull(house_no,'')");
    		list.add("ifnull(quality_no,'')");
    		list.add("ifnull(created_user_sid,'')");
    		list.add("ifnull(created_time,'')");
    		list.add("ifnull(x,'')");
    		list.add("ifnull(x_time,'')");
    		list.add("ifnull(x_user_sid,'')");
    		list.add("ifnull(modified_time,'')");
    		list.add("ifnull(modified_user_sid,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}