package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class OrderFromCategory implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	
	@ApiModelProperty(value = "SPU")
	private String spu;
	
	@ApiModelProperty(value = "规格")
	private String spuAssemble;
	
	@ApiModelProperty(value = "")
	private Long brandSid;
	
	@ApiModelProperty(value = "")
	private String categorySid;
	
	@ApiModelProperty(value = "名称")
	private String productTitle;
	
	@ApiModelProperty(value = "描述")
	private String productDesc;
	
	@ApiModelProperty(value = "销售单价")
	private BigDecimal price;
	
	@ApiModelProperty(value = "图片")
	private String barcode;
	
	@ApiModelProperty(value = "商品二维码")
	private String qrcode;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	
	@ApiModelProperty(value = "1：默认正常数据；-1表示数据被删除")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	
	@ApiModelProperty(value = "")
	private String str1;
	
	@ApiModelProperty(value = "")
	private String str2;
	
	@ApiModelProperty(value = "")
	private String str3;
	
	@ApiModelProperty(value = "")
	private Long num1;
	
	@ApiModelProperty(value = "")
	private Integer num2;
	
	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("spu")
	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu =  spu;
	}
	@JsonProperty("spuAssemble")
	public String getSpuAssemble() {
		return spuAssemble;
	}

	public void setSpuAssemble(String spuAssemble) {
		this.spuAssemble =  spuAssemble;
	}
	@JsonProperty("brandSid")
	public Long getBrandSid() {
		return brandSid;
	}

	public void setBrandSid(Long brandSid) {
		this.brandSid =  brandSid;
	}
	@JsonProperty("categorySid")
	public String getCategorySid() {
		return categorySid;
	}

	public void setCategorySid(String categorySid) {
		this.categorySid =  categorySid;
	}
	@JsonProperty("productTitle")
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle =  productTitle;
	}
	@JsonProperty("productDesc")
	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc =  productDesc;
	}
	@JsonProperty("price")
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price =  price;
	}
	@JsonProperty("barcode")
	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode =  barcode;
	}
	@JsonProperty("qrcode")
	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode =  qrcode;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	@JsonProperty("str1")
	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 =  str1;
	}
	@JsonProperty("str2")
	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 =  str2;
	}
	@JsonProperty("str3")
	public String getStr3() {
		return str3;
	}

	public void setStr3(String str3) {
		this.str3 =  str3;
	}
	@JsonProperty("num1")
	public Long getNum1() {
		return num1;
	}

	public void setNum1(Long num1) {
		this.num1 =  num1;
	}
	@JsonProperty("num2")
	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 =  num2;
	}

																																												
	public OrderFromCategory(Long sid,String spu,String spuAssemble,Long brandSid,String categorySid,String productTitle,String productDesc,BigDecimal price,String barcode,String qrcode,Date createdTime,Long createdUserSid,Date modifiedTime,Long modifiedUserSid,Integer x,Date xTime,Long xUserSid,String str1,String str2,String str3,Long num1,Integer num2) {
				
		this.sid = sid;
				
		this.spu = spu;
				
		this.spuAssemble = spuAssemble;
				
		this.brandSid = brandSid;
				
		this.categorySid = categorySid;
				
		this.productTitle = productTitle;
				
		this.productDesc = productDesc;
				
		this.price = price;
				
		this.barcode = barcode;
				
		this.qrcode = qrcode;
				
		this.createdTime = createdTime;
				
		this.createdUserSid = createdUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.str1 = str1;
				
		this.str2 = str2;
				
		this.str3 = str3;
				
		this.num1 = num1;
				
		this.num2 = num2;
				
	}

	public OrderFromCategory() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
	private String createName;

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}
}