package com.fc.v2.model.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 商品管理表 CommodityExample
 * @author lxl_自动生成
 * @date 2023-12-01 16:09:32
 */
public class CommodityExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CommodityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andSupplierIdIsNull() {
            addCriterion("supplier_id is null");
            return (Criteria) this;
        }

        public Criteria andSupplierIdIsNotNull() {
            addCriterion("supplier_id is not null");
            return (Criteria) this;
        }

        public Criteria andSupplierIdEqualTo(String value) {
            addCriterion("supplier_id =", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotEqualTo(String value) {
            addCriterion("supplier_id <>", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdGreaterThan(String value) {
            addCriterion("supplier_id >", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdGreaterThanOrEqualTo(String value) {
            addCriterion("supplier_id >=", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdLessThan(String value) {
            addCriterion("supplier_id <", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdLessThanOrEqualTo(String value) {
            addCriterion("supplier_id <=", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdLike(String value) {
            addCriterion("supplier_id like", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotLike(String value) {
            addCriterion("supplier_id not like", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdIn(List<String> values) {
            addCriterion("supplier_id in", values, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotIn(List<String> values) {
            addCriterion("supplier_id not in", values, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdBetween(String value1, String value2) {
            addCriterion("supplier_id between", value1, value2, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotBetween(String value1, String value2) {
            addCriterion("supplier_id not between", value1, value2, "supplierId");
            return (Criteria) this;
        }
        
				
        public Criteria andSupplierNameIsNull() {
            addCriterion("supplier_name is null");
            return (Criteria) this;
        }

        public Criteria andSupplierNameIsNotNull() {
            addCriterion("supplier_name is not null");
            return (Criteria) this;
        }

        public Criteria andSupplierNameEqualTo(String value) {
            addCriterion("supplier_name =", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameNotEqualTo(String value) {
            addCriterion("supplier_name <>", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameGreaterThan(String value) {
            addCriterion("supplier_name >", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameGreaterThanOrEqualTo(String value) {
            addCriterion("supplier_name >=", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameLessThan(String value) {
            addCriterion("supplier_name <", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameLessThanOrEqualTo(String value) {
            addCriterion("supplier_name <=", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameLike(String value) {
            addCriterion("supplier_name like", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameNotLike(String value) {
            addCriterion("supplier_name not like", value, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameIn(List<String> values) {
            addCriterion("supplier_name in", values, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameNotIn(List<String> values) {
            addCriterion("supplier_name not in", values, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameBetween(String value1, String value2) {
            addCriterion("supplier_name between", value1, value2, "supplierName");
            return (Criteria) this;
        }

        public Criteria andSupplierNameNotBetween(String value1, String value2) {
            addCriterion("supplier_name not between", value1, value2, "supplierName");
            return (Criteria) this;
        }
        
				
        public Criteria andCommodityTypeIsNull() {
            addCriterion("commodity_type is null");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeIsNotNull() {
            addCriterion("commodity_type is not null");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeEqualTo(String value) {
            addCriterion("commodity_type =", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeNotEqualTo(String value) {
            addCriterion("commodity_type <>", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeGreaterThan(String value) {
            addCriterion("commodity_type >", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeGreaterThanOrEqualTo(String value) {
            addCriterion("commodity_type >=", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeLessThan(String value) {
            addCriterion("commodity_type <", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeLessThanOrEqualTo(String value) {
            addCriterion("commodity_type <=", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeLike(String value) {
            addCriterion("commodity_type like", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeNotLike(String value) {
            addCriterion("commodity_type not like", value, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeIn(List<String> values) {
            addCriterion("commodity_type in", values, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeNotIn(List<String> values) {
            addCriterion("commodity_type not in", values, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeBetween(String value1, String value2) {
            addCriterion("commodity_type between", value1, value2, "commodityType");
            return (Criteria) this;
        }

        public Criteria andCommodityTypeNotBetween(String value1, String value2) {
            addCriterion("commodity_type not between", value1, value2, "commodityType");
            return (Criteria) this;
        }
        
				
        public Criteria andClassificationIsNull() {
            addCriterion("classification is null");
            return (Criteria) this;
        }

        public Criteria andClassificationIsNotNull() {
            addCriterion("classification is not null");
            return (Criteria) this;
        }

        public Criteria andClassificationEqualTo(String value) {
            addCriterion("classification =", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationNotEqualTo(String value) {
            addCriterion("classification <>", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationGreaterThan(String value) {
            addCriterion("classification >", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationGreaterThanOrEqualTo(String value) {
            addCriterion("classification >=", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationLessThan(String value) {
            addCriterion("classification <", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationLessThanOrEqualTo(String value) {
            addCriterion("classification <=", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationLike(String value) {
            addCriterion("classification like", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationNotLike(String value) {
            addCriterion("classification not like", value, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationIn(List<String> values) {
            addCriterion("classification in", values, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationNotIn(List<String> values) {
            addCriterion("classification not in", values, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationBetween(String value1, String value2) {
            addCriterion("classification between", value1, value2, "classification");
            return (Criteria) this;
        }

        public Criteria andClassificationNotBetween(String value1, String value2) {
            addCriterion("classification not between", value1, value2, "classification");
            return (Criteria) this;
        }
        
				
        public Criteria andCommodityNameIsNull() {
            addCriterion("commodity_name is null");
            return (Criteria) this;
        }

        public Criteria andCommodityNameIsNotNull() {
            addCriterion("commodity_name is not null");
            return (Criteria) this;
        }

        public Criteria andCommodityNameEqualTo(String value) {
            addCriterion("commodity_name =", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameNotEqualTo(String value) {
            addCriterion("commodity_name <>", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameGreaterThan(String value) {
            addCriterion("commodity_name >", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameGreaterThanOrEqualTo(String value) {
            addCriterion("commodity_name >=", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameLessThan(String value) {
            addCriterion("commodity_name <", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameLessThanOrEqualTo(String value) {
            addCriterion("commodity_name <=", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameLike(String value) {
            addCriterion("commodity_name like", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameNotLike(String value) {
            addCriterion("commodity_name not like", value, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameIn(List<String> values) {
            addCriterion("commodity_name in", values, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameNotIn(List<String> values) {
            addCriterion("commodity_name not in", values, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameBetween(String value1, String value2) {
            addCriterion("commodity_name between", value1, value2, "commodityName");
            return (Criteria) this;
        }

        public Criteria andCommodityNameNotBetween(String value1, String value2) {
            addCriterion("commodity_name not between", value1, value2, "commodityName");
            return (Criteria) this;
        }
        
				
        public Criteria andCostPriceIsNull() {
            addCriterion("cost_price is null");
            return (Criteria) this;
        }

        public Criteria andCostPriceIsNotNull() {
            addCriterion("cost_price is not null");
            return (Criteria) this;
        }

        public Criteria andCostPriceEqualTo(BigDecimal value) {
            addCriterion("cost_price =", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceNotEqualTo(BigDecimal value) {
            addCriterion("cost_price <>", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceGreaterThan(BigDecimal value) {
            addCriterion("cost_price >", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cost_price >=", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceLessThan(BigDecimal value) {
            addCriterion("cost_price <", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cost_price <=", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceLike(BigDecimal value) {
            addCriterion("cost_price like", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceNotLike(BigDecimal value) {
            addCriterion("cost_price not like", value, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceIn(List<BigDecimal> values) {
            addCriterion("cost_price in", values, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceNotIn(List<BigDecimal> values) {
            addCriterion("cost_price not in", values, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cost_price between", value1, value2, "costPrice");
            return (Criteria) this;
        }

        public Criteria andCostPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cost_price not between", value1, value2, "costPrice");
            return (Criteria) this;
        }
        
				
        public Criteria andRetailPriceIsNull() {
            addCriterion("retail_price is null");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIsNotNull() {
            addCriterion("retail_price is not null");
            return (Criteria) this;
        }

        public Criteria andRetailPriceEqualTo(BigDecimal value) {
            addCriterion("retail_price =", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotEqualTo(BigDecimal value) {
            addCriterion("retail_price <>", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceGreaterThan(BigDecimal value) {
            addCriterion("retail_price >", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("retail_price >=", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLessThan(BigDecimal value) {
            addCriterion("retail_price <", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("retail_price <=", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLike(BigDecimal value) {
            addCriterion("retail_price like", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotLike(BigDecimal value) {
            addCriterion("retail_price not like", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIn(List<BigDecimal> values) {
            addCriterion("retail_price in", values, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotIn(List<BigDecimal> values) {
            addCriterion("retail_price not in", values, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("retail_price between", value1, value2, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("retail_price not between", value1, value2, "retailPrice");
            return (Criteria) this;
        }
        
				
        public Criteria andBarCodeIsNull() {
            addCriterion("bar_code is null");
            return (Criteria) this;
        }

        public Criteria andBarCodeIsNotNull() {
            addCriterion("bar_code is not null");
            return (Criteria) this;
        }

        public Criteria andBarCodeEqualTo(String value) {
            addCriterion("bar_code =", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotEqualTo(String value) {
            addCriterion("bar_code <>", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeGreaterThan(String value) {
            addCriterion("bar_code >", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeGreaterThanOrEqualTo(String value) {
            addCriterion("bar_code >=", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLessThan(String value) {
            addCriterion("bar_code <", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLessThanOrEqualTo(String value) {
            addCriterion("bar_code <=", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLike(String value) {
            addCriterion("bar_code like", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotLike(String value) {
            addCriterion("bar_code not like", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeIn(List<String> values) {
            addCriterion("bar_code in", values, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotIn(List<String> values) {
            addCriterion("bar_code not in", values, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeBetween(String value1, String value2) {
            addCriterion("bar_code between", value1, value2, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotBetween(String value1, String value2) {
            addCriterion("bar_code not between", value1, value2, "barCode");
            return (Criteria) this;
        }
        
				
        public Criteria andQualityGuaranteeIsNull() {
            addCriterion("quality_guarantee is null");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeIsNotNull() {
            addCriterion("quality_guarantee is not null");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeEqualTo(String value) {
            addCriterion("quality_guarantee =", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeNotEqualTo(String value) {
            addCriterion("quality_guarantee <>", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeGreaterThan(String value) {
            addCriterion("quality_guarantee >", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeGreaterThanOrEqualTo(String value) {
            addCriterion("quality_guarantee >=", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeLessThan(String value) {
            addCriterion("quality_guarantee <", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeLessThanOrEqualTo(String value) {
            addCriterion("quality_guarantee <=", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeLike(String value) {
            addCriterion("quality_guarantee like", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeNotLike(String value) {
            addCriterion("quality_guarantee not like", value, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeIn(List<String> values) {
            addCriterion("quality_guarantee in", values, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeNotIn(List<String> values) {
            addCriterion("quality_guarantee not in", values, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeBetween(String value1, String value2) {
            addCriterion("quality_guarantee between", value1, value2, "qualityGuarantee");
            return (Criteria) this;
        }

        public Criteria andQualityGuaranteeNotBetween(String value1, String value2) {
            addCriterion("quality_guarantee not between", value1, value2, "qualityGuarantee");
            return (Criteria) this;
        }
        
				
        public Criteria andTemperatureUpIsNull() {
            addCriterion("temperature_up is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpIsNotNull() {
            addCriterion("temperature_up is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpEqualTo(String value) {
            addCriterion("temperature_up =", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpNotEqualTo(String value) {
            addCriterion("temperature_up <>", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpGreaterThan(String value) {
            addCriterion("temperature_up >", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpGreaterThanOrEqualTo(String value) {
            addCriterion("temperature_up >=", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpLessThan(String value) {
            addCriterion("temperature_up <", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpLessThanOrEqualTo(String value) {
            addCriterion("temperature_up <=", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpLike(String value) {
            addCriterion("temperature_up like", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpNotLike(String value) {
            addCriterion("temperature_up not like", value, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpIn(List<String> values) {
            addCriterion("temperature_up in", values, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpNotIn(List<String> values) {
            addCriterion("temperature_up not in", values, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpBetween(String value1, String value2) {
            addCriterion("temperature_up between", value1, value2, "temperatureUp");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpNotBetween(String value1, String value2) {
            addCriterion("temperature_up not between", value1, value2, "temperatureUp");
            return (Criteria) this;
        }
        
				
        public Criteria andTemperatureDoIsNull() {
            addCriterion("temperature_do is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoIsNotNull() {
            addCriterion("temperature_do is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoEqualTo(String value) {
            addCriterion("temperature_do =", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoNotEqualTo(String value) {
            addCriterion("temperature_do <>", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoGreaterThan(String value) {
            addCriterion("temperature_do >", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoGreaterThanOrEqualTo(String value) {
            addCriterion("temperature_do >=", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoLessThan(String value) {
            addCriterion("temperature_do <", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoLessThanOrEqualTo(String value) {
            addCriterion("temperature_do <=", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoLike(String value) {
            addCriterion("temperature_do like", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoNotLike(String value) {
            addCriterion("temperature_do not like", value, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoIn(List<String> values) {
            addCriterion("temperature_do in", values, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoNotIn(List<String> values) {
            addCriterion("temperature_do not in", values, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoBetween(String value1, String value2) {
            addCriterion("temperature_do between", value1, value2, "temperatureDo");
            return (Criteria) this;
        }

        public Criteria andTemperatureDoNotBetween(String value1, String value2) {
            addCriterion("temperature_do not between", value1, value2, "temperatureDo");
            return (Criteria) this;
        }
        
				
        public Criteria andIngredientsIsNull() {
            addCriterion("ingredients is null");
            return (Criteria) this;
        }

        public Criteria andIngredientsIsNotNull() {
            addCriterion("ingredients is not null");
            return (Criteria) this;
        }

        public Criteria andIngredientsEqualTo(String value) {
            addCriterion("ingredients =", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsNotEqualTo(String value) {
            addCriterion("ingredients <>", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsGreaterThan(String value) {
            addCriterion("ingredients >", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsGreaterThanOrEqualTo(String value) {
            addCriterion("ingredients >=", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsLessThan(String value) {
            addCriterion("ingredients <", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsLessThanOrEqualTo(String value) {
            addCriterion("ingredients <=", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsLike(String value) {
            addCriterion("ingredients like", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsNotLike(String value) {
            addCriterion("ingredients not like", value, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsIn(List<String> values) {
            addCriterion("ingredients in", values, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsNotIn(List<String> values) {
            addCriterion("ingredients not in", values, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsBetween(String value1, String value2) {
            addCriterion("ingredients between", value1, value2, "ingredients");
            return (Criteria) this;
        }

        public Criteria andIngredientsNotBetween(String value1, String value2) {
            addCriterion("ingredients not between", value1, value2, "ingredients");
            return (Criteria) this;
        }
        
				
        public Criteria andNotesIsNull() {
            addCriterion("notes is null");
            return (Criteria) this;
        }

        public Criteria andNotesIsNotNull() {
            addCriterion("notes is not null");
            return (Criteria) this;
        }

        public Criteria andNotesEqualTo(String value) {
            addCriterion("notes =", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotEqualTo(String value) {
            addCriterion("notes <>", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThan(String value) {
            addCriterion("notes >", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThanOrEqualTo(String value) {
            addCriterion("notes >=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThan(String value) {
            addCriterion("notes <", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThanOrEqualTo(String value) {
            addCriterion("notes <=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLike(String value) {
            addCriterion("notes like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotLike(String value) {
            addCriterion("notes not like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesIn(List<String> values) {
            addCriterion("notes in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotIn(List<String> values) {
            addCriterion("notes not in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesBetween(String value1, String value2) {
            addCriterion("notes between", value1, value2, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotBetween(String value1, String value2) {
            addCriterion("notes not between", value1, value2, "notes");
            return (Criteria) this;
        }
        
				
        public Criteria andCompanyDescIsNull() {
            addCriterion("company_desc is null");
            return (Criteria) this;
        }

        public Criteria andCompanyDescIsNotNull() {
            addCriterion("company_desc is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyDescEqualTo(String value) {
            addCriterion("company_desc =", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescNotEqualTo(String value) {
            addCriterion("company_desc <>", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescGreaterThan(String value) {
            addCriterion("company_desc >", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescGreaterThanOrEqualTo(String value) {
            addCriterion("company_desc >=", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescLessThan(String value) {
            addCriterion("company_desc <", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescLessThanOrEqualTo(String value) {
            addCriterion("company_desc <=", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescLike(String value) {
            addCriterion("company_desc like", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescNotLike(String value) {
            addCriterion("company_desc not like", value, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescIn(List<String> values) {
            addCriterion("company_desc in", values, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescNotIn(List<String> values) {
            addCriterion("company_desc not in", values, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescBetween(String value1, String value2) {
            addCriterion("company_desc between", value1, value2, "companyDesc");
            return (Criteria) this;
        }

        public Criteria andCompanyDescNotBetween(String value1, String value2) {
            addCriterion("company_desc not between", value1, value2, "companyDesc");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLike(Date value) {
            addCriterion("created_time like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotLike(Date value) {
            addCriterion("created_time not like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedUserSidIsNull() {
            addCriterion("created_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIsNotNull() {
            addCriterion("created_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidEqualTo(Long value) {
            addCriterion("created_user_sid =", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotEqualTo(Long value) {
            addCriterion("created_user_sid <>", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThan(Long value) {
            addCriterion("created_user_sid >", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("created_user_sid >=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThan(Long value) {
            addCriterion("created_user_sid <", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("created_user_sid <=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLike(Long value) {
            addCriterion("created_user_sid like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotLike(Long value) {
            addCriterion("created_user_sid not like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIn(List<Long> values) {
            addCriterion("created_user_sid in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotIn(List<Long> values) {
            addCriterion("created_user_sid not in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidBetween(Long value1, Long value2) {
            addCriterion("created_user_sid between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("created_user_sid not between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedTimeIsNull() {
            addCriterion("modified_time is null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIsNotNull() {
            addCriterion("modified_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeEqualTo(Date value) {
            addCriterion("modified_time =", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotEqualTo(Date value) {
            addCriterion("modified_time <>", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThan(Date value) {
            addCriterion("modified_time >", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modified_time >=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThan(Date value) {
            addCriterion("modified_time <", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThanOrEqualTo(Date value) {
            addCriterion("modified_time <=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLike(Date value) {
            addCriterion("modified_time like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotLike(Date value) {
            addCriterion("modified_time not like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIn(List<Date> values) {
            addCriterion("modified_time in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotIn(List<Date> values) {
            addCriterion("modified_time not in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeBetween(Date value1, Date value2) {
            addCriterion("modified_time between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotBetween(Date value1, Date value2) {
            addCriterion("modified_time not between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedUserSidIsNull() {
            addCriterion("modified_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIsNotNull() {
            addCriterion("modified_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidEqualTo(Long value) {
            addCriterion("modified_user_sid =", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotEqualTo(Long value) {
            addCriterion("modified_user_sid <>", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThan(Long value) {
            addCriterion("modified_user_sid >", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid >=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThan(Long value) {
            addCriterion("modified_user_sid <", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid <=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLike(Long value) {
            addCriterion("modified_user_sid like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotLike(Long value) {
            addCriterion("modified_user_sid not like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIn(List<Long> values) {
            addCriterion("modified_user_sid in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotIn(List<Long> values) {
            addCriterion("modified_user_sid not in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid not between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andXIsNull() {
            addCriterion("x is null");
            return (Criteria) this;
        }

        public Criteria andXIsNotNull() {
            addCriterion("x is not null");
            return (Criteria) this;
        }

        public Criteria andXEqualTo(Integer value) {
            addCriterion("x =", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotEqualTo(Integer value) {
            addCriterion("x <>", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThan(Integer value) {
            addCriterion("x >", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThanOrEqualTo(Integer value) {
            addCriterion("x >=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThan(Integer value) {
            addCriterion("x <", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThanOrEqualTo(Integer value) {
            addCriterion("x <=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLike(Integer value) {
            addCriterion("x like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotLike(Integer value) {
            addCriterion("x not like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXIn(List<Integer> values) {
            addCriterion("x in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXNotIn(List<Integer> values) {
            addCriterion("x not in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXBetween(Integer value1, Integer value2) {
            addCriterion("x between", value1, value2, "x");
            return (Criteria) this;
        }

        public Criteria andXNotBetween(Integer value1, Integer value2) {
            addCriterion("x not between", value1, value2, "x");
            return (Criteria) this;
        }
        
				
        public Criteria andXTimeIsNull() {
            addCriterion("x_time is null");
            return (Criteria) this;
        }

        public Criteria andXTimeIsNotNull() {
            addCriterion("x_time is not null");
            return (Criteria) this;
        }

        public Criteria andXTimeEqualTo(Date value) {
            addCriterion("x_time =", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotEqualTo(Date value) {
            addCriterion("x_time <>", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThan(Date value) {
            addCriterion("x_time >", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("x_time >=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThan(Date value) {
            addCriterion("x_time <", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThanOrEqualTo(Date value) {
            addCriterion("x_time <=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLike(Date value) {
            addCriterion("x_time like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotLike(Date value) {
            addCriterion("x_time not like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeIn(List<Date> values) {
            addCriterion("x_time in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotIn(List<Date> values) {
            addCriterion("x_time not in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeBetween(Date value1, Date value2) {
            addCriterion("x_time between", value1, value2, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotBetween(Date value1, Date value2) {
            addCriterion("x_time not between", value1, value2, "xTime");
            return (Criteria) this;
        }
        
				
        public Criteria andXUserSidIsNull() {
            addCriterion("x_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andXUserSidIsNotNull() {
            addCriterion("x_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andXUserSidEqualTo(Long value) {
            addCriterion("x_user_sid =", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotEqualTo(Long value) {
            addCriterion("x_user_sid <>", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThan(Long value) {
            addCriterion("x_user_sid >", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("x_user_sid >=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThan(Long value) {
            addCriterion("x_user_sid <", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThanOrEqualTo(Long value) {
            addCriterion("x_user_sid <=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLike(Long value) {
            addCriterion("x_user_sid like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotLike(Long value) {
            addCriterion("x_user_sid not like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidIn(List<Long> values) {
            addCriterion("x_user_sid in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotIn(List<Long> values) {
            addCriterion("x_user_sid not in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidBetween(Long value1, Long value2) {
            addCriterion("x_user_sid between", value1, value2, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotBetween(Long value1, Long value2) {
            addCriterion("x_user_sid not between", value1, value2, "xUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andStr1IsNull() {
            addCriterion("str_1 is null");
            return (Criteria) this;
        }

        public Criteria andStr1IsNotNull() {
            addCriterion("str_1 is not null");
            return (Criteria) this;
        }

        public Criteria andStr1EqualTo(String value) {
            addCriterion("str_1 =", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotEqualTo(String value) {
            addCriterion("str_1 <>", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThan(String value) {
            addCriterion("str_1 >", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThanOrEqualTo(String value) {
            addCriterion("str_1 >=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThan(String value) {
            addCriterion("str_1 <", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThanOrEqualTo(String value) {
            addCriterion("str_1 <=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Like(String value) {
            addCriterion("str_1 like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotLike(String value) {
            addCriterion("str_1 not like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1In(List<String> values) {
            addCriterion("str_1 in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotIn(List<String> values) {
            addCriterion("str_1 not in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Between(String value1, String value2) {
            addCriterion("str_1 between", value1, value2, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotBetween(String value1, String value2) {
            addCriterion("str_1 not between", value1, value2, "str1");
            return (Criteria) this;
        }
        
				
        public Criteria andStr2IsNull() {
            addCriterion("str_2 is null");
            return (Criteria) this;
        }

        public Criteria andStr2IsNotNull() {
            addCriterion("str_2 is not null");
            return (Criteria) this;
        }

        public Criteria andStr2EqualTo(String value) {
            addCriterion("str_2 =", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotEqualTo(String value) {
            addCriterion("str_2 <>", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThan(String value) {
            addCriterion("str_2 >", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThanOrEqualTo(String value) {
            addCriterion("str_2 >=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThan(String value) {
            addCriterion("str_2 <", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThanOrEqualTo(String value) {
            addCriterion("str_2 <=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Like(String value) {
            addCriterion("str_2 like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotLike(String value) {
            addCriterion("str_2 not like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2In(List<String> values) {
            addCriterion("str_2 in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotIn(List<String> values) {
            addCriterion("str_2 not in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Between(String value1, String value2) {
            addCriterion("str_2 between", value1, value2, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotBetween(String value1, String value2) {
            addCriterion("str_2 not between", value1, value2, "str2");
            return (Criteria) this;
        }
        
				
        public Criteria andStr3IsNull() {
            addCriterion("str_3 is null");
            return (Criteria) this;
        }

        public Criteria andStr3IsNotNull() {
            addCriterion("str_3 is not null");
            return (Criteria) this;
        }

        public Criteria andStr3EqualTo(String value) {
            addCriterion("str_3 =", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotEqualTo(String value) {
            addCriterion("str_3 <>", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3GreaterThan(String value) {
            addCriterion("str_3 >", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3GreaterThanOrEqualTo(String value) {
            addCriterion("str_3 >=", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3LessThan(String value) {
            addCriterion("str_3 <", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3LessThanOrEqualTo(String value) {
            addCriterion("str_3 <=", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3Like(String value) {
            addCriterion("str_3 like", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotLike(String value) {
            addCriterion("str_3 not like", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3In(List<String> values) {
            addCriterion("str_3 in", values, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotIn(List<String> values) {
            addCriterion("str_3 not in", values, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3Between(String value1, String value2) {
            addCriterion("str_3 between", value1, value2, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotBetween(String value1, String value2) {
            addCriterion("str_3 not between", value1, value2, "str3");
            return (Criteria) this;
        }
        
				
        public Criteria andNum1IsNull() {
            addCriterion("num_1 is null");
            return (Criteria) this;
        }

        public Criteria andNum1IsNotNull() {
            addCriterion("num_1 is not null");
            return (Criteria) this;
        }

        public Criteria andNum1EqualTo(Long value) {
            addCriterion("num_1 =", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotEqualTo(Long value) {
            addCriterion("num_1 <>", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThan(Long value) {
            addCriterion("num_1 >", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThanOrEqualTo(Long value) {
            addCriterion("num_1 >=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThan(Long value) {
            addCriterion("num_1 <", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThanOrEqualTo(Long value) {
            addCriterion("num_1 <=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Like(Long value) {
            addCriterion("num_1 like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotLike(Long value) {
            addCriterion("num_1 not like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1In(List<Long> values) {
            addCriterion("num_1 in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotIn(List<Long> values) {
            addCriterion("num_1 not in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Between(Long value1, Long value2) {
            addCriterion("num_1 between", value1, value2, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotBetween(Long value1, Long value2) {
            addCriterion("num_1 not between", value1, value2, "num1");
            return (Criteria) this;
        }
        
				
        public Criteria andNum2IsNull() {
            addCriterion("num_2 is null");
            return (Criteria) this;
        }

        public Criteria andNum2IsNotNull() {
            addCriterion("num_2 is not null");
            return (Criteria) this;
        }

        public Criteria andNum2EqualTo(Integer value) {
            addCriterion("num_2 =", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotEqualTo(Integer value) {
            addCriterion("num_2 <>", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThan(Integer value) {
            addCriterion("num_2 >", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThanOrEqualTo(Integer value) {
            addCriterion("num_2 >=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThan(Integer value) {
            addCriterion("num_2 <", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThanOrEqualTo(Integer value) {
            addCriterion("num_2 <=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Like(Integer value) {
            addCriterion("num_2 like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotLike(Integer value) {
            addCriterion("num_2 not like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2In(List<Integer> values) {
            addCriterion("num_2 in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotIn(List<Integer> values) {
            addCriterion("num_2 not in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Between(Integer value1, Integer value2) {
            addCriterion("num_2 between", value1, value2, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotBetween(Integer value1, Integer value2) {
            addCriterion("num_2 not between", value1, value2, "num2");
            return (Criteria) this;
        }
        
				
        public Criteria andNum3IsNull() {
            addCriterion("num_3 is null");
            return (Criteria) this;
        }

        public Criteria andNum3IsNotNull() {
            addCriterion("num_3 is not null");
            return (Criteria) this;
        }

        public Criteria andNum3EqualTo(Integer value) {
            addCriterion("num_3 =", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotEqualTo(Integer value) {
            addCriterion("num_3 <>", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3GreaterThan(Integer value) {
            addCriterion("num_3 >", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3GreaterThanOrEqualTo(Integer value) {
            addCriterion("num_3 >=", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3LessThan(Integer value) {
            addCriterion("num_3 <", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3LessThanOrEqualTo(Integer value) {
            addCriterion("num_3 <=", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3Like(Integer value) {
            addCriterion("num_3 like", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotLike(Integer value) {
            addCriterion("num_3 not like", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3In(List<Integer> values) {
            addCriterion("num_3 in", values, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotIn(List<Integer> values) {
            addCriterion("num_3 not in", values, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3Between(Integer value1, Integer value2) {
            addCriterion("num_3 between", value1, value2, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotBetween(Integer value1, Integer value2) {
            addCriterion("num_3 not between", value1, value2, "num3");
            return (Criteria) this;
        }
        
				
        public Criteria andProvinceCodeIsNull() {
            addCriterion("province_code is null");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIsNotNull() {
            addCriterion("province_code is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeEqualTo(String value) {
            addCriterion("province_code =", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotEqualTo(String value) {
            addCriterion("province_code <>", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeGreaterThan(String value) {
            addCriterion("province_code >", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("province_code >=", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLessThan(String value) {
            addCriterion("province_code <", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLessThanOrEqualTo(String value) {
            addCriterion("province_code <=", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLike(String value) {
            addCriterion("province_code like", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotLike(String value) {
            addCriterion("province_code not like", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIn(List<String> values) {
            addCriterion("province_code in", values, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotIn(List<String> values) {
            addCriterion("province_code not in", values, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeBetween(String value1, String value2) {
            addCriterion("province_code between", value1, value2, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotBetween(String value1, String value2) {
            addCriterion("province_code not between", value1, value2, "provinceCode");
            return (Criteria) this;
        }
        
				
        public Criteria andCityCodeIsNull() {
            addCriterion("city_code is null");
            return (Criteria) this;
        }

        public Criteria andCityCodeIsNotNull() {
            addCriterion("city_code is not null");
            return (Criteria) this;
        }

        public Criteria andCityCodeEqualTo(String value) {
            addCriterion("city_code =", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotEqualTo(String value) {
            addCriterion("city_code <>", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeGreaterThan(String value) {
            addCriterion("city_code >", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeGreaterThanOrEqualTo(String value) {
            addCriterion("city_code >=", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLessThan(String value) {
            addCriterion("city_code <", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLessThanOrEqualTo(String value) {
            addCriterion("city_code <=", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLike(String value) {
            addCriterion("city_code like", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotLike(String value) {
            addCriterion("city_code not like", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeIn(List<String> values) {
            addCriterion("city_code in", values, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotIn(List<String> values) {
            addCriterion("city_code not in", values, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeBetween(String value1, String value2) {
            addCriterion("city_code between", value1, value2, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotBetween(String value1, String value2) {
            addCriterion("city_code not between", value1, value2, "cityCode");
            return (Criteria) this;
        }
        
				
        public Criteria andAreaCodeIsNull() {
            addCriterion("area_code is null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNotNull() {
            addCriterion("area_code is not null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeEqualTo(String value) {
            addCriterion("area_code =", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotEqualTo(String value) {
            addCriterion("area_code <>", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThan(String value) {
            addCriterion("area_code >", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("area_code >=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThan(String value) {
            addCriterion("area_code <", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("area_code <=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLike(String value) {
            addCriterion("area_code like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotLike(String value) {
            addCriterion("area_code not like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIn(List<String> values) {
            addCriterion("area_code in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotIn(List<String> values) {
            addCriterion("area_code not in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeBetween(String value1, String value2) {
            addCriterion("area_code between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotBetween(String value1, String value2) {
            addCriterion("area_code not between", value1, value2, "areaCode");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(Commodity record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getSupplierId()!=null&&StrUtil.isNotEmpty(record.getSupplierId().toString())) {
    			 list.add("ifnull(supplier_id,'')");
    		}
			if(record.getSupplierName()!=null&&StrUtil.isNotEmpty(record.getSupplierName().toString())) {
    			 list.add("ifnull(supplier_name,'')");
    		}
			if(record.getCommodityType()!=null&&StrUtil.isNotEmpty(record.getCommodityType().toString())) {
    			 list.add("ifnull(commodity_type,'')");
    		}
			if(record.getClassification()!=null&&StrUtil.isNotEmpty(record.getClassification().toString())) {
    			 list.add("ifnull(classification,'')");
    		}
			if(record.getCommodityName()!=null&&StrUtil.isNotEmpty(record.getCommodityName().toString())) {
    			 list.add("ifnull(commodity_name,'')");
    		}
			if(record.getCostPrice()!=null&&StrUtil.isNotEmpty(record.getCostPrice().toString())) {
    			 list.add("ifnull(cost_price,'')");
    		}
			if(record.getRetailPrice()!=null&&StrUtil.isNotEmpty(record.getRetailPrice().toString())) {
    			 list.add("ifnull(retail_price,'')");
    		}
			if(record.getBarCode()!=null&&StrUtil.isNotEmpty(record.getBarCode().toString())) {
    			 list.add("ifnull(bar_code,'')");
    		}
			if(record.getQualityGuarantee()!=null&&StrUtil.isNotEmpty(record.getQualityGuarantee().toString())) {
    			 list.add("ifnull(quality_guarantee,'')");
    		}
			if(record.getTemperatureUp()!=null&&StrUtil.isNotEmpty(record.getTemperatureUp().toString())) {
    			 list.add("ifnull(temperature_up,'')");
    		}
			if(record.getTemperatureDo()!=null&&StrUtil.isNotEmpty(record.getTemperatureDo().toString())) {
    			 list.add("ifnull(temperature_do,'')");
    		}
			if(record.getIngredients()!=null&&StrUtil.isNotEmpty(record.getIngredients().toString())) {
    			 list.add("ifnull(ingredients,'')");
    		}
			if(record.getNotes()!=null&&StrUtil.isNotEmpty(record.getNotes().toString())) {
    			 list.add("ifnull(notes,'')");
    		}
			if(record.getCompanyDesc()!=null&&StrUtil.isNotEmpty(record.getCompanyDesc().toString())) {
    			 list.add("ifnull(company_desc,'')");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			 list.add("ifnull(created_time,'')");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			 list.add("ifnull(created_user_sid,'')");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			 list.add("ifnull(modified_time,'')");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			 list.add("ifnull(modified_user_sid,'')");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			 list.add("ifnull(x,'')");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			 list.add("ifnull(x_time,'')");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			 list.add("ifnull(x_user_sid,'')");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			 list.add("ifnull(str_1,'')");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			 list.add("ifnull(str_2,'')");
    		}
			if(record.getStr3()!=null&&StrUtil.isNotEmpty(record.getStr3().toString())) {
    			 list.add("ifnull(str_3,'')");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			 list.add("ifnull(num_1,'')");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			 list.add("ifnull(num_2,'')");
    		}
			if(record.getNum3()!=null&&StrUtil.isNotEmpty(record.getNum3().toString())) {
    			 list.add("ifnull(num_3,'')");
    		}
			if(record.getProvinceCode()!=null&&StrUtil.isNotEmpty(record.getProvinceCode().toString())) {
    			 list.add("ifnull(province_code,'')");
    		}
			if(record.getCityCode()!=null&&StrUtil.isNotEmpty(record.getCityCode().toString())) {
    			 list.add("ifnull(city_code,'')");
    		}
			if(record.getAreaCode()!=null&&StrUtil.isNotEmpty(record.getAreaCode().toString())) {
    			 list.add("ifnull(area_code,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getSupplierId()!=null&&StrUtil.isNotEmpty(record.getSupplierId().toString())) {
    			list2.add("'%"+record.getSupplierId()+"%'");
    		}
			if(record.getSupplierName()!=null&&StrUtil.isNotEmpty(record.getSupplierName().toString())) {
    			list2.add("'%"+record.getSupplierName()+"%'");
    		}
			if(record.getCommodityType()!=null&&StrUtil.isNotEmpty(record.getCommodityType().toString())) {
    			list2.add("'%"+record.getCommodityType()+"%'");
    		}
			if(record.getClassification()!=null&&StrUtil.isNotEmpty(record.getClassification().toString())) {
    			list2.add("'%"+record.getClassification()+"%'");
    		}
			if(record.getCommodityName()!=null&&StrUtil.isNotEmpty(record.getCommodityName().toString())) {
    			list2.add("'%"+record.getCommodityName()+"%'");
    		}
			if(record.getCostPrice()!=null&&StrUtil.isNotEmpty(record.getCostPrice().toString())) {
    			list2.add("'%"+record.getCostPrice()+"%'");
    		}
			if(record.getRetailPrice()!=null&&StrUtil.isNotEmpty(record.getRetailPrice().toString())) {
    			list2.add("'%"+record.getRetailPrice()+"%'");
    		}
			if(record.getBarCode()!=null&&StrUtil.isNotEmpty(record.getBarCode().toString())) {
    			list2.add("'%"+record.getBarCode()+"%'");
    		}
			if(record.getQualityGuarantee()!=null&&StrUtil.isNotEmpty(record.getQualityGuarantee().toString())) {
    			list2.add("'%"+record.getQualityGuarantee()+"%'");
    		}
			if(record.getTemperatureUp()!=null&&StrUtil.isNotEmpty(record.getTemperatureUp().toString())) {
    			list2.add("'%"+record.getTemperatureUp()+"%'");
    		}
			if(record.getTemperatureDo()!=null&&StrUtil.isNotEmpty(record.getTemperatureDo().toString())) {
    			list2.add("'%"+record.getTemperatureDo()+"%'");
    		}
			if(record.getIngredients()!=null&&StrUtil.isNotEmpty(record.getIngredients().toString())) {
    			list2.add("'%"+record.getIngredients()+"%'");
    		}
			if(record.getNotes()!=null&&StrUtil.isNotEmpty(record.getNotes().toString())) {
    			list2.add("'%"+record.getNotes()+"%'");
    		}
			if(record.getCompanyDesc()!=null&&StrUtil.isNotEmpty(record.getCompanyDesc().toString())) {
    			list2.add("'%"+record.getCompanyDesc()+"%'");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			list2.add("'%"+record.getCreatedTime()+"%'");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			list2.add("'%"+record.getCreatedUserSid()+"%'");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			list2.add("'%"+record.getModifiedTime()+"%'");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			list2.add("'%"+record.getModifiedUserSid()+"%'");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			list2.add("'%"+record.getX()+"%'");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			list2.add("'%"+record.getXTime()+"%'");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			list2.add("'%"+record.getXUserSid()+"%'");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			list2.add("'%"+record.getStr1()+"%'");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			list2.add("'%"+record.getStr2()+"%'");
    		}
			if(record.getStr3()!=null&&StrUtil.isNotEmpty(record.getStr3().toString())) {
    			list2.add("'%"+record.getStr3()+"%'");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			list2.add("'%"+record.getNum1()+"%'");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			list2.add("'%"+record.getNum2()+"%'");
    		}
			if(record.getNum3()!=null&&StrUtil.isNotEmpty(record.getNum3().toString())) {
    			list2.add("'%"+record.getNum3()+"%'");
    		}
			if(record.getProvinceCode()!=null&&StrUtil.isNotEmpty(record.getProvinceCode().toString())) {
    			list2.add("'%"+record.getProvinceCode()+"%'");
    		}
			if(record.getCityCode()!=null&&StrUtil.isNotEmpty(record.getCityCode().toString())) {
    			list2.add("'%"+record.getCityCode()+"%'");
    		}
			if(record.getAreaCode()!=null&&StrUtil.isNotEmpty(record.getAreaCode().toString())) {
    			list2.add("'%"+record.getAreaCode()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(supplier_id,'')");
    		list.add("ifnull(supplier_name,'')");
    		list.add("ifnull(commodity_type,'')");
    		list.add("ifnull(classification,'')");
    		list.add("ifnull(commodity_name,'')");
    		list.add("ifnull(cost_price,'')");
    		list.add("ifnull(retail_price,'')");
    		list.add("ifnull(bar_code,'')");
    		list.add("ifnull(quality_guarantee,'')");
    		list.add("ifnull(temperature_up,'')");
    		list.add("ifnull(temperature_do,'')");
    		list.add("ifnull(ingredients,'')");
    		list.add("ifnull(notes,'')");
    		list.add("ifnull(company_desc,'')");
    		list.add("ifnull(created_time,'')");
    		list.add("ifnull(created_user_sid,'')");
    		list.add("ifnull(modified_time,'')");
    		list.add("ifnull(modified_user_sid,'')");
    		list.add("ifnull(x,'')");
    		list.add("ifnull(x_time,'')");
    		list.add("ifnull(x_user_sid,'')");
    		list.add("ifnull(str_1,'')");
    		list.add("ifnull(str_2,'')");
    		list.add("ifnull(str_3,'')");
    		list.add("ifnull(num_1,'')");
    		list.add("ifnull(num_2,'')");
    		list.add("ifnull(num_3,'')");
    		list.add("ifnull(province_code,'')");
    		list.add("ifnull(city_code,'')");
    		list.add("ifnull(area_code,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}