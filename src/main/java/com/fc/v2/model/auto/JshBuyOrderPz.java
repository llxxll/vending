package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class JshBuyOrderPz implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "采购单号")
	private String buyNo;
	
	@ApiModelProperty(value = "产品id")
	private String canpinId;
	
	@ApiModelProperty(value = "采购数量")
	private Integer count;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@ApiModelProperty(value = "产品单价")
	private String canpinPrice;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@ApiModelProperty(value = "预留字段")
	private String field1;
	
	@ApiModelProperty(value = "预留字段")
	private String field2;
	
	@ApiModelProperty(value = "预留字段3")
	private String field3;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date finshTime;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("buyNo")
	public String getBuyNo() {
		return buyNo;
	}

	public void setBuyNo(String buyNo) {
		this.buyNo =  buyNo;
	}
	@JsonProperty("canpinId")
	public String getCanpinId() {
		return canpinId;
	}

	public void setCanpinId(String canpinId) {
		this.canpinId =  canpinId;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("canpinPrice")
	public String getCanpinPrice() {
		return canpinPrice;
	}

	public void setCanpinPrice(String canpinPrice) {
		this.canpinPrice =  canpinPrice;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 =  field1;
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 =  field2;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}
	@JsonProperty("finshTime")
	public Date getFinshTime() {
		return finshTime;
	}

	public void setFinshTime(Date finshTime) {
		this.finshTime =  finshTime;
	}

																												
	public JshBuyOrderPz(Long id,String buyNo,String canpinId,Integer count,Integer status,String canpinPrice,Date createTime,Long creator,Date updateTime,Long updater,String field1,String field2,String field3,Date finshTime) {
				
		this.id = id;
				
		this.buyNo = buyNo;
				
		this.canpinId = canpinId;
				
		this.count = count;
				
		this.status = status;
				
		this.canpinPrice = canpinPrice;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.finshTime = finshTime;
				
	}

	public JshBuyOrderPz() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}