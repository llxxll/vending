package com.fc.v2.model.auto;

import cn.hutool.setting.Setting;
import com.baomidou.mybatisplus.annotation.TableField;


import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;

/**
 * Entity - 购物车项
 * 
 */
@Entity
public class CartItem extends BaseEntity<CartItem> {

	private static final long serialVersionUID = 2979296789363163144L;

	/**
	 * 最大数量
	 */
	public static final Integer MAX_QUANTITY = 10000;

	/**
	 * 数量
	 */
	private Integer quantity;



	/**
	 * 购物车
	 */
	@TableField(exist = false)
	private Cart cart;

	//是否购买
	private int isBuy;

	public int getIsBuy() {
		return isBuy;
	}

	public void setIsBuy(int isBuy) {
		this.isBuy = isBuy;
	}

	/**
	 * 获取数量
	 * 
	 * @return 数量
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * 设置数量
	 * 
	 * @param quantity
	 *            数量
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}




	/**
	 * 获取购物车
	 * 
	 * @return 购物车
	 */
	public Cart getCart() {
		return cart;
	}

	/**
	 * 设置购物车
	 * 
	 * @param cart
	 *            购物车
	 */
	public void setCart(Cart cart) {
		this.cart = cart;
	}




}