package com.fc.v2.model.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 产品工艺加个扩展表 JshMaterialCarfpriceExample
 * @author fuce_自动生成
 * @date 2021-12-19 21:41:13
 */
public class JshMaterialCarfpriceExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public JshMaterialCarfpriceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andMaterialIdIsNull() {
            addCriterion("material_id is null");
            return (Criteria) this;
        }

        public Criteria andMaterialIdIsNotNull() {
            addCriterion("material_id is not null");
            return (Criteria) this;
        }

        public Criteria andMaterialIdEqualTo(Long value) {
            addCriterion("material_id =", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdNotEqualTo(Long value) {
            addCriterion("material_id <>", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdGreaterThan(Long value) {
            addCriterion("material_id >", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdGreaterThanOrEqualTo(Long value) {
            addCriterion("material_id >=", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdLessThan(Long value) {
            addCriterion("material_id <", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdLessThanOrEqualTo(Long value) {
            addCriterion("material_id <=", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdLike(Long value) {
            addCriterion("material_id like", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdNotLike(Long value) {
            addCriterion("material_id not like", value, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdIn(List<Long> values) {
            addCriterion("material_id in", values, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdNotIn(List<Long> values) {
            addCriterion("material_id not in", values, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdBetween(Long value1, Long value2) {
            addCriterion("material_id between", value1, value2, "materialId");
            return (Criteria) this;
        }

        public Criteria andMaterialIdNotBetween(Long value1, Long value2) {
            addCriterion("material_id not between", value1, value2, "materialId");
            return (Criteria) this;
        }
        
				
        public Criteria andOrderCodeIsNull() {
            addCriterion("order_code is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("order_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("order_code =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("order_code <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("order_code >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("order_code >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("order_code <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("order_code <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("order_code like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("order_code not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("order_code in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("order_code not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("order_code between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("order_code not between", value1, value2, "orderCode");
            return (Criteria) this;
        }
        
				
        public Criteria andSpecsIsNull() {
            addCriterion("specs is null");
            return (Criteria) this;
        }

        public Criteria andSpecsIsNotNull() {
            addCriterion("specs is not null");
            return (Criteria) this;
        }

        public Criteria andSpecsEqualTo(String value) {
            addCriterion("specs =", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsNotEqualTo(String value) {
            addCriterion("specs <>", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsGreaterThan(String value) {
            addCriterion("specs >", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsGreaterThanOrEqualTo(String value) {
            addCriterion("specs >=", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsLessThan(String value) {
            addCriterion("specs <", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsLessThanOrEqualTo(String value) {
            addCriterion("specs <=", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsLike(String value) {
            addCriterion("specs like", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsNotLike(String value) {
            addCriterion("specs not like", value, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsIn(List<String> values) {
            addCriterion("specs in", values, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsNotIn(List<String> values) {
            addCriterion("specs not in", values, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsBetween(String value1, String value2) {
            addCriterion("specs between", value1, value2, "specs");
            return (Criteria) this;
        }

        public Criteria andSpecsNotBetween(String value1, String value2) {
            addCriterion("specs not between", value1, value2, "specs");
            return (Criteria) this;
        }
        
				
        public Criteria andCarftIdIsNull() {
            addCriterion("carft_id is null");
            return (Criteria) this;
        }

        public Criteria andCarftIdIsNotNull() {
            addCriterion("carft_id is not null");
            return (Criteria) this;
        }

        public Criteria andCarftIdEqualTo(Long value) {
            addCriterion("carft_id =", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotEqualTo(Long value) {
            addCriterion("carft_id <>", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdGreaterThan(Long value) {
            addCriterion("carft_id >", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdGreaterThanOrEqualTo(Long value) {
            addCriterion("carft_id >=", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdLessThan(Long value) {
            addCriterion("carft_id <", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdLessThanOrEqualTo(Long value) {
            addCriterion("carft_id <=", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdLike(Long value) {
            addCriterion("carft_id like", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotLike(Long value) {
            addCriterion("carft_id not like", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdIn(List<Long> values) {
            addCriterion("carft_id in", values, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotIn(List<Long> values) {
            addCriterion("carft_id not in", values, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdBetween(Long value1, Long value2) {
            addCriterion("carft_id between", value1, value2, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotBetween(Long value1, Long value2) {
            addCriterion("carft_id not between", value1, value2, "carftId");
            return (Criteria) this;
        }
        
				
        public Criteria andPurchaseDecimalIsNull() {
            addCriterion("purchase_decimal is null");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalIsNotNull() {
            addCriterion("purchase_decimal is not null");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalEqualTo(BigDecimal value) {
            addCriterion("purchase_decimal =", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalNotEqualTo(BigDecimal value) {
            addCriterion("purchase_decimal <>", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalGreaterThan(BigDecimal value) {
            addCriterion("purchase_decimal >", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("purchase_decimal >=", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalLessThan(BigDecimal value) {
            addCriterion("purchase_decimal <", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("purchase_decimal <=", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalLike(BigDecimal value) {
            addCriterion("purchase_decimal like", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalNotLike(BigDecimal value) {
            addCriterion("purchase_decimal not like", value, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalIn(List<BigDecimal> values) {
            addCriterion("purchase_decimal in", values, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalNotIn(List<BigDecimal> values) {
            addCriterion("purchase_decimal not in", values, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchase_decimal between", value1, value2, "purchaseDecimal");
            return (Criteria) this;
        }

        public Criteria andPurchaseDecimalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchase_decimal not between", value1, value2, "purchaseDecimal");
            return (Criteria) this;
        }
        
				
        public Criteria andLowDecimalIsNull() {
            addCriterion("low_decimal is null");
            return (Criteria) this;
        }

        public Criteria andLowDecimalIsNotNull() {
            addCriterion("low_decimal is not null");
            return (Criteria) this;
        }

        public Criteria andLowDecimalEqualTo(BigDecimal value) {
            addCriterion("low_decimal =", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalNotEqualTo(BigDecimal value) {
            addCriterion("low_decimal <>", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalGreaterThan(BigDecimal value) {
            addCriterion("low_decimal >", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("low_decimal >=", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalLessThan(BigDecimal value) {
            addCriterion("low_decimal <", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("low_decimal <=", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalLike(BigDecimal value) {
            addCriterion("low_decimal like", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalNotLike(BigDecimal value) {
            addCriterion("low_decimal not like", value, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalIn(List<BigDecimal> values) {
            addCriterion("low_decimal in", values, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalNotIn(List<BigDecimal> values) {
            addCriterion("low_decimal not in", values, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_decimal between", value1, value2, "lowDecimal");
            return (Criteria) this;
        }

        public Criteria andLowDecimalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_decimal not between", value1, value2, "lowDecimal");
            return (Criteria) this;
        }
        
				
        public Criteria andTenantIdIsNull() {
            addCriterion("tenant_id is null");
            return (Criteria) this;
        }

        public Criteria andTenantIdIsNotNull() {
            addCriterion("tenant_id is not null");
            return (Criteria) this;
        }

        public Criteria andTenantIdEqualTo(Long value) {
            addCriterion("tenant_id =", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotEqualTo(Long value) {
            addCriterion("tenant_id <>", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdGreaterThan(Long value) {
            addCriterion("tenant_id >", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdGreaterThanOrEqualTo(Long value) {
            addCriterion("tenant_id >=", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLessThan(Long value) {
            addCriterion("tenant_id <", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLessThanOrEqualTo(Long value) {
            addCriterion("tenant_id <=", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdLike(Long value) {
            addCriterion("tenant_id like", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotLike(Long value) {
            addCriterion("tenant_id not like", value, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdIn(List<Long> values) {
            addCriterion("tenant_id in", values, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotIn(List<Long> values) {
            addCriterion("tenant_id not in", values, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdBetween(Long value1, Long value2) {
            addCriterion("tenant_id between", value1, value2, "tenantId");
            return (Criteria) this;
        }

        public Criteria andTenantIdNotBetween(Long value1, Long value2) {
            addCriterion("tenant_id not between", value1, value2, "tenantId");
            return (Criteria) this;
        }
        
				
        public Criteria andDeleteFlagIsNull() {
            addCriterion("delete_Flag is null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIsNotNull() {
            addCriterion("delete_Flag is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagEqualTo(String value) {
            addCriterion("delete_Flag =", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotEqualTo(String value) {
            addCriterion("delete_Flag <>", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThan(String value) {
            addCriterion("delete_Flag >", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThanOrEqualTo(String value) {
            addCriterion("delete_Flag >=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThan(String value) {
            addCriterion("delete_Flag <", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThanOrEqualTo(String value) {
            addCriterion("delete_Flag <=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLike(String value) {
            addCriterion("delete_Flag like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotLike(String value) {
            addCriterion("delete_Flag not like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIn(List<String> values) {
            addCriterion("delete_Flag in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotIn(List<String> values) {
            addCriterion("delete_Flag not in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagBetween(String value1, String value2) {
            addCriterion("delete_Flag between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotBetween(String value1, String value2) {
            addCriterion("delete_Flag not between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }
        
				
        public Criteria andTtffIsNull() {
            addCriterion("ttff is null");
            return (Criteria) this;
        }

        public Criteria andTtffIsNotNull() {
            addCriterion("ttff is not null");
            return (Criteria) this;
        }

        public Criteria andTtffEqualTo(String value) {
            addCriterion("ttff =", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffNotEqualTo(String value) {
            addCriterion("ttff <>", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffGreaterThan(String value) {
            addCriterion("ttff >", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffGreaterThanOrEqualTo(String value) {
            addCriterion("ttff >=", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffLessThan(String value) {
            addCriterion("ttff <", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffLessThanOrEqualTo(String value) {
            addCriterion("ttff <=", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffLike(String value) {
            addCriterion("ttff like", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffNotLike(String value) {
            addCriterion("ttff not like", value, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffIn(List<String> values) {
            addCriterion("ttff in", values, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffNotIn(List<String> values) {
            addCriterion("ttff not in", values, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffBetween(String value1, String value2) {
            addCriterion("ttff between", value1, value2, "ttff");
            return (Criteria) this;
        }

        public Criteria andTtffNotBetween(String value1, String value2) {
            addCriterion("ttff not between", value1, value2, "ttff");
            return (Criteria) this;
        }
        
				
        public Criteria andTtxxIsNull() {
            addCriterion("ttxx is null");
            return (Criteria) this;
        }

        public Criteria andTtxxIsNotNull() {
            addCriterion("ttxx is not null");
            return (Criteria) this;
        }

        public Criteria andTtxxEqualTo(String value) {
            addCriterion("ttxx =", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxNotEqualTo(String value) {
            addCriterion("ttxx <>", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxGreaterThan(String value) {
            addCriterion("ttxx >", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxGreaterThanOrEqualTo(String value) {
            addCriterion("ttxx >=", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxLessThan(String value) {
            addCriterion("ttxx <", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxLessThanOrEqualTo(String value) {
            addCriterion("ttxx <=", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxLike(String value) {
            addCriterion("ttxx like", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxNotLike(String value) {
            addCriterion("ttxx not like", value, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxIn(List<String> values) {
            addCriterion("ttxx in", values, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxNotIn(List<String> values) {
            addCriterion("ttxx not in", values, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxBetween(String value1, String value2) {
            addCriterion("ttxx between", value1, value2, "ttxx");
            return (Criteria) this;
        }

        public Criteria andTtxxNotBetween(String value1, String value2) {
            addCriterion("ttxx not between", value1, value2, "ttxx");
            return (Criteria) this;
        }
        
				
        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(Date value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(Date value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCreateUserIsNull() {
            addCriterion("create_user is null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNotNull() {
            addCriterion("create_user is not null");
            return (Criteria) this;
        }

        public Criteria andCreateUserEqualTo(Long value) {
            addCriterion("create_user =", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotEqualTo(Long value) {
            addCriterion("create_user <>", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThan(Long value) {
            addCriterion("create_user >", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThanOrEqualTo(Long value) {
            addCriterion("create_user >=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThan(Long value) {
            addCriterion("create_user <", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThanOrEqualTo(Long value) {
            addCriterion("create_user <=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLike(Long value) {
            addCriterion("create_user like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotLike(Long value) {
            addCriterion("create_user not like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserIn(List<Long> values) {
            addCriterion("create_user in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotIn(List<Long> values) {
            addCriterion("create_user not in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserBetween(Long value1, Long value2) {
            addCriterion("create_user between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotBetween(Long value1, Long value2) {
            addCriterion("create_user not between", value1, value2, "createUser");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(JshMaterialCarfprice record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getMaterialId()!=null&&StrUtil.isNotEmpty(record.getMaterialId().toString())) {
    			 list.add("ifnull(material_id,'')");
    		}
			if(record.getOrderCode()!=null&&StrUtil.isNotEmpty(record.getOrderCode().toString())) {
    			 list.add("ifnull(order_code,'')");
    		}
			if(record.getSpecs()!=null&&StrUtil.isNotEmpty(record.getSpecs().toString())) {
    			 list.add("ifnull(specs,'')");
    		}
			if(record.getCarftId()!=null&&StrUtil.isNotEmpty(record.getCarftId().toString())) {
    			 list.add("ifnull(carft_id,'')");
    		}
			if(record.getPurchaseDecimal()!=null&&StrUtil.isNotEmpty(record.getPurchaseDecimal().toString())) {
    			 list.add("ifnull(purchase_decimal,'')");
    		}
			if(record.getLowDecimal()!=null&&StrUtil.isNotEmpty(record.getLowDecimal().toString())) {
    			 list.add("ifnull(low_decimal,'')");
    		}
			if(record.getTenantId()!=null&&StrUtil.isNotEmpty(record.getTenantId().toString())) {
    			 list.add("ifnull(tenant_id,'')");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			 list.add("ifnull(delete_Flag,'')");
    		}
			if(record.getTtff()!=null&&StrUtil.isNotEmpty(record.getTtff().toString())) {
    			 list.add("ifnull(ttff,'')");
    		}
			if(record.getTtxx()!=null&&StrUtil.isNotEmpty(record.getTtxx().toString())) {
    			 list.add("ifnull(ttxx,'')");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			 list.add("ifnull(create_time,'')");
    		}
			if(record.getCreateUser()!=null&&StrUtil.isNotEmpty(record.getCreateUser().toString())) {
    			 list.add("ifnull(create_user,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getMaterialId()!=null&&StrUtil.isNotEmpty(record.getMaterialId().toString())) {
    			list2.add("'%"+record.getMaterialId()+"%'");
    		}
			if(record.getOrderCode()!=null&&StrUtil.isNotEmpty(record.getOrderCode().toString())) {
    			list2.add("'%"+record.getOrderCode()+"%'");
    		}
			if(record.getSpecs()!=null&&StrUtil.isNotEmpty(record.getSpecs().toString())) {
    			list2.add("'%"+record.getSpecs()+"%'");
    		}
			if(record.getCarftId()!=null&&StrUtil.isNotEmpty(record.getCarftId().toString())) {
    			list2.add("'%"+record.getCarftId()+"%'");
    		}
			if(record.getPurchaseDecimal()!=null&&StrUtil.isNotEmpty(record.getPurchaseDecimal().toString())) {
    			list2.add("'%"+record.getPurchaseDecimal()+"%'");
    		}
			if(record.getLowDecimal()!=null&&StrUtil.isNotEmpty(record.getLowDecimal().toString())) {
    			list2.add("'%"+record.getLowDecimal()+"%'");
    		}
			if(record.getTenantId()!=null&&StrUtil.isNotEmpty(record.getTenantId().toString())) {
    			list2.add("'%"+record.getTenantId()+"%'");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			list2.add("'%"+record.getDeleteFlag()+"%'");
    		}
			if(record.getTtff()!=null&&StrUtil.isNotEmpty(record.getTtff().toString())) {
    			list2.add("'%"+record.getTtff()+"%'");
    		}
			if(record.getTtxx()!=null&&StrUtil.isNotEmpty(record.getTtxx().toString())) {
    			list2.add("'%"+record.getTtxx()+"%'");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			list2.add("'%"+record.getCreateTime()+"%'");
    		}
			if(record.getCreateUser()!=null&&StrUtil.isNotEmpty(record.getCreateUser().toString())) {
    			list2.add("'%"+record.getCreateUser()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(material_id,'')");
    		list.add("ifnull(order_code,'')");
    		list.add("ifnull(specs,'')");
    		list.add("ifnull(carft_id,'')");
    		list.add("ifnull(purchase_decimal,'')");
    		list.add("ifnull(low_decimal,'')");
    		list.add("ifnull(tenant_id,'')");
    		list.add("ifnull(delete_Flag,'')");
    		list.add("ifnull(ttff,'')");
    		list.add("ifnull(ttxx,'')");
    		list.add("ifnull(create_time,'')");
    		list.add("ifnull(create_user,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}