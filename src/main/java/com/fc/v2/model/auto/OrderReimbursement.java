package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;
import java.util.List;

public class OrderReimbursement implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	@ApiModelProperty(value = "报销单号")
	private String orderNo;
	@ApiModelProperty(value = "报销金额")
	private String reiMoney;
	
	@ApiModelProperty(value = "报销类别")
	private String reiType;
	
	@ApiModelProperty(value = "收款账号")
	private String collectionUser;
	
	@ApiModelProperty(value = "图片")
	private String photo;
	
	@ApiModelProperty(value = "附件")
	private String enclosure;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	
	@ApiModelProperty(value = "1：默认正常数据；-1表示数据被删除")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	
	@ApiModelProperty(value = "")
	private String str1;
	
	@ApiModelProperty(value = "")
	private String str2;
	
	@ApiModelProperty(value = "")
	private String str3;
	
	@ApiModelProperty(value = "")
	private Long num1;
	
	@ApiModelProperty(value = "")
	private Integer num2;

	private List<OrderReimbursemendetail> detailList;

	public List<OrderReimbursemendetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<OrderReimbursemendetail> detailList) {
		this.detailList = detailList;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("reiMoney")
	public String getReiMoney() {
		return reiMoney;
	}

	public void setReiMoney(String reiMoney) {
		this.reiMoney =  reiMoney;
	}
	@JsonProperty("reiType")
	public String getReiType() {
		return reiType;
	}

	public void setReiType(String reiType) {
		this.reiType =  reiType;
	}

	@JsonProperty("collectionUser")
	public String getCollectionUser() {
		return collectionUser;
	}

	public void setCollectionUser(String collectionUser) {
		this.collectionUser =  collectionUser;
	}
	@JsonProperty("photo")
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo =  photo;
	}
	@JsonProperty("enclosure")
	public String getEnclosure() {
		return enclosure;
	}

	public void setEnclosure(String enclosure) {
		this.enclosure =  enclosure;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	@JsonProperty("str1")
	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 =  str1;
	}
	@JsonProperty("str2")
	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 =  str2;
	}
	@JsonProperty("str3")
	public String getStr3() {
		return str3;
	}

	public void setStr3(String str3) {
		this.str3 =  str3;
	}
	@JsonProperty("num1")
	public Long getNum1() {
		return num1;
	}

	public void setNum1(Long num1) {
		this.num1 =  num1;
	}
	@JsonProperty("num2")
	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 =  num2;
	}

																																						
	public OrderReimbursement(Long sid,String reiMoney,String reiType,String collectionUser,String photo,String enclosure,Date createdTime,Long createdUserSid,Date modifiedTime,Long modifiedUserSid,Integer x,Date xTime,Long xUserSid,String str1,String str2,String str3,Long num1,Integer num2) {
				
		this.sid = sid;
				
		this.reiMoney = reiMoney;
				
		this.reiType = reiType;
				
		this.collectionUser = collectionUser;
				
		this.photo = photo;
				
		this.enclosure = enclosure;
				
		this.createdTime = createdTime;
				
		this.createdUserSid = createdUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.str1 = str1;
				
		this.str2 = str2;
				
		this.str3 = str3;
				
		this.num1 = num1;
				
		this.num2 = num2;
				
	}

	public OrderReimbursement() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private String createName;

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

}