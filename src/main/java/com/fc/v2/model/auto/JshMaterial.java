package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;
import java.util.List;


public class JshMaterial implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "产品类型")
	private Long categoryId;
	@ApiModelProperty(value = "类型名称")
	private String categoryName;
	
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@ApiModelProperty(value = "名称")
	private String name;
	
	@ApiModelProperty(value = "制造商")
	private String mfrs;
	
	@ApiModelProperty(value = "包装（KG/包）")
	private BigDecimal packing;
	
	@ApiModelProperty(value = "安全存量（KG）")
	private BigDecimal safetyStock;
	
	@ApiModelProperty(value = "产品编号")
	private String model;
	
	@ApiModelProperty(value = "规格")
	private String standard;
	
	@ApiModelProperty(value = "颜色")
	private String color;
	
	@ApiModelProperty(value = "单位-单个")
	private String unit;
	
	@ApiModelProperty(value = "备注")
	private String remark;
	
	@ApiModelProperty(value = "零售价")
	private BigDecimal retailPrice;
	
	@ApiModelProperty(value = "最低售价")
	private BigDecimal lowPrice;
	
	@ApiModelProperty(value = "预设售价一")
	private BigDecimal presetPriceOne;
	
	@ApiModelProperty(value = "预设售价二")
	private BigDecimal presetPriceTwo;
	
	@ApiModelProperty(value = "计量单位Id")
	private Long unitId;
	
	@ApiModelProperty(value = "首选出库单位")
	private String firstOutUnit;
	
	@ApiModelProperty(value = "首选入库单位")
	private String firstInUnit;
	
	@ApiModelProperty(value = "价格策略")
	private String priceStrategy;
	
	@ApiModelProperty(value = "启用 0-禁用  1-启用")
	private Byte enabled;
	
	@ApiModelProperty(value = "材料")
	private String otherField1;
	
	@ApiModelProperty(value = "材料名称")
	private String otherField2;
	
	@ApiModelProperty(value = "材料规格")
	private String otherField3;
	
	@ApiModelProperty(value = "是否开启序列号，0否，1是")
	private String enableSerialNumber;
	
	@ApiModelProperty(value = "租户id")
	private Long tenantId;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private String deleteFlag;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("categoryId")
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId =  categoryId;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name =  name;
	}
	@JsonProperty("mfrs")
	public String getMfrs() {
		return mfrs;
	}

	public void setMfrs(String mfrs) {
		this.mfrs =  mfrs;
	}
	@JsonProperty("packing")
	public BigDecimal getPacking() {
		return packing;
	}

	public void setPacking(BigDecimal packing) {
		this.packing =  packing;
	}
	@JsonProperty("safetyStock")
	public BigDecimal getSafetyStock() {
		return safetyStock;
	}

	public void setSafetyStock(BigDecimal safetyStock) {
		this.safetyStock =  safetyStock;
	}
	@JsonProperty("model")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model =  model;
	}
	@JsonProperty("standard")
	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard =  standard;
	}
	@JsonProperty("color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color =  color;
	}
	@JsonProperty("unit")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit =  unit;
	}
	@JsonProperty("remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark =  remark;
	}
	@JsonProperty("retailPrice")
	public BigDecimal getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice =  retailPrice;
	}
	@JsonProperty("lowPrice")
	public BigDecimal getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(BigDecimal lowPrice) {
		this.lowPrice =  lowPrice;
	}
	@JsonProperty("presetPriceOne")
	public BigDecimal getPresetPriceOne() {
		return presetPriceOne;
	}

	public void setPresetPriceOne(BigDecimal presetPriceOne) {
		this.presetPriceOne =  presetPriceOne;
	}
	@JsonProperty("presetPriceTwo")
	public BigDecimal getPresetPriceTwo() {
		return presetPriceTwo;
	}

	public void setPresetPriceTwo(BigDecimal presetPriceTwo) {
		this.presetPriceTwo =  presetPriceTwo;
	}
	@JsonProperty("unitId")
	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId =  unitId;
	}
	@JsonProperty("firstOutUnit")
	public String getFirstOutUnit() {
		return firstOutUnit;
	}

	public void setFirstOutUnit(String firstOutUnit) {
		this.firstOutUnit =  firstOutUnit;
	}
	@JsonProperty("firstInUnit")
	public String getFirstInUnit() {
		return firstInUnit;
	}

	public void setFirstInUnit(String firstInUnit) {
		this.firstInUnit =  firstInUnit;
	}
	@JsonProperty("priceStrategy")
	public String getPriceStrategy() {
		return priceStrategy;
	}

	public void setPriceStrategy(String priceStrategy) {
		this.priceStrategy =  priceStrategy;
	}
	@JsonProperty("enabled")
	public Byte getEnabled() {
		return enabled;
	}

	public void setEnabled(Byte enabled) {
		this.enabled =  enabled;
	}
	@JsonProperty("otherField1")
	public String getOtherField1() {
		return otherField1;
	}

	public void setOtherField1(String otherField1) {
		this.otherField1 =  otherField1;
	}
	@JsonProperty("otherField2")
	public String getOtherField2() {
		return otherField2;
	}

	public void setOtherField2(String otherField2) {
		this.otherField2 =  otherField2;
	}
	@JsonProperty("otherField3")
	public String getOtherField3() {
		return otherField3;
	}

	public void setOtherField3(String otherField3) {
		this.otherField3 =  otherField3;
	}
	@JsonProperty("enableSerialNumber")
	public String getEnableSerialNumber() {
		return enableSerialNumber;
	}

	public void setEnableSerialNumber(String enableSerialNumber) {
		this.enableSerialNumber =  enableSerialNumber;
	}
	@JsonProperty("tenantId")
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId =  tenantId;
	}
	@JsonProperty("deleteFlag")
	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}

																																																				
	public JshMaterial(Long id,Long categoryId,String name,String mfrs,BigDecimal packing,BigDecimal safetyStock,String model,String standard,String color,String unit,String remark,BigDecimal retailPrice,BigDecimal lowPrice,BigDecimal presetPriceOne,BigDecimal presetPriceTwo,Long unitId,String firstOutUnit,String firstInUnit,String priceStrategy,Byte enabled,String otherField1,String otherField2,String otherField3,String enableSerialNumber,Long tenantId,String deleteFlag) {
				
		this.id = id;
				
		this.categoryId = categoryId;
				
		this.name = name;
				
		this.mfrs = mfrs;
				
		this.packing = packing;
				
		this.safetyStock = safetyStock;
				
		this.model = model;
				
		this.standard = standard;
				
		this.color = color;
				
		this.unit = unit;
				
		this.remark = remark;
				
		this.retailPrice = retailPrice;
				
		this.lowPrice = lowPrice;
				
		this.presetPriceOne = presetPriceOne;
				
		this.presetPriceTwo = presetPriceTwo;
				
		this.unitId = unitId;
				
		this.firstOutUnit = firstOutUnit;
				
		this.firstInUnit = firstInUnit;
				
		this.priceStrategy = priceStrategy;
				
		this.enabled = enabled;
				
		this.otherField1 = otherField1;
				
		this.otherField2 = otherField2;
				
		this.otherField3 = otherField3;
				
		this.enableSerialNumber = enableSerialNumber;
				
		this.tenantId = tenantId;
				
		this.deleteFlag = deleteFlag;
				
	}

	public JshMaterial() {
	    super();
	}
	 private List<JshMaterialCarfprice> jshMaterialCarfpriceList;
	 private List<PlanOut> planoutList;
	 private List<String> jshMaterialCarfpriceNameList;
	 private List<String> planoutNameList;

	public List<String> getJshMaterialCarfpriceNameList() {
		return jshMaterialCarfpriceNameList;
	}

	public void setJshMaterialCarfpriceNameList(List<String> jshMaterialCarfpriceNameList) {
		this.jshMaterialCarfpriceNameList = jshMaterialCarfpriceNameList;
	}

	public List<String> getPlanoutNameList() {
		return planoutNameList;
	}

	public void setPlanoutNameList(List<String> planoutNameList) {
		this.planoutNameList = planoutNameList;
	}

	public List<JshMaterialCarfprice> getJshMaterialCarfpriceList() {
		return jshMaterialCarfpriceList;
	}

	public void setJshMaterialCarfpriceList(List<JshMaterialCarfprice> jshMaterialCarfpriceList) {
		this.jshMaterialCarfpriceList = jshMaterialCarfpriceList;
	}

	public List<PlanOut> getPlanoutList() {
		return planoutList;
	}

	public void setPlanoutList(List<PlanOut> planoutList) {
		this.planoutList = planoutList;
	}
	 

}