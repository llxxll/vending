package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;
import java.util.List;

public class PlanOudetail implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "计划编号")
	private String produceNo;
	
	@ApiModelProperty(value = "物料号")
	private String materiaNo;
	
	@ApiModelProperty(value = "物料名称")
	private String materialName;
	
	@ApiModelProperty(value = "材料ID")
	private String standard;
	
	@ApiModelProperty(value = "材料库存")
	private Long stock;
	
	@ApiModelProperty(value = "备注")
	private String material;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "生产时间")
	private Date produceTime;
	
	@ApiModelProperty(value = "数量")
	private Integer count;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@ApiModelProperty(value = "状态0待领料1已领料")
	private String field1;
	
	@ApiModelProperty(value = "删除字段0删除1正常")
	private String field2;
	
	@ApiModelProperty(value = "物料重量")
	private String field3;
	
	@ApiModelProperty(value = "")
	private String departmentOne;
	
	@ApiModelProperty(value = "材料规格")
	private String departmentTwo;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("produceNo")
	public String getProduceNo() {
		return produceNo;
	}

	public void setProduceNo(String produceNo) {
		this.produceNo =  produceNo;
	}
	@JsonProperty("materiaNo")
	public String getMateriaNo() {
		return materiaNo;
	}

	public void setMateriaNo(String materiaNo) {
		this.materiaNo =  materiaNo;
	}
	@JsonProperty("materialName")
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName =  materialName;
	}
	@JsonProperty("standard")
	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard =  standard;
	}
	@JsonProperty("stock")
	public Long getStock() {
		return stock;
	}

	public void setStock(Long stock) {
		this.stock =  stock;
	}
	@JsonProperty("material")
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material =  material;
	}
	@JsonProperty("produceTime")
	public Date getProduceTime() {
		return produceTime;
	}

	public void setProduceTime(Date produceTime) {
		this.produceTime =  produceTime;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 =  field1;
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 =  field2;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}
	@JsonProperty("departmentOne")
	public String getDepartmentOne() {
		return departmentOne;
	}

	public void setDepartmentOne(String departmentOne) {
		this.departmentOne =  departmentOne;
	}
	@JsonProperty("departmentTwo")
	public String getDepartmentTwo() {
		return departmentTwo;
	}

	public void setDepartmentTwo(String departmentTwo) {
		this.departmentTwo =  departmentTwo;
	}

																																				
	public PlanOudetail(Long id,String produceNo,String materiaNo,String materialName,String standard,Long stock,String material,Date produceTime,Integer count,Date createTime,Long creator,Date updateTime,Long updater,String field1,String field2,String field3,String departmentOne,String departmentTwo) {
				
		this.id = id;
				
		this.produceNo = produceNo;
				
		this.materiaNo = materiaNo;
				
		this.materialName = materialName;
				
		this.standard = standard;
				
		this.stock = stock;
				
		this.material = material;
				
		this.produceTime = produceTime;
				
		this.count = count;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.departmentOne = departmentOne;
				
		this.departmentTwo = departmentTwo;
				
	}

	public PlanOudetail() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private String createrName;
	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
	private String tuName;//物料编号
	private String tuNo;//物料描述
	private String clName;//材料名称
	private String clProperty;//材料规格
	private String beginTime;//材料规格
	private String endTime;//材料规格
	private String houseNo;//材料规格
	private String jshcarftId;//材料规格
	private Integer workID;//工人id
	private Integer lingliaoCount;//领料数
	private Integer zuidaCount;//最大开单数
	private String departmentOneName;//材料名称
	private Integer count5;//已开单
	private Integer count4;//送货数
	private Integer count6;//待入库数
	private Integer count1;//已入库数
	private Integer count2;//不合格数
	private Integer count3;//退料数
	private Integer songhuocount;
	private Integer zaitucount;
	private Integer zaitucountqu;
	private Integer zaitucountzong;
	

	

	public Integer getZaitucountzong() {
		return zaitucountzong;
	}

	public void setZaitucountzong(Integer zaitucountzong) {
		this.zaitucountzong = zaitucountzong;
	}

	public Integer getZaitucountqu() {
		return zaitucountqu;
	}

	public void setZaitucountqu(Integer zaitucountqu) {
		this.zaitucountqu = zaitucountqu;
	}

	public Integer getZaitucount() {
		return zaitucount;
	}

	public void setZaitucount(Integer zaitucount) {
		this.zaitucount = zaitucount;
	}

	public Integer getSonghuocount() {
		return songhuocount;
	}

	public void setSonghuocount(Integer songhuocount) {
		this.songhuocount = songhuocount;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getCount5() {
		return count5;
	}

	public void setCount5(Integer count5) {
		this.count5 = count5;
	}

	public Integer getCount4() {
		return count4;
	}

	public void setCount4(Integer count4) {
		this.count4 = count4;
	}

	public Integer getCount6() {
		return count6;
	}

	public void setCount6(Integer count6) {
		this.count6 = count6;
	}

	public Integer getCount1() {
		return count1;
	}

	public void setCount1(Integer count1) {
		this.count1 = count1;
	}

	public Integer getCount2() {
		return count2;
	}

	public void setCount2(Integer count2) {
		this.count2 = count2;
	}

	public Integer getCount3() {
		return count3;
	}

	public void setCount3(Integer count3) {
		this.count3 = count3;
	}

	public String getDepartmentOneName() {
		return departmentOneName;
	}
	public void setDepartmentOneName(String departmentOneName) {
		this.departmentOneName = departmentOneName;
	}
	
	

	public String getTuName() {
		return tuName;
	}

	public void setTuName(String tuName) {
		this.tuName = tuName;
	}

	public String getTuNo() {
		return tuNo;
	}

	public void setTuNo(String tuNo) {
		this.tuNo = tuNo;
	}

	public String getClName() {
		return clName;
	}

	public void setClName(String clName) {
		this.clName = clName;
	}

	public String getClProperty() {
		return clProperty;
	}

	public void setClProperty(String clProperty) {
		this.clProperty = clProperty;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getJshcarftId() {
		return jshcarftId;
	}

	public void setJshcarftId(String jshcarftId) {
		this.jshcarftId = jshcarftId;
	}

	public Integer getWorkID() {
		return workID;
	}

	public void setWorkID(Integer workID) {
		this.workID = workID;
	}

	public Integer getLingliaoCount() {
		return lingliaoCount;
	}

	public void setLingliaoCount(Integer lingliaoCount) {
		this.lingliaoCount = lingliaoCount;
	}

	public Integer getZuidaCount() {
		return zuidaCount;
	}

	public void setZuidaCount(Integer zuidaCount) {
		this.zuidaCount = zuidaCount;
	}

	private List<PlanOudetailGongying> planOudetailGongyingList;

	public List<PlanOudetailGongying> getPlanOudetailGongyingList() {
		return planOudetailGongyingList;
	}

	public void setPlanOudetailGongyingList(List<PlanOudetailGongying> planOudetailGongyingList) {
		this.planOudetailGongyingList = planOudetailGongyingList;
	}
	
	
}