package com.fc.v2.model.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 销售-订单明细 OmsOrderDetailExample
 * @author fuce_自动生成
 * @date 2022-05-21 23:28:42
 */
public class OmsOrderDetailExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OmsOrderDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andSidIsNull() {
            addCriterion("sid is null");
            return (Criteria) this;
        }

        public Criteria andSidIsNotNull() {
            addCriterion("sid is not null");
            return (Criteria) this;
        }

        public Criteria andSidEqualTo(Long value) {
            addCriterion("sid =", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotEqualTo(Long value) {
            addCriterion("sid <>", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThan(Long value) {
            addCriterion("sid >", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThanOrEqualTo(Long value) {
            addCriterion("sid >=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThan(Long value) {
            addCriterion("sid <", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThanOrEqualTo(Long value) {
            addCriterion("sid <=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLike(Long value) {
            addCriterion("sid like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotLike(Long value) {
            addCriterion("sid not like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidIn(List<Long> values) {
            addCriterion("sid in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotIn(List<Long> values) {
            addCriterion("sid not in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidBetween(Long value1, Long value2) {
            addCriterion("sid between", value1, value2, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotBetween(Long value1, Long value2) {
            addCriterion("sid not between", value1, value2, "sid");
            return (Criteria) this;
        }
        
				
        public Criteria andItemNoIsNull() {
            addCriterion("item_no is null");
            return (Criteria) this;
        }

        public Criteria andItemNoIsNotNull() {
            addCriterion("item_no is not null");
            return (Criteria) this;
        }

        public Criteria andItemNoEqualTo(String value) {
            addCriterion("item_no =", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoNotEqualTo(String value) {
            addCriterion("item_no <>", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoGreaterThan(String value) {
            addCriterion("item_no >", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoGreaterThanOrEqualTo(String value) {
            addCriterion("item_no >=", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoLessThan(String value) {
            addCriterion("item_no <", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoLessThanOrEqualTo(String value) {
            addCriterion("item_no <=", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoLike(String value) {
            addCriterion("item_no like", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoNotLike(String value) {
            addCriterion("item_no not like", value, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoIn(List<String> values) {
            addCriterion("item_no in", values, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoNotIn(List<String> values) {
            addCriterion("item_no not in", values, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoBetween(String value1, String value2) {
            addCriterion("item_no between", value1, value2, "itemNo");
            return (Criteria) this;
        }

        public Criteria andItemNoNotBetween(String value1, String value2) {
            addCriterion("item_no not between", value1, value2, "itemNo");
            return (Criteria) this;
        }
        
				
        public Criteria andOrderNoIsNull() {
            addCriterion("order_no is null");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNotNull() {
            addCriterion("order_no is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNoEqualTo(String value) {
            addCriterion("order_no =", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotEqualTo(String value) {
            addCriterion("order_no <>", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThan(String value) {
            addCriterion("order_no >", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("order_no >=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThan(String value) {
            addCriterion("order_no <", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThanOrEqualTo(String value) {
            addCriterion("order_no <=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLike(String value) {
            addCriterion("order_no like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotLike(String value) {
            addCriterion("order_no not like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoIn(List<String> values) {
            addCriterion("order_no in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotIn(List<String> values) {
            addCriterion("order_no not in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoBetween(String value1, String value2) {
            addCriterion("order_no between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotBetween(String value1, String value2) {
            addCriterion("order_no not between", value1, value2, "orderNo");
            return (Criteria) this;
        }
        
				
        public Criteria andProductDetailSidIsNull() {
            addCriterion("product_detail_sid is null");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidIsNotNull() {
            addCriterion("product_detail_sid is not null");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidEqualTo(Long value) {
            addCriterion("product_detail_sid =", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidNotEqualTo(Long value) {
            addCriterion("product_detail_sid <>", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidGreaterThan(Long value) {
            addCriterion("product_detail_sid >", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidGreaterThanOrEqualTo(Long value) {
            addCriterion("product_detail_sid >=", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidLessThan(Long value) {
            addCriterion("product_detail_sid <", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidLessThanOrEqualTo(Long value) {
            addCriterion("product_detail_sid <=", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidLike(Long value) {
            addCriterion("product_detail_sid like", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidNotLike(Long value) {
            addCriterion("product_detail_sid not like", value, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidIn(List<Long> values) {
            addCriterion("product_detail_sid in", values, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidNotIn(List<Long> values) {
            addCriterion("product_detail_sid not in", values, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidBetween(Long value1, Long value2) {
            addCriterion("product_detail_sid between", value1, value2, "productDetailSid");
            return (Criteria) this;
        }

        public Criteria andProductDetailSidNotBetween(Long value1, Long value2) {
            addCriterion("product_detail_sid not between", value1, value2, "productDetailSid");
            return (Criteria) this;
        }
        
				
        public Criteria andProductSidIsNull() {
            addCriterion("product_sid is null");
            return (Criteria) this;
        }

        public Criteria andProductSidIsNotNull() {
            addCriterion("product_sid is not null");
            return (Criteria) this;
        }

        public Criteria andProductSidEqualTo(Long value) {
            addCriterion("product_sid =", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidNotEqualTo(Long value) {
            addCriterion("product_sid <>", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidGreaterThan(Long value) {
            addCriterion("product_sid >", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidGreaterThanOrEqualTo(Long value) {
            addCriterion("product_sid >=", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidLessThan(Long value) {
            addCriterion("product_sid <", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidLessThanOrEqualTo(Long value) {
            addCriterion("product_sid <=", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidLike(Long value) {
            addCriterion("product_sid like", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidNotLike(Long value) {
            addCriterion("product_sid not like", value, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidIn(List<Long> values) {
            addCriterion("product_sid in", values, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidNotIn(List<Long> values) {
            addCriterion("product_sid not in", values, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidBetween(Long value1, Long value2) {
            addCriterion("product_sid between", value1, value2, "productSid");
            return (Criteria) this;
        }

        public Criteria andProductSidNotBetween(Long value1, Long value2) {
            addCriterion("product_sid not between", value1, value2, "productSid");
            return (Criteria) this;
        }
        
				
        public Criteria andProductTitleIsNull() {
            addCriterion("product_title is null");
            return (Criteria) this;
        }

        public Criteria andProductTitleIsNotNull() {
            addCriterion("product_title is not null");
            return (Criteria) this;
        }

        public Criteria andProductTitleEqualTo(String value) {
            addCriterion("product_title =", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleNotEqualTo(String value) {
            addCriterion("product_title <>", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleGreaterThan(String value) {
            addCriterion("product_title >", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleGreaterThanOrEqualTo(String value) {
            addCriterion("product_title >=", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleLessThan(String value) {
            addCriterion("product_title <", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleLessThanOrEqualTo(String value) {
            addCriterion("product_title <=", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleLike(String value) {
            addCriterion("product_title like", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleNotLike(String value) {
            addCriterion("product_title not like", value, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleIn(List<String> values) {
            addCriterion("product_title in", values, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleNotIn(List<String> values) {
            addCriterion("product_title not in", values, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleBetween(String value1, String value2) {
            addCriterion("product_title between", value1, value2, "productTitle");
            return (Criteria) this;
        }

        public Criteria andProductTitleNotBetween(String value1, String value2) {
            addCriterion("product_title not between", value1, value2, "productTitle");
            return (Criteria) this;
        }
        
				
        public Criteria andProductAliasIsNull() {
            addCriterion("product_alias is null");
            return (Criteria) this;
        }

        public Criteria andProductAliasIsNotNull() {
            addCriterion("product_alias is not null");
            return (Criteria) this;
        }

        public Criteria andProductAliasEqualTo(String value) {
            addCriterion("product_alias =", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasNotEqualTo(String value) {
            addCriterion("product_alias <>", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasGreaterThan(String value) {
            addCriterion("product_alias >", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasGreaterThanOrEqualTo(String value) {
            addCriterion("product_alias >=", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasLessThan(String value) {
            addCriterion("product_alias <", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasLessThanOrEqualTo(String value) {
            addCriterion("product_alias <=", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasLike(String value) {
            addCriterion("product_alias like", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasNotLike(String value) {
            addCriterion("product_alias not like", value, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasIn(List<String> values) {
            addCriterion("product_alias in", values, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasNotIn(List<String> values) {
            addCriterion("product_alias not in", values, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasBetween(String value1, String value2) {
            addCriterion("product_alias between", value1, value2, "productAlias");
            return (Criteria) this;
        }

        public Criteria andProductAliasNotBetween(String value1, String value2) {
            addCriterion("product_alias not between", value1, value2, "productAlias");
            return (Criteria) this;
        }
        
				
        public Criteria andProductSpecificIsNull() {
            addCriterion("product_specific is null");
            return (Criteria) this;
        }

        public Criteria andProductSpecificIsNotNull() {
            addCriterion("product_specific is not null");
            return (Criteria) this;
        }

        public Criteria andProductSpecificEqualTo(String value) {
            addCriterion("product_specific =", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificNotEqualTo(String value) {
            addCriterion("product_specific <>", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificGreaterThan(String value) {
            addCriterion("product_specific >", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificGreaterThanOrEqualTo(String value) {
            addCriterion("product_specific >=", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificLessThan(String value) {
            addCriterion("product_specific <", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificLessThanOrEqualTo(String value) {
            addCriterion("product_specific <=", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificLike(String value) {
            addCriterion("product_specific like", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificNotLike(String value) {
            addCriterion("product_specific not like", value, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificIn(List<String> values) {
            addCriterion("product_specific in", values, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificNotIn(List<String> values) {
            addCriterion("product_specific not in", values, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificBetween(String value1, String value2) {
            addCriterion("product_specific between", value1, value2, "productSpecific");
            return (Criteria) this;
        }

        public Criteria andProductSpecificNotBetween(String value1, String value2) {
            addCriterion("product_specific not between", value1, value2, "productSpecific");
            return (Criteria) this;
        }
        
				
        public Criteria andUnitNameIsNull() {
            addCriterion("unit_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitNameIsNotNull() {
            addCriterion("unit_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitNameEqualTo(String value) {
            addCriterion("unit_name =", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotEqualTo(String value) {
            addCriterion("unit_name <>", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameGreaterThan(String value) {
            addCriterion("unit_name >", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_name >=", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameLessThan(String value) {
            addCriterion("unit_name <", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameLessThanOrEqualTo(String value) {
            addCriterion("unit_name <=", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameLike(String value) {
            addCriterion("unit_name like", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotLike(String value) {
            addCriterion("unit_name not like", value, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameIn(List<String> values) {
            addCriterion("unit_name in", values, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotIn(List<String> values) {
            addCriterion("unit_name not in", values, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameBetween(String value1, String value2) {
            addCriterion("unit_name between", value1, value2, "unitName");
            return (Criteria) this;
        }

        public Criteria andUnitNameNotBetween(String value1, String value2) {
            addCriterion("unit_name not between", value1, value2, "unitName");
            return (Criteria) this;
        }
        
				
        public Criteria andXiaoshouPriceIsNull() {
            addCriterion("xiaoshou_price is null");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceIsNotNull() {
            addCriterion("xiaoshou_price is not null");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceEqualTo(BigDecimal value) {
            addCriterion("xiaoshou_price =", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceNotEqualTo(BigDecimal value) {
            addCriterion("xiaoshou_price <>", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceGreaterThan(BigDecimal value) {
            addCriterion("xiaoshou_price >", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("xiaoshou_price >=", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceLessThan(BigDecimal value) {
            addCriterion("xiaoshou_price <", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("xiaoshou_price <=", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceLike(BigDecimal value) {
            addCriterion("xiaoshou_price like", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceNotLike(BigDecimal value) {
            addCriterion("xiaoshou_price not like", value, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceIn(List<BigDecimal> values) {
            addCriterion("xiaoshou_price in", values, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceNotIn(List<BigDecimal> values) {
            addCriterion("xiaoshou_price not in", values, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("xiaoshou_price between", value1, value2, "xiaoshouPrice");
            return (Criteria) this;
        }

        public Criteria andXiaoshouPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("xiaoshou_price not between", value1, value2, "xiaoshouPrice");
            return (Criteria) this;
        }
        
				
        public Criteria andCategorySidIsNull() {
            addCriterion("category_sid is null");
            return (Criteria) this;
        }

        public Criteria andCategorySidIsNotNull() {
            addCriterion("category_sid is not null");
            return (Criteria) this;
        }

        public Criteria andCategorySidEqualTo(String value) {
            addCriterion("category_sid =", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidNotEqualTo(String value) {
            addCriterion("category_sid <>", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidGreaterThan(String value) {
            addCriterion("category_sid >", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidGreaterThanOrEqualTo(String value) {
            addCriterion("category_sid >=", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidLessThan(String value) {
            addCriterion("category_sid <", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidLessThanOrEqualTo(String value) {
            addCriterion("category_sid <=", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidLike(String value) {
            addCriterion("category_sid like", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidNotLike(String value) {
            addCriterion("category_sid not like", value, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidIn(List<String> values) {
            addCriterion("category_sid in", values, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidNotIn(List<String> values) {
            addCriterion("category_sid not in", values, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidBetween(String value1, String value2) {
            addCriterion("category_sid between", value1, value2, "categorySid");
            return (Criteria) this;
        }

        public Criteria andCategorySidNotBetween(String value1, String value2) {
            addCriterion("category_sid not between", value1, value2, "categorySid");
            return (Criteria) this;
        }
        
				
        public Criteria andSupplySidIsNull() {
            addCriterion("supply_sid is null");
            return (Criteria) this;
        }

        public Criteria andSupplySidIsNotNull() {
            addCriterion("supply_sid is not null");
            return (Criteria) this;
        }

        public Criteria andSupplySidEqualTo(Long value) {
            addCriterion("supply_sid =", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidNotEqualTo(Long value) {
            addCriterion("supply_sid <>", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidGreaterThan(Long value) {
            addCriterion("supply_sid >", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidGreaterThanOrEqualTo(Long value) {
            addCriterion("supply_sid >=", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidLessThan(Long value) {
            addCriterion("supply_sid <", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidLessThanOrEqualTo(Long value) {
            addCriterion("supply_sid <=", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidLike(Long value) {
            addCriterion("supply_sid like", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidNotLike(Long value) {
            addCriterion("supply_sid not like", value, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidIn(List<Long> values) {
            addCriterion("supply_sid in", values, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidNotIn(List<Long> values) {
            addCriterion("supply_sid not in", values, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidBetween(Long value1, Long value2) {
            addCriterion("supply_sid between", value1, value2, "supplySid");
            return (Criteria) this;
        }

        public Criteria andSupplySidNotBetween(Long value1, Long value2) {
            addCriterion("supply_sid not between", value1, value2, "supplySid");
            return (Criteria) this;
        }
        
				
        public Criteria andShopSidIsNull() {
            addCriterion("shop_sid is null");
            return (Criteria) this;
        }

        public Criteria andShopSidIsNotNull() {
            addCriterion("shop_sid is not null");
            return (Criteria) this;
        }

        public Criteria andShopSidEqualTo(Long value) {
            addCriterion("shop_sid =", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidNotEqualTo(Long value) {
            addCriterion("shop_sid <>", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidGreaterThan(Long value) {
            addCriterion("shop_sid >", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidGreaterThanOrEqualTo(Long value) {
            addCriterion("shop_sid >=", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidLessThan(Long value) {
            addCriterion("shop_sid <", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidLessThanOrEqualTo(Long value) {
            addCriterion("shop_sid <=", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidLike(Long value) {
            addCriterion("shop_sid like", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidNotLike(Long value) {
            addCriterion("shop_sid not like", value, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidIn(List<Long> values) {
            addCriterion("shop_sid in", values, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidNotIn(List<Long> values) {
            addCriterion("shop_sid not in", values, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidBetween(Long value1, Long value2) {
            addCriterion("shop_sid between", value1, value2, "shopSid");
            return (Criteria) this;
        }

        public Criteria andShopSidNotBetween(Long value1, Long value2) {
            addCriterion("shop_sid not between", value1, value2, "shopSid");
            return (Criteria) this;
        }
        
				
        public Criteria andStockTypeSidIsNull() {
            addCriterion("stock_type_sid is null");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidIsNotNull() {
            addCriterion("stock_type_sid is not null");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidEqualTo(Long value) {
            addCriterion("stock_type_sid =", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidNotEqualTo(Long value) {
            addCriterion("stock_type_sid <>", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidGreaterThan(Long value) {
            addCriterion("stock_type_sid >", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidGreaterThanOrEqualTo(Long value) {
            addCriterion("stock_type_sid >=", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidLessThan(Long value) {
            addCriterion("stock_type_sid <", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidLessThanOrEqualTo(Long value) {
            addCriterion("stock_type_sid <=", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidLike(Long value) {
            addCriterion("stock_type_sid like", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidNotLike(Long value) {
            addCriterion("stock_type_sid not like", value, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidIn(List<Long> values) {
            addCriterion("stock_type_sid in", values, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidNotIn(List<Long> values) {
            addCriterion("stock_type_sid not in", values, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidBetween(Long value1, Long value2) {
            addCriterion("stock_type_sid between", value1, value2, "stockTypeSid");
            return (Criteria) this;
        }

        public Criteria andStockTypeSidNotBetween(Long value1, Long value2) {
            addCriterion("stock_type_sid not between", value1, value2, "stockTypeSid");
            return (Criteria) this;
        }
        
				
        public Criteria andItemNumIsNull() {
            addCriterion("item_num is null");
            return (Criteria) this;
        }

        public Criteria andItemNumIsNotNull() {
            addCriterion("item_num is not null");
            return (Criteria) this;
        }

        public Criteria andItemNumEqualTo(Long value) {
            addCriterion("item_num =", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumNotEqualTo(Long value) {
            addCriterion("item_num <>", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumGreaterThan(Long value) {
            addCriterion("item_num >", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumGreaterThanOrEqualTo(Long value) {
            addCriterion("item_num >=", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumLessThan(Long value) {
            addCriterion("item_num <", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumLessThanOrEqualTo(Long value) {
            addCriterion("item_num <=", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumLike(Long value) {
            addCriterion("item_num like", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumNotLike(Long value) {
            addCriterion("item_num not like", value, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumIn(List<Long> values) {
            addCriterion("item_num in", values, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumNotIn(List<Long> values) {
            addCriterion("item_num not in", values, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumBetween(Long value1, Long value2) {
            addCriterion("item_num between", value1, value2, "itemNum");
            return (Criteria) this;
        }

        public Criteria andItemNumNotBetween(Long value1, Long value2) {
            addCriterion("item_num not between", value1, value2, "itemNum");
            return (Criteria) this;
        }
        
				
        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Long value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Long value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Long value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Long value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Long value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLike(Long value) {
            addCriterion("price like", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotLike(Long value) {
            addCriterion("price not like", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Long> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Long> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Long value1, Long value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Long value1, Long value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }
        
				
        public Criteria andRefundNumIsNull() {
            addCriterion("refund_num is null");
            return (Criteria) this;
        }

        public Criteria andRefundNumIsNotNull() {
            addCriterion("refund_num is not null");
            return (Criteria) this;
        }

        public Criteria andRefundNumEqualTo(Long value) {
            addCriterion("refund_num =", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumNotEqualTo(Long value) {
            addCriterion("refund_num <>", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumGreaterThan(Long value) {
            addCriterion("refund_num >", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumGreaterThanOrEqualTo(Long value) {
            addCriterion("refund_num >=", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumLessThan(Long value) {
            addCriterion("refund_num <", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumLessThanOrEqualTo(Long value) {
            addCriterion("refund_num <=", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumLike(Long value) {
            addCriterion("refund_num like", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumNotLike(Long value) {
            addCriterion("refund_num not like", value, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumIn(List<Long> values) {
            addCriterion("refund_num in", values, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumNotIn(List<Long> values) {
            addCriterion("refund_num not in", values, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumBetween(Long value1, Long value2) {
            addCriterion("refund_num between", value1, value2, "refundNum");
            return (Criteria) this;
        }

        public Criteria andRefundNumNotBetween(Long value1, Long value2) {
            addCriterion("refund_num not between", value1, value2, "refundNum");
            return (Criteria) this;
        }
        
				
        public Criteria andImgUrlIsNull() {
            addCriterion("img_url is null");
            return (Criteria) this;
        }

        public Criteria andImgUrlIsNotNull() {
            addCriterion("img_url is not null");
            return (Criteria) this;
        }

        public Criteria andImgUrlEqualTo(String value) {
            addCriterion("img_url =", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotEqualTo(String value) {
            addCriterion("img_url <>", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlGreaterThan(String value) {
            addCriterion("img_url >", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("img_url >=", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlLessThan(String value) {
            addCriterion("img_url <", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlLessThanOrEqualTo(String value) {
            addCriterion("img_url <=", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlLike(String value) {
            addCriterion("img_url like", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotLike(String value) {
            addCriterion("img_url not like", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlIn(List<String> values) {
            addCriterion("img_url in", values, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotIn(List<String> values) {
            addCriterion("img_url not in", values, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlBetween(String value1, String value2) {
            addCriterion("img_url between", value1, value2, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotBetween(String value1, String value2) {
            addCriterion("img_url not between", value1, value2, "imgUrl");
            return (Criteria) this;
        }
        
				
        public Criteria andStr1IsNull() {
            addCriterion("str_1 is null");
            return (Criteria) this;
        }

        public Criteria andStr1IsNotNull() {
            addCriterion("str_1 is not null");
            return (Criteria) this;
        }

        public Criteria andStr1EqualTo(String value) {
            addCriterion("str_1 =", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotEqualTo(String value) {
            addCriterion("str_1 <>", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThan(String value) {
            addCriterion("str_1 >", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThanOrEqualTo(String value) {
            addCriterion("str_1 >=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThan(String value) {
            addCriterion("str_1 <", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThanOrEqualTo(String value) {
            addCriterion("str_1 <=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Like(String value) {
            addCriterion("str_1 like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotLike(String value) {
            addCriterion("str_1 not like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1In(List<String> values) {
            addCriterion("str_1 in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotIn(List<String> values) {
            addCriterion("str_1 not in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Between(String value1, String value2) {
            addCriterion("str_1 between", value1, value2, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotBetween(String value1, String value2) {
            addCriterion("str_1 not between", value1, value2, "str1");
            return (Criteria) this;
        }
        
				
        public Criteria andStr2IsNull() {
            addCriterion("str_2 is null");
            return (Criteria) this;
        }

        public Criteria andStr2IsNotNull() {
            addCriterion("str_2 is not null");
            return (Criteria) this;
        }

        public Criteria andStr2EqualTo(String value) {
            addCriterion("str_2 =", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotEqualTo(String value) {
            addCriterion("str_2 <>", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThan(String value) {
            addCriterion("str_2 >", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThanOrEqualTo(String value) {
            addCriterion("str_2 >=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThan(String value) {
            addCriterion("str_2 <", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThanOrEqualTo(String value) {
            addCriterion("str_2 <=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Like(String value) {
            addCriterion("str_2 like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotLike(String value) {
            addCriterion("str_2 not like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2In(List<String> values) {
            addCriterion("str_2 in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotIn(List<String> values) {
            addCriterion("str_2 not in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Between(String value1, String value2) {
            addCriterion("str_2 between", value1, value2, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotBetween(String value1, String value2) {
            addCriterion("str_2 not between", value1, value2, "str2");
            return (Criteria) this;
        }
        
				
        public Criteria andNum1IsNull() {
            addCriterion("num_1 is null");
            return (Criteria) this;
        }

        public Criteria andNum1IsNotNull() {
            addCriterion("num_1 is not null");
            return (Criteria) this;
        }

        public Criteria andNum1EqualTo(Long value) {
            addCriterion("num_1 =", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotEqualTo(Long value) {
            addCriterion("num_1 <>", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThan(Long value) {
            addCriterion("num_1 >", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThanOrEqualTo(Long value) {
            addCriterion("num_1 >=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThan(Long value) {
            addCriterion("num_1 <", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThanOrEqualTo(Long value) {
            addCriterion("num_1 <=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Like(Long value) {
            addCriterion("num_1 like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotLike(Long value) {
            addCriterion("num_1 not like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1In(List<Long> values) {
            addCriterion("num_1 in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotIn(List<Long> values) {
            addCriterion("num_1 not in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Between(Long value1, Long value2) {
            addCriterion("num_1 between", value1, value2, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotBetween(Long value1, Long value2) {
            addCriterion("num_1 not between", value1, value2, "num1");
            return (Criteria) this;
        }
        
				
        public Criteria andNum2IsNull() {
            addCriterion("num_2 is null");
            return (Criteria) this;
        }

        public Criteria andNum2IsNotNull() {
            addCriterion("num_2 is not null");
            return (Criteria) this;
        }

        public Criteria andNum2EqualTo(Integer value) {
            addCriterion("num_2 =", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotEqualTo(Integer value) {
            addCriterion("num_2 <>", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThan(Integer value) {
            addCriterion("num_2 >", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThanOrEqualTo(Integer value) {
            addCriterion("num_2 >=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThan(Integer value) {
            addCriterion("num_2 <", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThanOrEqualTo(Integer value) {
            addCriterion("num_2 <=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Like(Integer value) {
            addCriterion("num_2 like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotLike(Integer value) {
            addCriterion("num_2 not like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2In(List<Integer> values) {
            addCriterion("num_2 in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotIn(List<Integer> values) {
            addCriterion("num_2 not in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Between(Integer value1, Integer value2) {
            addCriterion("num_2 between", value1, value2, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotBetween(Integer value1, Integer value2) {
            addCriterion("num_2 not between", value1, value2, "num2");
            return (Criteria) this;
        }
        
				
        public Criteria andTsIsNull() {
            addCriterion("ts is null");
            return (Criteria) this;
        }

        public Criteria andTsIsNotNull() {
            addCriterion("ts is not null");
            return (Criteria) this;
        }

        public Criteria andTsEqualTo(Date value) {
            addCriterion("ts =", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotEqualTo(Date value) {
            addCriterion("ts <>", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsGreaterThan(Date value) {
            addCriterion("ts >", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsGreaterThanOrEqualTo(Date value) {
            addCriterion("ts >=", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLessThan(Date value) {
            addCriterion("ts <", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLessThanOrEqualTo(Date value) {
            addCriterion("ts <=", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLike(Date value) {
            addCriterion("ts like", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotLike(Date value) {
            addCriterion("ts not like", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsIn(List<Date> values) {
            addCriterion("ts in", values, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotIn(List<Date> values) {
            addCriterion("ts not in", values, "ts");
            return (Criteria) this;
        }

        public Criteria andTsBetween(Date value1, Date value2) {
            addCriterion("ts between", value1, value2, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotBetween(Date value1, Date value2) {
            addCriterion("ts not between", value1, value2, "ts");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(OmsOrderDetail record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			 list.add("ifnull(sid,'')");
    		}
			if(record.getItemNo()!=null&&StrUtil.isNotEmpty(record.getItemNo().toString())) {
    			 list.add("ifnull(item_no,'')");
    		}
			if(record.getOrderNo()!=null&&StrUtil.isNotEmpty(record.getOrderNo().toString())) {
    			 list.add("ifnull(order_no,'')");
    		}
			if(record.getProductDetailSid()!=null&&StrUtil.isNotEmpty(record.getProductDetailSid().toString())) {
    			 list.add("ifnull(product_detail_sid,'')");
    		}
			if(record.getProductSid()!=null&&StrUtil.isNotEmpty(record.getProductSid().toString())) {
    			 list.add("ifnull(product_sid,'')");
    		}
			if(record.getProductTitle()!=null&&StrUtil.isNotEmpty(record.getProductTitle().toString())) {
    			 list.add("ifnull(product_title,'')");
    		}
			if(record.getProductAlias()!=null&&StrUtil.isNotEmpty(record.getProductAlias().toString())) {
    			 list.add("ifnull(product_alias,'')");
    		}
			if(record.getProductSpecific()!=null&&StrUtil.isNotEmpty(record.getProductSpecific().toString())) {
    			 list.add("ifnull(product_specific,'')");
    		}
			if(record.getUnitName()!=null&&StrUtil.isNotEmpty(record.getUnitName().toString())) {
    			 list.add("ifnull(unit_name,'')");
    		}
			if(record.getXiaoshouPrice()!=null&&StrUtil.isNotEmpty(record.getXiaoshouPrice().toString())) {
    			 list.add("ifnull(xiaoshou_price,'')");
    		}
			if(record.getCategorySid()!=null&&StrUtil.isNotEmpty(record.getCategorySid().toString())) {
    			 list.add("ifnull(category_sid,'')");
    		}
			if(record.getSupplySid()!=null&&StrUtil.isNotEmpty(record.getSupplySid().toString())) {
    			 list.add("ifnull(supply_sid,'')");
    		}
			if(record.getShopSid()!=null&&StrUtil.isNotEmpty(record.getShopSid().toString())) {
    			 list.add("ifnull(shop_sid,'')");
    		}
			if(record.getStockTypeSid()!=null&&StrUtil.isNotEmpty(record.getStockTypeSid().toString())) {
    			 list.add("ifnull(stock_type_sid,'')");
    		}
			if(record.getItemNum()!=null&&StrUtil.isNotEmpty(record.getItemNum().toString())) {
    			 list.add("ifnull(item_num,'')");
    		}
			if(record.getPrice()!=null&&StrUtil.isNotEmpty(record.getPrice().toString())) {
    			 list.add("ifnull(price,'')");
    		}
			if(record.getRefundNum()!=null&&StrUtil.isNotEmpty(record.getRefundNum().toString())) {
    			 list.add("ifnull(refund_num,'')");
    		}
			if(record.getImgUrl()!=null&&StrUtil.isNotEmpty(record.getImgUrl().toString())) {
    			 list.add("ifnull(img_url,'')");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			 list.add("ifnull(str_1,'')");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			 list.add("ifnull(str_2,'')");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			 list.add("ifnull(num_1,'')");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			 list.add("ifnull(num_2,'')");
    		}
			if(record.getTs()!=null&&StrUtil.isNotEmpty(record.getTs().toString())) {
    			 list.add("ifnull(ts,'')");
    		}
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			list2.add("'%"+record.getSid()+"%'");
    		}
			if(record.getItemNo()!=null&&StrUtil.isNotEmpty(record.getItemNo().toString())) {
    			list2.add("'%"+record.getItemNo()+"%'");
    		}
			if(record.getOrderNo()!=null&&StrUtil.isNotEmpty(record.getOrderNo().toString())) {
    			list2.add("'%"+record.getOrderNo()+"%'");
    		}
			if(record.getProductDetailSid()!=null&&StrUtil.isNotEmpty(record.getProductDetailSid().toString())) {
    			list2.add("'%"+record.getProductDetailSid()+"%'");
    		}
			if(record.getProductSid()!=null&&StrUtil.isNotEmpty(record.getProductSid().toString())) {
    			list2.add("'%"+record.getProductSid()+"%'");
    		}
			if(record.getProductTitle()!=null&&StrUtil.isNotEmpty(record.getProductTitle().toString())) {
    			list2.add("'%"+record.getProductTitle()+"%'");
    		}
			if(record.getProductAlias()!=null&&StrUtil.isNotEmpty(record.getProductAlias().toString())) {
    			list2.add("'%"+record.getProductAlias()+"%'");
    		}
			if(record.getProductSpecific()!=null&&StrUtil.isNotEmpty(record.getProductSpecific().toString())) {
    			list2.add("'%"+record.getProductSpecific()+"%'");
    		}
			if(record.getUnitName()!=null&&StrUtil.isNotEmpty(record.getUnitName().toString())) {
    			list2.add("'%"+record.getUnitName()+"%'");
    		}
			if(record.getXiaoshouPrice()!=null&&StrUtil.isNotEmpty(record.getXiaoshouPrice().toString())) {
    			list2.add("'%"+record.getXiaoshouPrice()+"%'");
    		}
			if(record.getCategorySid()!=null&&StrUtil.isNotEmpty(record.getCategorySid().toString())) {
    			list2.add("'%"+record.getCategorySid()+"%'");
    		}
			if(record.getSupplySid()!=null&&StrUtil.isNotEmpty(record.getSupplySid().toString())) {
    			list2.add("'%"+record.getSupplySid()+"%'");
    		}
			if(record.getShopSid()!=null&&StrUtil.isNotEmpty(record.getShopSid().toString())) {
    			list2.add("'%"+record.getShopSid()+"%'");
    		}
			if(record.getStockTypeSid()!=null&&StrUtil.isNotEmpty(record.getStockTypeSid().toString())) {
    			list2.add("'%"+record.getStockTypeSid()+"%'");
    		}
			if(record.getItemNum()!=null&&StrUtil.isNotEmpty(record.getItemNum().toString())) {
    			list2.add("'%"+record.getItemNum()+"%'");
    		}
			if(record.getPrice()!=null&&StrUtil.isNotEmpty(record.getPrice().toString())) {
    			list2.add("'%"+record.getPrice()+"%'");
    		}
			if(record.getRefundNum()!=null&&StrUtil.isNotEmpty(record.getRefundNum().toString())) {
    			list2.add("'%"+record.getRefundNum()+"%'");
    		}
			if(record.getImgUrl()!=null&&StrUtil.isNotEmpty(record.getImgUrl().toString())) {
    			list2.add("'%"+record.getImgUrl()+"%'");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			list2.add("'%"+record.getStr1()+"%'");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			list2.add("'%"+record.getStr2()+"%'");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			list2.add("'%"+record.getNum1()+"%'");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			list2.add("'%"+record.getNum2()+"%'");
    		}
			if(record.getTs()!=null&&StrUtil.isNotEmpty(record.getTs().toString())) {
    			list2.add("'%"+record.getTs()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(sid,'')");
    		list.add("ifnull(item_no,'')");
    		list.add("ifnull(order_no,'')");
    		list.add("ifnull(product_detail_sid,'')");
    		list.add("ifnull(product_sid,'')");
    		list.add("ifnull(product_title,'')");
    		list.add("ifnull(product_alias,'')");
    		list.add("ifnull(product_specific,'')");
    		list.add("ifnull(unit_name,'')");
    		list.add("ifnull(xiaoshou_price,'')");
    		list.add("ifnull(category_sid,'')");
    		list.add("ifnull(supply_sid,'')");
    		list.add("ifnull(shop_sid,'')");
    		list.add("ifnull(stock_type_sid,'')");
    		list.add("ifnull(item_num,'')");
    		list.add("ifnull(price,'')");
    		list.add("ifnull(refund_num,'')");
    		list.add("ifnull(img_url,'')");
    		list.add("ifnull(str_1,'')");
    		list.add("ifnull(str_2,'')");
    		list.add("ifnull(num_1,'')");
    		list.add("ifnull(num_2,'')");
    		list.add("ifnull(ts,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}