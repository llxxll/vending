package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 点位管理表 PointExample
 * @author lxl_自动生成
 * @date 2023-12-01 16:36:52
 */
public class PointExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PointExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andPointNameIsNull() {
            addCriterion("point_name is null");
            return (Criteria) this;
        }

        public Criteria andPointNameIsNotNull() {
            addCriterion("point_name is not null");
            return (Criteria) this;
        }

        public Criteria andPointNameEqualTo(String value) {
            addCriterion("point_name =", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameNotEqualTo(String value) {
            addCriterion("point_name <>", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameGreaterThan(String value) {
            addCriterion("point_name >", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameGreaterThanOrEqualTo(String value) {
            addCriterion("point_name >=", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameLessThan(String value) {
            addCriterion("point_name <", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameLessThanOrEqualTo(String value) {
            addCriterion("point_name <=", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameLike(String value) {
            addCriterion("point_name like", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameNotLike(String value) {
            addCriterion("point_name not like", value, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameIn(List<String> values) {
            addCriterion("point_name in", values, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameNotIn(List<String> values) {
            addCriterion("point_name not in", values, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameBetween(String value1, String value2) {
            addCriterion("point_name between", value1, value2, "pointName");
            return (Criteria) this;
        }

        public Criteria andPointNameNotBetween(String value1, String value2) {
            addCriterion("point_name not between", value1, value2, "pointName");
            return (Criteria) this;
        }
        
				
        public Criteria andPointTypeIsNull() {
            addCriterion("point_type is null");
            return (Criteria) this;
        }

        public Criteria andPointTypeIsNotNull() {
            addCriterion("point_type is not null");
            return (Criteria) this;
        }

        public Criteria andPointTypeEqualTo(String value) {
            addCriterion("point_type =", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeNotEqualTo(String value) {
            addCriterion("point_type <>", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeGreaterThan(String value) {
            addCriterion("point_type >", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeGreaterThanOrEqualTo(String value) {
            addCriterion("point_type >=", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeLessThan(String value) {
            addCriterion("point_type <", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeLessThanOrEqualTo(String value) {
            addCriterion("point_type <=", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeLike(String value) {
            addCriterion("point_type like", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeNotLike(String value) {
            addCriterion("point_type not like", value, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeIn(List<String> values) {
            addCriterion("point_type in", values, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeNotIn(List<String> values) {
            addCriterion("point_type not in", values, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeBetween(String value1, String value2) {
            addCriterion("point_type between", value1, value2, "pointType");
            return (Criteria) this;
        }

        public Criteria andPointTypeNotBetween(String value1, String value2) {
            addCriterion("point_type not between", value1, value2, "pointType");
            return (Criteria) this;
        }
        
				
        public Criteria andLineIdIsNull() {
            addCriterion("line_id is null");
            return (Criteria) this;
        }

        public Criteria andLineIdIsNotNull() {
            addCriterion("line_id is not null");
            return (Criteria) this;
        }

        public Criteria andLineIdEqualTo(String value) {
            addCriterion("line_id =", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotEqualTo(String value) {
            addCriterion("line_id <>", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdGreaterThan(String value) {
            addCriterion("line_id >", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdGreaterThanOrEqualTo(String value) {
            addCriterion("line_id >=", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdLessThan(String value) {
            addCriterion("line_id <", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdLessThanOrEqualTo(String value) {
            addCriterion("line_id <=", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdLike(String value) {
            addCriterion("line_id like", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotLike(String value) {
            addCriterion("line_id not like", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdIn(List<String> values) {
            addCriterion("line_id in", values, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotIn(List<String> values) {
            addCriterion("line_id not in", values, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdBetween(String value1, String value2) {
            addCriterion("line_id between", value1, value2, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotBetween(String value1, String value2) {
            addCriterion("line_id not between", value1, value2, "lineId");
            return (Criteria) this;
        }
        
				
        public Criteria andLineNameIsNull() {
            addCriterion("line_name is null");
            return (Criteria) this;
        }

        public Criteria andLineNameIsNotNull() {
            addCriterion("line_name is not null");
            return (Criteria) this;
        }

        public Criteria andLineNameEqualTo(String value) {
            addCriterion("line_name =", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameNotEqualTo(String value) {
            addCriterion("line_name <>", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameGreaterThan(String value) {
            addCriterion("line_name >", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameGreaterThanOrEqualTo(String value) {
            addCriterion("line_name >=", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameLessThan(String value) {
            addCriterion("line_name <", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameLessThanOrEqualTo(String value) {
            addCriterion("line_name <=", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameLike(String value) {
            addCriterion("line_name like", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameNotLike(String value) {
            addCriterion("line_name not like", value, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameIn(List<String> values) {
            addCriterion("line_name in", values, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameNotIn(List<String> values) {
            addCriterion("line_name not in", values, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameBetween(String value1, String value2) {
            addCriterion("line_name between", value1, value2, "lineName");
            return (Criteria) this;
        }

        public Criteria andLineNameNotBetween(String value1, String value2) {
            addCriterion("line_name not between", value1, value2, "lineName");
            return (Criteria) this;
        }
        
				
        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }
        
				
        public Criteria andPointNotesIsNull() {
            addCriterion("point_notes is null");
            return (Criteria) this;
        }

        public Criteria andPointNotesIsNotNull() {
            addCriterion("point_notes is not null");
            return (Criteria) this;
        }

        public Criteria andPointNotesEqualTo(String value) {
            addCriterion("point_notes =", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesNotEqualTo(String value) {
            addCriterion("point_notes <>", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesGreaterThan(String value) {
            addCriterion("point_notes >", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesGreaterThanOrEqualTo(String value) {
            addCriterion("point_notes >=", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesLessThan(String value) {
            addCriterion("point_notes <", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesLessThanOrEqualTo(String value) {
            addCriterion("point_notes <=", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesLike(String value) {
            addCriterion("point_notes like", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesNotLike(String value) {
            addCriterion("point_notes not like", value, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesIn(List<String> values) {
            addCriterion("point_notes in", values, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesNotIn(List<String> values) {
            addCriterion("point_notes not in", values, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesBetween(String value1, String value2) {
            addCriterion("point_notes between", value1, value2, "pointNotes");
            return (Criteria) this;
        }

        public Criteria andPointNotesNotBetween(String value1, String value2) {
            addCriterion("point_notes not between", value1, value2, "pointNotes");
            return (Criteria) this;
        }
        
				
        public Criteria andPointDescIsNull() {
            addCriterion("point_desc is null");
            return (Criteria) this;
        }

        public Criteria andPointDescIsNotNull() {
            addCriterion("point_desc is not null");
            return (Criteria) this;
        }

        public Criteria andPointDescEqualTo(String value) {
            addCriterion("point_desc =", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescNotEqualTo(String value) {
            addCriterion("point_desc <>", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescGreaterThan(String value) {
            addCriterion("point_desc >", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescGreaterThanOrEqualTo(String value) {
            addCriterion("point_desc >=", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescLessThan(String value) {
            addCriterion("point_desc <", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescLessThanOrEqualTo(String value) {
            addCriterion("point_desc <=", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescLike(String value) {
            addCriterion("point_desc like", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescNotLike(String value) {
            addCriterion("point_desc not like", value, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescIn(List<String> values) {
            addCriterion("point_desc in", values, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescNotIn(List<String> values) {
            addCriterion("point_desc not in", values, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescBetween(String value1, String value2) {
            addCriterion("point_desc between", value1, value2, "pointDesc");
            return (Criteria) this;
        }

        public Criteria andPointDescNotBetween(String value1, String value2) {
            addCriterion("point_desc not between", value1, value2, "pointDesc");
            return (Criteria) this;
        }
        
				
        public Criteria andLongitudeIsNull() {
            addCriterion("longitude is null");
            return (Criteria) this;
        }

        public Criteria andLongitudeIsNotNull() {
            addCriterion("longitude is not null");
            return (Criteria) this;
        }

        public Criteria andLongitudeEqualTo(String value) {
            addCriterion("longitude =", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotEqualTo(String value) {
            addCriterion("longitude <>", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeGreaterThan(String value) {
            addCriterion("longitude >", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeGreaterThanOrEqualTo(String value) {
            addCriterion("longitude >=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLessThan(String value) {
            addCriterion("longitude <", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLessThanOrEqualTo(String value) {
            addCriterion("longitude <=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLike(String value) {
            addCriterion("longitude like", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotLike(String value) {
            addCriterion("longitude not like", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeIn(List<String> values) {
            addCriterion("longitude in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotIn(List<String> values) {
            addCriterion("longitude not in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeBetween(String value1, String value2) {
            addCriterion("longitude between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotBetween(String value1, String value2) {
            addCriterion("longitude not between", value1, value2, "longitude");
            return (Criteria) this;
        }
        
				
        public Criteria andLatitudeIsNull() {
            addCriterion("latitude is null");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNotNull() {
            addCriterion("latitude is not null");
            return (Criteria) this;
        }

        public Criteria andLatitudeEqualTo(String value) {
            addCriterion("latitude =", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotEqualTo(String value) {
            addCriterion("latitude <>", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThan(String value) {
            addCriterion("latitude >", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThanOrEqualTo(String value) {
            addCriterion("latitude >=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThan(String value) {
            addCriterion("latitude <", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThanOrEqualTo(String value) {
            addCriterion("latitude <=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLike(String value) {
            addCriterion("latitude like", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotLike(String value) {
            addCriterion("latitude not like", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeIn(List<String> values) {
            addCriterion("latitude in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotIn(List<String> values) {
            addCriterion("latitude not in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeBetween(String value1, String value2) {
            addCriterion("latitude between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotBetween(String value1, String value2) {
            addCriterion("latitude not between", value1, value2, "latitude");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLike(Date value) {
            addCriterion("created_time like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotLike(Date value) {
            addCriterion("created_time not like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedUserSidIsNull() {
            addCriterion("created_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIsNotNull() {
            addCriterion("created_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidEqualTo(Long value) {
            addCriterion("created_user_sid =", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotEqualTo(Long value) {
            addCriterion("created_user_sid <>", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThan(Long value) {
            addCriterion("created_user_sid >", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("created_user_sid >=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThan(Long value) {
            addCriterion("created_user_sid <", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("created_user_sid <=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLike(Long value) {
            addCriterion("created_user_sid like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotLike(Long value) {
            addCriterion("created_user_sid not like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIn(List<Long> values) {
            addCriterion("created_user_sid in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotIn(List<Long> values) {
            addCriterion("created_user_sid not in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidBetween(Long value1, Long value2) {
            addCriterion("created_user_sid between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("created_user_sid not between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedTimeIsNull() {
            addCriterion("modified_time is null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIsNotNull() {
            addCriterion("modified_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeEqualTo(Date value) {
            addCriterion("modified_time =", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotEqualTo(Date value) {
            addCriterion("modified_time <>", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThan(Date value) {
            addCriterion("modified_time >", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modified_time >=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThan(Date value) {
            addCriterion("modified_time <", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThanOrEqualTo(Date value) {
            addCriterion("modified_time <=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLike(Date value) {
            addCriterion("modified_time like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotLike(Date value) {
            addCriterion("modified_time not like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIn(List<Date> values) {
            addCriterion("modified_time in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotIn(List<Date> values) {
            addCriterion("modified_time not in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeBetween(Date value1, Date value2) {
            addCriterion("modified_time between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotBetween(Date value1, Date value2) {
            addCriterion("modified_time not between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedUserSidIsNull() {
            addCriterion("modified_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIsNotNull() {
            addCriterion("modified_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidEqualTo(Long value) {
            addCriterion("modified_user_sid =", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotEqualTo(Long value) {
            addCriterion("modified_user_sid <>", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThan(Long value) {
            addCriterion("modified_user_sid >", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid >=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThan(Long value) {
            addCriterion("modified_user_sid <", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid <=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLike(Long value) {
            addCriterion("modified_user_sid like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotLike(Long value) {
            addCriterion("modified_user_sid not like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIn(List<Long> values) {
            addCriterion("modified_user_sid in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotIn(List<Long> values) {
            addCriterion("modified_user_sid not in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid not between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andXIsNull() {
            addCriterion("x is null");
            return (Criteria) this;
        }

        public Criteria andXIsNotNull() {
            addCriterion("x is not null");
            return (Criteria) this;
        }

        public Criteria andXEqualTo(Integer value) {
            addCriterion("x =", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotEqualTo(Integer value) {
            addCriterion("x <>", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThan(Integer value) {
            addCriterion("x >", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThanOrEqualTo(Integer value) {
            addCriterion("x >=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThan(Integer value) {
            addCriterion("x <", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThanOrEqualTo(Integer value) {
            addCriterion("x <=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLike(Integer value) {
            addCriterion("x like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotLike(Integer value) {
            addCriterion("x not like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXIn(List<Integer> values) {
            addCriterion("x in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXNotIn(List<Integer> values) {
            addCriterion("x not in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXBetween(Integer value1, Integer value2) {
            addCriterion("x between", value1, value2, "x");
            return (Criteria) this;
        }

        public Criteria andXNotBetween(Integer value1, Integer value2) {
            addCriterion("x not between", value1, value2, "x");
            return (Criteria) this;
        }
        
				
        public Criteria andXTimeIsNull() {
            addCriterion("x_time is null");
            return (Criteria) this;
        }

        public Criteria andXTimeIsNotNull() {
            addCriterion("x_time is not null");
            return (Criteria) this;
        }

        public Criteria andXTimeEqualTo(Date value) {
            addCriterion("x_time =", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotEqualTo(Date value) {
            addCriterion("x_time <>", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThan(Date value) {
            addCriterion("x_time >", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("x_time >=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThan(Date value) {
            addCriterion("x_time <", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThanOrEqualTo(Date value) {
            addCriterion("x_time <=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLike(Date value) {
            addCriterion("x_time like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotLike(Date value) {
            addCriterion("x_time not like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeIn(List<Date> values) {
            addCriterion("x_time in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotIn(List<Date> values) {
            addCriterion("x_time not in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeBetween(Date value1, Date value2) {
            addCriterion("x_time between", value1, value2, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotBetween(Date value1, Date value2) {
            addCriterion("x_time not between", value1, value2, "xTime");
            return (Criteria) this;
        }
        
				
        public Criteria andXUserSidIsNull() {
            addCriterion("x_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andXUserSidIsNotNull() {
            addCriterion("x_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andXUserSidEqualTo(Long value) {
            addCriterion("x_user_sid =", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotEqualTo(Long value) {
            addCriterion("x_user_sid <>", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThan(Long value) {
            addCriterion("x_user_sid >", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("x_user_sid >=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThan(Long value) {
            addCriterion("x_user_sid <", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThanOrEqualTo(Long value) {
            addCriterion("x_user_sid <=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLike(Long value) {
            addCriterion("x_user_sid like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotLike(Long value) {
            addCriterion("x_user_sid not like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidIn(List<Long> values) {
            addCriterion("x_user_sid in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotIn(List<Long> values) {
            addCriterion("x_user_sid not in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidBetween(Long value1, Long value2) {
            addCriterion("x_user_sid between", value1, value2, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotBetween(Long value1, Long value2) {
            addCriterion("x_user_sid not between", value1, value2, "xUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andStr1IsNull() {
            addCriterion("str_1 is null");
            return (Criteria) this;
        }

        public Criteria andStr1IsNotNull() {
            addCriterion("str_1 is not null");
            return (Criteria) this;
        }

        public Criteria andStr1EqualTo(String value) {
            addCriterion("str_1 =", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotEqualTo(String value) {
            addCriterion("str_1 <>", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThan(String value) {
            addCriterion("str_1 >", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThanOrEqualTo(String value) {
            addCriterion("str_1 >=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThan(String value) {
            addCriterion("str_1 <", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThanOrEqualTo(String value) {
            addCriterion("str_1 <=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Like(String value) {
            addCriterion("str_1 like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotLike(String value) {
            addCriterion("str_1 not like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1In(List<String> values) {
            addCriterion("str_1 in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotIn(List<String> values) {
            addCriterion("str_1 not in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Between(String value1, String value2) {
            addCriterion("str_1 between", value1, value2, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotBetween(String value1, String value2) {
            addCriterion("str_1 not between", value1, value2, "str1");
            return (Criteria) this;
        }
        
				
        public Criteria andStr2IsNull() {
            addCriterion("str_2 is null");
            return (Criteria) this;
        }

        public Criteria andStr2IsNotNull() {
            addCriterion("str_2 is not null");
            return (Criteria) this;
        }

        public Criteria andStr2EqualTo(String value) {
            addCriterion("str_2 =", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotEqualTo(String value) {
            addCriterion("str_2 <>", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThan(String value) {
            addCriterion("str_2 >", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThanOrEqualTo(String value) {
            addCriterion("str_2 >=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThan(String value) {
            addCriterion("str_2 <", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThanOrEqualTo(String value) {
            addCriterion("str_2 <=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Like(String value) {
            addCriterion("str_2 like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotLike(String value) {
            addCriterion("str_2 not like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2In(List<String> values) {
            addCriterion("str_2 in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotIn(List<String> values) {
            addCriterion("str_2 not in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Between(String value1, String value2) {
            addCriterion("str_2 between", value1, value2, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotBetween(String value1, String value2) {
            addCriterion("str_2 not between", value1, value2, "str2");
            return (Criteria) this;
        }
        
				
        public Criteria andStr3IsNull() {
            addCriterion("str_3 is null");
            return (Criteria) this;
        }

        public Criteria andStr3IsNotNull() {
            addCriterion("str_3 is not null");
            return (Criteria) this;
        }

        public Criteria andStr3EqualTo(String value) {
            addCriterion("str_3 =", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotEqualTo(String value) {
            addCriterion("str_3 <>", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3GreaterThan(String value) {
            addCriterion("str_3 >", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3GreaterThanOrEqualTo(String value) {
            addCriterion("str_3 >=", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3LessThan(String value) {
            addCriterion("str_3 <", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3LessThanOrEqualTo(String value) {
            addCriterion("str_3 <=", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3Like(String value) {
            addCriterion("str_3 like", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotLike(String value) {
            addCriterion("str_3 not like", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3In(List<String> values) {
            addCriterion("str_3 in", values, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotIn(List<String> values) {
            addCriterion("str_3 not in", values, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3Between(String value1, String value2) {
            addCriterion("str_3 between", value1, value2, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotBetween(String value1, String value2) {
            addCriterion("str_3 not between", value1, value2, "str3");
            return (Criteria) this;
        }
        
				
        public Criteria andNum1IsNull() {
            addCriterion("num_1 is null");
            return (Criteria) this;
        }

        public Criteria andNum1IsNotNull() {
            addCriterion("num_1 is not null");
            return (Criteria) this;
        }

        public Criteria andNum1EqualTo(Long value) {
            addCriterion("num_1 =", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotEqualTo(Long value) {
            addCriterion("num_1 <>", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThan(Long value) {
            addCriterion("num_1 >", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThanOrEqualTo(Long value) {
            addCriterion("num_1 >=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThan(Long value) {
            addCriterion("num_1 <", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThanOrEqualTo(Long value) {
            addCriterion("num_1 <=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Like(Long value) {
            addCriterion("num_1 like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotLike(Long value) {
            addCriterion("num_1 not like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1In(List<Long> values) {
            addCriterion("num_1 in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotIn(List<Long> values) {
            addCriterion("num_1 not in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Between(Long value1, Long value2) {
            addCriterion("num_1 between", value1, value2, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotBetween(Long value1, Long value2) {
            addCriterion("num_1 not between", value1, value2, "num1");
            return (Criteria) this;
        }
        
				
        public Criteria andNum2IsNull() {
            addCriterion("num_2 is null");
            return (Criteria) this;
        }

        public Criteria andNum2IsNotNull() {
            addCriterion("num_2 is not null");
            return (Criteria) this;
        }

        public Criteria andNum2EqualTo(Integer value) {
            addCriterion("num_2 =", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotEqualTo(Integer value) {
            addCriterion("num_2 <>", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThan(Integer value) {
            addCriterion("num_2 >", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThanOrEqualTo(Integer value) {
            addCriterion("num_2 >=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThan(Integer value) {
            addCriterion("num_2 <", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThanOrEqualTo(Integer value) {
            addCriterion("num_2 <=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Like(Integer value) {
            addCriterion("num_2 like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotLike(Integer value) {
            addCriterion("num_2 not like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2In(List<Integer> values) {
            addCriterion("num_2 in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotIn(List<Integer> values) {
            addCriterion("num_2 not in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Between(Integer value1, Integer value2) {
            addCriterion("num_2 between", value1, value2, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotBetween(Integer value1, Integer value2) {
            addCriterion("num_2 not between", value1, value2, "num2");
            return (Criteria) this;
        }
        
				
        public Criteria andNum3IsNull() {
            addCriterion("num_3 is null");
            return (Criteria) this;
        }

        public Criteria andNum3IsNotNull() {
            addCriterion("num_3 is not null");
            return (Criteria) this;
        }

        public Criteria andNum3EqualTo(Integer value) {
            addCriterion("num_3 =", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotEqualTo(Integer value) {
            addCriterion("num_3 <>", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3GreaterThan(Integer value) {
            addCriterion("num_3 >", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3GreaterThanOrEqualTo(Integer value) {
            addCriterion("num_3 >=", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3LessThan(Integer value) {
            addCriterion("num_3 <", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3LessThanOrEqualTo(Integer value) {
            addCriterion("num_3 <=", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3Like(Integer value) {
            addCriterion("num_3 like", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotLike(Integer value) {
            addCriterion("num_3 not like", value, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3In(List<Integer> values) {
            addCriterion("num_3 in", values, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotIn(List<Integer> values) {
            addCriterion("num_3 not in", values, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3Between(Integer value1, Integer value2) {
            addCriterion("num_3 between", value1, value2, "num3");
            return (Criteria) this;
        }

        public Criteria andNum3NotBetween(Integer value1, Integer value2) {
            addCriterion("num_3 not between", value1, value2, "num3");
            return (Criteria) this;
        }
        
				
        public Criteria andProvinceCodeIsNull() {
            addCriterion("province_code is null");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIsNotNull() {
            addCriterion("province_code is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeEqualTo(String value) {
            addCriterion("province_code =", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotEqualTo(String value) {
            addCriterion("province_code <>", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeGreaterThan(String value) {
            addCriterion("province_code >", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("province_code >=", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLessThan(String value) {
            addCriterion("province_code <", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLessThanOrEqualTo(String value) {
            addCriterion("province_code <=", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLike(String value) {
            addCriterion("province_code like", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotLike(String value) {
            addCriterion("province_code not like", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIn(List<String> values) {
            addCriterion("province_code in", values, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotIn(List<String> values) {
            addCriterion("province_code not in", values, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeBetween(String value1, String value2) {
            addCriterion("province_code between", value1, value2, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotBetween(String value1, String value2) {
            addCriterion("province_code not between", value1, value2, "provinceCode");
            return (Criteria) this;
        }
        
				
        public Criteria andCityCodeIsNull() {
            addCriterion("city_code is null");
            return (Criteria) this;
        }

        public Criteria andCityCodeIsNotNull() {
            addCriterion("city_code is not null");
            return (Criteria) this;
        }

        public Criteria andCityCodeEqualTo(String value) {
            addCriterion("city_code =", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotEqualTo(String value) {
            addCriterion("city_code <>", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeGreaterThan(String value) {
            addCriterion("city_code >", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeGreaterThanOrEqualTo(String value) {
            addCriterion("city_code >=", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLessThan(String value) {
            addCriterion("city_code <", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLessThanOrEqualTo(String value) {
            addCriterion("city_code <=", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLike(String value) {
            addCriterion("city_code like", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotLike(String value) {
            addCriterion("city_code not like", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeIn(List<String> values) {
            addCriterion("city_code in", values, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotIn(List<String> values) {
            addCriterion("city_code not in", values, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeBetween(String value1, String value2) {
            addCriterion("city_code between", value1, value2, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotBetween(String value1, String value2) {
            addCriterion("city_code not between", value1, value2, "cityCode");
            return (Criteria) this;
        }
        
				
        public Criteria andAreaCodeIsNull() {
            addCriterion("area_code is null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNotNull() {
            addCriterion("area_code is not null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeEqualTo(String value) {
            addCriterion("area_code =", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotEqualTo(String value) {
            addCriterion("area_code <>", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThan(String value) {
            addCriterion("area_code >", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("area_code >=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThan(String value) {
            addCriterion("area_code <", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("area_code <=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLike(String value) {
            addCriterion("area_code like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotLike(String value) {
            addCriterion("area_code not like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIn(List<String> values) {
            addCriterion("area_code in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotIn(List<String> values) {
            addCriterion("area_code not in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeBetween(String value1, String value2) {
            addCriterion("area_code between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotBetween(String value1, String value2) {
            addCriterion("area_code not between", value1, value2, "areaCode");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(Point record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getPointName()!=null&&StrUtil.isNotEmpty(record.getPointName().toString())) {
    			 list.add("ifnull(point_name,'')");
    		}
			if(record.getPointType()!=null&&StrUtil.isNotEmpty(record.getPointType().toString())) {
    			 list.add("ifnull(point_type,'')");
    		}
			if(record.getLineId()!=null&&StrUtil.isNotEmpty(record.getLineId().toString())) {
    			 list.add("ifnull(line_id,'')");
    		}
			if(record.getLineName()!=null&&StrUtil.isNotEmpty(record.getLineName().toString())) {
    			 list.add("ifnull(line_name,'')");
    		}
			if(record.getAddress()!=null&&StrUtil.isNotEmpty(record.getAddress().toString())) {
    			 list.add("ifnull(address,'')");
    		}
			if(record.getPointNotes()!=null&&StrUtil.isNotEmpty(record.getPointNotes().toString())) {
    			 list.add("ifnull(point_notes,'')");
    		}
			if(record.getPointDesc()!=null&&StrUtil.isNotEmpty(record.getPointDesc().toString())) {
    			 list.add("ifnull(point_desc,'')");
    		}
			if(record.getLongitude()!=null&&StrUtil.isNotEmpty(record.getLongitude().toString())) {
    			 list.add("ifnull(longitude,'')");
    		}
			if(record.getLatitude()!=null&&StrUtil.isNotEmpty(record.getLatitude().toString())) {
    			 list.add("ifnull(latitude,'')");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			 list.add("ifnull(created_time,'')");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			 list.add("ifnull(created_user_sid,'')");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			 list.add("ifnull(modified_time,'')");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			 list.add("ifnull(modified_user_sid,'')");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			 list.add("ifnull(x,'')");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			 list.add("ifnull(x_time,'')");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			 list.add("ifnull(x_user_sid,'')");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			 list.add("ifnull(str_1,'')");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			 list.add("ifnull(str_2,'')");
    		}
			if(record.getStr3()!=null&&StrUtil.isNotEmpty(record.getStr3().toString())) {
    			 list.add("ifnull(str_3,'')");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			 list.add("ifnull(num_1,'')");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			 list.add("ifnull(num_2,'')");
    		}
			if(record.getNum3()!=null&&StrUtil.isNotEmpty(record.getNum3().toString())) {
    			 list.add("ifnull(num_3,'')");
    		}
			if(record.getProvinceCode()!=null&&StrUtil.isNotEmpty(record.getProvinceCode().toString())) {
    			 list.add("ifnull(province_code,'')");
    		}
			if(record.getCityCode()!=null&&StrUtil.isNotEmpty(record.getCityCode().toString())) {
    			 list.add("ifnull(city_code,'')");
    		}
			if(record.getAreaCode()!=null&&StrUtil.isNotEmpty(record.getAreaCode().toString())) {
    			 list.add("ifnull(area_code,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getPointName()!=null&&StrUtil.isNotEmpty(record.getPointName().toString())) {
    			list2.add("'%"+record.getPointName()+"%'");
    		}
			if(record.getPointType()!=null&&StrUtil.isNotEmpty(record.getPointType().toString())) {
    			list2.add("'%"+record.getPointType()+"%'");
    		}
			if(record.getLineId()!=null&&StrUtil.isNotEmpty(record.getLineId().toString())) {
    			list2.add("'%"+record.getLineId()+"%'");
    		}
			if(record.getLineName()!=null&&StrUtil.isNotEmpty(record.getLineName().toString())) {
    			list2.add("'%"+record.getLineName()+"%'");
    		}
			if(record.getAddress()!=null&&StrUtil.isNotEmpty(record.getAddress().toString())) {
    			list2.add("'%"+record.getAddress()+"%'");
    		}
			if(record.getPointNotes()!=null&&StrUtil.isNotEmpty(record.getPointNotes().toString())) {
    			list2.add("'%"+record.getPointNotes()+"%'");
    		}
			if(record.getPointDesc()!=null&&StrUtil.isNotEmpty(record.getPointDesc().toString())) {
    			list2.add("'%"+record.getPointDesc()+"%'");
    		}
			if(record.getLongitude()!=null&&StrUtil.isNotEmpty(record.getLongitude().toString())) {
    			list2.add("'%"+record.getLongitude()+"%'");
    		}
			if(record.getLatitude()!=null&&StrUtil.isNotEmpty(record.getLatitude().toString())) {
    			list2.add("'%"+record.getLatitude()+"%'");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			list2.add("'%"+record.getCreatedTime()+"%'");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			list2.add("'%"+record.getCreatedUserSid()+"%'");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			list2.add("'%"+record.getModifiedTime()+"%'");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			list2.add("'%"+record.getModifiedUserSid()+"%'");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			list2.add("'%"+record.getX()+"%'");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			list2.add("'%"+record.getXTime()+"%'");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			list2.add("'%"+record.getXUserSid()+"%'");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			list2.add("'%"+record.getStr1()+"%'");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			list2.add("'%"+record.getStr2()+"%'");
    		}
			if(record.getStr3()!=null&&StrUtil.isNotEmpty(record.getStr3().toString())) {
    			list2.add("'%"+record.getStr3()+"%'");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			list2.add("'%"+record.getNum1()+"%'");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			list2.add("'%"+record.getNum2()+"%'");
    		}
			if(record.getNum3()!=null&&StrUtil.isNotEmpty(record.getNum3().toString())) {
    			list2.add("'%"+record.getNum3()+"%'");
    		}
			if(record.getProvinceCode()!=null&&StrUtil.isNotEmpty(record.getProvinceCode().toString())) {
    			list2.add("'%"+record.getProvinceCode()+"%'");
    		}
			if(record.getCityCode()!=null&&StrUtil.isNotEmpty(record.getCityCode().toString())) {
    			list2.add("'%"+record.getCityCode()+"%'");
    		}
			if(record.getAreaCode()!=null&&StrUtil.isNotEmpty(record.getAreaCode().toString())) {
    			list2.add("'%"+record.getAreaCode()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(point_name,'')");
    		list.add("ifnull(point_type,'')");
    		list.add("ifnull(line_id,'')");
    		list.add("ifnull(line_name,'')");
    		list.add("ifnull(address,'')");
    		list.add("ifnull(point_notes,'')");
    		list.add("ifnull(point_desc,'')");
    		list.add("ifnull(longitude,'')");
    		list.add("ifnull(latitude,'')");
    		list.add("ifnull(created_time,'')");
    		list.add("ifnull(created_user_sid,'')");
    		list.add("ifnull(modified_time,'')");
    		list.add("ifnull(modified_user_sid,'')");
    		list.add("ifnull(x,'')");
    		list.add("ifnull(x_time,'')");
    		list.add("ifnull(x_user_sid,'')");
    		list.add("ifnull(str_1,'')");
    		list.add("ifnull(str_2,'')");
    		list.add("ifnull(str_3,'')");
    		list.add("ifnull(num_1,'')");
    		list.add("ifnull(num_2,'')");
    		list.add("ifnull(num_3,'')");
    		list.add("ifnull(province_code,'')");
    		list.add("ifnull(city_code,'')");
    		list.add("ifnull(area_code,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}