package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class PlanWorkOrderPzStatement implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "物料号")
	private String materialNo;
	
	@ApiModelProperty(value = "物料名称")
	private String materialName;
	
	@ApiModelProperty(value = "车间名称")
	private String orgName;
	
	@ApiModelProperty(value = "规格")
	private String spesc;
	
	@ApiModelProperty(value = "工序名称")
	private String carftName;
	
	@ApiModelProperty(value = "工人")
	private String worker;
	
	@ApiModelProperty(value = "单价")
	private Double price;
	
	@ApiModelProperty(value = "数量")
	private Integer count;
	
	@ApiModelProperty(value = "总价")
	private Double alAccount;
	
	@ApiModelProperty(value = "派工明细ID")
	private Integer pgId;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "结算时间")
	private Date distributionTime;
	
	@ApiModelProperty(value = "状态0:未结算1已结算")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("materialNo")
	public String getMaterialNo() {
		return materialNo;
	}

	public void setMaterialNo(String materialNo) {
		this.materialNo =  materialNo;
	}
	@JsonProperty("materialName")
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName =  materialName;
	}
	@JsonProperty("orgName")
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName =  orgName;
	}
	@JsonProperty("spesc")
	public String getSpesc() {
		return spesc;
	}

	public void setSpesc(String spesc) {
		this.spesc =  spesc;
	}
	@JsonProperty("carftName")
	public String getCarftName() {
		return carftName;
	}

	public void setCarftName(String carftName) {
		this.carftName =  carftName;
	}
	@JsonProperty("worker")
	public String getWorker() {
		return worker;
	}

	public void setWorker(String worker) {
		this.worker =  worker;
	}
	@JsonProperty("price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price =  price;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("alAccount")
	public Double getAlAccount() {
		return alAccount;
	}

	public void setAlAccount(Double alAccount) {
		this.alAccount =  alAccount;
	}
	@JsonProperty("pgId")
	public Integer getPgId() {
		return pgId;
	}

	public void setPgId(Integer pgId) {
		this.pgId =  pgId;
	}
	@JsonProperty("distributionTime")
	public Date getDistributionTime() {
		return distributionTime;
	}

	public void setDistributionTime(Date distributionTime) {
		this.distributionTime =  distributionTime;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}

																																		
	public PlanWorkOrderPzStatement(Long id,String materialNo,String materialName,String orgName,String spesc,String carftName,String worker,Double price,Integer count,Double alAccount,Integer pgId,Date distributionTime,Integer status,Date createTime,Long creator,Date updateTime,Long updater) {
				
		this.id = id;
				
		this.materialNo = materialNo;
				
		this.materialName = materialName;
				
		this.orgName = orgName;
				
		this.spesc = spesc;
				
		this.carftName = carftName;
				
		this.worker = worker;
				
		this.price = price;
				
		this.count = count;
				
		this.alAccount = alAccount;
				
		this.pgId = pgId;
				
		this.distributionTime = distributionTime;
				
		this.status = status;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
	}

	public PlanWorkOrderPzStatement() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
	private String createrName;
	private String beginTime;
	private String endTime;
	private String tuname;
	private String tuno;

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getTuname() {
		return tuname;
	}

	public void setTuname(String tuname) {
		this.tuname = tuname;
	}

	public String getTuno() {
		return tuno;
	}

	public void setTuno(String tuno) {
		this.tuno = tuno;
	}
}