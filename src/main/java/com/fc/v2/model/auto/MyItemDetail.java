package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class MyItemDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "")
	private String itemNo;
	
	@ApiModelProperty(value = "")
	private String ratio;
	
	@ApiModelProperty(value = "")
	private String descripe;
	
	@ApiModelProperty(value = "")
	private String remark;
	
	@ApiModelProperty(value = "")
	private Long creater;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private Long playerId;
	
	@ApiModelProperty(value = "")
	private String type;
	
	@ApiModelProperty(value = "")
	private String level;
	
	@ApiModelProperty(value = "")
	private String status;
	
	@ApiModelProperty(value = "")
	private String tt;
	
	@ApiModelProperty(value = "")
	private String tf;
	
	@ApiModelProperty(value = "")
	private BigDecimal bonus;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("itemNo")
	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo =  itemNo;
	}
	@JsonProperty("ratio")
	public String getRatio() {
		return ratio;
	}

	public void setRatio(String ratio) {
		this.ratio =  ratio;
	}
	@JsonProperty("descripe")
	public String getDescripe() {
		return descripe;
	}

	public void setDescripe(String descripe) {
		this.descripe =  descripe;
	}
	@JsonProperty("remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark =  remark;
	}
	@JsonProperty("creater")
	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater =  creater;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("playerId")
	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId =  playerId;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type =  type;
	}
	@JsonProperty("level")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level =  level;
	}
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status =  status;
	}
	@JsonProperty("tt")
	public String getTt() {
		return tt;
	}

	public void setTt(String tt) {
		this.tt =  tt;
	}
	@JsonProperty("tf")
	public String getTf() {
		return tf;
	}

	public void setTf(String tf) {
		this.tf =  tf;
	}
	@JsonProperty("bonus")
	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus =  bonus;
	}

																												
	public MyItemDetail(Long id,String itemNo,String ratio,String descripe,String remark,Long creater,Date createTime,Long playerId,String type,String level,String status,String tt,String tf,BigDecimal bonus) {
				
		this.id = id;
				
		this.itemNo = itemNo;
				
		this.ratio = ratio;
				
		this.descripe = descripe;
				
		this.remark = remark;
				
		this.creater = creater;
				
		this.createTime = createTime;
				
		this.playerId = playerId;
				
		this.type = type;
				
		this.level = level;
				
		this.status = status;
				
		this.tt = tt;
				
		this.tf = tf;
				
		this.bonus = bonus;
				
	}

	public MyItemDetail() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private String playerIdName;
	private String itemtype;

	public String getItemtype() {
		return itemtype;
	}

	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}

	public String getPlayerIdName() {
		return playerIdName;
	}

	public void setPlayerIdName(String playerIdName) {
		this.playerIdName = playerIdName;
	}
	private String itemName;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	private String createName;
	private String itemNamexx;

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getItemNamexx() {
		return itemNamexx;
	}

	public void setItemNamexx(String itemNamexx) {
		this.itemNamexx = itemNamexx;
	}
	private String itemtypeName;

	public String getItemtypeName() {
		return itemtypeName;
	}

	public void setItemtypeName(String itemtypeName) {
		this.itemtypeName = itemtypeName;
	}
	
	
}