package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class Buhuo implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "编号")
	private String code;
	
	@ApiModelProperty(value = "机器编号")
	private String machineId;
	
	@ApiModelProperty(value = "机器线路")
	private String lineId;
	
	@ApiModelProperty(value = "机器点位")
	private String pointId;
	
	@ApiModelProperty(value = "补货")
	private String userId;
	
	@ApiModelProperty(value = "补货人")
	private String userName;
	
	@ApiModelProperty(value = "补货内容")
	private String buhuoDetail;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@ApiModelProperty(value = "备注")
	private Integer notes;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code =  code;
	}
	@JsonProperty("machineId")
	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId =  machineId;
	}
	@JsonProperty("lineId")
	public String getLineId() {
		return lineId;
	}

	public void setLineId(String lineId) {
		this.lineId =  lineId;
	}
	@JsonProperty("pointId")
	public String getPointId() {
		return pointId;
	}

	public void setPointId(String pointId) {
		this.pointId =  pointId;
	}
	@JsonProperty("userId")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId =  userId;
	}
	@JsonProperty("userName")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName =  userName;
	}
	@JsonProperty("buhuoDetail")
	public String getBuhuoDetail() {
		return buhuoDetail;
	}

	public void setBuhuoDetail(String buhuoDetail) {
		this.buhuoDetail =  buhuoDetail;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("notes")
	public Integer getNotes() {
		return notes;
	}

	public void setNotes(Integer notes) {
		this.notes =  notes;
	}

																						
	public Buhuo(Long id,String code,String machineId,String lineId,String pointId,String userId,String userName,String buhuoDetail,Date createdTime,Integer status,Integer notes) {
				
		this.id = id;
				
		this.code = code;
				
		this.machineId = machineId;
				
		this.lineId = lineId;
				
		this.pointId = pointId;
				
		this.userId = userId;
				
		this.userName = userName;
				
		this.buhuoDetail = buhuoDetail;
				
		this.createdTime = createdTime;
				
		this.status = status;
				
		this.notes = notes;
				
	}

	public Buhuo() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}