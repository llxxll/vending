package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class Feneun implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "商户")
	private String fMerchant;
	
	@ApiModelProperty(value = "设备数量")
	private String deviceNum;
	
	@ApiModelProperty(value = "分润数量")
	private String fRegion;
	
	@ApiModelProperty(value = "分润比例")
	private String fProportion;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@ApiModelProperty(value = "备注")
	private Integer notes;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("fMerchant")
	public String getFMerchant() {
		return fMerchant;
	}

	public void setFMerchant(String fMerchant) {
		this.fMerchant =  fMerchant;
	}
	@JsonProperty("deviceNum")
	public String getDeviceNum() {
		return deviceNum;
	}

	public void setDeviceNum(String deviceNum) {
		this.deviceNum =  deviceNum;
	}
	@JsonProperty("fRegion")
	public String getFRegion() {
		return fRegion;
	}

	public void setFRegion(String fRegion) {
		this.fRegion =  fRegion;
	}
	@JsonProperty("fProportion")
	public String getFProportion() {
		return fProportion;
	}

	public void setFProportion(String fProportion) {
		this.fProportion =  fProportion;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("notes")
	public Integer getNotes() {
		return notes;
	}

	public void setNotes(Integer notes) {
		this.notes =  notes;
	}

																
	public Feneun(Long id,String fMerchant,String deviceNum,String fRegion,String fProportion,Date createdTime,Integer status,Integer notes) {
				
		this.id = id;
				
		this.fMerchant = fMerchant;
				
		this.deviceNum = deviceNum;
				
		this.fRegion = fRegion;
				
		this.fProportion = fProportion;
				
		this.createdTime = createdTime;
				
		this.status = status;
				
		this.notes = notes;
				
	}

	public Feneun() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}