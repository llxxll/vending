package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 提现审核 MoneyReviewExample
 * @author fuce_自动生成
 * @date 2024-01-23 22:17:52
 */
public class MoneyReviewExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MoneyReviewExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andTransferCompanyIsNull() {
            addCriterion("transfer_company is null");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyIsNotNull() {
            addCriterion("transfer_company is not null");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyEqualTo(String value) {
            addCriterion("transfer_company =", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyNotEqualTo(String value) {
            addCriterion("transfer_company <>", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyGreaterThan(String value) {
            addCriterion("transfer_company >", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("transfer_company >=", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyLessThan(String value) {
            addCriterion("transfer_company <", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyLessThanOrEqualTo(String value) {
            addCriterion("transfer_company <=", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyLike(String value) {
            addCriterion("transfer_company like", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyNotLike(String value) {
            addCriterion("transfer_company not like", value, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyIn(List<String> values) {
            addCriterion("transfer_company in", values, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyNotIn(List<String> values) {
            addCriterion("transfer_company not in", values, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyBetween(String value1, String value2) {
            addCriterion("transfer_company between", value1, value2, "transferCompany");
            return (Criteria) this;
        }

        public Criteria andTransferCompanyNotBetween(String value1, String value2) {
            addCriterion("transfer_company not between", value1, value2, "transferCompany");
            return (Criteria) this;
        }
        
				
        public Criteria andWithdrawalCompanyIsNull() {
            addCriterion("withdrawal_company is null");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyIsNotNull() {
            addCriterion("withdrawal_company is not null");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyEqualTo(String value) {
            addCriterion("withdrawal_company =", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyNotEqualTo(String value) {
            addCriterion("withdrawal_company <>", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyGreaterThan(String value) {
            addCriterion("withdrawal_company >", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("withdrawal_company >=", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyLessThan(String value) {
            addCriterion("withdrawal_company <", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyLessThanOrEqualTo(String value) {
            addCriterion("withdrawal_company <=", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyLike(String value) {
            addCriterion("withdrawal_company like", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyNotLike(String value) {
            addCriterion("withdrawal_company not like", value, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyIn(List<String> values) {
            addCriterion("withdrawal_company in", values, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyNotIn(List<String> values) {
            addCriterion("withdrawal_company not in", values, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyBetween(String value1, String value2) {
            addCriterion("withdrawal_company between", value1, value2, "withdrawalCompany");
            return (Criteria) this;
        }

        public Criteria andWithdrawalCompanyNotBetween(String value1, String value2) {
            addCriterion("withdrawal_company not between", value1, value2, "withdrawalCompany");
            return (Criteria) this;
        }
        
				
        public Criteria andNumberIsNull() {
            addCriterion("number is null");
            return (Criteria) this;
        }

        public Criteria andNumberIsNotNull() {
            addCriterion("number is not null");
            return (Criteria) this;
        }

        public Criteria andNumberEqualTo(String value) {
            addCriterion("number =", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotEqualTo(String value) {
            addCriterion("number <>", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberGreaterThan(String value) {
            addCriterion("number >", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberGreaterThanOrEqualTo(String value) {
            addCriterion("number >=", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberLessThan(String value) {
            addCriterion("number <", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberLessThanOrEqualTo(String value) {
            addCriterion("number <=", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberLike(String value) {
            addCriterion("number like", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotLike(String value) {
            addCriterion("number not like", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberIn(List<String> values) {
            addCriterion("number in", values, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotIn(List<String> values) {
            addCriterion("number not in", values, "number");
            return (Criteria) this;
        }

        public Criteria andNumberBetween(String value1, String value2) {
            addCriterion("number between", value1, value2, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotBetween(String value1, String value2) {
            addCriterion("number not between", value1, value2, "number");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLike(Date value) {
            addCriterion("created_time like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotLike(Date value) {
            addCriterion("created_time not like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }
        
				
        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(Integer value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(Integer value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
        
				
        public Criteria andTStatusIsNull() {
            addCriterion("t_status is null");
            return (Criteria) this;
        }

        public Criteria andTStatusIsNotNull() {
            addCriterion("t_status is not null");
            return (Criteria) this;
        }

        public Criteria andTStatusEqualTo(Integer value) {
            addCriterion("t_status =", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusNotEqualTo(Integer value) {
            addCriterion("t_status <>", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusGreaterThan(Integer value) {
            addCriterion("t_status >", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("t_status >=", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusLessThan(Integer value) {
            addCriterion("t_status <", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusLessThanOrEqualTo(Integer value) {
            addCriterion("t_status <=", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusLike(Integer value) {
            addCriterion("t_status like", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusNotLike(Integer value) {
            addCriterion("t_status not like", value, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusIn(List<Integer> values) {
            addCriterion("t_status in", values, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusNotIn(List<Integer> values) {
            addCriterion("t_status not in", values, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusBetween(Integer value1, Integer value2) {
            addCriterion("t_status between", value1, value2, "tStatus");
            return (Criteria) this;
        }

        public Criteria andTStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("t_status not between", value1, value2, "tStatus");
            return (Criteria) this;
        }
        
				
        public Criteria andNotesIsNull() {
            addCriterion("notes is null");
            return (Criteria) this;
        }

        public Criteria andNotesIsNotNull() {
            addCriterion("notes is not null");
            return (Criteria) this;
        }

        public Criteria andNotesEqualTo(Integer value) {
            addCriterion("notes =", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotEqualTo(Integer value) {
            addCriterion("notes <>", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThan(Integer value) {
            addCriterion("notes >", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThanOrEqualTo(Integer value) {
            addCriterion("notes >=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThan(Integer value) {
            addCriterion("notes <", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThanOrEqualTo(Integer value) {
            addCriterion("notes <=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLike(Integer value) {
            addCriterion("notes like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotLike(Integer value) {
            addCriterion("notes not like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesIn(List<Integer> values) {
            addCriterion("notes in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotIn(List<Integer> values) {
            addCriterion("notes not in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesBetween(Integer value1, Integer value2) {
            addCriterion("notes between", value1, value2, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotBetween(Integer value1, Integer value2) {
            addCriterion("notes not between", value1, value2, "notes");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(MoneyReview record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getTransferCompany()!=null&&StrUtil.isNotEmpty(record.getTransferCompany().toString())) {
    			 list.add("ifnull(transfer_company,'')");
    		}
			if(record.getWithdrawalCompany()!=null&&StrUtil.isNotEmpty(record.getWithdrawalCompany().toString())) {
    			 list.add("ifnull(withdrawal_company,'')");
    		}
			if(record.getNumber()!=null&&StrUtil.isNotEmpty(record.getNumber().toString())) {
    			 list.add("ifnull(number,'')");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			 list.add("ifnull(created_time,'')");
    		}
			if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			 list.add("ifnull(status,'')");
    		}
			if(record.getTStatus()!=null&&StrUtil.isNotEmpty(record.getTStatus().toString())) {
    			 list.add("ifnull(t_status,'')");
    		}
			if(record.getNotes()!=null&&StrUtil.isNotEmpty(record.getNotes().toString())) {
    			 list.add("ifnull(notes,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getTransferCompany()!=null&&StrUtil.isNotEmpty(record.getTransferCompany().toString())) {
    			list2.add("'%"+record.getTransferCompany()+"%'");
    		}
			if(record.getWithdrawalCompany()!=null&&StrUtil.isNotEmpty(record.getWithdrawalCompany().toString())) {
    			list2.add("'%"+record.getWithdrawalCompany()+"%'");
    		}
			if(record.getNumber()!=null&&StrUtil.isNotEmpty(record.getNumber().toString())) {
    			list2.add("'%"+record.getNumber()+"%'");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			list2.add("'%"+record.getCreatedTime()+"%'");
    		}
			if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			list2.add("'%"+record.getStatus()+"%'");
    		}
			if(record.getTStatus()!=null&&StrUtil.isNotEmpty(record.getTStatus().toString())) {
    			list2.add("'%"+record.getTStatus()+"%'");
    		}
			if(record.getNotes()!=null&&StrUtil.isNotEmpty(record.getNotes().toString())) {
    			list2.add("'%"+record.getNotes()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(transfer_company,'')");
    		list.add("ifnull(withdrawal_company,'')");
    		list.add("ifnull(number,'')");
    		list.add("ifnull(created_time,'')");
    		list.add("ifnull(status,'')");
    		list.add("ifnull(t_status,'')");
    		list.add("ifnull(notes,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}