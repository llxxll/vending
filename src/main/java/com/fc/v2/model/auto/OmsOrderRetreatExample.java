package com.fc.v2.model.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 销售退货表 OmsOrderRetreatExample
 * @author lxl_自动生成
 * @date 2022-09-22 22:34:43
 */
public class OmsOrderRetreatExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OmsOrderRetreatExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andSidIsNull() {
            addCriterion("sid is null");
            return (Criteria) this;
        }

        public Criteria andSidIsNotNull() {
            addCriterion("sid is not null");
            return (Criteria) this;
        }

        public Criteria andSidEqualTo(Long value) {
            addCriterion("sid =", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotEqualTo(Long value) {
            addCriterion("sid <>", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThan(Long value) {
            addCriterion("sid >", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThanOrEqualTo(Long value) {
            addCriterion("sid >=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThan(Long value) {
            addCriterion("sid <", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThanOrEqualTo(Long value) {
            addCriterion("sid <=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLike(Long value) {
            addCriterion("sid like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotLike(Long value) {
            addCriterion("sid not like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidIn(List<Long> values) {
            addCriterion("sid in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotIn(List<Long> values) {
            addCriterion("sid not in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidBetween(Long value1, Long value2) {
            addCriterion("sid between", value1, value2, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotBetween(Long value1, Long value2) {
            addCriterion("sid not between", value1, value2, "sid");
            return (Criteria) this;
        }
        
				
        public Criteria andOrderNoIsNull() {
            addCriterion("order_no is null");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNotNull() {
            addCriterion("order_no is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNoEqualTo(String value) {
            addCriterion("order_no =", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotEqualTo(String value) {
            addCriterion("order_no <>", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThan(String value) {
            addCriterion("order_no >", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("order_no >=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThan(String value) {
            addCriterion("order_no <", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThanOrEqualTo(String value) {
            addCriterion("order_no <=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLike(String value) {
            addCriterion("order_no like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotLike(String value) {
            addCriterion("order_no not like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoIn(List<String> values) {
            addCriterion("order_no in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotIn(List<String> values) {
            addCriterion("order_no not in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoBetween(String value1, String value2) {
            addCriterion("order_no between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotBetween(String value1, String value2) {
            addCriterion("order_no not between", value1, value2, "orderNo");
            return (Criteria) this;
        }
        
				
        public Criteria andBuySupplySidIsNull() {
            addCriterion("buy_supply_sid is null");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidIsNotNull() {
            addCriterion("buy_supply_sid is not null");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidEqualTo(Long value) {
            addCriterion("buy_supply_sid =", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidNotEqualTo(Long value) {
            addCriterion("buy_supply_sid <>", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidGreaterThan(Long value) {
            addCriterion("buy_supply_sid >", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidGreaterThanOrEqualTo(Long value) {
            addCriterion("buy_supply_sid >=", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidLessThan(Long value) {
            addCriterion("buy_supply_sid <", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidLessThanOrEqualTo(Long value) {
            addCriterion("buy_supply_sid <=", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidLike(Long value) {
            addCriterion("buy_supply_sid like", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidNotLike(Long value) {
            addCriterion("buy_supply_sid not like", value, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidIn(List<Long> values) {
            addCriterion("buy_supply_sid in", values, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidNotIn(List<Long> values) {
            addCriterion("buy_supply_sid not in", values, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidBetween(Long value1, Long value2) {
            addCriterion("buy_supply_sid between", value1, value2, "buySupplySid");
            return (Criteria) this;
        }

        public Criteria andBuySupplySidNotBetween(Long value1, Long value2) {
            addCriterion("buy_supply_sid not between", value1, value2, "buySupplySid");
            return (Criteria) this;
        }
        
				
        public Criteria andBuySupplyNameIsNull() {
            addCriterion("buy_supply_name is null");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameIsNotNull() {
            addCriterion("buy_supply_name is not null");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameEqualTo(String value) {
            addCriterion("buy_supply_name =", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameNotEqualTo(String value) {
            addCriterion("buy_supply_name <>", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameGreaterThan(String value) {
            addCriterion("buy_supply_name >", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameGreaterThanOrEqualTo(String value) {
            addCriterion("buy_supply_name >=", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameLessThan(String value) {
            addCriterion("buy_supply_name <", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameLessThanOrEqualTo(String value) {
            addCriterion("buy_supply_name <=", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameLike(String value) {
            addCriterion("buy_supply_name like", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameNotLike(String value) {
            addCriterion("buy_supply_name not like", value, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameIn(List<String> values) {
            addCriterion("buy_supply_name in", values, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameNotIn(List<String> values) {
            addCriterion("buy_supply_name not in", values, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameBetween(String value1, String value2) {
            addCriterion("buy_supply_name between", value1, value2, "buySupplyName");
            return (Criteria) this;
        }

        public Criteria andBuySupplyNameNotBetween(String value1, String value2) {
            addCriterion("buy_supply_name not between", value1, value2, "buySupplyName");
            return (Criteria) this;
        }
        
				
        public Criteria andSellSupplySidIsNull() {
            addCriterion("sell_supply_sid is null");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidIsNotNull() {
            addCriterion("sell_supply_sid is not null");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidEqualTo(Long value) {
            addCriterion("sell_supply_sid =", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidNotEqualTo(Long value) {
            addCriterion("sell_supply_sid <>", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidGreaterThan(Long value) {
            addCriterion("sell_supply_sid >", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidGreaterThanOrEqualTo(Long value) {
            addCriterion("sell_supply_sid >=", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidLessThan(Long value) {
            addCriterion("sell_supply_sid <", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidLessThanOrEqualTo(Long value) {
            addCriterion("sell_supply_sid <=", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidLike(Long value) {
            addCriterion("sell_supply_sid like", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidNotLike(Long value) {
            addCriterion("sell_supply_sid not like", value, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidIn(List<Long> values) {
            addCriterion("sell_supply_sid in", values, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidNotIn(List<Long> values) {
            addCriterion("sell_supply_sid not in", values, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidBetween(Long value1, Long value2) {
            addCriterion("sell_supply_sid between", value1, value2, "sellSupplySid");
            return (Criteria) this;
        }

        public Criteria andSellSupplySidNotBetween(Long value1, Long value2) {
            addCriterion("sell_supply_sid not between", value1, value2, "sellSupplySid");
            return (Criteria) this;
        }
        
				
        public Criteria andSellSupplyNameIsNull() {
            addCriterion("sell_supply_name is null");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameIsNotNull() {
            addCriterion("sell_supply_name is not null");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameEqualTo(String value) {
            addCriterion("sell_supply_name =", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameNotEqualTo(String value) {
            addCriterion("sell_supply_name <>", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameGreaterThan(String value) {
            addCriterion("sell_supply_name >", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameGreaterThanOrEqualTo(String value) {
            addCriterion("sell_supply_name >=", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameLessThan(String value) {
            addCriterion("sell_supply_name <", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameLessThanOrEqualTo(String value) {
            addCriterion("sell_supply_name <=", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameLike(String value) {
            addCriterion("sell_supply_name like", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameNotLike(String value) {
            addCriterion("sell_supply_name not like", value, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameIn(List<String> values) {
            addCriterion("sell_supply_name in", values, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameNotIn(List<String> values) {
            addCriterion("sell_supply_name not in", values, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameBetween(String value1, String value2) {
            addCriterion("sell_supply_name between", value1, value2, "sellSupplyName");
            return (Criteria) this;
        }

        public Criteria andSellSupplyNameNotBetween(String value1, String value2) {
            addCriterion("sell_supply_name not between", value1, value2, "sellSupplyName");
            return (Criteria) this;
        }
        
				
        public Criteria andUserSidIsNull() {
            addCriterion("user_sid is null");
            return (Criteria) this;
        }

        public Criteria andUserSidIsNotNull() {
            addCriterion("user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andUserSidEqualTo(Long value) {
            addCriterion("user_sid =", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidNotEqualTo(Long value) {
            addCriterion("user_sid <>", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidGreaterThan(Long value) {
            addCriterion("user_sid >", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("user_sid >=", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidLessThan(Long value) {
            addCriterion("user_sid <", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidLessThanOrEqualTo(Long value) {
            addCriterion("user_sid <=", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidLike(Long value) {
            addCriterion("user_sid like", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidNotLike(Long value) {
            addCriterion("user_sid not like", value, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidIn(List<Long> values) {
            addCriterion("user_sid in", values, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidNotIn(List<Long> values) {
            addCriterion("user_sid not in", values, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidBetween(Long value1, Long value2) {
            addCriterion("user_sid between", value1, value2, "userSid");
            return (Criteria) this;
        }

        public Criteria andUserSidNotBetween(Long value1, Long value2) {
            addCriterion("user_sid not between", value1, value2, "userSid");
            return (Criteria) this;
        }
        
				
        public Criteria andOrderSourceSidIsNull() {
            addCriterion("order_source_sid is null");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidIsNotNull() {
            addCriterion("order_source_sid is not null");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidEqualTo(Long value) {
            addCriterion("order_source_sid =", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidNotEqualTo(Long value) {
            addCriterion("order_source_sid <>", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidGreaterThan(Long value) {
            addCriterion("order_source_sid >", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidGreaterThanOrEqualTo(Long value) {
            addCriterion("order_source_sid >=", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidLessThan(Long value) {
            addCriterion("order_source_sid <", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidLessThanOrEqualTo(Long value) {
            addCriterion("order_source_sid <=", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidLike(Long value) {
            addCriterion("order_source_sid like", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidNotLike(Long value) {
            addCriterion("order_source_sid not like", value, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidIn(List<Long> values) {
            addCriterion("order_source_sid in", values, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidNotIn(List<Long> values) {
            addCriterion("order_source_sid not in", values, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidBetween(Long value1, Long value2) {
            addCriterion("order_source_sid between", value1, value2, "orderSourceSid");
            return (Criteria) this;
        }

        public Criteria andOrderSourceSidNotBetween(Long value1, Long value2) {
            addCriterion("order_source_sid not between", value1, value2, "orderSourceSid");
            return (Criteria) this;
        }
        
				
        public Criteria andOrderStatusIsNull() {
            addCriterion("order_status is null");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIsNotNull() {
            addCriterion("order_status is not null");
            return (Criteria) this;
        }

        public Criteria andOrderStatusEqualTo(Integer value) {
            addCriterion("order_status =", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotEqualTo(Integer value) {
            addCriterion("order_status <>", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusGreaterThan(Integer value) {
            addCriterion("order_status >", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_status >=", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLessThan(Integer value) {
            addCriterion("order_status <", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLessThanOrEqualTo(Integer value) {
            addCriterion("order_status <=", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLike(Integer value) {
            addCriterion("order_status like", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotLike(Integer value) {
            addCriterion("order_status not like", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIn(List<Integer> values) {
            addCriterion("order_status in", values, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotIn(List<Integer> values) {
            addCriterion("order_status not in", values, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusBetween(Integer value1, Integer value2) {
            addCriterion("order_status between", value1, value2, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("order_status not between", value1, value2, "orderStatus");
            return (Criteria) this;
        }
        
				
        public Criteria andHangBitIsNull() {
            addCriterion("hang_bit is null");
            return (Criteria) this;
        }

        public Criteria andHangBitIsNotNull() {
            addCriterion("hang_bit is not null");
            return (Criteria) this;
        }

        public Criteria andHangBitEqualTo(Integer value) {
            addCriterion("hang_bit =", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitNotEqualTo(Integer value) {
            addCriterion("hang_bit <>", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitGreaterThan(Integer value) {
            addCriterion("hang_bit >", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitGreaterThanOrEqualTo(Integer value) {
            addCriterion("hang_bit >=", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitLessThan(Integer value) {
            addCriterion("hang_bit <", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitLessThanOrEqualTo(Integer value) {
            addCriterion("hang_bit <=", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitLike(Integer value) {
            addCriterion("hang_bit like", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitNotLike(Integer value) {
            addCriterion("hang_bit not like", value, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitIn(List<Integer> values) {
            addCriterion("hang_bit in", values, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitNotIn(List<Integer> values) {
            addCriterion("hang_bit not in", values, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitBetween(Integer value1, Integer value2) {
            addCriterion("hang_bit between", value1, value2, "hangBit");
            return (Criteria) this;
        }

        public Criteria andHangBitNotBetween(Integer value1, Integer value2) {
            addCriterion("hang_bit not between", value1, value2, "hangBit");
            return (Criteria) this;
        }
        
				
        public Criteria andProductNumIsNull() {
            addCriterion("product_num is null");
            return (Criteria) this;
        }

        public Criteria andProductNumIsNotNull() {
            addCriterion("product_num is not null");
            return (Criteria) this;
        }

        public Criteria andProductNumEqualTo(Integer value) {
            addCriterion("product_num =", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumNotEqualTo(Integer value) {
            addCriterion("product_num <>", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumGreaterThan(Integer value) {
            addCriterion("product_num >", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("product_num >=", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumLessThan(Integer value) {
            addCriterion("product_num <", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumLessThanOrEqualTo(Integer value) {
            addCriterion("product_num <=", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumLike(Integer value) {
            addCriterion("product_num like", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumNotLike(Integer value) {
            addCriterion("product_num not like", value, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumIn(List<Integer> values) {
            addCriterion("product_num in", values, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumNotIn(List<Integer> values) {
            addCriterion("product_num not in", values, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumBetween(Integer value1, Integer value2) {
            addCriterion("product_num between", value1, value2, "productNum");
            return (Criteria) this;
        }

        public Criteria andProductNumNotBetween(Integer value1, Integer value2) {
            addCriterion("product_num not between", value1, value2, "productNum");
            return (Criteria) this;
        }
        
				
        public Criteria andPostageIsNull() {
            addCriterion("postage is null");
            return (Criteria) this;
        }

        public Criteria andPostageIsNotNull() {
            addCriterion("postage is not null");
            return (Criteria) this;
        }

        public Criteria andPostageEqualTo(Long value) {
            addCriterion("postage =", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageNotEqualTo(Long value) {
            addCriterion("postage <>", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageGreaterThan(Long value) {
            addCriterion("postage >", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageGreaterThanOrEqualTo(Long value) {
            addCriterion("postage >=", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageLessThan(Long value) {
            addCriterion("postage <", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageLessThanOrEqualTo(Long value) {
            addCriterion("postage <=", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageLike(Long value) {
            addCriterion("postage like", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageNotLike(Long value) {
            addCriterion("postage not like", value, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageIn(List<Long> values) {
            addCriterion("postage in", values, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageNotIn(List<Long> values) {
            addCriterion("postage not in", values, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageBetween(Long value1, Long value2) {
            addCriterion("postage between", value1, value2, "postage");
            return (Criteria) this;
        }

        public Criteria andPostageNotBetween(Long value1, Long value2) {
            addCriterion("postage not between", value1, value2, "postage");
            return (Criteria) this;
        }
        
				
        public Criteria andProductMoneyIsNull() {
            addCriterion("product_money is null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNotNull() {
            addCriterion("product_money is not null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyEqualTo(BigDecimal value) {
            addCriterion("product_money =", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotEqualTo(BigDecimal value) {
            addCriterion("product_money <>", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThan(BigDecimal value) {
            addCriterion("product_money >", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money >=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThan(BigDecimal value) {
            addCriterion("product_money <", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money <=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLike(BigDecimal value) {
            addCriterion("product_money like", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotLike(BigDecimal value) {
            addCriterion("product_money not like", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIn(List<BigDecimal> values) {
            addCriterion("product_money in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotIn(List<BigDecimal> values) {
            addCriterion("product_money not in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money not between", value1, value2, "productMoney");
            return (Criteria) this;
        }
        
				
        public Criteria andOrderMoneyIsNull() {
            addCriterion("order_money is null");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyIsNotNull() {
            addCriterion("order_money is not null");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyEqualTo(Long value) {
            addCriterion("order_money =", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyNotEqualTo(Long value) {
            addCriterion("order_money <>", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyGreaterThan(Long value) {
            addCriterion("order_money >", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyGreaterThanOrEqualTo(Long value) {
            addCriterion("order_money >=", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyLessThan(Long value) {
            addCriterion("order_money <", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyLessThanOrEqualTo(Long value) {
            addCriterion("order_money <=", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyLike(Long value) {
            addCriterion("order_money like", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyNotLike(Long value) {
            addCriterion("order_money not like", value, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyIn(List<Long> values) {
            addCriterion("order_money in", values, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyNotIn(List<Long> values) {
            addCriterion("order_money not in", values, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyBetween(Long value1, Long value2) {
            addCriterion("order_money between", value1, value2, "orderMoney");
            return (Criteria) this;
        }

        public Criteria andOrderMoneyNotBetween(Long value1, Long value2) {
            addCriterion("order_money not between", value1, value2, "orderMoney");
            return (Criteria) this;
        }
        
				
        public Criteria andBarginMoneyIsNull() {
            addCriterion("bargin_money is null");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyIsNotNull() {
            addCriterion("bargin_money is not null");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyEqualTo(BigDecimal value) {
            addCriterion("bargin_money =", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyNotEqualTo(BigDecimal value) {
            addCriterion("bargin_money <>", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyGreaterThan(BigDecimal value) {
            addCriterion("bargin_money >", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bargin_money >=", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyLessThan(BigDecimal value) {
            addCriterion("bargin_money <", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bargin_money <=", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyLike(BigDecimal value) {
            addCriterion("bargin_money like", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyNotLike(BigDecimal value) {
            addCriterion("bargin_money not like", value, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyIn(List<BigDecimal> values) {
            addCriterion("bargin_money in", values, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyNotIn(List<BigDecimal> values) {
            addCriterion("bargin_money not in", values, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bargin_money between", value1, value2, "barginMoney");
            return (Criteria) this;
        }

        public Criteria andBarginMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bargin_money not between", value1, value2, "barginMoney");
            return (Criteria) this;
        }
        
				
        public Criteria andPayMoneyIsNull() {
            addCriterion("pay_money is null");
            return (Criteria) this;
        }

        public Criteria andPayMoneyIsNotNull() {
            addCriterion("pay_money is not null");
            return (Criteria) this;
        }

        public Criteria andPayMoneyEqualTo(BigDecimal value) {
            addCriterion("pay_money =", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyNotEqualTo(BigDecimal value) {
            addCriterion("pay_money <>", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyGreaterThan(BigDecimal value) {
            addCriterion("pay_money >", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("pay_money >=", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyLessThan(BigDecimal value) {
            addCriterion("pay_money <", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("pay_money <=", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyLike(BigDecimal value) {
            addCriterion("pay_money like", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyNotLike(BigDecimal value) {
            addCriterion("pay_money not like", value, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyIn(List<BigDecimal> values) {
            addCriterion("pay_money in", values, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyNotIn(List<BigDecimal> values) {
            addCriterion("pay_money not in", values, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pay_money between", value1, value2, "payMoney");
            return (Criteria) this;
        }

        public Criteria andPayMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pay_money not between", value1, value2, "payMoney");
            return (Criteria) this;
        }
        
				
        public Criteria andDeliveryTypeSidIsNull() {
            addCriterion("delivery_type_sid is null");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidIsNotNull() {
            addCriterion("delivery_type_sid is not null");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidEqualTo(Integer value) {
            addCriterion("delivery_type_sid =", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidNotEqualTo(Integer value) {
            addCriterion("delivery_type_sid <>", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidGreaterThan(Integer value) {
            addCriterion("delivery_type_sid >", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidGreaterThanOrEqualTo(Integer value) {
            addCriterion("delivery_type_sid >=", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidLessThan(Integer value) {
            addCriterion("delivery_type_sid <", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidLessThanOrEqualTo(Integer value) {
            addCriterion("delivery_type_sid <=", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidLike(Integer value) {
            addCriterion("delivery_type_sid like", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidNotLike(Integer value) {
            addCriterion("delivery_type_sid not like", value, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidIn(List<Integer> values) {
            addCriterion("delivery_type_sid in", values, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidNotIn(List<Integer> values) {
            addCriterion("delivery_type_sid not in", values, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidBetween(Integer value1, Integer value2) {
            addCriterion("delivery_type_sid between", value1, value2, "deliveryTypeSid");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeSidNotBetween(Integer value1, Integer value2) {
            addCriterion("delivery_type_sid not between", value1, value2, "deliveryTypeSid");
            return (Criteria) this;
        }
        
				
        public Criteria andRefundFlagIsNull() {
            addCriterion("refund_flag is null");
            return (Criteria) this;
        }

        public Criteria andRefundFlagIsNotNull() {
            addCriterion("refund_flag is not null");
            return (Criteria) this;
        }

        public Criteria andRefundFlagEqualTo(Integer value) {
            addCriterion("refund_flag =", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagNotEqualTo(Integer value) {
            addCriterion("refund_flag <>", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagGreaterThan(Integer value) {
            addCriterion("refund_flag >", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("refund_flag >=", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagLessThan(Integer value) {
            addCriterion("refund_flag <", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagLessThanOrEqualTo(Integer value) {
            addCriterion("refund_flag <=", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagLike(Integer value) {
            addCriterion("refund_flag like", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagNotLike(Integer value) {
            addCriterion("refund_flag not like", value, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagIn(List<Integer> values) {
            addCriterion("refund_flag in", values, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagNotIn(List<Integer> values) {
            addCriterion("refund_flag not in", values, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagBetween(Integer value1, Integer value2) {
            addCriterion("refund_flag between", value1, value2, "refundFlag");
            return (Criteria) this;
        }

        public Criteria andRefundFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("refund_flag not between", value1, value2, "refundFlag");
            return (Criteria) this;
        }
        
				
        public Criteria andDeleteFlagIsNull() {
            addCriterion("delete_flag is null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIsNotNull() {
            addCriterion("delete_flag is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagEqualTo(Integer value) {
            addCriterion("delete_flag =", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotEqualTo(Integer value) {
            addCriterion("delete_flag <>", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThan(Integer value) {
            addCriterion("delete_flag >", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("delete_flag >=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThan(Integer value) {
            addCriterion("delete_flag <", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLessThanOrEqualTo(Integer value) {
            addCriterion("delete_flag <=", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagLike(Integer value) {
            addCriterion("delete_flag like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotLike(Integer value) {
            addCriterion("delete_flag not like", value, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagIn(List<Integer> values) {
            addCriterion("delete_flag in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotIn(List<Integer> values) {
            addCriterion("delete_flag not in", values, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagBetween(Integer value1, Integer value2) {
            addCriterion("delete_flag between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }

        public Criteria andDeleteFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("delete_flag not between", value1, value2, "deleteFlag");
            return (Criteria) this;
        }
        
				
        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(Date value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(Date value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
        
				
        public Criteria andTsIsNull() {
            addCriterion("ts is null");
            return (Criteria) this;
        }

        public Criteria andTsIsNotNull() {
            addCriterion("ts is not null");
            return (Criteria) this;
        }

        public Criteria andTsEqualTo(Date value) {
            addCriterion("ts =", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotEqualTo(Date value) {
            addCriterion("ts <>", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsGreaterThan(Date value) {
            addCriterion("ts >", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsGreaterThanOrEqualTo(Date value) {
            addCriterion("ts >=", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLessThan(Date value) {
            addCriterion("ts <", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLessThanOrEqualTo(Date value) {
            addCriterion("ts <=", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLike(Date value) {
            addCriterion("ts like", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotLike(Date value) {
            addCriterion("ts not like", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsIn(List<Date> values) {
            addCriterion("ts in", values, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotIn(List<Date> values) {
            addCriterion("ts not in", values, "ts");
            return (Criteria) this;
        }

        public Criteria andTsBetween(Date value1, Date value2) {
            addCriterion("ts between", value1, value2, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotBetween(Date value1, Date value2) {
            addCriterion("ts not between", value1, value2, "ts");
            return (Criteria) this;
        }
        
				
        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }
        
				
        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
        
				
        public Criteria andExpressIsNull() {
            addCriterion("express is null");
            return (Criteria) this;
        }

        public Criteria andExpressIsNotNull() {
            addCriterion("express is not null");
            return (Criteria) this;
        }

        public Criteria andExpressEqualTo(String value) {
            addCriterion("express =", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressNotEqualTo(String value) {
            addCriterion("express <>", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressGreaterThan(String value) {
            addCriterion("express >", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressGreaterThanOrEqualTo(String value) {
            addCriterion("express >=", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressLessThan(String value) {
            addCriterion("express <", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressLessThanOrEqualTo(String value) {
            addCriterion("express <=", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressLike(String value) {
            addCriterion("express like", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressNotLike(String value) {
            addCriterion("express not like", value, "express");
            return (Criteria) this;
        }

        public Criteria andExpressIn(List<String> values) {
            addCriterion("express in", values, "express");
            return (Criteria) this;
        }

        public Criteria andExpressNotIn(List<String> values) {
            addCriterion("express not in", values, "express");
            return (Criteria) this;
        }

        public Criteria andExpressBetween(String value1, String value2) {
            addCriterion("express between", value1, value2, "express");
            return (Criteria) this;
        }

        public Criteria andExpressNotBetween(String value1, String value2) {
            addCriterion("express not between", value1, value2, "express");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(OmsOrderRetreat record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			 list.add("ifnull(sid,'')");
    		}
			if(record.getOrderNo()!=null&&StrUtil.isNotEmpty(record.getOrderNo().toString())) {
    			 list.add("ifnull(order_no,'')");
    		}
			if(record.getBuySupplySid()!=null&&StrUtil.isNotEmpty(record.getBuySupplySid().toString())) {
    			 list.add("ifnull(buy_supply_sid,'')");
    		}
			if(record.getBuySupplyName()!=null&&StrUtil.isNotEmpty(record.getBuySupplyName().toString())) {
    			 list.add("ifnull(buy_supply_name,'')");
    		}
			if(record.getSellSupplySid()!=null&&StrUtil.isNotEmpty(record.getSellSupplySid().toString())) {
    			 list.add("ifnull(sell_supply_sid,'')");
    		}
			if(record.getSellSupplyName()!=null&&StrUtil.isNotEmpty(record.getSellSupplyName().toString())) {
    			 list.add("ifnull(sell_supply_name,'')");
    		}
			if(record.getUserSid()!=null&&StrUtil.isNotEmpty(record.getUserSid().toString())) {
    			 list.add("ifnull(user_sid,'')");
    		}
			if(record.getOrderSourceSid()!=null&&StrUtil.isNotEmpty(record.getOrderSourceSid().toString())) {
    			 list.add("ifnull(order_source_sid,'')");
    		}
			if(record.getOrderStatus()!=null&&StrUtil.isNotEmpty(record.getOrderStatus().toString())) {
    			 list.add("ifnull(order_status,'')");
    		}
			if(record.getHangBit()!=null&&StrUtil.isNotEmpty(record.getHangBit().toString())) {
    			 list.add("ifnull(hang_bit,'')");
    		}
			if(record.getProductNum()!=null&&StrUtil.isNotEmpty(record.getProductNum().toString())) {
    			 list.add("ifnull(product_num,'')");
    		}
			if(record.getPostage()!=null&&StrUtil.isNotEmpty(record.getPostage().toString())) {
    			 list.add("ifnull(postage,'')");
    		}
			if(record.getProductMoney()!=null&&StrUtil.isNotEmpty(record.getProductMoney().toString())) {
    			 list.add("ifnull(product_money,'')");
    		}
			if(record.getOrderMoney()!=null&&StrUtil.isNotEmpty(record.getOrderMoney().toString())) {
    			 list.add("ifnull(order_money,'')");
    		}
			if(record.getBarginMoney()!=null&&StrUtil.isNotEmpty(record.getBarginMoney().toString())) {
    			 list.add("ifnull(bargin_money,'')");
    		}
			if(record.getPayMoney()!=null&&StrUtil.isNotEmpty(record.getPayMoney().toString())) {
    			 list.add("ifnull(pay_money,'')");
    		}
			if(record.getDeliveryTypeSid()!=null&&StrUtil.isNotEmpty(record.getDeliveryTypeSid().toString())) {
    			 list.add("ifnull(delivery_type_sid,'')");
    		}
			if(record.getRefundFlag()!=null&&StrUtil.isNotEmpty(record.getRefundFlag().toString())) {
    			 list.add("ifnull(refund_flag,'')");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			 list.add("ifnull(delete_flag,'')");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			 list.add("ifnull(create_time,'')");
    		}
			if(record.getTs()!=null&&StrUtil.isNotEmpty(record.getTs().toString())) {
    			 list.add("ifnull(ts,'')");
    		}
			if(record.getAddress()!=null&&StrUtil.isNotEmpty(record.getAddress().toString())) {
    			 list.add("ifnull(address,'')");
    		}
			if(record.getRemark()!=null&&StrUtil.isNotEmpty(record.getRemark().toString())) {
    			 list.add("ifnull(remark,'')");
    		}
			if(record.getExpress()!=null&&StrUtil.isNotEmpty(record.getExpress().toString())) {
    			 list.add("ifnull(express,'')");
    		}
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			list2.add("'%"+record.getSid()+"%'");
    		}
			if(record.getOrderNo()!=null&&StrUtil.isNotEmpty(record.getOrderNo().toString())) {
    			list2.add("'%"+record.getOrderNo()+"%'");
    		}
			if(record.getBuySupplySid()!=null&&StrUtil.isNotEmpty(record.getBuySupplySid().toString())) {
    			list2.add("'%"+record.getBuySupplySid()+"%'");
    		}
			if(record.getBuySupplyName()!=null&&StrUtil.isNotEmpty(record.getBuySupplyName().toString())) {
    			list2.add("'%"+record.getBuySupplyName()+"%'");
    		}
			if(record.getSellSupplySid()!=null&&StrUtil.isNotEmpty(record.getSellSupplySid().toString())) {
    			list2.add("'%"+record.getSellSupplySid()+"%'");
    		}
			if(record.getSellSupplyName()!=null&&StrUtil.isNotEmpty(record.getSellSupplyName().toString())) {
    			list2.add("'%"+record.getSellSupplyName()+"%'");
    		}
			if(record.getUserSid()!=null&&StrUtil.isNotEmpty(record.getUserSid().toString())) {
    			list2.add("'%"+record.getUserSid()+"%'");
    		}
			if(record.getOrderSourceSid()!=null&&StrUtil.isNotEmpty(record.getOrderSourceSid().toString())) {
    			list2.add("'%"+record.getOrderSourceSid()+"%'");
    		}
			if(record.getOrderStatus()!=null&&StrUtil.isNotEmpty(record.getOrderStatus().toString())) {
    			list2.add("'%"+record.getOrderStatus()+"%'");
    		}
			if(record.getHangBit()!=null&&StrUtil.isNotEmpty(record.getHangBit().toString())) {
    			list2.add("'%"+record.getHangBit()+"%'");
    		}
			if(record.getProductNum()!=null&&StrUtil.isNotEmpty(record.getProductNum().toString())) {
    			list2.add("'%"+record.getProductNum()+"%'");
    		}
			if(record.getPostage()!=null&&StrUtil.isNotEmpty(record.getPostage().toString())) {
    			list2.add("'%"+record.getPostage()+"%'");
    		}
			if(record.getProductMoney()!=null&&StrUtil.isNotEmpty(record.getProductMoney().toString())) {
    			list2.add("'%"+record.getProductMoney()+"%'");
    		}
			if(record.getOrderMoney()!=null&&StrUtil.isNotEmpty(record.getOrderMoney().toString())) {
    			list2.add("'%"+record.getOrderMoney()+"%'");
    		}
			if(record.getBarginMoney()!=null&&StrUtil.isNotEmpty(record.getBarginMoney().toString())) {
    			list2.add("'%"+record.getBarginMoney()+"%'");
    		}
			if(record.getPayMoney()!=null&&StrUtil.isNotEmpty(record.getPayMoney().toString())) {
    			list2.add("'%"+record.getPayMoney()+"%'");
    		}
			if(record.getDeliveryTypeSid()!=null&&StrUtil.isNotEmpty(record.getDeliveryTypeSid().toString())) {
    			list2.add("'%"+record.getDeliveryTypeSid()+"%'");
    		}
			if(record.getRefundFlag()!=null&&StrUtil.isNotEmpty(record.getRefundFlag().toString())) {
    			list2.add("'%"+record.getRefundFlag()+"%'");
    		}
			if(record.getDeleteFlag()!=null&&StrUtil.isNotEmpty(record.getDeleteFlag().toString())) {
    			list2.add("'%"+record.getDeleteFlag()+"%'");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			list2.add("'%"+record.getCreateTime()+"%'");
    		}
			if(record.getTs()!=null&&StrUtil.isNotEmpty(record.getTs().toString())) {
    			list2.add("'%"+record.getTs()+"%'");
    		}
			if(record.getAddress()!=null&&StrUtil.isNotEmpty(record.getAddress().toString())) {
    			list2.add("'%"+record.getAddress()+"%'");
    		}
			if(record.getRemark()!=null&&StrUtil.isNotEmpty(record.getRemark().toString())) {
    			list2.add("'%"+record.getRemark()+"%'");
    		}
			if(record.getExpress()!=null&&StrUtil.isNotEmpty(record.getExpress().toString())) {
    			list2.add("'%"+record.getExpress()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(sid,'')");
    		list.add("ifnull(order_no,'')");
    		list.add("ifnull(buy_supply_sid,'')");
    		list.add("ifnull(buy_supply_name,'')");
    		list.add("ifnull(sell_supply_sid,'')");
    		list.add("ifnull(sell_supply_name,'')");
    		list.add("ifnull(user_sid,'')");
    		list.add("ifnull(order_source_sid,'')");
    		list.add("ifnull(order_status,'')");
    		list.add("ifnull(hang_bit,'')");
    		list.add("ifnull(product_num,'')");
    		list.add("ifnull(postage,'')");
    		list.add("ifnull(product_money,'')");
    		list.add("ifnull(order_money,'')");
    		list.add("ifnull(bargin_money,'')");
    		list.add("ifnull(pay_money,'')");
    		list.add("ifnull(delivery_type_sid,'')");
    		list.add("ifnull(refund_flag,'')");
    		list.add("ifnull(delete_flag,'')");
    		list.add("ifnull(create_time,'')");
    		list.add("ifnull(ts,'')");
    		list.add("ifnull(address,'')");
    		list.add("ifnull(remark,'')");
    		list.add("ifnull(express,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}