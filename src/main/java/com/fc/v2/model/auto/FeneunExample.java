package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 分润 FeneunExample
 * @author fuce_自动生成
 * @date 2024-01-23 22:17:56
 */
public class FeneunExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FeneunExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andFMerchantIsNull() {
            addCriterion("f_merchant is null");
            return (Criteria) this;
        }

        public Criteria andFMerchantIsNotNull() {
            addCriterion("f_merchant is not null");
            return (Criteria) this;
        }

        public Criteria andFMerchantEqualTo(String value) {
            addCriterion("f_merchant =", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantNotEqualTo(String value) {
            addCriterion("f_merchant <>", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantGreaterThan(String value) {
            addCriterion("f_merchant >", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantGreaterThanOrEqualTo(String value) {
            addCriterion("f_merchant >=", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantLessThan(String value) {
            addCriterion("f_merchant <", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantLessThanOrEqualTo(String value) {
            addCriterion("f_merchant <=", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantLike(String value) {
            addCriterion("f_merchant like", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantNotLike(String value) {
            addCriterion("f_merchant not like", value, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantIn(List<String> values) {
            addCriterion("f_merchant in", values, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantNotIn(List<String> values) {
            addCriterion("f_merchant not in", values, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantBetween(String value1, String value2) {
            addCriterion("f_merchant between", value1, value2, "fMerchant");
            return (Criteria) this;
        }

        public Criteria andFMerchantNotBetween(String value1, String value2) {
            addCriterion("f_merchant not between", value1, value2, "fMerchant");
            return (Criteria) this;
        }
        
				
        public Criteria andDeviceNumIsNull() {
            addCriterion("device_num is null");
            return (Criteria) this;
        }

        public Criteria andDeviceNumIsNotNull() {
            addCriterion("device_num is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceNumEqualTo(String value) {
            addCriterion("device_num =", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumNotEqualTo(String value) {
            addCriterion("device_num <>", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumGreaterThan(String value) {
            addCriterion("device_num >", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumGreaterThanOrEqualTo(String value) {
            addCriterion("device_num >=", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumLessThan(String value) {
            addCriterion("device_num <", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumLessThanOrEqualTo(String value) {
            addCriterion("device_num <=", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumLike(String value) {
            addCriterion("device_num like", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumNotLike(String value) {
            addCriterion("device_num not like", value, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumIn(List<String> values) {
            addCriterion("device_num in", values, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumNotIn(List<String> values) {
            addCriterion("device_num not in", values, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumBetween(String value1, String value2) {
            addCriterion("device_num between", value1, value2, "deviceNum");
            return (Criteria) this;
        }

        public Criteria andDeviceNumNotBetween(String value1, String value2) {
            addCriterion("device_num not between", value1, value2, "deviceNum");
            return (Criteria) this;
        }
        
				
        public Criteria andFRegionIsNull() {
            addCriterion("f_region is null");
            return (Criteria) this;
        }

        public Criteria andFRegionIsNotNull() {
            addCriterion("f_region is not null");
            return (Criteria) this;
        }

        public Criteria andFRegionEqualTo(String value) {
            addCriterion("f_region =", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionNotEqualTo(String value) {
            addCriterion("f_region <>", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionGreaterThan(String value) {
            addCriterion("f_region >", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionGreaterThanOrEqualTo(String value) {
            addCriterion("f_region >=", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionLessThan(String value) {
            addCriterion("f_region <", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionLessThanOrEqualTo(String value) {
            addCriterion("f_region <=", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionLike(String value) {
            addCriterion("f_region like", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionNotLike(String value) {
            addCriterion("f_region not like", value, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionIn(List<String> values) {
            addCriterion("f_region in", values, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionNotIn(List<String> values) {
            addCriterion("f_region not in", values, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionBetween(String value1, String value2) {
            addCriterion("f_region between", value1, value2, "fRegion");
            return (Criteria) this;
        }

        public Criteria andFRegionNotBetween(String value1, String value2) {
            addCriterion("f_region not between", value1, value2, "fRegion");
            return (Criteria) this;
        }
        
				
        public Criteria andFProportionIsNull() {
            addCriterion("f_proportion is null");
            return (Criteria) this;
        }

        public Criteria andFProportionIsNotNull() {
            addCriterion("f_proportion is not null");
            return (Criteria) this;
        }

        public Criteria andFProportionEqualTo(String value) {
            addCriterion("f_proportion =", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionNotEqualTo(String value) {
            addCriterion("f_proportion <>", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionGreaterThan(String value) {
            addCriterion("f_proportion >", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionGreaterThanOrEqualTo(String value) {
            addCriterion("f_proportion >=", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionLessThan(String value) {
            addCriterion("f_proportion <", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionLessThanOrEqualTo(String value) {
            addCriterion("f_proportion <=", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionLike(String value) {
            addCriterion("f_proportion like", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionNotLike(String value) {
            addCriterion("f_proportion not like", value, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionIn(List<String> values) {
            addCriterion("f_proportion in", values, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionNotIn(List<String> values) {
            addCriterion("f_proportion not in", values, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionBetween(String value1, String value2) {
            addCriterion("f_proportion between", value1, value2, "fProportion");
            return (Criteria) this;
        }

        public Criteria andFProportionNotBetween(String value1, String value2) {
            addCriterion("f_proportion not between", value1, value2, "fProportion");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLike(Date value) {
            addCriterion("created_time like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotLike(Date value) {
            addCriterion("created_time not like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }
        
				
        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(Integer value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(Integer value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
        
				
        public Criteria andNotesIsNull() {
            addCriterion("notes is null");
            return (Criteria) this;
        }

        public Criteria andNotesIsNotNull() {
            addCriterion("notes is not null");
            return (Criteria) this;
        }

        public Criteria andNotesEqualTo(Integer value) {
            addCriterion("notes =", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotEqualTo(Integer value) {
            addCriterion("notes <>", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThan(Integer value) {
            addCriterion("notes >", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThanOrEqualTo(Integer value) {
            addCriterion("notes >=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThan(Integer value) {
            addCriterion("notes <", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThanOrEqualTo(Integer value) {
            addCriterion("notes <=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLike(Integer value) {
            addCriterion("notes like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotLike(Integer value) {
            addCriterion("notes not like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesIn(List<Integer> values) {
            addCriterion("notes in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotIn(List<Integer> values) {
            addCriterion("notes not in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesBetween(Integer value1, Integer value2) {
            addCriterion("notes between", value1, value2, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotBetween(Integer value1, Integer value2) {
            addCriterion("notes not between", value1, value2, "notes");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(Feneun record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getFMerchant()!=null&&StrUtil.isNotEmpty(record.getFMerchant().toString())) {
    			 list.add("ifnull(f_merchant,'')");
    		}
			if(record.getDeviceNum()!=null&&StrUtil.isNotEmpty(record.getDeviceNum().toString())) {
    			 list.add("ifnull(device_num,'')");
    		}
			if(record.getFRegion()!=null&&StrUtil.isNotEmpty(record.getFRegion().toString())) {
    			 list.add("ifnull(f_region,'')");
    		}
			if(record.getFProportion()!=null&&StrUtil.isNotEmpty(record.getFProportion().toString())) {
    			 list.add("ifnull(f_proportion,'')");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			 list.add("ifnull(created_time,'')");
    		}
			if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			 list.add("ifnull(status,'')");
    		}
			if(record.getNotes()!=null&&StrUtil.isNotEmpty(record.getNotes().toString())) {
    			 list.add("ifnull(notes,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getFMerchant()!=null&&StrUtil.isNotEmpty(record.getFMerchant().toString())) {
    			list2.add("'%"+record.getFMerchant()+"%'");
    		}
			if(record.getDeviceNum()!=null&&StrUtil.isNotEmpty(record.getDeviceNum().toString())) {
    			list2.add("'%"+record.getDeviceNum()+"%'");
    		}
			if(record.getFRegion()!=null&&StrUtil.isNotEmpty(record.getFRegion().toString())) {
    			list2.add("'%"+record.getFRegion()+"%'");
    		}
			if(record.getFProportion()!=null&&StrUtil.isNotEmpty(record.getFProportion().toString())) {
    			list2.add("'%"+record.getFProportion()+"%'");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			list2.add("'%"+record.getCreatedTime()+"%'");
    		}
			if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			list2.add("'%"+record.getStatus()+"%'");
    		}
			if(record.getNotes()!=null&&StrUtil.isNotEmpty(record.getNotes().toString())) {
    			list2.add("'%"+record.getNotes()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(f_merchant,'')");
    		list.add("ifnull(device_num,'')");
    		list.add("ifnull(f_region,'')");
    		list.add("ifnull(f_proportion,'')");
    		list.add("ifnull(created_time,'')");
    		list.add("ifnull(status,'')");
    		list.add("ifnull(notes,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}