package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 工单明细 PlanWorkOrderDetailPzExample
 * @author fuce_自动生成
 * @date 2021-12-19 21:39:43
 */
public class PlanWorkOrderDetailPzExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlanWorkOrderDetailPzExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andWorkerOrderNoIsNull() {
            addCriterion("worker_order_no is null");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoIsNotNull() {
            addCriterion("worker_order_no is not null");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoEqualTo(String value) {
            addCriterion("worker_order_no =", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoNotEqualTo(String value) {
            addCriterion("worker_order_no <>", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoGreaterThan(String value) {
            addCriterion("worker_order_no >", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("worker_order_no >=", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoLessThan(String value) {
            addCriterion("worker_order_no <", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoLessThanOrEqualTo(String value) {
            addCriterion("worker_order_no <=", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoLike(String value) {
            addCriterion("worker_order_no like", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoNotLike(String value) {
            addCriterion("worker_order_no not like", value, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoIn(List<String> values) {
            addCriterion("worker_order_no in", values, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoNotIn(List<String> values) {
            addCriterion("worker_order_no not in", values, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoBetween(String value1, String value2) {
            addCriterion("worker_order_no between", value1, value2, "workerOrderNo");
            return (Criteria) this;
        }

        public Criteria andWorkerOrderNoNotBetween(String value1, String value2) {
            addCriterion("worker_order_no not between", value1, value2, "workerOrderNo");
            return (Criteria) this;
        }
        
				
        public Criteria andCarftOrderIsNull() {
            addCriterion("carft_order is null");
            return (Criteria) this;
        }

        public Criteria andCarftOrderIsNotNull() {
            addCriterion("carft_order is not null");
            return (Criteria) this;
        }

        public Criteria andCarftOrderEqualTo(Integer value) {
            addCriterion("carft_order =", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderNotEqualTo(Integer value) {
            addCriterion("carft_order <>", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderGreaterThan(Integer value) {
            addCriterion("carft_order >", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("carft_order >=", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderLessThan(Integer value) {
            addCriterion("carft_order <", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderLessThanOrEqualTo(Integer value) {
            addCriterion("carft_order <=", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderLike(Integer value) {
            addCriterion("carft_order like", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderNotLike(Integer value) {
            addCriterion("carft_order not like", value, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderIn(List<Integer> values) {
            addCriterion("carft_order in", values, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderNotIn(List<Integer> values) {
            addCriterion("carft_order not in", values, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderBetween(Integer value1, Integer value2) {
            addCriterion("carft_order between", value1, value2, "carftOrder");
            return (Criteria) this;
        }

        public Criteria andCarftOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("carft_order not between", value1, value2, "carftOrder");
            return (Criteria) this;
        }
        
				
        public Criteria andCarftIdIsNull() {
            addCriterion("carft_id is null");
            return (Criteria) this;
        }

        public Criteria andCarftIdIsNotNull() {
            addCriterion("carft_id is not null");
            return (Criteria) this;
        }

        public Criteria andCarftIdEqualTo(String value) {
            addCriterion("carft_id =", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotEqualTo(String value) {
            addCriterion("carft_id <>", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdGreaterThan(String value) {
            addCriterion("carft_id >", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdGreaterThanOrEqualTo(String value) {
            addCriterion("carft_id >=", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdLessThan(String value) {
            addCriterion("carft_id <", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdLessThanOrEqualTo(String value) {
            addCriterion("carft_id <=", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdLike(String value) {
            addCriterion("carft_id like", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotLike(String value) {
            addCriterion("carft_id not like", value, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdIn(List<String> values) {
            addCriterion("carft_id in", values, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotIn(List<String> values) {
            addCriterion("carft_id not in", values, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdBetween(String value1, String value2) {
            addCriterion("carft_id between", value1, value2, "carftId");
            return (Criteria) this;
        }

        public Criteria andCarftIdNotBetween(String value1, String value2) {
            addCriterion("carft_id not between", value1, value2, "carftId");
            return (Criteria) this;
        }
        
				
        public Criteria andWorkerIsNull() {
            addCriterion("worker is null");
            return (Criteria) this;
        }

        public Criteria andWorkerIsNotNull() {
            addCriterion("worker is not null");
            return (Criteria) this;
        }

        public Criteria andWorkerEqualTo(Integer value) {
            addCriterion("worker =", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerNotEqualTo(Integer value) {
            addCriterion("worker <>", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerGreaterThan(Integer value) {
            addCriterion("worker >", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerGreaterThanOrEqualTo(Integer value) {
            addCriterion("worker >=", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerLessThan(Integer value) {
            addCriterion("worker <", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerLessThanOrEqualTo(Integer value) {
            addCriterion("worker <=", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerLike(Integer value) {
            addCriterion("worker like", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerNotLike(Integer value) {
            addCriterion("worker not like", value, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerIn(List<Integer> values) {
            addCriterion("worker in", values, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerNotIn(List<Integer> values) {
            addCriterion("worker not in", values, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerBetween(Integer value1, Integer value2) {
            addCriterion("worker between", value1, value2, "worker");
            return (Criteria) this;
        }

        public Criteria andWorkerNotBetween(Integer value1, Integer value2) {
            addCriterion("worker not between", value1, value2, "worker");
            return (Criteria) this;
        }
        
				
        public Criteria andCountIsNull() {
            addCriterion("count is null");
            return (Criteria) this;
        }

        public Criteria andCountIsNotNull() {
            addCriterion("count is not null");
            return (Criteria) this;
        }

        public Criteria andCountEqualTo(Integer value) {
            addCriterion("count =", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotEqualTo(Integer value) {
            addCriterion("count <>", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThan(Integer value) {
            addCriterion("count >", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("count >=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThan(Integer value) {
            addCriterion("count <", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThanOrEqualTo(Integer value) {
            addCriterion("count <=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLike(Integer value) {
            addCriterion("count like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotLike(Integer value) {
            addCriterion("count not like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountIn(List<Integer> values) {
            addCriterion("count in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotIn(List<Integer> values) {
            addCriterion("count not in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountBetween(Integer value1, Integer value2) {
            addCriterion("count between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotBetween(Integer value1, Integer value2) {
            addCriterion("count not between", value1, value2, "count");
            return (Criteria) this;
        }
        
				
        public Criteria andUnqualifiedCountIsNull() {
            addCriterion("unqualified_count is null");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountIsNotNull() {
            addCriterion("unqualified_count is not null");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountEqualTo(Integer value) {
            addCriterion("unqualified_count =", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountNotEqualTo(Integer value) {
            addCriterion("unqualified_count <>", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountGreaterThan(Integer value) {
            addCriterion("unqualified_count >", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("unqualified_count >=", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountLessThan(Integer value) {
            addCriterion("unqualified_count <", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountLessThanOrEqualTo(Integer value) {
            addCriterion("unqualified_count <=", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountLike(Integer value) {
            addCriterion("unqualified_count like", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountNotLike(Integer value) {
            addCriterion("unqualified_count not like", value, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountIn(List<Integer> values) {
            addCriterion("unqualified_count in", values, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountNotIn(List<Integer> values) {
            addCriterion("unqualified_count not in", values, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountBetween(Integer value1, Integer value2) {
            addCriterion("unqualified_count between", value1, value2, "unqualifiedCount");
            return (Criteria) this;
        }

        public Criteria andUnqualifiedCountNotBetween(Integer value1, Integer value2) {
            addCriterion("unqualified_count not between", value1, value2, "unqualifiedCount");
            return (Criteria) this;
        }
        
				
        public Criteria andStockCountIsNull() {
            addCriterion("stock_count is null");
            return (Criteria) this;
        }

        public Criteria andStockCountIsNotNull() {
            addCriterion("stock_count is not null");
            return (Criteria) this;
        }

        public Criteria andStockCountEqualTo(Integer value) {
            addCriterion("stock_count =", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountNotEqualTo(Integer value) {
            addCriterion("stock_count <>", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountGreaterThan(Integer value) {
            addCriterion("stock_count >", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock_count >=", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountLessThan(Integer value) {
            addCriterion("stock_count <", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountLessThanOrEqualTo(Integer value) {
            addCriterion("stock_count <=", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountLike(Integer value) {
            addCriterion("stock_count like", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountNotLike(Integer value) {
            addCriterion("stock_count not like", value, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountIn(List<Integer> values) {
            addCriterion("stock_count in", values, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountNotIn(List<Integer> values) {
            addCriterion("stock_count not in", values, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountBetween(Integer value1, Integer value2) {
            addCriterion("stock_count between", value1, value2, "stockCount");
            return (Criteria) this;
        }

        public Criteria andStockCountNotBetween(Integer value1, Integer value2) {
            addCriterion("stock_count not between", value1, value2, "stockCount");
            return (Criteria) this;
        }
        
				
        public Criteria andManagerIsNull() {
            addCriterion("manager is null");
            return (Criteria) this;
        }

        public Criteria andManagerIsNotNull() {
            addCriterion("manager is not null");
            return (Criteria) this;
        }

        public Criteria andManagerEqualTo(Integer value) {
            addCriterion("manager =", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotEqualTo(Integer value) {
            addCriterion("manager <>", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThan(Integer value) {
            addCriterion("manager >", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThanOrEqualTo(Integer value) {
            addCriterion("manager >=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThan(Integer value) {
            addCriterion("manager <", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThanOrEqualTo(Integer value) {
            addCriterion("manager <=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLike(Integer value) {
            addCriterion("manager like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotLike(Integer value) {
            addCriterion("manager not like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerIn(List<Integer> values) {
            addCriterion("manager in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotIn(List<Integer> values) {
            addCriterion("manager not in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerBetween(Integer value1, Integer value2) {
            addCriterion("manager between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotBetween(Integer value1, Integer value2) {
            addCriterion("manager not between", value1, value2, "manager");
            return (Criteria) this;
        }
        
				
        public Criteria andDistributionTimeIsNull() {
            addCriterion("distribution_time is null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNotNull() {
            addCriterion("distribution_time is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeEqualTo(Date value) {
            addCriterion("distribution_time =", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotEqualTo(Date value) {
            addCriterion("distribution_time <>", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThan(Date value) {
            addCriterion("distribution_time >", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("distribution_time >=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThan(Date value) {
            addCriterion("distribution_time <", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThanOrEqualTo(Date value) {
            addCriterion("distribution_time <=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLike(Date value) {
            addCriterion("distribution_time like", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotLike(Date value) {
            addCriterion("distribution_time not like", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIn(List<Date> values) {
            addCriterion("distribution_time in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotIn(List<Date> values) {
            addCriterion("distribution_time not in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeBetween(Date value1, Date value2) {
            addCriterion("distribution_time between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotBetween(Date value1, Date value2) {
            addCriterion("distribution_time not between", value1, value2, "distributionTime");
            return (Criteria) this;
        }
        
				
        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(Integer value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(Integer value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
        
				
        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(Date value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(Date value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(Long value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(Long value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(Long value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(Long value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(Long value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(Long value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(Long value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(Long value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<Long> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<Long> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(Long value1, Long value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(Long value1, Long value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }
        
				
        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLike(Date value) {
            addCriterion("update_time like", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotLike(Date value) {
            addCriterion("update_time not like", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
        
				
        public Criteria andUpdaterIsNull() {
            addCriterion("updater is null");
            return (Criteria) this;
        }

        public Criteria andUpdaterIsNotNull() {
            addCriterion("updater is not null");
            return (Criteria) this;
        }

        public Criteria andUpdaterEqualTo(Long value) {
            addCriterion("updater =", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotEqualTo(Long value) {
            addCriterion("updater <>", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterGreaterThan(Long value) {
            addCriterion("updater >", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterGreaterThanOrEqualTo(Long value) {
            addCriterion("updater >=", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLessThan(Long value) {
            addCriterion("updater <", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLessThanOrEqualTo(Long value) {
            addCriterion("updater <=", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLike(Long value) {
            addCriterion("updater like", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotLike(Long value) {
            addCriterion("updater not like", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterIn(List<Long> values) {
            addCriterion("updater in", values, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotIn(List<Long> values) {
            addCriterion("updater not in", values, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterBetween(Long value1, Long value2) {
            addCriterion("updater between", value1, value2, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotBetween(Long value1, Long value2) {
            addCriterion("updater not between", value1, value2, "updater");
            return (Criteria) this;
        }
        
				
        public Criteria andField1IsNull() {
            addCriterion("field1 is null");
            return (Criteria) this;
        }

        public Criteria andField1IsNotNull() {
            addCriterion("field1 is not null");
            return (Criteria) this;
        }

        public Criteria andField1EqualTo(String value) {
            addCriterion("field1 =", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotEqualTo(String value) {
            addCriterion("field1 <>", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1GreaterThan(String value) {
            addCriterion("field1 >", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1GreaterThanOrEqualTo(String value) {
            addCriterion("field1 >=", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1LessThan(String value) {
            addCriterion("field1 <", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1LessThanOrEqualTo(String value) {
            addCriterion("field1 <=", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1Like(String value) {
            addCriterion("field1 like", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotLike(String value) {
            addCriterion("field1 not like", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1In(List<String> values) {
            addCriterion("field1 in", values, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotIn(List<String> values) {
            addCriterion("field1 not in", values, "field1");
            return (Criteria) this;
        }

        public Criteria andField1Between(String value1, String value2) {
            addCriterion("field1 between", value1, value2, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotBetween(String value1, String value2) {
            addCriterion("field1 not between", value1, value2, "field1");
            return (Criteria) this;
        }
        
				
        public Criteria andField2IsNull() {
            addCriterion("field2 is null");
            return (Criteria) this;
        }

        public Criteria andField2IsNotNull() {
            addCriterion("field2 is not null");
            return (Criteria) this;
        }

        public Criteria andField2EqualTo(String value) {
            addCriterion("field2 =", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotEqualTo(String value) {
            addCriterion("field2 <>", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2GreaterThan(String value) {
            addCriterion("field2 >", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2GreaterThanOrEqualTo(String value) {
            addCriterion("field2 >=", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2LessThan(String value) {
            addCriterion("field2 <", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2LessThanOrEqualTo(String value) {
            addCriterion("field2 <=", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2Like(String value) {
            addCriterion("field2 like", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotLike(String value) {
            addCriterion("field2 not like", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2In(List<String> values) {
            addCriterion("field2 in", values, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotIn(List<String> values) {
            addCriterion("field2 not in", values, "field2");
            return (Criteria) this;
        }

        public Criteria andField2Between(String value1, String value2) {
            addCriterion("field2 between", value1, value2, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotBetween(String value1, String value2) {
            addCriterion("field2 not between", value1, value2, "field2");
            return (Criteria) this;
        }
        
				
        public Criteria andField3IsNull() {
            addCriterion("field3 is null");
            return (Criteria) this;
        }

        public Criteria andField3IsNotNull() {
            addCriterion("field3 is not null");
            return (Criteria) this;
        }

        public Criteria andField3EqualTo(String value) {
            addCriterion("field3 =", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotEqualTo(String value) {
            addCriterion("field3 <>", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3GreaterThan(String value) {
            addCriterion("field3 >", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3GreaterThanOrEqualTo(String value) {
            addCriterion("field3 >=", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3LessThan(String value) {
            addCriterion("field3 <", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3LessThanOrEqualTo(String value) {
            addCriterion("field3 <=", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3Like(String value) {
            addCriterion("field3 like", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotLike(String value) {
            addCriterion("field3 not like", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3In(List<String> values) {
            addCriterion("field3 in", values, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotIn(List<String> values) {
            addCriterion("field3 not in", values, "field3");
            return (Criteria) this;
        }

        public Criteria andField3Between(String value1, String value2) {
            addCriterion("field3 between", value1, value2, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotBetween(String value1, String value2) {
            addCriterion("field3 not between", value1, value2, "field3");
            return (Criteria) this;
        }
        
				
        public Criteria andFinshTimeIsNull() {
            addCriterion("finsh_time is null");
            return (Criteria) this;
        }

        public Criteria andFinshTimeIsNotNull() {
            addCriterion("finsh_time is not null");
            return (Criteria) this;
        }

        public Criteria andFinshTimeEqualTo(Date value) {
            addCriterion("finsh_time =", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeNotEqualTo(Date value) {
            addCriterion("finsh_time <>", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeGreaterThan(Date value) {
            addCriterion("finsh_time >", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("finsh_time >=", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeLessThan(Date value) {
            addCriterion("finsh_time <", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeLessThanOrEqualTo(Date value) {
            addCriterion("finsh_time <=", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeLike(Date value) {
            addCriterion("finsh_time like", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeNotLike(Date value) {
            addCriterion("finsh_time not like", value, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeIn(List<Date> values) {
            addCriterion("finsh_time in", values, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeNotIn(List<Date> values) {
            addCriterion("finsh_time not in", values, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeBetween(Date value1, Date value2) {
            addCriterion("finsh_time between", value1, value2, "finshTime");
            return (Criteria) this;
        }

        public Criteria andFinshTimeNotBetween(Date value1, Date value2) {
            addCriterion("finsh_time not between", value1, value2, "finshTime");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(PlanWorkOrderDetailPz record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getWorkerOrderNo()!=null&&StrUtil.isNotEmpty(record.getWorkerOrderNo().toString())) {
    			 list.add("ifnull(worker_order_no,'')");
    		}
			if(record.getCarftOrder()!=null&&StrUtil.isNotEmpty(record.getCarftOrder().toString())) {
    			 list.add("ifnull(carft_order,'')");
    		}
			if(record.getCarftId()!=null&&StrUtil.isNotEmpty(record.getCarftId().toString())) {
    			 list.add("ifnull(carft_id,'')");
    		}
			if(record.getWorker()!=null&&StrUtil.isNotEmpty(record.getWorker().toString())) {
    			 list.add("ifnull(worker,'')");
    		}
			if(record.getCount()!=null&&StrUtil.isNotEmpty(record.getCount().toString())) {
    			 list.add("ifnull(count,'')");
    		}
			if(record.getUnqualifiedCount()!=null&&StrUtil.isNotEmpty(record.getUnqualifiedCount().toString())) {
    			 list.add("ifnull(unqualified_count,'')");
    		}
			if(record.getStockCount()!=null&&StrUtil.isNotEmpty(record.getStockCount().toString())) {
    			 list.add("ifnull(stock_count,'')");
    		}
			if(record.getManager()!=null&&StrUtil.isNotEmpty(record.getManager().toString())) {
    			 list.add("ifnull(manager,'')");
    		}
			if(record.getDistributionTime()!=null&&StrUtil.isNotEmpty(record.getDistributionTime().toString())) {
    			 list.add("ifnull(distribution_time,'')");
    		}
			if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			 list.add("ifnull(status,'')");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			 list.add("ifnull(create_time,'')");
    		}
			if(record.getCreator()!=null&&StrUtil.isNotEmpty(record.getCreator().toString())) {
    			 list.add("ifnull(creator,'')");
    		}
			if(record.getUpdateTime()!=null&&StrUtil.isNotEmpty(record.getUpdateTime().toString())) {
    			 list.add("ifnull(update_time,'')");
    		}
			if(record.getUpdater()!=null&&StrUtil.isNotEmpty(record.getUpdater().toString())) {
    			 list.add("ifnull(updater,'')");
    		}
			if(record.getField1()!=null&&StrUtil.isNotEmpty(record.getField1().toString())) {
    			 list.add("ifnull(field1,'')");
    		}
			if(record.getField2()!=null&&StrUtil.isNotEmpty(record.getField2().toString())) {
    			 list.add("ifnull(field2,'')");
    		}
			if(record.getField3()!=null&&StrUtil.isNotEmpty(record.getField3().toString())) {
    			 list.add("ifnull(field3,'')");
    		}
			if(record.getFinshTime()!=null&&StrUtil.isNotEmpty(record.getFinshTime().toString())) {
    			 list.add("ifnull(finsh_time,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getWorkerOrderNo()!=null&&StrUtil.isNotEmpty(record.getWorkerOrderNo().toString())) {
    			list2.add("'%"+record.getWorkerOrderNo()+"%'");
    		}
			if(record.getCarftOrder()!=null&&StrUtil.isNotEmpty(record.getCarftOrder().toString())) {
    			list2.add("'%"+record.getCarftOrder()+"%'");
    		}
			if(record.getCarftId()!=null&&StrUtil.isNotEmpty(record.getCarftId().toString())) {
    			list2.add("'%"+record.getCarftId()+"%'");
    		}
			if(record.getWorker()!=null&&StrUtil.isNotEmpty(record.getWorker().toString())) {
    			list2.add("'%"+record.getWorker()+"%'");
    		}
			if(record.getCount()!=null&&StrUtil.isNotEmpty(record.getCount().toString())) {
    			list2.add("'%"+record.getCount()+"%'");
    		}
			if(record.getUnqualifiedCount()!=null&&StrUtil.isNotEmpty(record.getUnqualifiedCount().toString())) {
    			list2.add("'%"+record.getUnqualifiedCount()+"%'");
    		}
			if(record.getStockCount()!=null&&StrUtil.isNotEmpty(record.getStockCount().toString())) {
    			list2.add("'%"+record.getStockCount()+"%'");
    		}
			if(record.getManager()!=null&&StrUtil.isNotEmpty(record.getManager().toString())) {
    			list2.add("'%"+record.getManager()+"%'");
    		}
			if(record.getDistributionTime()!=null&&StrUtil.isNotEmpty(record.getDistributionTime().toString())) {
    			list2.add("'%"+record.getDistributionTime()+"%'");
    		}
			if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			list2.add("'%"+record.getStatus()+"%'");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			list2.add("'%"+record.getCreateTime()+"%'");
    		}
			if(record.getCreator()!=null&&StrUtil.isNotEmpty(record.getCreator().toString())) {
    			list2.add("'%"+record.getCreator()+"%'");
    		}
			if(record.getUpdateTime()!=null&&StrUtil.isNotEmpty(record.getUpdateTime().toString())) {
    			list2.add("'%"+record.getUpdateTime()+"%'");
    		}
			if(record.getUpdater()!=null&&StrUtil.isNotEmpty(record.getUpdater().toString())) {
    			list2.add("'%"+record.getUpdater()+"%'");
    		}
			if(record.getField1()!=null&&StrUtil.isNotEmpty(record.getField1().toString())) {
    			list2.add("'%"+record.getField1()+"%'");
    		}
			if(record.getField2()!=null&&StrUtil.isNotEmpty(record.getField2().toString())) {
    			list2.add("'%"+record.getField2()+"%'");
    		}
			if(record.getField3()!=null&&StrUtil.isNotEmpty(record.getField3().toString())) {
    			list2.add("'%"+record.getField3()+"%'");
    		}
			if(record.getFinshTime()!=null&&StrUtil.isNotEmpty(record.getFinshTime().toString())) {
    			list2.add("'%"+record.getFinshTime()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(worker_order_no,'')");
    		list.add("ifnull(carft_order,'')");
    		list.add("ifnull(carft_id,'')");
    		list.add("ifnull(worker,'')");
    		list.add("ifnull(count,'')");
    		list.add("ifnull(unqualified_count,'')");
    		list.add("ifnull(stock_count,'')");
    		list.add("ifnull(manager,'')");
    		list.add("ifnull(distribution_time,'')");
    		list.add("ifnull(status,'')");
    		list.add("ifnull(create_time,'')");
    		list.add("ifnull(creator,'')");
    		list.add("ifnull(update_time,'')");
    		list.add("ifnull(updater,'')");
    		list.add("ifnull(field1,'')");
    		list.add("ifnull(field2,'')");
    		list.add("ifnull(field3,'')");
    		list.add("ifnull(finsh_time,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}