package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class WmsWarehouseDeliveryDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	@ApiModelProperty(value = "库存id")
	private Long stockId;
	
	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	@ApiModelProperty(value = "工单编号")
	private String deliverySid;
	
	@ApiModelProperty(value = "物料id")
	private String matterNo;
	
	@ApiModelProperty(value = "")
	private String matterDesc;
	
	@ApiModelProperty(value = "数量")
	private Long matterNumber;
	
	@ApiModelProperty(value = "计划id")
	private String unitName;
	
	@ApiModelProperty(value = "")
	private String houseNo;
	
	@ApiModelProperty(value = "1外协2车间3手动")
	private String qualityNo;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "状态2已出库1待出库")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;

	@ApiModelProperty(value = "zuida数量")
	private Long mostmatterNumber;
	public Long getMostmatterNumber() {
		return mostmatterNumber;
	}

	public void setMostmatterNumber(Long mostmatterNumber) {
		this.mostmatterNumber = mostmatterNumber;
	}
	//辅助字段
	private String tuname;
	private String tuno;
	private String chukuname;
	private String chukuspesc;
	private String houseName;
	private String createName;
	private String rukuName;
	private String beginTime;
	private String endTime;
	private String stockName;
	private String stockNumber;
	private String stockNameModel;
	private String stockNamegongyi;
	private String lingliaoren;
	

	public String getLingliaoren() {
		return lingliaoren;
	}

	public void setLingliaoren(String lingliaoren) {
		this.lingliaoren = lingliaoren;
	}

	public String getTuname() {
		return tuname;
	}

	public void setTuname(String tuname) {
		this.tuname = tuname;
	}

	public String getTuno() {
		return tuno;
	}

	public void setTuno(String tuno) {
		this.tuno = tuno;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}

	public String getStockNameModel() {
		return stockNameModel;
	}

	public void setStockNameModel(String stockNameModel) {
		this.stockNameModel = stockNameModel;
	}

	public String getStockNamegongyi() {
		return stockNamegongyi;
	}

	public void setStockNamegongyi(String stockNamegongyi) {
		this.stockNamegongyi = stockNamegongyi;
	}

	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("deliverySid")
	public String getDeliverySid() {
		return deliverySid;
	}

	public void setDeliverySid(String deliverySid) {
		this.deliverySid =  deliverySid;
	}
	@JsonProperty("matterNo")
	public String getMatterNo() {
		return matterNo;
	}

	public void setMatterNo(String matterNo) {
		this.matterNo =  matterNo;
	}
	@JsonProperty("matterDesc")
	public String getMatterDesc() {
		return matterDesc;
	}

	public void setMatterDesc(String matterDesc) {
		this.matterDesc =  matterDesc;
	}
	@JsonProperty("matterNumber")
	public Long getMatterNumber() {
		return matterNumber;
	}

	public void setMatterNumber(Long matterNumber) {
		this.matterNumber =  matterNumber;
	}
	@JsonProperty("unitName")
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName =  unitName;
	}
	@JsonProperty("houseNo")
	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo =  houseNo;
	}
	@JsonProperty("qualityNo")
	public String getQualityNo() {
		return qualityNo;
	}

	public void setQualityNo(String qualityNo) {
		this.qualityNo =  qualityNo;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}

																														
	public WmsWarehouseDeliveryDetail(Long sid,String deliverySid,String matterNo,String matterDesc,Long matterNumber,String unitName,String houseNo,String qualityNo,Long createdUserSid,Date createdTime,Integer x,Date xTime,Long xUserSid,Date modifiedTime,Long modifiedUserSid) {
				
		this.sid = sid;
				
		this.deliverySid = deliverySid;
				
		this.matterNo = matterNo;
				
		this.matterDesc = matterDesc;
				
		this.matterNumber = matterNumber;
				
		this.unitName = unitName;
				
		this.houseNo = houseNo;
				
		this.qualityNo = qualityNo;
				
		this.createdUserSid = createdUserSid;
				
		this.createdTime = createdTime;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
	}

	public WmsWarehouseDeliveryDetail() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getRukuName() {
		return rukuName;
	}

	public void setRukuName(String rukuName) {
		this.rukuName = rukuName;
	}

	public Date getxTime() {
		return xTime;
	}

	public void setxTime(Date xTime) {
		this.xTime = xTime;
	}

	public Long getxUserSid() {
		return xUserSid;
	}

	public void setxUserSid(Long xUserSid) {
		this.xUserSid = xUserSid;
	}

	public String getChukuname() {
		return chukuname;
	}

	public void setChukuname(String chukuname) {
		this.chukuname = chukuname;
	}

	public String getChukuspesc() {
		return chukuspesc;
	}

	public void setChukuspesc(String chukuspesc) {
		this.chukuspesc = chukuspesc;
	}
}