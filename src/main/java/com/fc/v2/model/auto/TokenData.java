package com.fc.v2.model.auto;

import java.io.Serializable;

public class TokenData implements Serializable{

	
	private String token;
	private Integer tokenCode;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getTokenCode() {
		return tokenCode;
	}
	public void setTokenCode(Integer tokenCode) {
		this.tokenCode = tokenCode;
	}
	
	
}
