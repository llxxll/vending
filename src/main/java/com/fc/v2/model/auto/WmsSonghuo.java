package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class WmsSonghuo implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	
	@ApiModelProperty(value = "委外计划id")
	private String deliverySid;
	
	@ApiModelProperty(value = "不合格")
	private String matterNo;
	
	@ApiModelProperty(value = "废料")
	private String matterDesc;
	
	@ApiModelProperty(value = "数量")
	private Long matterNumber;
	
	@ApiModelProperty(value = "委外单id")
	private String unitName;
	
	@ApiModelProperty(value = "状态0待质检1待入库2已入库")
	private String houseNo;
	
	@ApiModelProperty(value = "")
	private String qualityNo;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Integer x;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date xTime;
	
	@ApiModelProperty(value = "")
	private Long xUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	
	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("deliverySid")
	public String getDeliverySid() {
		return deliverySid;
	}

	public void setDeliverySid(String deliverySid) {
		this.deliverySid =  deliverySid;
	}
	@JsonProperty("matterNo")
	public String getMatterNo() {
		return matterNo;
	}

	public void setMatterNo(String matterNo) {
		this.matterNo =  matterNo;
	}
	@JsonProperty("matterDesc")
	public String getMatterDesc() {
		return matterDesc;
	}

	public void setMatterDesc(String matterDesc) {
		this.matterDesc =  matterDesc;
	}
	@JsonProperty("matterNumber")
	public Long getMatterNumber() {
		return matterNumber;
	}

	public void setMatterNumber(Long matterNumber) {
		this.matterNumber =  matterNumber;
	}
	@JsonProperty("unitName")
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName =  unitName;
	}
	@JsonProperty("houseNo")
	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo =  houseNo;
	}
	@JsonProperty("qualityNo")
	public String getQualityNo() {
		return qualityNo;
	}

	public void setQualityNo(String qualityNo) {
		this.qualityNo =  qualityNo;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("x")
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x =  x;
	}
	@JsonProperty("xTime")
	public Date getXTime() {
		return xTime;
	}

	public void setXTime(Date xTime) {
		this.xTime =  xTime;
	}
	@JsonProperty("xUserSid")
	public Long getXUserSid() {
		return xUserSid;
	}

	public void setXUserSid(Long xUserSid) {
		this.xUserSid =  xUserSid;
	}
	
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}

																														
	public WmsSonghuo(Long sid,String deliverySid,String matterNo,String matterDesc,Long matterNumber,String unitName,String houseNo,String qualityNo,Long createdUserSid,Date createdTime,Integer x,Date xTime,Long xUserSid,Date modifiedTime,Long modifiedUserSid) {
				
		this.sid = sid;
				
		this.deliverySid = deliverySid;
				
		this.matterNo = matterNo;
				
		this.matterDesc = matterDesc;
				
		this.matterNumber = matterNumber;
				
		this.unitName = unitName;
				
		this.houseNo = houseNo;
				
		this.qualityNo = qualityNo;
				
		this.createdUserSid = createdUserSid;
				
		this.createdTime = createdTime;
				
		this.x = x;
				
		this.xTime = xTime;
				
		this.xUserSid = xUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
	}

	public WmsSonghuo() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
	private String tuname;
	private String tuno;
	private String departmentName;
	private String createname;
	private String houseName;
	private String xName;
	

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getxName() {
		return xName;
	}

	public void setxName(String xName) {
		this.xName = xName;
	}

	

	public String getTuname() {
		return tuname;
	}

	public void setTuname(String tuname) {
		this.tuname = tuname;
	}

	public String getTuno() {
		return tuno;
	}

	public void setTuno(String tuno) {
		this.tuno = tuno;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getCreatename() {
		return createname;
	}

	public void setCreatename(String createname) {
		this.createname = createname;
	}

}