package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class MyItemType implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "")
	private String typeName;
	
	@ApiModelProperty(value = "")
	private String ttxx;
	
	@ApiModelProperty(value = "")
	private String ttff;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private Long createUser;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("typeName")
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName =  typeName;
	}
	@JsonProperty("ttxx")
	public String getTtxx() {
		return ttxx;
	}

	public void setTtxx(String ttxx) {
		this.ttxx =  ttxx;
	}
	@JsonProperty("ttff")
	public String getTtff() {
		return ttff;
	}

	public void setTtff(String ttff) {
		this.ttff =  ttff;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("createUser")
	public Long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Long createUser) {
		this.createUser =  createUser;
	}

												
	public MyItemType(Long id,String typeName,String ttxx,String ttff,Date createTime,Long createUser) {
				
		this.id = id;
				
		this.typeName = typeName;
				
		this.ttxx = ttxx;
				
		this.ttff = ttff;
				
		this.createTime = createTime;
				
		this.createUser = createUser;
				
	}

	public MyItemType() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	private String createUserName;

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	

}