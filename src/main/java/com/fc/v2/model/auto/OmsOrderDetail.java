package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;
import java.util.List;

public class OmsOrderDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	
	@ApiModelProperty(value = "产品id")
	private String itemNo;
	
	@ApiModelProperty(value = "订单号")
	private String orderNo;
	
	@ApiModelProperty(value = "物料ID")
	private Long productDetailSid;
	
	@ApiModelProperty(value = "产品id")
	private Long productSid;
	
	@ApiModelProperty(value = "")
	private String productTitle;
	
	@ApiModelProperty(value = "")
	private String productAlias;
	
	@ApiModelProperty(value = "")
	private String productSpecific;
	
	@ApiModelProperty(value = "单位")
	private String unitName;
	
	@ApiModelProperty(value = "销售价")
	private BigDecimal xiaoshouPrice;
	
	@ApiModelProperty(value = "")
	private String categorySid;
	
	@ApiModelProperty(value = "")
	private Long supplySid;
	
	@ApiModelProperty(value = "工厂SID")
	private Long shopSid;
	
	@ApiModelProperty(value = "")
	private Long stockTypeSid;
	
	@ApiModelProperty(value = "数量")
	private Long itemNum;
	
	@ApiModelProperty(value = "")
	private Long price;
	
	@ApiModelProperty(value = "退货数量")
	private Long refundNum;
	
	@ApiModelProperty(value = "")
	private String imgUrl;
	
	@ApiModelProperty(value = "")
	private String str1;
	
	@ApiModelProperty(value = "")
	private String str2;
	
	@ApiModelProperty(value = "")
	private Long num1;
	
	@ApiModelProperty(value = "")
	private Integer num2;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "技术字段")
	private Date ts;
	private List<String> supplySids;
	private List<String> stockTypeSids;
	private List<String> spuNames;
	private List<String> spuAssembleNames;

	public List<String> getSpuNames() {
		return spuNames;
	}

	public void setSpuNames(List<String> spuNames) {
		this.spuNames = spuNames;
	}

	public List<String> getSpuAssembleNames() {
		return spuAssembleNames;
	}

	public void setSpuAssembleNames(List<String> spuAssembleNames) {
		this.spuAssembleNames = spuAssembleNames;
	}

	public List<String> getSupplySids() {
		return supplySids;
	}

	public void setSupplySids(List<String> supplySids) {
		this.supplySids = supplySids;
	}

	public List<String> getStockTypeSids() {
		return stockTypeSids;
	}

	public void setStockTypeSids(List<String> stockTypeSids) {
		this.stockTypeSids = stockTypeSids;
	}

	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("itemNo")
	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo =  itemNo;
	}
	@JsonProperty("orderNo")
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo =  orderNo;
	}
	@JsonProperty("productDetailSid")
	public Long getProductDetailSid() {
		return productDetailSid;
	}

	public void setProductDetailSid(Long productDetailSid) {
		this.productDetailSid =  productDetailSid;
	}
	@JsonProperty("productSid")
	public Long getProductSid() {
		return productSid;
	}

	public void setProductSid(Long productSid) {
		this.productSid =  productSid;
	}
	@JsonProperty("productTitle")
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle =  productTitle;
	}
	@JsonProperty("productAlias")
	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias =  productAlias;
	}
	@JsonProperty("productSpecific")
	public String getProductSpecific() {
		return productSpecific;
	}

	public void setProductSpecific(String productSpecific) {
		this.productSpecific =  productSpecific;
	}
	@JsonProperty("unitName")
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName =  unitName;
	}
	@JsonProperty("xiaoshouPrice")
	public BigDecimal getXiaoshouPrice() {
		return xiaoshouPrice;
	}

	public void setXiaoshouPrice(BigDecimal xiaoshouPrice) {
		this.xiaoshouPrice =  xiaoshouPrice;
	}
	@JsonProperty("categorySid")
	public String getCategorySid() {
		return categorySid;
	}

	public void setCategorySid(String categorySid) {
		this.categorySid =  categorySid;
	}
	@JsonProperty("supplySid")
	public Long getSupplySid() {
		return supplySid;
	}

	public void setSupplySid(Long supplySid) {
		this.supplySid =  supplySid;
	}
	@JsonProperty("shopSid")
	public Long getShopSid() {
		return shopSid;
	}

	public void setShopSid(Long shopSid) {
		this.shopSid =  shopSid;
	}
	@JsonProperty("stockTypeSid")
	public Long getStockTypeSid() {
		return stockTypeSid;
	}

	public void setStockTypeSid(Long stockTypeSid) {
		this.stockTypeSid =  stockTypeSid;
	}
	@JsonProperty("itemNum")
	public Long getItemNum() {
		return itemNum;
	}

	public void setItemNum(Long itemNum) {
		this.itemNum =  itemNum;
	}
	@JsonProperty("price")
	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price =  price;
	}
	@JsonProperty("refundNum")
	public Long getRefundNum() {
		return refundNum;
	}

	public void setRefundNum(Long refundNum) {
		this.refundNum =  refundNum;
	}
	@JsonProperty("imgUrl")
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl =  imgUrl;
	}
	@JsonProperty("str1")
	public String getStr1() {
		return str1;
	}

	public void setStr1(String str1) {
		this.str1 =  str1;
	}
	@JsonProperty("str2")
	public String getStr2() {
		return str2;
	}

	public void setStr2(String str2) {
		this.str2 =  str2;
	}
	@JsonProperty("num1")
	public Long getNum1() {
		return num1;
	}

	public void setNum1(Long num1) {
		this.num1 =  num1;
	}
	@JsonProperty("num2")
	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 =  num2;
	}
	@JsonProperty("ts")
	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts =  ts;
	}

																																														
	public OmsOrderDetail(Long sid,String itemNo,String orderNo,Long productDetailSid,Long productSid,String productTitle,String productAlias,String productSpecific,String unitName,BigDecimal xiaoshouPrice,String categorySid,Long supplySid,Long shopSid,Long stockTypeSid,Long itemNum,Long price,Long refundNum,String imgUrl,String str1,String str2,Long num1,Integer num2,Date ts) {
				
		this.sid = sid;
				
		this.itemNo = itemNo;
				
		this.orderNo = orderNo;
				
		this.productDetailSid = productDetailSid;
				
		this.productSid = productSid;
				
		this.productTitle = productTitle;
				
		this.productAlias = productAlias;
				
		this.productSpecific = productSpecific;
				
		this.unitName = unitName;
				
		this.xiaoshouPrice = xiaoshouPrice;
				
		this.categorySid = categorySid;
				
		this.supplySid = supplySid;
				
		this.shopSid = shopSid;
				
		this.stockTypeSid = stockTypeSid;
				
		this.itemNum = itemNum;
				
		this.price = price;
				
		this.refundNum = refundNum;
				
		this.imgUrl = imgUrl;
				
		this.str1 = str1;
				
		this.str2 = str2;
				
		this.num1 = num1;
				
		this.num2 = num2;
				
		this.ts = ts;
				
	}

	public OmsOrderDetail() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
	
	private String buyName;
	private Long categroysid;
	private String categroyName;
	private String createName;
	private String productTitleName;
	private Long tuihuonumber;//退货在途数
	private Long yutuihuonumber;//退货在途数
	
	public Long getYutuihuonumber() {
		return yutuihuonumber;
	}

	public void setYutuihuonumber(Long yutuihuonumber) {
		this.yutuihuonumber = yutuihuonumber;
	}

	public Long getTuihuonumber() {
		return tuihuonumber;
	}

	public void setTuihuonumber(Long tuihuonumber) {
		this.tuihuonumber = tuihuonumber;
	}

	public String getProductTitleName() {
		return productTitleName;
	}

	public void setProductTitleName(String productTitleName) {
		this.productTitleName = productTitleName;
	}

	public Long getCategroysid() {
		return categroysid;
	}

	public void setCategroysid(Long categroysid) {
		this.categroysid = categroysid;
	}

	public String getBuyName() {
		return buyName;
	}

	public void setBuyName(String buyName) {
		this.buyName = buyName;
	}

	public String getCategroyName() {
		return categroyName;
	}

	public void setCategroyName(String categroyName) {
		this.categroyName = categroyName;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}
	private List<Long> delId;

	public List<Long> getDelId() {
		return delId;
	}

	public void setDelId(List<Long> delId) {
		this.delId = delId;
	}
	private Long buySupplySid;

	public Long getBuySupplySid() {
		return buySupplySid;
	}

	public void setBuySupplySid(Long buySupplySid) {
		this.buySupplySid = buySupplySid;
	}
	private String beginTime;
	private String endTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}