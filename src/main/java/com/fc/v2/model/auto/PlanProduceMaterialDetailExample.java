package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 生产计划明细 PlanProduceMaterialDetailExample
 * @author fuce_自动生成
 * @date 2021-12-19 20:11:36
 */
public class PlanProduceMaterialDetailExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlanProduceMaterialDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Long value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Long value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andProduceNoIsNull() {
            addCriterion("produce_no is null");
            return (Criteria) this;
        }

        public Criteria andProduceNoIsNotNull() {
            addCriterion("produce_no is not null");
            return (Criteria) this;
        }

        public Criteria andProduceNoEqualTo(String value) {
            addCriterion("produce_no =", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoNotEqualTo(String value) {
            addCriterion("produce_no <>", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoGreaterThan(String value) {
            addCriterion("produce_no >", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoGreaterThanOrEqualTo(String value) {
            addCriterion("produce_no >=", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoLessThan(String value) {
            addCriterion("produce_no <", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoLessThanOrEqualTo(String value) {
            addCriterion("produce_no <=", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoLike(String value) {
            addCriterion("produce_no like", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoNotLike(String value) {
            addCriterion("produce_no not like", value, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoIn(List<String> values) {
            addCriterion("produce_no in", values, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoNotIn(List<String> values) {
            addCriterion("produce_no not in", values, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoBetween(String value1, String value2) {
            addCriterion("produce_no between", value1, value2, "produceNo");
            return (Criteria) this;
        }

        public Criteria andProduceNoNotBetween(String value1, String value2) {
            addCriterion("produce_no not between", value1, value2, "produceNo");
            return (Criteria) this;
        }
        
				
        public Criteria andMateriaNoIsNull() {
            addCriterion("materia_no is null");
            return (Criteria) this;
        }

        public Criteria andMateriaNoIsNotNull() {
            addCriterion("materia_no is not null");
            return (Criteria) this;
        }

        public Criteria andMateriaNoEqualTo(String value) {
            addCriterion("materia_no =", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoNotEqualTo(String value) {
            addCriterion("materia_no <>", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoGreaterThan(String value) {
            addCriterion("materia_no >", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoGreaterThanOrEqualTo(String value) {
            addCriterion("materia_no >=", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoLessThan(String value) {
            addCriterion("materia_no <", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoLessThanOrEqualTo(String value) {
            addCriterion("materia_no <=", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoLike(String value) {
            addCriterion("materia_no like", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoNotLike(String value) {
            addCriterion("materia_no not like", value, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoIn(List<String> values) {
            addCriterion("materia_no in", values, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoNotIn(List<String> values) {
            addCriterion("materia_no not in", values, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoBetween(String value1, String value2) {
            addCriterion("materia_no between", value1, value2, "materiaNo");
            return (Criteria) this;
        }

        public Criteria andMateriaNoNotBetween(String value1, String value2) {
            addCriterion("materia_no not between", value1, value2, "materiaNo");
            return (Criteria) this;
        }
        
				
        public Criteria andMaterialNameIsNull() {
            addCriterion("material_name is null");
            return (Criteria) this;
        }

        public Criteria andMaterialNameIsNotNull() {
            addCriterion("material_name is not null");
            return (Criteria) this;
        }

        public Criteria andMaterialNameEqualTo(String value) {
            addCriterion("material_name =", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameNotEqualTo(String value) {
            addCriterion("material_name <>", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameGreaterThan(String value) {
            addCriterion("material_name >", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameGreaterThanOrEqualTo(String value) {
            addCriterion("material_name >=", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameLessThan(String value) {
            addCriterion("material_name <", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameLessThanOrEqualTo(String value) {
            addCriterion("material_name <=", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameLike(String value) {
            addCriterion("material_name like", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameNotLike(String value) {
            addCriterion("material_name not like", value, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameIn(List<String> values) {
            addCriterion("material_name in", values, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameNotIn(List<String> values) {
            addCriterion("material_name not in", values, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameBetween(String value1, String value2) {
            addCriterion("material_name between", value1, value2, "materialName");
            return (Criteria) this;
        }

        public Criteria andMaterialNameNotBetween(String value1, String value2) {
            addCriterion("material_name not between", value1, value2, "materialName");
            return (Criteria) this;
        }
        
				
        public Criteria andStandardIsNull() {
            addCriterion("standard is null");
            return (Criteria) this;
        }

        public Criteria andStandardIsNotNull() {
            addCriterion("standard is not null");
            return (Criteria) this;
        }

        public Criteria andStandardEqualTo(String value) {
            addCriterion("standard =", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotEqualTo(String value) {
            addCriterion("standard <>", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardGreaterThan(String value) {
            addCriterion("standard >", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardGreaterThanOrEqualTo(String value) {
            addCriterion("standard >=", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLessThan(String value) {
            addCriterion("standard <", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLessThanOrEqualTo(String value) {
            addCriterion("standard <=", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLike(String value) {
            addCriterion("standard like", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotLike(String value) {
            addCriterion("standard not like", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardIn(List<String> values) {
            addCriterion("standard in", values, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotIn(List<String> values) {
            addCriterion("standard not in", values, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardBetween(String value1, String value2) {
            addCriterion("standard between", value1, value2, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotBetween(String value1, String value2) {
            addCriterion("standard not between", value1, value2, "standard");
            return (Criteria) this;
        }
        
				
        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(Long value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(Long value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(Long value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(Long value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(Long value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(Long value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLike(Long value) {
            addCriterion("stock like", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotLike(Long value) {
            addCriterion("stock not like", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<Long> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<Long> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(Long value1, Long value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(Long value1, Long value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }
        
				
        public Criteria andMaterialIsNull() {
            addCriterion("material is null");
            return (Criteria) this;
        }

        public Criteria andMaterialIsNotNull() {
            addCriterion("material is not null");
            return (Criteria) this;
        }

        public Criteria andMaterialEqualTo(String value) {
            addCriterion("material =", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialNotEqualTo(String value) {
            addCriterion("material <>", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialGreaterThan(String value) {
            addCriterion("material >", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialGreaterThanOrEqualTo(String value) {
            addCriterion("material >=", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialLessThan(String value) {
            addCriterion("material <", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialLessThanOrEqualTo(String value) {
            addCriterion("material <=", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialLike(String value) {
            addCriterion("material like", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialNotLike(String value) {
            addCriterion("material not like", value, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialIn(List<String> values) {
            addCriterion("material in", values, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialNotIn(List<String> values) {
            addCriterion("material not in", values, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialBetween(String value1, String value2) {
            addCriterion("material between", value1, value2, "material");
            return (Criteria) this;
        }

        public Criteria andMaterialNotBetween(String value1, String value2) {
            addCriterion("material not between", value1, value2, "material");
            return (Criteria) this;
        }
        
				
        public Criteria andProduceTimeIsNull() {
            addCriterion("produce_time is null");
            return (Criteria) this;
        }

        public Criteria andProduceTimeIsNotNull() {
            addCriterion("produce_time is not null");
            return (Criteria) this;
        }

        public Criteria andProduceTimeEqualTo(Date value) {
            addCriterion("produce_time =", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeNotEqualTo(Date value) {
            addCriterion("produce_time <>", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeGreaterThan(Date value) {
            addCriterion("produce_time >", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("produce_time >=", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeLessThan(Date value) {
            addCriterion("produce_time <", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeLessThanOrEqualTo(Date value) {
            addCriterion("produce_time <=", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeLike(Date value) {
            addCriterion("produce_time like", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeNotLike(Date value) {
            addCriterion("produce_time not like", value, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeIn(List<Date> values) {
            addCriterion("produce_time in", values, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeNotIn(List<Date> values) {
            addCriterion("produce_time not in", values, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeBetween(Date value1, Date value2) {
            addCriterion("produce_time between", value1, value2, "produceTime");
            return (Criteria) this;
        }

        public Criteria andProduceTimeNotBetween(Date value1, Date value2) {
            addCriterion("produce_time not between", value1, value2, "produceTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCountIsNull() {
            addCriterion("count is null");
            return (Criteria) this;
        }

        public Criteria andCountIsNotNull() {
            addCriterion("count is not null");
            return (Criteria) this;
        }

        public Criteria andCountEqualTo(Integer value) {
            addCriterion("count =", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotEqualTo(Integer value) {
            addCriterion("count <>", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThan(Integer value) {
            addCriterion("count >", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("count >=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThan(Integer value) {
            addCriterion("count <", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThanOrEqualTo(Integer value) {
            addCriterion("count <=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLike(Integer value) {
            addCriterion("count like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotLike(Integer value) {
            addCriterion("count not like", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountIn(List<Integer> values) {
            addCriterion("count in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotIn(List<Integer> values) {
            addCriterion("count not in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountBetween(Integer value1, Integer value2) {
            addCriterion("count between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotBetween(Integer value1, Integer value2) {
            addCriterion("count not between", value1, value2, "count");
            return (Criteria) this;
        }
        
				
        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(Date value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(Date value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(Long value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(Long value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(Long value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(Long value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(Long value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(Long value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(Long value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(Long value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<Long> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<Long> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(Long value1, Long value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(Long value1, Long value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }
        
				
        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLike(Date value) {
            addCriterion("update_time like", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotLike(Date value) {
            addCriterion("update_time not like", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
        
				
        public Criteria andUpdaterIsNull() {
            addCriterion("updater is null");
            return (Criteria) this;
        }

        public Criteria andUpdaterIsNotNull() {
            addCriterion("updater is not null");
            return (Criteria) this;
        }

        public Criteria andUpdaterEqualTo(Long value) {
            addCriterion("updater =", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotEqualTo(Long value) {
            addCriterion("updater <>", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterGreaterThan(Long value) {
            addCriterion("updater >", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterGreaterThanOrEqualTo(Long value) {
            addCriterion("updater >=", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLessThan(Long value) {
            addCriterion("updater <", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLessThanOrEqualTo(Long value) {
            addCriterion("updater <=", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLike(Long value) {
            addCriterion("updater like", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotLike(Long value) {
            addCriterion("updater not like", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterIn(List<Long> values) {
            addCriterion("updater in", values, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotIn(List<Long> values) {
            addCriterion("updater not in", values, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterBetween(Long value1, Long value2) {
            addCriterion("updater between", value1, value2, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotBetween(Long value1, Long value2) {
            addCriterion("updater not between", value1, value2, "updater");
            return (Criteria) this;
        }
        
				
        public Criteria andField1IsNull() {
            addCriterion("field1 is null");
            return (Criteria) this;
        }

        public Criteria andField1IsNotNull() {
            addCriterion("field1 is not null");
            return (Criteria) this;
        }

        public Criteria andField1EqualTo(String value) {
            addCriterion("field1 =", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotEqualTo(String value) {
            addCriterion("field1 <>", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1GreaterThan(String value) {
            addCriterion("field1 >", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1GreaterThanOrEqualTo(String value) {
            addCriterion("field1 >=", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1LessThan(String value) {
            addCriterion("field1 <", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1LessThanOrEqualTo(String value) {
            addCriterion("field1 <=", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1Like(String value) {
            addCriterion("field1 like", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotLike(String value) {
            addCriterion("field1 not like", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1In(List<String> values) {
            addCriterion("field1 in", values, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotIn(List<String> values) {
            addCriterion("field1 not in", values, "field1");
            return (Criteria) this;
        }

        public Criteria andField1Between(String value1, String value2) {
            addCriterion("field1 between", value1, value2, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotBetween(String value1, String value2) {
            addCriterion("field1 not between", value1, value2, "field1");
            return (Criteria) this;
        }
        
				
        public Criteria andField2IsNull() {
            addCriterion("field2 is null");
            return (Criteria) this;
        }

        public Criteria andField2IsNotNull() {
            addCriterion("field2 is not null");
            return (Criteria) this;
        }

        public Criteria andField2EqualTo(String value) {
            addCriterion("field2 =", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotEqualTo(String value) {
            addCriterion("field2 <>", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2GreaterThan(String value) {
            addCriterion("field2 >", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2GreaterThanOrEqualTo(String value) {
            addCriterion("field2 >=", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2LessThan(String value) {
            addCriterion("field2 <", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2LessThanOrEqualTo(String value) {
            addCriterion("field2 <=", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2Like(String value) {
            addCriterion("field2 like", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotLike(String value) {
            addCriterion("field2 not like", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2In(List<String> values) {
            addCriterion("field2 in", values, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotIn(List<String> values) {
            addCriterion("field2 not in", values, "field2");
            return (Criteria) this;
        }

        public Criteria andField2Between(String value1, String value2) {
            addCriterion("field2 between", value1, value2, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotBetween(String value1, String value2) {
            addCriterion("field2 not between", value1, value2, "field2");
            return (Criteria) this;
        }
        
				
        public Criteria andField3IsNull() {
            addCriterion("field3 is null");
            return (Criteria) this;
        }

        public Criteria andField3IsNotNull() {
            addCriterion("field3 is not null");
            return (Criteria) this;
        }

        public Criteria andField3EqualTo(String value) {
            addCriterion("field3 =", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotEqualTo(String value) {
            addCriterion("field3 <>", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3GreaterThan(String value) {
            addCriterion("field3 >", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3GreaterThanOrEqualTo(String value) {
            addCriterion("field3 >=", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3LessThan(String value) {
            addCriterion("field3 <", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3LessThanOrEqualTo(String value) {
            addCriterion("field3 <=", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3Like(String value) {
            addCriterion("field3 like", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotLike(String value) {
            addCriterion("field3 not like", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3In(List<String> values) {
            addCriterion("field3 in", values, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotIn(List<String> values) {
            addCriterion("field3 not in", values, "field3");
            return (Criteria) this;
        }

        public Criteria andField3Between(String value1, String value2) {
            addCriterion("field3 between", value1, value2, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotBetween(String value1, String value2) {
            addCriterion("field3 not between", value1, value2, "field3");
            return (Criteria) this;
        }
        
				
        public Criteria andDepartmentOneIsNull() {
            addCriterion("department_one is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneIsNotNull() {
            addCriterion("department_one is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneEqualTo(String value) {
            addCriterion("department_one =", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneNotEqualTo(String value) {
            addCriterion("department_one <>", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneGreaterThan(String value) {
            addCriterion("department_one >", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneGreaterThanOrEqualTo(String value) {
            addCriterion("department_one >=", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneLessThan(String value) {
            addCriterion("department_one <", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneLessThanOrEqualTo(String value) {
            addCriterion("department_one <=", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneLike(String value) {
            addCriterion("department_one like", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneNotLike(String value) {
            addCriterion("department_one not like", value, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneIn(List<String> values) {
            addCriterion("department_one in", values, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneNotIn(List<String> values) {
            addCriterion("department_one not in", values, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneBetween(String value1, String value2) {
            addCriterion("department_one between", value1, value2, "departmentOne");
            return (Criteria) this;
        }

        public Criteria andDepartmentOneNotBetween(String value1, String value2) {
            addCriterion("department_one not between", value1, value2, "departmentOne");
            return (Criteria) this;
        }
        
				
        public Criteria andDepartmentTwoIsNull() {
            addCriterion("department_two is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoIsNotNull() {
            addCriterion("department_two is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoEqualTo(String value) {
            addCriterion("department_two =", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoNotEqualTo(String value) {
            addCriterion("department_two <>", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoGreaterThan(String value) {
            addCriterion("department_two >", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoGreaterThanOrEqualTo(String value) {
            addCriterion("department_two >=", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoLessThan(String value) {
            addCriterion("department_two <", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoLessThanOrEqualTo(String value) {
            addCriterion("department_two <=", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoLike(String value) {
            addCriterion("department_two like", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoNotLike(String value) {
            addCriterion("department_two not like", value, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoIn(List<String> values) {
            addCriterion("department_two in", values, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoNotIn(List<String> values) {
            addCriterion("department_two not in", values, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoBetween(String value1, String value2) {
            addCriterion("department_two between", value1, value2, "departmentTwo");
            return (Criteria) this;
        }

        public Criteria andDepartmentTwoNotBetween(String value1, String value2) {
            addCriterion("department_two not between", value1, value2, "departmentTwo");
            return (Criteria) this;
        }


        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(Integer value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(Integer value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStandardNameIsNull() {
            addCriterion("standardName is null");
            return (Criteria) this;
        }

        public Criteria andStandardNameIsNotNull() {
            addCriterion("standardName is not null");
            return (Criteria) this;
        }

        public Criteria andStandardNameEqualTo(String value) {
            addCriterion("standardName =", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameNotEqualTo(String value) {
            addCriterion("standardName <>", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameGreaterThan(String value) {
            addCriterion("standardName >", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameGreaterThanOrEqualTo(String value) {
            addCriterion("standardName >=", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameLessThan(String value) {
            addCriterion("standardName <", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameLessThanOrEqualTo(String value) {
            addCriterion("standardName <=", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameLike(String value) {
            addCriterion("standardName like", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameNotLike(String value) {
            addCriterion("standardName not like", value, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameIn(List<String> values) {
            addCriterion("standardName in", values, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameNotIn(List<String> values) {
            addCriterion("standardName not in", values, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameBetween(String value1, String value2) {
            addCriterion("standardName between", value1, value2, "standardName");
            return (Criteria) this;
        }

        public Criteria andStandardNameNotBetween(String value1, String value2) {
            addCriterion("standardName not between", value1, value2, "standardName");
            return (Criteria) this;
        }


        public Criteria andCreateNameIsNull() {
            addCriterion("createName is null");
            return (Criteria) this;
        }

        public Criteria andCreateNameIsNotNull() {
            addCriterion("createName is not null");
            return (Criteria) this;
        }

        public Criteria andCreateNameEqualTo(String value) {
            addCriterion("createName =", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotEqualTo(String value) {
            addCriterion("createName <>", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameGreaterThan(String value) {
            addCriterion("createName >", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameGreaterThanOrEqualTo(String value) {
            addCriterion("createName >=", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLessThan(String value) {
            addCriterion("createName <", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLessThanOrEqualTo(String value) {
            addCriterion("createName <=", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLike(String value) {
            addCriterion("createName like", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotLike(String value) {
            addCriterion("createName not like", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameIn(List<String> values) {
            addCriterion("createName in", values, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotIn(List<String> values) {
            addCriterion("createName not in", values, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameBetween(String value1, String value2) {
            addCriterion("createName between", value1, value2, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotBetween(String value1, String value2) {
            addCriterion("createName not between", value1, value2, "createName");
            return (Criteria) this;
        }

		 public Criteria andLikeQuery(PlanProduceMaterialDetail record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getProduceNo()!=null&&StrUtil.isNotEmpty(record.getProduceNo().toString())) {
    			 list.add("ifnull(produce_no,'')");
    		}
			if(record.getMateriaNo()!=null&&StrUtil.isNotEmpty(record.getMateriaNo().toString())) {
    			 list.add("ifnull(materia_no,'')");
    		}
			if(record.getMaterialName()!=null&&StrUtil.isNotEmpty(record.getMaterialName().toString())) {
    			 list.add("ifnull(material_name,'')");
    		}
			if(record.getStandard()!=null&&StrUtil.isNotEmpty(record.getStandard().toString())) {
    			 list.add("ifnull(standard,'')");
    		}
			if(record.getStock()!=null&&StrUtil.isNotEmpty(record.getStock().toString())) {
    			 list.add("ifnull(stock,'')");
    		}
			if(record.getMaterial()!=null&&StrUtil.isNotEmpty(record.getMaterial().toString())) {
    			 list.add("ifnull(material,'')");
    		}
			if(record.getProduceTime()!=null&&StrUtil.isNotEmpty(record.getProduceTime().toString())) {
    			 list.add("ifnull(produce_time,'')");
    		}
			if(record.getCount()!=null&&StrUtil.isNotEmpty(record.getCount().toString())) {
    			 list.add("ifnull(count,'')");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			 list.add("ifnull(create_time,'')");
    		}
			if(record.getCreator()!=null&&StrUtil.isNotEmpty(record.getCreator().toString())) {
    			 list.add("ifnull(creator,'')");
    		}
			if(record.getUpdateTime()!=null&&StrUtil.isNotEmpty(record.getUpdateTime().toString())) {
    			 list.add("ifnull(update_time,'')");
    		}
			if(record.getUpdater()!=null&&StrUtil.isNotEmpty(record.getUpdater().toString())) {
    			 list.add("ifnull(updater,'')");
    		}
			if(record.getField1()!=null&&StrUtil.isNotEmpty(record.getField1().toString())) {
    			 list.add("ifnull(field1,'')");
    		}
			if(record.getField2()!=null&&StrUtil.isNotEmpty(record.getField2().toString())) {
    			 list.add("ifnull(field2,'')");
    		}
			if(record.getField3()!=null&&StrUtil.isNotEmpty(record.getField3().toString())) {
    			 list.add("ifnull(field3,'')");
    		}
			if(record.getDepartmentOne()!=null&&StrUtil.isNotEmpty(record.getDepartmentOne().toString())) {
    			 list.add("ifnull(department_one,'')");
    		}
			if(record.getDepartmentTwo()!=null&&StrUtil.isNotEmpty(record.getDepartmentTwo().toString())) {
    			 list.add("ifnull(department_two,'')");
    		}
             if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
                 list.add("ifnull(status,'')");
             }
             if(record.getStandardName()!=null&&StrUtil.isNotEmpty(record.getStandardName().toString())) {
                 list.add("ifnull(standardName,'')");
             }
             if(record.getCreateName()!=null&&StrUtil.isNotEmpty(record.getCreateName().toString())) {
                 list.add("ifnull(createName,'')");
             }
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getProduceNo()!=null&&StrUtil.isNotEmpty(record.getProduceNo().toString())) {
    			list2.add("'%"+record.getProduceNo()+"%'");
    		}
			if(record.getMateriaNo()!=null&&StrUtil.isNotEmpty(record.getMateriaNo().toString())) {
    			list2.add("'%"+record.getMateriaNo()+"%'");
    		}
			if(record.getMaterialName()!=null&&StrUtil.isNotEmpty(record.getMaterialName().toString())) {
    			list2.add("'%"+record.getMaterialName()+"%'");
    		}
			if(record.getStandard()!=null&&StrUtil.isNotEmpty(record.getStandard().toString())) {
    			list2.add("'%"+record.getStandard()+"%'");
    		}
			if(record.getStock()!=null&&StrUtil.isNotEmpty(record.getStock().toString())) {
    			list2.add("'%"+record.getStock()+"%'");
    		}
			if(record.getMaterial()!=null&&StrUtil.isNotEmpty(record.getMaterial().toString())) {
    			list2.add("'%"+record.getMaterial()+"%'");
    		}
			if(record.getProduceTime()!=null&&StrUtil.isNotEmpty(record.getProduceTime().toString())) {
    			list2.add("'%"+record.getProduceTime()+"%'");
    		}
			if(record.getCount()!=null&&StrUtil.isNotEmpty(record.getCount().toString())) {
    			list2.add("'%"+record.getCount()+"%'");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			list2.add("'%"+record.getCreateTime()+"%'");
    		}
			if(record.getCreator()!=null&&StrUtil.isNotEmpty(record.getCreator().toString())) {
    			list2.add("'%"+record.getCreator()+"%'");
    		}
			if(record.getUpdateTime()!=null&&StrUtil.isNotEmpty(record.getUpdateTime().toString())) {
    			list2.add("'%"+record.getUpdateTime()+"%'");
    		}
			if(record.getUpdater()!=null&&StrUtil.isNotEmpty(record.getUpdater().toString())) {
    			list2.add("'%"+record.getUpdater()+"%'");
    		}
			if(record.getField1()!=null&&StrUtil.isNotEmpty(record.getField1().toString())) {
    			list2.add("'%"+record.getField1()+"%'");
    		}
			if(record.getField2()!=null&&StrUtil.isNotEmpty(record.getField2().toString())) {
    			list2.add("'%"+record.getField2()+"%'");
    		}
			if(record.getField3()!=null&&StrUtil.isNotEmpty(record.getField3().toString())) {
    			list2.add("'%"+record.getField3()+"%'");
    		}
			if(record.getDepartmentOne()!=null&&StrUtil.isNotEmpty(record.getDepartmentOne().toString())) {
    			list2.add("'%"+record.getDepartmentOne()+"%'");
    		}
			if(record.getDepartmentTwo()!=null&&StrUtil.isNotEmpty(record.getDepartmentTwo().toString())) {
    			list2.add("'%"+record.getDepartmentTwo()+"%'");
    		}
             if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
                 list2.add("'%"+record.getStatus()+"%'");
             }
             if(record.getStandardName()!=null&&StrUtil.isNotEmpty(record.getStandardName().toString())) {
                 list2.add("'%"+record.getStandardName()+"%'");
             }
             if(record.getCreateName()!=null&&StrUtil.isNotEmpty(record.getCreateName().toString())) {
                 list2.add("'%"+record.getCreateName()+"%'");
             }
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(produce_no,'')");
    		list.add("ifnull(materia_no,'')");
    		list.add("ifnull(material_name,'')");
    		list.add("ifnull(standard,'')");
    		list.add("ifnull(stock,'')");
    		list.add("ifnull(material,'')");
    		list.add("ifnull(produce_time,'')");
    		list.add("ifnull(count,'')");
    		list.add("ifnull(create_time,'')");
    		list.add("ifnull(creator,'')");
    		list.add("ifnull(update_time,'')");
    		list.add("ifnull(updater,'')");
    		list.add("ifnull(field1,'')");
    		list.add("ifnull(field2,'')");
    		list.add("ifnull(field3,'')");
    		list.add("ifnull(department_one,'')");
    		list.add("ifnull(department_two,'')");
            list.add("ifnull(status,'')");
            list.add("ifnull(standardName,'')");
            list.add("ifnull(createName,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}