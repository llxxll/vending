package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class JshSupplier implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "供应商名称")
	private String supplier;
	
	@ApiModelProperty(value = "联系人")
	private String contacts;
	
	@ApiModelProperty(value = "联系电话")
	private String phonenum;
	
	@ApiModelProperty(value = "电子邮箱")
	private String email;
	
	@ApiModelProperty(value = "备注")
	private String description;
	
	@ApiModelProperty(value = "是否系统自带 0==系统 1==非系统")
	private Integer isystem;
	
	@ApiModelProperty(value = "类型")
	private String type;
	
	@ApiModelProperty(value = "启用")
	private Byte enabled;
	
	@ApiModelProperty(value = "预收款")
	private BigDecimal advanceIn;
	
	@ApiModelProperty(value = "期初应收")
	private BigDecimal beginNeedGet;
	
	@ApiModelProperty(value = "期初应付")
	private BigDecimal beginNeedPay;
	
	@ApiModelProperty(value = "累计应收")
	private BigDecimal allNeedGet;
	
	@ApiModelProperty(value = "累计应付")
	private BigDecimal allNeedPay;
	
	@ApiModelProperty(value = "传真")
	private String fax;
	
	@ApiModelProperty(value = "手机")
	private String telephone;
	
	@ApiModelProperty(value = "地址")
	private String address;
	
	@ApiModelProperty(value = "纳税人识别号")
	private String taxNum;
	
	@ApiModelProperty(value = "开户行")
	private String bankName;
	
	@ApiModelProperty(value = "账号")
	private String accountNumber;
	
	@ApiModelProperty(value = "税率")
	private BigDecimal taxRate;
	
	@ApiModelProperty(value = "租户id")
	private Long tenantId;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private String deleteFlag;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("supplier")
	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier =  supplier;
	}
	@JsonProperty("contacts")
	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts =  contacts;
	}
	@JsonProperty("phonenum")
	public String getPhonenum() {
		return phonenum;
	}

	public void setPhonenum(String phonenum) {
		this.phonenum =  phonenum;
	}
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email =  email;
	}
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description =  description;
	}
	@JsonProperty("isystem")
	public Integer getIsystem() {
		return isystem;
	}

	public void setIsystem(Integer isystem) {
		this.isystem =  isystem;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type =  type;
	}
	@JsonProperty("enabled")
	public Byte getEnabled() {
		return enabled;
	}

	public void setEnabled(Byte enabled) {
		this.enabled =  enabled;
	}
	@JsonProperty("advanceIn")
	public BigDecimal getAdvanceIn() {
		return advanceIn;
	}

	public void setAdvanceIn(BigDecimal advanceIn) {
		this.advanceIn =  advanceIn;
	}
	@JsonProperty("beginNeedGet")
	public BigDecimal getBeginNeedGet() {
		return beginNeedGet;
	}

	public void setBeginNeedGet(BigDecimal beginNeedGet) {
		this.beginNeedGet =  beginNeedGet;
	}
	@JsonProperty("beginNeedPay")
	public BigDecimal getBeginNeedPay() {
		return beginNeedPay;
	}

	public void setBeginNeedPay(BigDecimal beginNeedPay) {
		this.beginNeedPay =  beginNeedPay;
	}
	@JsonProperty("allNeedGet")
	public BigDecimal getAllNeedGet() {
		return allNeedGet;
	}

	public void setAllNeedGet(BigDecimal allNeedGet) {
		this.allNeedGet =  allNeedGet;
	}
	@JsonProperty("allNeedPay")
	public BigDecimal getAllNeedPay() {
		return allNeedPay;
	}

	public void setAllNeedPay(BigDecimal allNeedPay) {
		this.allNeedPay =  allNeedPay;
	}
	@JsonProperty("fax")
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax =  fax;
	}
	@JsonProperty("telephone")
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone =  telephone;
	}
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address =  address;
	}
	@JsonProperty("taxNum")
	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum =  taxNum;
	}
	@JsonProperty("bankName")
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName =  bankName;
	}
	@JsonProperty("accountNumber")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber =  accountNumber;
	}
	@JsonProperty("taxRate")
	public BigDecimal getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate =  taxRate;
	}
	@JsonProperty("tenantId")
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId =  tenantId;
	}
	@JsonProperty("deleteFlag")
	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}

																																														
	public JshSupplier(Long id,String supplier,String contacts,String phonenum,String email,String description,Integer isystem,String type,Byte enabled,BigDecimal advanceIn,BigDecimal beginNeedGet,BigDecimal beginNeedPay,BigDecimal allNeedGet,BigDecimal allNeedPay,String fax,String telephone,String address,String taxNum,String bankName,String accountNumber,BigDecimal taxRate,Long tenantId,String deleteFlag) {
				
		this.id = id;
				
		this.supplier = supplier;
				
		this.contacts = contacts;
				
		this.phonenum = phonenum;
				
		this.email = email;
				
		this.description = description;
				
		this.isystem = isystem;
				
		this.type = type;
				
		this.enabled = enabled;
				
		this.advanceIn = advanceIn;
				
		this.beginNeedGet = beginNeedGet;
				
		this.beginNeedPay = beginNeedPay;
				
		this.allNeedGet = allNeedGet;
				
		this.allNeedPay = allNeedPay;
				
		this.fax = fax;
				
		this.telephone = telephone;
				
		this.address = address;
				
		this.taxNum = taxNum;
				
		this.bankName = bankName;
				
		this.accountNumber = accountNumber;
				
		this.taxRate = taxRate;
				
		this.tenantId = tenantId;
				
		this.deleteFlag = deleteFlag;
				
	}

	public JshSupplier() {
	    super();
	}

	

}