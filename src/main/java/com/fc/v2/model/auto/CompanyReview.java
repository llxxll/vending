package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class CompanyReview implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "名称")
	private String name;
	
	@ApiModelProperty(value = "账号")
	private String number;
	
	@ApiModelProperty(value = "所属公司")
	private String company;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Integer status;
	
	@ApiModelProperty(value = "")
	private Integer sStatus;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name =  name;
	}
	@JsonProperty("number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number =  number;
	}
	@JsonProperty("company")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company =  company;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("sStatus")
	public Integer getSStatus() {
		return sStatus;
	}

	public void setSStatus(Integer sStatus) {
		this.sStatus =  sStatus;
	}

														
	public CompanyReview(Long id,String name,String number,String company,Date createdTime,Integer status,Integer sStatus) {
				
		this.id = id;
				
		this.name = name;
				
		this.number = number;
				
		this.company = company;
				
		this.createdTime = createdTime;
				
		this.status = status;
				
		this.sStatus = sStatus;
				
	}

	public CompanyReview() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}