package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class WmsWarehouseStock implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long sid;
	
	@ApiModelProperty(value = "产品id")
	private String matterNo;
	
	@ApiModelProperty(value = "")
	private String matterDescribe;
	
	@ApiModelProperty(value = "仓库id")
	private String houseNo;
	
	@ApiModelProperty(value = "库存数量")
	private Long stockNumber;
	
	@ApiModelProperty(value = "库存位置")
	private String stockLocation;
	
	@ApiModelProperty(value = "单位")
	private String unitName;
	
	@ApiModelProperty(value = "工艺")
	private String xs;
	
	@ApiModelProperty(value = "")
	private String x;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "")
	private Long createdUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date modifiedTime;
	
	@ApiModelProperty(value = "")
	private Long modifiedUserSid;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date ts;
	
	@JsonProperty("sid")
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid =  sid;
	}
	@JsonProperty("matterNo")
	public String getMatterNo() {
		return matterNo;
	}

	public void setMatterNo(String matterNo) {
		this.matterNo =  matterNo;
	}
	@JsonProperty("matterDescribe")
	public String getMatterDescribe() {
		return matterDescribe;
	}

	public void setMatterDescribe(String matterDescribe) {
		this.matterDescribe =  matterDescribe;
	}
	@JsonProperty("houseNo")
	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo =  houseNo;
	}
	@JsonProperty("stockNumber")
	public Long getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(Long stockNumber) {
		this.stockNumber =  stockNumber;
	}
	@JsonProperty("stockLocation")
	public String getStockLocation() {
		return stockLocation;
	}

	public void setStockLocation(String stockLocation) {
		this.stockLocation =  stockLocation;
	}
	@JsonProperty("unitName")
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName =  unitName;
	}
	@JsonProperty("xs")
	public String getXs() {
		return xs;
	}

	public void setXs(String xs) {
		this.xs =  xs;
	}
	@JsonProperty("x")
	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x =  x;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("createdUserSid")
	public Long getCreatedUserSid() {
		return createdUserSid;
	}

	public void setCreatedUserSid(Long createdUserSid) {
		this.createdUserSid =  createdUserSid;
	}
	@JsonProperty("modifiedTime")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime =  modifiedTime;
	}
	@JsonProperty("modifiedUserSid")
	public Long getModifiedUserSid() {
		return modifiedUserSid;
	}

	public void setModifiedUserSid(Long modifiedUserSid) {
		this.modifiedUserSid =  modifiedUserSid;
	}
	@JsonProperty("ts")
	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts =  ts;
	}

																												
	public WmsWarehouseStock(Long sid,String matterNo,String matterDescribe,String houseNo,Long stockNumber,String stockLocation,String unitName,String xs,String x,Date createdTime,Long createdUserSid,Date modifiedTime,Long modifiedUserSid,Date ts) {
				
		this.sid = sid;
				
		this.matterNo = matterNo;
				
		this.matterDescribe = matterDescribe;
				
		this.houseNo = houseNo;
				
		this.stockNumber = stockNumber;
				
		this.stockLocation = stockLocation;
				
		this.unitName = unitName;
				
		this.xs = xs;
				
		this.x = x;
				
		this.createdTime = createdTime;
				
		this.createdUserSid = createdUserSid;
				
		this.modifiedTime = modifiedTime;
				
		this.modifiedUserSid = modifiedUserSid;
				
		this.ts = ts;
				
	}

	public WmsWarehouseStock() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	//辅助字段
	@ApiModelProperty(value = "产品名称")
	private String matterName;
	@ApiModelProperty(value = "规格")
	private String spesc;
	@ApiModelProperty(value = "仓库名称")
	private String houseName;
	@ApiModelProperty(value = "类型")
	private String type;
	@ApiModelProperty(value = "工艺")
	private String gongyi;
	private String createName;

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getGongyi() {
		return gongyi;
	}

	public void setGongyi(String gongyi) {
		this.gongyi = gongyi;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		
		if(type.equals("1")) {
			type="物料";
		}else if(type.equals("2")) {
			type="材料";
		}else if(type.equals("3")) {
			type="辅料";
		}
		this.type = type;
	}

	public String getSpesc() {
		return spesc;
	}

	public void setSpesc(String spesc) {
		this.spesc = spesc;
	}

	public String getMatterName() {
		return matterName;
	}

	public void setMatterName(String matterName) {
		this.matterName = matterName;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

}