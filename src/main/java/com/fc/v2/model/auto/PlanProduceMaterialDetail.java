package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class PlanProduceMaterialDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "计划编号")
	private String produceNo;
	
	@ApiModelProperty(value = "物料号")
	private String materiaNo;
	
	@ApiModelProperty(value = "物料名称(图号)")
	private String materialName;
	
	@ApiModelProperty(value = "材料ID")
	private String standard;

	@ApiModelProperty(value = "下料数量")
	private Long stock;
	
	@ApiModelProperty(value = "备注")
	private String material;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "生产时间")
	private Date produceTime;
	
	@ApiModelProperty(value = "数量")
	private Integer count;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	@ApiModelProperty(value = "创建人")
	private Long creator;

	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@ApiModelProperty(value = "")
	private String field1;
	
	@ApiModelProperty(value = "删除字段0删除1正常")
	private String field2;
	
	@ApiModelProperty(value = "加工车间")
	private String field3;
	
	@ApiModelProperty(value = "供应商")
	private String departmentOne;
	
	@ApiModelProperty(value = "材料规格")
	private String departmentTwo;

	@ApiModelProperty(value = "状态")
	private Integer status;

	@ApiModelProperty(value = "创建人姓名")
	private String createName;

	@ApiModelProperty(value = "材料名称")
	private String standardName;


	//数量
	private Integer number;
	//仓库
	private String houseId;



	@JsonProperty("standardName")
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName == null ? null : standardName.trim();
	}

	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getHouseId() {
		return houseId;
	}
	public void setHouseId(String houseId) {
		this.houseId = houseId == null ? null : houseId.trim();
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("produceNo")
	public String getProduceNo() {
		return produceNo;
	}

	public void setProduceNo(String produceNo) {
		this.produceNo = produceNo == null ? null : produceNo.trim();
	}
	@JsonProperty("materiaNo")
	public String getMateriaNo() {
		return materiaNo;
	}

	public void setMateriaNo(String materiaNo) {
		this.materiaNo = materiaNo == null ? null : materiaNo.trim();
	}
	@JsonProperty("materialName")
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName == null ? null : materialName.trim();
	}
	@JsonProperty("standard")
	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard == null ? null : standard.trim();
	}
	@JsonProperty("stock")
	public Long getStock() {
		return stock;
	}

	public void setStock(Long stock) {
		this.stock =  stock;
	}
	@JsonProperty("material")
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material == null ? null : material.trim();
	}
	@JsonProperty("produceTime")
	public Date getProduceTime() {
		return produceTime;
	}

	public void setProduceTime(Date produceTime) {
		this.produceTime =  produceTime;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1 == null ? null : field1.trim();
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2 == null ? null : field2.trim();
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3 == null ? null : field3.trim();
	}

	@JsonProperty("departmentOne")
	public String getDepartmentOne() {
		return departmentOne;
	}
	public void setDepartmentOne(String departmentOne) {
		this.departmentOne = departmentOne == null ? null : departmentOne.trim();
	}

	@JsonProperty("departmentTwo")
	public String getDepartmentTwo() {
		return departmentTwo;
	}
	public void setDepartmentTwo(String departmentTwo) {
		this.departmentTwo = departmentTwo == null ? null : departmentTwo.trim();
	}

	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status =  status;
	}

	@JsonProperty("createName")
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName == null ? null : createName.trim();
	}


	public PlanProduceMaterialDetail(Long id,String produceNo,String materiaNo,String materialName,String standard,Long stock,String material,Date produceTime,Integer count,Date createTime,Long creator,Date updateTime,Long updater,String field1,String field2,String field3,String departmentOne,String departmentTwo,Integer status, String createName, String standardName) {
				
		this.id = id;
				
		this.produceNo = produceNo;
				
		this.materiaNo = materiaNo;
				
		this.materialName = materialName;
				
		this.standard = standard;
				
		this.stock = stock;
				
		this.material = material;
				
		this.produceTime = produceTime;
				
		this.count = count;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.departmentOne = departmentOne;
				
		this.departmentTwo = departmentTwo;

		this.status = status;

		this.createName = createName;

		this.standardName = standardName;

	}

	public PlanProduceMaterialDetail() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
private String createorName;
private String tuName;//物料编号
private String tuNo;//物料描述
private String clName;//材料名称

private String clProperty;//材料规格
private String houseNo;//材料规格
private String jshcarftId;//材料规格
private Integer workID;//工人id
private Integer lingliaoCount;//领料数
private Integer zuidaCount;//最大开单数




public Integer getZuidaCount() {
	return zuidaCount;
}
public void setZuidaCount(Integer zuidaCount) {
	this.zuidaCount = zuidaCount;
}
public Integer getLingliaoCount() {
	return lingliaoCount;
}
public void setLingliaoCount(Integer lingliaoCount) {
	this.lingliaoCount = lingliaoCount;
}
public Integer getWorkID() {
	return workID;
}
public void setWorkID(Integer workID) {
	this.workID = workID;
}
public String getHouseNo() {
	return houseNo;
}
public void setHouseNo(String houseNo) {
	this.houseNo = houseNo;
}
public String getJshcarftId() {
	return jshcarftId;
}
public void setJshcarftId(String jshcarftId) {
	this.jshcarftId = jshcarftId;
}
public String getCreateorName() {
	return createorName;
}
public void setCreateorName(String createorName) {
	this.createorName = createorName;
}
public String getTuName() {
	return tuName;
}
public void setTuName(String tuName) {
	this.tuName = tuName;
}
public String getTuNo() {
	return tuNo;
}
public void setTuNo(String tuNo) {
	this.tuNo = tuNo;
}
public String getClName() {
	return clName;
}
public void setClName(String clName) {
	this.clName = clName;
}
public String getClProperty() {
	return clProperty;
}
public void setClProperty(String clProperty) {
	this.clProperty = clProperty;
}

}