package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class MoneyReview implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "转账公司")
	private String transferCompany;
	
	@ApiModelProperty(value = "提现公司")
	private String withdrawalCompany;
	
	@ApiModelProperty(value = "账号")
	private String number;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createdTime;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	
	@ApiModelProperty(value = "提现状态")
	private Integer tStatus;
	
	@ApiModelProperty(value = "备注")
	private Integer notes;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("transferCompany")
	public String getTransferCompany() {
		return transferCompany;
	}

	public void setTransferCompany(String transferCompany) {
		this.transferCompany =  transferCompany;
	}
	@JsonProperty("withdrawalCompany")
	public String getWithdrawalCompany() {
		return withdrawalCompany;
	}

	public void setWithdrawalCompany(String withdrawalCompany) {
		this.withdrawalCompany =  withdrawalCompany;
	}
	@JsonProperty("number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number =  number;
	}
	@JsonProperty("createdTime")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime =  createdTime;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("tStatus")
	public Integer getTStatus() {
		return tStatus;
	}

	public void setTStatus(Integer tStatus) {
		this.tStatus =  tStatus;
	}
	@JsonProperty("notes")
	public Integer getNotes() {
		return notes;
	}

	public void setNotes(Integer notes) {
		this.notes =  notes;
	}

																
	public MoneyReview(Long id,String transferCompany,String withdrawalCompany,String number,Date createdTime,Integer status,Integer tStatus,Integer notes) {
				
		this.id = id;
				
		this.transferCompany = transferCompany;
				
		this.withdrawalCompany = withdrawalCompany;
				
		this.number = number;
				
		this.createdTime = createdTime;
				
		this.status = status;
				
		this.tStatus = tStatus;
				
		this.notes = notes;
				
	}

	public MoneyReview() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}