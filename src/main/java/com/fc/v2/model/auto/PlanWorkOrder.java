package com.fc.v2.model.auto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class PlanWorkOrder implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "计划id")
	private String no;
	
	@ApiModelProperty(value = "工单数量")
	private Integer count;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date updateTime;
	
	@ApiModelProperty(value = "")
	private Long updater;
	
	@ApiModelProperty(value = "领料仓库ID")
	private String field1;
	
	@ApiModelProperty(value = "材料ID")
	private String field2;
	
	@ApiModelProperty()
	private String field3;
	
	@ApiModelProperty(value = "删除标记，0未删除，1删除")
	private String deleteFlag;
	
	@ApiModelProperty(value = "物料ID")
	private String materialNo;
	
	@ApiModelProperty(value = "")
	private String materialName;
	
	@ApiModelProperty(value = "")
	private Integer materialCount;
	
	@ApiModelProperty(value = "工单编号")
	private String workerOrderNo;
	
	
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("no")
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no =  no;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 =  field1;
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 =  field2;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}
	@JsonProperty("deleteFlag")
	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag =  deleteFlag;
	}
	@JsonProperty("materialNo")
	public String getMaterialNo() {
		return materialNo;
	}

	public void setMaterialNo(String materialNo) {
		this.materialNo =  materialNo;
	}
	@JsonProperty("materialName")
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName =  materialName;
	}
	@JsonProperty("materialCount")
	public Integer getMaterialCount() {
		return materialCount;
	}

	public void setMaterialCount(Integer materialCount) {
		this.materialCount =  materialCount;
	}
	@JsonProperty("workerOrderNo")
	public String getWorkerOrderNo() {
		return workerOrderNo;
	}

	public void setWorkerOrderNo(String workerOrderNo) {
		this.workerOrderNo =  workerOrderNo;
	}

																																
	public PlanWorkOrder(Long id,String no,Integer count,Integer status,Date createTime,Long creator,Date updateTime,Long updater,String field1,String field2,String field3,String deleteFlag,String materialNo,String materialName,Integer materialCount,String workerOrderNo) {
				
		this.id = id;
				
		this.no = no;
				
		this.count = count;
				
		this.status = status;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.deleteFlag = deleteFlag;
				
		this.materialNo = materialNo;
				
		this.materialName = materialName;
				
		this.materialCount = materialCount;
				
		this.workerOrderNo = workerOrderNo;
				
	}

	public PlanWorkOrder() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	//辅助字段
	@ApiModelProperty(value = "创建人")
	private String createrName;
	@ApiModelProperty(value = "物料号")
	private String tuhao;
	@ApiModelProperty(value = "物料名称")
	private String tuName;
	@ApiModelProperty(value = "材料")
	private String clName;
	@ApiModelProperty(value = "材料规格")
	private String clproperty;
	@ApiModelProperty(value = "出库材料")
	private String chukclName;
	@ApiModelProperty(value = "出库规格")
	private String chukclproperty;
	@ApiModelProperty(value = "领料仓库")
	private String housename;
	@ApiModelProperty(value = "领料数量")
	private String lingliaoCount;
	@ApiModelProperty(value = "放料人")
	private String lingliaoUser;
	@ApiModelProperty(value = "放料时间")
	private String lingliaoTime;
	@ApiModelProperty(value = "计划id")
	private String planno;
	@ApiModelProperty(value = "异常标识")
	private String yichangyuanyin;
	private String beginTime;
	private String endTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getYichangyuanyin() {
		return yichangyuanyin;
	}

	public void setYichangyuanyin(String yichangyuanyin) {
		this.yichangyuanyin = yichangyuanyin;
	}

	public String getChukclName() {
		return chukclName;
	}

	public void setChukclName(String chukclName) {
		this.chukclName = chukclName;
	}

	public String getChukclproperty() {
		return chukclproperty;
	}

	public void setChukclproperty(String chukclproperty) {
		this.chukclproperty = chukclproperty;
	}

	public String getTuName() {
		return tuName;
	}

	public void setTuName(String tuName) {
		this.tuName = tuName;
	}

	public String getLingliaoUser() {
		return lingliaoUser;
	}

	public void setLingliaoUser(String lingliaoUser) {
		this.lingliaoUser = lingliaoUser;
	}

	public String getLingliaoTime() {
		return lingliaoTime;
	}

	public void setLingliaoTime(String lingliaoTime) {
		this.lingliaoTime = lingliaoTime;
	}

	public String getPlanno() {
		return planno;
	}

	public void setPlanno(String planno) {
		this.planno = planno;
	}

	public String getLingliaoCount() {
		return lingliaoCount;
	}

	public void setLingliaoCount(String lingliaoCount) {
		this.lingliaoCount = lingliaoCount;
	}

	public String getHousename() {
		return housename;
	}

	public void setHousename(String housename) {
		this.housename = housename;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public String getTuhao() {
		return tuhao;
	}

	public void setTuhao(String tuhao) {
		this.tuhao = tuhao;
	}

	public String getClName() {
		return clName;
	}

	public void setClName(String clName) {
		this.clName = clName;
	}

	public String getClproperty() {
		return clproperty;
	}

	public void setClproperty(String clproperty) {
		this.clproperty = clproperty;
	}

	

}