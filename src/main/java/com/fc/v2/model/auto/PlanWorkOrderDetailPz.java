package com.fc.v2.model.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.date.DateUtil;
import java.util.Date;

public class PlanWorkOrderDetailPz implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键")
	private Long id;
	
	@ApiModelProperty(value = "工单号")
	private String workerOrderNo;
	
	@ApiModelProperty(value = "废料数量")
	private Integer carftOrder;
	
	@ApiModelProperty(value = "工序名称")
	private String carftId;
	
	@ApiModelProperty(value = "工人")
	private Integer worker;
	
	@ApiModelProperty(value = "数量")
	private Integer count;
	
	@ApiModelProperty(value = "不合格数量")
	private Integer unqualifiedCount;
	
	@ApiModelProperty(value = "")
	private Integer stockCount;
	
	@ApiModelProperty(value = "车间id")
	private Integer manager;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "分活时间")
	private Date distributionTime;
	
	@ApiModelProperty(value = "状态1待领料2待质检3已质检4待入库5已入库6待分工7已转序")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	
	@ApiModelProperty(value = "创建人")
	private Long creator;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "质检时间")
	private Date updateTime;
	
	@ApiModelProperty(value = "更新人")
	private Long updater;
	
	@ApiModelProperty(value = "入库仓库")
	private String field1;
	
	@ApiModelProperty(value = "质检员")
	private String field2;
	
	@ApiModelProperty(value = "转序车间")
	private String field3;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date finshTime;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("workerOrderNo")
	public String getWorkerOrderNo() {
		return workerOrderNo;
	}

	public void setWorkerOrderNo(String workerOrderNo) {
		this.workerOrderNo =  workerOrderNo;
	}
	@JsonProperty("carftOrder")
	public Integer getCarftOrder() {
		return carftOrder;
	}

	public void setCarftOrder(Integer carftOrder) {
		this.carftOrder =  carftOrder;
	}
	@JsonProperty("carftId")
	public String getCarftId() {
		return carftId;
	}

	public void setCarftId(String carftId) {
		this.carftId =  carftId;
	}
	@JsonProperty("worker")
	public Integer getWorker() {
		return worker;
	}

	public void setWorker(Integer worker) {
		this.worker =  worker;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count =  count;
	}
	@JsonProperty("unqualifiedCount")
	public Integer getUnqualifiedCount() {
		return unqualifiedCount;
	}

	public void setUnqualifiedCount(Integer unqualifiedCount) {
		this.unqualifiedCount =  unqualifiedCount;
	}
	
	@JsonProperty("stockCount")
	public Integer getStockCount() {
		return stockCount;
	}

	public void setStockCount(Integer stockCount) {
		this.stockCount =  stockCount;
	}
	@JsonProperty("manager")
	public Integer getManager() {
		return manager;
	}

	public void setManager(Integer manager) {
		this.manager =  manager;
	}
	@JsonProperty("distributionTime")
	public Date getDistributionTime() {
		return distributionTime;
	}

	public void setDistributionTime(Date distributionTime) {
		this.distributionTime =  distributionTime;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("creator")
	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator =  creator;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}
	@JsonProperty("updater")
	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater =  updater;
	}
	@JsonProperty("field1")
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 =  field1;
	}
	@JsonProperty("field2")
	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 =  field2;
	}
	@JsonProperty("field3")
	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 =  field3;
	}
	@JsonProperty("finshTime")
	public Date getFinshTime() {
		return finshTime;
	}

	public void setFinshTime(Date finshTime) {
		this.finshTime =  finshTime;
	}

																																						
	public PlanWorkOrderDetailPz(Long id,String workerOrderNo,Integer carftOrder,String carftId,Integer worker,Integer count,Integer unqualifiedCount,Integer stockCount,Integer manager,Date distributionTime,Integer status,Date createTime,Long creator,Date updateTime,Long updater,String field1,String field2,String field3,Date finshTime) {
				
		this.id = id;
				
		this.workerOrderNo = workerOrderNo;
				
		this.carftOrder = carftOrder;
				
		this.carftId = carftId;
				
		this.worker = worker;
				
		this.count = count;
				
		this.unqualifiedCount = unqualifiedCount;
				
		this.stockCount = stockCount;
				
		this.manager = manager;
				
		this.distributionTime = distributionTime;
				
		this.status = status;
				
		this.createTime = createTime;
				
		this.creator = creator;
				
		this.updateTime = updateTime;
				
		this.updater = updater;
				
		this.field1 = field1;
				
		this.field2 = field2;
				
		this.field3 = field3;
				
		this.finshTime = finshTime;
				
	}

	public PlanWorkOrderDetailPz() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	
	//辅助字段
	private String carftname;//工艺名称
	private String carftttxx;//工艺名称
	private String carftspecs;//工艺名称
	private BigDecimal ppprice;//单价
	private String tuhao;
	private String tuName;
	private String workername;
	private Integer planworkstatus;
	
	
	
	
	public Integer getPlanworkstatus() {
		return planworkstatus;
	}

	public void setPlanworkstatus(Integer planworkstatus) {
		this.planworkstatus = planworkstatus;
	}

	public String getWorkername() {
		return workername;
	}

	public void setWorkername(String workername) {
		this.workername = workername;
	}

	private String clNo;
	private String clName;
	private String rukuCount;
	private String zhuanxuchejian;
	private String createName;
	private String rukuhousename;
	private String chejianname;
	private String type;
	private Long zhuxuOrg;
	private Integer fenchuCount;
	private String fenchuWork;
	private String materialId;
	private Integer xianCount;
	private Integer xianManger;
	private Long clID;
	private String beginTime;
	private String endTime;


	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getZhuxuOrg() {
		return zhuxuOrg;
	}

	public void setZhuxuOrg(Long zhuxuOrg) {
		this.zhuxuOrg = zhuxuOrg;
	}

	public Long getClID() {
		return clID;
	}

	public void setClID(Long clID) {
		this.clID = clID;
	}

	public String getChejianname() {
		return chejianname;
	}

	public void setChejianname(String chejianname) {
		this.chejianname = chejianname;
	}

	public String getCarftttxx() {
		return carftttxx;
	}

	public void setCarftttxx(String carftttxx) {
		this.carftttxx = carftttxx;
	}

	public String getCarftspecs() {
		return carftspecs;
	}

	public void setCarftspecs(String carftspecs) {
		this.carftspecs = carftspecs;
	}


	public BigDecimal getPpprice() {
		return ppprice;
	}

	public void setPpprice(BigDecimal ppprice) {
		this.ppprice = ppprice;
	}

	public String getZhuanxuchejian() {
		return zhuanxuchejian;
	}

	public void setZhuanxuchejian(String zhuanxuchejian) {
		this.zhuanxuchejian = zhuanxuchejian;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getRukuhousename() {
		return rukuhousename;
	}

	public void setRukuhousename(String rukuhousename) {
		this.rukuhousename = rukuhousename;
	}

	public Integer getXianManger() {
		return xianManger;
	}

	public void setXianManger(Integer xianManger) {
		this.xianManger = xianManger;
	}

	public Integer getXianCount() {
		return xianCount;
	}

	public void setXianCount(Integer xianCount) {
		this.xianCount = xianCount;
	}

	public String getMaterialId() {
		return materialId;
	}

	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}

	public String getTuName() {
		return tuName;
	}

	public void setTuName(String tuName) {
		this.tuName = tuName;
	}

	public String getClNo() {
		return clNo;
	}

	public void setClNo(String clNo) {
		this.clNo = clNo;
	}

	public String getClName() {
		return clName;
	}

	public void setClName(String clName) {
		this.clName = clName;
	}

	public Integer getFenchuCount() {
		return fenchuCount;
	}

	public void setFenchuCount(Integer fenchuCount) {
		this.fenchuCount = fenchuCount;
	}

	public String getFenchuWork() {
		return fenchuWork;
	}

	public void setFenchuWork(String fenchuWork) {
		this.fenchuWork = fenchuWork;
	}

	public String getRukuCount() {
		return rukuCount;
	}

	public void setRukuCount(String rukuCount) {
		this.rukuCount = rukuCount;
	}

	public String getTuhao() {
		return tuhao;
	}

	public void setTuhao(String tuhao) {
		this.tuhao = tuhao;
	}

	public String getCarftname() {
		return carftname;
	}

	public void setCarftname(String carftname) {
		this.carftname = carftname;
	}
}