package com.fc.v2.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;

/**
 * 客户表 OrderCustomerExample
 * @author fuce_自动生成
 * @date 2022-09-15 20:45:08
 */
public class OrderCustomerExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrderCustomerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andSidIsNull() {
            addCriterion("sid is null");
            return (Criteria) this;
        }

        public Criteria andSidIsNotNull() {
            addCriterion("sid is not null");
            return (Criteria) this;
        }

        public Criteria andSidEqualTo(Long value) {
            addCriterion("sid =", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotEqualTo(Long value) {
            addCriterion("sid <>", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThan(Long value) {
            addCriterion("sid >", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidGreaterThanOrEqualTo(Long value) {
            addCriterion("sid >=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThan(Long value) {
            addCriterion("sid <", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLessThanOrEqualTo(Long value) {
            addCriterion("sid <=", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidLike(Long value) {
            addCriterion("sid like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotLike(Long value) {
            addCriterion("sid not like", value, "sid");
            return (Criteria) this;
        }

        public Criteria andSidIn(List<Long> values) {
            addCriterion("sid in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotIn(List<Long> values) {
            addCriterion("sid not in", values, "sid");
            return (Criteria) this;
        }

        public Criteria andSidBetween(Long value1, Long value2) {
            addCriterion("sid between", value1, value2, "sid");
            return (Criteria) this;
        }

        public Criteria andSidNotBetween(Long value1, Long value2) {
            addCriterion("sid not between", value1, value2, "sid");
            return (Criteria) this;
        }
        
				
        public Criteria andCustomerNameIsNull() {
            addCriterion("customer_name is null");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIsNotNull() {
            addCriterion("customer_name is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerNameEqualTo(String value) {
            addCriterion("customer_name =", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotEqualTo(String value) {
            addCriterion("customer_name <>", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameGreaterThan(String value) {
            addCriterion("customer_name >", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameGreaterThanOrEqualTo(String value) {
            addCriterion("customer_name >=", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLessThan(String value) {
            addCriterion("customer_name <", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLessThanOrEqualTo(String value) {
            addCriterion("customer_name <=", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLike(String value) {
            addCriterion("customer_name like", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotLike(String value) {
            addCriterion("customer_name not like", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIn(List<String> values) {
            addCriterion("customer_name in", values, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotIn(List<String> values) {
            addCriterion("customer_name not in", values, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameBetween(String value1, String value2) {
            addCriterion("customer_name between", value1, value2, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotBetween(String value1, String value2) {
            addCriterion("customer_name not between", value1, value2, "customerName");
            return (Criteria) this;
        }
        
				
        public Criteria andCustomerPhoneIsNull() {
            addCriterion("customer_phone is null");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneIsNotNull() {
            addCriterion("customer_phone is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneEqualTo(String value) {
            addCriterion("customer_phone =", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneNotEqualTo(String value) {
            addCriterion("customer_phone <>", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneGreaterThan(String value) {
            addCriterion("customer_phone >", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("customer_phone >=", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneLessThan(String value) {
            addCriterion("customer_phone <", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneLessThanOrEqualTo(String value) {
            addCriterion("customer_phone <=", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneLike(String value) {
            addCriterion("customer_phone like", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneNotLike(String value) {
            addCriterion("customer_phone not like", value, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneIn(List<String> values) {
            addCriterion("customer_phone in", values, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneNotIn(List<String> values) {
            addCriterion("customer_phone not in", values, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneBetween(String value1, String value2) {
            addCriterion("customer_phone between", value1, value2, "customerPhone");
            return (Criteria) this;
        }

        public Criteria andCustomerPhoneNotBetween(String value1, String value2) {
            addCriterion("customer_phone not between", value1, value2, "customerPhone");
            return (Criteria) this;
        }
        
				
        public Criteria andCustomerDescIsNull() {
            addCriterion("customer_desc is null");
            return (Criteria) this;
        }

        public Criteria andCustomerDescIsNotNull() {
            addCriterion("customer_desc is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerDescEqualTo(String value) {
            addCriterion("customer_desc =", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescNotEqualTo(String value) {
            addCriterion("customer_desc <>", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescGreaterThan(String value) {
            addCriterion("customer_desc >", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescGreaterThanOrEqualTo(String value) {
            addCriterion("customer_desc >=", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescLessThan(String value) {
            addCriterion("customer_desc <", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescLessThanOrEqualTo(String value) {
            addCriterion("customer_desc <=", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescLike(String value) {
            addCriterion("customer_desc like", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescNotLike(String value) {
            addCriterion("customer_desc not like", value, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescIn(List<String> values) {
            addCriterion("customer_desc in", values, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescNotIn(List<String> values) {
            addCriterion("customer_desc not in", values, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescBetween(String value1, String value2) {
            addCriterion("customer_desc between", value1, value2, "customerDesc");
            return (Criteria) this;
        }

        public Criteria andCustomerDescNotBetween(String value1, String value2) {
            addCriterion("customer_desc not between", value1, value2, "customerDesc");
            return (Criteria) this;
        }
        
				
        public Criteria andBarcodeIsNull() {
            addCriterion("barcode is null");
            return (Criteria) this;
        }

        public Criteria andBarcodeIsNotNull() {
            addCriterion("barcode is not null");
            return (Criteria) this;
        }

        public Criteria andBarcodeEqualTo(String value) {
            addCriterion("barcode =", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeNotEqualTo(String value) {
            addCriterion("barcode <>", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeGreaterThan(String value) {
            addCriterion("barcode >", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeGreaterThanOrEqualTo(String value) {
            addCriterion("barcode >=", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeLessThan(String value) {
            addCriterion("barcode <", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeLessThanOrEqualTo(String value) {
            addCriterion("barcode <=", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeLike(String value) {
            addCriterion("barcode like", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeNotLike(String value) {
            addCriterion("barcode not like", value, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeIn(List<String> values) {
            addCriterion("barcode in", values, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeNotIn(List<String> values) {
            addCriterion("barcode not in", values, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeBetween(String value1, String value2) {
            addCriterion("barcode between", value1, value2, "barcode");
            return (Criteria) this;
        }

        public Criteria andBarcodeNotBetween(String value1, String value2) {
            addCriterion("barcode not between", value1, value2, "barcode");
            return (Criteria) this;
        }
        
				
        public Criteria andQrcodeIsNull() {
            addCriterion("qrcode is null");
            return (Criteria) this;
        }

        public Criteria andQrcodeIsNotNull() {
            addCriterion("qrcode is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodeEqualTo(String value) {
            addCriterion("qrcode =", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotEqualTo(String value) {
            addCriterion("qrcode <>", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThan(String value) {
            addCriterion("qrcode >", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThanOrEqualTo(String value) {
            addCriterion("qrcode >=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThan(String value) {
            addCriterion("qrcode <", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThanOrEqualTo(String value) {
            addCriterion("qrcode <=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLike(String value) {
            addCriterion("qrcode like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotLike(String value) {
            addCriterion("qrcode not like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeIn(List<String> values) {
            addCriterion("qrcode in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotIn(List<String> values) {
            addCriterion("qrcode not in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeBetween(String value1, String value2) {
            addCriterion("qrcode between", value1, value2, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotBetween(String value1, String value2) {
            addCriterion("qrcode not between", value1, value2, "qrcode");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLike(Date value) {
            addCriterion("created_time like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotLike(Date value) {
            addCriterion("created_time not like", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }
        
				
        public Criteria andCreatedUserSidIsNull() {
            addCriterion("created_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIsNotNull() {
            addCriterion("created_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidEqualTo(Long value) {
            addCriterion("created_user_sid =", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotEqualTo(Long value) {
            addCriterion("created_user_sid <>", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThan(Long value) {
            addCriterion("created_user_sid >", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("created_user_sid >=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThan(Long value) {
            addCriterion("created_user_sid <", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("created_user_sid <=", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidLike(Long value) {
            addCriterion("created_user_sid like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotLike(Long value) {
            addCriterion("created_user_sid not like", value, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidIn(List<Long> values) {
            addCriterion("created_user_sid in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotIn(List<Long> values) {
            addCriterion("created_user_sid not in", values, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidBetween(Long value1, Long value2) {
            addCriterion("created_user_sid between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }

        public Criteria andCreatedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("created_user_sid not between", value1, value2, "createdUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedTimeIsNull() {
            addCriterion("modified_time is null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIsNotNull() {
            addCriterion("modified_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeEqualTo(Date value) {
            addCriterion("modified_time =", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotEqualTo(Date value) {
            addCriterion("modified_time <>", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThan(Date value) {
            addCriterion("modified_time >", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modified_time >=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThan(Date value) {
            addCriterion("modified_time <", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThanOrEqualTo(Date value) {
            addCriterion("modified_time <=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLike(Date value) {
            addCriterion("modified_time like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotLike(Date value) {
            addCriterion("modified_time not like", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIn(List<Date> values) {
            addCriterion("modified_time in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotIn(List<Date> values) {
            addCriterion("modified_time not in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeBetween(Date value1, Date value2) {
            addCriterion("modified_time between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotBetween(Date value1, Date value2) {
            addCriterion("modified_time not between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }
        
				
        public Criteria andModifiedUserSidIsNull() {
            addCriterion("modified_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIsNotNull() {
            addCriterion("modified_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidEqualTo(Long value) {
            addCriterion("modified_user_sid =", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotEqualTo(Long value) {
            addCriterion("modified_user_sid <>", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThan(Long value) {
            addCriterion("modified_user_sid >", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid >=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThan(Long value) {
            addCriterion("modified_user_sid <", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLessThanOrEqualTo(Long value) {
            addCriterion("modified_user_sid <=", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidLike(Long value) {
            addCriterion("modified_user_sid like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotLike(Long value) {
            addCriterion("modified_user_sid not like", value, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidIn(List<Long> values) {
            addCriterion("modified_user_sid in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotIn(List<Long> values) {
            addCriterion("modified_user_sid not in", values, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }

        public Criteria andModifiedUserSidNotBetween(Long value1, Long value2) {
            addCriterion("modified_user_sid not between", value1, value2, "modifiedUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andXIsNull() {
            addCriterion("x is null");
            return (Criteria) this;
        }

        public Criteria andXIsNotNull() {
            addCriterion("x is not null");
            return (Criteria) this;
        }

        public Criteria andXEqualTo(Integer value) {
            addCriterion("x =", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotEqualTo(Integer value) {
            addCriterion("x <>", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThan(Integer value) {
            addCriterion("x >", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThanOrEqualTo(Integer value) {
            addCriterion("x >=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThan(Integer value) {
            addCriterion("x <", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThanOrEqualTo(Integer value) {
            addCriterion("x <=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLike(Integer value) {
            addCriterion("x like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotLike(Integer value) {
            addCriterion("x not like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXIn(List<Integer> values) {
            addCriterion("x in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXNotIn(List<Integer> values) {
            addCriterion("x not in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXBetween(Integer value1, Integer value2) {
            addCriterion("x between", value1, value2, "x");
            return (Criteria) this;
        }

        public Criteria andXNotBetween(Integer value1, Integer value2) {
            addCriterion("x not between", value1, value2, "x");
            return (Criteria) this;
        }
        
				
        public Criteria andXTimeIsNull() {
            addCriterion("x_time is null");
            return (Criteria) this;
        }

        public Criteria andXTimeIsNotNull() {
            addCriterion("x_time is not null");
            return (Criteria) this;
        }

        public Criteria andXTimeEqualTo(Date value) {
            addCriterion("x_time =", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotEqualTo(Date value) {
            addCriterion("x_time <>", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThan(Date value) {
            addCriterion("x_time >", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("x_time >=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThan(Date value) {
            addCriterion("x_time <", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLessThanOrEqualTo(Date value) {
            addCriterion("x_time <=", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeLike(Date value) {
            addCriterion("x_time like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotLike(Date value) {
            addCriterion("x_time not like", value, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeIn(List<Date> values) {
            addCriterion("x_time in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotIn(List<Date> values) {
            addCriterion("x_time not in", values, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeBetween(Date value1, Date value2) {
            addCriterion("x_time between", value1, value2, "xTime");
            return (Criteria) this;
        }

        public Criteria andXTimeNotBetween(Date value1, Date value2) {
            addCriterion("x_time not between", value1, value2, "xTime");
            return (Criteria) this;
        }
        
				
        public Criteria andXUserSidIsNull() {
            addCriterion("x_user_sid is null");
            return (Criteria) this;
        }

        public Criteria andXUserSidIsNotNull() {
            addCriterion("x_user_sid is not null");
            return (Criteria) this;
        }

        public Criteria andXUserSidEqualTo(Long value) {
            addCriterion("x_user_sid =", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotEqualTo(Long value) {
            addCriterion("x_user_sid <>", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThan(Long value) {
            addCriterion("x_user_sid >", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidGreaterThanOrEqualTo(Long value) {
            addCriterion("x_user_sid >=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThan(Long value) {
            addCriterion("x_user_sid <", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLessThanOrEqualTo(Long value) {
            addCriterion("x_user_sid <=", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidLike(Long value) {
            addCriterion("x_user_sid like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotLike(Long value) {
            addCriterion("x_user_sid not like", value, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidIn(List<Long> values) {
            addCriterion("x_user_sid in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotIn(List<Long> values) {
            addCriterion("x_user_sid not in", values, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidBetween(Long value1, Long value2) {
            addCriterion("x_user_sid between", value1, value2, "xUserSid");
            return (Criteria) this;
        }

        public Criteria andXUserSidNotBetween(Long value1, Long value2) {
            addCriterion("x_user_sid not between", value1, value2, "xUserSid");
            return (Criteria) this;
        }
        
				
        public Criteria andStr1IsNull() {
            addCriterion("str_1 is null");
            return (Criteria) this;
        }

        public Criteria andStr1IsNotNull() {
            addCriterion("str_1 is not null");
            return (Criteria) this;
        }

        public Criteria andStr1EqualTo(String value) {
            addCriterion("str_1 =", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotEqualTo(String value) {
            addCriterion("str_1 <>", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThan(String value) {
            addCriterion("str_1 >", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1GreaterThanOrEqualTo(String value) {
            addCriterion("str_1 >=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThan(String value) {
            addCriterion("str_1 <", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1LessThanOrEqualTo(String value) {
            addCriterion("str_1 <=", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Like(String value) {
            addCriterion("str_1 like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotLike(String value) {
            addCriterion("str_1 not like", value, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1In(List<String> values) {
            addCriterion("str_1 in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotIn(List<String> values) {
            addCriterion("str_1 not in", values, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1Between(String value1, String value2) {
            addCriterion("str_1 between", value1, value2, "str1");
            return (Criteria) this;
        }

        public Criteria andStr1NotBetween(String value1, String value2) {
            addCriterion("str_1 not between", value1, value2, "str1");
            return (Criteria) this;
        }
        
				
        public Criteria andStr2IsNull() {
            addCriterion("str_2 is null");
            return (Criteria) this;
        }

        public Criteria andStr2IsNotNull() {
            addCriterion("str_2 is not null");
            return (Criteria) this;
        }

        public Criteria andStr2EqualTo(String value) {
            addCriterion("str_2 =", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotEqualTo(String value) {
            addCriterion("str_2 <>", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThan(String value) {
            addCriterion("str_2 >", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2GreaterThanOrEqualTo(String value) {
            addCriterion("str_2 >=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThan(String value) {
            addCriterion("str_2 <", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2LessThanOrEqualTo(String value) {
            addCriterion("str_2 <=", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Like(String value) {
            addCriterion("str_2 like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotLike(String value) {
            addCriterion("str_2 not like", value, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2In(List<String> values) {
            addCriterion("str_2 in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotIn(List<String> values) {
            addCriterion("str_2 not in", values, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2Between(String value1, String value2) {
            addCriterion("str_2 between", value1, value2, "str2");
            return (Criteria) this;
        }

        public Criteria andStr2NotBetween(String value1, String value2) {
            addCriterion("str_2 not between", value1, value2, "str2");
            return (Criteria) this;
        }
        
				
        public Criteria andStr3IsNull() {
            addCriterion("str_3 is null");
            return (Criteria) this;
        }

        public Criteria andStr3IsNotNull() {
            addCriterion("str_3 is not null");
            return (Criteria) this;
        }

        public Criteria andStr3EqualTo(String value) {
            addCriterion("str_3 =", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotEqualTo(String value) {
            addCriterion("str_3 <>", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3GreaterThan(String value) {
            addCriterion("str_3 >", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3GreaterThanOrEqualTo(String value) {
            addCriterion("str_3 >=", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3LessThan(String value) {
            addCriterion("str_3 <", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3LessThanOrEqualTo(String value) {
            addCriterion("str_3 <=", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3Like(String value) {
            addCriterion("str_3 like", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotLike(String value) {
            addCriterion("str_3 not like", value, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3In(List<String> values) {
            addCriterion("str_3 in", values, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotIn(List<String> values) {
            addCriterion("str_3 not in", values, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3Between(String value1, String value2) {
            addCriterion("str_3 between", value1, value2, "str3");
            return (Criteria) this;
        }

        public Criteria andStr3NotBetween(String value1, String value2) {
            addCriterion("str_3 not between", value1, value2, "str3");
            return (Criteria) this;
        }
        
				
        public Criteria andNum1IsNull() {
            addCriterion("num_1 is null");
            return (Criteria) this;
        }

        public Criteria andNum1IsNotNull() {
            addCriterion("num_1 is not null");
            return (Criteria) this;
        }

        public Criteria andNum1EqualTo(Long value) {
            addCriterion("num_1 =", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotEqualTo(Long value) {
            addCriterion("num_1 <>", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThan(Long value) {
            addCriterion("num_1 >", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1GreaterThanOrEqualTo(Long value) {
            addCriterion("num_1 >=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThan(Long value) {
            addCriterion("num_1 <", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1LessThanOrEqualTo(Long value) {
            addCriterion("num_1 <=", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Like(Long value) {
            addCriterion("num_1 like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotLike(Long value) {
            addCriterion("num_1 not like", value, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1In(List<Long> values) {
            addCriterion("num_1 in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotIn(List<Long> values) {
            addCriterion("num_1 not in", values, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1Between(Long value1, Long value2) {
            addCriterion("num_1 between", value1, value2, "num1");
            return (Criteria) this;
        }

        public Criteria andNum1NotBetween(Long value1, Long value2) {
            addCriterion("num_1 not between", value1, value2, "num1");
            return (Criteria) this;
        }
        
				
        public Criteria andNum2IsNull() {
            addCriterion("num_2 is null");
            return (Criteria) this;
        }

        public Criteria andNum2IsNotNull() {
            addCriterion("num_2 is not null");
            return (Criteria) this;
        }

        public Criteria andNum2EqualTo(Integer value) {
            addCriterion("num_2 =", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotEqualTo(Integer value) {
            addCriterion("num_2 <>", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThan(Integer value) {
            addCriterion("num_2 >", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2GreaterThanOrEqualTo(Integer value) {
            addCriterion("num_2 >=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThan(Integer value) {
            addCriterion("num_2 <", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2LessThanOrEqualTo(Integer value) {
            addCriterion("num_2 <=", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Like(Integer value) {
            addCriterion("num_2 like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotLike(Integer value) {
            addCriterion("num_2 not like", value, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2In(List<Integer> values) {
            addCriterion("num_2 in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotIn(List<Integer> values) {
            addCriterion("num_2 not in", values, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2Between(Integer value1, Integer value2) {
            addCriterion("num_2 between", value1, value2, "num2");
            return (Criteria) this;
        }

        public Criteria andNum2NotBetween(Integer value1, Integer value2) {
            addCriterion("num_2 not between", value1, value2, "num2");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(OrderCustomer record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			 list.add("ifnull(sid,'')");
    		}
			if(record.getCustomerName()!=null&&StrUtil.isNotEmpty(record.getCustomerName().toString())) {
    			 list.add("ifnull(customer_name,'')");
    		}
			if(record.getCustomerPhone()!=null&&StrUtil.isNotEmpty(record.getCustomerPhone().toString())) {
    			 list.add("ifnull(customer_phone,'')");
    		}
			if(record.getCustomerDesc()!=null&&StrUtil.isNotEmpty(record.getCustomerDesc().toString())) {
    			 list.add("ifnull(customer_desc,'')");
    		}
			if(record.getBarcode()!=null&&StrUtil.isNotEmpty(record.getBarcode().toString())) {
    			 list.add("ifnull(barcode,'')");
    		}
			if(record.getQrcode()!=null&&StrUtil.isNotEmpty(record.getQrcode().toString())) {
    			 list.add("ifnull(qrcode,'')");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			 list.add("ifnull(created_time,'')");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			 list.add("ifnull(created_user_sid,'')");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			 list.add("ifnull(modified_time,'')");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			 list.add("ifnull(modified_user_sid,'')");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			 list.add("ifnull(x,'')");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			 list.add("ifnull(x_time,'')");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			 list.add("ifnull(x_user_sid,'')");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			 list.add("ifnull(str_1,'')");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			 list.add("ifnull(str_2,'')");
    		}
			if(record.getStr3()!=null&&StrUtil.isNotEmpty(record.getStr3().toString())) {
    			 list.add("ifnull(str_3,'')");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			 list.add("ifnull(num_1,'')");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			 list.add("ifnull(num_2,'')");
    		}
			if(record.getSid()!=null&&StrUtil.isNotEmpty(record.getSid().toString())) {
    			list2.add("'%"+record.getSid()+"%'");
    		}
			if(record.getCustomerName()!=null&&StrUtil.isNotEmpty(record.getCustomerName().toString())) {
    			list2.add("'%"+record.getCustomerName()+"%'");
    		}
			if(record.getCustomerPhone()!=null&&StrUtil.isNotEmpty(record.getCustomerPhone().toString())) {
    			list2.add("'%"+record.getCustomerPhone()+"%'");
    		}
			if(record.getCustomerDesc()!=null&&StrUtil.isNotEmpty(record.getCustomerDesc().toString())) {
    			list2.add("'%"+record.getCustomerDesc()+"%'");
    		}
			if(record.getBarcode()!=null&&StrUtil.isNotEmpty(record.getBarcode().toString())) {
    			list2.add("'%"+record.getBarcode()+"%'");
    		}
			if(record.getQrcode()!=null&&StrUtil.isNotEmpty(record.getQrcode().toString())) {
    			list2.add("'%"+record.getQrcode()+"%'");
    		}
			if(record.getCreatedTime()!=null&&StrUtil.isNotEmpty(record.getCreatedTime().toString())) {
    			list2.add("'%"+record.getCreatedTime()+"%'");
    		}
			if(record.getCreatedUserSid()!=null&&StrUtil.isNotEmpty(record.getCreatedUserSid().toString())) {
    			list2.add("'%"+record.getCreatedUserSid()+"%'");
    		}
			if(record.getModifiedTime()!=null&&StrUtil.isNotEmpty(record.getModifiedTime().toString())) {
    			list2.add("'%"+record.getModifiedTime()+"%'");
    		}
			if(record.getModifiedUserSid()!=null&&StrUtil.isNotEmpty(record.getModifiedUserSid().toString())) {
    			list2.add("'%"+record.getModifiedUserSid()+"%'");
    		}
			if(record.getX()!=null&&StrUtil.isNotEmpty(record.getX().toString())) {
    			list2.add("'%"+record.getX()+"%'");
    		}
			if(record.getXTime()!=null&&StrUtil.isNotEmpty(record.getXTime().toString())) {
    			list2.add("'%"+record.getXTime()+"%'");
    		}
			if(record.getXUserSid()!=null&&StrUtil.isNotEmpty(record.getXUserSid().toString())) {
    			list2.add("'%"+record.getXUserSid()+"%'");
    		}
			if(record.getStr1()!=null&&StrUtil.isNotEmpty(record.getStr1().toString())) {
    			list2.add("'%"+record.getStr1()+"%'");
    		}
			if(record.getStr2()!=null&&StrUtil.isNotEmpty(record.getStr2().toString())) {
    			list2.add("'%"+record.getStr2()+"%'");
    		}
			if(record.getStr3()!=null&&StrUtil.isNotEmpty(record.getStr3().toString())) {
    			list2.add("'%"+record.getStr3()+"%'");
    		}
			if(record.getNum1()!=null&&StrUtil.isNotEmpty(record.getNum1().toString())) {
    			list2.add("'%"+record.getNum1()+"%'");
    		}
			if(record.getNum2()!=null&&StrUtil.isNotEmpty(record.getNum2().toString())) {
    			list2.add("'%"+record.getNum2()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(sid,'')");
    		list.add("ifnull(customer_name,'')");
    		list.add("ifnull(customer_phone,'')");
    		list.add("ifnull(customer_desc,'')");
    		list.add("ifnull(barcode,'')");
    		list.add("ifnull(qrcode,'')");
    		list.add("ifnull(created_time,'')");
    		list.add("ifnull(created_user_sid,'')");
    		list.add("ifnull(modified_time,'')");
    		list.add("ifnull(modified_user_sid,'')");
    		list.add("ifnull(x,'')");
    		list.add("ifnull(x_time,'')");
    		list.add("ifnull(x_user_sid,'')");
    		list.add("ifnull(str_1,'')");
    		list.add("ifnull(str_2,'')");
    		list.add("ifnull(str_3,'')");
    		list.add("ifnull(num_1,'')");
    		list.add("ifnull(num_2,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}