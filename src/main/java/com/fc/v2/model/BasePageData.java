package com.fc.v2.model;

public class BasePageData {

    private int code;//0表示成功，其它失败
    private String msg;//提示信息 //一般上传失败后返回
    private Object data;// 返回数据信息

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
