package com.fc.v2.model.custom;

/**
 * boostrap table post 参数
 * @author fc
 *
 */
public class Tablepar {
	private int page;//页码
	private int limit;//数量
	private String orderByColumn;//排序字段
	private String isAsc;//排序字符 asc desc 
	private String searchText;//列表table里面的搜索
	private Integer searchSatus;//列表table里面的搜索
	private String searchCreater;//列表table里面的搜索
	private String searchNo;//列表table里面的搜索
	private String searchHouse;//列表table里面的搜索
	private String searchType;//列表table里面的搜索
	private String searchBgeinTime;//列表table里面的搜索
	private String searchendTime;//列表table里面的搜索

	public String getSearchHouse() {
		return searchHouse;
	}

	public void setSearchHouse(String searchHouse) {
		this.searchHouse = searchHouse;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchBgeinTime() {
		return searchBgeinTime;
	}

	public void setSearchBgeinTime(String searchBgeinTime) {
		this.searchBgeinTime = searchBgeinTime;
	}

	public String getSearchendTime() {
		return searchendTime;
	}

	public void setSearchendTime(String searchendTime) {
		this.searchendTime = searchendTime;
	}

	public String getSearchNo() {
		return searchNo;
	}

	public void setSearchNo(String searchNo) {
		this.searchNo = searchNo == null ? null : searchNo.trim();
	}

	public Integer getSearchSatus() {
		return searchSatus;
	}

	public void setSearchSatus(Integer searchSatus) {
		this.searchSatus = searchSatus;
	}

	public String getSearchCreater() {
		
		return searchCreater;
	}

	public void setSearchCreater(String searchCreater) {
		this.searchCreater = searchCreater == null ? null : searchCreater.trim();
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getOrderByColumn() {
		return orderByColumn;
	}
	public void setOrderByColumn(String orderByColumn) {
		this.orderByColumn = orderByColumn;
	}
	public String getIsAsc() {
		return isAsc;
	}
	public void setIsAsc(String isAsc) {
		this.isAsc = isAsc;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText == null ? null : searchText.trim();
	}

}
