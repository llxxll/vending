package com.fc.v2.controller.admin.app;

import java.math.BigDecimal;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.binarywang.wx.miniapp.config.WxMaInMemoryConfig;
import cn.hutool.setting.Setting;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.amazonaws.util.XmlUtils;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;

import com.fc.v2.common.spring.SpringUtils;
import com.fc.v2.mapper.auto.JshInvoiceMapper;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;

import com.fc.v2.service.*;
import com.fc.v2.shiro.CurrentCart;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.CommonAttributes;
import com.fc.v2.util.ResultUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.ApiModelProperty;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.web.bind.annotation.*;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;


@RestController
@RequestMapping("/api")
public class ApiController extends BaseController {


	@Autowired
	private MdProductService mdProductService;

	@Autowired
	private SysUserService sysUserService;

	@Autowired
	private OmsOrderService omsOrderService;

	@Autowired
	private OmsOrderDetailService omsOrderDetailService;

	@Autowired
	private OrderFromCategoryService orderFromCategoryService;
	@Autowired
	private JshInvoiceMapper jshInvoiceMapper;

	private final WxMaServiceImpl wxMaService = new WxMaServiceImpl();

	private final Integer zjl = 100;
	private final Integer xszg = 101;
	private final Integer xs = 102;


	@ModelAttribute
	public void populateModel() {

		// 获取小程序服务实例
		WxMaInMemoryConfig wxMaInMemoryConfig = new WxMaInMemoryConfig();
		wxMaInMemoryConfig.setAppid("wx853a875e1e76c0d8");
		wxMaInMemoryConfig.setSecret("91838bd7020b4293062dcd52089f5bf1");
		wxMaService.setWxMaConfig(wxMaInMemoryConfig);
	}

	@GetMapping("/setting")
	@ResponseBody
	public ApiResult setting() {
		Map<String, Object> topTextColor = new HashMap<>();
		topTextColor.put("text", "#ffffff");
		topTextColor.put("value", "20");

		Map<String, Object> wxapp = new HashMap<>();
		wxapp.put("top_background_color", "#f02c6c");
		wxapp.put("wxapp_title", "售货机管理系统");

		wxapp.put("top_text_color", topTextColor);

		Map<String, Object> data = new HashMap<>();
		data.put("wxapp", wxapp);
		return ResultUtils.ok(data);
	}

	/**
	 * 首页商品list
	 * @return
	 */
	@GetMapping("/product/list")
	@ResponseBody
	public ApiResult productList(Tablepar tablepar, MdProduct mdProduct) {
		TsysUser tsysUser = sysUserService.selectByOpenId(mdProduct.getOpenId());
		Map<String, Object> data = new HashMap<>();
		PageInfo<MdProduct> page=mdProductService.list(tablepar,mdProduct) ;

		List<Map<String, Object>> productItems = new ArrayList<>();
		for (MdProduct product : page.getList()) {
			Map<String, Object> item = new HashMap<>();
			item.put("sid", String.valueOf(product.getSid()));
			item.put("productTitle", product.getProductTitle());
			item.put("price", product.getPrice());
			item.put("marketPrice", product.getPrice());
			item.put("barcode", "http://xmy.mynatapp.cc"+product.getBarcode());
			//item.put("barcode", "http://lxlcode.vicp.fun"+product.getBarcode());
			item.put("quantity", "1");
			item.put("spuAssemble", product.getSpuAssemble());
			item.put("spu", product.getSpu());
			productItems.add(item);
		}

		data.put("pageNumber", page.getPageNum());
		data.put("pageSize", page.getPageSize());
		data.put("lastPage", page.getLastPage());
		data.put("total", page.getTotal());
		data.put("productItems", productItems);
		if(tsysUser != null && getcommitcheck(Integer.valueOf(tsysUser.getPosId()))){
			data.put("iscommit", true);
		}else{
			data.put("iscommit", false);
		}
		return ResultUtils.ok(data);
	}
	public boolean getcommitcheck(Integer posId){
		if(posId == 100){ return true; }
		if(posId == 101){ return true; }
		if(posId == 102){ return true; }
		return false;
	}
	/**
	 * 获取当前购物车
	 */
	@GetMapping("/cart/get_current")
	public ApiResult getCurrent(@CurrentCart Cart currentCart) {
		Map<String, Object> data = new HashMap<>();
		List<Map<String, Object>> cartItems = new ArrayList<>();
//		if (currentCart != null && !currentCart.isEmpty()) {
//
//			Set<CartItem> cartItemSet=currentCart.getCartItems();
//			Set<CartItem>  set=new HashSet<CartItem>();
//			for (CartItem c:cartItemSet){
//				if (c.getIsBuy()==1){
//					set.add(c);
//				}
//			}
//			//计算被选中的价格
//			Cart currentCart1=new Cart();
//			currentCart1.setCartItems(set);
//
//			data.put("quantity", currentCart.getQuantity(false));
//			data.put("rewardPoint", currentCart.getRewardPoint());
//			data.put("effectivePrice", currentCart1.getEffectivePrice());
//			data.put("discount", currentCart.getDiscount());
//			data.put("id", String.valueOf(currentCart.getId()));
//			for (CartItem cartItem : currentCart) {
//				Map<String, Object> item = new HashMap<>();
//				Sku sku = cartItem.getSku();
//				Product product = sku.getProduct();
//				item.put("id", String.valueOf(cartItem.getId()));
//				item.put("productId", String.valueOf(product.getId()));
//				item.put("cartId", String.valueOf(cartItem.getCart().getId()));
//				item.put("skuId", String.valueOf(sku.getId()));
//				item.put("skuName", sku.getName());
//				item.put("skuThumbnail", sku.getThumbnail() != null ? setting.getSiteUrl() + sku.getThumbnail() : setting.getSiteUrl() + setting.getDefaultThumbnailProductImage());
//				item.put("price", cartItem.getPrice());
//				item.put("specifications", sku.getSpecifications());
//				item.put("quantity", cartItem.getQuantity());
//				item.put("subtotal", cartItem.getSubtotal());
//				item.put("isBuy", cartItem.getIsBuy()==1?true:false);
//				cartItems.add(item);
//			}
//		}
		data.put("cartItems", cartItems);
		return ResultUtils.ok(data);
	}

	/**
	 * 首页
	 */
	@GetMapping("/member/index")
	public ApiResult index(TsysUser currentUser) {

		Map<String, Object> data = new HashMap<>();
		currentUser =sysUserService.selectByOpenId(currentUser.getTenantId());
		Map<String, Object> member = new HashMap<>();
		if(currentUser!=null){
			member.put("id", currentUser.getId());
			member.put("username", currentUser.getUsername());
			member.put("mobile", currentUser.getDescription());
			member.put("name", currentUser.getNickname());
			member.put("numNo", currentUser.getUsername());
		}
		member.put("memberRankName", "1级");
		member.put("balance","货币balance");
		member.put("amount", "货币amount");
		member.put("point", "");
		data.put("member", member);
		return ResultUtils.ok(data);
	}
	/**
	 * 登录接口
	 */
	@PostMapping("/social_user/login")
	public ApiResult signIn(String code, String signature, String rawData, String encryptedData, String ivStr, HttpServletRequest request) {
		System.out.println("用户请求登录获取Token");
		System.out.println("code:"+code);
		if (StringUtils.isEmpty(code)) {
			return ResultUtils.badRequest("code不能为空");
		}

		WxMaJscode2SessionResult codeResult = null;
		try {
			codeResult = wxMaService.jsCode2SessionInfo(code);
		} catch (WxErrorException e) {
			e.printStackTrace();
			System.out.println("error : " + e.getMessage());
			return ResultUtils.badRequest("error : " + e.getMessage());
		}
		System.out.println("codeResult : " + codeResult);

		Map<String, Object> data = new HashMap<>();
		WxMaUserService wxMaUserService = wxMaService.getUserService();
		String sessionKey = codeResult.getSessionKey();

		System.out.println("请求获取用户信息 : sessionKey = " + sessionKey + ", rawData = " + rawData + ", signature = " + signature);
		// 用户信息校验
		if (!wxMaUserService.checkUserInfo(sessionKey, rawData, signature)) {
			return ResultUtils.badRequest("验证用户信息完整性失败!");
		}
		// 解密用户信息
		WxMaUserInfo wxMaUserInfo = wxMaUserService.getUserInfo(sessionKey, encryptedData, ivStr);
		System.out.println("获取解密用户信息："+wxMaUserInfo);
		try {
			TsysUser m1 = new TsysUser();
			Map<String, Object> item = new HashMap<>();
			m1 = sysUserService.selectByOpenId(wxMaUserInfo.getOpenId());
			if(m1 != null){
				item.put("nickName", m1.getNickname());
				item.put("posId", m1.getPosId());
				item.put("posName", m1.getPosName());
				item.put("deptId", m1.getDepId());
				item.put("deptName", m1.getDepName());
				item.put("phone", m1.getDescription());
				item.put("point", m1.getUsername());
				item.put("id", m1.getId());
			}

			item.put("avatarUrl", wxMaUserInfo.getAvatarUrl());
			item.put("open_id", wxMaUserInfo.getOpenId());

			// 签发token
			//Token token = tokenStore.createNewToken(wxMaUserInfo.getOpenId(), new String[] { "/social_user/login" }, new String[] { "R_MEMBER" }, CommonAttributes.EXPIRE_TIME);
			String token = wxMaUserInfo.getOpenId() + CommonAttributes.EXPIRE_TIME;
			data.put("token", "Bearer" + token);
			data.put("member", item);
			data.put("session_key", sessionKey);
			return ResultUtils.ok(data);
		}catch (Exception e){
			return ResultUtils.ok(JSON.toJSONString(e));
		}
	}

	@PostMapping("/social_phone/getPhone")
	public ApiResult getPhone(String encryptedData, String iv,String session_key,String userId) {
		TsysUser m1 = new TsysUser();
		String  phone=	getPhoneNumber(encryptedData,session_key,iv);
		Map<String, Object> data = new HashMap<>();
		if(StringUtils.isNotEmpty(phone)){
			 m1 = sysUserService.selectByPhone(phone);
			if(m1 != null){
				m1.setRemark(userId);
				sysUserService.updateByPrimaryKey(m1);
				data.put("userInfo",m1);
				data.put("phone",phone);
				data.put("msg",true);
			}else{
				data.put("phone",phone);
				data.put("msg",false);
			}
		}else{
			data.put("msg",false);
		}
		return ResultUtils.ok(data);

	}

	public  String getPhoneNumber(String encryptedData, String session_key, String iv)  {
		// 被加密的数据
		byte[] dataByte = Base64.getDecoder().decode(encryptedData);
		// 加密秘钥
		byte[] keyByte = Base64.getDecoder().decode(session_key);
		// 偏移量
		byte[] ivByte = Base64.getDecoder().decode(iv);
		try {
			// 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
			int base = 16;
			if (keyByte.length % base != 0) {
				int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
				byte[] temp = new byte[groups * base];
				Arrays.fill(temp, (byte) 0);
				System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
				keyByte = temp;
			}
			// 初始化
			Security.addProvider(new BouncyCastleProvider());
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
			AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
			parameters.init(new IvParameterSpec(ivByte));
			cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
			byte[] resultByte = cipher.doFinal(dataByte);
			if (null != resultByte && resultByte.length > 0) {
				String result = new String(resultByte, "UTF-8");
				JSONObject jsonObject=JSONObject.parseObject(result);


				System.out.println("获取到手机号信息："+jsonObject);
				return jsonObject.getString("phoneNumber");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("获取手机号失败");
		}
		return null;
	}

	/**
	 * barcode: "http://localhost:9091/upload/9bf7a3f5-9907-450c-ab44-29de4c749c58.jpg"
	 * marketPrice: 1000
	 * price: 1000
	 * productTitle: "收割机"
	 * sid: "9120"
	 * @param map
	 * @return
	 */
	@PostMapping("/product/submit")
	public ApiResult submit(@RequestBody Map map) {
		String openId = (String) map.get("userInfo");
		TsysUser tsysUser = sysUserService.selectByOpenId(openId);
		OmsOrder omsOrder = new OmsOrder();
		JshInvoice jshin = new JshInvoice();
		jshin.setOrderNo("销售订单");
		jshInvoiceMapper.insertSelective(jshin);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		String format = simpleDateFormat.format(new Date());
		omsOrder.setOrderNo(tsysUser.getId()+"-"+format+"-"+jshin.getId());
		omsOrder.setBuySupplyName(String.valueOf(map.get("buySupplyName")));
		BigDecimal big1 = new BigDecimal(String.valueOf(map.get("barginMoney")));
		omsOrder.setBarginMoney(big1);
		BigDecimal big2 = new BigDecimal(String.valueOf(map.get("productMoney")));
		omsOrder.setProductMoney(big2);
		omsOrder.setAddress(String.valueOf(map.get("address")));
		omsOrder.setDeliveryTypeSid((Integer) map.get("deliveryTypeSid"));
		//商品信息->改变状态
		List<Map> mdProducts = (List<Map>) map.get("product");
		//用户openid->确认用户
		
		//创建销售单
		BigDecimal productMoney = new BigDecimal(0);//商品总金额
		BigDecimal payMoney = new BigDecimal(0);//应付金额
		for(Map mdProductmap : mdProducts){
			OmsOrderDetail omsOrderDetail = new OmsOrderDetail();
			//omsOrderDetail.setImgUrl(String.valueOf(mdProductmap.get("barcode")));
			omsOrderDetail.setItemNum(Long.valueOf(String.valueOf(mdProductmap.get("quantity"))));
			omsOrderDetail.setProductSpecific(String.valueOf(mdProductmap.get("spuAssemble")));
			omsOrderDetail.setCategorySid(String.valueOf(mdProductmap.get("spu")));
			omsOrderDetail.setOrderNo(omsOrder.getOrderNo());
			omsOrderDetail.setSupplySid(tsysUser.getId());
			omsOrderDetail.setProductSid(Long.valueOf((String)mdProductmap.get("sid")));
			omsOrderDetail.setProductAlias((String) mdProductmap.get("productDesc"));
			omsOrderDetail.setProductTitle((String) mdProductmap.get("productTitle"));
			omsOrderDetail.setXiaoshouPrice(new BigDecimal(String.valueOf(mdProductmap.get("marketPrice"))));
			omsOrderDetail.setPrice(Long.valueOf(String.valueOf(mdProductmap.get("price"))));
			omsOrderDetailService.insertSelective(omsOrderDetail);
			//productMoney = productMoney.add(new BigDecimal(String.valueOf(mdProductmap.get("price"))));
			//orderMoney = orderMoney.add(new BigDecimal(String.valueOf(mdProductmap.get("marketPrice"))));
			//payMoney = orderMoney.add(new BigDecimal(String.valueOf(mdProductmap.get("marketPrice"))));

		}
		omsOrder.setSellSupplySid(tsysUser.getId());
		omsOrder.setSellSupplyName(tsysUser.getNickname());
		omsOrder.setOrderStatus(1);
		omsOrder.setCreateTime(new Date());
		int b=omsOrderService.insertSelectiveapi(omsOrder);
		if(b>0){
			return ResultUtils.ok();
		}else{
			return ResultUtils.badRequest("添加错误");
		}
	}

	//改变状态
	@GetMapping("/order/update")
	public ApiResult getPhone(String sid,String openId,String status) {
		//用户信息
		TsysUser tsysUser = sysUserService.selectByOpenId(openId);
		omsOrderService.updateOmsOrderByStatus(sid,status,tsysUser.getId());
		return ResultUtils.ok();

	}


	//销售单列表
	/**
	 * 首页商品list
	 * @return
	 */
	@GetMapping("/order/list")
	@ResponseBody
	public ApiResult orderList(Tablepar tablepar, OmsOrder omsOrder) {
		if(StringUtils.isEmpty(omsOrder.getOpenId())){
			return ResultUtils.ok();
		}
		TsysUser tsysUser = sysUserService.selectByOpenId(omsOrder.getOpenId());
		PageInfo<OmsOrder> page = null;
		if(StringUtils.equals(tsysUser.getPosId(),"100")){
			page = omsOrderService.list(tablepar,omsOrder);
		}else if(StringUtils.equals(tsysUser.getPosId(),"102")){
			omsOrder.setSellSupplySid(tsysUser.getId());
			page = omsOrderService.list(tablepar,omsOrder);
		}else if(StringUtils.equals(tsysUser.getPosId(),"101")){
			String ids = sysUserService.selectByDepId(tsysUser.getDepId());
			List<Long> delId = new ArrayList<Long>();
			for (String s : ids.split(",")) {
				delId.add(Long.valueOf(s));
			}
			omsOrder.setDelId(delId);
			page = omsOrderService.list(tablepar,omsOrder);
		}

		Map<String, Object> data = new HashMap<>();
		List<Map<String, Object>> productItems = new ArrayList<>();
		for (OmsOrder omsOrdernew : page.getList()) {
			Map<String, Object> item = new HashMap<>();
			item.put("sid", String.valueOf(omsOrdernew.getSid()));
			item.put("orderNo", omsOrdernew.getOrderNo());
			item.put("buySupplyName", omsOrdernew.getBuySupplyName());
			item.put("sellSupplyName", omsOrdernew.getSellSupplyName());
			item.put("orderStatus", omsOrdernew.getOrderStatus());
			item.put("productNum", omsOrdernew.getProductNum());
			item.put("postage", omsOrdernew.getPostage());
			item.put("productMoney", omsOrdernew.getProductMoney());
			item.put("orderMoney", omsOrdernew.getOrderMoney());
			item.put("payMoney", omsOrdernew.getPayMoney());
			item.put("barginMoney", omsOrdernew.getBarginMoney());
			item.put("address", omsOrdernew.getAddress());
			//来原
			OrderFromCategory orderFromCategory = orderFromCategoryService.selectByPrimaryKey(String.valueOf(omsOrdernew.getDeliveryTypeSid()));
			if(orderFromCategory != null){
				item.put("forcome", orderFromCategory.getProductTitle());
			}else{
				item.put("forcome", "空");
			}
			item.put("orderupdate", getupdatecheck(tsysUser.getPosId(),omsOrdernew.getOrderStatus()));
			List<OmsOrderDetail> omsOrderDetailList = omsOrderDetailService.selectByorderNo(omsOrdernew.getOrderNo());
			if(omsOrderDetailList.size()>0){
				for(OmsOrderDetail omsOrderDetail : omsOrderDetailList){
					omsOrderDetail.setImgUrl(omsOrderDetail.getImgUrl());
				}
			}
			item.put("omsOrderDetail",omsOrderDetailList);
			productItems.add(item);
		}

		data.put("pageNumber", page.getPageNum());
		data.put("pageSize", page.getPageSize());
		data.put("lastPage", page.getLastPage());
		data.put("total", page.getTotal());
		data.put("productItems", productItems);
		return ResultUtils.ok(data);
	}
	public boolean getupdatecheck(String posId,Integer orderStatus){

		if(posId.equals("100")  || posId.equals("101")){//总经理
			if(orderStatus ==1 ){
				return true;
			}
		}
		if(posId.equals("100")  || posId.equals("18")  ){//总经理
			if(orderStatus ==2 ){
				return true;
			}
		}
		return false;
	}

	@GetMapping("/orderFromCategory/list")
	@ResponseBody
	public ApiResult orderFromCategoryList() {
		Map<String, Object> data = new HashMap<>();
		List<Map<String, Object>> selectArray = orderFromCategoryService.selectorderFromCategorylist();
		data.put("selectArray", selectArray);
		return ResultUtils.ok(data);
	}
	@GetMapping("/member/order/list")
	@ResponseBody
	public ApiResult orderlists() {
		Map<String, Object> data = new HashMap<>();
		// 订单头
		List<Map<String, Object>> list = new ArrayList<>();
		for (int i=0; i<5;i++) {
			Map<String, Object> order = new HashMap<>();
			// 状态
			Map<String, Object> pStatus = new HashMap<>();
			pStatus.put("value", "0");
			pStatus.put("text", "PENDING_PAYMENT");
			order.put("status", pStatus);
			order.put("sn", "123123123");
			order.put("amount", "99");
			order.put("createdDate", new Date());
			order.put("quantity","2");

			Boolean  zf=false;
			Boolean  qx=false;
			Boolean  qr=false;
			//显示支付
			zf=true;
			//显示取消
			qx=true;
			//显示确认收货
			qr=true;


			order.put("zf",zf);
			order.put("qx",qx);
			order.put("qr",qr);
			list.add(order);
			// 订单行
			List<Map<String, Object>> items = new ArrayList<>();
			for (int j=0; j<5;j++) {
				Map<String, Object> item = new HashMap<>();
				item.put("thumbnail", "http://localhost:9091/upload/9bf7a3f5-9907-450c-ab44-29de4c749c58.jpg");
				item.put("price", "100");
				item.put("quantity", "2");
				items.add(item);
			}
			order.put("items", items);
		}





		data.put("list", list);
		return ResultUtils.ok(data);
	}
	@GetMapping("/test")
	public Object test() {
		Map<String, String> map=new HashMap<String, String>();
		map.put("a", "1");
		map.put("b", "2");
		map.put("c", "3");
		return map;

	}

}
