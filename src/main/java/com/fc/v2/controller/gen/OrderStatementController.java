package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.service.PlanWorkOrderService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 工单Controller
 * @ClassName: PlanWorkOrderController
 * @author fuce
 * @date 2021-12-19 21:27:47
 */
@Api(value = "工单")
@Controller
@RequestMapping("/OrderStatementController")
public class OrderStatementController extends BaseController{
	
	private String prefix = "gen/orderStatemet";
	
	@Autowired
	private PlanWorkOrderService planWorkOrderService;
	
	
	/**
	 * 工单页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:orderStatement:view")
    public String view(ModelMap model)
    {
		
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "工单", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:orderStatement:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanWorkOrder planWorkOrder){
		PageInfo<PlanWorkOrder> page=planWorkOrderService.orderlist(tablepar,planWorkOrder) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "工单新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:orderStatement:add")
	@ResponseBody
	public AjaxResult add(PlanWorkOrder planWorkOrder){
		int b=planWorkOrderService.insertSelective(planWorkOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 工单删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "工单删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:orderStatement:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planWorkOrderService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "结算跳转", notes = "结算跳转")
	@GetMapping("/jiesuan/{id}")
    public String jiesuan(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanWorkOrder", planWorkOrderService.selectByPrimaryKey(id));

        return prefix + "/jiesuan";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "工单修改", action = "111")
	@ApiOperation(value = "结算保存", notes = "结算保存")
    @RequiresPermissions("gen:orderStatement:jiesuan")
    @PostMapping("/jiesuan")
    @ResponseBody
    public AjaxResult editSave(PlanWorkOrder planWorkOrder)
    {
        return toAjax(planWorkOrderService.updateByPrimaryKeySelective(planWorkOrder));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanWorkOrder planWorkOrder){
		int i=planWorkOrderService.updateVisible(planWorkOrder);
		return toAjax(i);
	}

    
    

	
}
