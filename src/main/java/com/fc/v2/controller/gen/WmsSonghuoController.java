package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsSonghuo;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.WmsSonghuoService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.AutoCode.PageQueryInfo;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 出库单明细Controller
 * @ClassName: WmsSonghuoController
 * @author fuce
 * @date 2022-03-02 14:13:44
 */
@Api(value = "出库单明细")
@Controller
@RequestMapping("/WmsSonghuoController")
public class WmsSonghuoController extends BaseController{
	
	private String prefix = "gen/wmsSonghuo";
	
	@Autowired
	private WmsSonghuoService wmsSonghuoService;
	
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	/**
	 * 出库单明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:wmsSonghuo:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		if(user.getPosId().equals("9")) {
			List<TsysUser> userList = new ArrayList<TsysUser>();
			userList.add(user);
			model.put("userList", userList);
		}else {
			
			List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
			model.put("userList", userList);
		}
		
		
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "出库单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:wmsSonghuo:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,WmsSonghuo wmsSonghuo){
		TsysUser user = ShiroUtils.getUser();
		if(user.getPosId().equals("9")) {
			wmsSonghuo.setCreatedUserSid(user.getId());
		}
		PageInfo<WmsSonghuo> page=wmsSonghuoService.list(tablepar,wmsSonghuo) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listApp")
	@RequiresPermissions("gen:wmsSonghuo:list")
	@ResponseBody
	public AjaxResult listApp(@RequestParam(value = "currentPage", required = false) Integer currentPage,
			@RequestParam(value ="name" ,required = false) String name){
		
		
		Tablepar tablepar = new Tablepar();
		if(currentPage==null) {
			currentPage=1;
		}
		tablepar.setPage(currentPage);
		tablepar.setLimit(10);
		WmsSonghuo wmsSonghuo = new WmsSonghuo();
		wmsSonghuo.setTuname(name);
		
		TsysUser user = ShiroUtils.getUser();
		if(user.getPosId().equals("9")) {
			wmsSonghuo.setCreatedUserSid(user.getId());
		}
		if(user.getPosId().equals("11")) {
			wmsSonghuo.setHouseNo("0");
		}
		PageInfo<WmsSonghuo> page=wmsSonghuoService.list(tablepar,wmsSonghuo) ; 
		
		PageQueryInfo queryInfo = new PageQueryInfo();
		queryInfo.setRows(page.getList());
        queryInfo.setTotal(page.getTotal());
		Map<String, Object> objectMap = new HashMap<String, Object>();
		 objectMap.put("page", queryInfo);
	        
	        
	        return retobject(200, objectMap);
	}
	
	
	
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/getList")
	@RequiresPermissions("gen:wmsSonghuo:list")
	@ResponseBody
	public ResultTable getList(Tablepar tablepar,WmsSonghuo wmsSonghuo){
		TsysUser user = ShiroUtils.getUser();
		if(user.getPosId().equals("9")) {
			wmsSonghuo.setCreatedUserSid(user.getId());
		}
		PageInfo<WmsSonghuo> page=wmsSonghuoService.getList(tablepar,wmsSonghuo) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "出库单明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:wmsSonghuo:add")
	@ResponseBody
	public AjaxResult add(WmsSonghuo wmsSonghuo){
		int b=wmsSonghuoService.insertSelective(wmsSonghuo);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 出库单明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "出库单明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:wmsSonghuo:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=wmsSonghuoService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsSonghuo", wmsSonghuoService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "出库单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:wmsSonghuo:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WmsSonghuo wmsSonghuo)
    {
        return toAjax(wmsSonghuoService.updateByPrimaryKeySelective(wmsSonghuo));
    }
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/zhijian/{id}")
	public String zhijian(@PathVariable("id") String id, ModelMap map)
	{
		map.put("WmsSonghuo", wmsSonghuoService.selectByPrimaryKey(id));
		
		return prefix + "/zhijian";
	}
	
	/**
	 * 修改保存
	 */
	//@Log(title = "出库单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:wmsSonghuo:zhijian")
	@PostMapping("/zhijianSave")
	@ResponseBody
	public AjaxResult zhijianSave(WmsSonghuo wmsSonghuo)
	{
		
		int zhijianSave = wmsSonghuoService.zhijianSave(wmsSonghuo);
		if(zhijianSave==3) {
			return error("数量超限");
		}
		return toAjax(zhijianSave);
	}
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:wmsSonghuo:zhijian")
	@PostMapping("/zhijianSaveAPP")
	@ResponseBody
	public AjaxResult zhijianSaveAPP(WmsSonghuo wmsSonghuo)
	{
		
		int zhijianSave = wmsSonghuoService.zhijianSave(wmsSonghuo);
		if(zhijianSave==3) {
			return error("数量超限");
		}
		
		if(zhijianSave>0) {
			
			return AjaxResult.successData(200, "");
		}else {
			return error("操作失败");
		}
	}
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody WmsSonghuo wmsSonghuo){
		int i=wmsSonghuoService.updateVisible(wmsSonghuo);
		return toAjax(i);
	}

    
    

	
}
