package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.BasePageData;
import com.fc.v2.model.Result;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.*;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.ExeclUtil;
import com.fc.v2.util.ResultUtils;
import com.fc.v2.util.WordToHtml;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.Now;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Controller
 * @ClassName: YyDangjianController
 * @author fuce
 * @date 2023-02-03 21:19:10
 */
@Api(value = "")
@Controller
@RequestMapping("/YyDangjianController")
public class YyDangjianController extends BaseController{
	
	private String prefix = "gen/yyDangjian";
	
	@Autowired
	private YyDangjianService yyDangjianService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private YyNewsTypeService yyNewsTypeService;
	@Autowired
	private WxPayService wxPayService;
	
	/**
	 * 页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyDangjian:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	@GetMapping("/uploadview1")
	public String uploadview1(ModelMap model)
	{
		
		return prefix + "/uploadview1";
	}
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyDangjian:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyDangjian yyDangjian){
		PageInfo<YyDangjian> page=yyDangjianService.list(tablepar,yyDangjian) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<YyNewsType> list= yyNewsTypeService.selectByfw("dj");
		modelMap.put("typelist", list);
        return prefix + "/add";
    }
	@Autowired
	private AliPayService aliPayService;
	/**
	 * 新增跳转
	 */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
	@GetMapping("/add2/{id}")
	@ResponseBody
	public ApiResult add2(@PathVariable("id") Long id) throws AlipayApiException {
		return aliPayService.qrcoe(id);
	}


	@ApiOperation(value = "新增跳转", notes = "新增跳转")
	@GetMapping("/weix/{id}")
	@ResponseBody
	public ApiResult weix(@PathVariable("id") Long id) throws AlipayApiException {
		return wxPayService.qrCodePay(id);
	}
	/**
	 * 新增跳转
	 */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
	@GetMapping("/add3")
	public String add3(ModelMap modelMap) throws AlipayApiException {
		return prefix + "/fu";
	}
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyDangjian:add")
	@ResponseBody
	public AjaxResult add(YyDangjian yyDangjian){
		TsysUser user = ShiroUtils.getUser();
		yyDangjian.setCreateTime(new Date());
		yyDangjian.setDeleteFlag(0);
		yyDangjian.setCreator(user.getId());
		yyDangjian.setField3(user.getNickname());
		yyDangjian.setUpdateTime(new Date());
		yyDangjian.setUpdater(user.getId());
		int b=yyDangjianService.insertSelective(yyDangjian);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyDangjian:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyDangjianService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyDangjian", yyDangjianService.selectByPrimaryKey(id));
		List<YyNewsType> list= yyNewsTypeService.selectByfw("dj");
		map.put("typelist", list);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyDangjian:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyDangjian yyDangjian)
    {
        return toAjax(yyDangjianService.updateByPrimaryKeySelective(yyDangjian));
    }
    
    
    /**
	 * 修改状态
	 * @param
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyDangjian yyDangjian){
		int i=yyDangjianService.updateVisible(yyDangjian);
		return toAjax(i);
	}

	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		
		// 文件上传路径，相对路径
		String filePath = System.getProperty("user.dir")+File.separator+"upload";
		System.out.println("文件上传路径，相对路径"+filePath);
		// 获取文件名
		String fileName = file.getOriginalFilename();

		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));

		// 解决中文问题，liunx下中文路径，图片显示问题
		fileName = UUID.randomUUID() + suffixName;

		File fileDir  = new File(filePath);
		// 检测是否存在目录
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		// 构建真实的文件路径
		File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
		try {
			file.transferTo(dest);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String title = fileName;
		fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
		System.out.println(fileName+"文件上传成功");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key","true");
		jsonObject.put("name",fileName);
		jsonObject.put("title",title);
		jsonObject.put("path",fileName);
		return retobject(200,jsonObject);
	}

	/**
	 * 预览
	 * @param id
	 * @return
	 */
	@GetMapping("/yulan")
	@RequiresPermissions("gen:yyDangjian:remove")
	@ResponseBody
	public AjaxResult yulan(String id, HttpServletRequest request){
		YyDangjian yyDangjian = yyDangjianService.selectByPrimaryKey(id);
		long timeMillis = System.currentTimeMillis();
		System.out.println("开始转换！");
		String sss = "http://" + request.getServerName() + ":" + request.getServerPort();
		String fieldnewpath = yyDangjian.getField2().replace(sss,"");
		String wordFilePath = System.getProperty("user.dir")+File.separator+fieldnewpath;
		String htmlFilePath = System.getProperty("user.dir")+File.separator+"upload/1.html";
		File file = WordToHtml.wordToHtml(wordFilePath, htmlFilePath);
		// 读取html文件
		if (file != null) {
			System.out.println("文件存放路径："+file.getPath());
			System.out.println("转换结束！用时："+ (System.currentTimeMillis()-timeMillis));
		}else{

			System.out.println("文件转换失败！");
		}
		String htmlFilePath2 = sss + "/upload/1.html";
		return success(htmlFilePath2);
	}
	/**
	 * 审核
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "审核", notes = "审核")
	@DeleteMapping("/shenhe")
	@RequiresPermissions("gen:yyDangjian:shenhe")
	@ResponseBody
	public AjaxResult shenhe(String ids){
		int b=yyDangjianService.updateStatusByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 上架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "上架", notes = "上架")
	@DeleteMapping("/shangjia")
	@RequiresPermissions("gen:yyDangjian:shangjia")
	@ResponseBody
	public AjaxResult shangjia(String ids){
		int b=yyDangjianService.shangjiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 下架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "下架", notes = "下架")
	@GetMapping("/add4")
	@ResponseBody
	public AjaxResult add4(Long orderId) throws Exception {
		Result re = aliPayService.aliRefund2(orderId, null);
		System.out.println("退款："+JSONObject.toJSONString(re));
		return success();

	}
	/**
	 * 下架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "下架", notes = "下架")
	@DeleteMapping("/xiajia")
	@RequiresPermissions("gen:yyDangjian:xiajia")
	@ResponseBody
	public AjaxResult xiajia(String ids){
		int b=yyDangjianService.xiajiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 上传图片
	 *
	 * @param file
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/uploadLayeditImage")
	@ResponseBody
	public BasePageData uploadLayeditImage(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
		BasePageData data = new BasePageData();
		try {
			// 文件上传路径，相对路径
			String filePath = System.getProperty("user.dir")+File.separator+"upload";
			System.out.println("文件上传路径，相对路径"+filePath);
			// 获取文件名
			String fileName = file.getOriginalFilename();
			String title = fileName;
			// 获取文件的后缀名
			String suffixName = fileName.substring(fileName.lastIndexOf("."));

			// 解决中文问题，liunx下中文路径，图片显示问题
			fileName = UUID.randomUUID() + suffixName;

			File fileDir  = new File(filePath);
			// 检测是否存在目录
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			// 构建真实的文件路径
			File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
			try {
				file.transferTo(dest);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
			System.out.println(fileName+"文件上传成功");
			Map<String, String> map = new HashMap<>();
			map.put("src",fileName);
			map.put("title", title);
			data.setData(map);
			if (null != filePath) {
				data.setCode(0);
				data.setMsg("上传失败");
			} else {
				data.setCode(-1);
				data.setMsg("上传失败");
			}
		} catch (Exception e) {
			System.out.println("#######上传文件出现异常:" + e.getMessage());
			e.printStackTrace();
			data.setCode(-1);
			data.setMsg("上传失败!");
		}
		return data;
	}

	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload2")
	@ResponseBody
	public AjaxResult upload2(@RequestParam("files") MultipartFile[] files, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		int lengthx = files.length;
		String fileNamessss="";
		String urlsss="";
		for (int i = 0; i < lengthx; i++) {
			// 文件上传路径，相对路径
			String filePath = System.getProperty("user.dir")+File.separator+"upload";
			System.out.println("文件上传路径，相对路径"+filePath);
			// 获取文件名
			String fileName = files[i].getOriginalFilename();

			// 获取文件的后缀名
			String suffixName = fileName.substring(fileName.lastIndexOf("."));

			// 解决中文问题，liunx下中文路径，图片显示问题
			//fileName = UUID.randomUUID() + suffixName;

			File fileDir  = new File(filePath);
			// 检测是否存在目录
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			// 构建真实的文件路径
			File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
			try {
				files[i].transferTo(dest);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			String title = fileName;
			
			
			String urlx = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
			
			
			if(i==lengthx-1) {
				fileNamessss +=fileName;
				urlsss+=urlx;
			}else {
				fileNamessss +=fileName+",";
				urlsss+=urlx+",";
			}
			System.out.println(fileName+"文件上传成功");
			
		}
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key","true");
		jsonObject.put("name",urlsss);
		jsonObject.put("title",fileNamessss);
		jsonObject.put("path",urlsss);
		return retobject(200,jsonObject);
	}
	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload3")
	@ResponseBody
	public AjaxResult upload3(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		// 文件上传路径，相对路径
		String filePath = System.getProperty("user.dir")+File.separator+"upload";
		System.out.println("文件上传路径，相对路径"+filePath);
		// 获取文件名
		String fileName = file.getOriginalFilename();

		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));

		// 解决中文问题，liunx下中文路径，图片显示问题
		//fileName = UUID.randomUUID() + suffixName;

		File fileDir  = new File(filePath);
		// 检测是否存在目录
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		// 构建真实的文件路径
		File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
		try {
			file.transferTo(dest);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String title = fileName;
		fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
		System.out.println(fileName+"文件上传成功");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key","true");
		jsonObject.put("name",fileName);
		jsonObject.put("title",title);
		jsonObject.put("path",fileName);
		return retobject(200,jsonObject);
	}
}
