package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.service.YyNewsTypeService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 发布新闻类类型Controller
 * @ClassName: YyNewsTypeController
 * @author fuce
 * @date 2023-03-07 22:05:52
 */
@Api(value = "发布新闻类类型")
@Controller
@RequestMapping("/YyNewsTypeController")
public class YyNewsTypeController extends BaseController{
	
	private String prefix = "gen/yyNewsType";
	
	@Autowired
	private YyNewsTypeService yyNewsTypeService;
	
	
	/**
	 * 发布新闻类类型页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyNewsType:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "发布新闻类类型", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyNewsType:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyNewsType yyNewsType){
		PageInfo<YyNewsType> page=yyNewsTypeService.list(tablepar,yyNewsType) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "发布新闻类类型新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyNewsType:add")
	@ResponseBody
	public AjaxResult add(YyNewsType yyNewsType){
		TsysUser user = ShiroUtils.getUser();
		yyNewsType.setCreateTime(new Date());
		yyNewsType.setDeleteFlag(0);
		yyNewsType.setCreator(user.getId());
		yyNewsType.setField3(user.getNickname());
		yyNewsType.setUpdateTime(new Date());
		yyNewsType.setUpdater(user.getId());
		int b=yyNewsTypeService.insertSelective(yyNewsType);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 发布新闻类类型删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "发布新闻类类型删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyNewsType:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyNewsTypeService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyNewsType", yyNewsTypeService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "发布新闻类类型修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyNewsType:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyNewsType yyNewsType)
    {
		yyNewsType.setUpdateTime(new Date());
        return toAjax(yyNewsTypeService.updateByPrimaryKeySelective(yyNewsType));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyNewsType yyNewsType){
		int i=yyNewsTypeService.updateVisible(yyNewsType);
		return toAjax(i);
	}

    
    

	
}
