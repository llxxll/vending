package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.WmsWarehouseEntryDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 入库单明细Controller
 * @ClassName: WmsWarehouseEntryDetailController
 * @author fuce
 * @date 2021-12-19 21:28:17
 */
@Api(value = "入库单明细")
@Controller
@RequestMapping("/WmsWarehouseEntryDetailController")
public class WmsWarehouseEntryDetailController extends BaseController{
	
	private String prefix = "gen/wmsWarehouseEntryDetail";
	
	@Autowired
	private WmsWarehouseEntryDetailService wmsWarehouseEntryDetailService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	
	/**
	 * 入库单明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:view")
    public String view(ModelMap model)
    {
		
		
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(null);
		 model.put("userList", userList);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "入库单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
		 Long userId = ShiroUtils.getUserId();
			TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("3")) {
			wmsWarehouseEntryDetail.setHouseNo(user.getRemark());
		}
		
		PageInfo<WmsWarehouseEntryDetail> page=wmsWarehouseEntryDetailService.list(tablepar,wmsWarehouseEntryDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/viewwaixie")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:viewwaixie")
    public String viewwaixie(ModelMap model)
    {
		
		
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(null);
		 model.put("userList", userList);
        return prefix + "/waixieruku";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "入库单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listwwaixie")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:listwwaixie")
	@ResponseBody
	public ResultTable listwwaixie(Tablepar tablepar,WmsWarehouseEntryDetail wmsWarehouseEntryDetail) throws ParseException{
		 Long userId = ShiroUtils.getUserId();
			TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("3")) {
			wmsWarehouseEntryDetail.setHouseNo(user.getRemark());
		}
		wmsWarehouseEntryDetail.setQualityNo("1");
		
		String beginTime = wmsWarehouseEntryDetail.getBeginTime();
		 String endTime = wmsWarehouseEntryDetail.getEndTime();
		 if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 wmsWarehouseEntryDetail.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 wmsWarehouseEntryDetail.setEndTime(format);
			 }
		 }
		
		
		PageInfo<WmsWarehouseEntryDetail> page=wmsWarehouseEntryDetailService.listwwaixie(tablepar,wmsWarehouseEntryDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/viewchejianlist")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:viewchejianlist")
    public String viewchejianlist(ModelMap model)
    {
		
		
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(null);
		 model.put("userList", userList);
        return prefix + "/chejianruku";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "入库单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/chejianlist")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:chejianlist")
	@ResponseBody
	public ResultTable chejianlist(Tablepar tablepar,WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
		 Long userId = ShiroUtils.getUserId();
			TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("3")) {
			wmsWarehouseEntryDetail.setHouseNo(user.getRemark());
		}
		wmsWarehouseEntryDetail.setQualityNo("2");
		PageInfo<WmsWarehouseEntryDetail> page=wmsWarehouseEntryDetailService.chejianlist(tablepar,wmsWarehouseEntryDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	
	
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/getlist")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:list")
	@ResponseBody
	public ResultTable getlist(Tablepar tablepar,WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
		
		PageInfo<WmsWarehouseEntryDetail> page=wmsWarehouseEntryDetailService.getlist(tablepar,wmsWarehouseEntryDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "入库单明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:add")
	@ResponseBody
	public AjaxResult add(WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
		int b=wmsWarehouseEntryDetailService.insertSelective(wmsWarehouseEntryDetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 入库单明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "入库单明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=wmsWarehouseEntryDetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseEntryDetail", wmsWarehouseEntryDetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "入库单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:wmsWarehouseEntryDetail:edit")
    @PostMapping("/editSave")
    @ResponseBody
    public AjaxResult editSave(WmsWarehouseEntryDetail wmsWarehouseEntryDetail)
    {
		Long mostmatterNumber = wmsWarehouseEntryDetail.getMostmatterNumber();
		if(mostmatterNumber!=null) {
		if(wmsWarehouseEntryDetail.getMatterNumber()>wmsWarehouseEntryDetail.getMostmatterNumber()) {
			return error("超出最大数量");
		}
		}
		int editSave = wmsWarehouseEntryDetailService.editSave(wmsWarehouseEntryDetail);
		if(editSave==3) {
			return error("单据已完成无法修改,请上级反馈");
		}
        return toAjax(editSave);
    }
	@ApiOperation(value = "撤销", notes = "撤销")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:chexiao")
	@DeleteMapping("/chexiao")
	@ResponseBody
	public AjaxResult chexiao(String ids){
		int b=wmsWarehouseEntryDetailService.chexiao(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "zhuanxu跳转", notes = "zhuanxu跳转")
	@GetMapping("/tozhuanxu/{id}")
    public String tozhuanxu(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseEntryDetail", wmsWarehouseEntryDetailService.selectByPrimaryKey(id));
        List<SysDepartment> selectSysDepartmentListByParams = planProduceMaterialDetailService.selectSysDepartmentListByParams();
        map.put("departmentList", selectSysDepartmentListByParams);
        return prefix + "/zhuanxu";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "入库单明细修改", action = "111")
	@ApiOperation(value = "转序保存", notes = "转序保存")
    @RequiresPermissions("gen:wmsWarehouseEntryDetail:zhuanxu")
    @PostMapping("/savezhuanxu")
    @ResponseBody
    public AjaxResult savezhuanxu(WmsWarehouseEntryDetail wmsWarehouseEntryDetail)
    {
		
		int savezhuanxu = wmsWarehouseEntryDetailService.savezhuanxu(wmsWarehouseEntryDetail);
		if(savezhuanxu==3) {
			return error("重复操作");
		}
        return toAjax(savezhuanxu);
    }
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody WmsWarehouseEntryDetail wmsWarehouseEntryDetail){
		int i=wmsWarehouseEntryDetailService.updateVisible(wmsWarehouseEntryDetail);
		return toAjax(i);
	}

    

	/**
	 * 确认入库跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "入库跳转", notes = "入库跳转")
	@GetMapping("/toruku/{id}")
    public String toruku(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseEntryDetail", wmsWarehouseEntryDetailService.selectByPrimaryKey(id));
        

        return prefix + "/ruku";
    }
	
	/**
     * 入库保存
     */
    //@Log(title = "入库单明细修改", action = "111")
	@ApiOperation(value = "入库保存", notes = "入库保存")
    @PostMapping("/saveruku")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:ruku")
    @ResponseBody
    public AjaxResult saveruku(WmsWarehouseEntryDetail wmsWarehouseEntryDetail)
    {
		int updateByPrimaryKeySelective = wmsWarehouseEntryDetailService.updateByPrimaryKeySelective(wmsWarehouseEntryDetail);
		
		if(updateByPrimaryKeySelective==3) {
			return error("重复操作");
		}
		if(updateByPrimaryKeySelective==9) {
			return error("去料类型入库不能超过领料数");
		}
			return  toAjax(updateByPrimaryKeySelective);
		
    }

	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/piliangzhuanxu")
	public String piliangzhuanxu(@RequestParam(value="ids") String ids, ModelMap map)
	{
		 List<SysDepartment> selectSysDepartmentListByParams = planProduceMaterialDetailService.selectSysDepartmentListByParams();
	        map.put("departmentList", selectSysDepartmentListByParams);
		map.put("ids", ids);
		
		
		return prefix + "/piliangzhuan";
	}
	
	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	  @RequiresPermissions("gen:wmsWarehouseEntryDetail:piliangzhuanxu")
	@PostMapping("/piliangzhuanxusave")
	@ResponseBody
	public AjaxResult piliangzhuanxusave(@RequestParam(value="ids")String ids,@RequestParam(value="xianManger")String xianManger){
		int b=wmsWarehouseEntryDetailService.piliangzhuanxusave(ids,xianManger);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
}
