package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.PlanOudetailService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.ExeclUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 生产计划明细Controller
 * @ClassName: PlanOudetailController
 * @author fuce
 * @date 2022-03-02 14:13:11
 */
@Api(value = "生产计划明细")
@Controller
@RequestMapping("/PlanOudetailController")
public class PlanOudetailController extends BaseController{
	
	private String prefix = "gen/planOudetail";
	
	@Autowired
	private PlanOudetailService planOudetailService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private JshDepotService jshDepotService;
	
	/**
	 * 生产计划明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planOudetail:view")
    public String view(ModelMap model)
    {
		
		List<JshDepot> houseList = jshDepotService.getHouseList();
		model.put("houseList", houseList);
		List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
		model.put("userList", userList);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "生产计划明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planOudetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanOudetail planOudetail) throws ParseException{
		
		String beginTime = planOudetail.getBeginTime();
		String endTime = planOudetail.getEndTime();
 if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planOudetail.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planOudetail.setEndTime(format);
			 }
		 }
		PageInfo<PlanOudetail> page=planOudetailService.list(tablepar,planOudetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
		
		 modelMap.put("userList", userList);
		 List<JshDepot> houseList = jshDepotService.getHouseList();
		 modelMap.put("houseList", houseList);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "生产计划明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/addsave")
	@RequiresPermissions("gen:planOudetail:add")
	@ResponseBody
	public AjaxResult addsave(@RequestBody PlanOudetail planOudetail){
		planOudetail.setCount(null);
		//获取类型
		String field2 = planOudetail.getField2();
		String standard = planOudetail.getStandard();
		String trim = standard.trim();
		if(trim.equals("")) {
			planOudetail.setStandard(null);
		}
		if(field2.equals("0")) {
			if(planOudetail.getStandard()==null) {
				return error("去料请选择仓库");
			}
			
			
		}else if(field2.equals("1")) {
			planOudetail.setStandard(null);
		}
		
		List<PlanOudetailGongying> planOudetailGongyingList = planOudetail.getPlanOudetailGongyingList();
		int size = planOudetailGongyingList.size();
		if(size<=0) {
			return error("请选择物料");
		}
		int b=planOudetailService.insertSelective(planOudetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 生产计划明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "生产计划明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planOudetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planOudetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
		
		 map.put("userList", userList);
		
		
        map.put("PlanOudetail", planOudetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/piliangkaidan")
	public String piliangkaidan(@RequestParam(value="ids") String ids, ModelMap map)
	{
		List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
		
		map.put("userList", userList);
		map.put("ids", ids);
		
		
		return prefix + "/piliangkaidan";
	}
	
	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	  @RequiresPermissions("gen:planOudetail:piliangkaidan")
	@PostMapping("/piliangkaidansave")
	@ResponseBody
	public AjaxResult piliangkaidansave(@RequestParam(value="ids")String ids,@RequestParam(value="departmentOne")String departmentOne){
		int b=planOudetailService.piliangkaidansave(ids,departmentOne);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/piliangquliao")
	public String piliangquliao(@RequestParam(value="ids") String ids, ModelMap map)
	{
		List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
		List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("houseList", houseList);
		map.put("userList", userList);
		map.put("ids", ids);
		
		
		return prefix + "/piliangquliao";
	}
	
	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@RequiresPermissions("gen:planOudetail:piliangquliao")
	@PostMapping("/piliangquliaosave")
	@ResponseBody
	public AjaxResult piliangquliaosave(@RequestParam(value="ids")String ids,
			@RequestParam(value="departmentOne")String departmentOne,
			@RequestParam(value="standard")String standard){
		int b=planOudetailService.piliangquliaosave(ids,departmentOne,standard);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
     * 修改保存
     */
    //@Log(title = "生产计划明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planOudetail:edit")
    @PostMapping("/editSave")
    @ResponseBody
    public AjaxResult editSave(PlanOudetail planOudetail)
    {
        return toAjax(planOudetailService.updateByPrimaryKeySelective(planOudetail));
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/kaidan/{id}")
    public String kaidan(@PathVariable("id") String id, ModelMap map)
    {
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
		
		 map.put("userList", userList);
		 List<JshDepot> houseList = jshDepotService.getHouseList();
			map.put("houseList", houseList);
		
        map.put("PlanOudetail", planOudetailService.selectByPrimaryKey(id));

        return prefix + "/kaidan";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "生产计划明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planOudetail:kaidan")
    @PostMapping("/kaidanSave")
    @ResponseBody
    public AjaxResult kaidanSave(PlanOudetail planOudetail)
    {
		if(planOudetail.getField2().equals("1")) {
			String standard = planOudetail.getStandard();
			if(standard.isEmpty()) {
				return error("去料请选择领料库");
			}
			
		}
		Integer zuidaCount = planOudetail.getZuidaCount();
		Integer songhuocount = planOudetail.getSonghuocount();
		if(songhuocount>zuidaCount) {
			return error("超出计划上限");
		}
		
        return toAjax(planOudetailService.kaidanSave(planOudetail));
    }
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanOudetail planOudetail){
		int i=planOudetailService.updateVisible(planOudetail);
		return toAjax(i);
	}

    
    @ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload")
    @RequiresPermissions("gen:planOudetail:upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam("file") MultipartFile file) throws Exception
	{
		InputStream inputStream = file.getInputStream();
		Map<String, String> fieldd = new HashMap<String, String>();
		fieldd.put("物料号", "materialName");
		fieldd.put("物料描述", "materiaNo");
		fieldd.put("材料", "standard");
        fieldd.put("材料规格", "departmentTwo");
        fieldd.put("协作供应商", "departmentOne");
        fieldd.put("主机厂", "field3");
        fieldd.put("数量", "count");
        fieldd.put("备注", "material");
		
		List<PlanOudetail> execltoList = ExeclUtil.ExecltoList(inputStream, PlanOudetail.class, fieldd, file.getOriginalFilename());
		int b= planOudetailService.upload(execltoList);

		if(b>0){
			return success();
		}else{
			return error();
		}
	}

	
}
