package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanWorkOrderPzStatement;
import com.fc.v2.service.PlanWorkOrderPzStatementService;
import com.fc.v2.util.AutoCode.ExcelUtils;
import com.fc.v2.util.AutoCode.ExportExecUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 工单明细结算表(结算明细)Controller
 * @ClassName: PlanWorkOrderPzStatementController
 * @author fuce
 * @date 2022-02-26 16:24:34
 */
@Api(value = "工单明细结算表(结算明细)")
@Controller
@RequestMapping("/PlanWorkOrderPzStatementController")
public class PlanWorkOrderPzStatementController extends BaseController{
	
	private String prefix = "gen/planWorkOrderPzStatement";
	
	@Autowired
	private PlanWorkOrderPzStatementService planWorkOrderPzStatementService;
	
	
	/**
	 * 工单明细结算表(结算明细)页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planWorkOrderPzStatement:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "工单明细结算表(结算明细)", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planWorkOrderPzStatement:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanWorkOrderPzStatement planWorkOrderPzStatement) throws ParseException{
		PageInfo<PlanWorkOrderPzStatement> page=planWorkOrderPzStatementService.list(tablepar,planWorkOrderPzStatement) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "工单明细结算表(结算明细)新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:planWorkOrderPzStatement:add")
	@ResponseBody
	public AjaxResult add(PlanWorkOrderPzStatement planWorkOrderPzStatement){
		int b=planWorkOrderPzStatementService.insertSelective(planWorkOrderPzStatement);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 工单明细结算表(结算明细)删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "工单明细结算表(结算明细)删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planWorkOrderPzStatement:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planWorkOrderPzStatementService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanWorkOrderPzStatement", planWorkOrderPzStatementService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "工单明细结算表(结算明细)修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planWorkOrderPzStatement:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlanWorkOrderPzStatement planWorkOrderPzStatement)
    {
        return toAjax(planWorkOrderPzStatementService.updateByPrimaryKeySelective(planWorkOrderPzStatement));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanWorkOrderPzStatement planWorkOrderPzStatement){
		int i=planWorkOrderPzStatementService.updateVisible(planWorkOrderPzStatement);
		return toAjax(i);
	}

    
    @GetMapping(value = "/exportstatementExcel")
    @RequiresPermissions("gen:planWorkOrderPzStatement:daochu")
    public void exportstatementExcel(@RequestParam("name") String name,
                                        @RequestParam("beginTime") String beginTime,
                                        @RequestParam("endTime") String endTime,
                                		@RequestParam(value ="searchWorker" ,required = false) String searchWorker,
                                        HttpServletRequest request, HttpServletResponse response) {
		
    	PlanWorkOrderPzStatement  stock= new PlanWorkOrderPzStatement();
		stock.setMaterialNo(name);
		stock.setWorker(searchWorker);
		stock.setBeginTime(beginTime);
		stock.setEndTime(endTime);
        try {
        	List<PlanWorkOrderPzStatement> list = planWorkOrderPzStatementService.exclist(stock);
        	
            String[] names = {"工单编号", "物料号",  "物料描述", "工艺描述","工艺名称", 
            		"工艺规格", "车间名称","单价",
            		"工人", "数量",  "总价", "完工时间","结算时间", "结算人"};
            String title = "工资结算报表";
            List<String[]> objects = new ArrayList<String[]>();
            if (null != list) {
                for (PlanWorkOrderPzStatement s : list) {
                	String[] objs = new String[14];
                    objs[0] = s.getMaterialNo();
                    objs[1] = s.getTuname();
                    objs[2] = s.getTuno();
                    objs[3] = s.getMaterialName();
                    objs[4] = s.getCarftName();
                    objs[5] = s.getSpesc();
                    objs[6] = s.getOrgName();
                    objs[7] = s.getPrice().toString();
                    objs[8] = s.getWorker();
                    objs[9] = s.getCount().toString();
                    objs[10] = s.getAlAccount().toString();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        			
        				Calendar ca = Calendar.getInstance();
        				String format = null;
        				Date distributionTime = s.getDistributionTime();
        				if(distributionTime!=null) {
        					
        					ca.setTime(s.getDistributionTime());
        					format = sf.format(ca.getTime());
        				}
        				//ca.add(Calendar.HOUR, 8);
        				Calendar ca1 = Calendar.getInstance();
        				ca1.setTime(s.getCreateTime());
        				//ca1.add(Calendar.HOUR, 8);
        				String format1 = sf.format(ca1.getTime());
                    
                    objs[11] = format;
                    objs[12] = format1;
                    objs[13] = s.getCreaterName();
                    
                    objects.add(objs);
                }
            }
            File file = ExcelUtils.exportObjectsWithoutTitle(title, names, title, objects);
            ExportExecUtil.showExec(file, file.getName(), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
	    }

	
}
