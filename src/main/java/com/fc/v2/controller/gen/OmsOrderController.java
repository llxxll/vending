package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.controller.admin.app.ApiController;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.OmsOrderDetailService;
import com.fc.v2.service.OmsOrderService;
import com.fc.v2.service.OrderFromCategoryService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.StringUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 销售-订单表Controller
 * @ClassName: OmsOrderController
 * @author fuce
 * @date 2022-05-21 23:28:35
 */
@Api(value = "销售-订单表")
@Controller
@RequestMapping("/OmsOrderController")
public class OmsOrderController extends BaseController{
	
	private String prefix = "gen/omsOrder";
	
	@Autowired
	private OmsOrderService omsOrderService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	ApiController apiController;

	@Autowired
	private OrderFromCategoryService orderFromCategoryService;
	@Autowired
	private OmsOrderDetailService omsOrderDetailService;

	/**
	 * 销售-订单表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:omsOrder:view")
    public String view(ModelMap model)
    {
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		model.put("lyList", lyList);
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		List<TsysUser> selectListByDept =null;
		if(roleId.equals("102")||roleId.equals("109")) {
			selectListByDept = new ArrayList<TsysUser>();
			selectListByDept.add(user);
		}else {
			selectListByDept = planProduceMaterialDetailService.selectListByDept(null);

		}

		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "销售-订单表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:omsOrder:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OmsOrder omsOrder) throws ParseException{
		//获取当前登录人 
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		
		if(roleId.equals("102")||roleId.equals("109")) {
			if(user.getId()==556) {
				List<Long> delid=new ArrayList<Long>();
				delid.add(556l);
				delid.add(557l);
				omsOrder.setDelId(delid);
			}else if(user.getId()==555) {
				List<Long> delid=new ArrayList<Long>();
				delid.add(555l);
				delid.add(558l);
				omsOrder.setDelId(delid);
			}else {
				
				omsOrder.setSellSupplySid(user.getId());
			}
		}
		if(roleId.equals("101")) {
			List<Long> delid=new ArrayList<Long>();
			String remark = user.getRemark();
			String[] split = remark.split(",");
			for (String string : split) {
				delid.add(Long.valueOf(string));
			}
			delid.add(user.getId());
			omsOrder.setDelId(delid);
		}
		String beginTime = omsOrder.getBeginTime();
		String endTime = omsOrder.getEndTime();
 if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 omsOrder.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 omsOrder.setEndTime(format);
			 }
		 }
		PageInfo<OmsOrder> page=omsOrderService.list(tablepar,omsOrder) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		modelMap.put("lyList", lyList);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "销售-订单表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:omsOrder:add")
	@ResponseBody
	public AjaxResult add(@RequestBody OmsOrder omsOrder){
		omsOrder.setBuySupplySid(1l);
		int b=omsOrderService.insertSelective(omsOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 新增跳转
	 */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
	@GetMapping("/addda")
	public String addda(ModelMap modelMap)
	{
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		modelMap.put("lyList", lyList);
		return prefix + "/adddakehu";
	}
	
	/**
	 * 新增保存
	 * @param 
	 * @return
	 */
	//@Log(title = "销售-订单表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/adddasave")
	@RequiresPermissions("gen:omsOrder:addda")
	@ResponseBody
	public AjaxResult adddasave(@RequestBody OmsOrder omsOrder){
		omsOrder.setBuySupplySid(2l);
		int b=omsOrderService.insertSelective(omsOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 销售-订单表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "销售-订单表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:omsOrder:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=omsOrderService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}

	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/chexiao")
	@RequiresPermissions("gen:omsOrder:chexiao")
	@ResponseBody
	public AjaxResult chexiao(Long ids){
		int b =omsOrderService.chexiao(ids);
		if(b==3) {
			return error("状态不符");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/wancheng")
	@RequiresPermissions("gen:omsOrder:wancheng")
	@ResponseBody
	public AjaxResult wancheng(Long ids){
		int b =omsOrderService.wancheng(ids);
		if(b==3) {
			return error("退货未完成");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/querenfukuan")
	@RequiresPermissions("gen:omsOrder:querenfukuan")
	@ResponseBody
	public AjaxResult querenfukuan(String ids){
		int b =omsOrderService.querenfukuan(ids);
		
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/updateStatus")
	@RequiresPermissions("gen:omsOrder:updateStatus")
	@ResponseBody
	public AjaxResult updateStatus(String ids){
		OmsOrder omsOrder = omsOrderService.selectByPrimaryKey(ids);
		TsysUser user = ShiroUtils.getUser();
		int b =  omsOrderService.updateOmsOrderByStatusweb(ids, String.valueOf(omsOrder.getOrderStatus()+1), user.getId());
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/caiwuqueren")
	@RequiresPermissions("gen:omsOrder:caiwuqueren")
	@ResponseBody
	public AjaxResult caiwuqueren(Long ids){
		int b =omsOrderService.caiwuqueren(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/shengcanwancheng")
	@RequiresPermissions("gen:omsOrder:shengcanwancheng")
	@ResponseBody
	public AjaxResult shengcanwancheng(Long ids){
		int b =omsOrderService.shengcanwancheng(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "改状态", notes = "改状态")
	@DeleteMapping("/shengcanpai")
	@RequiresPermissions("gen:omsOrder:shengcanpai")
	@ResponseBody
	public AjaxResult shengcanpai(Long ids){
		int b =omsOrderService.shengcanpai(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/fahuoquerendetail/{sid}")
	public String fahuoquerendetail(@PathVariable("sid") String id, ModelMap map)
	{
		map.put("OmsOrder", omsOrderService.selectByPrimaryKey(id));
		
		return prefix + "/fahuoqueren";
	}
	
	@ApiOperation(value = "改状态", notes = "改状态")
	@PostMapping("/fahuoqueren")
	@RequiresPermissions("gen:omsOrder:fahuoqueren")
	@ResponseBody
	public AjaxResult fahuoqueren(OmsOrder omsOrder){
		omsOrder.setOrderStatus(4);
		int b  = omsOrderService.fahuoqueren(omsOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@RequiresPermissions("gen:omsOrder:edit")
	@GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") String id, ModelMap map)
    {
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		map.put("lyList", lyList);
		OmsOrder selectByPrimaryKey = omsOrderService.selectByPrimaryKey(id);
        map.put("OmsOrder", selectByPrimaryKey);
if(selectByPrimaryKey.getBuySupplySid()==null) {
        	
        	return prefix + "/editdakehu";
        }
if(selectByPrimaryKey.getBuySupplySid()==1) {
	
	return prefix + "/edit";
}else {
	return prefix + "/editdakehu";
	
}
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "销售-订单表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody OmsOrder omsOrder)
    {
		
		if(omsOrder.getMdProduct()==null) {
			return error("请添加明细");
		}
		if(omsOrder.getMdProduct().size()<=0) {
			return error("请添加明细");
		}
		int updateByPrimaryKeySelective = omsOrderService.updateByPrimaryKeySelective(omsOrder);
		
        return toAjax(updateByPrimaryKeySelective);
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@RequiresPermissions("gen:omsOrder:edit1")
	@GetMapping("/edit1/{sid}")
	public String edit1(@PathVariable("sid") String id, ModelMap map)
	{
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		map.put("lyList", lyList);
		OmsOrder selectByPrimaryKey = omsOrderService.selectByPrimaryKey(id);
        map.put("OmsOrder", selectByPrimaryKey);
        if(selectByPrimaryKey.getBuySupplySid()==null) {
        	
        	return prefix + "/editdakehu";
        }
if(selectByPrimaryKey.getBuySupplySid()==1) {
	
	return prefix + "/edit";
}else {
	return prefix + "/editdakehu";
	
}
	}
	
	
	/**
	 * 修改保存
	 */
	
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OmsOrder omsOrder){
		int i=omsOrderService.updateVisible(omsOrder);
		return toAjax(i);
	}
	/**
	 * 详情跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/fahudetail/{sid}")
    public String fahudetail(@PathVariable("sid") String id, ModelMap map)
    {
    	List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
    	map.put("lyList", lyList);
    	
    	OmsOrder order = omsOrderService.selectByPrimaryKey(id);
    	for(OrderFromCategory orderFromCategory : lyList){
    		if(order.getDeliveryTypeSid() == orderFromCategory.getSid().intValue()){
    			order.setDeliveryTypeName(orderFromCategory.getProductTitle());
    		}
    	}
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
    	String format = dateFormat.format(order.getCreateTime());
    	order.setCreateTimes(format);
    	
    	List<OmsOrderDetail> omsOrderDetails = omsOrderDetailService.getList(order.getOrderNo());
    	map.put("OmsOrderDetail", JSONObject.toJSON(omsOrderDetails));
    	OrderLiushui orderLiushui = omsOrderService.getUpdateUserSid(order.getOrderNo());
    	if(orderLiushui!=null) {
    		
    		order.setUserName(orderLiushui.getUserName());
    		order.setUpdateTimes(dateFormat.format(orderLiushui.getCreateTime()));
    	}
    	map.put("OmsOrder", order);
    	return prefix + "/fahuodetail";
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/detail/{sid}")
	public String detail(@PathVariable("sid") String id, ModelMap map)
	{
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		map.put("lyList", lyList);

		OmsOrder order = omsOrderService.selectByPrimaryKey(id);
		for(OrderFromCategory orderFromCategory : lyList){
			if(order.getDeliveryTypeSid() == orderFromCategory.getSid().intValue()){
				order.setDeliveryTypeName(orderFromCategory.getProductTitle());
			}
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		String format = dateFormat.format(order.getCreateTime());
		order.setCreateTimes(format);

		List<OmsOrderDetail> omsOrderDetails = omsOrderDetailService.getList(order.getOrderNo());
		map.put("OmsOrderDetail", JSONObject.toJSON(omsOrderDetails));
		OrderLiushui orderLiushui = omsOrderService.getUpdateUserSid(order.getOrderNo());
		if(orderLiushui!=null) {
			
			order.setUserName(orderLiushui.getUserName());
			order.setUpdateTimes(dateFormat.format(orderLiushui.getCreateTime()));
		}
		map.put("OmsOrder", order);
		return prefix + "/detail";
	}

	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listshengcan")
	@RequiresPermissions("gen:omsOrder:shengcanlist")
	@ResponseBody
	public ResultTable listshengcan(Tablepar tablepar,OmsOrder omsOrder){
		//获取当前登录人
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		if(roleId.equals("102")||roleId.equals("109")) {
			omsOrder.setSellSupplySid(user.getId());
		}
		
		
		PageInfo<OmsOrder> page=omsOrderService.listshengcan(tablepar,omsOrder) ;
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listfahuo")
	@RequiresPermissions("gen:omsOrder:list")
	@ResponseBody
	public ResultTable listfahuo(Tablepar tablepar,OmsOrder omsOrder){
		//获取当前登录人
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		if(roleId.equals("102")||roleId.equals("109")) {
			omsOrder.setSellSupplySid(user.getId());
		}
		

		PageInfo<OmsOrder> page=omsOrderService.listFH(tablepar,omsOrder) ;
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/fahuo")
	@RequiresPermissions("gen:omsOrder:view")
	public String fahuo(ModelMap model)
	{
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		model.put("lyList", lyList);
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		List<TsysUser> selectListByDept =null;
		if(roleId.equals("102")||roleId.equals("109")) {
			selectListByDept = new ArrayList<TsysUser>();
			selectListByDept.add(user);
		}else {
			selectListByDept = planProduceMaterialDetailService.selectListByDept(null);

		}

		model.put("userList", selectListByDept);
		return prefix + "/fahuo";
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/shengcan")
	@RequiresPermissions("gen:omsOrder:view")
	public String shengcan(ModelMap model)
	{
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		model.put("lyList", lyList);
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		List<TsysUser> selectListByDept =null;
		if(roleId.equals("102")||roleId.equals("109")) {
			selectListByDept = new ArrayList<TsysUser>();
			selectListByDept.add(user);
		}else {
			selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
			model.put("userList", selectListByDept);
		}
		return prefix + "/shengcan";
	}
		
	

}
