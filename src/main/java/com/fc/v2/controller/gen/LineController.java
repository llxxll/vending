package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.mapper.auto.CompanyMapper;
import com.fc.v2.mapper.auto.HeadMapper;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.CompanyService;
import com.fc.v2.service.HeadService;
import com.fc.v2.service.LineService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 线路管理表Controller
 * @ClassName: LineController
 * @author lxl
 * @date 2023-12-01 16:35:21
 */
@Api(value = "线路管理表")
@Controller
@RequestMapping("/LineController")
public class LineController extends BaseController{
	
	private String prefix = "gen/line";
	
	@Autowired
	private LineService lineService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private HeadService headService;
	@Resource
	private CompanyMapper companyMapper;
	@Resource
	private HeadMapper headMapper;
	/**
	 * 线路管理表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:line:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "线路管理表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:line:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,Line line){
		PageInfo<Line> page=lineService.list(tablepar,line) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<Company> clist= companyMapper.selectByExample(new CompanyExample());
		List<Head> hlist= headMapper.selectByExample(new HeadExample());
		modelMap.put("CompanyList", clist);
		modelMap.put("HeadList", hlist);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "线路管理表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:line:add")
	@ResponseBody
	public AjaxResult add(Line line){
		int b=lineService.insertSelective(line);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 线路管理表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "线路管理表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:line:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=lineService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Line", lineService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "线路管理表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:line:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Line line)
    {
        return toAjax(lineService.updateByPrimaryKeySelective(line));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody Line line){
		int i=lineService.updateVisible(line);
		return toAjax(i);
	}

    
    

	
}
