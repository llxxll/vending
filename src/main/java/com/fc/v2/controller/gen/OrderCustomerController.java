package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.OrderCustomer;
import com.fc.v2.service.OrderCustomerService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 客户表Controller
 * @ClassName: OrderCustomerController
 * @author fuce
 * @date 2022-09-15 20:45:08
 */
@Api(value = "客户表")
@Controller
@RequestMapping("/OrderCustomerController")
public class OrderCustomerController extends BaseController{
	
	private String prefix = "gen/orderCustomer";
	
	@Autowired
	private OrderCustomerService orderCustomerService;
	
	
	/**
	 * 客户表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:orderCustomer:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "客户表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:orderCustomer:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OrderCustomer orderCustomer){
		PageInfo<OrderCustomer> page=orderCustomerService.list(tablepar,orderCustomer) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "客户表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:orderCustomer:add")
	@ResponseBody
	public AjaxResult add(OrderCustomer orderCustomer){
		int b=orderCustomerService.insertSelective(orderCustomer);
		if(b==3) {
			return error("重复数据(电话号码唯一)");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 客户表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "客户表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:orderCustomer:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=orderCustomerService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("OrderCustomer", orderCustomerService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "客户表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:orderCustomer:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OrderCustomer orderCustomer)
    {
		int updateByPrimaryKeySelective = orderCustomerService.updateByPrimaryKeySelective(orderCustomer);
		if(updateByPrimaryKeySelective==3) {
			return error("数据重复");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OrderCustomer orderCustomer){
		int i=orderCustomerService.updateVisible(orderCustomer);
		return toAjax(i);
	}

    
    

	
}
