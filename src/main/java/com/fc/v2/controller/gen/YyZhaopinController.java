package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.YyDangjian;
import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyZhaopin;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyNewsTypeService;
import com.fc.v2.service.YyZhaopinService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.WordToHtml;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 招聘信息Controller
 * @ClassName: YyZhaopinController
 * @author fuce
 * @date 2023-02-10 16:01:46
 */
@Api(value = "招聘信息")
@Controller
@RequestMapping("/YyZhaopinController")
public class YyZhaopinController extends BaseController{
	
	private String prefix = "gen/yyZhaopin";
	
	@Autowired
	private YyZhaopinService yyZhaopinService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private YyNewsTypeService yyNewsTypeService;
	
	/**
	 * 招聘信息页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyZhaopin:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "招聘信息", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyZhaopin:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyZhaopin yyZhaopin){
		PageInfo<YyZhaopin> page=yyZhaopinService.list(tablepar,yyZhaopin) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<YyNewsType> list= yyNewsTypeService.selectByfw("zp");
		modelMap.put("typelist", list);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "招聘信息新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyZhaopin:add")
	@ResponseBody
	public AjaxResult add(YyZhaopin yyZhaopin){
		TsysUser user = ShiroUtils.getUser();
		yyZhaopin.setCreateTime(new Date());
		yyZhaopin.setDeleteFlag(0);
		yyZhaopin.setCreator(user.getId());
		yyZhaopin.setField3(user.getNickname());
		yyZhaopin.setUpdateTime(new Date());
		yyZhaopin.setUpdater(user.getId());
		int b=yyZhaopinService.insertSelective(yyZhaopin);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 招聘信息删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "招聘信息删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyZhaopin:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyZhaopinService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyZhaopin", yyZhaopinService.selectByPrimaryKey(id));
		List<YyNewsType> list= yyNewsTypeService.selectByfw("zp");
		map.put("typelist", list);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "招聘信息修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyZhaopin:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyZhaopin yyZhaopin)
    {
        return toAjax(yyZhaopinService.updateByPrimaryKeySelective(yyZhaopin));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyZhaopin yyZhaopin){
		int i=yyZhaopinService.updateVisible(yyZhaopin);
		return toAjax(i);
	}

	/**
	 * 预览
	 * @param id
	 * @return
	 */
	@GetMapping("/yulan")
	@RequiresPermissions("gen:yyDangjian:remove")
	@ResponseBody
	public AjaxResult yulan(String id, HttpServletRequest request){
		YyZhaopin yyDangjian = yyZhaopinService.selectByPrimaryKey(id);
		long timeMillis = System.currentTimeMillis();
		System.out.println("开始转换！");
		String sss = "http://" + request.getServerName() + ":" + request.getServerPort();
		String fieldnewpath = yyDangjian.getField2().replace(sss,"");
		String wordFilePath = System.getProperty("user.dir")+File.separator+fieldnewpath;
		String htmlFilePath = System.getProperty("user.dir")+File.separator+"upload/4.html";
		File file = WordToHtml.wordToHtml(wordFilePath, htmlFilePath);
		// 读取html文件
		if (file != null) {
			System.out.println("文件存放路径："+file.getPath());
			System.out.println("转换结束！用时："+ (System.currentTimeMillis()-timeMillis));
		}else{

			System.out.println("文件转换失败！");
		}
		String htmlFilePath2 = sss + "/upload/4.html";
		return success(htmlFilePath2);
	}
	/**
	 * 审核
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "审核", notes = "审核")
	@DeleteMapping("/shenhe")
	@RequiresPermissions("gen:yyDangjian:shenhe")
	@ResponseBody
	public AjaxResult shenhe(String ids){
		int b=yyZhaopinService.updateStatusByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 上架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "上架", notes = "上架")
	@DeleteMapping("/shangjia")
	@RequiresPermissions("gen:yyDangjian:shangjia")
	@ResponseBody
	public AjaxResult shangjia(String ids){
		int b=yyZhaopinService.shangjiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 下架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "下架", notes = "下架")
	@DeleteMapping("/xiajia")
	@RequiresPermissions("gen:yyDangjian:xiajia")
	@ResponseBody
	public AjaxResult xiajia(String ids){
		int b=yyZhaopinService.xiajiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}

	
}
