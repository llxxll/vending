package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.PlanOustatementDetailService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.AutoCode.ExcelUtils;
import com.fc.v2.util.AutoCode.ExportExecUtil;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 委外单结算Controller
 * @ClassName: PlanOustatementDetailController
 * @author fuce
 * @date 2022-03-02 14:13:33
 */
@Api(value = "委外单结算")
@Controller
@RequestMapping("/PlanOustatementDetailController")
public class PlanOustatementDetailController extends BaseController{
	
	private String prefix = "gen/planOustatementDetail";
	
	@Autowired
	private PlanOustatementDetailService planOustatementDetailService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	
	
	/**
	 * 委外单结算页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planOustatementDetail:view")
    public String view(ModelMap model)
    {
		
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			if(user.getPosId().equals("9")) {
				List<TsysUser> userList = new ArrayList<TsysUser>();
				userList.add(user);
				
				model.put("userList", userList);
				
			}else {
				List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
				model.put("userList", userList);
				
			}
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "委外单结算", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planOustatementDetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanOustatementDetail planOustatementDetail) throws ParseException{
		String beginTime = planOustatementDetail.getBeginTime();
		String endTime = planOustatementDetail.getEndTime();
		if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planOustatementDetail.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planOustatementDetail.setEndTime(format);
			 }
		 }
		
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		if(user.getPosId().equals("9")) {
			planOustatementDetail.setOrgName(user.getId()+"");
		}
		PageInfo<PlanOustatementDetail> page=planOustatementDetailService.list(tablepar,planOustatementDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@GetMapping(value = "/exportstatementExcel")
    public void exportstatementExcel(@RequestParam("name") String name,
                                        @RequestParam("beginTime") String beginTime,
                                        @RequestParam("endTime") String endTime,
                                        @RequestParam(value ="ids" ,required = false) String ids,
                                        @RequestParam(value ="materialName" ,required = false) String materialName,
                                		@RequestParam(value ="weiwaiId" ,required = false) Integer weiwaiId,
                                		@RequestParam(value ="searchStatus" ,required = false) Integer searchStatus,
                                		@RequestParam(value ="searchWorker" ,required = false) String searchWorker,
                                        HttpServletRequest request, HttpServletResponse response) throws ParseException {
		PlanOustatementDetail ppm = new PlanOustatementDetail();
    	ppm.setStatus(searchStatus);
    	ppm.setOrgName(searchWorker);//供应商
    	ppm.setBeginTime(beginTime);
    	ppm.setPgId(weiwaiId);
    	if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 ppm.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 ppm.setEndTime(format);
			 }
		 }
    	ppm.setMaterialName(materialName);	
    	Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		if(user.getPosId().equals("9")) {
			ppm.setOrgName(user.getId()+"");
		}
	        try {
	        	List<PlanOustatementDetail> list=null;
	        	if(ids.equals("")) {
	        		list= planOustatementDetailService.excgetlist(ppm);
	        	}else {
	        		Long[] integers = ConvertUtil.toLongArray(",", ids);
	    			List<Long> stringB = Arrays.asList(integers);
	        		list =planOustatementDetailService.getListByIds(stringB);
	        	}
	        	
	            String[] names = {"委外单","物料号", "物料名称", "数量","不合格数量" ,"单价","不含税单价", "不含税总价" ,"供应商" };
	            String title = "委外结算报表";
	            List<String[]> objects = new ArrayList<String[]>();
	            if (null != list) {
	                for (PlanOustatementDetail s : list) {
	                	String[] objs = new String[9];
	                	objs[0] = s.getPgId()+"";
	                    objs[1] = s.getMaterialNo();
	                    objs[2] = s.getMaterialName();
	                    objs[3] = s.getCount()+"";
	                    objs[4] = s.getSpesc();
	                    objs[5] = s.getPrice()+"";
	                    objs[6] = s.getPrice()+"";
	                    objs[7] = s.getAlAccount()+"";
	                    objs[8] = s.getOrggName();
	                	
	                    
	                    objects.add(objs);
	                }
	            }
	            File file = ExcelUtils.exportObjectsWithoutTitle(title, names, title, objects);
	            ExportExecUtil.showExec(file, file.getName(), response);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "委外单结算新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:planOustatementDetail:add")
	@ResponseBody
	public AjaxResult add(PlanOustatementDetail planOustatementDetail){
		int b=planOustatementDetailService.insertSelective(planOustatementDetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 委外单结算删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "委外单结算删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planOustatementDetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planOustatementDetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanOustatementDetail", planOustatementDetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "委外单结算修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planOustatementDetail:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlanOustatementDetail planOustatementDetail)
    {
        return toAjax(planOustatementDetailService.updateByPrimaryKeySelective(planOustatementDetail));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanOustatementDetail planOustatementDetail){
		int i=planOustatementDetailService.updateVisible(planOustatementDetail);
		return toAjax(i);
	}

    
    

	
}
