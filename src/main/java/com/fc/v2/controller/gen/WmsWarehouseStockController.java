package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.WmsWarehouseStock;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.WmsWarehouseStockService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.ApiResultUtil;
import com.fc.v2.util.ExeclUtil;
import com.fc.v2.util.Msg;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主数据-库存Controller
 * @ClassName: WmsWarehouseStockController
 * @author fuce
 * @date 2021-12-19 21:38:35
 */
@Api(value = "主数据-库存")
@Controller
@RequestMapping("/WmsWarehouseStockController")
public class WmsWarehouseStockController extends BaseController{
	
	private String prefix = "gen/wmsWarehouseStock";
	
	@Autowired
	private WmsWarehouseStockService wmsWarehouseStockService;
	@Autowired
	private JshDepotService jshDepotService;
	
	/**
	 * 主数据-库存页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:wmsWarehouseStock:view")
    public String view(ModelMap model)
    {
			List<JshDepot> houseList = jshDepotService.getHouseList();
			model.put("houseList", houseList);
        return prefix + "/list";
    }
	
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view1")
	@RequiresPermissions("gen:wmsWarehouseStock:view")
    public String view1(ModelMap model)
    {
		String roleId = ShiroUtils.getUser().getPosId();
		if(roleId.equals("10")||roleId.equals("3")) {
			 String remark = ShiroUtils.getUser().getRemark();
			 JshDepot selectByPrimaryKey = jshDepotService.selectByPrimaryKey(remark);
			 List<JshDepot> houseList = new ArrayList<JshDepot>();
			 houseList.add(selectByPrimaryKey);
			 model.put("houseList", houseList);
		}else {
			List<JshDepot> houseList = jshDepotService.getHouseList();
			model.put("houseList", houseList);
		}
        return prefix + "/list1";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "主数据-库存", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:wmsWarehouseStock:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,WmsWarehouseStock wmsWarehouseStock){
		
		PageInfo<WmsWarehouseStock> page=wmsWarehouseStockService.list(tablepar,wmsWarehouseStock) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	@ApiOperation(value = "查询下拉表", notes = "查询下拉表")
	@GetMapping("/selectAllList")
	@ResponseBody
	public Msg selectStandardList (Tablepar tablepar, WmsWarehouseStock wmsWarehouseStock)
	{
		PageInfo<WmsWarehouseStock> page=wmsWarehouseStockService.list(tablepar,wmsWarehouseStock) ;
		return ApiResultUtil.success(page.getList(),page.getTotal());
	}
	
	
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
			List<JshDepot> houseList = jshDepotService.getHouseList();
			modelMap.put("houseList", houseList);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "主数据-库存新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:wmsWarehouseStock:add")
	@ResponseBody
	public AjaxResult add(WmsWarehouseStock wmsWarehouseStock){
		Long stockNumber = wmsWarehouseStock.getStockNumber();
		if(stockNumber<0) {
			return error("数量不能为负数");
		}
		int b=wmsWarehouseStockService.insertSelective(wmsWarehouseStock);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 主数据-库存删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "主数据-库存删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:wmsWarehouseStock:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=wmsWarehouseStockService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseStock", wmsWarehouseStockService.selectByPrimaryKey(id));
        List<JshDepot> houseList = jshDepotService.getHouseList();
        map.put("houseList", houseList);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-库存修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:wmsWarehouseStock:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WmsWarehouseStock wmsWarehouseStock)
    {
		Long stockNumber = wmsWarehouseStock.getStockNumber();
		if(stockNumber<0) {
			return error("数量不能为负数");
		}
        return toAjax(wmsWarehouseStockService.updateByPrimaryKeySelective(wmsWarehouseStock));
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/chuku/{id}")
    public String chuku(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseStock", wmsWarehouseStockService.selectByPrimaryKey(id));
        return prefix + "/chuku";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-库存修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:wmsWarehouseStock:chuku")
    @PostMapping("/chukuSave")
    @ResponseBody
    public AjaxResult chukuSave(WmsWarehouseStock wmsWarehouseStock)
    {
		Long stockNumber = wmsWarehouseStock.getStockNumber();
		if(stockNumber<0) {
			return error("数量不能为负数");
		}
		int chukuSave = wmsWarehouseStockService.chukuSave(wmsWarehouseStock);
		if(chukuSave==3) {
			return error("数量超限");
		}
        return toAjax(chukuSave);
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/ruku/{id}")
    public String ruku(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseStock", wmsWarehouseStockService.selectByPrimaryKey(id));
        return prefix + "/ruku";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-库存修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:wmsWarehouseStock:ruku")
    @PostMapping("/rukuSave")
    @ResponseBody
    public AjaxResult rukuSave(WmsWarehouseStock wmsWarehouseStock)
    {
		Long stockNumber = wmsWarehouseStock.getStockNumber();
		if(stockNumber<0) {
			return error("数量不能为负数");
		}
		int rukuSave = wmsWarehouseStockService.rukuSave(wmsWarehouseStock);
        return toAjax(rukuSave);
    }
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody WmsWarehouseStock wmsWarehouseStock){
		int i=wmsWarehouseStockService.updateVisible(wmsWarehouseStock);
		return toAjax(i);
	}

    
    @ApiOperation(value = "文件上传", notes = "文件上传")
    @RequiresPermissions("gen:wmsWarehouseStock:shangchuan")
    @PostMapping("/upload")
    @ResponseBody
    public AjaxResult upload(@RequestParam("file") MultipartFile file) throws Exception
    {
    	InputStream inputStream = file.getInputStream();
    	Map<String, String> fieldd = new HashMap<String, String>(); 
        fieldd.put("名称", "matterNo");
        fieldd.put("规格", "matterDescribe");
        fieldd.put("数量", "stockNumber");
        fieldd.put("位置", "stockLocation");
        fieldd.put("类型", "xs");
        fieldd.put("仓库", "houseName");
    	List<WmsWarehouseStock> execltoList = ExeclUtil.ExecltoList(inputStream, WmsWarehouseStock.class, fieldd, file.getOriginalFilename());
    	int b= wmsWarehouseStockService.upload(execltoList);
    	
		if(b>0){
			return success();
		}else{
			return error();
		}
    }

	
}
