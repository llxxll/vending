package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.PlanWorkOrderDetailPz;
import com.fc.v2.model.auto.PlanWorkOrderPzStatement;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.JshMaterialCarfpriceService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.PlanWorkOrderDetailPzService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.AutoCode.ExcelUtils;
import com.fc.v2.util.AutoCode.ExportExecUtil;
import com.fc.v2.util.AutoCode.PageQueryInfo;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 工单明细Controller
 * @ClassName: PlanWorkOrderDetailPzController
 * @author fuce
 * @date 2021-12-19 21:39:43
 */
@Api(value = "工单明细")
@Controller
@RequestMapping("/PlanWorkOrderDetailPzController")
public class PlanWorkOrderDetailPzController extends BaseController{
	
	private String prefix = "gen/planWorkOrderDetailPz";
	
	@Autowired
	private PlanWorkOrderDetailPzService planWorkOrderDetailPzService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private JshMaterialCarfpriceService jshMaterialCarfpriceService;
	@Autowired
	private JshDepotService jshDepotService;
	
	/**
	 * 工单明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planWorkOrderDetailPz:view")
    public String view(ModelMap model)
    {
		
		TsysUser user = ShiroUtils.getUser();
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
		 model.put("userList", userList);
		
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws Exception 
	 */
	//@Log(title = "工单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planWorkOrderDetailPz:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanWorkOrderDetailPz planWorkOrderDetailPz) throws Exception{
		
		//获取当前用户角色
		TsysUser user = ShiroUtils.getUser();
		String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("1")||posId.equals("12")||posId.equals("4")||posId.equals("5")) {
			planWorkOrderDetailPz.setManager(user.getDepId());
		}
		if(posId.equals("5")) {
			planWorkOrderDetailPz.setWorker(user.getId().intValue());
		}
		if(posId.equals("1")) {
			planWorkOrderDetailPz.setStatus(2);
			if(user.getId()==510) {
				planWorkOrderDetailPz.setManager(null);
			}
		}
		 String beginTime = planWorkOrderDetailPz.getBeginTime();
		 String endTime = planWorkOrderDetailPz.getEndTime();
		 if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planWorkOrderDetailPz.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planWorkOrderDetailPz.setEndTime(format);
			 }
		 }
		PageInfo<PlanWorkOrderDetailPz> page=planWorkOrderDetailPzService.list(tablepar,planWorkOrderDetailPz) ; 
		
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listApp")
	@RequiresPermissions("gen:planWorkOrderDetailPz:list")
	@ResponseBody
	public AjaxResult listApp(@RequestParam(value = "currentPage", required = false) Integer currentPage,
			@RequestParam(value ="workerOrderNo" ,required = false) String workerOrderNo,
			@RequestParam(value ="tuhao" ,required = false) String tuhao,
			@RequestParam(value ="status" ,required = false) Integer status
			
			
			){
		Tablepar tablepar = new Tablepar();
		if(currentPage==null) {
			currentPage=1;
		}
		tablepar.setPage(currentPage);
		tablepar.setLimit(10);
		PlanWorkOrderDetailPz planWorkOrderDetailPz = new PlanWorkOrderDetailPz();
		planWorkOrderDetailPz.setWorkerOrderNo(workerOrderNo);
		planWorkOrderDetailPz.setTuhao(tuhao);
		planWorkOrderDetailPz.setStatus(status);
		//获取当前用户角色
		TsysUser user = ShiroUtils.getUser();
		String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("1")||posId.equals("12")||posId.equals("4")||posId.equals("5")) {
			planWorkOrderDetailPz.setManager(user.getDepId());
		}
		if(posId.equals("1")) {
			planWorkOrderDetailPz.setStatus(2);
			if(user.getId()==510) {
				planWorkOrderDetailPz.setManager(null);
			}
		}
		if(posId.equals("5")) {
			planWorkOrderDetailPz.setWorker(user.getId().intValue());
		}
		
		PageInfo<PlanWorkOrderDetailPz> page=planWorkOrderDetailPzService.list(tablepar,planWorkOrderDetailPz) ; 
		
		PageQueryInfo queryInfo = new PageQueryInfo();
		queryInfo.setRows(page.getList());
        queryInfo.setTotal(page.getTotal());
		Map<String, Object> objectMap = new HashMap<String, Object>();
		 objectMap.put("page", queryInfo);
	        
	        
	        return retobject(200, objectMap);
	}
	
	
	
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/getlist")
	@ResponseBody
	public ResultTable getlist(Tablepar tablepar,PlanWorkOrderDetailPz planWorkOrderDetailPz){
		
		PageInfo<PlanWorkOrderDetailPz> page=planWorkOrderDetailPzService.getlist(tablepar,planWorkOrderDetailPz) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "工单明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:planWorkOrderDetailPz:add")
	@ResponseBody
	public AjaxResult add(PlanWorkOrderDetailPz planWorkOrderDetailPz){
		int b=planWorkOrderDetailPzService.insertSelective(planWorkOrderDetailPz);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 工单明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "工单明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planWorkOrderDetailPz:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planWorkOrderDetailPzService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 工单明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "工单明细删除", action = "111")
	@ApiOperation(value = "撤销", notes = "撤销")
	@DeleteMapping("/chexiao")
	@RequiresPermissions("gen:planWorkOrderDetailPz:chexiao")
	@ResponseBody
	public AjaxResult chexiao(Long ids){
		int b=planWorkOrderDetailPzService.chexiao(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	
	
	@ApiOperation(value = "结算", notes = "结算")
	@DeleteMapping("/jiesuan")
	@ResponseBody
	public AjaxResult jiesuan(String ids){
		int b=planWorkOrderDetailPzService.jiesuan(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzService.selectByPrimaryKey(id);
		
        map.put("PlanWorkOrderDetailPz",selectByPrimaryKey );
        TsysUser user = ShiroUtils.getUser();
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
		 
		map.put("userList", userList);
        return prefix + "/edit";
    }
	/**
     * 修改保存
     */
    //@Log(title = "工单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planWorkOrderDetailPz:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlanWorkOrderDetailPz planWorkOrderDetailPz)
    {
        return toAjax(planWorkOrderDetailPzService.updateByPrimaryKeySelective(planWorkOrderDetailPz));
    }
	/**
	 * 质检跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "质检跳转", notes = "质检跳转")
	@RequiresPermissions("gen:planWorkOrderDetailPz:zhijian")
	@GetMapping("/zhijian/{id}")
    public String zhijian(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanWorkOrderDetailPz", planWorkOrderDetailPzService.selectByPrimaryKey(id));

        return prefix + "/zhijian";
    }
	
	/**
     * 质检保存
     */
    //@Log(title = "工单明细修改", action = "111")
	@ApiOperation(value = "保存", notes = "质检保存")
    @PostMapping("/savezhijian")
    @ResponseBody
    public AjaxResult savezhijian(PlanWorkOrderDetailPz planWorkOrderDetailPz)
    {
		TsysUser user = ShiroUtils.getUser();
		planWorkOrderDetailPz.setStatus(3);
		planWorkOrderDetailPz.setUpdateTime(new Date());
		planWorkOrderDetailPz.setField2(user.getId()+"");
		int x =planWorkOrderDetailPzService.savezhijian(planWorkOrderDetailPz);
        return toAjax(x);
    }
	
	@ApiOperation(value = "保存", notes = "质检保存")
    @PostMapping("/savezhijianAPP")
    @ResponseBody
    public AjaxResult savezhijianAPP(PlanWorkOrderDetailPz planWorkOrderDetailPz)
    {
		TsysUser user = ShiroUtils.getUser();
		planWorkOrderDetailPz.setStatus(3);
		planWorkOrderDetailPz.setUpdateTime(new Date());
		planWorkOrderDetailPz.setField2(user.getId()+"");
		int x =planWorkOrderDetailPzService.savezhijian(planWorkOrderDetailPz);
		if(x>0) {
			
			return AjaxResult.successData(200, "");
		}else {
			return error("操作失败");
		}
    }
	
	
	/**
	 * 分单跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "分单跳转", notes = "分单跳转")
	@RequiresPermissions("gen:planWorkOrderDetailPz:fendan")
	@GetMapping("/fendan/{id}")
    public String fendan(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanWorkOrderDetailPz", planWorkOrderDetailPzService.selectByPrimaryKey(id));
        TsysUser user = ShiroUtils.getUser();
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
		 
		map.put("userList", userList);
        return prefix + "/fendan";
    }
	
	/**
     * 分单保存
     */
    //@Log(title = "工单明细修改", action = "111")
	@ApiOperation(value = "分单保存", notes = "分单保存")
    @PostMapping("/savefendan")
    @ResponseBody
    public AjaxResult savefendan(PlanWorkOrderDetailPz planWorkOrderDetailPz)
    {
		
		//原单据 数量 变为减后数量
				Integer fenchuCount = planWorkOrderDetailPz.getFenchuCount();
				Integer count = planWorkOrderDetailPz.getCount();
				Integer xiancount =count-fenchuCount;
				if(xiancount<0) {
					return error("分单数量超限");
				}else {
		int x =planWorkOrderDetailPzService.savefendan(planWorkOrderDetailPz);
        return toAjax(x);
				}
    }
	/**
	 * 转序跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "转序跳转", notes = "转序跳转")
	@RequiresPermissions("gen:planWorkOrderDetailPz:zhuanxu")
	@GetMapping("/zhuanxu/{id}")
    public String zhuanxu(@PathVariable("id") String id, ModelMap map)
    {
		
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzService.selectByPrimaryKey(id);
		Integer unqualifiedCount = selectByPrimaryKey.getUnqualifiedCount();
		Integer count = selectByPrimaryKey.getCount();
		Integer xcount =count-unqualifiedCount;
		selectByPrimaryKey.setXianCount(xcount);
        map.put("PlanWorkOrderDetailPz", selectByPrimaryKey);
        List<SysDepartment> selectSysDepartmentListByParams = planProduceMaterialDetailService.selectSysDepartmentListByParams();
        map.put("departmentList", selectSysDepartmentListByParams);
        return prefix + "/zhuanxu";
    }
	
	/**
     * 转序保存
     */
    //@Log(title = "工单明细修改", action = "111")
	@ApiOperation(value = "转序保存", notes = "转序保存")
    @PostMapping("/savezhuanxu")
    @ResponseBody
    public AjaxResult savezhuanxu(PlanWorkOrderDetailPz planWorkOrderDetailPz)
    {
		planWorkOrderDetailPz.setField3(planWorkOrderDetailPz.getXianManger()+"");
		planWorkOrderDetailPz.setStatus(7);
		planWorkOrderDetailPz.setFinshTime(new Date());
		int x =planWorkOrderDetailPzService.savezhuanxu(planWorkOrderDetailPz);
        return toAjax(x);
    }
	
	
	/**
	 * 派工跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "派工跳转", notes = "派工跳转")
	@RequiresPermissions("gen:planWorkOrderDetailPz:paigong")
	@GetMapping("/paigong/{id}")
    public String paigong(@PathVariable("id") String id, ModelMap map)
    {
PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzService.selectByPrimaryKey(id);
		
        map.put("PlanWorkOrderDetailPz",selectByPrimaryKey );
        TsysUser user = ShiroUtils.getUser();
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
		 
		map.put("userList", userList);

        return prefix + "/paigong";
    }
	
	/**
     * 派工保存
     */
    //@Log(title = "工单明细修改", action = "111")
	@ApiOperation(value = "派工保存", notes = "派工保存")
    @PostMapping("/savepaigong")
    @ResponseBody
    public AjaxResult savepaigong(PlanWorkOrderDetailPz planWorkOrderDetailPz)
    {
		
		int x =planWorkOrderDetailPzService.savepaigong(planWorkOrderDetailPz);
        return toAjax(x);
    }
	
	/**
	 * 入库跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "入库跳转", notes = "入库跳转")
	@RequiresPermissions("gen:planWorkOrderDetailPz:ruku")
	@GetMapping("/ruku/{id}")
    public String ruku(@PathVariable("id") String id, ModelMap map)
    {
		PlanWorkOrderDetailPz selectByPrimaryKey = planWorkOrderDetailPzService.selectByPrimaryKey(id);
		
		Integer carftOrder = selectByPrimaryKey.getCarftOrder();
		Integer unqualifiedCount = selectByPrimaryKey.getUnqualifiedCount();
		Integer count = selectByPrimaryKey.getCount();
		Integer countx = count-carftOrder-unqualifiedCount;
		if(countx<0) {
			countx=0;
		}
		selectByPrimaryKey.setRukuCount(countx+"");
		//获取所有仓库
		List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("houseList", houseList);
		
        map.put("PlanWorkOrderDetailPz", selectByPrimaryKey);

        return prefix + "/ruku";
    }
	
	/**
     * 入库保存
     */
    //@Log(title = "工单明细修改", action = "111")
	@ApiOperation(value = "入库保存", notes = "入库保存")
    @PostMapping("/saveruku")
    @ResponseBody
    public AjaxResult saveruku(PlanWorkOrderDetailPz planWorkOrderDetailPz) throws Exception
    {
		
		int x =planWorkOrderDetailPzService.saveruku(planWorkOrderDetailPz);
        return toAjax(x);
    }
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanWorkOrderDetailPz planWorkOrderDetailPz){
		int i=planWorkOrderDetailPzService.updateVisible(planWorkOrderDetailPz);
		return toAjax(i);
	}

    
    @GetMapping(value = "/exportstatementExcel")
    public void exportstatementExcel(@RequestParam("name") String name,
                                        @RequestParam("beginTime") String beginTime,
                                        @RequestParam("endTime") String endTime,
                                        @RequestParam(value ="materialName" ,required = false) String materialName,
                                		@RequestParam(value ="searchWorker" ,required = false) Integer searchWorker,
                                		@RequestParam(value ="unqualifiedCount" ,required = false) Integer unqualifiedCount,
                                        HttpServletRequest request, HttpServletResponse response) throws ParseException {
    	PlanWorkOrderDetailPz planWorkOrderDetailPz = new PlanWorkOrderDetailPz();
    	planWorkOrderDetailPz.setWorker(searchWorker);//供应商
    	planWorkOrderDetailPz.setTuhao(name);
    	planWorkOrderDetailPz.setTuName(materialName);
    	planWorkOrderDetailPz.setUnqualifiedCount(unqualifiedCount);
    	if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planWorkOrderDetailPz.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planWorkOrderDetailPz.setEndTime(format);
			 }
		 }
	        try {
	        	List<PlanWorkOrderDetailPz> list= planWorkOrderDetailPzService.excgetlist(planWorkOrderDetailPz);
	        	
	        	
	        	
	        	String[] names = {"工单编号", "物料号",  "物料描述", "工艺描述","工艺名称", 
	            		"工艺规格", "车间名称",
	            		"工人","不合格", "数量", "完工时间"};
	            String title = "工资结算报表";
	            List<String[]> objects = new ArrayList<String[]>();
	            if (null != list) {
	                for (PlanWorkOrderDetailPz s : list) {
	                	String[] objs = new String[11];
	                    objs[0] = s.getWorkerOrderNo();
	                    objs[1] = s.getTuhao();
	                    objs[2] = s.getTuName();
	                    objs[3] = s.getCarftttxx();
	                    objs[4] = s.getCarftname();
	                    objs[5] = s.getCarftspecs();
	                    objs[6] = s.getChejianname();
	                    objs[7] = s.getWorkername();
	                    objs[8] = s.getUnqualifiedCount()+"";
	                    objs[9] = s.getCount().toString();
	                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
	        			
	        				Calendar ca = Calendar.getInstance();
	        				String format = null;
	        				Date distributionTime = s.getFinshTime();
	        				if(distributionTime!=null) {
	        					
	        					ca.setTime(s.getFinshTime());
	        					format = sf.format(ca.getTime());
	        				}
	        				objs[10] = format;
	        				//ca.add(Calendar.HOUR, 8);
	                    
	                    
	                    objects.add(objs);
	                }
	            }
	            File file = ExcelUtils.exportObjectsWithoutTitle(title, names, title, objects);
	            ExportExecUtil.showExec(file, file.getName(), response);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	
}
