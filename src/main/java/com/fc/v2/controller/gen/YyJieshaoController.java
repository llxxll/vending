package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyJieshao;
import com.fc.v2.service.YyJieshaoService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 医院介绍Controller
 * @ClassName: YyJieshaoController
 * @author lxl
 * @date 2023-05-22 19:46:03
 */
@Api(value = "医院介绍")
@Controller
@RequestMapping("/YyJieshaoController")
public class YyJieshaoController extends BaseController{
	
	private String prefix = "gen/yyJieshao";
	
	@Autowired
	private YyJieshaoService yyJieshaoService;
	
	
	/**
	 * 医院介绍页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyJieshao:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "医院介绍", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyJieshao:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyJieshao yyJieshao){
		PageInfo<YyJieshao> page=yyJieshaoService.list(tablepar,yyJieshao) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "医院介绍新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyJieshao:add")
	@ResponseBody
	public AjaxResult add(YyJieshao yyJieshao){
		TsysUser user = ShiroUtils.getUser();
		yyJieshao.setCreateTime(new Date());
		yyJieshao.setDeleteFlag(0);
		yyJieshao.setCreator(user.getId());
		yyJieshao.setField3(user.getNickname());
		yyJieshao.setUpdateTime(new Date());
		yyJieshao.setUpdater(user.getId());
		int b=yyJieshaoService.insertSelective(yyJieshao);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 医院介绍删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "医院介绍删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyJieshao:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyJieshaoService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyJieshao", yyJieshaoService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "医院介绍修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyJieshao:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyJieshao yyJieshao)
    {
        return toAjax(yyJieshaoService.updateByPrimaryKeySelective(yyJieshao));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyJieshao yyJieshao){
		int i=yyJieshaoService.updateVisible(yyJieshao);
		return toAjax(i);
	}

    
    

	
}
