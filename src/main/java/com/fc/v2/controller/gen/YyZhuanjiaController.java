package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyZhuanjia;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyKeshiService;
import com.fc.v2.service.YyZhuanjiaService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 医院专家Controller
 * @ClassName: YyZhuanjiaController
 * @author fuce
 * @date 2023-02-04 14:29:38
 */
@Api(value = "医院专家")
@Controller
@RequestMapping("/YyZhuanjiaController")
public class YyZhuanjiaController extends BaseController{
	
	private String prefix = "gen/yyZhuanjia";
	
	@Autowired
	private YyZhuanjiaService yyZhuanjiaService;
	@Autowired
	private YyKeshiService yyKeshiService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	
	/**
	 * 医院专家页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyZhuanjia:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
		model.put("keshiList", yyKeshiService.selectsslist());
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "医院专家", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyZhuanjia:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyZhuanjia yyZhuanjia){
		PageInfo<YyZhuanjia> page=yyZhuanjiaService.list(tablepar,yyZhuanjia) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		modelMap.put("keshiList", yyKeshiService.selectsslist());
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "医院专家新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyZhuanjia:add")
	@ResponseBody
	public AjaxResult add(YyZhuanjia yyZhuanjia){
		TsysUser user = ShiroUtils.getUser();
		yyZhuanjia.setCreateTime(new Date());
		yyZhuanjia.setDeleteFlag(0);
		yyZhuanjia.setCreator(user.getId());
		yyZhuanjia.setUpdateTime(new Date());
		yyZhuanjia.setUpdater(user.getId());
		yyZhuanjia.setField3(user.getNickname());
		int b=yyZhuanjiaService.insertSelective(yyZhuanjia);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 医院专家删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "医院专家删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyZhuanjia:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyZhuanjiaService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
		
		map.put("keshiList", yyKeshiService.selectsslist());
        map.put("YyZhuanjia", yyZhuanjiaService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "医院专家修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyZhuanjia:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyZhuanjia yyZhuanjia)
    {
        return toAjax(yyZhuanjiaService.updateByPrimaryKeySelective(yyZhuanjia));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyZhuanjia yyZhuanjia){
		int i=yyZhuanjiaService.updateVisible(yyZhuanjia);
		return toAjax(i);
	}

	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request){
		// 文件上传路径，相对路径
		String filePath = System.getProperty("user.dir")+File.separator+"upload";
		System.out.println("文件上传路径，相对路径"+filePath);
		// 获取文件名
		String fileName = file.getOriginalFilename();

		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));

		// 解决中文问题，liunx下中文路径，图片显示问题
		fileName = UUID.randomUUID() + suffixName;

		File fileDir  = new File(filePath);
		// 检测是否存在目录
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		// 构建真实的文件路径
		File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
		try {
			file.transferTo(dest);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String title = fileName;
		fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
		System.out.println(fileName+"文件上传成功");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key","true");
		jsonObject.put("name",fileName);
		jsonObject.put("path",fileName);
		jsonObject.put("title",title);
		return retobject(200,jsonObject);
	}
    

	
}
