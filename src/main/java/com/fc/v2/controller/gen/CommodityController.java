package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.controller.xinlingshou.ApiXlsController;
import com.fc.v2.controller.xinlingshou.DeviceFloorGood;
import com.fc.v2.mapper.auto.CompanyMapper;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.CommodityService;
import com.fc.v2.service.DeviceService;
import com.fc.v2.util.ResultUtils;
import com.fc.v2.util.SnowflakeIdWorker;
import com.fc.v2.util.ToolUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.fc.v2.util.HttpClientUtil.doUpload;

/**
 * 商品管理表Controller
 * @ClassName: CommodityController
 * @author lxl
 * @date 2023-12-01 16:09:32
 */
@Api(value = "商品管理表")
@Controller
@RequestMapping("/CommodityController")
public class CommodityController extends BaseController{
	
	private String prefix = "gen/commodity";
	
	@Autowired
	private CommodityService commodityService;
	@Resource
	private CompanyMapper companyMapper;
	@Autowired
	private DeviceService deviceService;
	@Autowired
	private ApiXlsController apiXlsController;

	/**
	 * 商品管理表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:commodity:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "商品管理表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:commodity:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,Commodity commodity){
		PageInfo<Commodity> page=commodityService.list2(tablepar,commodity) ;
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<Company> clist= companyMapper.selectByExample(new CompanyExample());
		modelMap.put("CompanyList", clist);
        return prefix + "/add";
    }


    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "商品管理表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:commodity:add")
	@ResponseBody
	public AjaxResult add(Commodity commodity) throws Exception {
		Random random = new Random();
		int randomNumber = random.nextInt(100000000);
		commodity.setQualityGuarantee(String.valueOf(randomNumber));
		commodity.setCityCode("1");
		if(commodity.getCommodityType().equals("2")){
			commodity.setIngredients("5");
			commodity.setBarCode("");
		}
		int b=commodityService.insertSelective(commodity);
		if(b>0){

			ApiResult apiResult = apiXlsController.submitGood(commodity.getQualityGuarantee(),commodity.getCommodityName(),
					commodity.getRetailPrice(),Integer.valueOf(commodity.getCommodityType()),commodity.getCompanyDesc(),
					Integer.valueOf(commodity.getIngredients()), Integer.valueOf(commodity.getClassification()), commodity.getBarCode(),
					commodity.getStr1(),commodity.getAreaCode(),commodity.getAreaCode(),commodity.getAreaCode(),commodity.getAreaCode(),1);
			if(apiResult.getStatus() == 200){
				Map data = (Map) apiResult.getData();
				if(((String)data.get("resultCode")).equals("666")){
					return success();
				}else{
					return error((String) data.get("resultMsg"));
				}
			}else{
				return error(apiResult.getMessage());
			}

		}else{
			return error();
		}
	}
	
	/**
	 * 商品管理表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "商品管理表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:commodity:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=commodityService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Commodity", commodityService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "商品管理表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:commodity:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Commodity commodity)
    {
        return toAjax(commodityService.updateByPrimaryKeySelective(commodity));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody Commodity commodity){
		int i=commodityService.updateVisible(commodity);
		return toAjax(i);
	}


	/**
	 * 商品管理表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view2")
	@RequiresPermissions("gen:commodity:view")
	public String view2(ModelMap model)
	{
		return prefix + "/list2";
	}

	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "商品管理表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list2")
	@RequiresPermissions("gen:commodity:list")
	@ResponseBody
	public ResultTable list2(Tablepar tablepar,Commodity commodity) throws Exception {
		PageInfo<Commodity> page=commodityService.list(tablepar,commodity) ;
		return pageTable(page.getList(),page.getTotal());
	}
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "上架", notes = "上架")
	@GetMapping("/shangjia/{id}")
	public String shangjia(@PathVariable("id") String id, ModelMap map)
	{
		map.put("Commodity", commodityService.selectByPrimaryKey(id));

		List<Device> deviceList= deviceService.getAll();
		map.put("deviceList", deviceList);
		return prefix + "/shangjia";
	}
	@ApiOperation(value = "上架保存", notes = "上架保存")
	@RequiresPermissions("gen:commodity:edit")
	@PostMapping("/shangjia")
	@ResponseBody
	public AjaxResult shangjiasave(Commodity commodity) throws Exception {
		ApiResult apiResult = apiXlsController.onlineDeviceFloorGood(commodity.getDeviceCode(), Math.toIntExact(commodity.getNum1()),commodity.getRow(),commodity.getCol());
		if(apiResult.getStatus() == 200){
			Map data = (Map) apiResult.getData();
			if(((String)data.get("resultCode")).equals("666")){
				return toAjax(1);
			}else{
				return error((String) data.get("resultMsg"));
			}
		}else{
			return error(apiResult.getMessage());
		}

	}
	@GetMapping("/getshebei")
	@ResponseBody
	public ApiResult getshebei( String code) throws Exception {
		ApiResult apiResult = apiXlsController.getDeviceFloorList(code);
		return apiResult;
	}
	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request) throws Exception {
		String filePath = System.getProperty("user.dir")+File.separator+"upload";
		System.out.println("文件上传路径，相对路径"+filePath);
		String fileName = file.getOriginalFilename();
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
		fileName = UUID.randomUUID() + suffixName;
		File fileDir  = new File(filePath);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
		try {
			file.transferTo(dest);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("path",fileName);

		String urll ="https://v2.lufff.com/hiApi/";
		String url =urll+"upload_photo";
		Map<String, Object> params = new HashMap<>();
		params.put("fileName",file.getOriginalFilename());
		params.put("isRemoveBG",true);
		Map<String, Object> newParams = apiXlsController.getParams(params);
		newParams.put("goodsImage",dest);
		String s = doUpload(url, newParams);
		Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
		if(parseObject.get("resultCode").equals("666")){
			String imgurl = (String) parseObject.get("resultMsg");
			jsonObject.put("imgurl",imgurl);
		}else{
			jsonObject.put("imgurl",fileName);
		}
		Map<String, Object> params2 = new HashMap<>();
		params2.put("fileName","2" + file.getOriginalFilename());
		params2.put("isRemoveBG",false);
		Map<String, Object> newParams2 = apiXlsController.getParams(params2);
		newParams2.put("goodsImage",dest);
		String s2 = doUpload(url, newParams2);
		Map<String, Object> parseObject2 = JSON.parseObject(s2,Map.class);
		if(parseObject2.get("resultCode").equals("666")){
			String imgurl = (String) parseObject2.get("resultMsg");
			jsonObject.put("imgurl2",imgurl);
		}else{
			jsonObject.put("imgurl2",fileName);
		}
		return retobject(200,jsonObject);
	}

}
