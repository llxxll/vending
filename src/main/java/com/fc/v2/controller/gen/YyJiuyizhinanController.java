package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.YyDangjian;
import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyJiuyizhinan;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyJiuyizhinanService;
import com.fc.v2.service.YyNewsTypeService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.WordToHtml;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * 就医指南Controller
 * @ClassName: YyJiuyizhinanController
 * @author lxl
 * @date 2023-02-13 12:17:41
 */
@Api(value = "就医指南")
@Controller
@RequestMapping("/YyJiuyizhinanController")
public class YyJiuyizhinanController extends BaseController{
	
	private String prefix = "gen/yyJiuyizhinan";
	
	@Autowired
	private YyJiuyizhinanService yyJiuyizhinanService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private YyNewsTypeService yyNewsTypeService;
	
	/**
	 * 就医指南页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyJiuyizhinan:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "就医指南", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyJiuyizhinan:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyJiuyizhinan yyJiuyizhinan){
		PageInfo<YyJiuyizhinan> page=yyJiuyizhinanService.list(tablepar,yyJiuyizhinan) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<YyNewsType> list= yyNewsTypeService.selectByfw("jyzn");
		modelMap.put("typelist", list);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "就医指南新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyJiuyizhinan:add")
	@ResponseBody
	public AjaxResult add(YyJiuyizhinan yyJiuyizhinan){
		TsysUser user = ShiroUtils.getUser();
		yyJiuyizhinan.setCreateTime(new Date());
		yyJiuyizhinan.setDeleteFlag(0);
		yyJiuyizhinan.setCreator(user.getId());
		yyJiuyizhinan.setField3(user.getNickname());
		yyJiuyizhinan.setUpdateTime(new Date());
		yyJiuyizhinan.setUpdater(user.getId());
		int b=yyJiuyizhinanService.insertSelective(yyJiuyizhinan);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 就医指南删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "就医指南删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyJiuyizhinan:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyJiuyizhinanService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyJiuyizhinan", yyJiuyizhinanService.selectByPrimaryKey(id));
		List<YyNewsType> list= yyNewsTypeService.selectByfw("jyzn");
		map.put("typelist", list);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "就医指南修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyJiuyizhinan:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyJiuyizhinan yyJiuyizhinan)
    {
        return toAjax(yyJiuyizhinanService.updateByPrimaryKeySelective(yyJiuyizhinan));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyJiuyizhinan yyJiuyizhinan){
		int i=yyJiuyizhinanService.updateVisible(yyJiuyizhinan);
		return toAjax(i);
	}

	/**
	 * 预览
	 * @param id
	 * @return
	 */
	@GetMapping("/yulan")
	@RequiresPermissions("gen:yyJiuyizhinan:remove")
	@ResponseBody
	public AjaxResult yulan(String id, HttpServletRequest request){
		YyJiuyizhinan yyJiuyizhinan = yyJiuyizhinanService.selectByPrimaryKey(id);
		long timeMillis = System.currentTimeMillis();
		System.out.println("开始转换！");
		String sss = "http://" + request.getServerName() + ":" + request.getServerPort();
		String fieldnewpath = yyJiuyizhinan.getField2().replace(sss,"");
		String wordFilePath = System.getProperty("user.dir")+File.separator+fieldnewpath;
		String htmlFilePath = System.getProperty("user.dir")+File.separator+"upload/5.html";
		File file = WordToHtml.wordToHtml(wordFilePath, htmlFilePath);
		// 读取html文件
		if (file != null) {
			System.out.println("文件存放路径："+file.getPath());
			System.out.println("转换结束！用时："+ (System.currentTimeMillis()-timeMillis));
		}else{

			System.out.println("文件转换失败！");
		}
		String htmlFilePath2 = sss + "/upload/5.html";
		return success(htmlFilePath2);
	}
	/**
	 * 审核
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "审核", notes = "审核")
	@DeleteMapping("/shenhe")
	@RequiresPermissions("gen:yyJiuyizhinan:shenhe")
	@ResponseBody
	public AjaxResult shenhe(String ids){
		int b=yyJiuyizhinanService.updateStatusByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 上架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "上架", notes = "上架")
	@DeleteMapping("/shangjia")
	@RequiresPermissions("gen:yyJiuyizhinan:shangjia")
	@ResponseBody
	public AjaxResult shangjia(String ids){
		int b=yyJiuyizhinanService.shangjiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 下架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "下架", notes = "下架")
	@DeleteMapping("/xiajia")
	@RequiresPermissions("gen:yyJiuyizhinan:xiajia")
	@ResponseBody
	public AjaxResult xiajia(String ids){
		int b=yyJiuyizhinanService.xiajiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
    

	
}
