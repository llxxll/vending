package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.YyDangjian;
import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyJiankangzhishi;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyJiankangzhishiService;
import com.fc.v2.service.YyNewsTypeService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.WordToHtml;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * 健康知识Controller
 * @ClassName: YyJiankangzhishiController
 * @author lxl
 * @date 2023-02-13 12:16:40
 */
@Api(value = "健康知识")
@Controller
@RequestMapping("/YyJiankangzhishiController")
public class YyJiankangzhishiController extends BaseController{
	
	private String prefix = "gen/yyJiankangzhishi";
	
	@Autowired
	private YyJiankangzhishiService yyJiankangzhishiService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private YyNewsTypeService yyNewsTypeService;
	/**
	 * 健康知识页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyJiankangzhishi:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "健康知识", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyJiankangzhishi:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyJiankangzhishi yyJiankangzhishi){
		PageInfo<YyJiankangzhishi> page=yyJiankangzhishiService.list(tablepar,yyJiankangzhishi) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<YyNewsType> list= yyNewsTypeService.selectByfw("jkzs");
		modelMap.put("typelist", list);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "健康知识新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyJiankangzhishi:add")
	@ResponseBody
	public AjaxResult add(YyJiankangzhishi yyJiankangzhishi){
		TsysUser user = ShiroUtils.getUser();
		yyJiankangzhishi.setCreateTime(new Date());
		yyJiankangzhishi.setDeleteFlag(0);
		yyJiankangzhishi.setCreator(user.getId());
		yyJiankangzhishi.setField3(user.getNickname());
		yyJiankangzhishi.setUpdateTime(new Date());
		yyJiankangzhishi.setUpdater(user.getId());
		int b=yyJiankangzhishiService.insertSelective(yyJiankangzhishi);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 健康知识删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "健康知识删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyJiankangzhishi:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyJiankangzhishiService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyJiankangzhishi", yyJiankangzhishiService.selectByPrimaryKey(id));
		List<YyNewsType> list= yyNewsTypeService.selectByfw("jkzs");
		map.put("typelist", list);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "健康知识修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyJiankangzhishi:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyJiankangzhishi yyJiankangzhishi)
    {
        return toAjax(yyJiankangzhishiService.updateByPrimaryKeySelective(yyJiankangzhishi));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyJiankangzhishi yyJiankangzhishi){
		int i=yyJiankangzhishiService.updateVisible(yyJiankangzhishi);
		return toAjax(i);
	}


	/**
	 * 预览
	 * @param id
	 * @return
	 */
	@GetMapping("/yulan")
	@RequiresPermissions("gen:yyJiankangzhishi:remove")
	@ResponseBody
	public AjaxResult yulan(String id, HttpServletRequest request){
		YyJiankangzhishi yyJiankangzhishi = yyJiankangzhishiService.selectByPrimaryKey(id);
		long timeMillis = System.currentTimeMillis();
		System.out.println("开始转换！");
		String sss = "http://" + request.getServerName() + ":" + request.getServerPort();
		String fieldnewpath = yyJiankangzhishi.getField2().replace(sss,"");
		String wordFilePath = System.getProperty("user.dir")+File.separator+fieldnewpath;
		String htmlFilePath = System.getProperty("user.dir")+File.separator+"upload/7.html";
		File file = WordToHtml.wordToHtml(wordFilePath, htmlFilePath);
		// 读取html文件
		if (file != null) {
			System.out.println("文件存放路径："+file.getPath());
			System.out.println("转换结束！用时："+ (System.currentTimeMillis()-timeMillis));
		}else{

			System.out.println("文件转换失败！");
		}
		String htmlFilePath2 = sss + "/upload/7.html";
		return success(htmlFilePath2);
	}
	/**
	 * 审核
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "审核", notes = "审核")
	@DeleteMapping("/shenhe")
	@RequiresPermissions("gen:yyJiankangzhishi:shenhe")
	@ResponseBody
	public AjaxResult shenhe(String ids){
		int b=yyJiankangzhishiService.updateStatusByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 上架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "上架", notes = "上架")
	@DeleteMapping("/shangjia")
	@RequiresPermissions("gen:yyJiankangzhishi:shangjia")
	@ResponseBody
	public AjaxResult shangjia(String ids){
		int b=yyJiankangzhishiService.shangjiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 下架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "下架", notes = "下架")
	@DeleteMapping("/xiajia")
	@RequiresPermissions("gen:yyJiankangzhishi:xiajia")
	@ResponseBody
	public AjaxResult xiajia(String ids){
		int b=yyJiankangzhishiService.xiajiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}

	
}
