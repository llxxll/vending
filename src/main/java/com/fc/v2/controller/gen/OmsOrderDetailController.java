package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.auto.MdProducspecs;
import com.fc.v2.model.auto.OmsOrderDetail;
import com.fc.v2.model.auto.OrderFromCategory;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.MdProduccategoryService;
import com.fc.v2.service.MdProducspecsService;
import com.fc.v2.service.OmsOrderDetailService;
import com.fc.v2.service.OrderFromCategoryService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.AutoCode.ExcelUtils;
import com.fc.v2.util.AutoCode.ExportExecUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 销售-订单明细Controller
 * @ClassName: OmsOrderDetailController
 * @author fuce
 * @date 2022-05-21 23:28:42
 */
@Api(value = "销售-订单明细")
@Controller
@RequestMapping("/OmsOrderDetailController")
public class OmsOrderDetailController extends BaseController{
	
	private String prefix = "gen/omsOrderDetail";
	
	@Autowired
	private OmsOrderDetailService omsOrderDetailService;
	@Autowired
	private OrderFromCategoryService orderFromCategoryService;
	@Autowired
	private MdProduccategoryService mdProduccategoryService;
	@Autowired
	private MdProducspecsService mdProducspecsService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	/**
	 * 销售-订单明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:omsOrderDetail:view")
    public String view(ModelMap model)
    {
		
		List<OrderFromCategory> lyList = orderFromCategoryService.getorderFromCategoryList();
		TsysUser user = ShiroUtils.getUser();
		String roleId = user.getPosId();
		List<TsysUser> selectListByDept =null;
		if(roleId.equals("102")||roleId.equals("109")) {
			selectListByDept = new ArrayList<TsysUser>();
			selectListByDept.add(user);
		}else {
			selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
			
		}
		List<MdProduccategory> list = mdProduccategoryService.getList(null);
		//获取所有规格
		List<MdProducspecs> list1 = mdProducspecsService.getList(null);
		model.put("mdProduccategoryList", list);
		model.put("mdProducspecsList", list1);
		model.put("userList", selectListByDept);
		model.put("lyList", lyList);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "销售-订单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:omsOrderDetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OmsOrderDetail omsOrderDetail) throws ParseException{
		
		//获取当前登录人 
				TsysUser user = ShiroUtils.getUser();
				String roleId = user.getPosId();
				
				if(roleId.equals("102")||roleId.equals("109")) {
					omsOrderDetail.setSupplySid(user.getId());
				}
				String beginTime = omsOrderDetail.getBeginTime();
				String endTime = omsOrderDetail.getEndTime();
		 if(beginTime!=null) {
					 
					 if(!beginTime.trim().equals("")) {
						 beginTime=beginTime+" 00:00:00";
						 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						 
						 Date date =sdf.parse(beginTime);
						  
						 Calendar calendar = Calendar.getInstance();
						  
						 calendar.setTime(date);
						 calendar.add(Calendar.HOUR, -8);
						 
						 String format = sdf.format(calendar.getTime());
						 omsOrderDetail.setBeginTime(format);
					 }
				 }
				 
				 if(endTime!=null) {
					 if(!endTime.trim().equals("")) {
						 endTime=endTime+" 23:59:59";
						 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						 
						 Date date =sdf.parse(endTime);
						  
						 Calendar calendar = Calendar.getInstance();
						  
						 calendar.setTime(date);
						 calendar.add(Calendar.HOUR, -8);
						 
						 String format = sdf.format(calendar.getTime());
						 omsOrderDetail.setEndTime(format);
					 }
				 }
		
		
		PageInfo<OmsOrderDetail> page=omsOrderDetailService.list(tablepar,omsOrderDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	//@Log(title = "销售-订单明细", action = "111")
		@ApiOperation(value = "分页跳转", notes = "分页跳转")
		@GetMapping("/listxxx")
		@RequiresPermissions("gen:omsOrderDetail:list")
		@ResponseBody
		public ResultTable listxxx(Tablepar tablepar,OmsOrderDetail omsOrderDetail){
			
			//获取当前登录人 
					TsysUser user = ShiroUtils.getUser();
					String roleId = user.getPosId();
					
					if(roleId.equals("102")||roleId.equals("109")) {
						omsOrderDetail.setSupplySid(user.getId());
					}
					
			
			
			PageInfo<OmsOrderDetail> page=omsOrderDetailService.list(tablepar,omsOrderDetail) ; 
			return pageTable(page.getList(),page.getTotal());
		}
	
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "销售-订单明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:omsOrderDetail:add")
	@ResponseBody
	public AjaxResult add(OmsOrderDetail omsOrderDetail){
		omsOrderDetail.setSupplySid(ShiroUtils.getUserId());
		int b=omsOrderDetailService.insertSelective(omsOrderDetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 销售-订单明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "销售-订单明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:omsOrderDetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=omsOrderDetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("OmsOrderDetail", omsOrderDetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "销售-订单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:omsOrderDetail:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OmsOrderDetail omsOrderDetail)
    {
        return toAjax(omsOrderDetailService.updateByPrimaryKeySelective(omsOrderDetail));
    }
	@ApiOperation(value = "退货", notes = "修改跳转")
	@GetMapping("/tuihuo/{id}")
	public String tuihuo(@PathVariable("id") String id, ModelMap map)
	{
		map.put("OmsOrderDetail", omsOrderDetailService.selectByPrimaryKey(id));
		
		return prefix + "/tuihuo";
	}
	
	/**
	 * 修改保存
	 */
	//@Log(title = "销售-订单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:omsOrderDetail:tuihuo")
	@PostMapping("/tuihuoSave")
	@ResponseBody
	public AjaxResult tuihuoSave(OmsOrderDetail omsOrderDetail)
	{
		
		Long tuihuonumber = omsOrderDetail.getTuihuonumber();
		Long itemNum = omsOrderDetail.getItemNum();
		Long itemNumx =itemNum-tuihuonumber;
		Long yutuihuonumber = omsOrderDetail.getYutuihuonumber();
		if(yutuihuonumber>itemNumx) {
			return error("数量超限");
		}
		
		int tuihuo = omsOrderDetailService.tuihuo(omsOrderDetail);
		if(tuihuo==4) {
			return error("单据已完成");
		}
		return toAjax(tuihuo);
	}
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OmsOrderDetail omsOrderDetail){
		int i=omsOrderDetailService.updateVisible(omsOrderDetail);
		return toAjax(i);
	}

    
    @GetMapping(value = "/exportstatementExcel")
    @RequiresPermissions("gen:omsOrderDetail:daochu")
    public void exportstatementExcel( OmsOrderDetail omsOrderDetail,
                                        HttpServletRequest request, HttpServletResponse response) throws ParseException {
	        try {
	        	List<OmsOrderDetail> list=omsOrderDetailService.excgetlist(omsOrderDetail);
	        	
	            String[] names = {"单号","来源", "客户", "产品","类别" ,"规格","销售价", "数量" ,"创建人" };
	            String title = "销售明细报表";
	            List<String[]> objects = new ArrayList<String[]>();
	            if (null != list) {
	                for (OmsOrderDetail s : list) {
	                	String[] objs = new String[9];
	                	objs[0] = s.getOrderNo();
	                    objs[1] = s.getCategroyName();
	                    objs[2] = s.getBuyName();
	                    objs[3] = s.getProductTitle();
	                    objs[4] = s.getStr1();
	                    objs[5] = s.getStr2();
	                    
	                    objs[6] = s.getXiaoshouPrice()+"";
	                    objs[7] = s.getItemNum()+"";
	                    objs[8] = s.getCreateName();
	                	
	                    
	                    objects.add(objs);
	                }
	            }
	            File file = ExcelUtils.exportObjectsWithoutTitle(title, names, title, objects);
	            ExportExecUtil.showExec(file, file.getName(), response);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	
}
