package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshBuyOrderPz;
import com.fc.v2.service.JshBuyOrderPzService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 工单明细扩展表(派工,质检明细)Controller
 * @ClassName: JshBuyOrderPzController
 * @author fuce
 * @date 2021-12-26 23:13:28
 */
@Api(value = "采购明细")
@Controller
@RequestMapping("/JshBuyOrderPzController")
public class JshBuyOrderPzController extends BaseController{
	
	private String prefix = "gen/jshBuyOrderPz";
	
	@Autowired
	private JshBuyOrderPzService jshBuyOrderPzService;
	
	
	/**
	 * 工单明细扩展表(派工,质检明细)页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:jshBuyOrderPz:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "工单明细扩展表(派工,质检明细)", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:jshBuyOrderPz:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,JshBuyOrderPz jshBuyOrderPz){
		PageInfo<JshBuyOrderPz> page=jshBuyOrderPzService.list(tablepar,jshBuyOrderPz) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "工单明细扩展表(派工,质检明细)新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:jshBuyOrderPz:add")
	@ResponseBody
	public AjaxResult add(JshBuyOrderPz jshBuyOrderPz){
		int b=jshBuyOrderPzService.insertSelective(jshBuyOrderPz);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 工单明细扩展表(派工,质检明细)删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "工单明细扩展表(派工,质检明细)删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:jshBuyOrderPz:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=jshBuyOrderPzService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("JshBuyOrderPz", jshBuyOrderPzService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "工单明细扩展表(派工,质检明细)修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshBuyOrderPz:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JshBuyOrderPz jshBuyOrderPz)
    {
        return toAjax(jshBuyOrderPzService.updateByPrimaryKeySelective(jshBuyOrderPz));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody JshBuyOrderPz jshBuyOrderPz){
		int i=jshBuyOrderPzService.updateVisible(jshBuyOrderPz);
		return toAjax(i);
	}

    
    

	
}
