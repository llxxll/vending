package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.JshBuyOrderService;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.JshMaterialService;
import com.fc.v2.service.JshSupplierService;
import com.fc.v2.util.ApiResultUtil;
import com.fc.v2.util.Msg;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 采购单Controller
 * @ClassName: JshBuyOrderController
 * @author fuce
 * @date 2021-12-26 23:13:09
 */
@Api(value = "采购单")
@Controller
@RequestMapping("/JshBuyOrderController")
public class JshBuyOrderController extends BaseController{
	
	private String prefix = "gen/jshBuyOrder";
	
	@Autowired
	private JshBuyOrderService jshBuyOrderService;
	@Autowired
	private JshSupplierService jshSupplierService;
	@Autowired
	private JshDepotService jshDepotService;
	@Autowired
	private JshMaterialService jshMaterialService;
	
	/**
	 * 采购单页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:jshBuyOrder:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "采购单", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:jshBuyOrder:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,JshBuyOrder jshBuyOrder){
		PageInfo<JshBuyOrder> page=jshBuyOrderService.list(tablepar,jshBuyOrder) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap map)
    {
		List<JshSupplier> list =  jshSupplierService.selectSupplierList();
		//获取所有 仓库
		List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("jshSupplierList",list);
		map.put("houseList", houseList);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "采购单新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:jshBuyOrder:add")
	@ResponseBody
	public AjaxResult add(JshBuyOrder jshBuyOrder){
		int b=jshBuyOrderService.insertSelective(jshBuyOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 采购单删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "采购单删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:jshBuyOrder:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=jshBuyOrderService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("JshBuyOrder", jshBuyOrderService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "采购单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshBuyOrder:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JshBuyOrder jshBuyOrder)
    {
        return toAjax(jshBuyOrderService.updateByPrimaryKeySelective(jshBuyOrder));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody JshBuyOrder jshBuyOrder){
		int i=jshBuyOrderService.updateVisible(jshBuyOrder);
		return toAjax(i);
	}


	/**
	 * 查询物料片区下拉表
	 */
	@ApiOperation(value = "查询区下拉表", notes = "查询下拉表")
	@GetMapping("/selectAllList")
	@RequiresPermissions("gen:jshBuyOrder:add")
	@ResponseBody
	public Msg selectAllList (Tablepar tablepar, JshMaterial jshMaterial)
	{
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ;
		return ApiResultUtil.success(page.getList(),page.getTotal());
	}

	
}
