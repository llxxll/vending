package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.YyDangjian;
import com.fc.v2.model.auto.YyNewsType;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyYiyuangaikuang;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyNewsTypeService;
import com.fc.v2.service.YyYiyuangaikuangService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.WordToHtml;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * 医院概况Controller
 * @ClassName: YyYiyuangaikuangController
 * @author lxl
 * @date 2023-02-13 12:17:30
 */
@Api(value = "医院概况")
@Controller
@RequestMapping("/YyYiyuangaikuangController")
public class YyYiyuangaikuangController extends BaseController{
	
	private String prefix = "gen/yyYiyuangaikuang";
	
	@Autowired
	private YyYiyuangaikuangService yyYiyuangaikuangService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private YyNewsTypeService yyNewsTypeService;
	/**
	 * 医院概况页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyYiyuangaikuang:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "医院概况", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyYiyuangaikuang:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyYiyuangaikuang yyYiyuangaikuang){
		PageInfo<YyYiyuangaikuang> page=yyYiyuangaikuangService.list(tablepar,yyYiyuangaikuang) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<YyNewsType> list= yyNewsTypeService.selectByfw("yygk");
		modelMap.put("typelist", list);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "医院概况新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyYiyuangaikuang:add")
	@ResponseBody
	public AjaxResult add(YyYiyuangaikuang yyYiyuangaikuang){
		TsysUser user = ShiroUtils.getUser();
		yyYiyuangaikuang.setCreateTime(new Date());
		yyYiyuangaikuang.setDeleteFlag(0);
		yyYiyuangaikuang.setCreator(user.getId());
		yyYiyuangaikuang.setField3(user.getNickname());
		yyYiyuangaikuang.setUpdateTime(new Date());
		yyYiyuangaikuang.setUpdater(user.getId());
		int b=yyYiyuangaikuangService.insertSelective(yyYiyuangaikuang);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 医院概况删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "医院概况删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyYiyuangaikuang:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyYiyuangaikuangService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyYiyuangaikuang", yyYiyuangaikuangService.selectByPrimaryKey(id));
		List<YyNewsType> list= yyNewsTypeService.selectByfw("yygk");
		map.put("typelist", list);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "医院概况修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyYiyuangaikuang:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyYiyuangaikuang yyYiyuangaikuang)
    {
        return toAjax(yyYiyuangaikuangService.updateByPrimaryKeySelective(yyYiyuangaikuang));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyYiyuangaikuang yyYiyuangaikuang){
		int i=yyYiyuangaikuangService.updateVisible(yyYiyuangaikuang);
		return toAjax(i);
	}

	/**
	 * 预览
	 * @param id
	 * @return
	 */
	@GetMapping("/yulan")
	@RequiresPermissions("gen:yyYiyuangaikuang:remove")
	@ResponseBody
	public AjaxResult yulan(String id, HttpServletRequest request){
		YyYiyuangaikuang yyYiyuangaikuang = yyYiyuangaikuangService.selectByPrimaryKey(id);
		long timeMillis = System.currentTimeMillis();
		System.out.println("开始转换！");
		String sss = "http://" + request.getServerName() + ":" + request.getServerPort();
		String fieldnewpath = yyYiyuangaikuang.getField2().replace(sss,"");
		String wordFilePath = System.getProperty("user.dir")+File.separator+fieldnewpath;
		String htmlFilePath = System.getProperty("user.dir")+File.separator+"upload/6.html";
		File file = WordToHtml.wordToHtml(wordFilePath, htmlFilePath);
		// 读取html文件
		if (file != null) {
			System.out.println("文件存放路径："+file.getPath());
			System.out.println("转换结束！用时："+ (System.currentTimeMillis()-timeMillis));
		}else{

			System.out.println("文件转换失败！");
		}
		String htmlFilePath2 = sss + "/upload/6.html";
		return success(htmlFilePath2);
	}
	/**
	 * 审核
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "审核", notes = "审核")
	@DeleteMapping("/shenhe")
	@RequiresPermissions("gen:yyYiyuangaikuang:shenhe")
	@ResponseBody
	public AjaxResult shenhe(String ids){
		int b=yyYiyuangaikuangService.updateStatusByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 上架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "上架", notes = "上架")
	@DeleteMapping("/shangjia")
	@RequiresPermissions("gen:yyYiyuangaikuang:shangjia")
	@ResponseBody
	public AjaxResult shangjia(String ids){
		int b=yyYiyuangaikuangService.shangjiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 下架
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "下架", notes = "下架")
	@DeleteMapping("/xiajia")
	@RequiresPermissions("gen:yyYiyuangaikuang:xiajia")
	@ResponseBody
	public AjaxResult xiajia(String ids){
		int b=yyYiyuangaikuangService.xiajiaByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
    

	
}
