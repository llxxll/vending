package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.JshMaterialCarfpriceService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.SysDepartmentService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 产品工艺加个扩展表Controller
 * @ClassName: JshMaterialCarfpriceController
 * @author fuce
 * @date 2021-12-19 21:41:13
 */
@Api(value = "产品工艺加个扩展表")
@Controller
@RequestMapping("/JshMaterialCarfpriceController")
public class JshMaterialCarfpriceController extends BaseController{
	
	private String prefix = "gen/jshMaterialCarfprice";
	
	@Autowired
	private JshMaterialCarfpriceService jshMaterialCarfpriceService;
	@Autowired 
	private SysDepartmentService sysDepartmentService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	
	
	/**
	 * 产品工艺加个扩展表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:jshMaterialCarfprice:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		String posId = user.getPosId();
		if(posId.equals("4")||posId.equals("12")) {
			SysDepartment selectByPrimaryKey = sysDepartmentService.selectByPrimaryKey(user.getDepId()+"");
			List<SysDepartment> selectSysDepartmentListByParams = new ArrayList<SysDepartment>();
			selectSysDepartmentListByParams.add(selectByPrimaryKey);
			model.put("departmentList", selectSysDepartmentListByParams);
		}else {
			List<SysDepartment> selectSysDepartmentListByParams = planProduceMaterialDetailService.selectSysDepartmentListByParams();
			model.put("departmentList", selectSysDepartmentListByParams);
		}
        return prefix + "/list";
    }
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view1")
	@RequiresPermissions("gen:jshMaterialCarfprice:view")
    public String view1(@RequestParam(value = "mid") String mid,ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		String posId = user.getPosId();
		if(posId.equals("4")||posId.equals("12")) {
			SysDepartment selectByPrimaryKey = sysDepartmentService.selectByPrimaryKey(user.getDepId()+"");
			List<SysDepartment> selectSysDepartmentListByParams = new ArrayList<SysDepartment>();
			selectSysDepartmentListByParams.add(selectByPrimaryKey);
			model.put("departmentList", selectSysDepartmentListByParams);
		}else {
			List<SysDepartment> selectSysDepartmentListByParams = planProduceMaterialDetailService.selectSysDepartmentListByParams();
			model.put("departmentList", selectSysDepartmentListByParams);
		}
		model.put("mid", mid);
		
        return prefix + "/list12";
    }
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "产品工艺加个扩展表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:jshMaterialCarfprice:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,JshMaterialCarfprice jshMaterialCarfprice){
		TsysUser user = ShiroUtils.getUser();
			String posId = user.getPosId();
			if(posId.equals("4")||posId.equals("12")) {
				jshMaterialCarfprice.setTenantId(Long.valueOf(user.getDepId()));
			}
		
		
		PageInfo<JshMaterialCarfprice> page=jshMaterialCarfpriceService.list(tablepar,jshMaterialCarfprice) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		TsysUser user = ShiroUtils.getUser();
		Integer depid=null;
		if(user.getPosId().equals("4")||user.getPosId().equals("12")) {
			depid=user.getDepId();
		}
		List<SysDepartment> selectListByParamsbymyself = planProduceMaterialDetailService.selectListByParamsbymyself(depid);
		
		modelMap.put("departmentList", selectListByParamsbymyself);
        return prefix + "/add";
    }
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add1")
    public String add1(@RequestParam(value = "materialId") String materialId,ModelMap modelMap)
    {
		TsysUser user = ShiroUtils.getUser();
		Integer depid=null;
		if(user.getPosId().equals("4")||user.getPosId().equals("12")) {
			depid=user.getDepId();
		}
		List<SysDepartment> selectListByParamsbymyself = planProduceMaterialDetailService.selectListByParamsbymyself(depid);
		
		modelMap.put("materialId", materialId);
		modelMap.put("departmentList", selectListByParamsbymyself);
        return prefix + "/add1";
    }
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "产品工艺加个扩展表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:jshMaterialCarfprice:add")
	@ResponseBody
	public AjaxResult add(JshMaterialCarfprice jshMaterialCarfprice){
		int b=jshMaterialCarfpriceService.insertSelective(jshMaterialCarfprice);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 产品工艺加个扩展表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "产品工艺加个扩展表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:jshMaterialCarfprice:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=jshMaterialCarfpriceService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
		
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		Integer depid=null;
		if(user.getPosId().equals("4")) {
			depid=user.getDepId();
		}
		List<SysDepartment> selectListByParamsbymyself = planProduceMaterialDetailService.selectListByParamsbymyself(depid);
		
		map.put("departmentList", selectListByParamsbymyself);
        map.put("JshMaterialCarfprice", jshMaterialCarfpriceService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "产品工艺加个扩展表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshMaterialCarfprice:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JshMaterialCarfprice jshMaterialCarfprice)
    {
        return toAjax(jshMaterialCarfpriceService.updateByPrimaryKeySelective(jshMaterialCarfprice));
    }
    
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/dingjia/{id}")
    public String dingjia(@PathVariable("id") String id, ModelMap map)
    {
		
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		Integer depid=null;
		if(user.getPosId().equals("4")) {
			depid=user.getDepId();
		}
		List<SysDepartment> selectListByParamsbymyself = planProduceMaterialDetailService.selectListByParamsbymyself(depid);
		
		map.put("departmentList", selectListByParamsbymyself);
        map.put("JshMaterialCarfprice", jshMaterialCarfpriceService.selectByPrimaryKey(id));

        return prefix + "/dingjia";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "产品工艺加个扩展表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshMaterialCarfprice:dingjia")
    @PostMapping("/dingjiaSave")
    @ResponseBody
    public AjaxResult dingjiaSave(JshMaterialCarfprice jshMaterialCarfprice)
    {
        return toAjax(jshMaterialCarfpriceService.updateByPrimaryKeySelective(jshMaterialCarfprice));
    }
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody JshMaterialCarfprice jshMaterialCarfprice){
		int i=jshMaterialCarfpriceService.updateVisible(jshMaterialCarfprice);
		return toAjax(i);
	}

    
    

	
}
