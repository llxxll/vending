package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanWorkOrder;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.PlanWorkOrderService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 工单Controller
 * @ClassName: PlanWorkOrderController
 * @author fuce
 * @date 2021-12-19 21:27:47
 */
@Api(value = "工单")
@Controller
@RequestMapping("/PlanWorkOrderController")
public class PlanWorkOrderController extends BaseController{
	
	private String prefix = "gen/planWorkOrder";
	
	@Autowired
	private PlanWorkOrderService planWorkOrderService;
	@Autowired
	private JshDepotService jshDepotService;
	
	
	/**
	 * 工单页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planWorkOrder:view")
    public String view(ModelMap model)
    {
//		Integer x= planWorkOrderService.getCountByStatus(null);
//		Integer x0= planWorkOrderService.getCountByStatus(1);
//		Integer x1= planWorkOrderService.getCountByStatus(2);
//		Integer x2= planWorkOrderService.getCountByStatus(3);
//		Integer x3= planWorkOrderService.getCountByStatus(4);
//		Integer x4= planWorkOrderService.getCountByStatus(5);
//		model.put("count", x);
//		model.put("count0", x0);
//		model.put("count1", x1);
//		model.put("count2", x2);
//		model.put("count3", x3);
//		model.put("count4", x4);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "工单", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planWorkOrder:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanWorkOrder planWorkOrder){
		PageInfo<PlanWorkOrder> page=planWorkOrderService.list(tablepar,planWorkOrder) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "工单新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:planWorkOrder:add")
	@ResponseBody
	public AjaxResult add(PlanWorkOrder planWorkOrder){
		int b=planWorkOrderService.insertSelective(planWorkOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 工单删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "工单删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planWorkOrder:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planWorkOrderService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanWorkOrder", planWorkOrderService.selectByPrimaryKey(id));
        List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("houseList", houseList);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "工单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planWorkOrder:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlanWorkOrder planWorkOrder)
    {
		
		int updateByPrimaryKeySelective = planWorkOrderService.updateByPrimaryKeySelective(planWorkOrder);
		if(updateByPrimaryKeySelective==3) {
			return error("数量超出计划上限");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
	@ApiOperation(value = "退料跳转", notes = "涂料跳转")
	@GetMapping("/tuiliao/{id}")
    public String tuiliao(@PathVariable("id") Long id, ModelMap map)
    {
		PlanWorkOrder planwor= new PlanWorkOrder();
		planwor.setId(id);
        List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("houseList", houseList);
		map.put("PlanWorkOrder", planwor);
        return prefix + "/tuiliao";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "工单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planWorkOrder:tuiliao")
    @PostMapping("/tuiliaoSave")
    @ResponseBody
    public AjaxResult tuiliaoSave(PlanWorkOrder planWorkOrder)
    {
		
		int updateByPrimaryKeySelective = planWorkOrderService.tuiliaoSave(planWorkOrder);
		
		if(updateByPrimaryKeySelective==3) {
			return error("退料数量超出领料数量");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
    
    /**
	 * 修改状态
	 * @param record 602583
	 * @return
	 */
    @PostMapping("/updateVisible")
    @RequiresPermissions("gen:planWorkOrder:updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestParam(value="id")Long id ){
    	PlanWorkOrder planWorkOrder = new PlanWorkOrder();
    	planWorkOrder.setId(id);
		int i=planWorkOrderService.updateVisible(planWorkOrder);
		return toAjax(i);
	}

    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:orderStatement:jiesuan")
    @PostMapping("/jiesuansave")
    @ResponseBody
    public AjaxResult jiesuansave(PlanWorkOrder planWorkOrder)
    {
		
		int jiesuansave = planWorkOrderService.jiesuansave(planWorkOrder);
		if(jiesuansave==2) {
			return error("状态不符,无法结算");
		}
        return toAjax(jiesuansave);
    }
    
    @ApiOperation(value = "结算跳转", notes = "结算跳转")
	@GetMapping("/detail/{id}")
    @RequiresPermissions("gen:planWorkOrder:detail")
    public String detail(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanWorkOrder", planWorkOrderService.selectByPrimaryKey(id));

        return prefix + "/detail";
    }
	
}
