package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.OrderFromCategory;
import com.fc.v2.service.OrderFromCategoryService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 主数据-商品信息表Controller
 * @ClassName: OrderFromCategoryController
 * @author fuce
 * @date 2022-06-23 22:58:10
 */
@Api(value = "主数据-商品信息表")
@Controller
@RequestMapping("/OrderFromCategoryController")
public class OrderFromCategoryController extends BaseController{
	
	private String prefix = "gen/orderFromCategory";
	
	@Autowired
	private OrderFromCategoryService orderFromCategoryService;
	
	
	/**
	 * 主数据-商品信息表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:orderFromCategory:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "主数据-商品信息表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:orderFromCategory:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OrderFromCategory orderFromCategory){
		PageInfo<OrderFromCategory> page=orderFromCategoryService.list(tablepar,orderFromCategory) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "主数据-商品信息表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:orderFromCategory:add")
	@ResponseBody
	public AjaxResult add(OrderFromCategory orderFromCategory){
		int b=orderFromCategoryService.insertSelective(orderFromCategory);
		if(b==3) {
			return error("数据重复");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 主数据-商品信息表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "主数据-商品信息表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:orderFromCategory:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=orderFromCategoryService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") String id, ModelMap map)
    {
        map.put("OrderFromCategory", orderFromCategoryService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-商品信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:orderFromCategory:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OrderFromCategory orderFromCategory)
    {
		int updateByPrimaryKeySelective = orderFromCategoryService.updateByPrimaryKeySelective(orderFromCategory);
		if(updateByPrimaryKeySelective==3) {
			return error("数据重复");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OrderFromCategory orderFromCategory){
		int i=orderFromCategoryService.updateVisible(orderFromCategory);
		return toAjax(i);
	}

    
    

	
}
