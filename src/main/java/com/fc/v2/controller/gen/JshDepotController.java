package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.service.JshDepotService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 仓库表Controller
 * @ClassName: JshDepotController
 * @author fuce
 * @date 2021-12-19 23:06:31
 */
@Api(value = "仓库表")
@Controller
@RequestMapping("/JshDepotController")
public class JshDepotController extends BaseController{
	
	private String prefix = "gen/jshDepot";
	
	@Autowired
	private JshDepotService jshDepotService;
	
	
	/**
	 * 仓库表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:jshDepot:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "仓库表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:jshDepot:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,JshDepot jshDepot){
		PageInfo<JshDepot> page=jshDepotService.list(tablepar,jshDepot) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "仓库表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:jshDepot:add")
	@ResponseBody
	public AjaxResult add(JshDepot jshDepot){
		int b=jshDepotService.insertSelective(jshDepot);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 仓库表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "仓库表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:jshDepot:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=jshDepotService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("JshDepot", jshDepotService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "仓库表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshDepot:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JshDepot jshDepot)
    {
        return toAjax(jshDepotService.updateByPrimaryKeySelective(jshDepot));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody JshDepot jshDepot){
		int i=jshDepotService.updateVisible(jshDepot);
		return toAjax(i);
	}

    
    @ApiOperation(value = "下拉仓库列表", notes = "下拉仓库列表")
	@RequestMapping("/getHouseList")
	@ResponseBody
	public List<JshDepot> getHouseList(){
		List<JshDepot> listName=jshDepotService.getHouseList(); 
		return listName;
	}

	
}
