package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyKeshi;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyKeshiService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 医院科室Controller
 * @ClassName: YyKeshiController
 * @author fuce
 * @date 2023-02-04 14:29:28
 */
@Api(value = "医院科室")
@Controller
@RequestMapping("/YyKeshiController")
public class YyKeshiController extends BaseController{
	
	private String prefix = "gen/yyKeshi";
	
	@Autowired
	private YyKeshiService yyKeshiService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	
	/**
	 * 医院科室页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyKeshi:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "医院科室", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyKeshi:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyKeshi yyKeshi){
		PageInfo<YyKeshi> page=yyKeshiService.list(tablepar,yyKeshi) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "医院科室新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyKeshi:add")
	@ResponseBody
	public AjaxResult add(YyKeshi yyKeshi){
		TsysUser user = ShiroUtils.getUser();
		yyKeshi.setCreateTime(new Date());
		yyKeshi.setDeleteFlag(0);
		yyKeshi.setCreator(user.getId());
		yyKeshi.setField3(user.getNickname());
		yyKeshi.setUpdateTime(new Date());
		yyKeshi.setUpdater(user.getId());
		int b=yyKeshiService.insertSelective(yyKeshi);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 医院科室删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "医院科室删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyKeshi:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyKeshiService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyKeshi", yyKeshiService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "医院科室修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyKeshi:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyKeshi yyKeshi)
    {
        return toAjax(yyKeshiService.updateByPrimaryKeySelective(yyKeshi));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyKeshi yyKeshi){
		int i=yyKeshiService.updateVisible(yyKeshi);
		return toAjax(i);
	}

	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request){
		// 文件上传路径，相对路径
		String filePath = System.getProperty("user.dir")+File.separator+"upload";
		System.out.println("文件上传路径，相对路径"+filePath);
		// 获取文件名
		String fileName = file.getOriginalFilename();

		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));

		// 解决中文问题，liunx下中文路径，图片显示问题
		fileName = UUID.randomUUID() + suffixName;

		File fileDir  = new File(filePath);
		// 检测是否存在目录
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		// 构建真实的文件路径
		File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
		try {
			file.transferTo(dest);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String title = fileName;
		fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
		System.out.println(fileName+"文件上传成功");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key","true");
		jsonObject.put("name",fileName);
		jsonObject.put("path",fileName);
		jsonObject.put("title",title);
		return retobject(200,jsonObject);
	}
    

	
}
