package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.SysProvinceExample;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.Head;
import com.fc.v2.service.HeadService;
import com.fc.v2.service.SysProvinceService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 负责人管理表Controller
 * @ClassName: HeadController
 * @author lxl
 * @date 2023-12-01 16:37:59
 */
@Api(value = "负责人管理表")
@Controller
@RequestMapping("/HeadController")
public class HeadController extends BaseController{
	
	private String prefix = "gen/head";
	
	@Autowired
	private HeadService headService;
	@Autowired
	private SysProvinceService sysProvinceService;
	
	/**
	 * 负责人管理表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:head:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "负责人管理表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:head:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,Head head){
		PageInfo<Head> page=headService.list(tablepar,head) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		modelMap.put("provinceList",sysProvinceService.selectByExample(new SysProvinceExample()));
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "负责人管理表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:head:add")
	@ResponseBody
	public AjaxResult add(Head head){
		int b=headService.insertSelective(head);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 负责人管理表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "负责人管理表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:head:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=headService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Head", headService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "负责人管理表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:head:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Head head)
    {
        return toAjax(headService.updateByPrimaryKeySelective(head));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody Head head){
		int i=headService.updateVisible(head);
		return toAjax(i);
	}

    
    

	
}
