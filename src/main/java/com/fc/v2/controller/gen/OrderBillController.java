package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.OrderBill;
import com.fc.v2.service.OrderBillService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 支付订单流水表Controller
 * @ClassName: OrderBillController
 * @author lxl
 * @date 2023-08-30 22:11:47
 */
@Api(value = "支付订单流水表")
@Controller
@RequestMapping("/OrderBillController")
public class OrderBillController extends BaseController{
	
	private String prefix = "gen/orderBill";
	
	@Autowired
	private OrderBillService orderBillService;
	
	
	/**
	 * 支付订单流水表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:orderBill:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "支付订单流水表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:orderBill:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OrderBill orderBill){
		PageInfo<OrderBill> page=orderBillService.list(tablepar,orderBill) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "支付订单流水表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:orderBill:add")
	@ResponseBody
	public AjaxResult add(OrderBill orderBill){
		int b=orderBillService.insertSelective(orderBill);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 支付订单流水表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "支付订单流水表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:orderBill:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=orderBillService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("OrderBill", orderBillService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "支付订单流水表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:orderBill:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OrderBill orderBill)
    {
        return toAjax(orderBillService.updateByPrimaryKeySelective(orderBill));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OrderBill orderBill){
		int i=orderBillService.updateVisible(orderBill);
		return toAjax(i);
	}

    
    

	
}
