package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.PlanOudetailGongying;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.PlanOudetailGongyingService;
import com.fc.v2.service.PlanOudetailService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.AutoCode.ExcelUtils;
import com.fc.v2.util.AutoCode.ExportExecUtil;
import com.fc.v2.util.AutoCode.PageQueryInfo;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 委外单Controller
 * @ClassName: PlanOudetailGongyingController
 * @author fuce
 * @date 2022-03-02 14:13:15
 */
@Api(value = "委外单")
@Controller
@RequestMapping("/PlanOudetailGongyingController")
public class PlanOudetailGongyingController extends BaseController{
	
	private String prefix = "gen/planOudetailGongying";
	
	@Autowired
	private PlanOudetailGongyingService planOudetailGongyingService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private JshDepotService jshDepotService;
	
	/**
	 * 委外单页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planOudetailGongying:view")
    public String view(ModelMap model)
    {
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			if(user.getPosId().equals("9")) {
				List<TsysUser> userList = new ArrayList<TsysUser>();
				userList.add(user);
				
				model.put("userList", userList);
				
			}else {
				List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(29);
				model.put("userList", userList);
				
			}
			List<JshDepot> houseList = jshDepotService.getHouseList();
			model.put("houseList", houseList);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 * @throws ParseException 
	 */
	//@Log(title = "委外单", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planOudetailGongying:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanOudetailGongying planOudetailGongying) throws ParseException{
		String beginTime = planOudetailGongying.getBeginTime();
		String endTime = planOudetailGongying.getEndTime();
 if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planOudetailGongying.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 planOudetailGongying.setEndTime(format);
			 }
		 }
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		if(user.getPosId().equals("9")) {
			planOudetailGongying.setDepartmentOne(userId+"");
		}
		
		PageInfo<PlanOudetailGongying> page=planOudetailGongyingService.list(tablepar,planOudetailGongying) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listApp")
	@RequiresPermissions("gen:planOudetailGongying:list")
	@ResponseBody
	public AjaxResult  listApp(@RequestParam(value = "currentPage", required = false) Integer currentPage,
			@RequestParam(value ="name" ,required = false) String name,
			@RequestParam(value ="id" ,required = false) Long id,
			@RequestParam(value ="field1" ,required = false) String field1
			){
		
		
		Tablepar tablepar = new Tablepar();
		if(currentPage==null) {
			currentPage=1;
		}
		tablepar.setPage(currentPage);
		tablepar.setLimit(10);
		PlanOudetailGongying planOudetailGongying = new PlanOudetailGongying();
		planOudetailGongying.setMateriaNo(name);
		planOudetailGongying.setId(id);
		planOudetailGongying.setField1(field1);
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		if(user.getPosId().equals("9")) {
			planOudetailGongying.setDepartmentOne(userId+"");
		}
		
		PageInfo<PlanOudetailGongying> page=planOudetailGongyingService.list(tablepar,planOudetailGongying) ; 
		
		PageQueryInfo queryInfo = new PageQueryInfo();
		queryInfo.setRows(page.getList());
        queryInfo.setTotal(page.getTotal());
		Map<String, Object> objectMap = new HashMap<String, Object>();
		 objectMap.put("page", queryInfo);
	        
	        
	        return retobject(200, objectMap);
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "委外单新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:planOudetailGongying:add")
	@ResponseBody
	public AjaxResult add(PlanOudetailGongying planOudetailGongying){
		int b=planOudetailGongyingService.insertSelective(planOudetailGongying);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 委外单删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "委外单删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planOudetailGongying:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planOudetailGongyingService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/batchjiesuan")
	@RequiresPermissions("gen:planOudetailGongying:jiesuan")
	@ResponseBody
	public AjaxResult batchjiesuan(String ids){
		int b=planOudetailGongyingService.batchjiesuan(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/jiesuan/{id}")
    public String jiesuan(@PathVariable("id") String id, ModelMap map)
    {
		
		
        map.put("PlanOudetailGongying", planOudetailGongyingService.selectByPrimaryKey(id));

        return prefix + "/jiesuan";
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@RequiresPermissions("gen:planOudetailGongying:detail")
	@GetMapping("/detail/{id}")
	public String detail(@PathVariable("id") String id, ModelMap map)
	{
		
		
		map.put("PlanOudetailGongying", planOudetailGongyingService.selectByPrimaryKey(id));
		
		return prefix + "/detail";
	}
	
	/**
     * 修改保存
     */
    //@Log(title = "委外单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planOudetailGongying:jiesuan")
    @PostMapping("/jiesuansave")
    @ResponseBody
    public AjaxResult jiesuansave(PlanOudetailGongying planOudetailGongying)
    {
		
		int jiesuansave = planOudetailGongyingService.jiesuansave(planOudetailGongying);
		if(jiesuansave==5) {
			return error("状态未完成");
		}
		if(jiesuansave==4) {
			return error("送货单无入库记录");
		}
        return toAjax(jiesuansave);
    }
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
		
		
        map.put("PlanOudetailGongying", planOudetailGongyingService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "委外单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planOudetailGongying:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlanOudetailGongying planOudetailGongying)
    {
        return toAjax(planOudetailGongyingService.updateByPrimaryKeySelective(planOudetailGongying));
    }
	@ApiOperation(value = "送货修改跳转", notes = "修改跳转")
	@GetMapping("/songhuo/{id}")
	public String songhuo(@PathVariable("id") String id, ModelMap map)
	{
		
		List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("houseList", houseList);
		map.put("PlanOudetailGongying", planOudetailGongyingService.selectByPrimaryKey(id));
		return prefix + "/songhuo";
	}
	
	/**
	 * 修改保存
	 */
	//@Log(title = "委外单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:planOudetailGongying:songhuo")
	@PostMapping("/songhuoSave")
	@ResponseBody
	public AjaxResult songhuoSave(PlanOudetailGongying planOudetailGongying)
	{
		int songhuoSave = planOudetailGongyingService.songhuoSave(planOudetailGongying);
		if(songhuoSave==4) {
			return error("状态不符无法送货");
		}
		if(songhuoSave==3) {
			return error("数量超出限制");
		}
		return toAjax(songhuoSave);
	}
	@ApiOperation(value = "送货修改跳转", notes = "修改跳转")
	@GetMapping("/zhijian/{id}")
	public String zhijian(@PathVariable("id") String id, ModelMap map)
	{
		
		map.put("PlanOudetailGongying", planOudetailGongyingService.selectByPrimaryKey(id));
		return prefix + "/zhijian";
	}
	
	/**
	 * 修改保存
	 */
	//@Log(title = "委外单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:planOudetailGongying:zhijian")
	@PostMapping("/zhijianSave")
	@ResponseBody
	public AjaxResult zhijianSave(PlanOudetailGongying planOudetailGongying)
	{
		int songhuoSave = planOudetailGongyingService.zhijianSave(planOudetailGongying);
		
		return toAjax(songhuoSave);
	}
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:planOudetailGongying:songhuo")
	@PostMapping("/songhuoSaveAPP")
	@ResponseBody
	public AjaxResult songhuoSaveAPP(PlanOudetailGongying planOudetailGongying)
	{
		int songhuoSave = planOudetailGongyingService.songhuoSave(planOudetailGongying);
		if(songhuoSave==4) {
			return error("状态不符无法送货");
		}
		if(songhuoSave==3) {
			return error("数量超出限制");
		}
		if(songhuoSave>0) {
			
			return AjaxResult.successData(200, "");
		}else {
			return error("操作失败");
		}
	}
	
	
	@ApiOperation(value = "送货修改跳转", notes = "修改跳转")
	@GetMapping("/tuiliao/{id}")
	public String tuiliao(@PathVariable("id") String id, ModelMap map)
	{
		
		List<JshDepot> houseList = jshDepotService.getHouseList();
		map.put("houseList", houseList);
		map.put("PlanOudetailGongying", planOudetailGongyingService.selectByPrimaryKey(id));
		return prefix + "/tuiliao";
	}
	
	/**
	 * 修改保存
	 */
	//@Log(title = "委外单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:planOudetailGongying:tuiliao")
	@PostMapping("/tuiliaoSave")
	@ResponseBody
	public AjaxResult tuiliaoSave(PlanOudetailGongying planOudetailGongying)
	{
		int songhuoSave = planOudetailGongyingService.tuiliaoSave(planOudetailGongying);
		if(songhuoSave==4) {
			return error("状态不符无法退料");
		}
		if(songhuoSave==3) {
			return error("数量超出限制");
		}
		return toAjax(songhuoSave);
	}
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:planOudetailGongying:tuiliao")
	@PostMapping("/tuiliaoSaveAPP")
	@ResponseBody
	public AjaxResult tuiliaoSaveAPP(PlanOudetailGongying planOudetailGongying)
	{
		int songhuoSave = planOudetailGongyingService.tuiliaoSave(planOudetailGongying);
		if(songhuoSave==4) {
			return error("状态不符无法退料");
		}
		if(songhuoSave==3) {
			return error("数量超出限制");
		}
		if(songhuoSave>0) {
			
			return AjaxResult.successData(200, "");
		}else {
			return error("操作失败");
		}
	}
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PostMapping("/updateVisible")
    @RequiresPermissions("gen:planOudetailGongying:wancheng")
	@ResponseBody
    public AjaxResult updateVisible(@RequestParam(value="id")Long id,@RequestParam(value="field1") String field1){
    	PlanOudetailGongying planOudetailGongying = new PlanOudetailGongying();
    	planOudetailGongying.setId(id);
    	planOudetailGongying.setField1(field1);
		int i=planOudetailGongyingService.updateVisible(planOudetailGongying);
		return toAjax(i);
	}

    
    @GetMapping(value = "/exportstatementExcel")
    @RequiresPermissions("gen:planOudetailGongying:daochu")
    public void exportstatementExcel(@RequestParam("name") String name,
                                        @RequestParam("beginTime") String beginTime,
                                        @RequestParam("endTime") String endTime,
                                        @RequestParam(value ="ids" ,required = false) String ids,
                                        @RequestParam(value ="materialName" ,required = false) String materialName,
                                		@RequestParam(value ="weiwaiId" ,required = false) String weiwaiId,
                                		@RequestParam(value ="searchStatus" ,required = false) String searchStatus,
                                		@RequestParam(value ="searchType" ,required = false) String searchType,
                                		@RequestParam(value ="searchWorker" ,required = false) String searchWorker,
                                        HttpServletRequest request, HttpServletResponse response) throws ParseException {
		PlanOudetailGongying ppm = new PlanOudetailGongying();
    	ppm.setField1("2");
    	ppm.setDepartmentOne(searchWorker);//供应商
    	if(weiwaiId!=null) {
    		if(!weiwaiId.trim().equals("")) {
    			ppm.setId(Long.valueOf(weiwaiId));
    		}
    	}
    	if(beginTime!=null) {
			 
			 if(!beginTime.trim().equals("")) {
				 beginTime=beginTime+" 00:00:00";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(beginTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 ppm.setBeginTime(format);
			 }
		 }
		 
		 if(endTime!=null) {
			 if(!endTime.trim().equals("")) {
				 endTime=endTime+" 23:59:59";
				 SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 
				 Date date =sdf.parse(endTime);
				  
				 Calendar calendar = Calendar.getInstance();
				  
				 calendar.setTime(date);
				 calendar.add(Calendar.HOUR, -8);
				 
				 String format = sdf.format(calendar.getTime());
				 ppm.setEndTime(format);
			 }
		 }
    	ppm.setMateriaNo(name);
    	ppm.setField2(searchType);
    	ppm.setMaterialName(materialName);	
	        try {
	        	List<PlanOudetailGongying> list=null;
	        	if(ids.equals("")) {
	        		list= planOudetailGongyingService.excgetlist(ppm);
	        	}else {
	        		Long[] integers = ConvertUtil.toLongArray(",", ids);
	    			List<Long> stringB = Arrays.asList(integers);
	        		list =planOudetailGongyingService.getListByIds(stringB);
	        	}
	        	
	            String[] names = {"委外单","物料号", "物料名称", "数量","不合格数量" ,"单价","不含税单价", "不含税总价" ,"供应商" };
	            String title = "委外结算报表";
	            List<String[]> objects = new ArrayList<String[]>();
	            if (null != list) {
	                for (PlanOudetailGongying s : list) {
	                	String[] objs = new String[9];
	                	objs[0] = s.getId()+"";
	                    objs[1] = s.getMateriaNo();
	                    objs[2] = s.getMaterialName();
	                    objs[3] = s.getCount()+"";
	                    objs[4] = s.getCount2()+"";
	                    objs[5] = "0.00"+"";
	                    objs[6] = "0.00"+"";
	                    objs[7] = "0.00"+"";
	                    objs[8] = s.getDepartmentOne();
	                	
	                    
	                    objects.add(objs);
	                }
	            }
	            File file = ExcelUtils.exportObjectsWithoutTitle(title, names, title, objects);
	            ExportExecUtil.showExec(file, file.getName(), response);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	
}
