package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.Beihuo;
import com.fc.v2.service.BeihuoService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 备货管理Controller
 * @ClassName: BeihuoController
 * @author fuce
 * @date 2024-01-23 22:17:48
 */
@Api(value = "备货管理")
@Controller
@RequestMapping("/BeihuoController")
public class BeihuoController extends BaseController{
	
	private String prefix = "gen/beihuo";
	
	@Autowired
	private BeihuoService beihuoService;
	
	
	/**
	 * 备货管理页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:beihuo:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "备货管理", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:beihuo:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,Beihuo beihuo){
		PageInfo<Beihuo> page=beihuoService.list(tablepar,beihuo) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "备货管理新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:beihuo:add")
	@ResponseBody
	public AjaxResult add(Beihuo beihuo){
		int b=beihuoService.insertSelective(beihuo);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 备货管理删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "备货管理删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:beihuo:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=beihuoService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Beihuo", beihuoService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "备货管理修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:beihuo:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Beihuo beihuo)
    {
        return toAjax(beihuoService.updateByPrimaryKeySelective(beihuo));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody Beihuo beihuo){
		int i=beihuoService.updateVisible(beihuo);
		return toAjax(i);
	}

    
    

	
}
