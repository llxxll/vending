package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.PlanOut;
import com.fc.v2.service.PlanOutService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 外协计划Controller
 * @ClassName: PlanOutController
 * @author fuce
 * @date 2022-03-02 14:13:02
 */
@Api(value = "外协计划")
@Controller
@RequestMapping("/PlanOutController")
public class PlanOutController extends BaseController{
	
	private String prefix = "gen/planOut";
	
	@Autowired
	private PlanOutService planOutService;
	
	
	/**
	 * 外协计划页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planOut:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "外协计划", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planOut:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanOut planOut){
		PageInfo<PlanOut> page=planOutService.list(tablepar,planOut) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "外协计划新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:planOut:add")
	@ResponseBody
	public AjaxResult add(PlanOut planOut){
		int b=planOutService.insertSelective(planOut);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 外协计划删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "外协计划删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planOut:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planOutService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanOut", planOutService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "外协计划修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:planOut:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlanOut planOut)
    {
        return toAjax(planOutService.updateByPrimaryKeySelective(planOut));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanOut planOut){
		int i=planOutService.updateVisible(planOut);
		return toAjax(i);
	}

    
    

	
}
