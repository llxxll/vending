package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.Machine;
import com.fc.v2.service.MachineService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 机器管理表Controller
 * @ClassName: MachineController
 * @author lxl
 * @date 2023-12-01 16:39:06
 */
@Api(value = "机器管理表")
@Controller
@RequestMapping("/MachineController")
public class MachineController extends BaseController{
	
	private String prefix = "gen/machine";
	
	@Autowired
	private MachineService machineService;
	
	
	/**
	 * 机器管理表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:machine:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "机器管理表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:machine:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,Machine machine){
		PageInfo<Machine> page=machineService.list(tablepar,machine) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "机器管理表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:machine:add")
	@ResponseBody
	public AjaxResult add(Machine machine){
		int b=machineService.insertSelective(machine);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 机器管理表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "机器管理表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:machine:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=machineService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Machine", machineService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "机器管理表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:machine:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Machine machine)
    {
        return toAjax(machineService.updateByPrimaryKeySelective(machine));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody Machine machine){
		int i=machineService.updateVisible(machine);
		return toAjax(i);
	}

    
    

	
}
