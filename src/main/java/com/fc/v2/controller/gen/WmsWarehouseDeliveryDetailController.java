package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.auto.WmsWarehouseEntryDetail;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.WmsWarehouseDeliveryDetail;
import com.fc.v2.service.WmsWarehouseDeliveryDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 出库单明细Controller
 * @ClassName: WmsWarehouseDeliveryDetailController
 * @author fuce
 * @date 2021-12-19 21:28:43
 */
@Api(value = "出库单明细")
@Controller
@RequestMapping("/WmsWarehouseDeliveryDetailController")
public class WmsWarehouseDeliveryDetailController extends BaseController{
	
	private String prefix = "gen/wmsWarehouseDeliveryDetail";
	
	@Autowired
	private WmsWarehouseDeliveryDetailService wmsWarehouseDeliveryDetailService;
	
	
	/**
	 * 出库单明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:view")
    public String view(ModelMap model)
    {
    	
        return prefix + "/list";
    }
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/viewwaixei")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:viewwaixei")
	public String viewwaixei(ModelMap model)
	{
		
		return prefix + "/waixiechuku";
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/viewchejian")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:viewchejian")
	public String viewchejian(ModelMap model)
	{
		
		return prefix + "/chejianchuku";
	}
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "出库单明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		 
		PageInfo<WmsWarehouseDeliveryDetail> page=wmsWarehouseDeliveryDetailService.list(tablepar,wmsWarehouseDeliveryDetail) ;
		return pageTable(page.getList(),page.getTotal());
	}
	
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listwaixie")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:listwaixie")
	@ResponseBody
	public ResultTable listwaixie(Tablepar tablepar,WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		 Long userId = ShiroUtils.getUserId();
			TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("3")) {
			wmsWarehouseDeliveryDetail.setHouseNo(user.getRemark());
		}
		wmsWarehouseDeliveryDetail.setQualityNo("1");
		PageInfo<WmsWarehouseDeliveryDetail> page=wmsWarehouseDeliveryDetailService.listweiwai(tablepar,wmsWarehouseDeliveryDetail) ;
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/listchejian")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:listchejian")
	@ResponseBody
	public ResultTable listchejian(Tablepar tablepar,WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		 Long userId = ShiroUtils.getUserId();
			TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
			String posId = user.getPosId();
		if(posId.equals("10")||posId.equals("3")) {
			wmsWarehouseDeliveryDetail.setHouseNo(user.getRemark());
		}
		wmsWarehouseDeliveryDetail.setQualityNo("2");
		PageInfo<WmsWarehouseDeliveryDetail> page=wmsWarehouseDeliveryDetailService.listchejian(tablepar,wmsWarehouseDeliveryDetail) ;
		return pageTable(page.getList(),page.getTotal());
	}
	
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "出库单明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:add")
	@ResponseBody
	public AjaxResult add(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		int b=wmsWarehouseDeliveryDetailService.insertSelective(wmsWarehouseDeliveryDetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 出库单明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "出库单明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:wmsWarehouseDeliveryDetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=wmsWarehouseDeliveryDetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("WmsWarehouseDeliveryDetail", wmsWarehouseDeliveryDetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "出库单明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:wmsWarehouseDeliveryDetail:edit")
    @PostMapping("/editSave")
    @ResponseBody
    public AjaxResult editSave(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail)
    {
		
		
		
		int updateByPrimaryKeySelective = wmsWarehouseDeliveryDetailService.editSave(wmsWarehouseDeliveryDetail);
		
		if(updateByPrimaryKeySelective==3) {
			return  error("工单已完成,无法修改,请上级反馈");
		}
		if(updateByPrimaryKeySelective==4) {
			return  error("委外单已完成,无法修改,请上级反馈");
		}
		
			return  toAjax(updateByPrimaryKeySelective);
		
        
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail){
		int i=wmsWarehouseDeliveryDetailService.updateVisible(wmsWarehouseDeliveryDetail);
		return toAjax(i);
	}


	/**
	 * 确认出库跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "出库跳转", notes = "出库跳转")
	@GetMapping("/bill/{id}")
	public String bill(@PathVariable("id") String id, ModelMap map)
	{
		map.put("wmsWarehouseDeliveryDetail", wmsWarehouseDeliveryDetailService.selectByPrimaryKey(id));
		return prefix + "/chuku";
	}
	

	/**
	 * 出库保存
	 */
	//@Log(title = "出库单明细修改", action = "111")
	@ApiOperation(value = "出库保存", notes = "出库保存")
	@PostMapping("/savechuku")
	@RequiresPermissions("gen:wmsWarehouseEntryDetail:ruku")
	@ResponseBody
	public AjaxResult savechuku(WmsWarehouseDeliveryDetail wmsWarehouseDeliveryDetail)
	{
		
		
		int updateByPrimaryKeySelective = wmsWarehouseDeliveryDetailService.savechuku(wmsWarehouseDeliveryDetail);
		if(updateByPrimaryKeySelective==5) {
			return error("已出库,重复操作");
		}
		
			return  toAjax(updateByPrimaryKeySelective);
	}

	
}
