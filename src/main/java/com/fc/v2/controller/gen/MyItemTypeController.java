package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.MyItemType;
import com.fc.v2.service.MyItemTypeService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Controller
 * @ClassName: MyItemTypeController
 * @author fuce
 * @date 2022-10-16 22:58:25
 */
@Api(value = "")
@Controller
@RequestMapping("/MyItemTypeController")
public class MyItemTypeController extends BaseController{
	
	private String prefix = "gen/myItemType";
	
	@Autowired
	private MyItemTypeService myItemTypeService;
	
	
	/**
	 * 页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:myItemType:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:myItemType:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MyItemType myItemType){
		PageInfo<MyItemType> page=myItemTypeService.list(tablepar,myItemType) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:myItemType:add")
	@ResponseBody
	public AjaxResult add(MyItemType myItemType){
		int b=myItemTypeService.insertSelective(myItemType);
		if(b==5) {
			return error("名称重复");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:myItemType:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=myItemTypeService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("MyItemType", myItemTypeService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:myItemType:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MyItemType myItemType)
    {
		int updateByPrimaryKeySelective = myItemTypeService.updateByPrimaryKeySelective(myItemType);
		if(updateByPrimaryKeySelective==5) {
			return error("名称重复");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MyItemType myItemType){
		int i=myItemTypeService.updateVisible(myItemType);
		return toAjax(i);
	}

    
    

	
}
