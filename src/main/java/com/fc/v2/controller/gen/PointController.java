package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.mapper.auto.CompanyMapper;
import com.fc.v2.mapper.auto.HeadMapper;
import com.fc.v2.mapper.auto.LineMapper;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.CompanyService;
import com.fc.v2.service.HeadService;
import com.fc.v2.service.PointService;
import com.fc.v2.service.SysProvinceService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 点位管理表Controller
 * @ClassName: PointController
 * @author lxl
 * @date 2023-12-01 16:36:52
 */
@Api(value = "点位管理表")
@Controller
@RequestMapping("/PointController")
public class PointController extends BaseController{
	
	private String prefix = "gen/point";
	
	@Autowired
	private PointService pointService;
	@Autowired
	private SysProvinceService sysProvinceService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private HeadService headService;
	@Resource
	private CompanyMapper companyMapper;
	@Resource
	private HeadMapper headMapper;
	@Resource
	private LineMapper lineMapper;
	/**
	 * 点位管理表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:point:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "点位管理表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:point:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,Point point){
		PageInfo<Point> page=pointService.list(tablepar,point) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<Company> clist= companyMapper.selectByExample(new CompanyExample());
		List<Line> llist= lineMapper.selectByExample(new LineExample());
		modelMap.put("CompanyList", clist);
		modelMap.put("LineList", llist);
		modelMap.put("provinceList",sysProvinceService.selectByExample(new SysProvinceExample()));
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "点位管理表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:point:add")
	@ResponseBody
	public AjaxResult add(Point point){
		int b=pointService.insertSelective(point);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 点位管理表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "点位管理表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:point:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=pointService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Point", pointService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "点位管理表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:point:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Point point)
    {
        return toAjax(pointService.updateByPrimaryKeySelective(point));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody Point point){
		int i=pointService.updateVisible(point);
		return toAjax(i);
	}

    
    

	
}
