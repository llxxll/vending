package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.service.MdProduccategoryService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 主数据-商品信息表 类别Controller
 * @ClassName: MdProduccategoryController
 * @author fuce
 * @date 2022-06-23 22:57:59
 */
@Api(value = "主数据-商品信息表 类别")
@Controller
@RequestMapping("/MdProduccategoryController")
public class MdProduccategoryController extends BaseController{
	
	private String prefix = "gen/mdProduccategory";
	
	@Autowired
	private MdProduccategoryService mdProduccategoryService;
	
	
	/**
	 * 主数据-商品信息表 类别页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:mdProduccategory:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "主数据-商品信息表 类别", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:mdProduccategory:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MdProduccategory mdProduccategory){
		PageInfo<MdProduccategory> page=mdProduccategoryService.list(tablepar,mdProduccategory) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "主数据-商品信息表 类别新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:mdProduccategory:add")
	@ResponseBody
	public AjaxResult add(MdProduccategory mdProduccategory){
		int b=mdProduccategoryService.insertSelective(mdProduccategory);
		if(b==3) {
			return error("数据重复");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 主数据-商品信息表 类别删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "主数据-商品信息表 类别删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:mdProduccategory:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=mdProduccategoryService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") String id, ModelMap map)
    {
        map.put("MdProduccategory", mdProduccategoryService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-商品信息表 类别修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:mdProduccategory:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdProduccategory mdProduccategory)
    {
		int updateByPrimaryKeySelective = mdProduccategoryService.updateByPrimaryKeySelective(mdProduccategory);
		
		if(updateByPrimaryKeySelective==3) {
			return error("数据重复");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MdProduccategory mdProduccategory){
		int i=mdProduccategoryService.updateVisible(mdProduccategory);
		return toAjax(i);
	}

    
    

	
}
