package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.auto.MdProducdetail;
import com.fc.v2.model.auto.MdProducspecs;
import com.fc.v2.service.MdProduccategoryService;
import com.fc.v2.service.MdProducdetailService;
import com.fc.v2.service.MdProducspecsService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 主数据-商品信息表Controller
 * @ClassName: MdProducdetailController
 * @author fuce
 * @date 2022-06-27 22:56:47
 */
@Api(value = "主数据-商品信息表")
@Controller
@RequestMapping("/MdProducdetailController")
public class MdProducdetailController extends BaseController{
	
	private String prefix = "gen/mdProducdetail";
	
	@Autowired
	private MdProducdetailService mdProducdetailService;
	@Autowired
	private MdProduccategoryService mdProduccategoryService;
	@Autowired
	private MdProducspecsService mdProducspecsService;
	
	/**
	 * 主数据-商品信息表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:mdProducdetail:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "主数据-商品信息表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:mdProducdetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MdProducdetail mdProducdetail){
		PageInfo<MdProducdetail> page=mdProducdetailService.list(tablepar,mdProducdetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list1")
	@ResponseBody
	public ResultTable list1(Tablepar tablepar,MdProducdetail mdProducdetail){
		PageInfo<MdProducdetail> page=mdProducdetailService.list(tablepar,mdProducdetail) ;
		return pageTable(page.getList(),page.getTotal());
	}
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "主数据-商品信息表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:mdProducdetail:add")
	@ResponseBody
	public AjaxResult add(MdProducdetail mdProducdetail){
		int b=mdProducdetailService.insertSelective(mdProducdetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "新增", notes = "新增")
	@GetMapping("/addrow")
	@ResponseBody
	public MdProducdetail addrow(){
		//获取所有类别
		List<MdProduccategory> list = mdProduccategoryService.getList(null);
		//获取所有规格
		List<MdProducspecs> list1 = mdProducspecsService.getList(null);
		MdProducdetail mdProducdetail = new MdProducdetail();
		mdProducdetail.setMdProduccategory(list);
		mdProducdetail.setMdProducspecslist(list1);
		return mdProducdetail;
	}
	
	/**
	 * 主数据-商品信息表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "主数据-商品信息表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:mdProducdetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=mdProducdetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("MdProducdetail", mdProducdetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-商品信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:mdProducdetail:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdProducdetail mdProducdetail)
    {
        return toAjax(mdProducdetailService.updateByPrimaryKeySelective(mdProducdetail));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MdProducdetail mdProducdetail){
		int i=mdProducdetailService.updateVisible(mdProducdetail);
		return toAjax(i);
	}

    
    

	
}
