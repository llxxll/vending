package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.MyItem;
import com.fc.v2.model.auto.MyItemDetail;
import com.fc.v2.model.auto.MyItemReport;
import com.fc.v2.model.auto.MyItemType;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.MyItemService;
import com.fc.v2.service.MyItemTypeService;
import com.fc.v2.service.SysUserService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Controller
 * @ClassName: MyItemController
 * @author fuce
 * @date 2022-08-28 21:49:04
 */
@Api(value = "项目管理")
@Controller
@RequestMapping("/MyItemController")
public class MyItemController extends BaseController{
	
	private String prefix = "gen/myItem";
	
	@Autowired
	private MyItemTypeService myItemTypeService;
	@Autowired
	private MyItemService myItemService;
	@Autowired
	private SysUserService sysUserService;
	
	
	/**
	 * 页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:myItem:view")
    public String view(ModelMap model)
    {
		
		List<MyItemType> getslist = myItemTypeService.getslist(null);
		model.put("myItemTypeList", getslist);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:myItem:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MyItem myItem){
		PageInfo<MyItem> page=myItemService.list(tablepar,myItem) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<TsysUser> gettttlist = sysUserService.gettttlist(null);
		modelMap.put("userList", gettttlist);
		List<MyItemType> getslist = myItemTypeService.getslist(null);
		modelMap.put("myItemTypeList", getslist);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:myItem:add")
	@ResponseBody
	public AjaxResult add(@RequestBody MyItem myItem){
		List<MyItemDetail> detailList = myItem.getDetailList();
		
		int b=myItemService.insertSelective(myItem);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:myItem:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=myItemService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "开始", notes = "删除")
	@PostMapping("/kaishi")
	@RequiresPermissions("gen:myItem:kaishi")
	@ResponseBody
	public AjaxResult kaishi(String ids){
		int b=myItemService.kaishi(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "开始", notes = "删除")
	@PostMapping("/jiaji")
	@RequiresPermissions("gen:myItem:jiaji")
	@ResponseBody
	public AjaxResult jiaji(String ids){
		int b=myItemService.jiaji(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	@ApiOperation(value = "完成", notes = "删除")
	@PostMapping("/wancheng")
	@RequiresPermissions("gen:myItem:wancheng")
	@ResponseBody
	public AjaxResult wancheng(String ids){
		int b=myItemService.wancheng(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "撤销", notes = "删除")
	@PostMapping("/chexiao")
	@RequiresPermissions("gen:myItem:chexiao")
	@ResponseBody
	public AjaxResult chexiao(String ids){
		int b=myItemService.chexiao(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/detail/{id}")
	 @RequiresPermissions("gen:myItem:detail")
	public String detail(@PathVariable("id") String id, ModelMap map)
	{
		map.put("MyItem", myItemService.selectByPrimaryKey(id));
		
		return prefix + "/detail";
	}
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("MyItem", myItemService.selectByPrimaryKey(id));
        List<TsysUser> gettttlist = sysUserService.gettttlist(null);
        map.put("userList", gettttlist);
        List<MyItemType> getslist = myItemTypeService.getslist(null);
        map.put("myItemTypeList", getslist);
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:myItem:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody MyItem myItem)
    {
        return toAjax(myItemService.updateByPrimaryKeySelective(myItem));
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/huibao/{id}")
    public String huibao(@PathVariable("id") String id, ModelMap map)
    {
        map.put("MyItem", myItemService.selectByPrimaryKey(id));

        return prefix + "/huibao";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:myItem:huibao")
    @PostMapping("/huibaoSave")
    @ResponseBody
    public AjaxResult huibaoSave(@RequestBody MyItemReport myItem)
    {
        return toAjax(myItemService.huibaoSave(myItem));
    }
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MyItem myItem){
		int i=myItemService.updateVisible(myItem);
		return toAjax(i);
	}

    
    

	
}
