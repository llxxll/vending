package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshSupplier;
import com.fc.v2.service.JshSupplierService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 供应商/客户信息表Controller
 * @ClassName: JshSupplierController
 * @author fuce
 * @date 2021-12-19 23:17:26
 */
@Api(value = "供应商/客户信息表")
@Controller
@RequestMapping("/JshSupplierController")
public class JshSupplierController extends BaseController{
	
	private String prefix = "gen/jshSupplier";
	
	@Autowired
	private JshSupplierService jshSupplierService;
	
	
	/**
	 * 供应商/客户信息表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:jshSupplier:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "供应商/客户信息表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:jshSupplier:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,JshSupplier jshSupplier){
		PageInfo<JshSupplier> page=jshSupplierService.list(tablepar,jshSupplier) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "供应商/客户信息表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:jshSupplier:add")
	@ResponseBody
	public AjaxResult add(JshSupplier jshSupplier){
		int b=jshSupplierService.insertSelective(jshSupplier);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 供应商/客户信息表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "供应商/客户信息表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:jshSupplier:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=jshSupplierService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("JshSupplier", jshSupplierService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "供应商/客户信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshSupplier:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JshSupplier jshSupplier)
    {
        return toAjax(jshSupplierService.updateByPrimaryKeySelective(jshSupplier));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody JshSupplier jshSupplier){
		int i=jshSupplierService.updateVisible(jshSupplier);
		return toAjax(i);
	}

    
    

	
}
