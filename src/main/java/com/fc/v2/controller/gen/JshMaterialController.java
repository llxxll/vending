package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.service.JshMaterialService;
import com.fc.v2.util.ApiResultUtil;
import com.fc.v2.util.ExeclUtil;
import com.fc.v2.util.Msg;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 产品表Controller
 * @ClassName: JshMaterialController
 * @author fuce
 * @date 2021-12-19 21:40:36
 */
@Api(value = "产品表")
@Controller
@RequestMapping("/JshMaterialController")
public class JshMaterialController extends BaseController{
	
	private String prefix = "gen/jshMaterial";
	
	@Autowired
	private JshMaterialService jshMaterialService;
	
	
	/**
	 * 产品表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:jshMaterial:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view1")
	@RequiresPermissions("gen:jshMaterial:view")
	public String view1(ModelMap model)
	{
		return prefix + "/list1";
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view2")
	@RequiresPermissions("gen:jshMaterial:view")
	public String view12(ModelMap model)
	{
		return prefix + "/list2";
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view3")
	@RequiresPermissions("gen:jshMaterial:view")
	public String view3(ModelMap model)
	{
		return prefix + "/list3";
	}
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "产品表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:jshMaterial:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,JshMaterial jshMaterial){
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "产品表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/getmatterlist")
	@RequiresPermissions("gen:jshMaterial:list")
	@ResponseBody
	public ResultTable getmatterlist(Tablepar tablepar,JshMaterial jshMaterial){
		jshMaterial.setCategoryId(1l);
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/getCLlist")
	@RequiresPermissions("gen:jshMaterial:list")
	@ResponseBody
	public ResultTable getCLlist(Tablepar tablepar,JshMaterial jshMaterial){
		jshMaterial.setCategoryId(2l);
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	@ApiOperation(value = "查询下拉表", notes = "查询区下拉表")
	@GetMapping("/selectList")
	@RequiresPermissions("gen:planProduceMaterialDetail:list")
	@ResponseBody
	public Msg selectList (Tablepar tablepar, JshMaterial jshMaterial)
	{
		jshMaterial.setCategoryId(null);
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ;
		return ApiResultUtil.success(page.getList(),page.getTotal());
	}
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "产品表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:jshMaterial:add")
	@ResponseBody
	public AjaxResult add(JshMaterial jshMaterial){
		int b=jshMaterialService.insertSelective(jshMaterial);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 产品表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "产品表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:jshMaterial:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=jshMaterialService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("JshMaterial", jshMaterialService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "产品表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:jshMaterial:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JshMaterial jshMaterial)
    {
        return toAjax(jshMaterialService.updateByPrimaryKeySelective(jshMaterial));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody JshMaterial jshMaterial){
		int i=jshMaterialService.updateVisible(jshMaterial);
		return toAjax(i);
	}

    
    @ApiOperation(value = "文件上传", notes = "文件上传")
    @PostMapping("/upload")
    @ResponseBody
    public AjaxResult upload(@RequestParam("file") MultipartFile file) throws Exception
    {
    	InputStream inputStream = file.getInputStream();
    	Map<String, String> fieldd = new HashMap<String, String>(); 
        fieldd.put("物料号", "name");
        fieldd.put("物料描述", "model");
        fieldd.put("类型", "categoryName");
    	List<JshMaterial> execltoList = ExeclUtil.ExecltoList(inputStream, JshMaterial.class, fieldd, file.getOriginalFilename());
    	int b= jshMaterialService.upload(execltoList);
    	
		if(b>0){
			return success();
		}else{
			return error();
		}
    }

	
}
