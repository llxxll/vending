package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.OmsOrderRetreat;
import com.fc.v2.service.OmsOrderRetreatService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 销售退货表Controller
 * @ClassName: OmsOrderRetreatController
 * @author lxl
 * @date 2022-09-22 22:34:43
 */
@Api(value = "销售退货表")
@Controller
@RequestMapping("/OmsOrderRetreatController")
public class OmsOrderRetreatController extends BaseController{
	
	private String prefix = "gen/omsOrderRetreat";
	
	@Autowired
	private OmsOrderRetreatService omsOrderRetreatService;
	
	
	/**
	 * 销售退货表页面展示
	 * @param model
	 * @return String
	 * @author lxl
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:omsOrderRetreat:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "销售退货表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:omsOrderRetreat:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OmsOrderRetreat omsOrderRetreat){
		PageInfo<OmsOrderRetreat> page=omsOrderRetreatService.list(tablepar,omsOrderRetreat) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "销售退货表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:omsOrderRetreat:add")
	@ResponseBody
	public AjaxResult add(OmsOrderRetreat omsOrderRetreat){
		int b=omsOrderRetreatService.insertSelective(omsOrderRetreat);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 销售退货表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "销售退货表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:omsOrderRetreat:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=omsOrderRetreatService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("OmsOrderRetreat", omsOrderRetreatService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "销售退货表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:omsOrderRetreat:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OmsOrderRetreat omsOrderRetreat)
    {
		int updateByPrimaryKeySelective = omsOrderRetreatService.updateByPrimaryKeySelective(omsOrderRetreat);
		if(updateByPrimaryKeySelective==3) {
			return error("数量超限");
		}
		
		
        return toAjax(updateByPrimaryKeySelective);
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OmsOrderRetreat omsOrderRetreat){
		int i=omsOrderRetreatService.updateVisible(omsOrderRetreat);
		return toAjax(i);
	}
	@ApiOperation(value = "退货审核", notes = "退货审核")
	@DeleteMapping("/tuihuo")
	@RequiresPermissions("gen:omsOrderRetreat:tuihuo")
	@ResponseBody
	public AjaxResult tuihuo(String id){
		int b =omsOrderRetreatService.tuihuo(id);

		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	@ApiOperation(value = "退货确认收货", notes = "退货确认收货")
	@DeleteMapping("/tuihuoqueren")
	@RequiresPermissions("gen:omsOrderRetreat:tuihuoqueren")
	@ResponseBody
	public AjaxResult tuihuoqueren(String id){
		int b =omsOrderRetreatService.tuihuoqueren(id);

		if(b>0){
			return success();
		}else{
			return error();
		}
	}
    

	
}
