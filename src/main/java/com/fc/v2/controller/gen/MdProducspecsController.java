package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.common.support.ConvertUtil;
import com.fc.v2.model.auto.MdProduccategory;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.MdProducspecs;
import com.fc.v2.model.auto.PlanOustatementDetail;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.MdProduccategoryService;
import com.fc.v2.service.MdProducspecsService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.AutoCode.ExcelUtils;
import com.fc.v2.util.AutoCode.ExportExecUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 主数据-商品信息表Controller
 * @ClassName: MdProducspecsController
 * @author fuce
 * @date 2022-06-23 22:58:28
 */
@Api(value = "主数据-商品信息表")
@Controller
@RequestMapping("/MdProducspecsController")
public class MdProducspecsController extends BaseController{
	
	private String prefix = "gen/mdProducspecs";
	
	@Autowired
	private MdProducspecsService mdProducspecsService;
	@Autowired
	private MdProduccategoryService mdProduccategoryService;

	/**
	 * 主数据-商品信息表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:mdProducspecs:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "主数据-商品信息表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:mdProducspecs:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MdProducspecs mdProducspecs){
		PageInfo<MdProducspecs> page=mdProducspecsService.list(tablepar,mdProducspecs) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		List<MdProduccategory> list = mdProduccategoryService.getList(null);
		modelMap.put("mdProduccategoryList", list);
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "主数据-商品信息表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:mdProducspecs:add")
	@ResponseBody
	public AjaxResult add(MdProducspecs mdProducspecs){
		int b=mdProducspecsService.insertSelective(mdProducspecs);
		if(b==3) {
			return error("数据重复");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 主数据-商品信息表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "主数据-商品信息表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:mdProducspecs:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=mdProducspecsService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") String id, ModelMap map)
    {
		List<MdProduccategory> list = mdProduccategoryService.getList(null);
		map.put("mdProduccategoryList", list);
        map.put("MdProducspecs", mdProducspecsService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-商品信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:mdProducspecs:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdProducspecs mdProducspecs)
    {
		int updateByPrimaryKeySelective = mdProducspecsService.updateByPrimaryKeySelective(mdProducspecs);
		if(updateByPrimaryKeySelective==3) {
			return error("数据重复");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/gongjia/{sid}")
	public String gongjia(@PathVariable("sid") String id, ModelMap map)
	{
		map.put("MdProducspecs", mdProducspecsService.selectByPrimaryKey(id));
		
		return prefix + "/gongjia";
	}
	
	/**
	 * 修改保存
	 */
	//@Log(title = "主数据-商品信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:mdProducspecs:gongjia")
	@PostMapping("/gongjiaSave")
	@ResponseBody
	public AjaxResult gongjiaSave(MdProducspecs mdProducspecs)
	{
		int updateByPrimaryKeySelective = mdProducspecsService.gongjiaSelective(mdProducspecs);
		
		return toAjax(updateByPrimaryKeySelective);
	}
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MdProducspecs mdProducspecs){
		int i=mdProducspecsService.updateVisible(mdProducspecs);
		return toAjax(i);
	}

    
    @GetMapping(value = "/exportstatementExcel")
    public void exportstatementExcel(@RequestParam("name") String name,
                                        HttpServletRequest request, HttpServletResponse response) throws ParseException {
    	MdProducspecs ppm = new MdProducspecs();
    	ppm.setProductTitle(name);
	        try {
	        	List<MdProducspecs> list= mdProducspecsService.excgetlist(ppm);
	        	
	        	
	            String[] names = {"规格ID","规格名称", "单价", "类别名称" };
	            String title = "规格报表";
	            List<String[]> objects = new ArrayList<String[]>();
	            if (null != list) {
	                for (MdProducspecs s : list) {
	                	String[] objs = new String[4];
	                	objs[0] = s.getSid()+"";
	                    objs[1] = s.getProductTitle();
	                    BigDecimal price = s.getPrice();
	                    if(price==null) {
	                    	objs[2] = "";
	                    }else {
	                    	
	                    	objs[2] = s.getPrice().toString();
	                    }
	                    objs[3] = s.getBrandName();
	                	
	                    
	                    objects.add(objs);
	                }
	            }
	            File file = ExcelUtils.exportObjectsWithoutTitle(title, names, title, objects);
	            ExportExecUtil.showExec(file, file.getName(), response);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	
}
