package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.OrderReimbursemendetail;
import com.fc.v2.service.OrderReimbursemendetailService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 报销明细表Controller
 * @ClassName: OrderReimbursemendetailController
 * @author fuce
 * @date 2022-09-18 21:31:37
 */
@Api(value = "报销明细表")
@Controller
@RequestMapping("/OrderReimbursemendetailController")
public class OrderReimbursemendetailController extends BaseController{
	
	private String prefix = "gen/orderReimbursemendetail";
	
	@Autowired
	private OrderReimbursemendetailService orderReimbursemendetailService;
	
	
	/**
	 * 报销明细表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:orderReimbursemendetail:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "报销明细表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:orderReimbursemendetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,OrderReimbursemendetail orderReimbursemendetail){
		PageInfo<OrderReimbursemendetail> page=orderReimbursemendetailService.list(tablepar,orderReimbursemendetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "报销明细表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:orderReimbursemendetail:add")
	@ResponseBody
	public AjaxResult add(OrderReimbursemendetail orderReimbursemendetail){
		int b=orderReimbursemendetailService.insertSelective(orderReimbursemendetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 报销明细表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "报销明细表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:orderReimbursemendetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=orderReimbursemendetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("OrderReimbursemendetail", orderReimbursemendetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "报销明细表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:orderReimbursemendetail:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OrderReimbursemendetail orderReimbursemendetail)
    {
        return toAjax(orderReimbursemendetailService.updateByPrimaryKeySelective(orderReimbursemendetail));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody OrderReimbursemendetail orderReimbursemendetail){
		int i=orderReimbursemendetailService.updateVisible(orderReimbursemendetail);
		return toAjax(i);
	}

    
    

	
}
