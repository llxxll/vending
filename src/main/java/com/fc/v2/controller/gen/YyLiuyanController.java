package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.YyLiuyan;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.YyLiuyanService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 医院联系我们Controller
 * @ClassName: YyLiuyanController
 * @author fuce
 * @date 2023-02-04 14:30:03
 */
@Api(value = "医院联系我们")
@Controller
@RequestMapping("/YyLiuyanController")
public class YyLiuyanController extends BaseController{
	
	private String prefix = "gen/yyLiuyan";
	
	@Autowired
	private YyLiuyanService yyLiuyanService;
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	
	/**
	 * 医院联系我们页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:yyLiuyan:view")
    public String view(ModelMap model)
    {
		TsysUser user = ShiroUtils.getUser();
		List<TsysUser> selectListByDept = planProduceMaterialDetailService.selectListByDept(null);
		model.put("userList", selectListByDept);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "医院联系我们", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:yyLiuyan:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,YyLiuyan yyLiuyan){
		PageInfo<YyLiuyan> page=yyLiuyanService.list(tablepar,yyLiuyan) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "医院联系我们新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:yyLiuyan:add")
	@ResponseBody
	public AjaxResult add(YyLiuyan yyLiuyan){
		TsysUser user = ShiroUtils.getUser();
		yyLiuyan.setCreateTime(new Date());
		yyLiuyan.setDeleteFlag(0);
		yyLiuyan.setUpdateTime(new Date());
		int b=yyLiuyanService.insertSelective(yyLiuyan);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 医院联系我们删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "医院联系我们删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:yyLiuyan:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=yyLiuyanService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("YyLiuyan", yyLiuyanService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "医院联系我们修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:yyLiuyan:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(YyLiuyan yyLiuyan)
    {
        return toAjax(yyLiuyanService.updateByPrimaryKeySelective(yyLiuyan));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody YyLiuyan yyLiuyan){
		int i=yyLiuyanService.updateVisible(yyLiuyan);
		return toAjax(i);
	}

    
    

	
}
