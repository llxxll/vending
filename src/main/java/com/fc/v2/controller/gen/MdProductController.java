package com.fc.v2.controller.gen;

import com.alibaba.fastjson.JSONObject;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.MdProduccategoryService;
import com.fc.v2.service.MdProducdetailService;
import com.fc.v2.service.MdProducspecsService;
import com.fc.v2.service.MdProductService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * 主数据-商品信息表Controller
 * @ClassName: MdProductController
 * @author fuce
 * @date 2022-05-21 23:28:15
 */
@Api(value = "主数据-商品信息表")
@Controller
@RequestMapping("/MdProductController")
public class MdProductController extends BaseController{
	
	private String prefix = "gen/mdProduct";
	
	@Autowired
	private MdProductService mdProductService;
	@Autowired
	private MdProduccategoryService mdProduccategoryService;
	@Autowired
	private MdProducspecsService mdProducspecsService;
	@Autowired
	private MdProducdetailService mdProducdetailService;
	/**
	 * 主数据-商品信息表页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:mdProduct:view")
    public String view(ModelMap model)
    {
		//获取所有类别
				List<MdProduccategory> list = mdProduccategoryService.getList(null);
				//获取所有规格
				List<MdProducspecs> list1 = mdProducspecsService.getList(null);
				model.put("mdProduccategoryList", list);
				model.put("mdProducspecsList", list1);
        return prefix + "/list";
    }
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view1")
	@RequiresPermissions("gen:mdProduct:view")
	public String view1(ModelMap model)
	{
		//获取所有类别
				List<MdProduccategory> list = mdProduccategoryService.getList(null);
				//获取所有规格
				List<MdProducspecs> list1 = mdProducspecsService.getList(null);
				model.put("mdProduccategoryList", list);
				model.put("mdProducspecsList", list1);
		return prefix + "/list1";
	}
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "主数据-商品信息表", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:mdProduct:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MdProduct mdProduct){
		PageInfo<MdProduct> page=mdProductService.getlist(tablepar,mdProduct) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		//获取所有类别
		List<MdProduccategory> list = mdProduccategoryService.getList(null);
		//获取所有规格
		List<MdProducspecs> list1 = mdProducspecsService.getList(null);
		modelMap.put("mdProduccategoryList", JSONObject.toJSON(list));
		//modelMap.put("mdProducspecsList", JSONObject.toJSON(list1));
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "主数据-商品信息表新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@RequiresPermissions("gen:mdProduct:add")
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult add(@RequestBody MdProduct mdProduct){
		int b=mdProductService.insertSelective(mdProduct);
		if(b==3) {
			return error("数据重复");
		}
		if(b==4) {
			return error("类别不为空");
		}
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 主数据-商品信息表删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "主数据-商品信息表删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:mdProduct:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=mdProductService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") String id, ModelMap map)
    {
        map.put("MdProduct", mdProductService.selectByPrimaryKey(id));
        //获取所有类别
      		List<MdProduccategory> list = mdProduccategoryService.getList(null);
      		//获取所有规格
      		List<MdProducspecs> list1 = mdProducspecsService.getList(null);
      		map.put("mdProduccategoryList", JSONObject.toJSON(list));
      		map.put("mdProducspecsList", JSONObject.toJSON(list1));
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-商品信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:mdProduct:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody MdProduct mdProduct)
    {
		int updateByPrimaryKeySelective = mdProductService.updateByPrimaryKeySelective(mdProduct);
		if(updateByPrimaryKeySelective==3) {
			return error("数据重复");
		}
        return toAjax(updateByPrimaryKeySelective);
    }
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/gongjia/{sid}")
    public String gongjia(@PathVariable("sid") String id, ModelMap map)
    {
        map.put("MdProduct", mdProductService.selectByPrimaryKey(id));
        return prefix + "/gongjia";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "主数据-商品信息表修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:mdProduct:gongjia")
    @PostMapping("/gongjia")
    @ResponseBody
    public AjaxResult gongjiaSave(@RequestBody MdProduct mdProduct)
    {
		int updateByPrimaryKeySelective = mdProductService.gongjiaSelective(mdProduct);
		
        return toAjax(updateByPrimaryKeySelective);
    }
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MdProduct mdProduct){
		int i=mdProductService.updateVisible(mdProduct);
		return toAjax(i);
	}

	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping("/upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request){
		// 文件上传路径，相对路径
		String filePath = System.getProperty("user.dir")+File.separator+"upload";
		System.out.println("文件上传路径，相对路径"+filePath);
		// 获取文件名
		String fileName = file.getOriginalFilename();

		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));

		// 解决中文问题，liunx下中文路径，图片显示问题
		fileName = UUID.randomUUID() + suffixName;

		File fileDir  = new File(filePath);
		// 检测是否存在目录
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		// 构建真实的文件路径
		File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
		try {
			file.transferTo(dest);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		fileName = "/upload/" + fileName;
		System.out.println(fileName+"文件上传成功");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key","true");
		jsonObject.put("name",fileName);
		jsonObject.put("path",fileName);
		return retobject(200,jsonObject);
	}

	@GetMapping("/getlb")
	@ResponseBody
	public List<Map> getlb(String  brandSid)
	{
		List<MdProducdetail> list = mdProducdetailService.getByBrandSid(brandSid);
		List<Map> mapList = new ArrayList<>();
		for(MdProducdetail mdProducdetail: list){
			Map map = new HashMap<>();
			List<MdProducspecs> byBrandSid = mdProducspecsService.getByBrandSid(mdProducdetail.getSpu());
			MdProduccategory mdProduccategory = mdProduccategoryService.selectByPrimaryKey(mdProducdetail.getSpu());
			map.put("spuId",mdProducdetail.getSpu());
			map.put("spuName",mdProduccategory.getProductTitle());
			map.put("spuAssembleList",byBrandSid);
			mapList.add(map);
		}
		return mapList;
	}

	@GetMapping("/getgg")
	@ResponseBody
	public List<MdProducspecs> getgg(String  brandSid)
	{
		List<MdProducspecs> list = mdProducspecsService.getByBrandSid(brandSid);
		return list;
	}

	@GetMapping("/mxlist")
	@RequiresPermissions("gen:mdProduct:view")
	public String mxlist(ModelMap model)
	{
		//获取所有类别
		List<MdProduccategory> list = mdProduccategoryService.getList(null);
		//获取所有规格
		List<MdProducspecs> list1 = mdProducspecsService.getList(null);
		model.put("mdProduccategoryList", list);
		model.put("mdProducspecsList", list1);
		return prefix + "/mxlist";
	}

	@GetMapping("/mdplist")
	@RequiresPermissions("gen:mdProduct:view")
	public String mdplist(ModelMap model)
	{
		//获取产品
		List<MdProduct> list = mdProductService.getList(null);

		model.put("mdProductList", list);
		return prefix + "/mdplist";
	}
	
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@PostMapping("/chaxunjiage")
	 @ResponseBody
    public Map<String, String>  chaxunjiage(@RequestParam(value = "productId") String productId,@RequestParam(value = "lbidsids") String lbidsids, @RequestParam(value = "gugeids") String gugeids )
    {
		Map<String, String> chaxunjiage = mdProductService.chaxunjiage(productId,lbidsids,gugeids);
        return chaxunjiage;
    }
	
}
