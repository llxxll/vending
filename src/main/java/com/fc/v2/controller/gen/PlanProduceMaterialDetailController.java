package com.fc.v2.controller.gen;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.common.domain.ResultTable;
import com.fc.v2.model.auto.JshDepot;
import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.JshMaterialCarfprice;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.model.auto.SysDepartment;
import com.fc.v2.model.auto.TsysUser;
import com.fc.v2.service.JshDepotService;
import com.fc.v2.service.JshMaterialCarfpriceService;
import com.fc.v2.service.JshMaterialService;
import com.fc.v2.service.PlanProduceMaterialDetailService;
import com.fc.v2.service.PlanWorkOrderService;
import com.fc.v2.service.WmsWarehouseDeliveryDetailService;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.ApiResultUtil;
import com.fc.v2.util.ExeclUtil;
import com.fc.v2.util.Msg;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 生产计划明细Controller
 * @ClassName: PlanProduceMaterialDetailController
 * @author fuce
 * @date 2021-12-19 20:11:36
 */
@Api(value = "生产计划明细")
@Controller
@RequestMapping("/PlanProduceMaterialDetailController")
public class PlanProduceMaterialDetailController extends BaseController{
	
	private String prefix = "gen/planProduceMaterialDetail";
	
	@Autowired
	private PlanProduceMaterialDetailService planProduceMaterialDetailService;
	@Autowired
	private JshMaterialService jshMaterialService;
	@Autowired
	private JshDepotService jshDepotService;
	@Autowired
	private JshMaterialCarfpriceService jshMaterialCarfpriceService;
	@Autowired
	private PlanWorkOrderService planWorkOrderService;
	@Autowired
	private WmsWarehouseDeliveryDetailService wmsWarehouseDeliveryDetailService;
	/**
	 * 生产计划明细页面展示
	 * @param model
	 * @return String
	 * @author fuce
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:planProduceMaterialDetail:view")
    public String view(ModelMap model)
    {
		
//		Integer x= planProduceMaterialDetailService.getCountByStatus(null);
//		Integer x0= planProduceMaterialDetailService.getCountByStatus(1);
//		Integer x1= planProduceMaterialDetailService.getCountByStatus(2);
//		Integer x2= planProduceMaterialDetailService.getCountByStatus(3);
//		model.put("count", x);
//		model.put("count0", x0);
//		model.put("count1", x1);
//		model.put("count2", x2);
		List<SysDepartment> selectSysDepartmentListByParams = planProduceMaterialDetailService.selectSysDepartmentListByParams();
		model.put("departmentList", selectSysDepartmentListByParams);
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "生产计划明细", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@RequiresPermissions("gen:planProduceMaterialDetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PlanProduceMaterialDetail planProduceMaterialDetail){
		PageInfo<PlanProduceMaterialDetail> page=planProduceMaterialDetailService.list(tablepar,planProduceMaterialDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "生产计划明细新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/addsave")
	@RequiresPermissions("gen:planProduceMaterialDetail:add")
	@ResponseBody
	public AjaxResult addsave(PlanProduceMaterialDetail planProduceMaterialDetail){
		int b=planProduceMaterialDetailService.insertSelective(planProduceMaterialDetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 生产计划明细删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "生产计划明细删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@RequiresPermissions("gen:planProduceMaterialDetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=planProduceMaterialDetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PlanProduceMaterialDetail", planProduceMaterialDetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

	/**
	 * 修改保存
	 */
	//@Log(title = "生产计划明细修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
	@RequiresPermissions("gen:planProduceMaterialDetail:edit")
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(PlanProduceMaterialDetail planProduceMaterialDetail)
	{
		return toAjax(planProduceMaterialDetailService.updateByPrimaryKeySelective(planProduceMaterialDetail));
	}
	/**
	 * 开车间工单单跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "开单跳转", notes = "开单跳转")
	@GetMapping("/bill/{id}")
	public String bill(@PathVariable("id") String id, ModelMap map)
	{
		PlanProduceMaterialDetail selectByPrimaryKey = planProduceMaterialDetailService.selectByPrimaryKey(id);
		//获取所有 仓库
		List<JshDepot> houseList = jshDepotService.getHouseList();
		//获取所有工人
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
		
		//获取物料工艺
		List<JshMaterialCarfprice> jshcarftList =jshMaterialCarfpriceService.getListByMaterid(Long.valueOf(selectByPrimaryKey.getMateriaNo()));
		
		//获取领料数
		Integer x = wmsWarehouseDeliveryDetailService.getCountbydevlied(selectByPrimaryKey.getId()+"","2");
		if(x==null) {
			x=0;
		}
		selectByPrimaryKey.setLingliaoCount(x);
		Integer number = selectByPrimaryKey.getCount();
		Integer ttt =number-x;
		if(ttt<0) {
			ttt=0;
		}
		selectByPrimaryKey.setZuidaCount(ttt);
		selectByPrimaryKey.setClName(selectByPrimaryKey.getClName()+"--"+selectByPrimaryKey.getClProperty());
		map.put("PlanProduceMaterialDetail", selectByPrimaryKey);
		map.put("houseList", houseList);
		map.put("userList", userList);
		map.put("jshcarftList", jshcarftList);

		return prefix + "/addWork";
	}
	/**
	 * 开单保存
	 */
	@ApiOperation(value = "开单保存", notes = "开单保存")
	@RequiresPermissions("gen:planProduceMaterialDetail:bill")
	@PostMapping("/bill")
	@ResponseBody
	public AjaxResult billSave(PlanProduceMaterialDetail planProduceMaterialDetail)
	{
		Integer zuidaCount = planProduceMaterialDetail.getZuidaCount();
		Integer number = planProduceMaterialDetail.getNumber();
		Integer number1 = zuidaCount-number;
		if(number1<0) {
			return error("超出最大开单数量");
		}else {
			planProduceMaterialDetail.setField2("0");
			return toAjax(planProduceMaterialDetailService.billSave(planProduceMaterialDetail));
		}
	}
	/**
	 * 开车间工单单跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "开单跳转", notes = "开单跳转")
	@GetMapping("/xialiao/{id}")
	public String xialiao(@PathVariable("id") String id, ModelMap map)
	{
		PlanProduceMaterialDetail selectByPrimaryKey = planProduceMaterialDetailService.selectByPrimaryKey(id);
		//获取所有 仓库
		List<JshDepot> houseList = jshDepotService.getHouseList();
		//获取所有工人
		Long userId = ShiroUtils.getUserId();
		TsysUser user = sysUserService.selectByPrimaryKey(userId+"");
		 List<TsysUser> userList =planProduceMaterialDetailService.selectListByDept(user.getDepId());
		
		//获取物料工艺
		List<JshMaterialCarfprice> jshcarftList =jshMaterialCarfpriceService.getListByMaterid(Long.valueOf(selectByPrimaryKey.getMateriaNo()));
		
		//获取下料单数量
		Integer x = planWorkOrderService.getCountbydevlied(selectByPrimaryKey.getId()+"","1");
		if(x==null) {
			x=0;
		}
		selectByPrimaryKey.setLingliaoCount(x);
		Integer number = selectByPrimaryKey.getCount();
		selectByPrimaryKey.setZuidaCount(number-x);
		selectByPrimaryKey.setClName(selectByPrimaryKey.getClName()+"--"+selectByPrimaryKey.getClProperty());
		map.put("PlanProduceMaterialDetail", selectByPrimaryKey);
		map.put("houseList", houseList);
		map.put("userList", userList);
		map.put("jshcarftList", jshcarftList);

		return prefix + "/xialiao";
	}
	/**
	 * 开单保存
	 */
	@ApiOperation(value = "开单保存", notes = "开单保存")
	@RequiresPermissions("gen:planProduceMaterialDetail:xialiao")
	@PostMapping("/billxialiao")
	@ResponseBody
	public AjaxResult billxialiao(PlanProduceMaterialDetail planProduceMaterialDetail)
	{
		Integer zuidaCount = planProduceMaterialDetail.getZuidaCount();
		Integer number = planProduceMaterialDetail.getNumber();
		Integer number1 = zuidaCount-number;
		if(number1<0) {
			return error("超出最大开单数量");
		}else {
			planProduceMaterialDetail.setField2("1");
			return toAjax(planProduceMaterialDetailService.billSave(planProduceMaterialDetail));
		}
	}
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PlanProduceMaterialDetail planProduceMaterialDetail){
		int i=planProduceMaterialDetailService.updateVisible(planProduceMaterialDetail);
		return toAjax(i);
	}

	/**
	 * 查询物料片区下拉表
	 */
	@ApiOperation(value = "查询物料片区下拉表", notes = "查询物料片区下拉表")
	@GetMapping("/selectMateriaList")
	@RequiresPermissions("gen:planProduceMaterialDetail:list")
	@ResponseBody
	public Msg selectMateriaList (Tablepar tablepar, JshMaterial jshMaterial)
	{
		jshMaterial.setCategoryId(1l);
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ;
		return ApiResultUtil.success(page.getList(),page.getTotal());
	}
	/**
	 * 查询材料片区下拉表
	 * @return
	 */
	@ApiOperation(value = "查询材料片区下拉表", notes = "查询材料片区下拉表")
	@GetMapping("/selectStandardList")
	@RequiresPermissions("gen:planProduceMaterialDetail:list")
	@ResponseBody
	public Msg selectStandardList (Tablepar tablepar, JshMaterial jshMaterial)
	{
		jshMaterial.setCategoryId(2l);
		PageInfo<JshMaterial> page=jshMaterialService.list(tablepar,jshMaterial) ;
		return ApiResultUtil.success(page.getList(),page.getTotal());
	}


	
}
