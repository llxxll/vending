package com.fc.v2.controller.yiyuan;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fc.V2Application;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.common.domain.AjaxResult;
import com.fc.v2.controller.gen.YyDangjianController;
import com.fc.v2.controller.gen.YyGonggaoController;
import com.fc.v2.controller.gen.YyNewsController;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.Submit;
import com.fc.v2.model.auto.*;
import com.fc.v2.model.custom.Tablepar;
import com.fc.v2.service.*;
import com.fc.v2.util.*;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.fc.v2.util.HttpClientUtil.*;

@Api(tags = "前端模块")
@RestController
@RequestMapping("/yiyuan")
public class YiyuanController extends BaseController {

    @Autowired
    private YyNewsService yyNewsService;
    @Autowired
    private YyDangjianController yyDangjianController;
    @Autowired
    private YyGonggaoController yyGonggaoController;
    @Autowired
    private YyNewsController yyNewsController;
    @Autowired
    private YyGonggaoService yyGonggaoService;
    @Autowired
    private YyDangjianService yyDangjianService;
    @Autowired
    private YyLunbotuService yyLunbotuService;
    @Autowired
    private YyZhaopinService yyZhaopinService;

    @Autowired
    private YyLiuyanService yyLiuyanService;

    @Autowired
    private YyZhuanjiaService yyZhuanjiaService;

    @Autowired
    private YyKeshiService yyKeshiService;
    @Autowired
    YyJiuyizhinanService yyJiuyizhinanService;
    @Autowired
    YyYiyuangaikuangService yyYiyuangaikuangService;
    @Autowired
    YyJiankangzhishiService yyJiankangzhishiService;
    @Autowired
    SysDictDataService tSysDictDataService;
    @Autowired
    private YyDetailService yyDetailService;
    @Autowired
    YyNewsTypeService yyNewsTypeService;
    @Autowired
    YyJieshaoService yyJieshaoService;
    @Resource
    private OrderService orderService;
    @Resource
    private WxPayService wxPayService;
    @Resource
    private AliPayService aliPayService;
    @Resource
    private OrderBillService orderBillService;

    protected static final Log LOG = LogFactory.getLog(YiyuanController.class);
    private static final String hisip = "http://172.64.88.220:9100/mhwzserver?loginname=mhwz&service=";
    private static final String organizationId = "46706958-9";
    private static final String outpatientType = "1";


    /**
     * 医院信息获取
     */
    @GetMapping("/yiyuanxx")
    @ResponseBody
    public ApiResult yiyuanxx() throws Exception{
        LOG.info("医院信息获取开始");
        String url = hisip+"";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("outpatientType",outpatientType);
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("医院信息获取返回:"+s);
        System.out.println("医院信息获取返回:"+s);
        Map<String, Object> data = new HashMap<>();
        data.put("data", s);
        return ResultUtils.ok(data);
    }

    /**
     * 科室信息获取
     */
    @GetMapping("/listDepartment")
    @ResponseBody
    public ApiResult listDepartment() throws Exception{
        LOG.info("科室信息获取开始");
        String url = hisip+"listDepartment";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("outpatientType",outpatientType);
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("科室信息获取返回his："+s);
        System.out.println("科室信息获取返回his:"+s);
        //改变

        List<Object> listok = new ArrayList<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        if ("200".equals(jsonMap.get("code"))) {
            List<Object> data1 = (List<Object>) jsonMap.get("data");
            for (Object obj : data1){
                Map<String, Object> mapok = new HashMap<>();
                Map map = (Map) obj;
                mapok.put("departmentId", map.get("parentDepartmentId"));
                mapok.put("departmentName", map.get("parentDepartmentName"));
                mapok.put("departmentList", map.get("departmentList"));
                listok.add(mapok);
            }
        }
        Map<String, Object> data = new HashMap<>();
        data.put("data", listok);
        return ResultUtils.ok(data);
    }



    /**
     * 医生信息获取
     */
    @GetMapping("/listDoctor")
    @ResponseBody
    public ApiResult listDoctor(String departmentId,String scheduleDate,String doctorId,String doctorType, HttpServletRequest request) throws Exception{
        LOG.info("医生信息获取开始");
        String url = hisip+"listDoctor";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("departmentId",departmentId);//科室id
        jo.put("scheduleDate",scheduleDate);//yyyy-MM-dd HH:mm:ss  时间为0
        jo.put("doctorId",doctorId);//医生代码
        jo.put("doctorType",doctorType);//参照字典 doctorType
        jo.put("outpatientType",outpatientType);//参照字典outpatientType
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("医生信息获取返回："+s);
        System.out.println("医生信息获取返回:"+s);
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        //时间
        DataTest dataTest = new DataTest();
        List<Map>  dates = dataTest.dates(8);
        if ("200".equals(jsonMap.get("code"))) {
            List<Object> data1 = (List<Object>) jsonMap.get("data");
            List listnew = new ArrayList();
            for (Object obj : data1){
                Map map = (Map) obj;
                List<Map> listSchedule = (List<Map>) map.get("listSchedule");
                for(Map m :dates){
                    Map mnew = new HashMap();
                    mnew.put("year", m.get("year"));
                    mnew.put("day", m.get("day"));
                    mnew.put("zhou", m.get("zhou"));
                    List<Map> amPmResults = new ArrayList<>();
                    Map mnewsw = new HashMap();
                    mnewsw.put("type", "上午");
                    mnewsw.put("list", "");
                    Map mnewxw = new HashMap();
                    mnewxw.put("type", "下午");
                    mnewxw.put("list", "");
                    for(Map m2:listSchedule){
                        String year = (String) m.get("year");
                        String scheduleDate1 = (String) m2.get("scheduleDate");

                        if(scheduleDate1.contains(year)){
                            if("1".equals(m2.get("periodId"))){
                                Map mnew2 = new HashMap();
                                mnew2.put("type", "上午");
                                mnew2.put("periodId", m2.get("periodId"));
                                mnew2.put("resourceId", m2.get("resourceId"));
                                mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
                                SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String fabuData = sdf.format(new Date((String) m2.get("scheduleDate")));
                                mnew2.put("scheduleDate", fabuData);
                                mnew2.put("orderCount", m2.get("orderCount"));
                                mnew2.put("admitAddress", m2.get("admitAddress"));
                                mnew2.put("sourceState", m2.get("sourceState"));
                                mnewsw.put("list", mnew2);
                            }
                            if("2".equals(m2.get("periodId"))){
                                Map mnew2 = new HashMap();
                                mnew2.put("type", "下午");
                                mnew2.put("periodId", m2.get("periodId"));
                                mnew2.put("resourceId", m2.get("resourceId"));
                                mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
                                SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String fabuData = sdf.format(new Date((String) m2.get("scheduleDate")));
                                mnew2.put("scheduleDate", fabuData);
                                mnew2.put("orderCount", m2.get("orderCount"));
                                mnew2.put("admitAddress", m2.get("admitAddress"));
                                mnew2.put("sourceState", m2.get("sourceState"));
                                mnewxw.put("list", mnew2);
                            }
                        }

                    }
                    amPmResults.add(mnewsw);
                    amPmResults.add(mnewxw);
                    mnew.put("amPmResults", amPmResults);
                    listnew.add(mnew);
                }
                map.put("listSchedule",listnew);
                String durl = upload((String)map.get("doctorId"),request);
                map.put("durl",durl);
            }
        }
        Map<String, Object> data = new HashMap<>();
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    public String upload(String fileName, HttpServletRequest request){
        // 文件上传路径，相对路径
        fileName = fileName + ".jpg";
        String filePath = System.getProperty("user.dir")+File.separator+"upload";
        System.out.println("文件检查"+filePath+fileName);
        File fileDir  = new File(filePath);
        // 检测是否存在目录
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        // 构建真实的文件路径
        File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
        if (dest.exists()) {
            fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/" + fileName;
            System.out.println("文件检查ok"+fileName);
            return fileName;
        }else{
            fileName = "http://"+request.getServerName()+":"+request.getServerPort()+"/upload/000.jpg";
            return fileName;
        }

    }
    /**
     * 医生信息获取bydata
     */
    @GetMapping("/listDoctorbydata")
    @ResponseBody
    public ApiResult listDoctorbydata(String departmentId,String scheduleDate,String doctorId,String doctorType) throws Exception{
        LOG.info("医生信息获取bydata");
        String url = hisip+"listDoctor";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("departmentId",departmentId);//科室id
        jo.put("scheduleDate",scheduleDate);//yyyy-MM-dd HH:mm:ss  时间为0
        jo.put("doctorId",doctorId);//医生代码
        jo.put("doctorType",doctorType);//参照字典 doctorType
        jo.put("outpatientType",outpatientType);//参照字典outpatientType
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("医生信息获取bydata："+s);
        System.out.println("医生信息获取bydata:"+s);
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        Map sw = new HashMap();
        Map xw = new HashMap();
        List listnew = new ArrayList();
        List swlist = new ArrayList();
        List xwlist = new ArrayList();
        //上午下午
        if ("200".equals(jsonMap.get("code"))) {
            List<Object> data1 = (List<Object>) jsonMap.get("data");
            for (Object obj : data1){
                Map map = (Map) obj;

                List<Map> listSchedule = (List<Map>) map.get("listSchedule");
                if(listSchedule.size()>0){

                    for(Map m2:listSchedule){

                        if("1".equals(m2.get("periodId"))){
                            Map mnew = new HashMap();
                            mnew.put("departmentId", map.get("departmentId"));
                            mnew.put("departmentName", map.get("departmentName"));
                            mnew.put("flag", map.get("flag"));
                            mnew.put("doctorSex", map.get("doctorSex"));
                            mnew.put("keyWord", map.get("keyWord"));
                            mnew.put("organizationId", map.get("organizationId"));
                            mnew.put("workRankId", map.get("workRankId"));
                            mnew.put("doctorName", map.get("doctorName"));
                            mnew.put("doctorInformation", map.get("doctorInformation"));
                            mnew.put("doctorJob", map.get("doctorJob"));
                            mnew.put("doctorId", map.get("doctorId"));
                            mnew.put("regFee", map.get("regFee"));
                            mnew.put("doctorGoodAt", map.get("doctorGoodAt"));
                            mnew.put("majorQualify", map.get("majorQualify"));
                            List linew = new ArrayList();
                            Map mnew2 = new HashMap();
                            mnew2.put("periodId", m2.get("periodId"));
                            mnew2.put("resourceId", m2.get("resourceId"));
                            mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
                            SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String fabuData = sdf.format(new Date((String) m2.get("scheduleDate")));
                            mnew2.put("scheduleDate", fabuData);
                            mnew2.put("orderCount", m2.get("orderCount"));
                            mnew2.put("admitAddress", m2.get("admitAddress"));
                            mnew2.put("sourceState", m2.get("sourceState"));
                            linew.add(mnew2);
                            mnew.put("listSchedule", linew);
                            swlist.add(mnew);
                        }
                        if("2".equals(m2.get("periodId"))){
                            Map mnew = new HashMap();
                            mnew.put("departmentId", map.get("departmentId"));
                            mnew.put("departmentName", map.get("departmentName"));
                            mnew.put("flag", map.get("flag"));
                            mnew.put("doctorSex", map.get("doctorSex"));
                            mnew.put("keyWord", map.get("keyWord"));
                            mnew.put("organizationId", map.get("organizationId"));
                            mnew.put("workRankId", map.get("workRankId"));
                            mnew.put("doctorName", map.get("doctorName"));
                            mnew.put("doctorInformation", map.get("doctorInformation"));
                            mnew.put("doctorJob", map.get("doctorJob"));
                            mnew.put("doctorId", map.get("doctorId"));
                            mnew.put("regFee", map.get("regFee"));
                            mnew.put("doctorGoodAt", map.get("doctorGoodAt"));
                            mnew.put("majorQualify", map.get("majorQualify"));
                            List linew = new ArrayList();
                            Map mnew2 = new HashMap();
                            mnew2.put("periodId", m2.get("periodId"));
                            mnew2.put("resourceId", m2.get("resourceId"));
                            mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
                            SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String fabuData = sdf.format(new Date((String) m2.get("scheduleDate")));
                            mnew2.put("scheduleDate", fabuData);
                            mnew2.put("orderCount", m2.get("orderCount"));
                            mnew2.put("admitAddress", m2.get("admitAddress"));
                            mnew2.put("sourceState", m2.get("sourceState"));
                            linew.add(mnew2);
                            mnew.put("listSchedule", linew);
                            xwlist.add(mnew);
                        }
                    }
                }
            }
            sw.put("type", "上午");
            sw.put("list", swlist);
            xw.put("type", "下午");
            xw.put("list", xwlist);
        }
        listnew.add(sw);
        listnew.add(xw);
        Map<String, Object> data = new HashMap<>();
        data.put("data", listnew);
        return ResultUtils.ok(data);
    }
    /**
     *  按日期查询挂号科室
     */
    @GetMapping("/listDepartmentBydate")
    @ResponseBody
    public ApiResult listDepartmentBydate(String scheduleDate) throws Exception{
        LOG.info("按日期查询挂号科室");
        String url = hisip+"listDepartmentBydate";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("scheduleDate",scheduleDate);//排班日期 yyyy-mm-dd
        jo.put("outpatientType",outpatientType);//参照字典outpatientType
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("按日期查询挂号科室："+s);
        System.out.println("按日期查询挂号科室:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    /**
     *  排班信息获取
     */
    @GetMapping("/listNumber")
    @ResponseBody
    public ApiResult listNumber(String departmentId,String scheduleDate,String doctorId,String periodId) throws Exception{
        LOG.info("排班信息获取开始");
        String url = hisip+"listNumber";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("departmentId",departmentId);//科室id
        jo.put("scheduleDate",scheduleDate);//出诊日期（格式：yyyy-MM-dd hh:mm:ss）空则返回指定科室下的所有有效的排班。
        jo.put("doctorId",doctorId);//医生代码
        jo.put("periodId",periodId);//值班类别 1上午   2下午
        jo.put("outpatientType",outpatientType);//参照字典outpatientType
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("排班信息获取返回："+s);
        System.out.println("排班信息获取返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }

    /**
     * 预约提交
     */
    @GetMapping("/confirmAppointment")
    @ResponseBody
    public ApiResult confirmAppointment(String regFee,String address,String patientMedicalCardNumber,String patientMedicalCardType,String appointmentType,String departmentName,
                                        String patientMobile,String patientName,
                                        String resourceId,String departmentId,String doctorId, String cardNumber, String patientSex, String workId, String scheduleDate,String periodId) throws Exception{
        LOG.info("预约提交开始");
        String url = hisip+"confirmAppointment";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);

        jo.put("appointmentType",appointmentType);//预约类型
        jo.put("departmentName",departmentName);//科室名称
        jo.put("patientMobile",patientMobile);//病人手机号
        jo.put("patientName",patientName);//病人姓名
        jo.put("resourceId",resourceId);//号源序号
        jo.put("departmentId",departmentId);//科室id
        jo.put("doctorId",doctorId);//医生代码
        jo.put("workId",workId);//排班序号
        jo.put("patientMedicalCardType",patientMedicalCardType);
        jo.put("patientMedicalCardNumber",patientMedicalCardNumber);
        jo.put("regFee",regFee);//费用
        //就诊人判断
        String patientId = checkUserMsg(address,patientMedicalCardNumber,patientMedicalCardType,patientMobile ,cardNumber, patientName ,patientSex);
        if(StringUtils.isNotEmpty(patientId)){
            jo.put("patientId",patientId);
        }
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("预约提交返回："+s);
        System.out.println("预约提交返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        if("200".equals(jsonMap.get("code"))){
            System.out.println("预约成功，预约挂号预结算");
            List list = (List) jsonMap.get("data");
            Map<String,Object> map = (Map<String, Object>) list.get(0);
            String outTradeNo = (String)map.get("orderId") +"_"+doctorId+"_"+departmentId+"_"+resourceId+"_"+ RandomUtil.randomNum(10);
            Map<String, Object> objectMap =paymentBudget( outTradeNo,  patientId,  (String)map.get("orderId"),  departmentId,  doctorId,  patientName,  scheduleDate,  periodId);
            if("200".equals(objectMap.get("code"))){
                System.out.println("预约成功-》预约挂号预结算-成功："+JSONObject.toJSONString(objectMap));
                System.out.println("预约提交后插入order");
                List paymentBudgetlist = (List) objectMap.get("data");
                Map<String,Object> paymentBudgetmap = (Map<String, Object>) paymentBudgetlist.get(0);
                Order order = new Order();
                String op = paymentBudgetmap.get("payAmount").toString();
                BigDecimal decimal = new BigDecimal(op);
                int result = decimal.intValue();
                order.setTotalFee(result);
                order.setOutTradeNo(outTradeNo);

                //order.setOrderId(Long.valueOf((String) map.get("hisOrderNumber")));
                //		"orderId": "7607",
                //		"resourceId": "195562",
                String orderid = (String)map.get("orderId") + (String)map.get("resourceId");
                System.out.println("预约提交后插入orderId："+orderid);
                order.setOrderId(Long.valueOf(orderid));
                order.setCreateTime(new Date());
                order.setRemark(patientName);
                order.setUserId(Long.valueOf(patientId));
                order.setConsignee(patientMedicalCardNumber);
                order.setCreateIp((String) paymentBudgetmap.get("paymentBudgetNumber"));
                System.out.println("预约提交后插入order信息："+JSONObject.toJSONString(order));

                orderService.insertSelective(order);
                System.out.println("预约提交后插入order成功，返回前端");
                jsonMap.put("orderId", orderid);
            }
            data.put("data", jsonMap);
            data.put("data2", objectMap);
            System.out.println("预约提交后插入order成功，返回前端："+JSONObject.toJSONString(data));
            return ResultUtils.ok(data);

        }else{
            data.put("data", jsonMap);
            data.put("data2", null);
            System.out.println("预约失败："+JSONObject.toJSONString(jsonMap));
            return ResultUtils.ok(data);
        }
    }

        /**
         * 4.1.7预约取消
         */
    @GetMapping("/appointmentPayCancel")
    @ResponseBody
    public ApiResult appointmentPayCancel(String orderId,String patientId,String patientName) throws Exception{
        LOG.info("预约取消开始");
        String url = hisip+"appointmentPayCancel";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("orderId",orderId);//预约流水号
        jo.put("patientId",patientId);//
        jo.put("patientName",patientName);//病人姓名
        jo.put("outpatientType","1");
        //paymentBudgetNumber 处理失败未传入预结算流水号
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("预约取消返回："+s);
        System.out.println("预约取消返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    /**
     * 4.1.6预约列表获取
     */
    @GetMapping("/listAppointmentWait")
    @ResponseBody
    public ApiResult listAppointmentWait(String queryType, String patientId, String BeginTime,  String EndTime,  String patientMedicalCardNumber) throws Exception{
        LOG.info("预约列表获取");
        System.out.println("预约列表获取");
        String url = hisip+"listAppointmentWait";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("EndTime",EndTime);
        jo.put("BeginTime",BeginTime);
        jo.put("queryType",queryType);//查询类型，参数设置，1表示按患者患者标识集合查询；2表示按证件集合查询；3表示按诊疗卡集合查询
        //patientIdList
        Map<String, Object>  patientIdMap = new HashMap<>();
        patientIdMap.put("patientId",patientId);
        patientIdMap.put("patientMedicalCardNumber",patientMedicalCardNumber);
        List patientIdList = new ArrayList();
        patientIdList.add(patientIdMap);
        jo.put("patientIdList",patientIdList);
        System.out.println("预约列表获取url"+url);
        System.out.println("预约列表获取参数："+JSONObject.toJSONString(jo));
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("预约列表获取提交返回："+s);
        System.out.println("预约列表获取返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    /**
     * 4.1.8预约挂号预结算
     */
    @GetMapping("/paymentBudget")
    @ResponseBody
    public Map<String, Object> paymentBudget(String hisOrderNumber, String patientId, String orderId, String departmentId, String doctorId, String patientName, String scheduleDate, String periodId) throws Exception{
        LOG.info("预约挂号预结算");
        System.out.println("预约挂号预结算");
        String url = hisip+"paymentBudget";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("hisOrderNumber",hisOrderNumber);//预约流水号
        jo.put("patientId",patientId);//病人id
        jo.put("orderId",orderId);//病人id
        jo.put("departmentId",departmentId);//病人id
        jo.put("doctorId",doctorId);//病人idpatientName
        jo.put("patientName",patientName);//
        jo.put("scheduleDate",scheduleDate);//
        jo.put("periodId",periodId);//

        //处理失败未传入排班日期【scheduleDate】
        System.out.println("预约挂号预结算url"+url);
        System.out.println("预约挂号预结算参数："+JSONObject.toJSONString(jo));
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("预约挂号预结算提交返回："+s);
        System.out.println("预约挂号预结算返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return jsonMap;
    }

    /**
     * 4.1.9预约挂号支付
     */
    @GetMapping("/appointmentPay")
    @ResponseBody
    public ApiResult appointmentPay(String outOrderNumber,String insuplc_admdvs,
                                        String patientId,String payTime,
                                        String payMode,String med_type,String mdtrt_cert_no, String insutype, String paymentBudgetNumber, Long orderId, String payAmount) throws Exception{
        LOG.info("预约挂号支付");
        System.out.println("预约挂号支付");
        String url = hisip+"appointmentPay";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("outOrderNumber",outOrderNumber);//外部流水号
        jo.put("insuplc_admdvs",insuplc_admdvs);//统筹区号
        jo.put("patientId",patientId);//病人id
        jo.put("payTime",payTime);//支付时间
        jo.put("payMode",payMode);//支付方式1-支付宝/2-微信
        jo.put("med_type",med_type);//
        jo.put("mdtrt_cert_no",mdtrt_cert_no);//身份证号
        jo.put("insutype",insutype);//医保类型
        jo.put("paymentBudgetNumber",paymentBudgetNumber);
        jo.put("agtOrderNumber",outOrderNumber);
        jo.put("MachineNo","MHWZ");
        Double d= Double.parseDouble(payAmount);
        DecimalFormat df = new DecimalFormat("0.00");
        String number = df.format(d);
        Double ddnumber= Double.parseDouble(number);
        jo.put("payAmount",ddnumber);

        System.out.println("预约挂号支付url"+url);
        System.out.println("预约挂号支付参数："+JSONObject.toJSONString(jo));
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("预约提交返回："+s);
        System.out.println("预约提交返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        if("200".equals(jsonMap.get("code"))){
            System.out.println("预约挂号支付成功");
        }else{
            System.out.println("预约挂号支付失败");
            //退款
            if("1".equals(payMode )){
                aliPayService.aliRefund2(orderId, null);
            }
            if("2".equals(payMode )){
                wxPayService.wxRefund2(orderId);
            }

        }
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    @GetMapping("/tuikuan")
    @ResponseBody
    public ApiResult appointmentPay(String outOrderNumber,Integer type) throws Exception {
        try {
            if(1 == type){
                aliPayService.aliRefundByoutOrderNumber(outOrderNumber);
            }
            else if(2 == type){
                wxPayService.wxRefundByoutOrderNumber(outOrderNumber);
            }
            return ResultUtils.ok("成功");
        }catch (Exception e){
            return ResultUtils.ok(e.getMessage());
        }

    }
    @GetMapping("/checkUserMsg")
    @ResponseBody
    public ApiResult checkUserMsgtoappointmentPay(String address,String patientMedicalCardNumber,String patientMedicalCardType,String operationType,String patientMobile, String cardNumber, String patientName, String patientSex ) throws Exception {
        LOG.info("患者信息认证开始");
        String url = hisip+"getPhoneAndCards";
        String patientId = "";
        //params.put("sourceType",2);
        //params.put("idCard",idCard);
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("operationType",operationType);
        jo.put("patientMobile",patientMobile);
        jo.put("cardType","01");//证件类型
        jo.put("cardNumber",cardNumber);//证件号码
        jo.put("patientName",patientName);//患者姓名
        jo.put("patientSex",patientSex);//患者性别1	男性 2	女性
        jo.put("patientMedicalCardType",patientMedicalCardType);
        jo.put("patientMedicalCardNumber",patientMedicalCardNumber);
        jo.put("address",address);
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("患者信息认证返回："+s);
        System.out.println("患者信息认证返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    public String checkUserMsg(String address,String patientMedicalCardNumber,String patientMedicalCardType, String patientMobile, String cardNumber, String patientName, String patientSex ) throws Exception {
        LOG.info("患者信息认证开始1");
        String url = hisip+"getPhoneAndCards";
        String patientId = "";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("patientMobile",patientMobile);
        //params.put("sourceType",2);
        jo.put("cardType","01");//证件类型
        jo.put("cardNumber",cardNumber);//证件号码
        jo.put("patientName",patientName);//患者姓名
        jo.put("patientSex",patientSex);//患者性别1	男性 2	女性
        jo.put("patientMedicalCardType",patientMedicalCardType);
        jo.put("patientMedicalCardNumber",patientMedicalCardNumber);
        jo.put("operationType","1");
        jo.put("address",address);
        //params.put("idCard",idCard);
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("患者信息认证返回1："+s);
        System.out.println("患者信息认证返回1:"+s);
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        if("200".equals(jsonMap.get("code"))){
            List<Object> data1 = (List<Object>) jsonMap.get("data");
            for (Object obj : data1) {
                Map map = (Map) obj;
                List<Map> listSchedule = (List<Map>) map.get("listMedicalCard");
                for(Map m2:listSchedule) {
                    patientId = (String) m2.get("patientId");
                }
            }
        }
        System.out.println("患者信息认证返回1patientId:"+patientId);
        return patientId;
    }
    /**
     * 获取号源信息
     */
    @GetMapping("/getmedtype")
    @ResponseBody
    public ApiResult getmedtype(String departmentId,String scheduleDate,String doctorId,String periodId) throws Exception{
        LOG.info("获取号源信息开始");
        TSysDictData tSysDictData = tSysDictDataService.getHisIp();
        String url = tSysDictData.getDictValue()+"getmedtype";
        TSysDictData getorganizationId = tSysDictDataService.organizationId();
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",getorganizationId.getDictValue());
        jo.put("departmentId",departmentId);//科室id
        jo.put("scheduleDate",scheduleDate);//出诊日期（格式：yyyy-MM-dd hh:mm:ss）空则返回指定科室下的所有有效的排班。
        jo.put("doctorId",doctorId);//医生代码
        jo.put("periodId",periodId);//值班类别 1上午   2下午
        jo.put("outpatientType",outpatientType);//参照字典outpatientType
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("获取号源信息返回："+s);
        System.out.println("获取号源信息返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    @GetMapping("/getcode")
    @ResponseBody
    public ApiResult getcode(String address,String patientMedicalCardNumber,String patientMedicalCardType, String patientMobile, String cardNumber, String patientName, String patientSex) throws Exception {
        LOG.info("患者信息认证开始");
        String url = hisip+"getPhoneAndCards";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("patientMobile",patientMobile);
        //jo.put("sourceType",2);
        jo.put("cardType","01");//证件类型
        jo.put("cardNumber",cardNumber);//证件号码
        jo.put("patientName",patientName);//患者姓名
        jo.put("patientSex",patientSex);//患者性别1	男性 2	女性
        jo.put("patientMedicalCardType",patientMedicalCardType);
        jo.put("patientMedicalCardNumber",patientMedicalCardNumber);
        jo.put("operationType","1");
        jo.put("address",address);
        //params.put("idCard",idCard);
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("患者信息认证返回："+s);
        System.out.println("患者信息认证返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        if("200".equals(jsonMap.get("code"))){
            data.put("code", 200);
            List<Object> data1 = (List<Object>) jsonMap.get("data");
            Map map = (Map) data1.get(0);
            List<Map> listSchedule = (List<Map>) map.get("listMedicalCard");
            Map map2 = listSchedule.get(0);
            String patientId = (String) map2.get("patientId");
            String phoneNumber = (String) map2.get("patientMobile");
            if(StringUtils.isNotEmpty(phoneNumber)){
                String patientName2 = (String) map2.get("patientName");
                System.out.println("短信发送手机号："+phoneNumber);
                ApiResult apiResult = sendMsg(phoneNumber, patientMedicalCardNumber, patientName2);
                if(apiResult.getStatus() == 200){
                    String hidePhoneNumber = phoneNumber.substring(0, 3) + "****" + phoneNumber.substring(7);
                    data.put("checkMsg", hidePhoneNumber);
                    data.put("patientId", patientId);
                }
            }else{
                data.put("code", 202);
                data.put("checkMsg", "用户没有绑定手机号");
            }

        }else{
            data.put("code", 201);
            data.put("checkMsg", jsonMap.get("message"));
        }
        return ResultUtils.ok(data);
    }

    /**
     * 4.3.1检验报告列表
     */
    @GetMapping("/getInspectionReport")
    @ResponseBody
    public ApiResult getInspectionReport(String patientId,String cardNumber, String patientMobile,  String patientName,  String code) throws Exception{
        LOG.info("检验报告列表开始");
        ApiResult apiResult = checkMsg(null, cardNumber ,code);
        LOG.info("验证返回信息："+JSONObject.toJSONString(apiResult));
        Map map = (Map) apiResult.getData();
        if("true".equals(map.get("checkMsg"))){
            LOG.info("验证成功，开始查询");
            String url = hisip+"getInspectionReport";
            RestTemplate restTemplate = new RestTemplate();
            JSONObject jo = new JSONObject();
            jo.put("organizationId",organizationId);
            jo.put("patientId",patientId);//病人id
            jo.put("source","1");//
            jo.put("cardType","01");//
            jo.put("cardNumber",cardNumber);//
            jo.put("patientMobile",patientMobile);//
            jo.put("patientName",patientName);//
//        if(StringUtils.isNotEmpty(patientId)){
//            patientId = checkUserMsg(null,cardNumber,"02",patientMobile ,cardNumber, patientName ,null);
//            params.put("patientId",patientId);
//        }
            String s = restTemplate.postForObject(url, jo, String.class);
            LOG.info("检验报告列表返回："+s);
            System.out.println("检验报告列表返回:"+s);
            Map<String, Object> data = new HashMap<>();
            Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
            data.put("data", jsonMap);
            return ResultUtils.ok(data);
        }else{
            LOG.info("验证失败");
            return apiResult;
        }

    }
    /**
     * 4.3.2检验报告详情
     */
    @GetMapping("/listInspectionReport")
    @ResponseBody
    public ApiResult listInspectionReport(String odsId) throws Exception{
        LOG.info("检验报告详情开始");
        String url = hisip+"listInspectionReport";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("odsId",odsId);//
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("检验报告详情返回："+s);
        System.out.println("检验报告详情返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }

    /**
     * 4.3.3检查报告列表
     */
    @GetMapping("/getCheckReport")
    @ResponseBody
    public ApiResult getCheckReport(String patientId,String cardNumber, String patientMobile,  String patientName,  String code) throws Exception{
        LOG.info("检查报告列表开始");
        ApiResult apiResult = checkMsg(null, cardNumber ,code);
        LOG.info("验证返回信息："+JSONObject.toJSONString(apiResult));
        Map map = (Map) apiResult.getData();
        if("true".equals(map.get("checkMsg"))){
            LOG.info("验证成功，开始查询");
            String url = hisip+"getCheckReport";
            RestTemplate restTemplate = new RestTemplate();
            JSONObject jo = new JSONObject();
            jo.put("organizationId",organizationId);
            jo.put("patientId",patientId);//病人id
            jo.put("source","1");//
            jo.put("cardType","01");//
            jo.put("cardNumber",cardNumber);//
            jo.put("patientMobile",patientMobile);//
            jo.put("patientName",patientName);//
//        if(StringUtils.isNotEmpty(patientId)){
//            patientId = checkUserMsg(null,cardNumber,"02",patientMobile ,cardNumber, patientName ,null);
//            params.put("patientId",patientId);
//        }
            String s = restTemplate.postForObject(url, jo, String.class);
            LOG.info("检查报告列表返回："+s);
            System.out.println("检查报告列表返回:"+s);
            Map<String, Object> data = new HashMap<>();
            Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
            data.put("data", jsonMap);
            return ResultUtils.ok(data);
        }else{
            return apiResult;
        }

    }

    /**
     * 4.3.4检查报告详情
     */
    @GetMapping("/listCheckReport")
    @ResponseBody
    public ApiResult listCheckReport(String odsId) throws Exception{
        LOG.info("检查报告详情开始");
        String url = hisip+"listCheckReport";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("odsId",odsId);//病人id
        String s = restTemplate.postForObject(url, jo, String.class);
        LOG.info("检查报告详情返回："+s);
        System.out.println("检查报告详情返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    ////=================统一支付+++++++++++++++




    @GetMapping("/getInspectionReport1")
    @ResponseBody
    public ApiResult getInspectionReport1(String patientId,String cardNumber, String patientMobile,  String patientName) throws Exception{
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> data2 = new HashMap<>();
        data2.put("odsId","123");
        data2.put("orgName","123");
        data2.put("type","生化");
        data2.put("reportDt","门诊");
        data2.put("reportSource","门诊");
        data2.put("reportDt","reportSourceText");
        List<Map> list = new ArrayList<>();
        list.add(data2);
        data.put("code", "200");
        data.put("message", "");
        data.put("data", list);
        return ResultUtils.ok(data);
    }
    /**
     * 4.3.2检验报告详情
     */
    @GetMapping("/listInspectionReport1")
    @ResponseBody
    public ApiResult listInspectionReport1(String odsId) throws Exception{
        LOG.info("检验报告详情开始");
        String url = hisip+"listInspectionReport";
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> data2 = new HashMap<>();
        data2.put("mainLabName","123");
        data2.put("orgName","2022-07-31 02:38:33");
        data2.put("rpOrgName","生化");
        data2.put("createDt","门诊");
        data2.put("reportDt","门诊");
        data2.put("labName","门诊");
        data2.put("result","门诊");
        data2.put("resultUnit","门诊");
        data2.put("referUpper","门诊");
        data.put("code", "200");
        data.put("message", "");
        data.put("data", data2);
        return ResultUtils.ok(data);
    }

    /**
     * 4.3.3检查报告列表
     */
    @GetMapping("/getCheckReport1")
    @ResponseBody
    public ApiResult getCheckReport1(String patientId,String cardNumber, String patientMobile,  String patientName) throws Exception{
        LOG.info("检查报告列表开始");
        String url = hisip+"getCheckReport";
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> data2 = new HashMap<>();
        data2.put("odsId","123");
        data2.put("orgName","2022-07-31 02:38:33");
        data2.put("type","生化");
        data2.put("reportDt","门诊");
        data2.put("itemName","门诊");
        data2.put("reportSource","门诊");
        data2.put("reportSourceText","门诊");
        List<Map> list = new ArrayList<>();
        list.add(data2);
        data.put("code", "200");
        data.put("message", "");
        data.put("data", list);
        return ResultUtils.ok(data);
    }

    /**
     * 4.3.4检查报告详情
     */
    @GetMapping("/listCheckReport1")
    @ResponseBody
    public ApiResult listCheckReport1(String odsId) throws Exception{
        LOG.info("检查报告详情开始");
        String url = hisip+"listCheckReport";
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> data2 = new HashMap<>();
        data2.put("esamName","123");
        data2.put("orgName","2022-07-31 02:38:33");
        data2.put("reportDt","门诊");
        data2.put("createDt","门诊");
        data2.put("examView","门诊");
        data2.put("diagnoseOpinion","门诊");
        data.put("code", "200");
        data.put("message", "");
        data.put("data", data2);
        return ResultUtils.ok(data);
    }



    ////=================HTTPS接口短信接口+++++++++++++++
    private static final String DUANXINURL = "http://112.35.1.155:1992/sms/norsubmit";
    private static final String DUANXINURL2 = "http://112.35.1.155:1992/sms/norsubmit";
    @GetMapping("/sendMsg")
    @ResponseBody
    public ApiResult sendMsg(String patientMobile, String cardNumber, String patientName) throws Exception {
        LOG.info("短信接口开始");
        Submit submit = new Submit();
        submit.setEcName("建湖双湖中医院");
        submit.setApId("jhshzy");
        submit.setSecretKey("MwU$rW*4");
        submit.setMobiles(patientMobile);
        String code = RandomUtil.randomNum(6);
        submit.setContent("尊敬的用户您好,验证码为："+ code+ "。此验证码五分钟内有效，如重新发送以最新为准！请勿将验证码给别人！");
        submit.setSign("8a01UB15g");
        submit.setAddSerial("");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(submit.getEcName());
        stringBuffer.append(submit.getApId());
        stringBuffer.append(submit.getSecretKey());
        stringBuffer.append(submit.getMobiles());
        stringBuffer.append(submit.getContent());
        stringBuffer.append(submit.getSign());
        stringBuffer.append(submit.getAddSerial());
        String selfMac =md5(stringBuffer.toString());
        System.out.println("selfMac:"+selfMac);
        submit.setMac(selfMac);
        String param = JSON.toJSONString(submit);
        System.out.println("param:"+param);
        //
        OrderBill orderBill = new OrderBill();
        orderBill.setUserId(Long.valueOf(patientMobile));
        orderBill.setOrderType(code);
        orderBill.setCreateTime(new Date());
        orderBill.setOutTradeNo(cardNumber);
        orderBillService.insertSelective(orderBill);
        //Base64加密
        String encode = base64Encoded(param);
        System.out.println(encode);
        String s = doPostJson(DUANXINURL2, encode);
        LOG.info("短信接口返回："+s);
        System.out.println("短信接口返回:"+s);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        data.put("data", jsonMap);
        return ResultUtils.ok(data);
    }
    @GetMapping("/checkMsg")
    @ResponseBody
    public ApiResult checkMsg(String patientMobile, String cardNumber, String code) throws Exception {
        LOG.info("检查验证码");
        OrderBill orderBill = orderBillService.selectByOutTradeNo(cardNumber);
        Map map = new HashMap();
        if(orderBill != null && orderBill.getDeleteFlag() == 0){
            map.put("code", 201);
            map.put("checkMsg", "验证码已过期");
            return ResultUtils.ok(map);
        }
        if(orderBill != null && code.equals(orderBill.getOrderType())){
            orderBill.setDeleteFlag(0);
            orderBillService.updateByPrimaryKeySelective(orderBill);
            map.put("code", 200);
            map.put("checkMsg", "true");
            return ResultUtils.ok(map);
        }else{
            map.put("code", 201);
            map.put("checkMsg", "false");
            return ResultUtils.ok(map);
        }
    }

    public String md5(String value){
        String result = null;
        MessageDigest md5 = null;
        try{
            md5 = MessageDigest.getInstance("MD5");
            md5.update((value).getBytes("UTF-8"));
        }catch (NoSuchAlgorithmException error){
            error.printStackTrace();
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        byte b[] = md5.digest();
        int i;
        StringBuffer buf = new StringBuffer("");

        for(int offset=0; offset<b.length; offset++){
            i = b[offset];
            if(i<0){
                i+=256;
            }
            if(i<16){
                buf.append("0");
            }
            buf.append(Integer.toHexString(i));
        }

        result = buf.toString();
        return result;
    }

    public  String base64Encoded(String originalInput) {
        byte[] inputBytes = originalInput.getBytes(StandardCharsets.UTF_8);
        // 加密
        byte[] encodedBytes = Base64.getEncoder().encode(inputBytes);
        String encodedString = new String(encodedBytes, StandardCharsets.UTF_8);
        System.out.println("Encoded: " + encodedString);
        return encodedString;
    }
    public String  base64Decoded(String originalInput) {
        byte[] inputBytes = originalInput.getBytes(StandardCharsets.UTF_8);
        // 解密
        byte[] decodedBytes = Base64.getDecoder().decode(inputBytes);
        String decodedString = new String(decodedBytes, StandardCharsets.UTF_8);
        System.out.println("Decoded: " + decodedString);
        return decodedString;
    }
    /**
     * 首页医生信息轮播
     */
    @GetMapping("/listDoctorten")
    @ResponseBody
    public ApiResult listDoctorten(String doctorId,String doctorType) throws Exception{
        LOG.info("医生信息获取开始");
        String url = hisip+"listDoctor";
        List<Object> apiResult = listDepartmentlist();
        List<Object> arrayListnew = new ArrayList<>();
        for(Object obj : apiResult){
            Map map = (Map) obj;
            String departmentId = (String) map.get("departmentId");
            RestTemplate restTemplate = new RestTemplate();
            JSONObject jo = new JSONObject();
            jo.put("organizationId",organizationId);
            jo.put("departmentId",departmentId);//科室id
            jo.put("scheduleDate","");//yyyy-MM-dd HH:mm:ss  时间为0
            jo.put("doctorId",doctorId);//医生代码
            jo.put("doctorType",doctorType);//参照字典 doctorType
            jo.put("outpatientType",outpatientType);//参照字典outpatientType
            String s = restTemplate.postForObject(url, jo, String.class);
            Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
            List<Object> data1 = (List<Object>) jsonMap.get("data");
            for (Object objdoc : data1){
                Map mapold = (Map) objdoc;
                String sss = (String) mapold.get("doctorLevel");
                if (sss.equals("主治医师")) {
                    arrayListnew.add(objdoc);
                }
            }
            LOG.info("医生信息获取返回："+s);
            System.out.println("医生信息获取返回:"+s);
        }
        Map<String, Object> data = new HashMap<>();
        data.put("data", arrayListnew);
        return ResultUtils.ok(data);
    }
    /**
     * 科室信息获取list
     */
    @GetMapping("/listDepartmentlist")
    @ResponseBody
    public List<Object> listDepartmentlist() throws Exception{
        LOG.info("科室信息获取开始list");
        String url = hisip+"listDepartment";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jo = new JSONObject();
        jo.put("organizationId",organizationId);
        jo.put("outpatientType",outpatientType);
        String s = restTemplate.postForObject(url, jo, String.class);
        //改变
        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
        List<Object> data1 = (List<Object>) jsonMap.get("data");
        List<Object> datanew = new ArrayList<>();
        for (Object obj : data1){
            Map map = (Map) obj;
            List<Object> list2 = (List<Object>) map.get("departmentList");
            datanew.add(list2);
        }
        LOG.info("科室信息获取LIST返回："+s);
        System.out.println("科室信息获取LIST返回:"+s);
        return datanew;
    }
    //=================================================================
    /**
     * 科室信息获取
     */
    @GetMapping("/listDepartment1")
    @ResponseBody
    public ApiResult listDepartment1(String outpatientType) throws Exception{
        LOG.info("科室信息获取开始");
        String url = hisip+"listDepartment";
        Map departmentLists = new HashMap();
        departmentLists.put("departmentId", "101");
        departmentLists.put("departmentName", "骨科专");
        departmentLists.put("address", "门诊2楼西侧");
        Map departmentLists2 = new HashMap();
        departmentLists2.put("departmentId", "102");
        departmentLists2.put("departmentName", "精神科");
        departmentLists2.put("address", "门诊2楼");
        List<Map> departmentList = new ArrayList<>();
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        departmentList.add(departmentLists);
        departmentList.add(departmentLists2);
        Map data = new HashMap();
        data.put("departmentId", "4");
        data.put("departmentName", "外科门诊");
        data.put("departmentList", departmentList);
        List<Map> li = new ArrayList<>();
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        li.add(data);
        Map map = new HashMap();
        map.put("code", "200");
        map.put("data", li);

        LOG.info("科室信息获取返回："+map.toString());
        System.out.println("科室信息获取返回:"+map.toString());
        Map<String, Object> data2 = new HashMap<>();
        data2.put("data", map);
        return ResultUtils.ok(data2);
    }
    /**
     * 医生信息获取
     */
    @GetMapping("/listDoctor1")
    @ResponseBody
    public ApiResult listDoctor1(String departmentId,String scheduleDate,String doctorId,String doctorType,String outpatientType) throws Exception{
        LOG.info("医生信息获取开始");
        String url = hisip+"listDoctor";

        String s = "{" +
                "code: 200," +
                "data: [" +
                "        {" +
                "hospitalCode: 46830854-5," +
                "departmentId: 09," +
                "departmentName: 耳鼻喉科," +
                "doctorId: 0303," +
                "doctorCode: 0303," +
                "doctorName: 尤东旭," +
                "doctorSex: 1," +
                "doctorResume: ," +
                "doctorGoodAt: ," +
                "doctorJob: ," +
                "doctorLevel: 主治医师," +
                "regFee: 10.0," +
                "workRankId: 1," +
                "keyWord: ," +
                "flag: 1," +
                "sourceState: 2," +
                "visitingVolume: 0," +
                "listSchedule: [" +
                "" +
                "            ]," +
                "doctorCollect: 2" +
                "        }," +
                "        {" +
                "organizationId: 46830854-5," +
                "departmentId: 09," +
                "departmentName: 耳鼻喉科," +
                "doctorId: 0321," +
                "doctorCode: 0321," +
                "doctorName: 张进," +
                "doctorSex: 1," +
                "doctorResume: 1989.7南通医学院医疗系毕业，后分配至东台市人民医院工作至今。," +
                "doctorGoodAt: ," +
                "doctorJob: ," +
                "doctorLevel: 主任医师," +
                "regFee: 10.0," +
                "workRankId: 1," +
                "keyWord: ," +
                "flag: 1," +
                "sourceState: 2," +
                "visitingVolume: 0," +
                "listSchedule: [" +
                "" +
                "            ]," +
                "doctorCollect: 2" +
                "        }" +
                "    ]" +
                "}";
        LOG.info("医生信息获取返回："+s);
        System.out.println("医生信息获取返回:"+s);
        Map<String, Object> data = new HashMap<>();
        data.put("data", s);
        return ResultUtils.ok(data);
    }

    /**
     *  排班信息获取
     */
    @GetMapping("/listNumber1")
    @ResponseBody
    public ApiResult listNumber1(String departmentId,String scheduleDate,String doctorId,String outpatientType,String periodId) throws Exception{
        LOG.info("排班信息获取开始");
        String url = hisip+"listNumber";

        String s = "{" +
                "code: 200," +
                "data: [" +
                "        {" +
                "beginTime: 2022-08-01 08:00:00," +
                "endTime: 2022-08-01 08:05:00," +
                "regFee: 10.0," +
                "totalNumber: 1," +
                "leftNumber: 1," +
                "serialNumber: 0," +
                "numberStatus: 1," +
                "workId: 28592363" +
                "        }," +
                "        {" +
                "beginTime: 2022-08-01 08:05:00," +
                "endTime: 2022-08-01 08:10:00," +
                "regFee: 10.0," +
                "totalNumber: 1," +
                "leftNumber: 1," +
                "serialNumber: 0," +
                "numberStatus: 1," +
                "workId: 28592675" +
                "        }" +
                "    ]" +
                "}";
        LOG.info("排班信息获取返回："+s);
        System.out.println("排班信息获取返回:"+s);
        Map<String, Object> data = new HashMap<>();
        data.put("data", s);
        return ResultUtils.ok(data);
    }

    /**
     * 预约提交
     */
    @GetMapping("/confirmAppointment1")
    @ResponseBody
    public ApiResult confirmAppointment1(String appointmentType,String departmentName,
                                        String patientMobile,String patientName,
                                        String resourceId,String departmentId,String doctorId) throws Exception{
        LOG.info("预约提交开始");
        String url = hisip+"confirmAppointment";

        String s = "{" +
                "code: 200," +
                "message: 提交预约成功！," +
                "data: {" +
                "hospitalCode: 46830854-5," +
                "hisOrderNumber: 1552157," +
                "source: 2," +
                "workId: 28654511," +
                "serialNumber: 0," +
                "clinicLocation: 门诊3楼中部," +
                "clinicDescription: ," +
                "regFee: 10.00," +
                "endPayTime: ," +
                "patientCode: 2655500," +
                "scheduleDate: 2022-08-02 14:35:00," +
                "accountExpenditure: 0.0," +
                "cashExpenditure: 0.0," +
                "overallPayment: 0.0," +
                "success: false" +
                "    }" +
                "}";
        LOG.info("预约提交返回："+s);
        System.out.println("预约提交返回:"+s);
        Map<String, Object> data = new HashMap<>();
        data.put("data", s);
        return ResultUtils.ok(data);
    }
    /**
     * 首页医生信息轮播
     */
    @GetMapping("/listDoctorten1")
    @ResponseBody
    public ApiResult listDoctorten1(String doctorId,String doctorType) throws Exception{
        LOG.info("医生信息获取开始1");
        List<Object> arrayListnew = new ArrayList<>();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("hospitalCode","46830854-5");
        data1.put("departmentId","09-5");
        data1.put("hospitalCode","46830854-5");
        data1.put("departmentName","耳鼻喉科");
        data1.put("doctorName","尤东旭");
        data1.put("doctorLevel","主治医师");
        data1.put("doctorId","03");
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        arrayListnew.add(data1);
        Map<String, Object> data = new HashMap<>();
        data.put("data", arrayListnew);
        return ResultUtils.ok(data);
    }
    @GetMapping("/leibie")
    @ResponseBody
    public ApiResult leibie() {
        Map map = new HashMap();
        List<YyNewsType> dj = yyNewsTypeService.selectByfw("dj");
        map.put("dj",dj);
        List<YyNewsType> gg = yyNewsTypeService.selectByfw("gg");
        map.put("gg",gg);
        List<YyNewsType> news = yyNewsTypeService.selectByfw("new");
        map.put("new",news);
        List<YyNewsType> zp = yyNewsTypeService.selectByfw("zp");
        map.put("zp",zp);
        List<YyNewsType> jyzn = yyNewsTypeService.selectByfw("jyzn");
        map.put("jyzn",jyzn);
        List<YyNewsType> yygk = yyNewsTypeService.selectByfw("yygk");
        map.put("yygk",yygk);
        return ResultUtils.ok(map);
    }
    @GetMapping("/jieshao")
    @ResponseBody
    public ApiResult jieshao(Tablepar tablepar, YyJieshao yyJieshao) {

        PageInfo<YyJieshao> page = yyJieshaoService.list(tablepar, yyJieshao);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }
    @GetMapping("/zhuanjiaDetail")
    @ResponseBody
    public ApiResult zhuanjiaDetail(String id) {
        Integer viewNumber = 0;
        YyZhuanjia yyZhuanjia = yyZhuanjiaService.selectByPrimaryKey(id);
        YyKeshi keshi = yyKeshiService.selectByPrimaryKey(yyZhuanjia.getKeshi());
        yyZhuanjia.setKeshiname(keshi.getName());

        if(yyZhuanjia.getField2() != null){
            viewNumber = Integer.valueOf(yyZhuanjia.getField2()) + 1;
        }else{
            viewNumber = 1;
        }
        yyZhuanjia.setField2(String.valueOf(viewNumber));
        yyZhuanjiaService.updateByPrimaryKey(yyZhuanjia);

        return ResultUtils.ok(yyZhuanjia);
    }

    @GetMapping("/keshiDetail")
    @ResponseBody
    public ApiResult keshiDetail(String id) {
        Integer viewNumber = 0;
        YyKeshi keshi = yyKeshiService.selectByPrimaryKey(id);
        if(keshi.getField2() != null){
            viewNumber = Integer.valueOf(keshi.getField2()) + 1;
        }else{
            viewNumber = 1;
        }
        keshi.setField2(String.valueOf(viewNumber));
        yyKeshiService.updateByPrimaryKey(keshi);
        return ResultUtils.ok(keshi);
    }
    @GetMapping("/news")
    @ResponseBody
    public ApiResult news(Tablepar tablepar, YyNews yyNews) {
        yyNews.setStatus(2);
        PageInfo<YyNews> page = yyNewsService.list(tablepar, yyNews);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }

    @GetMapping("/dangjian")
    @ResponseBody
    public ApiResult dangjian(Tablepar tablepar, YyDangjian yyDangjian) {
        yyDangjian.setStatus(2);
        PageInfo<YyDangjian> page = yyDangjianService.list(tablepar, yyDangjian);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }

    @GetMapping("/gonggao")
    @ResponseBody
    public ApiResult gonggao(Tablepar tablepar, YyGonggao yyGonggao) {
        yyGonggao.setStatus(2);
        PageInfo<YyGonggao> page = yyGonggaoService.list(tablepar, yyGonggao);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }

    @PostMapping("/saveyijian")
    @ResponseBody
    public ApiResult saveyijian(@RequestBody YyLiuyan yyLiuyan) {
        System.out.println("saveyijian入参：");
        System.out.println(JSONObject.toJSONString(yyLiuyan));
        yyLiuyan.setCreateTime(new Date());
        yyLiuyan.setDeleteFlag(0);
        yyLiuyan.setUpdateTime(new Date());
        int b = yyLiuyanService.insertSelective(yyLiuyan);
        Map<String, Object> data = new HashMap<>();
        data.put("data", "success");
        return ResultUtils.ok(data);
    }

    @GetMapping("/lunbotu")
    @ResponseBody
    public ApiResult lunbotu(String type) {
        List<YyLunbotu> list = yyLunbotuService.getList(type);
        Map<String, Object> data = new HashMap<>();
        data.put("data", list);
        return ResultUtils.ok(data);
    }

    @GetMapping("/zhaoping")
    @ResponseBody
    public ApiResult zhaoping(Tablepar tablepar, YyZhaopin yyZhaopin) {
        yyZhaopin.setStatus(2);
        PageInfo<YyZhaopin> page = yyZhaopinService.list(tablepar, yyZhaopin);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }


    @GetMapping("/lianxi")
    @ResponseBody
    public ApiResult lianxi() {
        Map<String, Object> data2 = new HashMap<>();
        data2.put("地址", "江苏省盐城市建湖县汇文东路169号");
        data2.put("邮编", "224799");
        data2.put("电话", "xxxxxxxxxx");
        Map<String, Object> data = new HashMap<>();
        data.put("data", data2);
        return ResultUtils.ok(data);
    }

    @GetMapping("/zhuanjia")
    @ResponseBody
    public ApiResult zhuanjia(Tablepar tablepar, YyZhuanjia yyZhuanjia) {
        PageInfo<YyZhuanjia> page = yyZhuanjiaService.list(tablepar, yyZhuanjia);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }

    @GetMapping("/keshi")
    @ResponseBody
    public ApiResult keshi(Tablepar tablepar, YyKeshi yyKeshi) {
        List<String> field1List = yyKeshiService.getListByField1();
        List<Map<String, Object>> keshi = new ArrayList<Map<String,Object>>();
        int x =0;
        for(String field1 : field1List){
            x =x+1;
            Map<String, Object> keshitree = new HashMap<>();
            keshitree.put("id", x);
            keshitree.put("name", field1);

            List<YyKeshi> listByFiled1 = yyKeshiService.selectByFiled1(field1);
            keshitree.put("keshiList", listByFiled1);

            keshi.add(keshitree);
        }
        Map<String, Object> data = new HashMap<>();
        data.put("page", keshi);
        return ResultUtils.ok(data);
    }
    @GetMapping("/keshitoten")
    @ResponseBody
    public ApiResult keshitoten() {
//        List<YyKeshi> page = yyKeshiService.listtoten();
        List<YyKeshi> page = yyKeshiService.selectsslist();
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }
    @GetMapping("/jyzn")
    @ResponseBody
    public ApiResult jyzn(Tablepar tablepar, YyJiuyizhinan yyJiuyizhinan) {
        yyJiuyizhinan.setStatus(2);
        PageInfo<YyJiuyizhinan> page = yyJiuyizhinanService.list(tablepar, yyJiuyizhinan);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }
    @GetMapping("/yygk")
    @ResponseBody
    public ApiResult yygk(Tablepar tablepar,YyYiyuangaikuang yyYiyuangaikuang) {
        yyYiyuangaikuang.setStatus(2);
        PageInfo<YyYiyuangaikuang> page = yyYiyuangaikuangService.list(tablepar, yyYiyuangaikuang);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }
    @GetMapping("/jkzs")
    @ResponseBody
    public ApiResult jkzs(Tablepar tablepar, YyJiankangzhishi yyJiankangzhishi) {
        yyJiankangzhishi.setStatus(2);
        PageInfo<YyJiankangzhishi> page = yyJiankangzhishiService.list(tablepar, yyJiankangzhishi);
        Map<String, Object> data = new HashMap<>();
        data.put("page", page);
        return ResultUtils.ok(data);
    }

    @GetMapping("/getdetail")
    @ResponseBody
    public ApiResult getnew(String type, String id, HttpServletRequest request) {
        String htmlFilePath2 = "";
        String paths = "";
        String htmlFilePaths = "";
        String fabuData = "";
        String wordUrl = "";
        Integer viewNumber = 0;
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        Long iddetail =0l;
        if (type.equals("new")) {
            YyNews yyNews = yyNewsService.selectByPrimaryKey(id);
            iddetail = yyNews.getId();
            paths = yyNews.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/3.html";
            htmlFilePaths = "upload/3.html";
            fabuData = sdf.format(yyNews.getCreateTime());
            wordUrl=yyNews.getField2();
            if(yyNews.getField10() != null){
                viewNumber = Integer.valueOf(yyNews.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyNews.setField10(String.valueOf(viewNumber));
            yyNewsService.updateByPrimaryKey(yyNews);
        }
        if (type.equals("dj")) {
            YyDangjian yyDangjian = yyDangjianService.selectByPrimaryKey(id);
            iddetail = yyDangjian.getId();
            paths = yyDangjian.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/1.html";
            htmlFilePaths = "upload/1.html";
            fabuData = sdf.format(yyDangjian.getCreateTime());
            wordUrl=yyDangjian.getField2();
            if(yyDangjian.getField10() != null){
                viewNumber = Integer.valueOf(yyDangjian.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyDangjian.setField10(String.valueOf(viewNumber));
            yyDangjianService.updateByPrimaryKey(yyDangjian);
        }
        if (type.equals("gg")) {
            YyGonggao yyGonggao = yyGonggaoService.selectByPrimaryKey(id);
            iddetail = yyGonggao.getId();
            paths = yyGonggao.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/2.html";
            htmlFilePaths = "upload/2.html";
            fabuData = sdf.format(yyGonggao.getCreateTime());
            wordUrl=yyGonggao.getField2();
            if(yyGonggao.getField10() != null){
                viewNumber = Integer.valueOf(yyGonggao.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyGonggao.setField10(String.valueOf(viewNumber));
            yyGonggaoService.updateByPrimaryKey(yyGonggao);
        }
        if (type.equals("zp")) {
            YyZhaopin yyZhaopin = yyZhaopinService.selectByPrimaryKey(id);
            iddetail = yyZhaopin.getId();
            paths = yyZhaopin.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/4.html";
            htmlFilePaths = "upload/4.html";
            fabuData = sdf.format(yyZhaopin.getCreateTime());
            wordUrl=yyZhaopin.getField2();
            if(yyZhaopin.getField10() != null){
                viewNumber = Integer.valueOf(yyZhaopin.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyZhaopin.setField10(String.valueOf(viewNumber));
            yyZhaopinService.updateByPrimaryKey(yyZhaopin);
        }
        if (type.equals("jyzn")) {
            YyJiuyizhinan yyJiuyizhinan = yyJiuyizhinanService.selectByPrimaryKey(id);
            iddetail = yyJiuyizhinan.getId();
            paths = yyJiuyizhinan.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/4.html";
            htmlFilePaths = "upload/5.html";
            fabuData = sdf.format(yyJiuyizhinan.getCreateTime());
            wordUrl=yyJiuyizhinan.getField2();
            if(yyJiuyizhinan.getField10() != null){
                viewNumber = Integer.valueOf(yyJiuyizhinan.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyJiuyizhinan.setField10(String.valueOf(viewNumber));
            yyJiuyizhinanService.updateByPrimaryKey(yyJiuyizhinan);
        }
        if (type.equals("yygk")) {
            //YyYiyuangaikuang yyYiyuangaikuang = yyYiyuangaikuangService.selectByPrimaryKey(id);
            YyYiyuangaikuang yyYiyuangaikuang = yyYiyuangaikuangService.selectOne();
            iddetail = yyYiyuangaikuang.getId();
            paths = yyYiyuangaikuang.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/4.html";
            htmlFilePaths = "upload/6.html";
            fabuData = sdf.format(yyYiyuangaikuang.getCreateTime());
            wordUrl=yyYiyuangaikuang.getField2();
            if(yyYiyuangaikuang.getField10() != null){
                viewNumber = Integer.valueOf(yyYiyuangaikuang.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyYiyuangaikuang.setField10(String.valueOf(viewNumber));
            yyYiyuangaikuangService.updateByPrimaryKey(yyYiyuangaikuang);
        }
        if (type.equals("jkzs")) {
            YyJiankangzhishi yyJiankangzhishi = yyJiankangzhishiService.selectByPrimaryKey(id);
            iddetail = yyJiankangzhishi.getId();
            paths = yyJiankangzhishi.getField2();
            htmlFilePath2 = "http://" + request.getServerName() + ":" + request.getServerPort() + "/upload/4.html";
            htmlFilePaths = "upload/7.html";
            fabuData = sdf.format(yyJiankangzhishi.getCreateTime());
            wordUrl=yyJiankangzhishi.getField2();
            if(yyJiankangzhishi.getField10() != null){
                viewNumber = Integer.valueOf(yyJiankangzhishi.getField10()) + 1;
            }else{
                viewNumber = 1;
            }
            yyJiankangzhishi.setField10(String.valueOf(viewNumber));
            yyJiankangzhishiService.updateByPrimaryKey(yyJiankangzhishi);
        }
        List<YyDetail> yyDetails = yyDetailService.selectByYyIdAndYyType(iddetail,type);
        long timeMillis = System.currentTimeMillis();
        System.out.println("开始转换！");
        String sss = "http://" + request.getServerName() + ":" + request.getServerPort();
        String fieldnewpath = paths.replace(sss,"");
        String wordFilePath = System.getProperty("user.dir") + File.separator + fieldnewpath;
        String htmlFilePath = System.getProperty("user.dir") + File.separator + htmlFilePaths;
        File file = WordToHtml.wordToHtml(wordFilePath, htmlFilePath);
        // 读取html文件
        Map<String, Object> data = new HashMap<>();
        if (file != null) {
            System.out.println("文件存放路径：" + file.getPath());
            System.out.println("转换结束！用时：" + (System.currentTimeMillis() - timeMillis));
            data.put("url", htmlFilePath2);
        } else {
            data.put("url", 404);
        }
        data.put("data", fabuData);
        data.put("wordUrl", wordUrl);
        data.put("yyDetails", yyDetails);
        data.put("viewNumber", viewNumber);
        return ResultUtils.ok(data);
    }

//    public static void main(String[] args) {
//        String s = "{\"data\":[{\"organizationId\":\"46706958-9\",\"departmentId\":\"30\",\"departmentName\":\"测试\",\"doctorId\":\"10399\",\"doctorName\":\"管理员\",\"doctorSex\":\"1\",\"doctorInformation\":\"\",\"doctorGoodAt\":\"\",\"doctorJob\":\"\",\"majorQualify\":\"主任医师\",\n" +
//                "\"regFee\":\"0.10\",\"workRankId\":\"3\",\"keyWord\":\"\",\"flag\":\"1\",\"listSchedule\":[{\"scheduleDate\":\"2023/3/2 00:00:00\",\"periodId\":\"1\",\"orderCount\":null,\"remainNumber\":\"0\"\n" +
//                ",\"sourceState\":\"2\",\"admitAddress\":\"\",\"resourceId\":\"_1_30_10399\"},{\"scheduleDate\"\n" +
//                ":\"2023/3/9 00:00:00\",\"periodId\":\"1\",\"orderCount\":null,\"remainNumber\":\"0\",\"sourceState\":\"2\",\"admitAddress\":\"\",\"resourceId\":\"_1_30_10399\"},{\"scheduleDate\":\"2023/3/9 00:00:00\",\"periodId\":\"2\",\"orderCount\":null,\"remainNumber\":\"0\",\"sourceState\":\"\n" +
//                "2\",\"admitAddress\":\"\",\"resourceId\":\"_2_30_10399\"}]}],\"code\":\"200\",\"message\":\"处理成功\"}";
//        LOG.info("医生信息获取返回："+s);
//        System.out.println("医生信息获取返回:");
//        System.out.println(s);
//        Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
//        //时间
//        //时间
//        DataTest dataTest = new DataTest();
//        List<Map>  dates = dataTest.dates(8);
//        if ("200".equals(jsonMap.get("code"))) {
//            List<Object> data1 = (List<Object>) jsonMap.get("data");
//            List listnew = new ArrayList();
//            for (Object obj : data1){
//                Map map = (Map) obj;
//                List<Map> listSchedule = (List<Map>) map.get("listSchedule");
//                for(Map m :dates){
//                    Map mnew = new HashMap();
//                    mnew.put("year", m.get("year"));
//                    mnew.put("day", m.get("day"));
//                    mnew.put("zhou", m.get("zhou"));
//                    List<Map> amPmResults = new ArrayList<>();
//                    Map mnewsw = new HashMap();
//                    mnewsw.put("type", "上午");
//                    mnewsw.put("list", "");
//                    Map mnewxw = new HashMap();
//                    mnewxw.put("type", "下午");
//                    mnewxw.put("list", "");
//                    for(Map m2:listSchedule){
//                        String year = (String) m.get("year");
//                        String scheduleDate1 = (String) m2.get("scheduleDate");
//
//                        if(scheduleDate1.contains(year)){
//                            if("1".equals(m2.get("periodId"))){
//                                Map mnew2 = new HashMap();
//                                mnew2.put("type", "上午");
//                                mnew2.put("periodId", m2.get("periodId"));
//                                mnew2.put("resourceId", m2.get("resourceId"));
//                                mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
//                                mnew2.put("scheduleDate", m2.get("scheduleDate"));
//                                mnew2.put("orderCount", m2.get("orderCount"));
//                                mnew2.put("admitAddress", m2.get("admitAddress"));
//                                mnew2.put("sourceState", m2.get("sourceState"));
//                                mnewsw.put("list", mnew2);
//                            }
//                            if("2".equals(m2.get("periodId"))){
//                                Map mnew2 = new HashMap();
//                                mnew2.put("type", "下午");
//                                mnew2.put("periodId", m2.get("periodId"));
//                                mnew2.put("resourceId", m2.get("resourceId"));
//                                mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
//                                mnew2.put("scheduleDate", m2.get("scheduleDate"));
//                                mnew2.put("orderCount", m2.get("orderCount"));
//                                mnew2.put("admitAddress", m2.get("admitAddress"));
//                                mnew2.put("sourceState", m2.get("sourceState"));
//                                mnewxw.put("list", mnew2);
//                            }
//                        }
//
//                    }
//                    amPmResults.add(mnewsw);
//                    amPmResults.add(mnewxw);
//                    mnew.put("amPmResults", amPmResults);
//                    listnew.add(mnew);
//                }
//                map.put("listSchedule",listnew);
//            }
//        }
//        Map<String, Object> data = new HashMap<>();
//        data.put("data", jsonMap);
//        System.out.println(data.toString());
//    }


//public static void main(String[] args) {
//    String s = "{\n" +
//            "\t\"code\": \"200\",\n" +
//            "\t\"data\": [{\n" +
//            "\t\t\t\"departmentName\": \"测试\",\n" +
//            "\t\t\t\"flag\": \"1\",\n" +
//            "\t\t\t\"departmentId\": \"30\",\n" +
//            "\t\t\t\"doctorSex\": \"1\",\n" +
//            "\t\t\t\"keyWord\": \"\",\n" +
//            "\t\t\t\"organizationId\": \"46706958-9\",\n" +
//            "\t\t\t\"workRankId\": \"3\",\n" +
//            "\t\t\t\"doctorName\": \"管理员\",\n" +
//            "\t\t\t\"doctorInformation\": \"\",\n" +
//            "\t\t\t\"doctorJob\": \"\",\n" +
//            "\t\t\t\"doctorId\": \"10399\",\n" +
//            "\t\t\t\"regFee\": \"0.10\",\n" +
//            "\t\t\t\"listSchedule\": [\n" +
//            "\t\t\t  {\n" +
//            "\t\t\t\t\t\"periodId\": \"1\",\n" +
//            "\t\t\t\t\t\"resourceId\": \"_1_30_10399\",\n" +
//            "\t\t\t\t\t\"remainNumber\": \"0\",\n" +
//            "\t\t\t\t\t\"scheduleDate\": \"2023/3/2 00:00:00\",\n" +
//            "\t\t\t\t\t\"orderCount\": null,\n" +
//            "\t\t\t\t\t\"admitAddress\": \"\",\n" +
//            "\t\t\t\t\t\"sourceState\": \"2\"\n" +
//            "\t\t\t\t},\n" +
//            "\t\t\t\t{\n" +
//            "\t\t\t\t\t\"periodId\": \"2\",\n" +
//            "\t\t\t\t\t\"resourceId\": \"_2_30_10399\",\n" +
//            "\t\t\t\t\t\"remainNumber\": \"0\",\n" +
//            "\t\t\t\t\t\"scheduleDate\": \"2023/3/2 00:00:00\",\n" +
//            "\t\t\t\t\t\"orderCount\": null,\n" +
//            "\t\t\t\t\t\"admitAddress\": \"\",\n" +
//            "\t\t\t\t\t\"sourceState\": \"2\"\n" +
//            "\t\t\t\t}\n" +
//            "\t\t\t],\n" +
//            "\t\t\t\"doctorGoodAt\": \"\",\n" +
//            "\t\t\t\"majorQualify\": \"主任医师\"\n" +
//            "\t\t},\n" +
//            "\t\t{\n" +
//            "\t\t\t\"departmentName\": \"测试2\",\n" +
//            "\t\t\t\"flag\": \"1\",\n" +
//            "\t\t\t\"departmentId\": \"30\",\n" +
//            "\t\t\t\"doctorSex\": \"1\",\n" +
//            "\t\t\t\"keyWord\": \"\",\n" +
//            "\t\t\t\"organizationId\": \"46706958-9\",\n" +
//            "\t\t\t\"workRankId\": \"3\",\n" +
//            "\t\t\t\"doctorName\": \"管理员\",\n" +
//            "\t\t\t\"doctorInformation\": \"\",\n" +
//            "\t\t\t\"doctorJob\": \"\",\n" +
//            "\t\t\t\"doctorId\": \"10399\",\n" +
//            "\t\t\t\"regFee\": \"0.10\",\n" +
//            "\t\t\t\"listSchedule\": [{\n" +
//            "\t\t\t\t\t\"periodId\": \"1\",\n" +
//            "\t\t\t\t\t\"resourceId\": \"_1_30_10399\",\n" +
//            "\t\t\t\t\t\"remainNumber\": \"0\",\n" +
//            "\t\t\t\t\t\"scheduleDate\": \"2023/3/2 00:00:00\",\n" +
//            "\t\t\t\t\t\"orderCount\": null,\n" +
//            "\t\t\t\t\t\"admitAddress\": \"\",\n" +
//            "\t\t\t\t\t\"sourceState\": \"2\"\n" +
//            "\t\t\t\t},\n" +
//            "\t\t\t\t\t\t\t {\n" +
//            "\t\t\t\t\t\"periodId\": \"2\",\n" +
//            "\t\t\t\t\t\"resourceId\": \"_1_30_10399\",\n" +
//            "\t\t\t\t\t\"remainNumber\": \"0\",\n" +
//            "\t\t\t\t\t\"scheduleDate\": \"2023/3/2 00:00:00\",\n" +
//            "\t\t\t\t\t\"orderCount\": null,\n" +
//            "\t\t\t\t\t\"admitAddress\": \"\",\n" +
//            "\t\t\t\t\t\"sourceState\": \"2\"\n" +
//            "\t\t\t\t}\n" +
//            "\t\t\t],\n" +
//            "\t\t\t\"doctorGoodAt\": \"\",\n" +
//            "\t\t\t\"majorQualify\": \"主任医师\"\n" +
//            "\t\t}\n" +
//            "\t],\n" +
//            "\t\"message\": \"处理成功\"\n" +
//            "}";
//    LOG.info("医生信息获取返回："+s);
//    System.out.println("医生信息获取返回:");
//    System.out.println(s);
//    Map<String, Object> jsonMap = JSON.parseObject(s,Map.class);
//    Map sw = new HashMap();
//    Map xw = new HashMap();
//    List listnew = new ArrayList();
//    List swlist = new ArrayList();
//    List xwlist = new ArrayList();
//    //上午下午
//    if ("200".equals(jsonMap.get("code"))) {
//        List<Object> data1 = (List<Object>) jsonMap.get("data");
//        for (Object obj : data1){
//            Map map = (Map) obj;
//
//            List<Map> listSchedule = (List<Map>) map.get("listSchedule");
//            if(listSchedule.size()>0){
//
//                for(Map m2:listSchedule){
//
//                    if("1".equals(m2.get("periodId"))){
//                        Map mnew = new HashMap();
//                        mnew.put("departmentName", map.get("departmentName"));
//                        mnew.put("flag", map.get("flag"));
//                        mnew.put("doctorSex", map.get("doctorSex"));
//                        mnew.put("keyWord", map.get("keyWord"));
//                        mnew.put("organizationId", map.get("organizationId"));
//                        mnew.put("workRankId", map.get("workRankId"));
//                        mnew.put("doctorName", map.get("doctorName"));
//                        mnew.put("doctorInformation", map.get("doctorInformation"));
//                        mnew.put("doctorJob", map.get("doctorJob"));
//                        mnew.put("doctorId", map.get("doctorId"));
//                        mnew.put("regFee", map.get("regFee"));
//                        mnew.put("doctorGoodAt", map.get("doctorGoodAt"));
//                        mnew.put("majorQualify", map.get("majorQualify"));
//                        List linew = new ArrayList();
//                        Map mnew2 = new HashMap();
//                        mnew2.put("periodId", m2.get("periodId"));
//                        mnew2.put("resourceId", m2.get("resourceId"));
//                        mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
//                        mnew2.put("scheduleDate", m2.get("scheduleDate"));
//                        mnew2.put("orderCount", m2.get("orderCount"));
//                        mnew2.put("admitAddress", m2.get("admitAddress"));
//                        mnew2.put("sourceState", m2.get("sourceState"));
//                        linew.add(mnew2);
//                        mnew.put("listSchedule", linew);
//                        swlist.add(mnew);
//                    }
//                    if("2".equals(m2.get("periodId"))){
//                        Map mnew = new HashMap();
//                        mnew.put("departmentName", map.get("departmentName"));
//                        mnew.put("flag", map.get("flag"));
//                        mnew.put("doctorSex", map.get("doctorSex"));
//                        mnew.put("keyWord", map.get("keyWord"));
//                        mnew.put("organizationId", map.get("organizationId"));
//                        mnew.put("workRankId", map.get("workRankId"));
//                        mnew.put("doctorName", map.get("doctorName"));
//                        mnew.put("doctorInformation", map.get("doctorInformation"));
//                        mnew.put("doctorJob", map.get("doctorJob"));
//                        mnew.put("doctorId", map.get("doctorId"));
//                        mnew.put("regFee", map.get("regFee"));
//                        mnew.put("doctorGoodAt", map.get("doctorGoodAt"));
//                        mnew.put("majorQualify", map.get("majorQualify"));
//                        List linew = new ArrayList();
//                        Map mnew2 = new HashMap();
//                        mnew2.put("periodId", m2.get("periodId"));
//                        mnew2.put("resourceId", m2.get("resourceId"));
//                        mnew2.put("remainNumber", m2.get("perioremainNumberdId"));
//                        mnew2.put("scheduleDate", m2.get("scheduleDate"));
//                        mnew2.put("orderCount", m2.get("orderCount"));
//                        mnew2.put("admitAddress", m2.get("admitAddress"));
//                        mnew2.put("sourceState", m2.get("sourceState"));
//                        linew.add(mnew2);
//                        mnew.put("listSchedule", linew);
//                        xwlist.add(mnew);
//                    }
//                }
//            }
//        }
//        sw.put("type", "上午");
//        sw.put("list", swlist);
//        xw.put("type", "下午");
//        xw.put("list", xwlist);
//    }
//    listnew.add(sw);
//    listnew.add(xw);
//    Map<String, Object> data = new HashMap<>();
//    data.put("data", listnew);
//    System.out.println(ResultUtils.ok(JSONObject.toJSONString(data)));
//}
}
