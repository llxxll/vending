package com.fc.v2.controller.yiyuan;

import com.alipay.api.AlipayApiException;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.Result;
import com.fc.v2.model.auto.Order;
import com.fc.v2.service.AliPayService;
import com.fc.v2.service.OrderService;
import com.fc.v2.util.RandomUtil;
import com.fc.v2.util.ResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

@Slf4j
@RestController
@Api(tags = "支付宝支付")
@RequestMapping(value = "/api/pay/ali")
public class AliPayController extends BaseController {

    @Autowired
    private AliPayService aliPayService;

    @GetMapping("/app")
    @ApiOperation("支付宝App支付预下单")
    public Result appPay(Long orderId, HttpServletRequest request, HttpServletResponse response) {
        return Result.success(aliPayService.appPay(orderId, request));
    }

    @GetMapping("/qrcode2")
    @ApiOperation("支付宝扫码支付预下单")
    public Result qrCodePay(Long orderId, HttpServletResponse response) throws Exception {
        return aliPayService.aliQrCodePay(orderId, response);
    }

    @GetMapping("/qrcode")
    @ApiOperation("支付宝扫码支付预下单")
    public ApiResult qrCodePay2(Long orderId, HttpServletResponse response) throws Exception {
        return aliPayService.qrcoe(orderId);
    }

    @GetMapping("/cha")
    @ApiOperation("支付宝扫码支付查询")
    public Result cha(String outTradeNo) throws Exception {
        return aliPayService.cha(outTradeNo);
    }

    @ApiOperation("支付宝App支付退款")
    @GetMapping("/refund2")
    public Result appRefund2(Long orderId, HttpServletRequest request) throws Exception {
        return aliPayService.aliRefund(orderId, request);
    }

    @ApiOperation("支付宝App支付退款")
    @GetMapping("/refund")
    public Result appRefund(Long orderId, HttpServletRequest request) throws Exception {
        return aliPayService.aliRefund2(orderId, request);
    }

    @ApiOperation("支付宝支付通知")
    @RequestMapping(value = "/notify", method = {RequestMethod.GET, RequestMethod.POST})
    public Result aliNotify(HttpServletRequest request) throws Exception {
        return aliPayService.aliNotify2(request);
    }

    @ApiOperation("支付宝支付通知")
    @RequestMapping(value = "/notify2", method = {RequestMethod.POST})
    public Result aliNotify2(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return aliPayService.aliNotify(request, response);
    }
    @Resource
    private OrderService orderService;
    @ApiOperation("")
    @RequestMapping(value = "/send", method = {RequestMethod.POST})
    public ApiResult send(){
        Order order = new Order();
        order.setTotalFee(1);
        order.setOutTradeNo(RandomUtil.randomNum(32));
        order.setCreateTime(new Date());
        order.setRemark("挂号费用");
        orderService.insertSelective(order);
        return ResultUtils.ok(order);
    }

    @GetMapping(value = "/cahxun")
    public ApiResult cahxun(String orderId){
        Order order = orderService.selectByPrimaryKey(orderId);
        Byte status = order.getStatus();
        if(status==1){
            return ResultUtils.ok("支付成功");
        }else{
            return ResultUtils.ok("支付中");
        }
    }
}
