package com.fc.v2.controller.yiyuan;

import com.fc.v2.common.base.BaseController;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.Result;
import com.fc.v2.model.property.AppProperties;
import com.fc.v2.service.WxPayService;
import com.wechat.pay.java.service.payments.app.AppService;
import com.wechat.pay.java.service.payments.app.model.QueryOrderByIdRequest;
import com.wechat.pay.java.service.payments.model.Transaction;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 微信支付
 *
 * @author leone
 * @since 2018-05-20
 **/
@Slf4j
@RestController
@Api(tags = "微信支付接口")
@RequestMapping("/wx/pay/order")
public class WxPayController extends BaseController {

    @Resource
    private WxPayService wxPayService;


    @GetMapping("/qrcode")
    @ApiOperation("微信扫码支付预下单")
    public ApiResult qrCodePay(Long orderId) {
        return wxPayService.qrCodePay(orderId);
    }
    @ApiOperation("微信App支付退款")
    @GetMapping("/app/refund2")
    public ApiResult appRefund(Long orderId) throws Exception {
        return wxPayService.wxRefund(orderId);
    }

    @ApiOperation("微信App支付退款")
    @GetMapping("/app/refund")
    public ApiResult appRefund2(Long orderId) throws Exception {
        return wxPayService.wxRefund2(orderId);
    }

    @ApiOperation("微信支付通知")
    @PostMapping(value = "/notify")
    public ApiResult appNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return wxPayService.notify2(request, response);
    }

    @ApiOperation("微信支付订单号查询订单")
    @GetMapping(value = "/query")
    public ApiResult query(Long orderId) throws Exception {
        return wxPayService.queryOrderById(orderId);
    }

    @ApiOperation("商户订单号查询订单")
    @GetMapping(value = "/queryOut")
    public ApiResult queryOut(Long orderId) throws Exception {
        return wxPayService.queryOut(orderId);
    }


    @GetMapping("/app")
    @ApiOperation("微信App支付预下单")
    public Result appPay(Long orderId, HttpServletRequest request, HttpServletResponse response) {
        return Result.success(wxPayService.appPay(request, orderId));
    }

    @GetMapping("/xcx")
    @ApiOperation("小程序扫支付预下单")
    public Result xcxPay(Long orderId, HttpServletRequest request, HttpServletResponse response) {
        return wxPayService.xcxPay(orderId, request);
    }



}
