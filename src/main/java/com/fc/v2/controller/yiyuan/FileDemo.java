package com.fc.v2.controller.yiyuan;

import com.fc.v2.model.auto.JshMaterial;
import com.fc.v2.model.auto.PlanOudetail;
import com.fc.v2.model.auto.PlanProduceMaterialDetail;
import com.fc.v2.util.ExeclUtil;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *@ClassName FileDemo
 *@Description TODO
 *@Author William
 *@Date 2019/8/8 14:51
 *@Version 1.0
 */
public class FileDemo {

    public static void main(String[] args) throws Exception {
        //changeFileName("/Users/lxl/Desktop/123/电子档（350X500）");
        String a = "2018年6对处理结果很不满意月在移动公司网点处购买手机";
        String a2 = "对处理结果很不满意";
        String a3 = "2019年移动宽带在我不知情的情况下弄了个每月保底消费58的宽带处理结果很不满意套餐 移动很清楚的知道我每个月的保底消费根本达不到58 中国移动侵犯了我的消费者权益 没有做到让消费者知根知底 2022年我退了宽带 中国移动那边只愿意给我退309 我不满意他们的方案 投诉了多次没有结果 最近一次他们处理了十天还是没有结果 我希望工信部能帮忙处理";
        String a4 = "我打了很多次电话，他总是以我的手机号还有什么签约还没到，必须等合约到，当初我班这个号码就是为了拉宽带 ，现在家里没人用了 我想把号码注销掉，打移动客服总是推三阻四，各种理由，要什么本人，带上有效证件，到他指定的营养厅办理注销。";
        String a5 = "保底消费收费不合理，虚拟网月费，来电提醒费用，增值业务费，都不算在内，";
        List<String> list = new ArrayList<>();
        list.add(a);
        list.add(a2);
        list.add(a3);
        list.add(a4);
        list.add(a5);
        Map<Integer, Map<Integer, Object>> map = new HashMap<>();
        Map<Integer, Object> m = new HashMap<>();
        m.put(1, list);
        map.put(1, m);
        map.put(2, m);
        map.put(3, m);
        map.put(4, m);
        Map<Integer, Map<Integer, Object>> list2 = new HashMap<>(map);
        for(int l = 1; l < map.size(); l++){
            if (l == 2) {
                list2.remove(l);
                System.out.println(list2);
            }if (l == 3) {
                Map<Integer, Map<Integer, Object>> list3 = new HashMap<>(map);
                list3.remove(l);
                System.out.println(list3);
            }
        }
//        for(int l = 0; l < list.size(); l++){
//            String s = list.get(l);
//            List<String> list2 = new ArrayList<>(list);
//            list2.remove(l);
//            for (int i = 0; i < s.length()-8; i++) {
//                String substring = s.substring(i, i+8);
//                for(String s2 : list2){
//                    if(s2.contains(substring)){
//                        System.out.println("重复数据："+substring+"，重复listid："+l);
//                    }
//                }
//            }
//        }


    }

    /**
     *@description: 通过文件路径，修改该路径下所有文件的名字
     * @param path 文件夹路径
     * @return:
     * @author: William
     * @date 2019/8/8 14:52
     */
    public static void changeFileName(String path) throws Exception {
        File file = new File(path);
        if(file.exists()){
            File[] files = file.listFiles();
            if (null == files || files.length == 0) {
                System.out.println("文件夹是空的!");
                return;
            } else {
                for (File file2 : files) {
                    if (file2.isDirectory()) {
                        changeFileName(file2.getAbsolutePath());
                    } else {
                        System.out.println("文件:" + file2.getAbsolutePath());
                        String filePath = file2.getAbsolutePath();
                        String fileName = filePath.substring(0,filePath.lastIndexOf("/"))+"/aaa"+filePath.substring(filePath.lastIndexOf("."));
                        System.out.println("filePath:"+filePath);
                        System.out.println("fileName:"+fileName);
                        //File oriFile = new File(filePath);
                        //boolean b = oriFile.renameTo(new File(fileName));
                        //System.out.println(b);
                    }
                }
            }
        }else{
            System.out.println("该路径不存在");
        }

    }

}