package com.fc.v2.controller.xinlingshou;

import java.math.BigDecimal;

public class DeviceFloorGood {

    private Long goodsId;
    private BigDecimal price;
    private String name;
    private String image;


    private Integer defaultStore;
    private Integer store;
    private Integer floorIndex;
    private Integer colIndex;
    private Integer floor;
    private Integer colNum;
    private Integer colGoodsNum;

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getColNum() {
        return colNum;
    }

    public void setColNum(Integer colNum) {
        this.colNum = colNum;
    }

    public Integer getColGoodsNum() {
        return colGoodsNum;
    }

    public void setColGoodsNum(Integer colGoodsNum) {
        this.colGoodsNum = colGoodsNum;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getDefaultStore() {
        return defaultStore;
    }

    public void setDefaultStore(Integer defaultStore) {
        this.defaultStore = defaultStore;
    }

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        this.store = store;
    }

    public Integer getFloorIndex() {
        return floorIndex;
    }

    public void setFloorIndex(Integer floorIndex) {
        this.floorIndex = floorIndex;
    }

    public Integer getColIndex() {
        return colIndex;
    }

    public void setColIndex(Integer colIndex) {
        this.colIndex = colIndex;
    }
}
