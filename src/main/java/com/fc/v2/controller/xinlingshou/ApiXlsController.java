package com.fc.v2.controller.xinlingshou;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.amazonaws.util.Md5Utils;
import com.fc.v2.common.base.BaseController;
import com.fc.v2.mapper.auto.CompanyMapper;
import com.fc.v2.model.ApiResult;
import com.fc.v2.model.FacadeResponse;
import com.fc.v2.model.auto.Commodity;
import com.fc.v2.model.auto.Device;
import com.fc.v2.model.auto.Goodsorder;
import com.fc.v2.model.auto.Goodsorderdetail;
import com.fc.v2.service.*;
import com.fc.v2.shiro.util.ShiroUtils;
import com.fc.v2.util.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.fc.v2.util.HttpClientUtil.*;



@Api(tags = "售货机api模块")
@RestController
@RequestMapping("/shop")
public class ApiXlsController extends BaseController {

    private static String urll ="https://v2.lufff.com/hiApi/";
    private static String companyId ="790";

    @Autowired
    private CommodityService commodityService;
    @Resource
    private CompanyMapper companyMapper;
    @Autowired
    private MachineService machineService;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private GoodsorderService goodsorderService;
    @Autowired
    private GoodsorderdetailService goodsorderdetailService;

//=============================================================================
//=================================商品管理======================================
// ============================================================================
    /**
     * 查询云库商品
     * @return
     * @throws Exception
     */
    @GetMapping("/searchGoods")
    @ResponseBody
    public ApiResult searchGoods(Integer type,Integer start,Integer length) throws Exception {
        String url =urll+"searchGoods";
        Map<String, Object> params = new HashMap<>();
        params.put("type",type);
        params.put("start",start);
        params.put("length",length);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        if(parseObject.get("returnCode").equals("666")){
            List<Goods> list = JSON.parseArray((String) parseObject.get("resultMsg"), Goods.class);
            for (Goods goods : list){
                Commodity recordold = commodityService.selectBygoodsId(goods.getGoodsId());
                if(recordold == null){
                    if(goods.getLock() != 1){
                        Commodity recordnew = new Commodity();
                        recordnew.setNum1(goods.getGoodsId());
                        recordnew.setCommodityName(goods.getName());
                        recordnew.setRetailPrice(goods.getPrice());
                        recordnew.setCompanyDesc(goods.getSku());
                        recordnew.setBarCode(goods.getBarCode());
                        recordnew.setNum2(goods.getStatus());
                        recordnew.setNum3(goods.getRecognizeType());
                        recordnew.setStr1(goods.getImageUrl());
                        recordnew.setModifiedUserSid(Long.valueOf(goods.getAvgWeight()));
                        commodityService.insertSelective(recordnew);
                    }

                }

            }
        }
        return ResultUtils.ok(parseObject);
    }
    /**
     * 查询单个商品详细信息
     * @return
     * @throws Exception
     */
    @GetMapping("/getGoodsDetailInfo")
    @ResponseBody
    public ApiResult getGoodsDetailInfo(Integer goodsId) throws Exception {
        String url =urll+"getGoodsDetailInfo";
        Map<String, Object> params = new HashMap<>();
        params.put("goodsId",goodsId);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 新商品建模
     * 目前云库有10w+商品，如果云库不存在可以通过该接口提交新商品
     * @return
     * @throws Exception
     */
    @PostMapping("/upload_photo")
    @ResponseBody
    public ApiResult upload_photo(
                                  @RequestParam(value = "isRemoveBG")String isRemoveBG,
                                  @RequestParam(value = "file")MultipartFile file, HttpServletRequest request) throws Exception {
        String url =urll+"upload_photo";
        String filePath = System.getProperty("user.dir")+File.separator+"upload";
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        fileName = UUID.randomUUID() + suffixName;
        File fileDir  = new File(filePath);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        File dest = new File(fileDir.getAbsolutePath() + File.separator + fileName);
        try {
            file.transferTo(dest);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, Object> params = new HashMap<>();
        params.put("fileName",file.getOriginalFilename());
        params.put("isRemoveBG",isRemoveBG);
        Map<String, Object> newParams = getParams(params);
        newParams.put("goodsImage",dest);
        String s = doUpload(url, newParams);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        if(parseObject.get("returnCode").equals("666")){
            String imgurl = (String) parseObject.get("resultMsg");
            return ResultUtils.ok(imgurl);
        }
        return ResultUtils.ok(parseObject);
    }
    /**
     * 新商品申请 提交/修改
     * @return
     * @throws Exception
     */
    @GetMapping("/submitGood")
    @ResponseBody
    public ApiResult submitGood(String outGoodId, String goodName,
                                BigDecimal price, Integer type,
                                String sku, Integer weight,
                                Integer categoryId, String barcode,
                                String imageUrl1, String imageUrl2,
                                String imageUrl3, String imageUrl4
            , String imageUrl5, Integer submitType) throws Exception {
        String url =urll+"submitGood";
        Map<String, Object> params = new HashMap<>();
        params.put("outGoodId",outGoodId);
        params.put("goodName",goodName);
        params.put("price",price);
        params.put("type",type);
        params.put("sku",sku);
        params.put("weight",weight);
        params.put("categoryId",categoryId);
        params.put("barcode",barcode);
        params.put("imageUrl1",imageUrl1);
        params.put("imageUrl2",imageUrl2);
        params.put("imageUrl3",imageUrl3);
        params.put("imageUrl4",imageUrl4);
        params.put("imageUrl5",imageUrl5);
        params.put("submitType",submitType);
        Map<String, Object> newParams = getParams(params);
        //String s= doPostJson(url, newParams);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }


    /**
     * 新商品申请 查询已提交的申请
     * @return
     * @throws Exception
     */
    @GetMapping("/getSubmitGoodsList")
    @ResponseBody
    public ApiResult getSubmitGoodsList(String outGoodId, String goodName,String status, Integer start,
                                        Integer length) throws Exception {
        String url =urll+"getSubmitGoodsList";
        Map<String, Object> params = new HashMap<>();
        params.put("outGoodId",outGoodId);
        params.put("goodName",goodName);
        params.put("status",status);
        params.put("start",start);
        params.put("length",length);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }

    /**
     * 新商品申请 审核结果通知
     * @return
     * @throws Exception
     */
    @GetMapping("/审核结果通知")
    @ResponseBody
    public ApiResult shenghejieguotongzhi(String outGoodId, String goodsId, String goodName,
                                          String code,String msg) throws Exception {
        String url =urll+"审核结果通知";
        Map<String, Object> params = new HashMap<>();
        params.put("outGoodId",outGoodId);
        params.put("goodsId",goodsId);
        params.put("goodName",goodName);
        params.put("code",code);
        params.put("msg",msg);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 商品上架 获取设备层列
     * @return
     * @throws Exception
     */
    @GetMapping("/getDeviceFloorList")
    @ResponseBody
    public ApiResult getDeviceFloorList(String deviceCode) throws Exception {
        String url =urll+"getDeviceFloorList";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 商品上架 设置柜体货架层数
     * @return
     * @throws Exception
     */
    @GetMapping("/setDeviceMaxFloor")
    @ResponseBody
    public ApiResult setDeviceMaxFloor(String deviceCode, Integer maxFloor) throws Exception {
        String url =urll+"setDeviceMaxFloor";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("maxFloor",maxFloor);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 商品上架 商品上架到货道
     * @return
     * @throws Exception
     */
    @GetMapping("/onlineDeviceFloorGood")
    @ResponseBody
    public ApiResult onlineDeviceFloorGood(String deviceCode, Integer goodId, Integer row, Integer col) throws Exception {
        String url =urll+"onlineDeviceFloorGood";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("goodId",goodId);
        params.put("row",row);
        params.put("col",col);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 商品上架 设备货道商品批量上架
     * @return
     * @throws Exception
     */
    @GetMapping("/batchOnlineDeviceFloorGood")
    @ResponseBody
    public ApiResult batchOnlineDeviceFloorGood(String deviceCode, String goodId, String row, String col) throws Exception {
        String url =urll+"batchOnlineDeviceFloorGood";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("goodId",goodId);
        params.put("row",row);
        params.put("col",col);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 商品下架 设备货道商品下架
     * @return
     * @throws Exception
     */
    @GetMapping("/offlineDeviceFloorGood")
    @ResponseBody
    public ApiResult offlineDeviceFloorGood(String deviceCode, Integer goodId, Integer row, Integer col) throws Exception {
        String url =urll+"offlineDeviceFloorGood";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("goodId",goodId);
        params.put("row",row);
        params.put("col",col);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 商品下架 设备货道商品批量下架
     * @return
     * @throws Exception
     */
    @GetMapping("/batchOfflineDeviceFloorGood")
    @ResponseBody
    public ApiResult batchOfflineDeviceFloorGood(String deviceCode, String goodId, String row, String col) throws Exception {
        String url =urll+"batchOfflineDeviceFloorGood";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("goodId",goodId);
        params.put("row",row);
        params.put("col",col);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 查询出售中商品
     * @return
     * @throws Exception
     */
    @GetMapping("/getDeviceFloorGood")
    @ResponseBody
    public ApiResult getDeviceFloorGood(String deviceCode) throws Exception {
        String url =urll+"getDeviceFloorGood";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    //=============================================================================
    //=================================设备管理======================================
    // ============================================================================
    /**
     * 查询设备信息  查询单个设备信息
     * @return
     * @throws Exception
     */
    @GetMapping("/getDevicesInfo")
    @ResponseBody
    public ApiResult getDevicesInfo(String deviceCode) throws Exception {
        String url =urll+"getDevicesInfo";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 查询设备信息  所有设备信息
     * @return
     * @throws Exception
     */
    @GetMapping("/getAllDeviceCode")
    @ResponseBody
    public ApiResult getAllDeviceCode(Integer status,Integer start,Integer length) throws Exception {
        String url =urll+"getAllDeviceCode";
        Map<String, Object> params = new HashMap<>();
        if(status != null){
            params.put("status",status);
        }
        if(start != null){
            params.put("start",start);
        }
        if(length != null){
            params.put("length",length);
        }
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        if(parseObject.get("returnCode").equals("666")){
            List<Device> list = JSON.parseArray((String) parseObject.get("resultMsg"), Device.class);
            for (Device device : list){
                Device deviceold = deviceService.selectByPrimaryKey(String.valueOf(device.getId()));
                if(deviceold == null){
                    deviceService.insertSelective(device);
                }else{
                    deviceService.updateByPrimaryKeySelective(device);
                }
            }
        }
        return ResultUtils.ok(parseObject);
    }
    /**
     * 查询设备信息  查询最近上下线的设备
     * @return
     * @throws Exception
     */
    @GetMapping("/getAllDeviceNetStatusNewUpdate")
    @ResponseBody
    public ApiResult getAllDeviceNetStatusNewUpdate(Integer minutes) throws Exception {
        String url =urll+"getAllDeviceNetStatusNewUpdate";
        Map<String, Object> params = new HashMap<>();
        if(minutes != null){
            params.put("minutes",minutes);
        }else{
            params.put("minutes","60");
        }

        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 查询设备信息  查询设备网络记录
     * @return
     * @throws Exception
     */
    @GetMapping("/getDeviceNetLog")
    @ResponseBody
    public ApiResult getDeviceNetLog(String deviceCode,String startTime,String endTime,Integer start,Integer length) throws Exception {
        String url =urll+"getDeviceNetLog";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        if(start != null){
            params.put("start",start);
        }else{
            params.put("start",1);
        }
        if(length != null){
            params.put("length",length);
        }else{
            params.put("length",100);
        }

        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     *  设备开关门 设备开门
     * @return
     * @throws Exception
     */
    @GetMapping("/openDeviceDoor")
    @ResponseBody
    public ApiResult openDeviceDoor(String userId,Integer doorNum,String deviceCode,String type) throws Exception {
        String url =urll+"openDeviceDoor";
        Map<String, Object> params = new HashMap<>();
        params.put("userId",userId);
        if(doorNum!=null){
            params.put("doorNum",doorNum);
        }else{
            params.put("doorNum","");
        }
        params.put("deviceCode",deviceCode);
        params.put("type",type);
        params.put("payType","0");
        Map<String, Object> newParams = getParams2(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     *  设备开关门 设备开门通知
     * @return
     * @throws Exception
     */
    @GetMapping("/openDeviceDoorBackMsg")
    @ResponseBody
    public ApiResult openDeviceDoorBackMsg(String userId,String deviceCode,String requestSerial) throws Exception {
        String url =urll+"openDeviceDoorBackMsg";
        Map<String, Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("deviceCode",deviceCode);
        params.put("type",requestSerial);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     *  设备开关门 设备关门通知
     * @return
     * @throws Exception
     */
    @GetMapping("/closeDeviceDoorBackMsg")
    @ResponseBody
    public ApiResult closeDeviceDoorBackMsg(String userId,String deviceCode,String requestSerial,Integer isOpenDoor) throws Exception {
        String url =urll+"closeDeviceDoorBackMsg";
        Map<String, Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("deviceCode",deviceCode);
        params.put("requestSerial",requestSerial);
        params.put("isOpenDoor",isOpenDoor);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     *  设备开关门 锁定/解锁 设备
     * @return
     * @throws Exception
     */
    @GetMapping("/forbiddenDevice")
    @ResponseBody
    public ApiResult forbiddenDevice(String deviceCode,Integer type) throws Exception {
        String url =urll+"forbiddenDevice";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("type",type);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     *  设备重启 重启设备
     * @return
     * @throws Exception
     */
    @GetMapping("/rebootDevices")
    @ResponseBody
    public ApiResult rebootDevices(String deviceCode) throws Exception {
        String url =urll+"rebootDevices";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 音量调节  修改设备音量
     * @return
     * @throws Exception
     */
    @GetMapping("/modifyVolume")
    @ResponseBody
    public ApiResult modifyVolume(String deviceCode,String type,String volumeType) throws Exception {
        String url =urll+"modifyVolume";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("type",type);
        params.put("volumeType",volumeType);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 广告管理  修改设备广告
     * @return
     * @throws Exception
     */
    @GetMapping("/updateDeviceAdv")
    @ResponseBody
    public ApiResult updateDeviceAdv(String deviceCode,String advName,Integer advSize,String urll) throws Exception {
        String url =urll+"updateDeviceAdv";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("advName",advName);
        params.put("advSize",advSize);
        params.put("url",urll);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 温控管理  查询温控配置（仅适用于带智能温控设备）
     * @return
     * @throws Exception
     */
    @GetMapping("/getDeviceTempSet")
    @ResponseBody
    public ApiResult getDeviceTempSet(String deviceCode) throws Exception {
        String url =urll+"getDeviceTempSet";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 温控管理  修改温控配置（仅适用于带智能温控设备）
     * @return
     * @throws Exception
     */
    @GetMapping("/changeDeviceTempSet")
    @ResponseBody
    public ApiResult changeDeviceTempSet(String deviceCode,Integer tempModel,Integer tempThreshold,
                                         String autoLowTempModeStart,String autoLowTempModeEnd) throws Exception {
        String url =urll+"changeDeviceTempSet";
        Map<String, Object> params = new HashMap<>();
        params.put("deviceCode",deviceCode);
        params.put("tempModel",tempModel);
        params.put("tempThreshold",tempThreshold);
        params.put("autoLowTempModeStart",autoLowTempModeStart);
        params.put("autoLowTempModeEnd",autoLowTempModeEnd);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }

    //=============================================================================
    //=================================订单管理======================================
    // ============================================================================
    /**
     * 订单商品上架快照
     * @return
     * @throws Exception
     */
    @GetMapping("/getOnlineGoodsByOrderCode")
    @ResponseBody
    public ApiResult getOnlineGoodsByOrderCode(String orderCode) throws Exception {
        String url =urll+"getOnlineGoodsByOrderCode";
        Map<String, Object> params = new HashMap<>();
        params.put("orderCode",orderCode);
        Map<String, Object> newParams = getParams(params);
        String s= doPostNew(newParams, url);
        Map<String, Object> parseObject = JSON.parseObject(s,Map.class);
        return ResultUtils.ok(parseObject);
    }
    /**
     * 订单
     * @return
     * @throws Exception
     */
    @GetMapping("/orderCallback")
    @ResponseBody
    public ApiResult orderCallback(@RequestBody Goodsorder goodsorder) throws Exception {
        if(goodsorder != null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            String format = simpleDateFormat.format(new Date());
            String orderno = goodsorder.getDeviceCode() + "-" + format;
            goodsorder.setOrderNo(orderno);
            int i = goodsorderService.insertSelective(goodsorder);
            if(i > 0){
                List<Goodsorderdetail> goodsList = goodsorder.getGoodsList();
                if(goodsList.size() >0 ){
                    for(Goodsorderdetail goodsorderdetail : goodsList){
                        goodsorderdetail.setStr1(orderno);
                        goodsorderdetailService.insertSelective(goodsorderdetail);
                    }

                }
            }
        }
        return ResultUtils.ok("true");
    }

    @GetMapping("/notifySubmitGoodsResult")
    @ResponseBody
    public ApiResult notifySubmitGoodsResult(@RequestBody Goods goods) throws Exception {
        if(goods != null){
            Commodity commodity = commodityService.selectByOutGoodId(goods.getOutGoodId());
            if(commodity != null){
                if(goods.getCode().equals("666")){
                    commodity.setCityCode("2");
                }else if(goods.getCode().equals("444")){
                    commodity.setCityCode("3");
                }

                commodity.setNotes(goods.getMsg());
                commodityService.updateByPrimaryKeySelective(commodity);
            }
        }
        return ResultUtils.ok("true");
    }
    //=======================================================================

    public String doPostNew(Map<String, Object> newParams, String url) throws Exception {
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        return  doPost(url,newParams,header);
    }



    public FacadeResponse multipartPost(Map<String, Object> newParams, String url) throws Exception {
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/x-www-form-urlencoded");
        return HttpFiled.multipartPost(url,header,newParams);
    }

    public Map<String, Object> getParams(Map<String, Object> params) {
        String nonceStr = generateRandomString(15);
        String requestSerial = generateRandomString(15);
        String timeStamp = DateUtil.format(new Date(), "yyyyMMddHHmmssSSS");;
        params.put("companyId",companyId);
        params.put("nonceStr",nonceStr);
        params.put("requestSerial",requestSerial);
        params.put("timeStamp",timeStamp);
        String sign = createSign(params);
        params.put("sign",sign);
        return params;
    }
    public Map<String, Object> getParams2(Map<String, Object> params) {
        String nonceStr = generateRandomString(15);
        String requestSerial = generateRandomString(15);
        String timeStamp = DateUtil.format(new Date(), "yyyyMMddHHmmssSSS");;
        params.put("nonceStr",nonceStr);
        params.put("requestSerial",requestSerial);
        params.put("timeStamp",timeStamp);
        String sign = createSign2(params);
        params.put("sign",sign);
        return params;
    }

    public String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder();
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        int charLength = characters.length();

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * charLength);
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    public static String createSign(Map<String, Object> params){
        StringBuilder sb = new StringBuilder();
        // 将参数以参数名的字典升序排序
        Map<String, Object> sortParams = new TreeMap<String, Object>(params);

        for (Map.Entry<String, Object> entry : sortParams.entrySet()) {
            String key = entry.getKey();
            String value =  entry.getValue().toString().trim();
            if (!StringUtils.isEmpty(value)){
                sb.append("&").append(key).append("=").append(value);
            }
        }
        String stringA = sb.toString().replaceFirst("&","");
        String key = stringA + "&key=zJyOuZhAnSiGn";
        //将签名使用MD5加密并全部字母变为大写
        String signValue = MD5Util.hash(key).toUpperCase();
        return signValue;
    }

    public static String createSign2(Map<String, Object> params){
        StringBuilder sb = new StringBuilder();
        // 将参数以参数名的字典升序排序
        Map<String, Object> sortParams = new TreeMap<String, Object>(params);

        for (Map.Entry<String, Object> entry : sortParams.entrySet()) {
            String key = entry.getKey();
            String value =  entry.getValue().toString().trim();
            if (!StringUtils.isEmpty(value)){
                sb.append("&").append(key).append("=").append(value);
            }
        }
        String stringA = sb.toString().replaceFirst("&","");
        String key = stringA + "&key=zJyOuZhAnSiGn";
        //将签名使用MD5加密并全部字母变为大写
        String signValue = MD5Util.hash(key).toUpperCase();
        return signValue;
    }


}
