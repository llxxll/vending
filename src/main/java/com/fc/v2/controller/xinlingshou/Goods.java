package com.fc.v2.controller.xinlingshou;

import java.math.BigDecimal;


public class Goods {
    private String goodName;
    private BigDecimal price;
    private Integer lock;
    private String sku;
    private String barCode;
    private String name;
    private Integer status;
    private Integer recognizeType;
    private Long goodsId;
    private String imageUrl;
    private Integer avgWeight;
    private String outGoodId;
    private String goodsName;
    private String code;
    private String msg;


    public String getOutGoodId() {
        return outGoodId;
    }

    public void setOutGoodId(String outGoodId) {
        this.outGoodId = outGoodId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getLock() {
        return lock;
    }

    public void setLock(Integer lock) {
        this.lock = lock;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRecognizeType() {
        return recognizeType;
    }

    public void setRecognizeType(Integer recognizeType) {
        this.recognizeType = recognizeType;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getAvgWeight() {
        return avgWeight;
    }

    public void setAvgWeight(Integer avgWeight) {
        this.avgWeight = avgWeight;
    }
}
