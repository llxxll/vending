package com.fc.v2.controller.xinlingshou;

public class WangLuo {

    private String createtime;//时间

    private Integer pingcount;//自设备上线ping的总次数。ping每30秒一次，每6次形成一条记录
    private Integer discardcount;//周期内ping的丢包数
    private Integer maxdelay;//周期内ping的最大延迟
    private Integer advdelay;//周期内ping的平均延迟
    private String netType;//设备网络类型，例4G;2G,空则为未知
    private String netQuality;//设备信号强度，1-4，4最好1最差。其他为未知

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Integer getPingcount() {
        return pingcount;
    }

    public void setPingcount(Integer pingcount) {
        this.pingcount = pingcount;
    }

    public Integer getDiscardcount() {
        return discardcount;
    }

    public void setDiscardcount(Integer discardcount) {
        this.discardcount = discardcount;
    }

    public Integer getMaxdelay() {
        return maxdelay;
    }

    public void setMaxdelay(Integer maxdelay) {
        this.maxdelay = maxdelay;
    }

    public Integer getAdvdelay() {
        return advdelay;
    }

    public void setAdvdelay(Integer advdelay) {
        this.advdelay = advdelay;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public String getNetQuality() {
        return netQuality;
    }

    public void setNetQuality(String netQuality) {
        this.netQuality = netQuality;
    }
}
