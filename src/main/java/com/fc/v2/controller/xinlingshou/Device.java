package com.fc.v2.controller.xinlingshou;

public class Device {

    private String simCardNumber;//物联网卡卡号
    private String simCardLastRecharge;//物联网卡到期时间（仅支持小麦采购的网卡）
    private String code;//设备活码

    private Integer cameraStatus;//
    private String cpuId;//
    private String nickName;//

    private Integer isOpend;//0未知2关门1开门
    private Integer forbid;//
    private Integer productiontime;//

    private Integer netStatus;//0未知2离线1在线
    private Integer lastkeepalivetime;//
    private Integer deviceStatus;//0正常1限制使用，由平台进行操作

    private String macAddress;//

    private Integer isDeleted;//
    private Integer isForbidden;//0未锁定1锁定（限制消费者开门），商户可以操作
    private Integer lockStatus;//0未知2开锁1关锁

    private Integer createTime;//
    private Integer eleStatus;//0未知2断电1通电
    private Integer id;//

    private String deviceinfo;//json 字符串，productType 0单开门1双开门，adEnabled 0无广告1广告
    private Integer status;//默认1设备已出厂，其他未出厂需要先激活

    public String getSimCardNumber() {
        return simCardNumber;
    }

    public void setSimCardNumber(String simCardNumber) {
        this.simCardNumber = simCardNumber;
    }

    public String getSimCardLastRecharge() {
        return simCardLastRecharge;
    }

    public void setSimCardLastRecharge(String simCardLastRecharge) {
        this.simCardLastRecharge = simCardLastRecharge;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCameraStatus() {
        return cameraStatus;
    }

    public void setCameraStatus(Integer cameraStatus) {
        this.cameraStatus = cameraStatus;
    }

    public String getCpuId() {
        return cpuId;
    }

    public void setCpuId(String cpuId) {
        this.cpuId = cpuId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getIsOpend() {
        return isOpend;
    }

    public void setIsOpend(Integer isOpend) {
        this.isOpend = isOpend;
    }

    public Integer getForbid() {
        return forbid;
    }

    public void setForbid(Integer forbid) {
        this.forbid = forbid;
    }

    public Integer getProductiontime() {
        return productiontime;
    }

    public void setProductiontime(Integer productiontime) {
        this.productiontime = productiontime;
    }

    public Integer getNetStatus() {
        return netStatus;
    }

    public void setNetStatus(Integer netStatus) {
        this.netStatus = netStatus;
    }

    public Integer getLastkeepalivetime() {
        return lastkeepalivetime;
    }

    public void setLastkeepalivetime(Integer lastkeepalivetime) {
        this.lastkeepalivetime = lastkeepalivetime;
    }

    public Integer getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(Integer deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getIsForbidden() {
        return isForbidden;
    }

    public void setIsForbidden(Integer isForbidden) {
        this.isForbidden = isForbidden;
    }

    public Integer getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Integer lockStatus) {
        this.lockStatus = lockStatus;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public Integer getEleStatus() {
        return eleStatus;
    }

    public void setEleStatus(Integer eleStatus) {
        this.eleStatus = eleStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviceinfo() {
        return deviceinfo;
    }

    public void setDeviceinfo(String deviceinfo) {
        this.deviceinfo = deviceinfo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
